# Dashboard Client

## Requirements

- [Node.js](https://nodejs.org/en/), the latest 10.x LTS version.
- [Firefox](https://www.mozilla.org/en-US/firefox/new/) web browser. (optional, needed for unit tests in headless mode)

## Development

### Setup

The easiest way to setup the development environment is to simply run `make`. This can only be done, though, on an environment that supports building the tapestry application.

On other platforms, like Windows or MacOS, run the following commands.

```
npm install
npm run rest-client
```

> In order to generate the rest-client, you must have the compiled swagger files. The compiled swagger files can only be built on an environment that supports building the tapestry application.  Run `make compile-swagger` at `/pangolin`, on a supported environment, then download these files to the same location.
>
> * ./mneme/rest/swagger/swagger.compiled.yaml
> * ./weaver/rest/swagger/swagger.compiled.yaml
> * ./heimdallr/swagger/swagger.compiled.yaml
### Run in dev mode

Running the application in dev mode allows you to modify code, while compilation occurs in the background, and the application on the browser is refreshed automatically when updated.

Modify file `proxy.conf.json`, to specify the desired back-end.  By default, all API requests are directed to `http://localhost:3030`, which is the local mock server. Other popular destinations are included in the file, and to activate, set the key to `"/"`.  For example, if you wish to utilize the e2e server, do the following:
1. Change line `"/": {` to `"mock server": {`
1. Change line `"e2e server": {` to `"/": {`

To start the dev server, run the command `npm start`.  After a minute of compilation, the application should now be accessible via `http://localhost:4200`. Whenever a source file in `dashboard/web` is modified and saved, the application will automatically re-compile and the browser will reload the application.

## Test

To run the unit tests, execute the command `npm run test`. This executes all unit tests in headless mode (Firefox must be installed).

For unit test development, it will be useful to run the unit tests in the foreground. To do so, execute the command `npx ng test --watch --browsers Chrome`. This executes all unit tests, in the foreground, using the Chrome web browser.  When a source file in `dashboard/web` is modified and saved, the application will automatically re-compile and all unit tests are re-executed.

To only execute specific tests, change the test's `it()` or test suite's `describe()` functions to `fit()` or `fdescribe()`. This is extremely useful when concentrating on a specific set of test(s).
