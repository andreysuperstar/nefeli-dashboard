export * from './tenants.service';
import { TenantsService } from './tenants.service';
export * from './users.service';
import { UsersService } from './users.service';
export const APIS = [TenantsService, UsersService];
