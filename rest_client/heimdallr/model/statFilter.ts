/**
 * Authentication & Authorization
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type StatFilter = 'STATS_FILTER_UNSET' | 'STATS_FILTER_MEMORY_USED' | 'STATS_FILTER_MEMORY_AVAILABLE' | 'STATS_FILTER_THROUGHPUT' | 'STATS_FILTER_THROUGHPUT_OUT' | 'STATS_FILTER_THROUGHPUT_IN' | 'STATS_FILTER_THROUGHPUT_SERVER_OUT' | 'STATS_FILTER_THROUGHPUT_SERVER_IN' | 'STATS_FILTER_THROUGHPUT_SERVICE_OUT' | 'STATS_FILTER_THROUGHPUT_SERVICE_IN' | 'STATS_FILTER_CORES_USED' | 'STATS_FILTER_CORES_TOTAL' | 'STATS_FILTER_CONFIGURED_SERVICES' | 'STATS_FILTER_LATENCY' | 'STATS_FILTER_LATENCY_TOTAL' | 'STATS_FILTER_PACKET_LOSS' | 'STATS_FILTER_PACKET_LOSS_TOTAL';

export const StatFilter = {
    UNSET: 'STATS_FILTER_UNSET' as StatFilter,
    MEMORYUSED: 'STATS_FILTER_MEMORY_USED' as StatFilter,
    MEMORYAVAILABLE: 'STATS_FILTER_MEMORY_AVAILABLE' as StatFilter,
    THROUGHPUT: 'STATS_FILTER_THROUGHPUT' as StatFilter,
    THROUGHPUTOUT: 'STATS_FILTER_THROUGHPUT_OUT' as StatFilter,
    THROUGHPUTIN: 'STATS_FILTER_THROUGHPUT_IN' as StatFilter,
    THROUGHPUTSERVEROUT: 'STATS_FILTER_THROUGHPUT_SERVER_OUT' as StatFilter,
    THROUGHPUTSERVERIN: 'STATS_FILTER_THROUGHPUT_SERVER_IN' as StatFilter,
    THROUGHPUTSERVICEOUT: 'STATS_FILTER_THROUGHPUT_SERVICE_OUT' as StatFilter,
    THROUGHPUTSERVICEIN: 'STATS_FILTER_THROUGHPUT_SERVICE_IN' as StatFilter,
    CORESUSED: 'STATS_FILTER_CORES_USED' as StatFilter,
    CORESTOTAL: 'STATS_FILTER_CORES_TOTAL' as StatFilter,
    CONFIGUREDSERVICES: 'STATS_FILTER_CONFIGURED_SERVICES' as StatFilter,
    LATENCY: 'STATS_FILTER_LATENCY' as StatFilter,
    LATENCYTOTAL: 'STATS_FILTER_LATENCY_TOTAL' as StatFilter,
    PACKETLOSS: 'STATS_FILTER_PACKET_LOSS' as StatFilter,
    PACKETLOSSTOTAL: 'STATS_FILTER_PACKET_LOSS_TOTAL' as StatFilter
};
