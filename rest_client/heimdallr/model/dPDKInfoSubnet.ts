/**
 * Authentication & Authorization
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type DPDKInfoSubnet = 'NULL' | 'GENERIC' | 'INTRA' | 'EXTRA' | 'LAN' | 'WAN' | 'HA';

export const DPDKInfoSubnet = {
    NULL: 'NULL' as DPDKInfoSubnet,
    GENERIC: 'GENERIC' as DPDKInfoSubnet,
    INTRA: 'INTRA' as DPDKInfoSubnet,
    EXTRA: 'EXTRA' as DPDKInfoSubnet,
    LAN: 'LAN' as DPDKInfoSubnet,
    WAN: 'WAN' as DPDKInfoSubnet,
    HA: 'HA' as DPDKInfoSubnet
};
