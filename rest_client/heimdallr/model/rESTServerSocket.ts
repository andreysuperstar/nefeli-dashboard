/**
 * Authentication & Authorization
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { RESTServerHugepages } from './rESTServerHugepages';
import { RESTServerNetworkInterface } from './rESTServerNetworkInterface';


export interface RESTServerSocket { 
    cores?: Array<number>;
    hugepages?: Array<RESTServerHugepages>;
    id?: number;
    /**
     * Currently unused.
     */
    interfaces?: Array<RESTServerNetworkInterface>;
    memory?: string;
}

