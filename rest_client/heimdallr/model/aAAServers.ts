/**
 * Authentication & Authorization
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { AAAServer } from './aAAServer';
import { CollectionMetadata } from './collectionMetadata';


export interface AAAServers { 
    /**
     * A collection of AAA Servers.
     */
    aaaServers?: Array<AAAServer>;
    metadata?: CollectionMetadata;
}

