export * from './files.service';
import { FilesService } from './files.service';
export * from './health.service';
import { HealthService } from './health.service';
export const APIS = [FilesService, HealthService];
