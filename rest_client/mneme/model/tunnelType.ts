/**
 * mneme-rest-server
 * Mnmeme REST Server
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type TunnelType = 'DUMMY_TunnelTYPE' | 'RAW' | 'VXLAN' | 'VLAN' | 'GREINUDP' | 'VLAN_QINQ' | 'OTHER';

export const TunnelType = {
    DUMMYTunnelTYPE: 'DUMMY_TunnelTYPE' as TunnelType,
    RAW: 'RAW' as TunnelType,
    VXLAN: 'VXLAN' as TunnelType,
    VLAN: 'VLAN' as TunnelType,
    GREINUDP: 'GREINUDP' as TunnelType,
    VLANQINQ: 'VLAN_QINQ' as TunnelType,
    OTHER: 'OTHER' as TunnelType
};
