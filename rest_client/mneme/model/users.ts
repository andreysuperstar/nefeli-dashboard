/**
 * mneme-rest-server
 * Mnmeme REST Server
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { CollectionMetadata } from './collectionMetadata';
import { User } from './user';


export interface Users { 
    metadata?: CollectionMetadata;
    users?: Array<User>;
}

