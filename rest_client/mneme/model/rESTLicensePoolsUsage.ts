/**
 * mneme-rest-server
 * Mnmeme REST Server
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { LicensePoolUsage } from './licensePoolUsage';


export interface RESTLicensePoolsUsage { 
    usageByPool?: { [key: string]: LicensePoolUsage; };
}

