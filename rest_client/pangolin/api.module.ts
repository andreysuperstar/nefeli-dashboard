import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';


import { AAAService } from './api/aAA.service';
import { AlarmsService } from './api/alarms.service';
import { AttachmentsService } from './api/attachments.service';
import { ExPConfigService } from './api/exPConfig.service';
import { ExPStatusService } from './api/exPStatus.service';
import { ExPTasksService } from './api/exPTasks.service';
import { HardwareService } from './api/hardware.service';
import { HealthService } from './api/health.service';
import { HighAvailabilityService } from './api/highAvailability.service';
import { LicensesService } from './api/licenses.service';
import { LogsService } from './api/logs.service';
import { NetworkFunctionsService } from './api/networkFunctions.service';
import { ProvisioningService } from './api/provisioning.service';
import { ServersService } from './api/servers.service';
import { ServiceTemplatesService } from './api/serviceTemplates.service';
import { ServicesService } from './api/services.service';
import { SitesService } from './api/sites.service';
import { SmtpService } from './api/smtp.service';
import { SoftwareProfilesService } from './api/softwareProfiles.service';
import { StatsService } from './api/stats.service';
import { SubscriptionService } from './api/subscription.service';
import { TapestryExportService } from './api/tapestryExport.service';
import { TenantsService } from './api/tenants.service';
import { TunnelsService } from './api/tunnels.service';
import { WeaverService } from './api/weaver.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: [
    AAAService,
    AlarmsService,
    AttachmentsService,
    ExPConfigService,
    ExPStatusService,
    ExPTasksService,
    HardwareService,
    HealthService,
    HighAvailabilityService,
    LicensesService,
    LogsService,
    NetworkFunctionsService,
    ProvisioningService,
    ServersService,
    ServiceTemplatesService,
    ServicesService,
    SitesService,
    SmtpService,
    SoftwareProfilesService,
    StatsService,
    SubscriptionService,
    TapestryExportService,
    TenantsService,
    TunnelsService,
    WeaverService ]
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
