/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type WeaverSyncStatusState = 'DISABLED' | 'ACTIVE' | 'STANDBY';

export const WeaverSyncStatusState = {
    DISABLED: 'DISABLED' as WeaverSyncStatusState,
    ACTIVE: 'ACTIVE' as WeaverSyncStatusState,
    STANDBY: 'STANDBY' as WeaverSyncStatusState
};
