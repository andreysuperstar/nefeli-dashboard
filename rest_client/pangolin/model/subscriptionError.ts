/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { BaseError } from './baseError';


export interface SubscriptionError { 
    httpStatus?: number;
    message: string;
    /**
     * * `2204` `LOCAL_CLUSTER_NOT_ACTIVE` The local cluster was not set to active when enabling weaversync. * `2900` `NOT_FOUND` * `2901` `UNAUTHORIZED` The user is unauthorized (library requests only). * `2902` `ID_IN_POST` POST cannot have an id. * `2903` `INVALID_VERSION` The version specified is invalid. * `2904` `INVALID_PATH` The path specified is invalid. 
     */
    errorCode?: number;
}

