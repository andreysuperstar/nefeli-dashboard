/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 *  - STAGED: The site has been registered with Weaver but has not come online. Weaver will attempt to configure it.  - PROVISIONING: ZTP is actively provisioning the site. Weaver will not attempt to configure it.  - ACTIVE: The site is in normal operating state. Weaver will keep its configuration up to date.  - INACTIVE: The site has been marked inactive by an operator (e.g., for maintenance). Weaver will not attempt to configure it.  - UNREACHABLE: Weaver is unable to reach or configure the site. The most recent data plane configuration is still active on the site. Weaver will attempt to reconnect.
 */
export type SiteConfigSiteStatus = 'SITE_STATUS_NULL' | 'STAGED' | 'PROVISIONING' | 'ACTIVE' | 'INACTIVE' | 'UNREACHABLE';

export const SiteConfigSiteStatus = {
    SITESTATUSNULL: 'SITE_STATUS_NULL' as SiteConfigSiteStatus,
    STAGED: 'STAGED' as SiteConfigSiteStatus,
    PROVISIONING: 'PROVISIONING' as SiteConfigSiteStatus,
    ACTIVE: 'ACTIVE' as SiteConfigSiteStatus,
    INACTIVE: 'INACTIVE' as SiteConfigSiteStatus,
    UNREACHABLE: 'UNREACHABLE' as SiteConfigSiteStatus
};
