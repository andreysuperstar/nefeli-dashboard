/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface RemoveMachineTaskRequest { 
    /**
     * a unique identifier for the request and server.
     */
    identifier?: string;
    leaseId?: string;
    /**
     * server UUID.
     */
    server?: string;
    /**
     * site UUID.
     */
    site?: string;
}

