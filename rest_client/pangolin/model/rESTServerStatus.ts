/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export type RESTServerStatus = 'UNSET' | 'GOOD' | 'WARNING' | 'CRITICAL' | 'UNKNOWN';

export const RESTServerStatus = {
    UNSET: 'UNSET' as RESTServerStatus,
    GOOD: 'GOOD' as RESTServerStatus,
    WARNING: 'WARNING' as RESTServerStatus,
    CRITICAL: 'CRITICAL' as RESTServerStatus,
    UNKNOWN: 'UNKNOWN' as RESTServerStatus
};
