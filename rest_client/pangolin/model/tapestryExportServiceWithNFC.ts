/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { ServiceConfig } from './serviceConfig';
import { TapestryExportNodeNFCConfig } from './tapestryExportNodeNFCConfig';


export interface TapestryExportServiceWithNFC { 
    configs?: Array<TapestryExportNodeNFCConfig>;
    id?: string;
    licensePoolId?: string;
    name?: string;
    policy?: ServiceConfig;
    tenantId?: string;
}

