/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
/* tslint:disable:no-unused-variable member-ordering */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams,
         HttpResponse, HttpEvent }                           from '@angular/common/http';
import { CustomHttpUrlEncodingCodec }                        from '../encoder';

import { Observable }                                        from 'rxjs';

import { EtcdSnapshot } from '../model/etcdSnapshot';
import { EtcdSyncError } from '../model/etcdSyncError';
import { WeaverSync } from '../model/weaverSync';
import { WeaverSyncConfig } from '../model/weaverSyncConfig';
import { WeaverSyncError } from '../model/weaverSyncError';

import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';


@Injectable({
  providedIn: 'root'
})
export class HighAvailabilityService {

    protected basePath = 'http://http:/v1';
    public defaultHeaders = new HttpHeaders();
    public configuration = new Configuration();

    constructor(protected httpClient: HttpClient, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {

        if (configuration) {
            this.configuration = configuration;
            this.configuration.basePath = configuration.basePath || basePath || this.basePath;

        } else {
            this.configuration.basePath = basePath || this.basePath;
        }
    }

    /**
     * @param consumes string[] mime-types
     * @return true: consumes contains 'multipart/form-data', false: otherwise
     */
    private canConsumeForm(consumes: string[]): boolean {
        const form = 'multipart/form-data';
        for (const consume of consumes) {
            if (form === consume) {
                return true;
            }
        }
        return false;
    }


    /**
     * 
     * Fetch the current WeaverSync configuration
     * @param status Include status information in response
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public getWeaverSync(status?: boolean, observe?: 'body', reportProgress?: boolean): Observable<WeaverSync>;
    public getWeaverSync(status?: boolean, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<WeaverSync>>;
    public getWeaverSync(status?: boolean, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<WeaverSync>>;
    public getWeaverSync(status?: boolean, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (status !== undefined && status !== null) {
            queryParameters = queryParameters.set('status', <any>status);
        }

        let headers = this.defaultHeaders;

        // authentication (SystemAdmin) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // authentication (SystemUser) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.get<WeaverSync>(`${this.configuration.basePath}/ha/weaversync`,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * Apply an etcd snapshot. For internal use only.
     * @param snapshot The etcd events to apply
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postEtcdSnapshot(snapshot: EtcdSnapshot, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postEtcdSnapshot(snapshot: EtcdSnapshot, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postEtcdSnapshot(snapshot: EtcdSnapshot, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postEtcdSnapshot(snapshot: EtcdSnapshot, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (snapshot === null || snapshot === undefined) {
            throw new Error('Required parameter snapshot was null or undefined when calling postEtcdSnapshot.');
        }

        let headers = this.defaultHeaders;

        // authentication (isAuthorized) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected !== undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.configuration.basePath}/ha/weaversync/etcd`,
            snapshot,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * Update the WeaverSync configuration (Operator)
     * @param config The configuration to apply
     * @param force Skip health check of standbys
     * @param Transaction_Id 
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postWeaverSync(config: WeaverSyncConfig, force?: boolean, Transaction_Id?: string, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postWeaverSync(config: WeaverSyncConfig, force?: boolean, Transaction_Id?: string, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postWeaverSync(config: WeaverSyncConfig, force?: boolean, Transaction_Id?: string, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postWeaverSync(config: WeaverSyncConfig, force?: boolean, Transaction_Id?: string, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (config === null || config === undefined) {
            throw new Error('Required parameter config was null or undefined when calling postWeaverSync.');
        }

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (force !== undefined && force !== null) {
            queryParameters = queryParameters.set('force', <any>force);
        }

        let headers = this.defaultHeaders;
        if (Transaction_Id !== undefined && Transaction_Id !== null) {
            headers = headers.set('Transaction-Id', String(Transaction_Id));
        }

        // authentication (SystemAdmin) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected !== undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.post<any>(`${this.configuration.basePath}/ha/weaversync`,
            config,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * Disable WeaverSync.
     * @param force Skip disabling standbys
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postWeaverSyncDisable(force?: boolean, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postWeaverSyncDisable(force?: boolean, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postWeaverSyncDisable(force?: boolean, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postWeaverSyncDisable(force?: boolean, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let queryParameters = new HttpParams({encoder: new CustomHttpUrlEncodingCodec()});
        if (force !== undefined && force !== null) {
            queryParameters = queryParameters.set('force', <any>force);
        }

        let headers = this.defaultHeaders;

        // authentication (SystemAdmin) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.post<any>(`${this.configuration.basePath}/ha/weaversync/disable`,
            null,
            {
                params: queryParameters,
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * Promote this Weaver instance to the primary role
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public postWeaverSyncPromotion(observe?: 'body', reportProgress?: boolean): Observable<any>;
    public postWeaverSyncPromotion(observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public postWeaverSyncPromotion(observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public postWeaverSyncPromotion(observe: any = 'body', reportProgress: boolean = false ): Observable<any> {

        let headers = this.defaultHeaders;

        // authentication (SystemAdmin) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
        ];

        return this.httpClient.post<any>(`${this.configuration.basePath}/ha/weaversync/promote`,
            null,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

    /**
     * 
     * Update the WeaverSync configuration. For internal use only. Operators should use POST /ha/weaversync.
     * @param config The configuration to apply
     * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
     * @param reportProgress flag to report request and response progress.
     */
    public putWeaverSync(config: WeaverSyncConfig, observe?: 'body', reportProgress?: boolean): Observable<any>;
    public putWeaverSync(config: WeaverSyncConfig, observe?: 'response', reportProgress?: boolean): Observable<HttpResponse<any>>;
    public putWeaverSync(config: WeaverSyncConfig, observe?: 'events', reportProgress?: boolean): Observable<HttpEvent<any>>;
    public putWeaverSync(config: WeaverSyncConfig, observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
        if (config === null || config === undefined) {
            throw new Error('Required parameter config was null or undefined when calling putWeaverSync.');
        }

        let headers = this.defaultHeaders;

        // authentication (isAuthorized) required
        if (this.configuration.apiKeys && this.configuration.apiKeys["Authorization"]) {
            headers = headers.set('Authorization', this.configuration.apiKeys["Authorization"]);
        }

        // to determine the Accept header
        const httpHeaderAccepts: string[] = [
            'application/json'
        ];
        const httpHeaderAcceptSelected: string | undefined = this.configuration.selectHeaderAccept(httpHeaderAccepts);
        if (httpHeaderAcceptSelected !== undefined) {
            headers = headers.set('Accept', httpHeaderAcceptSelected);
        }

        // to determine the Content-Type header
        const consumes: string[] = [
            'application/json'
        ];
        const httpContentTypeSelected: string | undefined = this.configuration.selectHeaderContentType(consumes);
        if (httpContentTypeSelected !== undefined) {
            headers = headers.set('Content-Type', httpContentTypeSelected);
        }

        return this.httpClient.put<any>(`${this.configuration.basePath}/ha/weaversync`,
            config,
            {
                withCredentials: this.configuration.withCredentials,
                headers: headers,
                observe: observe,
                reportProgress: reportProgress
            }
        );
    }

}
