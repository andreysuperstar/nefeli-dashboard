import { ServiceStatus } from '../rest_client/pangolin/model/serviceStatus';
import { InstanceStatusState } from '../rest_client/pangolin/model/instanceStatusState';
import { RESTGetTraceResponse } from '../rest_client/pangolin/model/rESTGetTraceResponse';

const status: ServiceStatus = {
  currentSiteVersions: {},
  currentVersion: 'new-version',
  nodes: {
    filter: {
      instances: {
        filter_1: {
          identity: {
            instanceId: 'filter_1'
          },
          location: {
            site: 'eric-exp',
            placement: {
              machineId: 'sylvester',
              bandwidthBySocket: {
                '0': '12112000'
              },
              draining: false,
              hugepagesBySocket: {
                '0': {
                  hugepages: { '1234': 3 }
                }
              },
              coresBySocket: {
                '0': {
                  cores: [2]
                }
              },
              license: null
            }
          },
          metadata: null,
          state: InstanceStatusState.CONFIGURED,
          launchSpec: null,
          systemdUnit: '',
          controlPorts: {},
          telnetConsole: '',
          placementStatus: {
            blockedOnMachineResources: false,
            blockedOnLicense: false
          },
          launchStatus: {
            blockedOnFiles: false,
            blockedOnHypervisor: false,
            blockedOnMemory: false
          }
        },
        filter_2: {
          identity: {
            instanceId: 'filter_2'
          },
          location: {
            site: 'eric-exp',
            placement: {
              machineId: 'fluffy',
              bandwidthBySocket: {
                '0': '12112000'
              },
              draining: false,
              hugepagesBySocket: {},
              coresBySocket: {
                '0': { cores: [2] }
              },
              license: null
            }
          },
          metadata: null,
          state: InstanceStatusState.FAILED,
          launchSpec: null,
          systemdUnit: '',
          controlPorts: {},
          telnetConsole: '',
          placementStatus: {
            blockedOnMachineResources: false,
            blockedOnLicense: false
          },
          launchStatus: {
            blockedOnFiles: false,
            blockedOnHypervisor: false,
            blockedOnMemory: false
          }
        },
        filter_3: {
          identity: {
            instanceId: 'filter_3'
          },
          location: {
            site: 'eric-exp',
            placement: {
              machineId: 'flock',
              bandwidthBySocket: {
                '0': '12112000'
              },
              draining: false,
              hugepagesBySocket: {},
              coresBySocket: {
                '0': { cores: [2] }
              },
              license: null
            }
          },
          metadata: null,
          state: InstanceStatusState.FAILED,
          launchSpec: null,
          systemdUnit: '',
          controlPorts: {},
          telnetConsole: '',
          placementStatus: {
            blockedOnMachineResources: false,
            blockedOnLicense: false
          },
          launchStatus: {
            blockedOnFiles: false,
            blockedOnHypervisor: false,
            blockedOnMemory: false
          }
        }
      }
    }
  },
  previousSiteVersions: {},
  requestedVersion: 'requested-version'
};

export default status;

export const getTrace = (value: string): RESTGetTraceResponse => {
  return {
    trace: {
      bpf: {
        value
      }
    }
  };
};
