import { RESTLicenses } from '../rest_client/pangolin/model/rESTLicenses';
import { LicenseState } from '../rest_client/pangolin/model/licenseState';

const license: RESTLicenses = {
  licenses: [{
    license: {
      config: {
        identifier: 'license-id1',
        nfId: 'nf-catalog-id1',
        dateAdded: '1559266898',
        startDate: '1559263898',
        endDate: '1559269898',
        name: 'Arista Router License',
        description: 'description for this license',
        data: ''
      },
      status: {
        state: LicenseState.AVAILABLE,
        rootPool: 'Default Root Pool',
        parentPool: 'Tenant Pool'
      }
    }
  }]
};

export default license;
