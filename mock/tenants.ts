import { Tenants } from '../rest_client/pangolin/model/tenants';
import { TenantSites } from '../rest_client/pangolin/model/tenantSites';

const tenants: Tenants = {
  tenants: [
    {
      identifier: '1',
      name: 'Tenant 1'
    },
    {
      identifier: '2',
      name: 'Tenant 2'
    },
    {
      identifier: '3',
      name: 'Tenant 3'
    },
    {
      identifier: '4',
      name: 'Nefeli Networks'
    }
  ],
  metadata: {
    index: 0,
    total: 3
  }
};

export default tenants;

export const getTenantSites = (): TenantSites => {
  return {
    sites: ['eric_site', 'andrew_site', 'anton_site', 'inactive_site']
  };
};
