/* tslint:disable: no-magic-numbers */

import { RESTServerSocketList } from '../rest_client/pangolin/model/rESTServerSocketList';

const sockets: RESTServerSocketList = {
  sockets: [
    {
      id: 0,
      memory: '33612103680',
      hugepages: [
        {
          size: '1073741824',
          count: 20
        }
      ],
      cores: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    }, {
      id: 1,
      memory: '33612103680',
      hugepages: [
        {
          size: '1073741824',
          count: 20
        }
      ],
      cores: [16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
    }
  ]
};

export default sockets;
