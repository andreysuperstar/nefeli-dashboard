/* tslint:disable: no-magic-numbers */
import { SystemStats } from "../rest_client/pangolin/model/systemStats";

// TODO(ecarino): enum & interfaces were copied, need to figure out an elegant way to reference from project
enum STATUS {
  UNKNOWN = 'unknown',
  CRITICAL = 'critical',
  WARN = 'warn',
  GOOD = 'good'
}

interface PacketLossStat {
  name: string;
  site: string;
  stats: Array<[number, string]>;
}

interface LatencyStat {
  name: string;
  site: string;
  stats: Array<[number, string]>;
}

interface ChartStats {
  throughputOut: number | Array<[number, string]>;
  throughputIn: number | Array<[number, string]>;
  packetLoss: number | Array<PacketLossStat>;
  latency: number | Array<LatencyStat>;
}

interface TenantStats extends ChartStats {
  id: string;
  name: string;
  services: number;
  packetLossTotal?: number;
  status: STATUS;
}

interface ServiceStat {
  id: string;
  name: string;
  site: string;
  stats: Array<any>;
}

interface ClusterStats extends ChartStats {
  id: number;
  services: number;
  coresUsed: number;
  coresTotal: number;
  memUsed: number;
  memTotal: number;
  throughputServiceOut: Array<ServiceStat>;
  throughputServiceIn: Array<ServiceStat>;
  capacity: number;
}

interface ClusterService {
  weaverService: string;
  expService: string;
  weaverTenant: string;
  expTenant: string;
}

const mockServices = ['Palo Alto', 'Load Balancing', 'Routing', 'Nefeli'];

const getStatsPoints = (startTime: number, seconds: number, startPoint: number): [number, string][] => {
  if (!seconds) {
    return [];
  }

  const stats = new Array<[number, string]>();
  let previousPoint = startPoint;
  const step = 1;
  const variance = 500;

  for (let i = seconds; i >= 0; i--) {
    if ((i % step) === 0) {
      const plusOrMinus = Math.random() < 0.5 ? -1 : 1;
      const p = (Math.floor(Math.random() * 50) + (previousPoint + (plusOrMinus * variance)));
      previousPoint = p;

      stats.push([
        startTime - i,
        p.toString()
      ]);
    }
  }

  return stats;
};

const getStatsFilter = (filter: string, totalStats: TenantStats | ClusterStats, duration = 300): TenantStats | ClusterStats => {
  const seconds = duration;
  const startTime = new Date().getTime() / 1000;

  if (filter === 'throughput') {
    totalStats[filter + 'In'] = getStatsPoints(startTime, seconds, 50000);
    totalStats[filter + 'Out'] = getStatsPoints(startTime, seconds, 40000);

    totalStats['throughputServiceOut'] = [
      {
        id: '4',
        name: 'c0_pid4',
        site: 'eric_site',
        stats: getStatsPoints(startTime, seconds, 22000)
      },
      {
        id: '5',
        name: 'ddp_pid2',
        site: 'anton_site',
        stats: getStatsPoints(startTime, seconds, 17000)
      }
    ];

    totalStats['throughputServiceIn'] = [
      {
        id: '4',
        name: 'c0_pid4',
        site: 'eric_site',
        stats: getStatsPoints(startTime, seconds, 25000)
      },
      {
        id: '5',
        name: 'ddp_pid2',
        site: 'anton_site',
        stats: getStatsPoints(startTime, seconds, 18000)
      }
    ];
  } else {
    totalStats[filter] = [
      {
        name: mockServices[0],
        site: 'eric_site',
        stats: getStatsPoints(startTime, seconds, 50000)
      }, {
        name: mockServices[1],
        site: 'anton_site',
        stats: getStatsPoints(startTime, seconds, 50000)
      }, {
        name: mockServices[2],
        site: 'andrew_site',
        stats: getStatsPoints(startTime, seconds, 50000)
      }, {
        name: mockServices[3],
        site: 'eric_site',
        stats: getStatsPoints(startTime, seconds, 50000)
      }
    ];
  }

  return totalStats;
};

export const postClusterStats = (): ClusterStats => {
  const stats: ClusterStats = {
    id: -1,
    services: 2,
    coresUsed: 2,
    coresTotal: 8,
    memUsed: 77,
    memTotal: 100,
    throughputOut: 678.0563143983617,
    throughputIn: 6482,
    capacity: 1,
    packetLoss: [],
    latency: [],
    throughputServiceOut: [],
    throughputServiceIn: []
  };

  return stats;
};

export const postTenantStats = (): TenantStats => {
  const stats = new Array<TenantStats>();

  stats.push({
    id: '-1',
    name: '',
    services: 2,
    throughputOut: 6382,
    throughputIn: 6482,
    status: STATUS.GOOD,
    packetLoss: [
      {
        name: 'Unclassified',
        site: 'Site 1',
        stats: [
          [
            1531154025,
            '356.6'
          ]
        ]
      },
      {
        name: 'customer0_pid4',
        site: 'Site 1',
        stats: [
          [
            1531154025,
            '21400.6'
          ]
        ]
      },
      {
        name: 'ddp_pid2',
        site: 'Site 1',
        stats: [
          [
            1531154025,
            '0'
          ]
        ]
      }
    ],
    latency: [
      {
        name: 'ddp_pid2',
        site: 'Site 1',
        stats: [
          [
            1531154093,
            '0'
          ]
        ]
      }
    ]
  },
    {
      id: '-2',
      name: '',
      services: 4,
      throughputOut: 6382,
      throughputIn: 6482,
      status: STATUS.WARN,
      packetLoss: [
        {
          name: 'Unclassified',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '356.6'
            ]
          ]
        },
        {
          name: 'customer0_pid4',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '21400.6'
            ]
          ]
        },
        {
          name: 'ddp_pid2',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '0'
            ]
          ]
        }
      ],
      latency: [
        {
          name: 'ddp_pid2',
          site: 'Site 1',
          stats: [
            [
              1531154093,
              '0'
            ]
          ]
        }
      ]
    },
    {
      id: '-3',
      name: '',
      services: 8,
      throughputOut: 5000,
      throughputIn: 5100,
      status: STATUS.CRITICAL,
      packetLoss: [
        {
          name: 'Unclassified',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '356.6'
            ]
          ]
        },
        {
          name: 'customer0_pid4',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '21400.6'
            ]
          ]
        },
        {
          name: 'ddp_pid2',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '0'
            ]
          ]
        }
      ],
      latency: [
        {
          name: 'ddp_pid2',
          site: 'Site 1',
          stats: [
            [
              1531154093,
              '0'
            ]
          ]
        }
      ]
    },
    {
      id: '-4',
      name: '',
      services: 1,
      throughputOut: 6382,
      throughputIn: 6482,
      status: STATUS.GOOD,
      packetLoss: [
        {
          name: 'Unclassified',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '356.6'
            ]
          ]
        },
        {
          name: 'customer0_pid4',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '21400.6'
            ]
          ]
        },
        {
          name: 'ddp_pid2',
          site: 'Site 1',
          stats: [
            [
              1531154025,
              '0'
            ]
          ]
        }
      ],
      latency: [
        {
          name: 'ddp_pid2',
          site: 'Site 1',
          stats: [
            [
              1531154093,
              '0'
            ]
          ]
        }
      ]
    });

  return stats[Math.floor(Math.random() * stats.length)];
};

export const postSystemStats = (): SystemStats => ({
  servicesTotal: 46,
  clusterSites: {
    total: 325,
    active: 322
  },
  ucpeSites: {
    total: 193,
    active: 191
  }
})

export const getClusterServices = (): ClusterService[] => {
  return mockServices.map(service => ({
    weaverService: service,
    expService: service,
    weaverTenant: service,
    expTenant: service
  }));
};

export const postTenantStatsFilter = (filter: string): TenantStats => {
  const totalStats = postTenantStats();
  return <TenantStats> getStatsFilter(filter, totalStats);
};

export const postClusterStatsFilter = (filter: string, duration?: number): ClusterStats => {
  const totalStats = postClusterStats();
  return <ClusterStats> getStatsFilter(filter, totalStats, duration);
};
