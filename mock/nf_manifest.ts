import { NFDescriptor } from '../rest_client/pangolin/model/nFDescriptor';

const manifest: NFDescriptor = {
  name: "vsrx3",
  type: "THIRD_PARTY",
  controllerType: "NF_CONTROLLER",
  scalingOptions: {
    supportsScaling: false,
    maxKppsPerInstance: "1000",
    useCpu: false
  },
  metadataOptions: {
    wrapper: "MAC_ADDRESS"
  },
  components: {
    datapath: {
      name: "vsrx",
      vendor: "Juniper Networks",
      type: "CONTROLLER",
      vmManifest: {
        images: [
          {
            image: {
              url: "file:///opt/nefeli/images/juniper/junos-vsrx3-x86-64-19.4R1.10.qcow2",
              checksum: "",
              checksumAlg: "NO_CHECKSUM"
            },
            format: "",
            device: "ide-hd,bus=ide.0,unit=0,bootindex=1"
          },
          {
            image: {
              url: "file:///opt/nefeli/images/juniper/vSRX3-init.iso",
              checksum: "",
              checksumAlg: "NO_CHECKSUM"
            },
            format: "media=cdrom",
            device: "ide-cd,bus=ide.0,unit=1,bootindex=2"
          }
        ],
        interfaces: [
          {
            id: "mgmt0",
            type: "MGMT",
            numTxQueues: 0,
            numRxQueues: 0,
            hardwareMac: "",
            mac: "",
            subnet: "",
            pciDevice: null,
            controlPorts: [
              22,
              8080
            ],
            checksum: false,
            gso: false,
            tso4: false,
            tso6: false,
            ecn: false,
            mergeRxBuffers: false,
            noMultiQueue: false
          },
          {
            id: "dp0",
            type: "DATA",
            numTxQueues: 0,
            numRxQueues: 0,
            hardwareMac: "",
            mac: "52:54:00:be:85:02",
            subnet: "",
            pciDevice: {
              slot: 4
            },
            controlPorts: [],
            checksum: false,
            gso: false,
            tso4: false,
            tso6: false,
            ecn: false,
            mergeRxBuffers: true,
            noMultiQueue: true
          },
          {
            id: "dp1",
            type: "DATA",
            numTxQueues: 0,
            numRxQueues: 0,
            hardwareMac: "",
            mac: "52:54:00:be:85:03",
            subnet: "",
            pciDevice: {
              slot: 5
            },
            controlPorts: [],
            checksum: false,
            gso: false,
            tso4: false,
            tso6: false,
            ecn: false,
            mergeRxBuffers: true,
            noMultiQueue: true
          }
        ],
        extraQemuCpuFlags: "",
        extraQemuFlags: "-rtc base=utc,driftfix=slew -boot strict=on",
        licenseMode: "BLOB",
        telnetConsole: "NO_CONSOLE",
        consolePciAddr: null,
        tmpDir: ""
      },
      execManifest: null,
      cpuCores: "2",
      memoryMb: "4096",
      cpuOversubscriptionFactor: "0"
    }
  },
  identifier: "a245add9-c5fc-4886-8132-d5f6f9b99d84",
  description: ""
};

export default manifest;
