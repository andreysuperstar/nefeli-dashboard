import { Services } from "../rest_client/pangolin/model/services";

const pipelinesMetadata: Services = {
  services: [
    {
      config: {
        identifier: "1",
        name: "ddp_pid2",
      },
    },
    {
      config: {
        identifier: "2",
        name: "customer0_pid4",
      },
      status: {
        hasDraft: true
      },
    },
  ],
};

export default pipelinesMetadata;
