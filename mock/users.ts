import { Users } from "../rest_client/pangolin/model/users";

const users: Users = {
  users: [
    {
      email: 'eric@nefeli.io',
      firstName: 'Eric',
      lastName: 'Carino',
      roles: {
        scope: 'tenant',
        id: '1',
        roles: ['admin']
      },
      username: 'ecarino'
    },
    {
      email: 'test@email.com',
      firstName: 'test',
      lastName: 'one',
      roles: {
        scope: 'tenant',
        id: '3',
        roles: ['user']
      },
      username: 'test'
    },
    {
      email: 'mock@nefeli.io',
      firstName: 'Mock',
      lastName: 'User',
      roles: {
        scope: 'system',
        id: 'mock',
        roles: ['admin']
      },
      username: 'mock'
    }
  ],
  metadata: {
    index: 0,
    total: 2
  }
};

export default users;
