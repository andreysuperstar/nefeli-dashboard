import { SoftwareProfiles } from '../rest_client/pangolin/model/softwareProfiles';
import files from './files';

const filesNumber = files.files.length;

const profiles: SoftwareProfiles = {
  softwareProfiles: [
    {
      identifier: '1',
      name: 'Andrew Profile',
      description: 'Andrew\'s profile',
      distroVersion: files.files[Math.floor(Math.random() * filesNumber)].name,
      notBefore: new Date(2030, 1, 2, 11, 24)
    },
    {
      identifier: '2',
      name: 'Eric Profile',
      description: 'Eric\'s profile',
      distroVersion: files.files[Math.floor(Math.random() * filesNumber)].name,
      notBefore: new Date(2035, 5, 1)
    },
    {
      identifier: '3',
      name: 'Anton Profile',
      description: 'Anton\'s profile',
      distroVersion: files.files[Math.floor(Math.random() * filesNumber)].name,
      notBefore: new Date(2077, 11, 15, 1, 21)
    }
  ]
};

export default profiles;
