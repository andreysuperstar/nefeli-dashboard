import { ServiceTemplate } from "../rest_client/pangolin/model/serviceTemplate";
import templates from "./templates";

const template: ServiceTemplate = templates.serviceTemplates[0];

export default template;
