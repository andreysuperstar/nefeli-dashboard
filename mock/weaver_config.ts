import { TunnelType } from '../rest_client/pangolin/model/tunnelType';
import { ClusterArchType } from '../rest_client/pangolin/model/clusterArchType';
import { ClusterConf } from '../rest_client/pangolin/model/clusterConf';

const config: ClusterConf = {
  id: '-1',
  name: '642b47c1-1238-4c55-bf95-00fb207be49a',
  type: 'weaver',
  clusterNetwork: {
    arch: ClusterArchType.RACK,
    rack: {
      world: TunnelType.VLAN,
      machine: TunnelType.GREINUDP,
      cluster: TunnelType.GREINUDP
    },
    premise: {}
  },
  fqdn: '10.10.10.66',
  ip: '10.10.10.66',
  weaverFqdn: '10.10.10.66',
  weaverIp: '10.10.10.66',
  storageMode: 's3'
}

export default config;
