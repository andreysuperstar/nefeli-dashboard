import { SlackHooks } from '../rest_client/pangolin/model/slackHooks';
import { AlertLevel } from '../rest_client/pangolin/model/alertLevel';

const slack: SlackHooks = {
  hooks: {
    hooks: [
      {
        name: 'My Slack Config',
        uri: 'https://slack.com/api',
        channel: 'MyAlarms',
        username: 'admin',
        levels: {
          value: [AlertLevel.INFO, AlertLevel.WARNING, AlertLevel.ERROR],
        },
        enabled: true,
        identifier: 'dc143517-757c-41f4-95a1-4e85e38ad832'
      }
    ]
  },
  metadata: {
    count: 1,
    index: 0
  }
};

export default slack;
