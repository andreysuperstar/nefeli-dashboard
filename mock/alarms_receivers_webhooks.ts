import { WebHooks } from '../rest_client/pangolin/model/webHooks';
import { AlertLevel } from '../rest_client/pangolin/model/alertLevel';

const webhook: WebHooks = {
  hooks: {
    hooks: [
      {
        name: '',
        uri: 'https://my.webhooks.com/api',
        levels: {
          value: [AlertLevel.INFO, AlertLevel.WARNING],
        },
        enabled: true,
        identifier: 'dc143517-757c-41f4-95a1-4e85e38ad832'
      },
      {
        name: '',
        uri: 'https://my.webhooks.com/api',
        levels: {
          value: [
            AlertLevel.INFO,
            AlertLevel.WARNING,
            AlertLevel.ERROR,
            AlertLevel.CRITICAL,
          ],
        },
        enabled: true,
        identifier: 'dc143517-757c-41f4-95a1-4e85e38ad832'
      }
    ]
  },
  metadata: {
    count: 2,
    index: 0
  }
};

export default webhook;
