import { LoggingConfig } from '../rest_client/pangolin/model/loggingConfig';
import { LoggingConfigStatus } from '../rest_client/pangolin/model/loggingConfigStatus';

const logging: LoggingConfig = {
  endpoint: {
    host: 'logging.server.com',
    password: 'mypass',
    user: 'admin'
  },
  status: LoggingConfigStatus.ENABLED
};

export default logging;
