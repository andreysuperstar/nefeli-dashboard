import { SiteConfigSiteStatus } from '../rest_client/pangolin/model/siteConfigSiteStatus';
import { SiteConfigSiteType } from '../rest_client/pangolin/model/siteConfigSiteType';
import { Site } from '../rest_client/pangolin/model/site';
import { Sites } from '../rest_client/pangolin/model/sites';
import profiles from './hardware_profiles';

const ericSite: Site = {
  config: {
    identifier: 'eric_site',
    name: 'Eric',
    publicIp: '127.0.0.1',
    privateIp: '127.0.0.5',
    siteStatus: SiteConfigSiteStatus.ACTIVE,
    dataplaneVlanTag: 0,
    gatewayMac: '00:00:00:00:00:01',
    mgmtUrl: 'https://fake.com:1234',
    siteType: SiteConfigSiteType.CLUSTER,
    vniRangeEnd: 1,
    vniRangeStart: 0,
    hardwareProfileId: '1',
    wans: [
      {
        bondId: 'bond_1'
      }
    ]
  },
  status: {
    lastHeardFromAt: new Date()
  }
};

ericSite.config.wans[0]['static'] = {
  dnsAddrs: ['1.1.1.1', '3.3.3.3'],
  gatewayAddr: '255.255.255.255',
  wanCidr: '112.23.78.1/32'
}

const mockLastHeardFromAt = new Date();

const hours = mockLastHeardFromAt.getHours() - 7;

mockLastHeardFromAt.setHours(hours);

const andrewSite: Site = {
  config: {
    identifier: 'andrew_site',
    name: 'Andrew',
    publicIp: '127.0.0.2',
    privateIp: '127.0.0.6',
    siteStatus: SiteConfigSiteStatus.STAGED,
    dataplaneVlanTag: 0,
    gatewayMac: '00:00:00:00:00:01',
    mgmtUrl: 'https://fake.com:1234',
    siteType: SiteConfigSiteType.UCPE,
    vniRangeEnd: 1,
    vniRangeStart: 0,
    hardwareProfileId: profiles.hardwareProfiles[0].identifier
  },
  status: {
    lastHeardFromAt: mockLastHeardFromAt
  }
};

const antonSite: Site = {
  config: {
    identifier: 'anton_site',
    name: 'Anton',
    publicIp: '127.0.0.3',
    privateIp: '127.0.0.7',
    siteStatus: SiteConfigSiteStatus.PROVISIONING,
    dataplaneVlanTag: 0,
    gatewayMac: '00:00:00:00:00:01',
    mgmtUrl: 'https://fake.com:1234',
    siteType: SiteConfigSiteType.CLUSTER,
    vniRangeEnd: 1,
    vniRangeStart: 0
  },
  status: {
    lastHeardFromAt: new Date(2021, 11, 21)
  }
};

const inactiveSite: Site = {
  config: {
    identifier: 'inactive_site',
    name: 'Inactive',
    publicIp: '127.0.0.4',
    privateIp: '127.0.0.8',
    siteStatus: SiteConfigSiteStatus.INACTIVE,
    dataplaneVlanTag: 0,
    gatewayMac: '00:00:00:00:00:01',
    mgmtUrl: 'https://fake.com:1234',
    siteType: SiteConfigSiteType.CLUSTER,
    vniRangeEnd: 1,
    vniRangeStart: 0
  }
};

const sites: Sites = {
  sites: [
    ericSite,
    andrewSite,
    antonSite,
    inactiveSite
  ],
  metadata: {
    index: 0,
    total: 4
  }
};

export default sites;
