import { TACPlusConfig } from '../rest_client/pangolin/model/tACPlusConfig';
import { TACPlusStatus } from '../rest_client/pangolin/model/tACPlusStatus';

const config: TACPlusConfig = {
  enabled: TACPlusStatus.ENABLED
}

export default config;
