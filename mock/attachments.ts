import { Attachments } from '../rest_client/pangolin/model/attachments';

const attachments: Attachments = {
  attachments: [
    {
      identifier: '1',
      name: 'attachment1',
      siteId: 'eric_site',
      description: 'First Attachment'
    },
    {
      identifier: '2',
      name: 'attachment2',
      siteId: 'eric_site',
      description: 'Second Attachment',
      encap: {
        ivid: 1,
        ovid: 2077
      }
    },
    {
      identifier: '3',
      name: 'attachment3',
      siteId: 'eric_site',
      description: 'Third Attachment',
      encap: {
        ivid: 0,
        ovid: 2035
      }
    }
  ]
}

export default attachments;
