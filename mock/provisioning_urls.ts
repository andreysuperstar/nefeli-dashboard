import { ProvisioningURLs } from '../rest_client/pangolin/model/provisioningURLs';

const urls: ProvisioningURLs = {
  provisioningUrls: [
    {
      identifier: '1',
      url: 'https://ztp.nefeli.io/'
    },
    {
      identifier: '2',
      url: 'https://provisioning.servers.com/'
    },
    {
      identifier: '3',
      url: 'https://192.168.5.100:2300/'
    }
  ]
};

export default urls;
