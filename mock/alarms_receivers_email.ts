import { EmailHooks } from '../rest_client/pangolin/model/emailHooks';

const alerts: EmailHooks = {
  hooks: {
    hooks: [
      {
        address: 'eric@nefeli.io',
        levels: {
          value: ['ERROR', 'CRITICAL']
        },
        name: 'E-Mail Notify',
        enabled: true,
        identifier: 'my-uuid'
      }
    ]
  },
  metadata: {
    count: 1,
    index: 0
  }
};

export default alerts;
