import { HardwareProfiles } from '../rest_client/pangolin/model/hardwareProfiles';
import { NicBandwidth } from '../rest_client/pangolin/model/nicBandwidth';
import { BondMode } from '../rest_client/pangolin/model/bondMode';
import { BondModeXorPolicy } from '../rest_client/pangolin/model/bondModeXorPolicy';
import { TunnelType } from '../rest_client/pangolin/model/tunnelType';
import { NicDriver } from '../rest_client/pangolin/model/nicDriver';
import { KernelMode } from '../rest_client/pangolin/model/kernelMode';
import { SubnetType } from '../rest_client/pangolin/model/subnetType';

const profiles: HardwareProfiles = {
  hardwareProfiles: [
    {
      identifier: '1',
      description: 'Profile 1 description',
      name: 'Profile 1',
      bonds: [
        {
          identifier: 'bond_1',
          bond: {
            bandwidth: NicBandwidth.GB1,
            bondMode: BondMode.TXLB,
            bondModeXorPolicy: BondModeXorPolicy.L2,
            connectivity: TunnelType.VLAN,
            devices: ['device 1', 'device 2', 'device 3'],
            driver: NicDriver.IGBUIO,
            kernel: KernelMode.BYPASS,
            macAddr: 'ac:de:48:00:11:22',
            purpose: SubnetType.WAN,
            socket: 'sock'
          }
        },
        {
          identifier: 'bond_2',
          bond: {
            bandwidth: NicBandwidth.GB1,
            bondMode: BondMode.TXLB,
            bondModeXorPolicy: BondModeXorPolicy.L2,
            connectivity: TunnelType.VLAN,
            devices: ['device 1', 'device 2', 'device 3'],
            driver: NicDriver.IGBUIO,
            kernel: KernelMode.BYPASS,
            macAddr: 'ac:de:48:00:11:23',
            purpose: SubnetType.LAN,
            socket: 'sock'
          }
        }
      ]
    },
    {
      identifier: '2',
      name: 'Profile 2',
      description: 'Profile 2 description',
      bonds: [
        {
          identifier: 'bond_2',
          bond: {
            bandwidth: NicBandwidth.GB1,
            bondMode: BondMode.TXLB,
            bondModeXorPolicy: BondModeXorPolicy.L2,
            connectivity: TunnelType.VLAN,
            devices: ['device 1', 'device 2', 'device 3'],
            driver: NicDriver.IGBUIO,
            kernel: KernelMode.BYPASS,
            macAddr: 'ac:de:48:00:11:22',
            purpose: SubnetType.GENERIC,
            socket: '1'
          }
        }, {
          identifier: 'bond_4',
          bond: {
            bandwidth: NicBandwidth.GB100,
            bondMode: BondMode.XOR,
            bondModeXorPolicy: BondModeXorPolicy.L2,
            connectivity: TunnelType.VLANQINQ,
            devices: ['device 4', 'device 5', 'device 6'],
            driver: NicDriver.VFIOPCI,
            kernel: KernelMode.SINGLEIP,
            macAddr: 'ac:de:48:00:44:33',
            purpose: SubnetType.LAN,
            socket: '2'
          }
        }
      ]
    },
    {
      identifier: '3',
      description: 'Profile 3 description',
      name: 'A very long profile name',
      bonds: [
        {
          identifier: 'bond_3',
          bond: {
            bandwidth: NicBandwidth.GB1,
            bondMode: BondMode.TXLB,
            bondModeXorPolicy: BondModeXorPolicy.L2,
            connectivity: TunnelType.VLAN,
            devices: ['device 1', 'device 2', 'device 3'],
            driver: NicDriver.IGBUIO,
            kernel: KernelMode.BYPASS,
            macAddr: 'ac:de:48:00:11:22',
            purpose: SubnetType.GENERIC,
            socket: 'sock'
          }
        }
      ]
    }
  ]
};

export default profiles;
