import { MnemeBFState } from '../rest_client/mneme/model/mnemeBFState';
import { MnemeFileInfos } from '../rest_client/mneme/model/mnemeFileInfos';

const files: MnemeFileInfos = {
  files: [
    {
      desc: 'Juniper vSRX Image',
      name: 'vsrx_img',
      bfi: {
        state: MnemeBFState.Deleting
      }
    },
    {
      desc: 'Arista Router Image',
      name: 'arista_img',
      bfi: {
        state: MnemeBFState.Ready
      }
    },
    {
      desc: 'PAN Firewall Image',
      name: 'pan_img',
      bfi: {
        state: MnemeBFState.Failed
      }
    },
    {
      name: 'distro-nefeli_1.7.0_all.deb',
      desc: 'uploaded distro',
      bfi: {
        state: MnemeBFState.Ready,
      },
      isDistroDeb: true
    }
  ]
};

export default files;
