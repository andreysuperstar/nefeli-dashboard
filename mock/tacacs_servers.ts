import { AAAServer } from '../rest_client/pangolin/model/aAAServer';
import { AAAServers } from '../rest_client/pangolin/model/aAAServers';

const serverList: AAAServers = {
  aaaServers: [{
    identifier: '1',
    address: 'https://ztp.nefeli.io/'
  }, {
    identifier: '2',
    address: 'https://provisioning.servers.com/'
  }, {
    identifier: '3',
    address: 'https://192.168.5.100:2300/'
  }]
};

export default serverList;
