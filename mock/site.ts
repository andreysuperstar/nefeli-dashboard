import { Site } from '../rest_client/pangolin/model/site';
import { SiteConfigSiteStatus } from '../rest_client/pangolin/model/siteConfigSiteStatus';
import { SiteConfigSiteType } from '../rest_client/pangolin/model/siteConfigSiteType';

const site: Site = {
  config: {
    dataplaneVlanTag: 3,
    name: 'Nefeli Site',
    identifier: 'nefeli-site',
    publicIp: '127.0.0.1',
    privateIp: '127.0.0.2',
    gatewayMac: '00:00:00:00:00:01',
    mgmtUrl: 'https://127.0.0.1',
    siteStatus: SiteConfigSiteStatus.ACTIVE,
    siteType: SiteConfigSiteType.CLUSTER,
    vniRangeEnd: 1,
    vniRangeStart: 100
  }
};

export default site;

export const getSiteById = (id: string): Site => {
  const friendlyNames = {
    eric_site: 'Eric\'s Site',
    andrew_site: 'Andrew\'s Site',
    anton_site: 'Anton\'s Site',
    inactive_site: 'Inactive Site'
  };
  const siteStatuses = {
    eric_site: 'ACTIVE',
    andrew_site: 'ACTIVE',
    anton_site: 'ACTIVE',
    inactive_site: 'INACTIVE'

  };
  const siteIps = {
    eric_site: '127.0.0.1',
    andrew_site: '127.0.0.2',
    anton_site: '127.0.0.3',
    inactive_site: '127.0.0.100'
  };

  return {
    config: {
      identifier: id,
      dataplaneVlanTag: 0,
      name: friendlyNames[id],
      publicIp: siteIps[id],
      privateIp: siteIps[id],
      gatewayMac: '00:00:00:00:00:01',
      mgmtUrl: 'https://fake.com:1234',
      siteStatus: siteStatuses[id],
      siteType: SiteConfigSiteType.CLUSTER,
      vniRangeEnd: 1,
      vniRangeStart: 0
    }
  };
};
