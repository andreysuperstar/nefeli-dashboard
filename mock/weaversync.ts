import { WeaverSync } from '../rest_client/pangolin/model/weaverSync';
import { WeaverSyncConfigWeaver } from '../rest_client/pangolin/model/weaverSyncConfigWeaver';
import { WeaverSyncConfig } from '../rest_client/pangolin/model/weaverSyncConfig';
import { WeaverSyncConfigMode } from '../rest_client/pangolin/model/weaverSyncConfigMode';
import { WeaverSyncStatus } from '../rest_client/pangolin/model/weaverSyncStatus';
import { WeaverSyncStatusState } from '../rest_client/pangolin/model/weaverSyncStatusState';

const weavers: WeaverSyncConfigWeaver[] = [
  {
    id: '-1',
    endpoint: 'https://weaver-active.nefeli.io',
    mode: WeaverSyncConfigMode.ACTIVE
  },
  {
    id: '-2',
    endpoint: 'https://weaver-standby.nefeli.io',
    mode: WeaverSyncConfigMode.STANDBY
  }
];

const config: WeaverSyncConfig = {
  beganAt: new Date(),
  term: 0,
  weavers
};

const status: WeaverSyncStatus = {
  state: WeaverSyncStatusState.ACTIVE,
  configSyncStatus: {
    '1609692900': {
      lastAttemptedAt: new Date(),
      lastSucceededAt: undefined,
      error: 'Unexpected failure'
    }
  },
  etcdSyncStatus: {
    '1609692900': {
      lastAttemptedAt: new Date(),
      lastSucceededAt: new Date()
    }
  },
  fileSyncStatus: {
    '1609692900': {
      lastAttemptedAt: new Date(),
      lastSucceededAt: new Date()
    }
  },
  vaultSyncStatus: {
    '1609692900': {
      lastAttemptedAt: new Date(),
      lastSucceededAt: new Date()
    }
  }
};

const weaversync: WeaverSync = { config, status };

export default weaversync;
