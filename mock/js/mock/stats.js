"use strict";
exports.__esModule = true;
exports.postClusterStatsFilter = exports.postTenantStatsFilter = exports.getClusterServices = exports.postSystemStats = exports.postTenantStats = exports.postClusterStats = void 0;
// TODO(ecarino): enum & interfaces were copied, need to figure out an elegant way to reference from project
var STATUS;
(function (STATUS) {
    STATUS["UNKNOWN"] = "unknown";
    STATUS["CRITICAL"] = "critical";
    STATUS["WARN"] = "warn";
    STATUS["GOOD"] = "good";
})(STATUS || (STATUS = {}));
var mockServices = ['Palo Alto', 'Load Balancing', 'Routing', 'Nefeli'];
var getStatsPoints = function (startTime, seconds, startPoint) {
    if (!seconds) {
        return [];
    }
    var stats = new Array();
    var previousPoint = startPoint;
    var step = 1;
    var variance = 500;
    for (var i = seconds; i >= 0; i--) {
        if ((i % step) === 0) {
            var plusOrMinus = Math.random() < 0.5 ? -1 : 1;
            var p = (Math.floor(Math.random() * 50) + (previousPoint + (plusOrMinus * variance)));
            previousPoint = p;
            stats.push([
                startTime - i,
                p.toString()
            ]);
        }
    }
    return stats;
};
var getStatsFilter = function (filter, totalStats, duration) {
    if (duration === void 0) { duration = 300; }
    var seconds = duration;
    var startTime = new Date().getTime() / 1000;
    if (filter === 'throughput') {
        totalStats[filter + 'In'] = getStatsPoints(startTime, seconds, 50000);
        totalStats[filter + 'Out'] = getStatsPoints(startTime, seconds, 40000);
        totalStats['throughputServiceOut'] = [
            {
                id: '4',
                name: 'c0_pid4',
                site: 'eric_site',
                stats: getStatsPoints(startTime, seconds, 22000)
            },
            {
                id: '5',
                name: 'ddp_pid2',
                site: 'anton_site',
                stats: getStatsPoints(startTime, seconds, 17000)
            }
        ];
        totalStats['throughputServiceIn'] = [
            {
                id: '4',
                name: 'c0_pid4',
                site: 'eric_site',
                stats: getStatsPoints(startTime, seconds, 25000)
            },
            {
                id: '5',
                name: 'ddp_pid2',
                site: 'anton_site',
                stats: getStatsPoints(startTime, seconds, 18000)
            }
        ];
    }
    else {
        totalStats[filter] = [
            {
                name: mockServices[0],
                site: 'eric_site',
                stats: getStatsPoints(startTime, seconds, 50000)
            }, {
                name: mockServices[1],
                site: 'anton_site',
                stats: getStatsPoints(startTime, seconds, 50000)
            }, {
                name: mockServices[2],
                site: 'andrew_site',
                stats: getStatsPoints(startTime, seconds, 50000)
            }, {
                name: mockServices[3],
                site: 'eric_site',
                stats: getStatsPoints(startTime, seconds, 50000)
            }
        ];
    }
    return totalStats;
};
var postClusterStats = function () {
    var stats = {
        id: -1,
        services: 2,
        coresUsed: 2,
        coresTotal: 8,
        memUsed: 77,
        memTotal: 100,
        throughputOut: 678.0563143983617,
        throughputIn: 6482,
        capacity: 1,
        packetLoss: [],
        latency: [],
        throughputServiceOut: [],
        throughputServiceIn: []
    };
    return stats;
};
exports.postClusterStats = postClusterStats;
var postTenantStats = function () {
    var stats = new Array();
    stats.push({
        id: '-1',
        name: '',
        services: 2,
        throughputOut: 6382,
        throughputIn: 6482,
        status: STATUS.GOOD,
        packetLoss: [
            {
                name: 'Unclassified',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '356.6'
                    ]
                ]
            },
            {
                name: 'customer0_pid4',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '21400.6'
                    ]
                ]
            },
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '0'
                    ]
                ]
            }
        ],
        latency: [
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154093,
                        '0'
                    ]
                ]
            }
        ]
    }, {
        id: '-2',
        name: '',
        services: 4,
        throughputOut: 6382,
        throughputIn: 6482,
        status: STATUS.WARN,
        packetLoss: [
            {
                name: 'Unclassified',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '356.6'
                    ]
                ]
            },
            {
                name: 'customer0_pid4',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '21400.6'
                    ]
                ]
            },
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '0'
                    ]
                ]
            }
        ],
        latency: [
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154093,
                        '0'
                    ]
                ]
            }
        ]
    }, {
        id: '-3',
        name: '',
        services: 8,
        throughputOut: 5000,
        throughputIn: 5100,
        status: STATUS.CRITICAL,
        packetLoss: [
            {
                name: 'Unclassified',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '356.6'
                    ]
                ]
            },
            {
                name: 'customer0_pid4',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '21400.6'
                    ]
                ]
            },
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '0'
                    ]
                ]
            }
        ],
        latency: [
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154093,
                        '0'
                    ]
                ]
            }
        ]
    }, {
        id: '-4',
        name: '',
        services: 1,
        throughputOut: 6382,
        throughputIn: 6482,
        status: STATUS.GOOD,
        packetLoss: [
            {
                name: 'Unclassified',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '356.6'
                    ]
                ]
            },
            {
                name: 'customer0_pid4',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '21400.6'
                    ]
                ]
            },
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154025,
                        '0'
                    ]
                ]
            }
        ],
        latency: [
            {
                name: 'ddp_pid2',
                site: 'Site 1',
                stats: [
                    [
                        1531154093,
                        '0'
                    ]
                ]
            }
        ]
    });
    return stats[Math.floor(Math.random() * stats.length)];
};
exports.postTenantStats = postTenantStats;
var postSystemStats = function () { return ({
    servicesTotal: 46,
    clusterSites: {
        total: 325,
        active: 322
    },
    ucpeSites: {
        total: 193,
        active: 191
    }
}); };
exports.postSystemStats = postSystemStats;
var getClusterServices = function () {
    return mockServices.map(function (service) { return ({
        weaverService: service,
        expService: service,
        weaverTenant: service,
        expTenant: service
    }); });
};
exports.getClusterServices = getClusterServices;
var postTenantStatsFilter = function (filter) {
    var totalStats = exports.postTenantStats();
    return getStatsFilter(filter, totalStats);
};
exports.postTenantStatsFilter = postTenantStatsFilter;
var postClusterStatsFilter = function (filter, duration) {
    var totalStats = exports.postClusterStats();
    return getStatsFilter(filter, totalStats, duration);
};
exports.postClusterStatsFilter = postClusterStatsFilter;
