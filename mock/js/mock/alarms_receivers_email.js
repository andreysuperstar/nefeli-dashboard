"use strict";
exports.__esModule = true;
var alerts = {
    hooks: {
        hooks: [
            {
                address: 'eric@nefeli.io',
                levels: {
                    value: ['ERROR', 'CRITICAL']
                },
                name: 'E-Mail Notify',
                enabled: true,
                identifier: 'my-uuid'
            }
        ]
    },
    metadata: {
        count: 1,
        index: 0
    }
};
exports["default"] = alerts;
