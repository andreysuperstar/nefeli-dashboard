"use strict";
exports.__esModule = true;
var alertLevel_1 = require("../rest_client/pangolin/model/alertLevel");
var slack = {
    hooks: {
        hooks: [
            {
                name: 'My Slack Config',
                uri: 'https://slack.com/api',
                channel: 'MyAlarms',
                username: 'admin',
                levels: {
                    value: [alertLevel_1.AlertLevel.INFO, alertLevel_1.AlertLevel.WARNING, alertLevel_1.AlertLevel.ERROR]
                },
                enabled: true,
                identifier: 'dc143517-757c-41f4-95a1-4e85e38ad832'
            }
        ]
    },
    metadata: {
        count: 1,
        index: 0
    }
};
exports["default"] = slack;
