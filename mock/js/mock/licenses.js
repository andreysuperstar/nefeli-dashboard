"use strict";
exports.__esModule = true;
var licenseState_1 = require("../rest_client/pangolin/model/licenseState");
var license = {
    licenses: [{
            license: {
                config: {
                    identifier: 'license-id1',
                    nfId: 'nf-catalog-id1',
                    dateAdded: '1559266898',
                    startDate: '1559263898',
                    endDate: '1559269898',
                    name: 'Arista Router License',
                    description: 'description for this license',
                    data: ''
                },
                status: {
                    state: licenseState_1.LicenseState.AVAILABLE,
                    rootPool: 'Default Root Pool',
                    parentPool: 'Tenant Pool'
                }
            }
        }]
};
exports["default"] = license;
