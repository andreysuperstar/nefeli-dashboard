"use strict";
exports.__esModule = true;
var alertLevel_1 = require("../rest_client/pangolin/model/alertLevel");
var webhook = {
    hooks: {
        hooks: [
            {
                name: '',
                uri: 'https://my.webhooks.com/api',
                levels: {
                    value: [alertLevel_1.AlertLevel.INFO, alertLevel_1.AlertLevel.WARNING]
                },
                enabled: true,
                identifier: 'dc143517-757c-41f4-95a1-4e85e38ad832'
            },
            {
                name: '',
                uri: 'https://my.webhooks.com/api',
                levels: {
                    value: [
                        alertLevel_1.AlertLevel.INFO,
                        alertLevel_1.AlertLevel.WARNING,
                        alertLevel_1.AlertLevel.ERROR,
                        alertLevel_1.AlertLevel.CRITICAL,
                    ]
                },
                enabled: true,
                identifier: 'dc143517-757c-41f4-95a1-4e85e38ad832'
            }
        ]
    },
    metadata: {
        count: 2,
        index: 0
    }
};
exports["default"] = webhook;
