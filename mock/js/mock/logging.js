"use strict";
exports.__esModule = true;
var loggingConfigStatus_1 = require("../rest_client/pangolin/model/loggingConfigStatus");
var logging = {
    endpoint: {
        host: 'logging.server.com',
        password: 'mypass',
        user: 'admin'
    },
    status: loggingConfigStatus_1.LoggingConfigStatus.ENABLED
};
exports["default"] = logging;
