"use strict";
exports.__esModule = true;
var weaverSyncConfigMode_1 = require("../rest_client/pangolin/model/weaverSyncConfigMode");
var weaverSyncStatusState_1 = require("../rest_client/pangolin/model/weaverSyncStatusState");
var weavers = [
    {
        id: '-1',
        endpoint: 'https://weaver-active.nefeli.io',
        mode: weaverSyncConfigMode_1.WeaverSyncConfigMode.ACTIVE
    },
    {
        id: '-2',
        endpoint: 'https://weaver-standby.nefeli.io',
        mode: weaverSyncConfigMode_1.WeaverSyncConfigMode.STANDBY
    }
];
var config = {
    beganAt: new Date(),
    term: 0,
    weavers: weavers
};
var status = {
    state: weaverSyncStatusState_1.WeaverSyncStatusState.ACTIVE,
    configSyncStatus: {
        '1609692900': {
            lastAttemptedAt: new Date(),
            lastSucceededAt: undefined,
            error: 'Unexpected failure'
        }
    },
    etcdSyncStatus: {
        '1609692900': {
            lastAttemptedAt: new Date(),
            lastSucceededAt: new Date()
        }
    },
    fileSyncStatus: {
        '1609692900': {
            lastAttemptedAt: new Date(),
            lastSucceededAt: new Date()
        }
    },
    vaultSyncStatus: {
        '1609692900': {
            lastAttemptedAt: new Date(),
            lastSucceededAt: new Date()
        }
    }
};
var weaversync = { config: config, status: status };
exports["default"] = weaversync;
