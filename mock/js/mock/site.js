"use strict";
exports.__esModule = true;
exports.getSiteById = void 0;
var siteConfigSiteStatus_1 = require("../rest_client/pangolin/model/siteConfigSiteStatus");
var siteConfigSiteType_1 = require("../rest_client/pangolin/model/siteConfigSiteType");
var site = {
    config: {
        dataplaneVlanTag: 3,
        name: 'Nefeli Site',
        identifier: 'nefeli-site',
        publicIp: '127.0.0.1',
        privateIp: '127.0.0.2',
        gatewayMac: '00:00:00:00:00:01',
        mgmtUrl: 'https://127.0.0.1',
        siteStatus: siteConfigSiteStatus_1.SiteConfigSiteStatus.ACTIVE,
        siteType: siteConfigSiteType_1.SiteConfigSiteType.CLUSTER,
        vniRangeEnd: 1,
        vniRangeStart: 100
    }
};
exports["default"] = site;
var getSiteById = function (id) {
    var friendlyNames = {
        eric_site: 'Eric\'s Site',
        andrew_site: 'Andrew\'s Site',
        anton_site: 'Anton\'s Site',
        inactive_site: 'Inactive Site'
    };
    var siteStatuses = {
        eric_site: 'ACTIVE',
        andrew_site: 'ACTIVE',
        anton_site: 'ACTIVE',
        inactive_site: 'INACTIVE'
    };
    var siteIps = {
        eric_site: '127.0.0.1',
        andrew_site: '127.0.0.2',
        anton_site: '127.0.0.3',
        inactive_site: '127.0.0.100'
    };
    return {
        config: {
            identifier: id,
            dataplaneVlanTag: 0,
            name: friendlyNames[id],
            publicIp: siteIps[id],
            privateIp: siteIps[id],
            gatewayMac: '00:00:00:00:00:01',
            mgmtUrl: 'https://fake.com:1234',
            siteStatus: siteStatuses[id],
            siteType: siteConfigSiteType_1.SiteConfigSiteType.CLUSTER,
            vniRangeEnd: 1,
            vniRangeStart: 0
        }
    };
};
exports.getSiteById = getSiteById;
