"use strict";
exports.__esModule = true;
var tunnelType_1 = require("../rest_client/pangolin/model/tunnelType");
var clusterArchType_1 = require("../rest_client/pangolin/model/clusterArchType");
var config = {
    id: '-1',
    name: '642b47c1-1238-4c55-bf95-00fb207be49a',
    type: 'weaver',
    clusterNetwork: {
        arch: clusterArchType_1.ClusterArchType.RACK,
        rack: {
            world: tunnelType_1.TunnelType.VLAN,
            machine: tunnelType_1.TunnelType.GREINUDP,
            cluster: tunnelType_1.TunnelType.GREINUDP
        },
        premise: {}
    },
    fqdn: '10.10.10.66',
    ip: '10.10.10.66',
    weaverFqdn: '10.10.10.66',
    weaverIp: '10.10.10.66',
    storageMode: 's3'
};
exports["default"] = config;
