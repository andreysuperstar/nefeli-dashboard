"use strict";
exports.__esModule = true;
var instanceStatusState_1 = require("../rest_client/pangolin/model/instanceStatusState");
var serviceChain = {
    config: {
        nodes: {
            portX: {
                port: {
                    tunnel: "2797da16-b367-44de-be2f-3c5ecc51bf90"
                },
                site: "eric_site",
                name: "portX",
                identifier: "portX"
            },
            portY: {
                port: {
                    tunnel: "eb672753-f1fd-44e0-8703-378af599ad0c"
                },
                site: "eric_site",
                name: "portY",
                identifier: "portY"
            },
            vsrxA: {
                nf: {
                    catalogId: "a245add9-c5fc-4886-8132-d5f6f9b99d84",
                    disableScaling: false,
                    initialInstances: 1,
                    minInstances: 1,
                    maxInstances: 0,
                    overprovisionInstances: 0
                },
                site: "eric_site",
                name: "vsrxA",
                identifier: "vsrxA"
            }
        },
        edges: [
            {
                a: {
                    node: "portX",
                    interface: ""
                },
                b: {
                    node: "vsrxA",
                    interface: "dp0"
                },
                filterAb: {
                    bpf: ""
                },
                filterBa: {
                    bpf: ""
                }
            },
            {
                a: {
                    node: "portY",
                    interface: ""
                },
                b: {
                    node: "vsrxA",
                    interface: "dp1"
                },
                filterAb: {
                    bpf: ""
                },
                filterBa: {
                    bpf: ""
                }
            }
        ],
        maxRateKpps: "100000",
        identifier: "62bced4b-2ae1-4371-b696-283a0b3042a1",
        name: "vsrx",
        tenantId: "955e7869-2982-4257-aa94-b3a4a75d3620",
        description: ""
    },
    status: {
        nodes: {
            vsrxA: {
                instances: {
                    "c35a7da0-6d56-4e83-a69e-441ac1a63b5c": {
                        location: {
                            site: "eric_site",
                            placement: {
                                machineId: "fritz",
                                bandwidthBySocket: {
                                    0: "12112000"
                                },
                                draining: false,
                                hugepagesBySocket: {
                                    0: {
                                        hugepages: {
                                            1073741824: 4
                                        }
                                    }
                                },
                                coresBySocket: {
                                    0: {
                                        cores: [
                                            4,
                                            5
                                        ]
                                    }
                                },
                                license: {
                                    config: {
                                        identifier: "dfa5973b-62ee-424a-93f8-7f4073621c40",
                                        nfId: "a245add9-c5fc-4886-8132-d5f6f9b99d84",
                                        data: "SlVOT1M1NzAxMzk0MTUgYWVhcWljIGFnYWozNCBtMmdoNnEgZnJlbWp2IGd5MnRpbSB6dGdheXQNCiAgICAgICAgICAgICAgIGFtYnFmdSAzZm93bHEga2lnYmt0IHNmaXpjdSB5c2phanogY3ZpdjJwDQogICAgICAgICAgICAgICBramZ2Z2wgYmFqZmhlIGdscWhmYSAyd2xqbjQgNGptbjRzIDVobjZwZw0KICAgICAgICAgICAgICAgY3N5eGpwIHA1NXI2biA0Z2xwYm8gYWJ0bHlrIHdwYmJnNSBhc2xva3cNCiAgICAgICAgICAgICAgIGNrZGZqNCBhYml5bmwgaQ0KDQo=",
                                        dateAdded: "2021-10-08T06:37:57-07:00",
                                        startDate: "",
                                        endDate: "",
                                        name: "",
                                        description: ""
                                    },
                                    status: {
                                        state: "AVAILABLE",
                                        user: null,
                                        rootPool: "",
                                        parentPool: ""
                                    }
                                },
                                cpuOversubscriptionFactor: "0"
                            }
                        },
                        metadata: null,
                        state: instanceStatusState_1.InstanceStatusState.CONFIGURED,
                        failedBy: "",
                        launchSpec: {
                            assignedMachine: "fritz",
                            assignedCores: [
                                4,
                                5
                            ],
                            assignedNumaNode: 0,
                            assignedHugepages: {
                                1073741824: 4
                            },
                            instanceId: "92",
                            license: {
                                config: {
                                    identifier: "dfa5973b-62ee-424a-93f8-7f4073621c40",
                                    nfId: "a245add9-c5fc-4886-8132-d5f6f9b99d84",
                                    data: "SlVOT1M1NzAxMzk0MTUgYWVhcWljIGFnYWozNCBtMmdoNnEgZnJlbWp2IGd5MnRpbSB6dGdheXQNCiAgICAgICAgICAgICAgIGFtYnFmdSAzZm93bHEga2lnYmt0IHNmaXpjdSB5c2phanogY3ZpdjJwDQogICAgICAgICAgICAgICBramZ2Z2wgYmFqZmhlIGdscWhmYSAyd2xqbjQgNGptbjRzIDVobjZwZw0KICAgICAgICAgICAgICAgY3N5eGpwIHA1NXI2biA0Z2xwYm8gYWJ0bHlrIHdwYmJnNSBhc2xva3cNCiAgICAgICAgICAgICAgIGNrZGZqNCBhYml5bmwgaQ0KDQo=",
                                    dateAdded: "2021-10-08T06:37:57-07:00",
                                    startDate: "",
                                    endDate: "",
                                    name: "",
                                    description: ""
                                },
                                status: {
                                    state: "AVAILABLE",
                                    user: null,
                                    rootPool: "",
                                    parentPool: ""
                                }
                            },
                            logicalNf: "user_a245add9-c5fc-4886-8132-d5f6f9b99d84_vsrxA",
                            pipeline: "_36a113a4f3-82fe-41d2-84a4-4f9590967d4962bced4b-2ae1-4371-b696-283a0b3042a1",
                            portSockets: {
                                dp0: "/tmp/eth_vhost_2",
                                dp1: "/tmp/eth_vhost_3"
                            },
                            type: "THIRD_PARTY",
                            rxqSize: 0,
                            txqSize: 0,
                            component: {
                                name: "vsrx",
                                vendor: "Juniper Networks",
                                type: "CONTROLLER",
                                vmManifest: {
                                    images: [
                                        {
                                            image: {
                                                url: "file:///opt/nefeli/images/juniper/junos-vsrx3-x86-64-19.4R1.10.qcow2",
                                                checksum: "",
                                                checksumAlg: "NO_CHECKSUM"
                                            },
                                            format: "",
                                            device: "ide-hd,bus=ide.0,unit=0,bootindex=1"
                                        },
                                        {
                                            image: {
                                                url: "file:///opt/nefeli/images/juniper/vSRX3-init.iso",
                                                checksum: "",
                                                checksumAlg: "NO_CHECKSUM"
                                            },
                                            format: "media=cdrom",
                                            device: "ide-cd,bus=ide.0,unit=1,bootindex=2"
                                        }
                                    ],
                                    interfaces: [
                                        {
                                            id: "mgmt0",
                                            type: "MGMT",
                                            numTxQueues: 0,
                                            numRxQueues: 0,
                                            hardwareMac: "",
                                            mac: "",
                                            subnet: "",
                                            pciDevice: null,
                                            controlPorts: [
                                                22,
                                                8080
                                            ],
                                            checksum: false,
                                            gso: false,
                                            tso4: false,
                                            tso6: false,
                                            ecn: false,
                                            mergeRxBuffers: false,
                                            noMultiQueue: false
                                        },
                                        {
                                            id: "dp0",
                                            type: "DATA",
                                            numTxQueues: 0,
                                            numRxQueues: 0,
                                            hardwareMac: "",
                                            mac: "52:54:00:be:85:02",
                                            subnet: "",
                                            pciDevice: {
                                                slot: 4
                                            },
                                            controlPorts: [],
                                            checksum: false,
                                            gso: false,
                                            tso4: false,
                                            tso6: false,
                                            ecn: false,
                                            mergeRxBuffers: true,
                                            noMultiQueue: true
                                        },
                                        {
                                            id: "dp1",
                                            type: "DATA",
                                            numTxQueues: 0,
                                            numRxQueues: 0,
                                            hardwareMac: "",
                                            mac: "52:54:00:be:85:03",
                                            subnet: "",
                                            pciDevice: {
                                                slot: 5
                                            },
                                            controlPorts: [],
                                            checksum: false,
                                            gso: false,
                                            tso4: false,
                                            tso6: false,
                                            ecn: false,
                                            mergeRxBuffers: true,
                                            noMultiQueue: true
                                        }
                                    ],
                                    extraQemuCpuFlags: "",
                                    extraQemuFlags: "-rtc base=utc,driftfix=slew -boot strict=on",
                                    licenseMode: "BLOB",
                                    telnetConsole: "NO_CONSOLE",
                                    consolePciAddr: null,
                                    tmpDir: ""
                                },
                                execManifest: null,
                                cpuCores: "2",
                                memoryMb: "4096",
                                cpuOversubscriptionFactor: "0"
                            },
                            instanceName: "c35a7da0-6d56-4e83-a69e-441ac1a63b5c",
                            tenantId: "955e7869-2982-4257-aa94-b3a4a75d3620",
                            weaverName: "vsrx"
                        },
                        systemdUnit: "nefrun-vsrx-_36a113a4f3-82fe-41d2-84a4-4f9590967d4962bced4b-2ae1-4371-b696-283a0b3042a1_c35a7da0-6d56-4e83-a69e-441ac1a63b5c",
                        controlPorts: {
                            22: "fritz:45891",
                            8080: "fritz:41463"
                        },
                        telnetConsole: "",
                        placementStatus: {
                            blockedOnMachineResources: false,
                            blockedOnLicense: false,
                            errorMachineResources: "",
                            errorLicense: "",
                            cpuMachineCandidates: [],
                            hugepagesMachineCandidates: [],
                            bandwidthMachineCandidates: [],
                            blockedOnCpuResourceLimit: false
                        },
                        launchStatus: {
                            ok: true,
                            blockedOnMemory: false,
                            blockedOnFiles: false,
                            blockedOnHypervisor: false,
                            hypervisorStatus: "",
                            blockingFiles: []
                        },
                        nfcStatus: {
                            inProgress: false,
                            success: true,
                            version: 1,
                            progressMessage: "",
                            errorMessage: "",
                            shortErrorMessage: "",
                            configPolicyVersion: 1
                        },
                        identity: {
                            pipeline: "_36a113a4f3-82fe-41d2-84a4-4f9590967d4962bced4b-2ae1-4371-b696-283a0b3042a1",
                            logicalNf: "user_a245add9-c5fc-4886-8132-d5f6f9b99d84_vsrxA",
                            instanceId: "c35a7da0-6d56-4e83-a69e-441ac1a63b5c",
                            nfClass: "a245add9-c5fc-4886-8132-d5f6f9b99d84",
                            tenantId: "955e7869-2982-4257-aa94-b3a4a75d3620",
                            weaverName: "vsrx"
                        }
                    }
                },
                state: "NORMAL",
                frozenSince: null,
                configuration: {
                    configTemplate: "\u003cconfig\u003e\n    \u003cconfiguration\u003e\n            \u003csystem\u003e\n                \u003croot-authentication\u003e\n                    \u003cencrypted-password\u003e$6$vIMOH9za$KkR2D6gatVedSBDMrhXUTtMRhS0948taEv/KAvogn.QnK5nNy0aFOHUM6itM15NHBRnMVI6UdEhPVXJMZVk6l1\u003c/encrypted-password\u003e\n                \u003c/root-authentication\u003e\n                \u003clogin\u003e\n                    \u003cuser\u003e\n                        \u003cname\u003enefeli\u003c/name\u003e\n                        \u003cuid\u003e2000\u003c/uid\u003e\n                        \u003cclass\u003esuper-user\u003c/class\u003e\n                        \u003cauthentication\u003e\n                            \u003cencrypted-password\u003e$6$Q1tMS.XI$62PDE4dTv9TSWaUjTOCxvgpAm91CmiTLYq.wc6bDLys42WahQLpRIwJXiTMaJWYlWVC32Zk.kid3VZawEGriW/\u003c/encrypted-password\u003e\n                            \u003cssh-ecdsa\u003e\n                                \u003cname\u003eecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBAhf9QrR78p5D1hl1iHPqjpK0iAdUjX6FSngAd48FMu1iAkg/nJeVELm8S3jswYwf0m/Bk0+/GTkOV7iiPygoH4= nfproxy\u003c/name\u003e\n                            \u003c/ssh-ecdsa\u003e\n                        \u003c/authentication\u003e\n                    \u003c/user\u003e\n                \u003c/login\u003e\n                \u003cservices\u003e\n                    \u003cssh\u003e\n                    \u003c/ssh\u003e\n                    \u003cnetconf\u003e\n                        \u003cssh\u003e\n                        \u003c/ssh\u003e\n                    \u003c/netconf\u003e\n                    \u003cweb-management\u003e\n                        \u003chttp\u003e\n                            \u003cinterface\u003efxp0.0\u003c/interface\u003e\n                        \u003c/http\u003e\n                    \u003c/web-management\u003e\n                \u003c/services\u003e\n                \u003clicense\u003e\n                    \u003ckeys\u003e\n                        \u003ckey\u003e\n                            \u003cname\u003eJUNOS570139415 aeaqicagaj34m2gh6qfremjvgy2timztgaytambqfu3fowlqkigbktsfizcuysjajzcviv2pkjfvglbajfheglqhfa2wljn44jmn4s5hn6pgcsyxjpp55r6n4glpboabtlykwpbbg5aslokwckdfj4abiynli\u003c/name\u003e\n                        \u003c/key\u003e\n                    \u003c/keys\u003e\n                \u003c/license\u003e\n            \u003c/system\u003e\n            \u003csecurity\u003e\n                \u003cscreen\u003e\n                    \u003cids-option\u003e\n                        \u003cname\u003euntrust-screen\u003c/name\u003e\n                        \u003cicmp\u003e\n                            \u003cping-death/\u003e\n                        \u003c/icmp\u003e\n                        \u003cip\u003e\n                            \u003csource-route-option/\u003e\n                            \u003ctear-drop/\u003e\n                        \u003c/ip\u003e\n                        \u003ctcp\u003e\n                            \u003csyn-flood\u003e\n                                \u003calarm-threshold\u003e1024\u003c/alarm-threshold\u003e\n                                \u003cattack-threshold\u003e200\u003c/attack-threshold\u003e\n                                \u003csource-threshold\u003e1024\u003c/source-threshold\u003e\n                                \u003cdestination-threshold\u003e2048\u003c/destination-threshold\u003e\n                                \u003cundocumented\u003e\u003cqueue-size\u003e2000\u003c/queue-size\u003e\u003c/undocumented\u003e\n                                \u003ctimeout\u003e20\u003c/timeout\u003e\n                            \u003c/syn-flood\u003e\n                            \u003cland/\u003e\n                        \u003c/tcp\u003e\n                    \u003c/ids-option\u003e\n                \u003c/screen\u003e\n                \u003caddress-book\u003e\n                    \u003cname\u003eglobal\u003c/name\u003e\n                    \u003caddress\u003e\n                        \u003cname\u003esource-net\u003c/name\u003e\n                        \u003cip-prefix\u003e172.16.16.0/24\u003c/ip-prefix\u003e\n                    \u003c/address\u003e\n                    \u003caddress\u003e\n                        \u003cname\u003edest-net\u003c/name\u003e\n                        \u003cip-prefix\u003e10.0.16.0/24\u003c/ip-prefix\u003e\n                    \u003c/address\u003e\n                    \u003caddress\u003e\n                        \u003cname\u003edest-allowed\u003c/name\u003e\n                        \u003cip-prefix\u003e10.0.16.0/24\u003c/ip-prefix\u003e\n                    \u003c/address\u003e\n                \u003c/address-book\u003e\n                \u003cpolicies\u003e\n                    \u003cpolicy\u003e\n                        \u003cfrom-zone-name\u003etrust\u003c/from-zone-name\u003e\n                        \u003cto-zone-name\u003etrust\u003c/to-zone-name\u003e\n                        \u003cpolicy\u003e\n                            \u003cname\u003edefault-permit\u003c/name\u003e\n                            \u003cmatch\u003e\n                                \u003csource-address\u003eany\u003c/source-address\u003e\n                                \u003cdestination-address\u003eany\u003c/destination-address\u003e\n                                \u003capplication\u003eany\u003c/application\u003e\n                            \u003c/match\u003e\n                            \u003cthen\u003e\n                                \u003cpermit\u003e\n                                \u003c/permit\u003e\n                            \u003c/then\u003e\n                        \u003c/policy\u003e\n                    \u003c/policy\u003e\n                    \u003cpolicy\u003e\n                        \u003cfrom-zone-name\u003etrust\u003c/from-zone-name\u003e\n                        \u003cto-zone-name\u003euntrust\u003c/to-zone-name\u003e\n                        \u003cpolicy\u003e\n                            \u003cname\u003edefault-permit\u003c/name\u003e\n                            \u003cmatch\u003e\n                                \u003csource-address\u003eany\u003c/source-address\u003e\n                                \u003cdestination-address\u003eany\u003c/destination-address\u003e\n                                \u003capplication\u003eany\u003c/application\u003e\n                            \u003c/match\u003e\n                            \u003cthen\u003e\n                                \u003cpermit\u003e\n                                \u003c/permit\u003e\n                            \u003c/then\u003e\n                        \u003c/policy\u003e\n                        \u003cpolicy\u003e\n                            \u003cname\u003eallow-half\u003c/name\u003e\n                            \u003cmatch\u003e\n                                \u003csource-address\u003esource-net\u003c/source-address\u003e\n                                \u003cdestination-address\u003edest-allowed\u003c/destination-address\u003e\n                                \u003capplication\u003eany\u003c/application\u003e\n                            \u003c/match\u003e\n                            \u003cthen\u003e\n                                \u003cpermit\u003e\n                                \u003c/permit\u003e\n                            \u003c/then\u003e\n                        \u003c/policy\u003e\n                    \u003c/policy\u003e\n                \u003c/policies\u003e\n                \u003czones\u003e\n                    \u003csecurity-zone\u003e\n                        \u003cname\u003etrust\u003c/name\u003e\n                        \u003ctcp-rst/\u003e\n                        \u003cinterfaces\u003e\n                            \u003cname\u003ege-0/0/0.0\u003c/name\u003e\n                        \u003c/interfaces\u003e\n                    \u003c/security-zone\u003e\n                    \u003csecurity-zone\u003e\n                        \u003cname\u003euntrust\u003c/name\u003e\n                        \u003cscreen\u003euntrust-screen\u003c/screen\u003e\n                        \u003cinterfaces\u003e\n                            \u003cname\u003ege-0/0/1.0\u003c/name\u003e\n                        \u003c/interfaces\u003e\n                    \u003c/security-zone\u003e\n                \u003c/zones\u003e\n            \u003c/security\u003e\n            \u003cinterfaces\u003e\n                \u003cinterface\u003e\n                    \u003cname\u003ege-0/0/0\u003c/name\u003e\n                    \u003cunit\u003e\n                        \u003cname\u003e0\u003c/name\u003e\n                        \u003cfamily\u003e\n                            \u003cinet\u003e\n                                \u003caddress\u003e\n                                    \u003cname\u003e172.16.16.20/24\u003c/name\u003e\n                                \u003c/address\u003e\n                            \u003c/inet\u003e\n                        \u003c/family\u003e\n                    \u003c/unit\u003e\n                \u003c/interface\u003e\n                \u003cinterface\u003e\n                    \u003cname\u003ege-0/0/1\u003c/name\u003e\n                    \u003cunit\u003e\n                        \u003cname\u003e0\u003c/name\u003e\n                        \u003cfamily\u003e\n                            \u003cinet\u003e\n                                \u003caddress\u003e\n                                    \u003cname\u003e10.0.16.20/24\u003c/name\u003e\n                                \u003c/address\u003e\n                            \u003c/inet\u003e\n                        \u003c/family\u003e\n                    \u003c/unit\u003e\n                \u003c/interface\u003e\n                \u003cinterface\u003e\n                    \u003cname\u003efxp0\u003c/name\u003e\n                    \u003cunit\u003e\n                        \u003cname\u003e0\u003c/name\u003e\n                        \u003cfamily\u003e\n                            \u003cinet\u003e\n                                \u003cdhcp\u003e\n                                \u003c/dhcp\u003e\n                            \u003c/inet\u003e\n                        \u003c/family\u003e\n                    \u003c/unit\u003e\n                \u003c/interface\u003e\n            \u003c/interfaces\u003e\n            \u003crouting-options\u003e\n                \u003cstatic\u003e\n                    \u003croute\u003e\n                        \u003cname\u003e0.0.0.0/0\u003c/name\u003e\n                        \u003cnext-hop\u003e10.0.16.11\u003c/next-hop\u003e\n                    \u003c/route\u003e\n                \u003c/static\u003e\n            \u003c/routing-options\u003e\n    \u003c/configuration\u003e\n\u003c/config\u003e\n",
                    mapping: "",
                    filter: "#!/usr/bin/env python3\n\"\"\"\nfilter.py\n\"\"\"\n#\n# Copyright (C) 2017-2018 Nefeli Networks Inc. - All Rights Reserved\n# Proprietary and confidential.\n#\n# Notice: All information contained within is, and remains, the\n# property of Nefeli Networks Inc.  and its suppliers or authorized\n# agents, if any.  The intellectual and technical concepts contained\n# herein are proprietary to Nefeli Networks Inc. and may be covered by\n# U.S. and Foreign Patents, patents in process, and are protected by\n# trade secret or copyright law.  Redistribution of this information\n# or reproduction of this material is strictly forbidden unless prior\n# written permission is obtained from Nefeli Networks Inc.\n#\n# For information regarding licensing, contact info@nefeli.io\n#\n\n# pylint: disable=invalid-name\n\nimport argparse\nimport importlib\nimport os.path\nimport re\nimport sys\n\n\ndef main():\n    \"\"\"\n    Filter config file\n    cat config | filter.py --mapping mapping.py --total 3 --instance 0\n    \"\"\"\n\n    parser = argparse.ArgumentParser()\n    parser.add_argument(\n        \"--mapping\", required=True, help=\"File containing template susbstitute values\"\n    )\n    parser.add_argument(\"--total\", required=True, help=\"Total number of NF instance\")\n    parser.add_argument(\n        \"--instance\", required=True, help=\"Instance for which config is being generated\"\n    )\n    args = parser.parse_args()\n\n    # The mapping file is an absolute  path name.\n    # Import requires basename without .py extension\n\n    mapping = importlib.import_module(  # pylint: disable=possibly-unused-variable\n        os.path.basename(args.mapping)[:-3]\n    )\n    total = int(args.total) - 1  # pylint: disable=possibly-unused-variable\n    instance = int(args.instance)  # pylint: disable=possibly-unused-variable\n\n    prog = re.compile(\"%.*%\")\n    gs = globals()\n    ls = locals()\n\n    def replace(matchobj):\n        variable = matchobj.group(0)[1:-1]\n        command = \"mapping.{}[total][instance]\".format(variable)\n        variable = eval(command, gs, ls)  # pylint: disable=eval-used\n        return variable\n\n    for line in sys.stdin:\n        m = prog.sub(replace, line)\n        if m:\n            line = m\n        print(line, end=\"\")\n\n    sys.exit(0)\n\n\nif __name__ == \"__main__\":\n    main()\n",
                    rollback: "{\n    \"Rollback\: {\n\t\"_comment\: \"On config failure just signal failure to pangolin\",\n\t\"Policy\: \"reboot\"\n    }\n}\n"
                }
            }
        },
        requestedVersion: "0",
        currentVersion: "0",
        currentSiteVersions: {
            eric_site: "0"
        },
        previousSiteVersions: {
            eric_site: "0"
        },
        state: "NORMAL",
        hasDraft: false
    }
};
exports["default"] = serviceChain;
