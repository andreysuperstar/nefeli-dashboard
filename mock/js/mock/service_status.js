"use strict";
exports.__esModule = true;
exports.getTrace = void 0;
var instanceStatusState_1 = require("../rest_client/pangolin/model/instanceStatusState");
var status = {
    currentSiteVersions: {},
    currentVersion: 'new-version',
    nodes: {
        filter: {
            instances: {
                filter_1: {
                    identity: {
                        instanceId: 'filter_1'
                    },
                    location: {
                        site: 'eric-exp',
                        placement: {
                            machineId: 'sylvester',
                            bandwidthBySocket: {
                                '0': '12112000'
                            },
                            draining: false,
                            hugepagesBySocket: {
                                '0': {
                                    hugepages: { '1234': 3 }
                                }
                            },
                            coresBySocket: {
                                '0': {
                                    cores: [2]
                                }
                            },
                            license: null
                        }
                    },
                    metadata: null,
                    state: instanceStatusState_1.InstanceStatusState.CONFIGURED,
                    launchSpec: null,
                    systemdUnit: '',
                    controlPorts: {},
                    telnetConsole: '',
                    placementStatus: {
                        blockedOnMachineResources: false,
                        blockedOnLicense: false
                    },
                    launchStatus: {
                        blockedOnFiles: false,
                        blockedOnHypervisor: false,
                        blockedOnMemory: false
                    }
                },
                filter_2: {
                    identity: {
                        instanceId: 'filter_2'
                    },
                    location: {
                        site: 'eric-exp',
                        placement: {
                            machineId: 'fluffy',
                            bandwidthBySocket: {
                                '0': '12112000'
                            },
                            draining: false,
                            hugepagesBySocket: {},
                            coresBySocket: {
                                '0': { cores: [2] }
                            },
                            license: null
                        }
                    },
                    metadata: null,
                    state: instanceStatusState_1.InstanceStatusState.FAILED,
                    launchSpec: null,
                    systemdUnit: '',
                    controlPorts: {},
                    telnetConsole: '',
                    placementStatus: {
                        blockedOnMachineResources: false,
                        blockedOnLicense: false
                    },
                    launchStatus: {
                        blockedOnFiles: false,
                        blockedOnHypervisor: false,
                        blockedOnMemory: false
                    }
                },
                filter_3: {
                    identity: {
                        instanceId: 'filter_3'
                    },
                    location: {
                        site: 'eric-exp',
                        placement: {
                            machineId: 'flock',
                            bandwidthBySocket: {
                                '0': '12112000'
                            },
                            draining: false,
                            hugepagesBySocket: {},
                            coresBySocket: {
                                '0': { cores: [2] }
                            },
                            license: null
                        }
                    },
                    metadata: null,
                    state: instanceStatusState_1.InstanceStatusState.FAILED,
                    launchSpec: null,
                    systemdUnit: '',
                    controlPorts: {},
                    telnetConsole: '',
                    placementStatus: {
                        blockedOnMachineResources: false,
                        blockedOnLicense: false
                    },
                    launchStatus: {
                        blockedOnFiles: false,
                        blockedOnHypervisor: false,
                        blockedOnMemory: false
                    }
                }
            }
        }
    },
    previousSiteVersions: {},
    requestedVersion: 'requested-version'
};
exports["default"] = status;
var getTrace = function (value) {
    return {
        trace: {
            bpf: {
                value: value
            }
        }
    };
};
exports.getTrace = getTrace;
