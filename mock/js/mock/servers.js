"use strict";
/* tslint:disable: no-magic-numbers */
exports.__esModule = true;
var subnetType_1 = require("../rest_client/pangolin/model/subnetType");
var servers = {
    servers: [
        {
            identifier: 'sylvester',
            status: 'GOOD',
            name: 'sylvester',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ],
                    interfaces: [
                        {
                            name: 'wan-port',
                            purpose: subnetType_1.SubnetType.WAN
                        },
                        {
                            name: 'lan-port',
                            purpose: subnetType_1.SubnetType.LAN
                        }
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            nfs: {
                nfInstances: [
                    {
                        metadata: {
                            logicalNf: 'filter',
                            name: 'filter_0',
                            machine: 'sylvester',
                            service: '1',
                            tenant: 'Tenant 1',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                0: {
                                    cores: [
                                        2, 3, 4, 5
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                0: '12112000'
                            },
                            hugepagesBySocket: {
                                0: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                },
                                1: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                }
                            }
                        }
                    },
                    {
                        metadata: {
                            logicalNf: 'filter',
                            name: 'filter_1',
                            machine: 'sylvester',
                            service: '1',
                            tenant: 'Tenant 1',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                1: {
                                    cores: [
                                        8, 9, 10
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                1: '12112000'
                            },
                            hugepagesBySocket: {
                                0: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                },
                                1: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                }
                            }
                        }
                    },
                    {
                        metadata: {
                            logicalNf: 'nat',
                            name: 'nat_0',
                            machine: 'sylvester',
                            service: '2',
                            tenant: 'Tenant 2',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                0: {
                                    cores: [
                                        6,
                                        7
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                0: '12112000'
                            },
                            hugepagesBySocket: {
                                0: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                },
                                1: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                }
                            }
                        }
                    },
                    {
                        metadata: {
                            logicalNf: 'nat',
                            name: 'nat_1',
                            machine: 'sylvester',
                            service: '2',
                            tenant: 'Tenant 2',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                1: {
                                    cores: [
                                        14
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                0: '12112000'
                            },
                            hugepagesBySocket: {
                                0: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                },
                                1: {
                                    hugepages: {
                                        '1234': 3
                                    }
                                }
                            }
                        }
                    }
                ]
            },
            control: {
                numaResources: {
                    0: {
                        cores: [
                            13
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'felix',
            status: 'WARNING',
            name: 'felix',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            control: {
                numaResources: {
                    0: {
                        cores: [
                            27
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0,
                            13,
                            14,
                            15,
                            16,
                            17,
                            18
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'fluffy',
            status: 'CRITICAL',
            name: 'fluffy',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            control: {
                numaResources: {
                    0: {
                        cores: [
                            13
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0,
                            12
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'server5',
            status: 'GOOD',
            name: 'server5',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            'size': '1073741824',
                            'count': 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            nfs: {
                nfInstances: [
                    {
                        metadata: {
                            logicalNf: 'filter',
                            name: 'filter_0',
                            machine: 'vidar',
                            service: '1',
                            tenant: 'Tenant 1',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                '0': {
                                    'cores': [
                                        2,
                                        3,
                                        4,
                                        5
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                '0': '12112000'
                            },
                            hugepagesBySocket: {}
                        }
                    },
                    {
                        metadata: {
                            logicalNf: 'filter',
                            name: 'filter_1',
                            machine: 'vidar',
                            service: '1',
                            tenant: 'Tenant 1',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                '1': {
                                    cores: [
                                        8,
                                        9,
                                        10
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                '1': '12112000'
                            },
                            hugepagesBySocket: {}
                        }
                    },
                    {
                        metadata: {
                            logicalNf: 'nat',
                            name: 'nat_0',
                            machine: 'vidar',
                            service: '2',
                            tenant: 'Tenant 2',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                '0': {
                                    cores: [
                                        6,
                                        7
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                '0': '12112000'
                            },
                            hugepagesBySocket: {}
                        }
                    },
                    {
                        metadata: {
                            logicalNf: 'nat',
                            name: 'nat_1',
                            machine: 'vidar',
                            service: '2',
                            tenant: 'Tenant 2',
                            nfType: '',
                            draining: false
                        },
                        resources: {
                            coresBySocket: {
                                '1': {
                                    cores: [
                                        14
                                    ]
                                }
                            },
                            bandwidthBySocket: {
                                '0': '12112000'
                            },
                            hugepagesBySocket: {}
                        }
                    }
                ]
            },
            control: {
                numaResources: {
                    '0': {
                        cores: [
                            13
                        ],
                        hugepages: {
                            '1073741824': 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    '0': {
                        cores: [
                            0
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'flock',
            status: 'GOOD',
            name: 'flock',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            control: {
                numaResources: {
                    0: {
                        cores: [
                            13
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0,
                            12
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'server6',
            status: 'WARNING',
            name: 'server6',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            control: {
                numaResources: {
                    0: {
                        cores: [
                            13
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0,
                            12
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'server7',
            status: 'WARNING',
            name: 'server7',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            control: {
                numaResources: {
                    0: {
                        cores: [
                            13
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0,
                            12
                        ],
                        hugepages: {}
                    }
                }
            }
        },
        {
            identifier: 'server8',
            status: 'WARNING',
            name: 'server8',
            sockets: [
                {
                    id: 0,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        0,
                        1,
                        2,
                        3,
                        4,
                        5,
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13,
                        14,
                        15
                    ]
                },
                {
                    id: 1,
                    memory: '33612103680',
                    hugepages: [
                        {
                            size: '1073741824',
                            count: 20
                        }
                    ],
                    cores: [
                        16,
                        17,
                        18,
                        19,
                        20,
                        21,
                        22,
                        23,
                        24,
                        25,
                        26,
                        27,
                        28,
                        29,
                        30,
                        31
                    ]
                }
            ],
            control: {
                numaResources: {
                    0: {
                        cores: [
                            13
                        ],
                        hugepages: {
                            1073741824: 1
                        }
                    }
                }
            },
            bess: {
                numaResources: {
                    0: {
                        cores: [
                            0,
                            12
                        ],
                        hugepages: {}
                    }
                }
            }
        }
    ]
};
exports["default"] = servers;
