"use strict";
exports.__esModule = true;
var pipelinesMetadata = {
    services: [
        {
            config: {
                identifier: "1",
                name: "ddp_pid2"
            }
        },
        {
            config: {
                identifier: "2",
                name: "customer0_pid4"
            },
            status: {
                hasDraft: true
            }
        },
    ]
};
exports["default"] = pipelinesMetadata;
