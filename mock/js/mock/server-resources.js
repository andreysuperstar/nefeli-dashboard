"use strict";
exports.__esModule = true;
var resources = {
    control: {
        numaResources: {
            key1: {
                cores: [
                    0
                ],
                hugepages: {
                    1073741824: 4
                }
            },
            key2: {
                cores: [
                    1
                ],
                hugepages: {
                    1073741824: 4
                }
            }
        }
    },
    bess: {
        numaResources: {
            bess_key1: {
                cores: [
                    0
                ],
                hugepages: {
                    1073741824: 4
                }
            },
            bess_key2: {
                cores: [
                    1
                ],
                hugepages: {
                    1073741824: 4
                }
            }
        }
    },
    vnf: [
        {
            service: 'Arista',
            instance: 'arista_0',
            logicalNf: 'Router',
            resources: {},
            draining: false,
            tenant: 'Stumptown Coffee',
            tenantId: '123456789'
        },
        {
            service: 'Juniper',
            instance: 'vsrx_0',
            logicalNf: 'VSRX',
            resources: {
                numaResources: {
                    key1: {
                        cores: [
                            4, 5
                        ],
                        hugepages: {
                            1073741824: 4
                        }
                    },
                    key2: {
                        cores: [
                            6, 7
                        ],
                        hugepages: {
                            1073741824: 4
                        }
                    }
                }
            },
            draining: false,
            tenant: 'Bed, Bath & Beyond',
            tenantId: '987654321'
        }
    ]
};
exports["default"] = resources;
