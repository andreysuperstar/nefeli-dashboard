"use strict";
exports.__esModule = true;
var siteConfigSiteStatus_1 = require("../rest_client/pangolin/model/siteConfigSiteStatus");
var siteConfigSiteType_1 = require("../rest_client/pangolin/model/siteConfigSiteType");
var hardware_profiles_1 = require("./hardware_profiles");
var ericSite = {
    config: {
        identifier: 'eric_site',
        name: 'Eric',
        publicIp: '127.0.0.1',
        privateIp: '127.0.0.5',
        siteStatus: siteConfigSiteStatus_1.SiteConfigSiteStatus.ACTIVE,
        dataplaneVlanTag: 0,
        gatewayMac: '00:00:00:00:00:01',
        mgmtUrl: 'https://fake.com:1234',
        siteType: siteConfigSiteType_1.SiteConfigSiteType.CLUSTER,
        vniRangeEnd: 1,
        vniRangeStart: 0,
        hardwareProfileId: '1',
        wans: [
            {
                bondId: 'bond_1'
            }
        ]
    },
    status: {
        lastHeardFromAt: new Date()
    }
};
ericSite.config.wans[0]['static'] = {
    dnsAddrs: ['1.1.1.1', '3.3.3.3'],
    gatewayAddr: '255.255.255.255',
    wanCidr: '112.23.78.1/32'
};
var mockLastHeardFromAt = new Date();
var hours = mockLastHeardFromAt.getHours() - 7;
mockLastHeardFromAt.setHours(hours);
var andrewSite = {
    config: {
        identifier: 'andrew_site',
        name: 'Andrew',
        publicIp: '127.0.0.2',
        privateIp: '127.0.0.6',
        siteStatus: siteConfigSiteStatus_1.SiteConfigSiteStatus.STAGED,
        dataplaneVlanTag: 0,
        gatewayMac: '00:00:00:00:00:01',
        mgmtUrl: 'https://fake.com:1234',
        siteType: siteConfigSiteType_1.SiteConfigSiteType.UCPE,
        vniRangeEnd: 1,
        vniRangeStart: 0,
        hardwareProfileId: hardware_profiles_1["default"].hardwareProfiles[0].identifier
    },
    status: {
        lastHeardFromAt: mockLastHeardFromAt
    }
};
var antonSite = {
    config: {
        identifier: 'anton_site',
        name: 'Anton',
        publicIp: '127.0.0.3',
        privateIp: '127.0.0.7',
        siteStatus: siteConfigSiteStatus_1.SiteConfigSiteStatus.PROVISIONING,
        dataplaneVlanTag: 0,
        gatewayMac: '00:00:00:00:00:01',
        mgmtUrl: 'https://fake.com:1234',
        siteType: siteConfigSiteType_1.SiteConfigSiteType.CLUSTER,
        vniRangeEnd: 1,
        vniRangeStart: 0
    },
    status: {
        lastHeardFromAt: new Date(2021, 11, 21)
    }
};
var inactiveSite = {
    config: {
        identifier: 'inactive_site',
        name: 'Inactive',
        publicIp: '127.0.0.4',
        privateIp: '127.0.0.8',
        siteStatus: siteConfigSiteStatus_1.SiteConfigSiteStatus.INACTIVE,
        dataplaneVlanTag: 0,
        gatewayMac: '00:00:00:00:00:01',
        mgmtUrl: 'https://fake.com:1234',
        siteType: siteConfigSiteType_1.SiteConfigSiteType.CLUSTER,
        vniRangeEnd: 1,
        vniRangeStart: 0
    }
};
var sites = {
    sites: [
        ericSite,
        andrewSite,
        antonSite,
        inactiveSite
    ],
    metadata: {
        index: 0,
        total: 4
    }
};
exports["default"] = sites;
