"use strict";
exports.__esModule = true;
var nfc = {
    configTemplate: JSON.stringify({
        jsonrpc: '2.0',
        method: 'runCmds',
        params: {
            version: 1,
            cmds: [
                {
                    cmd: 'enable',
                    input: 'admin'
                },
                'configure',
                'interface Ethernet1',
                'no switchport',
                'ip address %INTERFACE1_IP%',
                'interface Ethernet2',
                'no switchport',
                'ip address %INTERFACE2_IP%',
                'spanning-tree mode none',
                'ip routing',
                'management api netconf',
                'transport ssh def'
            ],
            format: 'json'
        },
        id: 'nefeli'
    }),
    filter: 'filter program',
    mapping: 'mapping data',
    rollback: ''
};
exports["default"] = nfc;
