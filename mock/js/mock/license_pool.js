"use strict";
exports.__esModule = true;
var licenseState_1 = require("../rest_client/pangolin/model/licenseState");
var pool = {
    licensePools: [
        {
            identifier: '25310885-c544-4646-8cb8-4d8b46f9b223',
            name: 'Pool #1',
            licenses: {
                'Name-XY-10-Z': {
                    config: {
                        identifier: '1',
                        nfId: 'Firewall (PAN)',
                        dateAdded: '6/6/2019',
                        startDate: '6/6/2019',
                        endDate: '8/6/2019',
                        name: 'Name-XY-12-Z',
                        description: '',
                        data: ''
                    },
                    status: {
                        state: licenseState_1.LicenseState.ALLOCATEDTOSITE,
                        rootPool: 'pool-id1',
                        parentPool: 'pool-id1'
                    }
                },
                'Name-XY-12-Z': {
                    config: {
                        identifier: '2',
                        nfId: 'Load Balancer (F5)',
                        dateAdded: '1/1/2019',
                        startDate: '1/1/2019',
                        endDate: '6/5/2019',
                        name: 'Name-XY-10-Z',
                        description: '',
                        data: ''
                    },
                    status: {
                        state: licenseState_1.LicenseState.INUSE,
                        rootPool: 'pool-id1',
                        parentPool: 'pool-id1',
                        user: {
                            instanceId: '123e4567-e89b-12d3-a456-426614174000',
                            logicalNf: 'vsrxA',
                            nfClass: 'vsrx',
                            pipeline: 'vsrx3'
                        }
                    }
                }
            },
            licenseOwners: {}
        },
        {
            identifier: '4d43a4ff-73cc-49fe-9c9a-0eb823230121',
            name: 'Pool #2',
            licenses: {
                'Name-XY-10-Z': {
                    config: {
                        identifier: '1',
                        nfId: 'Firewall (PAN)',
                        dateAdded: '6/6/2019',
                        startDate: '6/6/2019',
                        endDate: '8/6/2019',
                        name: 'Name-XY-12-Z',
                        description: '',
                        data: ''
                    },
                    status: {
                        state: licenseState_1.LicenseState.RECLAIMING,
                        rootPool: 'pool-id1',
                        parentPool: 'pool-id1'
                    }
                }
            },
            licenseOwners: {}
        },
        {
            identifier: 'e825723a-1ef5-48af-985b-0bf4571f8b00',
            name: 'Pool #3',
            licenses: {
                'Name-XY-10-Z': {
                    config: {
                        identifier: '1',
                        nfId: 'Firewall (PAN)',
                        dateAdded: '6/6/2019',
                        startDate: '6/6/2019',
                        endDate: '8/6/2019',
                        name: 'Name-XY-12-Z',
                        description: '',
                        data: ''
                    },
                    status: {
                        state: licenseState_1.LicenseState.AVAILABLE,
                        rootPool: 'pool-id1',
                        parentPool: 'pool-id1'
                    }
                }
            },
            licenseOwners: {}
        },
        {
            identifier: 'f399c431-6c87-462b-ba2e-eaa70d905e22',
            name: 'Pool #4',
            licenses: {
                'Name-XY-10-Z': {
                    config: {
                        identifier: '1',
                        nfId: 'Firewall (PAN)',
                        dateAdded: '6/6/2019',
                        startDate: '6/6/2019',
                        endDate: '8/6/2019',
                        name: 'Name-XY-12-Z',
                        description: '',
                        data: ''
                    },
                    status: {
                        state: licenseState_1.LicenseState.AVAILABLE,
                        rootPool: 'pool-id1',
                        parentPool: 'pool-id1'
                    }
                }
            },
            licenseOwners: {}
        }
    ]
};
exports["default"] = pool;
