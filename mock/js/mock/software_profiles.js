"use strict";
exports.__esModule = true;
var files_1 = require("./files");
var filesNumber = files_1["default"].files.length;
var profiles = {
    softwareProfiles: [
        {
            identifier: '1',
            name: 'Andrew Profile',
            description: 'Andrew\'s profile',
            distroVersion: files_1["default"].files[Math.floor(Math.random() * filesNumber)].name,
            notBefore: new Date(2030, 1, 2, 11, 24)
        },
        {
            identifier: '2',
            name: 'Eric Profile',
            description: 'Eric\'s profile',
            distroVersion: files_1["default"].files[Math.floor(Math.random() * filesNumber)].name,
            notBefore: new Date(2035, 5, 1)
        },
        {
            identifier: '3',
            name: 'Anton Profile',
            description: 'Anton\'s profile',
            distroVersion: files_1["default"].files[Math.floor(Math.random() * filesNumber)].name,
            notBefore: new Date(2077, 11, 15, 1, 21)
        }
    ]
};
exports["default"] = profiles;
