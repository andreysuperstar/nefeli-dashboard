"use strict";
exports.__esModule = true;
var tunnels = {
    tunnels: [
        {
            config: {
                identifier: '1',
                name: 'Left Tunnel',
                siteId: 'eric_site',
                tenantId: '1',
                remoteTunnel: {
                    physicalPort: '1'
                },
                attachmentId: '2'
            },
            status: {
                usedBy: '1'
            }
        },
        {
            config: {
                identifier: '2',
                name: 'Unpaired Local Tunnel',
                siteId: 'eric_site',
                tenantId: '2',
                localTunnel: {},
                attachmentId: '3'
            },
            status: {
                usedBy: '2'
            }
        },
        {
            config: {
                identifier: '3',
                name: 'Tunnel 3',
                siteId: 'Site 3',
                tenantId: '3',
                remoteTunnel: {
                    encap: {
                        vxlan: {
                            vni: 1,
                            vtep: {
                                ipAddr: '1.1.1.1',
                                mac: '00-00-00-00-00-00',
                                udpPort: 1024
                            }
                        }
                    }
                }
            },
            status: {
                usedBy: '3'
            }
        },
        {
            config: {
                identifier: '123',
                name: 'My Local Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                localTunnel: {
                    peerTunnelId: '321'
                },
                attachmentId: '1'
            }
        },
        {
            config: {
                identifier: '4',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '4'
            }
        },
        {
            config: {
                identifier: '5',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '5'
            }
        },
        {
            config: {
                identifier: '6',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '6'
            }
        },
        {
            config: {
                identifier: '7',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '7'
            }
        },
        {
            config: {
                identifier: '8',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '8'
            }
        },
        {
            config: {
                identifier: '9',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '9'
            }
        },
        {
            config: {
                identifier: '10',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '10'
            }
        },
        {
            config: {
                identifier: '11',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '11'
            }
        },
        {
            config: {
                identifier: '12',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '12'
            }
        },
        {
            config: {
                identifier: '13',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '13'
            }
        },
        {
            config: {
                identifier: '14',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '14'
            }
        },
        {
            config: {
                identifier: '15',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '15'
            }
        },
        {
            config: {
                identifier: '16',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '16'
            }
        },
        {
            config: {
                identifier: '17',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '17'
            }
        },
        {
            config: {
                identifier: '18',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '18'
            }
        },
        {
            config: {
                identifier: '19',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '19'
            }
        },
        {
            config: {
                identifier: '20',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '20'
            }
        },
        {
            config: {
                identifier: '21',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '21'
            }
        },
        {
            config: {
                identifier: '22',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '22'
            }
        },
        {
            config: {
                identifier: '321',
                name: 'My Peer Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                localTunnel: {
                    peerTunnelId: '123'
                },
                attachmentId: '1'
            }
        },
        {
            config: {
                identifier: '23',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '23'
            }
        },
        {
            config: {
                identifier: '24',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '24'
            }
        },
        {
            config: {
                identifier: '25',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '25'
            }
        },
        {
            config: {
                identifier: '26',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '26'
            }
        },
        {
            config: {
                identifier: '27',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '27'
            }
        },
        {
            config: {
                identifier: '28',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '28'
            }
        },
        {
            config: {
                identifier: '29',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '29'
            }
        },
        {
            config: {
                identifier: '30',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '30'
            }
        },
        {
            config: {
                identifier: '31',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '31'
            }
        },
        {
            config: {
                identifier: '32',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '32'
            }
        },
        {
            config: {
                identifier: '33',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '33'
            }
        },
        {
            config: {
                identifier: '34',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '34'
            }
        },
        {
            config: {
                identifier: '35',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '35'
            }
        },
        {
            config: {
                identifier: '36',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '36'
            }
        },
        {
            config: {
                identifier: '37',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '37'
            }
        },
        {
            config: {
                identifier: '38',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '38'
            }
        },
        {
            config: {
                identifier: '39',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            },
            status: {
                usedBy: '39'
            }
        },
        {
            config: {
                identifier: '2797da16-b367-44de-be2f-3c5ecc51bf90',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        ivid: 1,
                        ovid: 2035
                    }
                },
                attachmentId: '1'
            }
        },
        {
            config: {
                identifier: 'eb672753-f1fd-44e0-8703-378af599ad0c',
                name: 'Nefeli Networks Tunnel',
                siteId: 'Site 4',
                tenantId: '4',
                remoteTunnel: {
                    encap: {
                        vxlan: {
                            vni: 123,
                            vtep: {
                                ipAddr: '74.125.21.106',
                                mac: 'ac:de:48:00:11:22',
                                udpPort: 4789
                            }
                        }
                    }
                },
                attachmentId: '1'
            }
        }
    ]
};
exports["default"] = tunnels;
