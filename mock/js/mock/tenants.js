"use strict";
exports.__esModule = true;
exports.getTenantSites = void 0;
var tenants = {
    tenants: [
        {
            identifier: '1',
            name: 'Tenant 1'
        },
        {
            identifier: '2',
            name: 'Tenant 2'
        },
        {
            identifier: '3',
            name: 'Tenant 3'
        },
        {
            identifier: '4',
            name: 'Nefeli Networks'
        }
    ],
    metadata: {
        index: 0,
        total: 3
    }
};
exports["default"] = tenants;
var getTenantSites = function () {
    return {
        sites: ['eric_site', 'andrew_site', 'anton_site', 'inactive_site']
    };
};
exports.getTenantSites = getTenantSites;
