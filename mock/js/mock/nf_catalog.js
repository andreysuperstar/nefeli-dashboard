"use strict";
exports.__esModule = true;
var componentType_1 = require("../rest_client/pangolin/model/componentType");
var nFType_1 = require("../rest_client/pangolin/model/nFType");
var nfs = {
    descriptors: [
        {
            name: 'legacy_arista',
            type: nFType_1.NFType.THIRDPARTY,
            controllerType: 'NF_CONTROLLER',
            scalingOptions: {
                supportsScaling: false,
                maxKppsPerInstance: '1000',
                useCpu: false
            },
            metadataOptions: {
                wrapper: 'IP_OPTION'
            },
            components: {
                datapath: {
                    name: 'arista2',
                    vendor: 'Arista Networks',
                    type: componentType_1.ComponentType.CONTROLLER,
                    vmManifest: {
                        images: [
                            {
                                image: {
                                    url: 'file:///opt/nefeli/images/arista/EOS-4.20.1FX-Virtual-Router.qcow2',
                                    checksum: '',
                                    checksumAlg: 'NO_CHECKSUM'
                                },
                                format: '',
                                device: 'ide-hd,bus=ide.0,unit=0,bootindex=2'
                            },
                            {
                                image: {
                                    url: 'file:///opt/nefeli/images/arista/Aboot-veos-8.0.0.iso',
                                    checksum: '',
                                    checksumAlg: 'NO_CHECKSUM'
                                },
                                format: 'media=cdrom',
                                device: 'ide-cd,bus=ide.0,unit=1,bootindex=1'
                            }
                        ],
                        interfaces: [
                            {
                                id: 'mgmt0',
                                type: 'MGMT',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '',
                                pciDevice: null,
                                controlPorts: [
                                    22,
                                    80,
                                    437,
                                    443
                                ],
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp0',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '00:16:3d:22:33:57',
                                pciDevice: null,
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp1',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '00:16:3d:22:33:58',
                                pciDevice: null,
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            }
                        ],
                        extraQemuCpuFlags: 'level=9',
                        extraQemuFlags: '-uuid f7dea811-4456-44eb-a6ec-11fe6412194b',
                        licenseMode: 'NO_LICENSE',
                        telnetConsole: 'NO_CONSOLE',
                        consolePciAddr: null
                    },
                    execManifest: null,
                    cpuCores: '4',
                    memoryMb: '4096',
                    cpuOversubscriptionFactor: '0'
                }
            }
        },
        {
            name: 'legacy_fortigate',
            type: nFType_1.NFType.THIRDPARTY,
            controllerType: 'NF_CONTROLLER',
            scalingOptions: {
                supportsScaling: false,
                maxKppsPerInstance: '1000',
                useCpu: false
            },
            metadataOptions: {
                wrapper: 'IP_OPTION'
            },
            components: {
                datapath: {
                    name: 'Fortigate',
                    vendor: 'Fortinet',
                    type: componentType_1.ComponentType.CONTROLLER,
                    vmManifest: {
                        images: [
                            {
                                image: {
                                    url: 'file:///opt/nefeli/images/fortinet/fortigate_vm64_kvm-v6-build0866.qcow2',
                                    checksum: '',
                                    checksumAlg: 'NO_CHECKSUM'
                                },
                                format: '',
                                device: ''
                            }
                        ],
                        interfaces: [
                            {
                                id: 'mgmt0',
                                type: 'MGMT',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '',
                                pciDevice: null,
                                controlPorts: [
                                    22
                                ],
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp0',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '00:16:3d:22:33:57',
                                pciDevice: {
                                    slot: 16
                                },
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp1',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '00:16:3d:22:33:58',
                                pciDevice: {
                                    slot: 17
                                },
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            }
                        ],
                        extraQemuCpuFlags: '',
                        extraQemuFlags: '-uuid 63a4ff68-9a4d-48a8-85e2-a146267b2a8e',
                        licenseMode: 'NO_LICENSE',
                        telnetConsole: 'NO_CONSOLE',
                        consolePciAddr: null
                    },
                    execManifest: null,
                    cpuCores: '1',
                    memoryMb: '1024',
                    cpuOversubscriptionFactor: '0'
                }
            }
        },
        {
            name: 'legacy_paloalto',
            identifier: '123',
            type: nFType_1.NFType.THIRDPARTY,
            controllerType: 'NF_CONTROLLER',
            scalingOptions: {
                supportsScaling: true,
                maxKppsPerInstance: '1000',
                useCpu: false
            },
            metadataOptions: {
                wrapper: 'IP_OPTION'
            },
            components: {
                datapath: {
                    name: 'pa-dataplane',
                    vendor: 'Palo Alto Networks',
                    type: componentType_1.ComponentType.CONTROLLER,
                    vmManifest: {
                        images: [
                            {
                                image: {
                                    url: 'file:///opt/nefeli/images/pan/delta.qcow2',
                                    checksum: '',
                                    checksumAlg: 'NO_CHECKSUM'
                                },
                                format: '',
                                device: ''
                            }
                        ],
                        interfaces: [
                            {
                                id: 'mgmt0',
                                type: 'MGMT',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '',
                                pciDevice: null,
                                controlPorts: [
                                    22,
                                    443
                                ],
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp0',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '00:16:3d:22:33:57',
                                pciDevice: null,
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp1',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '00:16:3d:22:33:58',
                                pciDevice: null,
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            }
                        ],
                        extraQemuCpuFlags: '',
                        extraQemuFlags: '-rtc base=2017-08-25T10:00:01 -uuid f7dea811-4456-44eb-a6ec-11fe6412194b',
                        licenseMode: 'NO_LICENSE',
                        telnetConsole: 'NO_CONSOLE',
                        consolePciAddr: null
                    },
                    execManifest: null,
                    cpuCores: '2',
                    memoryMb: '10240',
                    cpuOversubscriptionFactor: '0'
                }
            }
        },
        {
            name: 'vsrx',
            type: nFType_1.NFType.THIRDPARTY,
            controllerType: 'NF_CONTROLLER',
            scalingOptions: {
                supportsScaling: true,
                maxKppsPerInstance: '1000',
                useCpu: false
            },
            metadataOptions: {
                wrapper: 'IP_OPTION'
            },
            components: {
                datapath: {
                    name: 'vsrx',
                    vendor: 'Juniper Networks',
                    type: componentType_1.ComponentType.CONTROLLER,
                    vmManifest: {
                        images: [
                            {
                                image: {
                                    url: 'file:///opt/nefeli/images/juniper/vsrx-snap.qcow2',
                                    checksum: '',
                                    checksumAlg: 'NO_CHECKSUM'
                                },
                                format: '',
                                device: 'ide-hd,bus=ide.0,unit=0,bootindex=1'
                            }
                        ],
                        interfaces: [
                            {
                                id: 'mgmt0',
                                type: 'MGMT',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '',
                                pciDevice: null,
                                controlPorts: [
                                    22,
                                    830,
                                    8080
                                ],
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp0',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '52:54:00:be:85:02',
                                pciDevice: {
                                    slot: 4
                                },
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            },
                            {
                                id: 'dp1',
                                type: 'DATA',
                                numTxQueues: 0,
                                numRxQueues: 0,
                                mac: '52:54:00:be:85:03',
                                pciDevice: {
                                    slot: 5
                                },
                                checksum: false,
                                gso: false,
                                tso4: false,
                                tso6: false,
                                ecn: false,
                                mergeRxBuffers: false,
                                noMultiQueue: false
                            }
                        ],
                        extraQemuCpuFlags: '',
                        extraQemuFlags: '-rtc base=utc,driftfix=slew -boot strict=on',
                        licenseMode: 'NO_LICENSE',
                        telnetConsole: 'NO_CONSOLE',
                        consolePciAddr: null
                    },
                    execManifest: null,
                    cpuCores: '2',
                    memoryMb: '4096',
                    cpuOversubscriptionFactor: '0'
                }
            }
        }
    ]
};
exports["default"] = nfs;
