"use strict";
exports.__esModule = true;
var mnemeBFState_1 = require("../rest_client/mneme/model/mnemeBFState");
var files = {
    files: [
        {
            desc: 'Juniper vSRX Image',
            name: 'vsrx_img',
            bfi: {
                state: mnemeBFState_1.MnemeBFState.Deleting
            }
        },
        {
            desc: 'Arista Router Image',
            name: 'arista_img',
            bfi: {
                state: mnemeBFState_1.MnemeBFState.Ready
            }
        },
        {
            desc: 'PAN Firewall Image',
            name: 'pan_img',
            bfi: {
                state: mnemeBFState_1.MnemeBFState.Failed
            }
        },
        {
            name: 'distro-nefeli_1.7.0_all.deb',
            desc: 'uploaded distro',
            bfi: {
                state: mnemeBFState_1.MnemeBFState.Ready
            },
            isDistroDeb: true
        }
    ]
};
exports["default"] = files;
