"use strict";
exports.__esModule = true;
var sites_1 = require("./sites");
var tenants_1 = require("./tenants");
var templates = {
    serviceTemplates: [
        {
            config: {
                "nodes": {
                    "panA": {
                        "nf": {
                            "catalogId": "a245add9-c5fc-4886-8132-d5f6f9b99d84",
                            "disableScaling": "true",
                            "initialInstances": "1",
                            "minInstances": "1",
                            "maxInstances": "0",
                            "overprovisionInstances": "0"
                        },
                        "site": "" + sites_1["default"].sites[0].config.identifier,
                        "name": "pan A",
                        "identifier": "panA"
                    },
                    "portX": {
                        "port": {
                            "tunnel": "{{createTunnel { \"name\": \"pan-vni{{.SourceVNI}}\", \"remoteTunnel\":{\"encap\":{\"vxlan\":{\"vni\": \"{{.SourceVNI}}\", \"vtep\":{\"mac\":\"{{.SourceMAC}}\",\"ipAddr\":\"{{.SourceAddr}}\"}  }}}} }}"
                        },
                        "site": "" + sites_1["default"].sites[0].config.identifier,
                        "name": "port X",
                        "identifier": "portX"
                    },
                    "portY": {
                        "port": {
                            "tunnel": "{{createTunnel { \"name\": \"pan-vni{{.SinkVNI}}\", \"remoteTunnel\":{\"encap\":{\"vxlan\":{\"vni\": \"{{.SinkVNI}}\", \"vtep\":{\"mac\":\"{{.SinkMAC}}\",\"ipAddr\":\"{{.SinkAddr}}\"}  }}}} }}"
                        },
                        "site": "" + sites_1["default"].sites[0].config.identifier,
                        "name": "port Y",
                        "identifier": "portY"
                    }
                },
                "edges": [
                    {
                        "a": {
                            "node": "portX",
                            //@ts-ignore
                            "interface": ""
                        },
                        "b": {
                            "node": "panA",
                            //@ts-ignore
                            "interface": "dp0"
                        },
                        "filterAb": {
                            "bpf": ""
                        },
                        "filterBa": {
                            "bpf": ""
                        }
                    },
                    {
                        "a": {
                            "node": "portY",
                            //@ts-ignore
                            "interface": ""
                        },
                        "b": {
                            "node": "panA",
                            //@ts-ignore
                            "interface": "dp1"
                        },
                        "filterAb": {
                            "bpf": ""
                        },
                        "filterBa": {
                            "bpf": ""
                        }
                    }
                ],
                "maxRateKpps": "100000",
                "identifier": "",
                "name": "{{.ServiceName}}",
                "tenantId": "{{tenantID \"{{.TenantName}}\"}}",
                "description": "",
                "licensePoolId": ""
            },
            description: 'Description for Template #1',
            identifier: '1',
            name: 'Template #1',
            nfConfigs: {},
            ownerTenantId: tenants_1["default"].tenants[0].identifier
        },
        {
            config: {},
            description: 'Description for Template #2',
            identifier: '2',
            name: 'Template #2',
            nfConfigs: {},
            ownerTenantId: tenants_1["default"].tenants[1].identifier
        },
        {
            config: {},
            description: 'Description for Template #3',
            identifier: '3',
            name: 'Template #3',
            nfConfigs: {},
            ownerTenantId: tenants_1["default"].tenants[2].identifier
        }
    ]
};
exports["default"] = templates;
