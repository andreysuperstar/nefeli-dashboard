"use strict";
exports.__esModule = true;
exports.getAlarmCount = void 0;
var alarms = {
    alerts: [
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'spy'
            },
            metadata: {},
            severity: 'info',
            timestamp: '1559686357',
            uuid: 'bcecf4b2-2d38-4319-9531-9ccf72a8be11',
            reason: 'Machine Down'
        },
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'peddler'
            },
            metadata: {},
            severity: 'info',
            timestamp: '1559686357',
            uuid: '887f7c95-edb0-46c6-9ad4-adba15ba7464',
            reason: 'Process Started: Peddler'
        },
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'heimdallr'
            },
            metadata: {},
            severity: 'info',
            timestamp: '1559686357',
            uuid: 'f2b9af75-f214-4245-ac02-ff9fc21509a1',
            reason: 'Fortigate is out of license'
        },
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'heimdallr'
            },
            metadata: {},
            severity: 'critical',
            timestamp: '1559686362',
            uuid: 'e394d19d-6978-46e0-8faa-d376f703fd1e',
            reason: 'Running low on memory'
        },
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'heimdallr'
            },
            metadata: {},
            severity: 'info',
            timestamp: '1559687327',
            uuid: 'd2caf212-0e45-4c1b-91d0-cc459d4582d5',
            reason: 'VSRX failed to scale'
        },
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'heimdallr'
            },
            metadata: {},
            severity: 'critical',
            timestamp: '1559688131',
            uuid: 'd688ed54-be1a-439c-9968-b29aa9226e91',
            reason: 'Disk space is running low'
        },
        {
            _class: 'process',
            name: 'ProcessStarted',
            identity: {
                process: 'heimdallr',
                pipeline_name: 'vsrx',
                vnf: 'vsrx_v3',
                instance_id: '3'
            },
            metadata: {},
            severity: 'critical',
            timestamp: '1559689216',
            uuid: '241441a7-2c29-4d8a-9721-f9c9ba025371',
            reason: 'Process Started: Heimdallr'
        }
    ],
    metadata: {
        endTime: '2019-06-04T23:00:16.000Z',
        count: 7,
        startTime: '2019-06-04T22:12:37.000Z'
    }
};
var getAlarmCount = function (count) {
    return { count: count };
};
exports.getAlarmCount = getAlarmCount;
exports["default"] = alarms;
