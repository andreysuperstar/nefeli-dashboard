"use strict";
exports.__esModule = true;
var nicBandwidth_1 = require("../rest_client/pangolin/model/nicBandwidth");
var bondMode_1 = require("../rest_client/pangolin/model/bondMode");
var bondModeXorPolicy_1 = require("../rest_client/pangolin/model/bondModeXorPolicy");
var tunnelType_1 = require("../rest_client/pangolin/model/tunnelType");
var nicDriver_1 = require("../rest_client/pangolin/model/nicDriver");
var kernelMode_1 = require("../rest_client/pangolin/model/kernelMode");
var subnetType_1 = require("../rest_client/pangolin/model/subnetType");
var profiles = {
    hardwareProfiles: [
        {
            identifier: '1',
            description: 'Profile 1 description',
            name: 'Profile 1',
            bonds: [
                {
                    identifier: 'bond_1',
                    bond: {
                        bandwidth: nicBandwidth_1.NicBandwidth.GB1,
                        bondMode: bondMode_1.BondMode.TXLB,
                        bondModeXorPolicy: bondModeXorPolicy_1.BondModeXorPolicy.L2,
                        connectivity: tunnelType_1.TunnelType.VLAN,
                        devices: ['device 1', 'device 2', 'device 3'],
                        driver: nicDriver_1.NicDriver.IGBUIO,
                        kernel: kernelMode_1.KernelMode.BYPASS,
                        macAddr: 'ac:de:48:00:11:22',
                        purpose: subnetType_1.SubnetType.WAN,
                        socket: 'sock'
                    }
                },
                {
                    identifier: 'bond_2',
                    bond: {
                        bandwidth: nicBandwidth_1.NicBandwidth.GB1,
                        bondMode: bondMode_1.BondMode.TXLB,
                        bondModeXorPolicy: bondModeXorPolicy_1.BondModeXorPolicy.L2,
                        connectivity: tunnelType_1.TunnelType.VLAN,
                        devices: ['device 1', 'device 2', 'device 3'],
                        driver: nicDriver_1.NicDriver.IGBUIO,
                        kernel: kernelMode_1.KernelMode.BYPASS,
                        macAddr: 'ac:de:48:00:11:23',
                        purpose: subnetType_1.SubnetType.LAN,
                        socket: 'sock'
                    }
                }
            ]
        },
        {
            identifier: '2',
            name: 'Profile 2',
            description: 'Profile 2 description',
            bonds: [
                {
                    identifier: 'bond_2',
                    bond: {
                        bandwidth: nicBandwidth_1.NicBandwidth.GB1,
                        bondMode: bondMode_1.BondMode.TXLB,
                        bondModeXorPolicy: bondModeXorPolicy_1.BondModeXorPolicy.L2,
                        connectivity: tunnelType_1.TunnelType.VLAN,
                        devices: ['device 1', 'device 2', 'device 3'],
                        driver: nicDriver_1.NicDriver.IGBUIO,
                        kernel: kernelMode_1.KernelMode.BYPASS,
                        macAddr: 'ac:de:48:00:11:22',
                        purpose: subnetType_1.SubnetType.GENERIC,
                        socket: '1'
                    }
                }, {
                    identifier: 'bond_4',
                    bond: {
                        bandwidth: nicBandwidth_1.NicBandwidth.GB100,
                        bondMode: bondMode_1.BondMode.XOR,
                        bondModeXorPolicy: bondModeXorPolicy_1.BondModeXorPolicy.L2,
                        connectivity: tunnelType_1.TunnelType.VLANQINQ,
                        devices: ['device 4', 'device 5', 'device 6'],
                        driver: nicDriver_1.NicDriver.VFIOPCI,
                        kernel: kernelMode_1.KernelMode.SINGLEIP,
                        macAddr: 'ac:de:48:00:44:33',
                        purpose: subnetType_1.SubnetType.LAN,
                        socket: '2'
                    }
                }
            ]
        },
        {
            identifier: '3',
            description: 'Profile 3 description',
            name: 'A very long profile name',
            bonds: [
                {
                    identifier: 'bond_3',
                    bond: {
                        bandwidth: nicBandwidth_1.NicBandwidth.GB1,
                        bondMode: bondMode_1.BondMode.TXLB,
                        bondModeXorPolicy: bondModeXorPolicy_1.BondModeXorPolicy.L2,
                        connectivity: tunnelType_1.TunnelType.VLAN,
                        devices: ['device 1', 'device 2', 'device 3'],
                        driver: nicDriver_1.NicDriver.IGBUIO,
                        kernel: kernelMode_1.KernelMode.BYPASS,
                        macAddr: 'ac:de:48:00:11:22',
                        purpose: subnetType_1.SubnetType.GENERIC,
                        socket: 'sock'
                    }
                }
            ]
        }
    ]
};
exports["default"] = profiles;
