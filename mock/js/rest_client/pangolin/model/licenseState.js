"use strict";
/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
exports.__esModule = true;
exports.LicenseState = void 0;
exports.LicenseState = {
    AVAILABLE: 'AVAILABLE',
    RECLAIMING: 'RECLAIMING',
    GRANTED: 'GRANTED',
    ALLOCATEDTOSITE: 'ALLOCATED_TO_SITE',
    INUSE: 'IN_USE'
};
