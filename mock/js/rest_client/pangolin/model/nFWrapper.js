"use strict";
/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
exports.__esModule = true;
exports.NFWrapper = void 0;
exports.NFWrapper = {
    DEFAULT: 'DEFAULT',
    IPOPTION: 'IP_OPTION',
    IPEXCESS: 'IP_EXCESS',
    L4HACK: 'L4HACK',
    NSH: 'NSH',
    VLANID: 'VLAN_ID',
    IPID: 'IP_ID',
    MACADDRESS: 'MAC_ADDRESS',
    NONE: 'NONE'
};
