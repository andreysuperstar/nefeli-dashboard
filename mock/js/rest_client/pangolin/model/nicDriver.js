"use strict";
/**
 * Orchestration
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * OpenAPI spec version: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
exports.__esModule = true;
exports.NicDriver = void 0;
exports.NicDriver = {
    DUMMYNicDriver: 'DUMMY_NicDriver',
    VFIOPCI: 'VFIO_PCI',
    UIOPCIGENERIC: 'UIO_PCI_GENERIC',
    IGBUIO: 'IGB_UIO',
    UIOHVGENERIC: 'UIO_HV_GENERIC'
};
