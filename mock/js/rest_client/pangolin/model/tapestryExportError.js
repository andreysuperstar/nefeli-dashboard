"use strict";
exports.__esModule = true;
exports.TapestryExportError = void 0;
var TapestryExportError;
(function (TapestryExportError) {
    TapestryExportError.ErrorCodeEnum = {
        NUMBER_1500: 1500,
        NUMBER_1501: 1501,
        NUMBER_1502: 1502,
        NUMBER_1503: 1503,
        NUMBER_1504: 1504,
        NUMBER_1505: 1505,
        NUMBER_1506: 1506,
        NUMBER_1507: 1507,
        NUMBER_1508: 1508,
        NUMBER_1509: 1509,
        NUMBER_1510: 1510,
        NUMBER_1511: 1511,
        NUMBER_1512: 1512,
        NUMBER_1513: 1513,
        NUMBER_1514: 1514,
        NUMBER_1515: 1515,
        NUMBER_1516: 1516,
        NUMBER_1517: 1517,
        NUMBER_1518: 1518,
        NUMBER_1519: 1519,
        NUMBER_1520: 1520,
        NUMBER_1521: 1521,
        NUMBER_1522: 1522,
        NUMBER_1523: 1523,
        NUMBER_1524: 1524,
        NUMBER_1525: 1525,
        NUMBER_1526: 1526,
        NUMBER_1527: 1527,
        NUMBER_1528: 1528,
        NUMBER_1529: 1529,
        NUMBER_1530: 1530,
        NUMBER_1531: 1531,
        NUMBER_1532: 1532,
        NUMBER_1533: 1533
    };
})(TapestryExportError = exports.TapestryExportError || (exports.TapestryExportError = {}));
