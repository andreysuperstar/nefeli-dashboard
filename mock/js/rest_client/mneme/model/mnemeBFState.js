"use strict";
/**
 * mneme-rest-server
 * Mnmeme REST Server
 *
 * OpenAPI spec version: 0.0.1
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
exports.__esModule = true;
exports.MnemeBFState = void 0;
exports.MnemeBFState = {
    UNUSED: 'UNUSED',
    Storing: 'Storing',
    Ready: 'Ready',
    Failed: 'Failed',
    Deleting: 'Deleting',
    DelASAP: 'DelASAP'
};
