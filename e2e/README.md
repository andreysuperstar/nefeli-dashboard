How to run Dashboard's End-to-End Tests
=======================================

Prerequisites
-------------

* Firefox (this is the default browser)
* Go to directory `dashboard/web`. All commands below should be executed in this directory.
* Execute the following commands at least once:
```
$ npm install
$ npm run webdriver
```

Test with Development Server
----------------------------

If you are implementing e2e tests, it is advised to use a development server.  You can use a real server, but using a dev server will simply be faster.  *It is still advised to test against a real server before merging to master.*

1. Start the dev server `ng serve`
1. Start the mock server `npm run mock-server`
1. Execute the e2e tests `npm run protractor`. To re-run the tests, only this step is needed.

You may replace steps 1 & 3 with just the command `ng run e2e`.  This command builds the application, starts the dev server, then executes the e2e tests.  This process is much slower, though, so I recommend using the three steps above.

Test with "Real" Server
-----------------------

To test against an actual server, modify the file `e2e/protractor.conf.js`.

1. Modify `baseUrl` to `https://<address>/dashboard/`.  For example: `https://fluffy.lazy-dog.org/dashboard/`
1. Modify `params.login` object to a valid user.
1. Execute the e2e test `npm run protractor`