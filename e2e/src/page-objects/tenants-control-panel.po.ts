import { browser, element, by, ElementFinder, ElementArrayFinder, ExpectedConditions, promise } from 'protractor';

import { ControlPanelPage } from './control-panel.po';
import { ToolbarComponent } from './toolbar.po';

import { CONFIG } from '../config';

export class TenantsControlPanelPage {

  public static URI = '#/control-panel/tenants';
  private _toolbar = new ToolbarComponent();

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        if (!url.endsWith(TenantsControlPanelPage.URI)) {
          throw new Error(`Expected to be on Tenants Control Panel page, not (${url})`);
        }
      });
  }

  public get toolbar(): ToolbarComponent {
    return this._toolbar;
  }

  private get controlPanelLink(): ElementFinder {
    return element(by.css('a.back'));
  }

  public async navigateBackToControlPanel(): Promise<ControlPanelPage> {
    await this.controlPanelLink.click();

    return new ControlPanelPage();
  }

  private get addNewTenantButton(): ElementFinder {
    return element(by.css('button.add'));
  }

  private get tenantRows(): ElementArrayFinder {
    return element.all(by.tagName('mat-row'));
  }

  public tenantRowsWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.tenantRows.last()),
      CONFIG.DOM_WAIT_TIMEOUT,
      'tenant rows are not found');
  }

  private tenantRow(name: string): ElementFinder {
    return element(by.cssContainingText('mat-row', name));
  }

  public tenantRowWait(name: string, presence: boolean = true): promise.Promise<void> {
    const condition: Function = presence
      ? ExpectedConditions.presenceOf(this.tenantRow(name))
      : ExpectedConditions.stalenessOf(this.tenantRow(name));

    const message: string = presence
      ? 'tenant row is not found'
      : 'tenant row is found';

    return browser.wait(condition, CONFIG.DOM_WAIT_TIMEOUT, message);
  }

  private tenantRemoveButton(name: string): ElementFinder {
    return this.tenantRow(name).element(by.buttonText('Remove'));
  }

  private get overlayBackdrop(): ElementFinder {
    return element(by.css('.cdk-overlay-backdrop'));
  }

  private overlayBackdropWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.stalenessOf(this.overlayBackdrop),
      CONFIG.DOM_WAIT_TIMEOUT,
      'overlay backdrop is present on the DOM');
  }

  private get tenantDialog(): ElementFinder {
    return element(by.tagName('nef-edit-tenant-dialog'));
  }

  private tenantDialogWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.tenantDialog),
      CONFIG.DOM_WAIT_TIMEOUT,
      'New Tenant Dialog is not found');
  }

  private get tenantNameInput(): ElementFinder {
    return element(by.css('input[formcontrolname="name"]'));
  }

  private get submitTenantDialogButton(): ElementFinder {
    return element(by.css('button[form="tenant-form"]'));
  }

  public async addTenant(): Promise<string> {
    await this.addNewTenantButton.click();

    await this.tenantDialogWait();

    const tenantsCount = 100000000;
    const tenantNumber: number = Math.floor(Math.random() * tenantsCount);

    const name = `Tenant ${tenantNumber}`;

    await this.tenantNameInput.sendKeys(name);
    await this.submitTenantDialogButton.click();

    await this.overlayBackdropWait();

    await this.tenantRowWait(name);

    return name;
  }

  public async removeTenant(name: string): Promise<void> {
    await this.tenantRemoveButton(name).click();

    await this.tenantRowWait(name, false);
  }

}
