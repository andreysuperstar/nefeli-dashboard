import { browser, element, by, ElementFinder, ElementArrayFinder, ExpectedConditions, promise } from 'protractor';

import { ServerPage } from './server.po';

import { CONFIG } from '../config';

export class SitesPage {

  public static URI = '#/sites/';

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        const siteId: string = url.split(SitesPage.URI)[1];

        if (!url.includes(SitesPage.URI) || !siteId) {
          throw new Error(`Expected to be on Sites page, not (${url})`);
        }
      });
  }

  private statusList(heading: string): ElementFinder {
    return element(by.cssContainingText('nef-status-list', heading));
  }

  private statusListLinks(listHeading: string): ElementArrayFinder {
    return this.statusList(listHeading)
      .all(by.css('mat-nav-list a'));
  }

  public statusListLinksWait(listHeading: string): promise.Promise<void> {
    return browser.wait(
      ExpectedConditions.presenceOf(this.statusListLinks(listHeading).last()),
      CONFIG.DOM_WAIT_TIMEOUT,
      `${listHeading} status list links are not found`);
  }

  private serverLink(index: number = 0): ElementFinder {
    return this.statusListLinks('Servers')
      .get(index);
  }

  public async navigateToServer(serverLinkIndex?: number): Promise<ServerPage> {
    await this.serverLink(serverLinkIndex).click();

    await browser.wait(ExpectedConditions.urlContains(ServerPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new ServerPage();
  }

}
