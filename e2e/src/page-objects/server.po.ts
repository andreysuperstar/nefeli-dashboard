import { browser, element, by, ElementFinder, ElementArrayFinder, ExpectedConditions, promise } from 'protractor';

import { ToolbarComponent } from './toolbar.po';

import { CONFIG } from '../config';

export class ServerPage {

  public static URI = '/servers/';
  private _toolbar = new ToolbarComponent();

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        const serverPath: string = url.split(ServerPage.URI)[1];
        const serverId: string = serverPath.replace('/socket', '');

        if (!url.includes(ServerPage.URI) || !serverId || !url.endsWith('socket')) {
          throw new Error(`Expected to be on Server page, not (${url})`);
        }
      });
  }

  public get toolbar(): ToolbarComponent {
    return this._toolbar;
  }

  private get nFsLegendComponent(): ElementFinder {
    return element(by.css('.network-functions nef-legend'));
  }

  public nFsLegendComponentWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.nFsLegendComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'network functions Legend component is not found');
  }

  private get nFLegendItems(): ElementArrayFinder {
    return this.nFsLegendComponent.all(by.tagName('span'));
  }

  public async nFLegendNFsCount(): Promise<number> {
    const nFLegendItemsCount: number = await this.nFLegendItems.count();

    // Subtract 'Available' legend item
    const nFLegendNFsCount: number = nFLegendItemsCount - 1;

    return nFLegendNFsCount;
  }

  private get socketComponents(): ElementArrayFinder {
    return element.all(by.tagName('nef-socket'));
  }

  public socketComponentsWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.socketComponents.last()),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Socket components are not found');
  }

  private getSocketComponent(index: number = 0): ElementFinder {
    return this.socketComponents.get(index);
  }

  private socketCores(socketIndex?: number): ElementArrayFinder {
    return this.getSocketComponent(socketIndex)
      .all(by.css('nef-cell-grid .cell'));
  }

  public socketCoresCount(socketIndex?: number): promise.Promise<number> {
    return this.socketCores(socketIndex)
      .count();
  }

  private async isActiveCell(cell: ElementFinder): Promise<boolean> {
    const inactiveCellColor = '#E1E5EB';
    const cellBackgroundColor: string = await cell.getCssValue('background-color');

    return cellBackgroundColor !== inactiveCellColor;
  }

  public socketActiveCoresCount(socketIndex?: number): promise.Promise<number> {
    return this.socketCores(socketIndex)
      .filter(this.isActiveCell)
      .count();
  }

  private memoryCells(socketIndex?: number): ElementArrayFinder {
    return this.getSocketComponent(socketIndex)
      .all(by.css('nef-memory-meter .cell'));
  }

  public memoryActiveCellsCount(socketIndex?: number): promise.Promise<number> {
    return this.memoryCells(socketIndex)
      .filter(this.isActiveCell)
      .count();
  }

  private get throughputChartComponent(): ElementFinder {
    return element(by.tagName('nef-chart-throughput'));
  }

  public throughputChartComponentWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.throughputChartComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Throughput Chart component is not found');
  }

  private get latencyChartComponent(): ElementFinder {
    return element(by.tagName('nef-chart-latency'));
  }

  public latencyChartComponentWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.latencyChartComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Latency Chart component is not found');
  }

  private get packetlossChartComponent(): ElementFinder {
    return element(by.tagName('nef-chart-packetloss'));
  }

  public packetlossChartComponentWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.packetlossChartComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Packetloss Chart component is not found');
  }

}
