import { browser, element, by, ElementFinder, ExpectedConditions, promise } from 'protractor';

import { OverviewPage } from './overview.po';

import { CONFIG } from '../config';

export class LoginPage {

  public static URI = '#/auth/login';

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        if (!url.endsWith(LoginPage.URI)) {
          throw new Error(`Expected to be on Login page, not (${url})`);
        }
      });
  }

  public static async goTo(): Promise<LoginPage> {
    await browser.get(LoginPage.URI, CONFIG.BROWSER_WAIT_TIMEOUT);

    return new LoginPage();
  }

  private get usernameInput(): ElementFinder {
    return element(by.css('input[formcontrolname="username"]'));
  }

  public usernameInputWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.usernameInput),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Username input field not found');
  }

  private get passwordInput(): ElementFinder {
    return element(by.css('input[formcontrolname="password"]'));
  }

  public passwordInputWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.passwordInput),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Password input field not found');
  }

  private get loginButton(): ElementFinder {
    return element(by.css('button[type="submit"]'));
  }

  public loginButtonWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.loginButton),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Login button not found');
  }

  public async loginAs(username: string, password: string): Promise<OverviewPage> {
    await this.usernameInput.sendKeys(username);
    await this.passwordInput.sendKeys(password);
    await this.loginButton.click();

    await browser.wait(ExpectedConditions.urlContains(OverviewPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new OverviewPage();
  }

  public loginAsRoot(): Promise<OverviewPage> {
    const username: string = browser.params.login.user;
    const password: string = browser.params.login.password;

    return this.loginAs(username, password);
  }

}
