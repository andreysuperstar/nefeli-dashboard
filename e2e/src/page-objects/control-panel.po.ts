import { browser, element, by, ElementFinder, ExpectedConditions, promise } from 'protractor';

import { UsersControlPanelPage } from './users-control-panel.po';
import { TenantsControlPanelPage } from './tenants-control-panel.po';

import { CONFIG } from '../config';

export class ControlPanelPage {

  public static URI = '#/control-panel';

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        if (!url.endsWith(ControlPanelPage.URI)) {
          throw new Error(`Expected to be on Control Panel page, not (${url})`);
        }
      });
  }

  private get usersCardLink(): ElementFinder {
    return element(by.linkText('View Users'));
  }

  public usersCardLinkWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.usersCardLink),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Users card link not found');
  }

  public async navigateToUsersControlPanel(): Promise<UsersControlPanelPage> {
    await this.usersCardLink.click();

    await browser.wait(ExpectedConditions.urlContains(UsersControlPanelPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new UsersControlPanelPage();
  }

  private get tenantsCardLink(): ElementFinder {
    return element(by.linkText('View Tenants'));
  }

  public tenantsCardLinkWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.tenantsCardLink),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Tenants card link not found');
  }

  public async navigateToTenantsControlPanel(): Promise<TenantsControlPanelPage> {
    await this.tenantsCardLink.click();

    await browser.wait(ExpectedConditions.urlContains(TenantsControlPanelPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new TenantsControlPanelPage();
  }

  private get networkFunctionsCardLink(): ElementFinder {
    return element(by.linkText('View Network Functions'));
  }

  public networkFunctionsCardLinkWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.networkFunctionsCardLink),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Network Functions card link not found');
  }

}
