import { browser, element, by, ElementFinder, ElementArrayFinder, ExpectedConditions, promise } from 'protractor';

import { ControlPanelPage } from './control-panel.po';

import { CONFIG } from '../config';

export class UsersControlPanelPage {

  public static URI = '#/control-panel/users';

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        if (!url.endsWith(UsersControlPanelPage.URI)) {
          throw new Error(`Expected to be on Users Control Panel page, not (${url})`);
        }
      });
  }

  private get controlPanelLink(): ElementFinder {
    return element(by.css('a.back'));
  }

  public async navigateBackToControlPanel(): Promise<ControlPanelPage> {
    await this.controlPanelLink.click();

    return new ControlPanelPage();
  }

  private get removeButton(): ElementFinder {
    return element(by.buttonText('Remove'));
  }

  private get addNewUserButton(): ElementFinder {
    return element(by.css('button.add'));
  }

  private get userRows(): ElementArrayFinder {
    return element.all(by.tagName('mat-row'));
  }

  public userRowsWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.stalenessOf(this.userRows.first()),
      CONFIG.DOM_WAIT_TIMEOUT,
      'users are listed');
  }

  private userRow(email: string): ElementFinder {
    return element(by.cssContainingText('mat-row', email));
  }

  public userRowWait(email: string, presence: boolean = true): promise.Promise<void> {
    const condition: Function = presence
      ? ExpectedConditions.presenceOf(this.userRow(email))
      : ExpectedConditions.stalenessOf(this.userRow(email));

    const message: string = presence
      ? 'user row is not found'
      : 'user row is found';

    return browser.wait(condition, CONFIG.DOM_WAIT_TIMEOUT, message);
  }

  private userSelectCheckbox(email: string): ElementFinder {
    return this.userRow(email).element(by.css('.mat-column-select mat-checkbox'));
  }

  private get overlayBackdrops(): ElementArrayFinder {
    return element.all(by.css('.cdk-overlay-backdrop'));
  }

  private overlayBackdropWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.stalenessOf(this.overlayBackdrops.first()),
      CONFIG.DOM_WAIT_TIMEOUT,
      'overlay backdrop is present on the DOM');
  }

  private get addUserDialog(): ElementFinder {
    return element(by.tagName('nef-add-user-dialog'));
  }

  private addUserDialogWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.addUserDialog),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Add User Dialog is not found');
  }

  private get usernameInput(): ElementFinder {
    return element(by.css('input[formcontrolname="username"]'));
  }

  private get emailInput(): ElementFinder {
    return element(by.css('input[formcontrolname="email"]'));
  }

  private get tenantSelect(): ElementFinder {
    return element(by.css('mat-select[formcontrolname="tenant"]'));
  }

  private get rolesSelect(): ElementFinder {
    return element(by.css('mat-select[formcontrolname="roles"]'));
  }

  private get selectOptions(): ElementArrayFinder {
    return element.all(by.tagName('mat-option'));
  }

  private get cancelAddUserDialogButton(): ElementFinder {
    return element(by.css('button[mat-dialog-close]'));
  }

  private get submitAddUserDialogButton(): ElementFinder {
    return element(by.css('button[form="user-form"]'));
  }

  public async addUser(): Promise<string> {
    await this.addNewUserButton.click();

    await this.addUserDialogWait();

    const username = 'Andrew';
    const email = 'andrew@nefeli.io';

    await this.usernameInput.sendKeys(username);
    await this.emailInput.sendKeys(email);

    await this.tenantSelect.click();
    await this.selectOptions
      .first()
      .click();

    await this.rolesSelect.click();
    await this.selectOptions
      .first()
      .click();

    await this.overlayBackdrops
      .last()
      .click();

    /* Use Cancel button here to close dialog because when adding a user, an email
       should be sent to that user. By default email is not configured so it will always
       get error and prevent dialog closing but the user is still actually created */
    await this.submitAddUserDialogButton.click();
    await this.cancelAddUserDialogButton.click();

    await this.overlayBackdropWait();

    await this.userRowWait(email);

    return email;
  }

  public async removeUser(email: string): Promise<void> {
    await this.userSelectCheckbox(email).click();

    await this.removeButton.click();

    await this.userRowWait(email, false);
  }

}
