import { browser, element, by, ElementFinder, ExpectedConditions } from 'protractor';

import { ToolbarComponent } from './toolbar.po';

import { CONFIG } from '../config';

export class OverviewPage {

  public static URI = '#/overview';
  private _toolbar = new ToolbarComponent();

  constructor() {
    browser
      .getCurrentUrl()
      .then((url: string) => {
        if (!url.endsWith(OverviewPage.URI)) {
          throw new Error(`Expected to be on Overview page, not (${url})`);
        }
      });
  }

  public get toolbar(): ToolbarComponent {
    return this._toolbar;
  }

  private get summaryComponent(): ElementFinder {
    return element(by.tagName('nef-summary'));
  }

  public summaryComponentWait() {
    browser.wait(ExpectedConditions.presenceOf(this.summaryComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Summary component is not found');
  }

  private get performanceComponent(): ElementFinder {
    return element(by.tagName('nef-performance'));
  }

  public performanceComponentWait() {
    browser.wait(ExpectedConditions.presenceOf(this.performanceComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Performance component is not found');
  }

  public get siteUsageComponent(): ElementFinder {
    return element(by.tagName('nef-cluster-usage'));
  }

  public siteUsageComponentWait() {
    browser.wait(ExpectedConditions.presenceOf(this.siteUsageComponent),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Site Usage component is not found');
  }

}
