import { browser, element, by, ElementFinder, ExpectedConditions, promise } from 'protractor';

import { LoginPage } from './login.po';
import { SitesPage } from './sites.po';
import { ControlPanelPage } from './control-panel.po';

import { CONFIG } from '../config';

export class ToolbarComponent {

  public async navigateToControlPanel(): Promise<ControlPanelPage> {
    await this.openUserDropdown();
    await this.controlPanelLinkWait();
    await this.controlPanelLink.click();

    await browser.wait(ExpectedConditions.urlContains(ControlPanelPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new ControlPanelPage();
  }

  private get sitesLink(): ElementFinder {
    return element(by.linkText('Sites'));
  }

  public sitesLinkWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.sitesLink),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Sites link is not found');
  }

  private get userButton(): ElementFinder {
    return element(by.css('.user-avatar'));
  }

  public userButtonWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.userButton),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Username button is not found');
  }

  public openUserDropdown(): promise.Promise<void> {
    return this.userButton.click();
  }

  private get controlPanelLink(): ElementFinder {
    return element(by.linkText('Control Panel'));
  }

  private controlPanelLinkWait(): promise.Promise<{}> {
    return browser.wait(ExpectedConditions.presenceOf(this.controlPanelLink),
      CONFIG.DOM_WAIT_TIMEOUT,
      'Control Panel link is not found');
  }

  private get logoutButton(): ElementFinder {
    return element(by.buttonText('Logout'));
  }

  public async navigateToSites(): Promise<SitesPage> {
    await this.sitesLink.click();

    await browser.wait(ExpectedConditions.urlContains(SitesPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new SitesPage();
  }

  public async logout(): Promise<LoginPage> {
    await this.openUserDropdown();
    await this.logoutButton.click();

    await browser.wait(ExpectedConditions.urlContains(LoginPage.URI),
      CONFIG.BROWSER_WAIT_TIMEOUT);

    return new LoginPage();
  }

}
