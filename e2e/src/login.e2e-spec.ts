import { browser } from 'protractor';

import { LoginPage } from './page-objects/login.po';
import { OverviewPage } from './page-objects/overview.po';

describe('Login', () => {
  let loginPage: LoginPage;

  beforeEach(async () => {
    loginPage = await LoginPage.goTo();
  });

  it('should login as system user and logout successfully', async () => {
    browser.waitForAngularEnabled(false);

    // Verify Login page elements are rendered
    await Promise.all([
      loginPage.usernameInputWait(),
      loginPage.passwordInputWait(),
      loginPage.loginButtonWait()
    ]);

    // Log in and verify Overview page is displayed properly
    const overviewPage: OverviewPage = await loginPage.loginAsRoot();

    overviewPage.summaryComponentWait();
    overviewPage.performanceComponentWait();
    overviewPage.siteUsageComponentWait();

    await overviewPage.toolbar.userButtonWait();

    // Log out user and Login page is displayed properly
    loginPage = await overviewPage.toolbar.logout();

    await Promise.all([
      loginPage.usernameInputWait(),
      loginPage.passwordInputWait(),
      loginPage.loginButtonWait()
    ]);
  });
});
