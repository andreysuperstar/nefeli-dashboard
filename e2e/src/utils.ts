import { browser } from 'protractor';

export class Utils {

  // abstract writing screen shot to a file
  public static writeToFile(data: string, filename: string, encoding = 'base64') {
    const fs = require('fs');
    const stream = fs.createWriteStream(filename);
    stream.write(Buffer.from(data, encoding));
    stream.end();
  }

  public static takeScreenshot(filename = 'screenshot.png') {
    browser.takeScreenshot().then((png) => {
      Utils.writeToFile(png, filename);
    });
  }
}
