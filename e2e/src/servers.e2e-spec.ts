import { browser } from 'protractor';

import { LoginPage } from './page-objects/login.po';
import { OverviewPage } from './page-objects/overview.po';
import { SitesPage } from './page-objects/sites.po';
import { ServerPage } from './page-objects/server.po';

describe('Servers', () => {
  let loginPage: LoginPage;
  let overviewPage: OverviewPage;

  beforeEach(async () => {
    loginPage = await LoginPage.goTo();

    browser.waitForAngularEnabled(false);

    // Log in and verify Toolbar component is displayed properly
    overviewPage = await loginPage.loginAsRoot();
    await overviewPage.toolbar.sitesLinkWait();
  });

  it('should render network functions, cores and memory cells on Server page', async () => {
    // Navigate to Sites page and verify Servers status list is displayed
    const sitesPage: SitesPage = await overviewPage.toolbar.navigateToSites();
    await sitesPage.statusListLinksWait('Servers');

    // Navigate to Server page and verify the page is displayed properly
    const serverPage: ServerPage = await sitesPage.navigateToServer();

    await Promise.all([
      serverPage.nFsLegendComponentWait(),
      serverPage.socketComponentsWait(),
      serverPage.throughputChartComponentWait(),
      serverPage.latencyChartComponentWait(),
      serverPage.packetlossChartComponentWait()
    ]);

    // Verify network functions are listed in legend
    const nFLegendNFsCount: number = await serverPage.nFLegendNFsCount();
    const expectedNFsCount = 5;

    expect(nFLegendNFsCount).toEqual(expectedNFsCount,
      `network functions count is not equal to ${expectedNFsCount}`);

    // Verify socket cores
    const socketCoresCount: number = await serverPage.socketCoresCount();
    let expectedCoresCount = 12;

    expect(socketCoresCount).toEqual(expectedCoresCount,
      `socket cores count is not equal to ${expectedCoresCount}`);

    // Verify active socket cores
    const socketActiveCoresCount: number = await serverPage.socketActiveCoresCount();
    expectedCoresCount = 0;

    expect(socketActiveCoresCount).toEqual(expectedCoresCount,
      `active socket cores count is not equal to ${expectedCoresCount}`);

    // Verify active memory cells
    const memoryActiveCellsCount: number = await serverPage.memoryActiveCellsCount();
    const memoryCellsCount = 0;

    expect(memoryActiveCellsCount).toEqual(memoryCellsCount,
      `active memory cells count is not equal to ${memoryCellsCount}`);

    // Log out user
    await serverPage.toolbar.logout();
  });
});
