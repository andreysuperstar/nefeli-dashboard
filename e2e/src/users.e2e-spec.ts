import { browser } from 'protractor';

import { LoginPage } from './page-objects/login.po';
import { OverviewPage } from './page-objects/overview.po';
import { ControlPanelPage } from './page-objects/control-panel.po';
import { TenantsControlPanelPage } from './page-objects/tenants-control-panel.po';
import { UsersControlPanelPage } from './page-objects/users-control-panel.po';

describe('Users', () => {
  let loginPage: LoginPage;
  let overviewPage: OverviewPage;
  let controlPanelPage: ControlPanelPage;
  let tenantsControlPanelPage: TenantsControlPanelPage;

  beforeEach(async () => {
    loginPage = await LoginPage.goTo();

    browser.waitForAngularEnabled(false);

    // Log in and verify Toolbar component is displayed properly
    overviewPage = await loginPage.loginAsRoot();
    await overviewPage.toolbar.userButtonWait();
  });

  it('should create tenant and user successfully', async () => {
    // Navigate to Control Panel page and verify tenants card link is displayed
    controlPanelPage = await overviewPage.toolbar.navigateToControlPanel();
    await controlPanelPage.tenantsCardLinkWait();

    // Navigate to Tenants Control Panel page and verify tenants rows are displayed
    tenantsControlPanelPage = await controlPanelPage.navigateToTenantsControlPanel();
    await tenantsControlPanelPage.tenantRowsWait();

    /* Add new tenant, navigate back to Control Panel page
       and verify users card link is displayed */
    const tenantName: string = await tenantsControlPanelPage.addTenant();

    await tenantsControlPanelPage.navigateBackToControlPanel();
    await controlPanelPage.usersCardLinkWait();

    // Navigate to Users Control Panel page and verify no users are listed
    browser.waitForAngularEnabled(true);

    const usersControlPanelPage: UsersControlPanelPage = await controlPanelPage.navigateToUsersControlPanel();
    await usersControlPanelPage.userRowsWait();

    // Add user and verify user is listed
    const userEmail: string = await usersControlPanelPage.addUser();

    // Remove user and verify user is not listed
    await usersControlPanelPage.removeUser(userEmail);

    browser.waitForAngularEnabled(false);

    /* Navigate back to Control Panel and Tenant Control Panel pages,
       remove tenant and verify tenant is not listed */
    controlPanelPage = await usersControlPanelPage.navigateBackToControlPanel();
    tenantsControlPanelPage = await controlPanelPage.navigateToTenantsControlPanel();

    await tenantsControlPanelPage.tenantRowWait(tenantName);
    // Commented until remove tenant functionality is implemented
    // await tenantsControlPanelPage.removeTenant(tenantName);

    // Log out user
    await tenantsControlPanelPage.toolbar.logout();
  });
});
