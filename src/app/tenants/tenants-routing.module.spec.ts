import { NgZone } from '@angular/core';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TenantsRoutingModule, routes } from './tenants-routing.module';
import { MatPaginatorModule } from '@angular/material/paginator';

import { TenantsListComponent } from './tenants-list.component';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { NetworkFunctionComponent } from '../pipeline/network-function.component';
import { NetworkFunctionMenuComponent } from '../pipeline/network-function-menu.component';
import { BackButtonComponent } from '../shared/back-button/back-button.component';
import { TableComponent } from '../shared/table/table.component';
import { MatChipsModule } from '@angular/material/chips';

describe('TenantsRoutingModule', () => {
  let tenantsRoutingModule: TenantsRoutingModule;
  let router: Router;
  let location: Location;
  let ngZone: NgZone;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        ReactiveFormsModule,
        OverlayModule,
        MatExpansionModule,
        MatDividerModule,
        MatTableModule,
        MatCardModule,
        MatInputModule,
        MatBadgeModule,
        MatListModule,
        MatMenuModule,
        MatChipsModule,
        MatPaginatorModule
      ],
      declarations: [
        TenantsListComponent,
        ToolbarComponent,
        AvatarComponent,
        NetworkFunctionComponent,
        NetworkFunctionMenuComponent,
        BackButtonComponent,
        TableComponent
      ]
    })
    tenantsRoutingModule = new TenantsRoutingModule();
    ngZone = TestBed.inject(NgZone);
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
  });

  it('should create an instance', () => {
    expect(TenantsRoutingModule).toBeTruthy();
  });

  it('should navigate to tenant edit', fakeAsync(() => {
    /**
     * TODO(anton): workaround for the current version of NgZone for the following warn
     *
     * WARN: 'Navigation triggered outside Angular zone, did you forget to call 'ngZone.run()'?'
     *
     * Could be fixed in future then remove.
     *
     * https://github.com/angular/angular/issues/25837
     */

    ngZone.run(() => {
      router.navigate([-1]);
      tick();
      expect(location.path()).toBe('/-1');
    });
  }));
});
