import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';

import { SharedModule } from '../shared/shared.module';
import { PipelineModule } from '../pipeline/pipeline.module';

import { TenantsRoutingModule } from './tenants-routing.module';

import { TenantsListComponent } from './tenants-list.component';
import { EditTenantComponent } from './edit-tenant/edit-tenant.component';
import { EditTenantDialogComponent } from './edit-tenant-dialog/edit-tenant-dialog.component';

@NgModule({
  declarations: [
    TenantsListComponent,
    EditTenantComponent,
    EditTenantDialogComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSortModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    SharedModule,
    PipelineModule,
    TenantsRoutingModule
  ]
})
export class TenantsModule { }
