import { cloneDeep } from 'lodash-es';
import { map, catchError, take } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscription, of } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';
import { TenantService, Tenant, TenantStats } from 'src/app/tenant/tenant.service';
import { SortDirection } from '@angular/material/sort';
import { TableActions } from '../shared/table/table.component';
import { TenantRow } from '../shared/table/row-types';
import { TableDataSource } from '../shared/table/table.component';
import { Tenants } from 'rest_client/heimdallr/model/tenants';

const Config = {
  POLL_INTERVAL: 5000,
  DURATION: 1,
  STEP: 2
};

export const ROLES = {
  cluster: {
    admin: 'Site Admin',
    user: 'Site User'
  },
  system: {
    admin: 'System Admin',
    user: 'System User'
  },
  tenant: {
    admin: 'Tenant Admin',
    user: 'Tenant User'
  }
};

const DEFAULT_PAGE_SIZE = 50;

export class TenantsDataSource implements TableDataSource<TenantRow> {

  private _tenantRows$ = new BehaviorSubject<TenantRow[]>([]);
  private _loading$ = new BehaviorSubject<boolean>(false);
  private _tenantsSubscription: Subscription;
  private _statStreamSubscriptions = new Array<Subscription>();
  private _total: number;
  private _tenants: Tenant[];
  private _rows: TenantRow[];
  private _index = 0;
  private _pageSize = DEFAULT_PAGE_SIZE;

  constructor(
    private _tenantService: TenantService
  ) { }

  public connect(collectionViewer: CollectionViewer): Observable<TenantRow[]> {
    return this._tenantRows$.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    if (this._tenantsSubscription) {
      this._tenantsSubscription.unsubscribe();
    }
    this._tenantRows$.complete();
    this._loading$.complete();
    this.disableTenantStatStream();
  }

  public fetch(index?: number, pageSize?: number, sortColumn?: string, sortDirection?: SortDirection) {
    this._index = index ?? this._index;
    this._pageSize = pageSize ?? this._pageSize;
    this._loading$.next(true);

    if (this._tenantsSubscription) {
      this._tenantsSubscription.unsubscribe();
      this.disableTenantStatStream();
    }

    // sort column and direction should be defined together
    sortColumn = sortDirection ? sortColumn : undefined;

    this._tenantsSubscription = this._tenantService
      .getTenants(this._index, this._pageSize, sortColumn, sortDirection)
      .pipe(
        map(({ tenants, metadata }: Tenants): TenantRow[] => {
          this._total = metadata?.total;
          this._tenants = tenants;
          return tenants.map((tenant: Tenant): TenantRow => this.getTenantRow(tenant));
        }),
        catchError(() => {
          this._loading$.next(false);
          return of([]);
        })
      ).subscribe((tenantRows: TenantRow[]) => {
        this._rows = tenantRows;
        this._tenantRows$.next(tenantRows);
        this.enableTenantStatStream();
        this._loading$.next(false);
      });
  }

  private enableTenantStatStream() {
    this._tenants?.forEach((tenant: Tenant) => {
      this._statStreamSubscriptions.push(this._tenantService.streamTenantStats(
        tenant.identifier,
        Config.POLL_INTERVAL,
        undefined,
        Config.DURATION,
        Config.STEP
      ).subscribe((stats: TenantStats) => {
        tenant.stats = stats;
        const index: number = this._tenants.findIndex(
          (t: Tenant): boolean => tenant.identifier === t.identifier
        );
        this._rows[index] = this.getTenantRow(tenant);
        this._tenantRows$.next(this._rows);
      }));
    });
  }

  private disableTenantStatStream() {
    this._statStreamSubscriptions.forEach((statStreamSubscription: Subscription) => {
      statStreamSubscription.unsubscribe();
    });

    this._statStreamSubscriptions.length = 0;
  }

  private getTenantRow({ identifier, name, stats }: Tenant): TenantRow {
    const services: number = stats ? stats.services : undefined;
    return {
      id: identifier,
      name,
      services,
      actions: [
        {
          action: TableActions.Edit,
          icon: 'edit',
          text: 'Edit'
        },
        {
          action: TableActions.Remove,
          icon: 'trash-blue',
          text: 'Remove'
        }
      ]
    } as TenantRow;
  }

  public update(editedTenant: Tenant) {
    const index: number = this._tenants.findIndex(
      (tenant: Tenant): boolean => tenant.identifier === editedTenant.identifier
    );

    const stats = this._tenants[index].stats;
    this._tenants[index] = cloneDeep(editedTenant);
    this._tenants[index].stats = stats;
    this._rows[index] = this.getTenantRow(this._tenants[index]);
    this._tenantRows$.next(this._rows);
  }

  public remove(id: string) {
    const index: number = this._tenants.findIndex(
      (tenant: Tenant): boolean => tenant.identifier === id
    );
    this._tenants.splice(index, 1);
    this._rows.splice(index, 1);
    this._tenantRows$.next(this._rows);
    this._total -= 1;
  }

  public set total(count: number) {
    this._total = count;
  }

  public get total(): number {
    return this._total;
  }

  public get loading$(): Observable<boolean> {
    return this._loading$.asObservable();
  }

  public get length(): number {
    return this._tenantRows$.value.length;
  }

  public get tenants(): Tenant[] {
    return this._tenants;
  }

  public get rows(): TenantRow[] {
    return this._rows;
  }
}
