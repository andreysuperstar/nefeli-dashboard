import { By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { of } from 'rxjs';
import { environment } from '../../../environments/environment';
import { routes } from '../../home/home-routing.module';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { NetworkFunction, NFCatalog } from '../../pipeline/network-function.model';
import { AlertService } from '../../shared/alert.service';
import { ClusterService, Cluster } from '../../home/cluster.service';
import { TenantService, Tenant } from '../../tenant/tenant.service';
import { NetworkFunctionService } from '../../pipeline/network-function.service';
import { ToolbarComponent } from '../../toolbar/toolbar.component';
import { AvatarComponent } from '../../shared/avatar/avatar.component';
import { HomeComponent } from '../../home/home.component';
import { OverviewComponent } from '../../overview/overview.component';
import { TableComponent } from '../../shared/table/table.component';
import { EditTenantComponent } from './edit-tenant.component';
import { NfType } from 'src/app/pipeline/pipeline.model';
import { MatTooltipModule } from '@angular/material/tooltip';

xdescribe('EditTenantComponent', () => {
  let component: EditTenantComponent;
  let fixture: ComponentFixture<EditTenantComponent>;
  let el: HTMLElement;
  let mockServer: HttpTestingController;
  let loader: HarnessLoader;
  let tenantService: TenantService;
  let nFService: NetworkFunctionService;

  const mockTenant: Tenant = {
    identifier: '-1',
    name: 'Coffee'
  };

  const mockAvailableNFs: any = {
    "pan": {
      "name": "Firewall-1",
      "vendor": "pan",
      "id": "pan-fw",
      "type": "bess",
      "kpps_per_core": 5300,
      "scaling_enabled": true,
      "metadata_enabled": true
    }
  };

  const mockNF: NetworkFunction = {
    name: 'Filter NF',
    type: NfType.NATIVE,
    controllerType: "NF_CONTROLLER",
    components: undefined,
    local: {
      id: '-1',
      capability: undefined,
      vendor: "",
      catalogId: ""
    }
  };

  const mockClusters: Cluster[] = [
    {
      "id": "-1",
      "name": "Cluster B",
    },
    {
      "id": "-2",
      "name": "Cluster B",
    }
  ];

  const mockClusterNFs: NFCatalog = {
    "pan": [
      {
        local: {
          id: "pan-fw",
          capability: undefined,
          vendor: "",
          catalogId: "",
          tenant: {
            identifier: "-1",
            name: "Coffee"
          }
        },
        name: "Firewall-1",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: undefined,
      },
      {
        local: {
          id: "pan-bw",
          capability: undefined,
          vendor: "",
          catalogId: ""
        },
        name: "Firewall-2",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: undefined,
      }
    ],
    "f5": [
      {
        local: {
          id: "f5-bigip",
          capability: undefined,
          vendor: "",
          catalogId: ""
        },
        name: "Big IP",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: undefined,
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        ReactiveFormsModule,
        OverlayModule,
        MatSnackBarModule,
        MatDividerModule,
        MatTableModule,
        MatSelectModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatCardModule,
        MatInputModule,
        MatBadgeModule,
        MatListModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        HomeComponent,
        OverviewComponent,
        ToolbarComponent,
        AvatarComponent,
        TableComponent,
        EditTenantComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({}),
            paramMap: of({
              get: () => '-1'
            })
          }
        },
        AlertService,
        ClusterService,
        TenantService,
        NetworkFunctionService
      ]
    });

    mockServer = TestBed.inject(HttpTestingController);
    tenantService = TestBed.inject(TenantService);
    nFService = TestBed.inject(NetworkFunctionService);
  });

  beforeEach(waitForAsync(() => {
    fixture = TestBed.createComponent(EditTenantComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;

    const req = mockServer.expectOne(`${environment.restServer}/auth/tenants/-1`);
    expect(req.request.responseType).toBe('json');
    req.flush(mockTenant);

    const nFsReq = mockServer.match(`${environment.restServer}/tenants/-1/nfs`);
    nFsReq.forEach((req) => {
      req.flush(mockAvailableNFs);
    })
    const clusterReq = mockServer.expectOne(`${environment.restServer}/v1/sites/`);
    clusterReq.flush(mockClusters);
    const cluster1NFsReq = mockServer.expectOne(`${environment.restServer}/v1/nfs`);
    const cluster2NFsReq = mockServer.expectOne(`${environment.restServer}/v1/nfs`);
    cluster1NFsReq.flush(mockClusterNFs);
    cluster2NFsReq.flush(mockClusterNFs);
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have return link', () => {
    const returnLink: HTMLLinkElement = el.querySelector('.back');
    expect(returnLink).toBeDefined();
    expect(returnLink.href).toMatch(/\/tenants-list$/);
  });

  it('should have open tenant button', () => {
    const openButton: HTMLButtonElement = el.querySelector('nav a[mat-flat-button]');
    expect(openButton).toBeDefined();
    expect(openButton.textContent.trim()).toBe('Open Tenant');
  });

  it('should render Tenant name in the title', () => {
    const tenantName: string = component.tenantForm.value.name;
    expect(tenantName).toBe('Coffee');
    expect(component.tenant.name).toBe('Coffee');
  });

  it('should update tenant name with PUT request and revert to original value on fail', () => {
    component.tenantForm.setValue({ name: 'Test' });
    component.submitTenantForm();
    const req = mockServer.expectOne(`${environment.restServer}/auth/tenants/-1`);
    expect(req.request.method).toBe('PUT');
    const mockErrorResponseOptions: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };
    req.flush({}, mockErrorResponseOptions);
    expect(component.tenantForm.value.name).toBe('Coffee');
  });

  it('should show "please select nf" message', () => {
    expect(el.querySelector('.content mat-card').textContent.trim()).toBe('Please select Network Function');
  });

  it('should show selected nf in the edit form', () => {
    component.selectedNF = mockNF;
    fixture.detectChanges();

    expect(el.querySelector('.content .mat-card-title').textContent.trim()).toBe('Edit Filter NF');
  });

  it('should remove NF from tenant', async () => {
    component.selectedNF = mockNF;
    fixture.detectChanges();

    const removeButton = await loader.getHarness(MatButtonHarness);

    await removeButton.click();

    const nfFormDe = fixture.debugElement.query(By.css('form[id="nf-form"]'));

    console.log(nfFormDe);
  });

  it('should get available NF system catalog', () => {
    spyOn<any>(nFService, 'getAvailableNFs').and.callThrough();
    component['getAvailableNFs'](mockTenant.identifier);
    expect(nFService['getAvailableNFs']).toHaveBeenCalledWith(mockTenant.identifier);
  });

  it('should be able to find vendor by name in the catalog', () => {
    let vendor = component['findVendorByName'](component.availableCatalog, 'pan');
    expect(vendor).toBe('pan');
    vendor = component['findVendorByName'](component.availableCatalog, 'pAn');
    expect(vendor).toBe('pan');
  });

});
