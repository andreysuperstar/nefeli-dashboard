import { UserService } from 'src/app/users/user.service';
import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Subscription, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { cloneDeep } from 'lodash-es';

import { NetworkFunction, NFCatalog } from '../../pipeline/network-function.model';

import { AlertService, AlertType } from '../../shared/alert.service';
import { TenantService, Tenant } from '../../tenant/tenant.service';
import { NetworkFunctionService } from '../../pipeline/network-function.service';

@Component({
  selector: 'nef-edit-tenant',
  templateUrl: './edit-tenant.component.html',
  styleUrls: ['./edit-tenant.component.less']
})
export class EditTenantComponent implements OnInit, OnDestroy {

  private _tenantForm: FormGroup;
  private _nFForm: FormGroup;
  private _tenant: Tenant;
  private _selectedNF: NetworkFunction;
  private _availableCatalog: NFCatalog;
  private _activatedCatalog: NFCatalog;
  private _tenantSubscription: Subscription;
  private _tenantNFSubscription: Subscription;
  private _subscription = new Subscription();

  @ViewChild('vendor') private _vendor: ElementRef<HTMLInputElement>;

  constructor(
    private _cd: ChangeDetectorRef,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _tenantService: TenantService,
    private _nFService: NetworkFunctionService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this.initTenantForm();
    this.initNFForm();

    const subscription = this._route.paramMap
      .pipe(
        map((data: ParamMap) => data.get('id')),
        switchMap((id: string) => this._tenantService.getTenant(id))
      )
      .subscribe((tenant: Tenant) => {
        this._tenant = tenant;

        this.tenantForm.setValue({
          name: tenant.name
        });

        if (tenant) {
          this.getAvailableNFs(tenant.identifier);
          this.getActivatedNFs(tenant.identifier);
        }
      });

    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    if (this._tenantSubscription) {
      this._tenantSubscription.unsubscribe();
    }

    if (this._tenantNFSubscription) {
      this._tenantNFSubscription.unsubscribe();
    }

    this._subscription.unsubscribe();
  }

  private getAvailableNFs(tenantId: string) {
    /* TODO(ecarino): re-enable once we support tenant-level NFs
    const subscription = this._nFService
      .getAvailableNFs(tenantId)
      .subscribe((catalog: NFCatalog) => this._availableCatalog = catalog);

    this._subscription.add(subscription);
    */
  }

  private getActivatedNFs(tenantId: string) {
    const subscription = this._tenantService
      .getNfCatalog(tenantId)
      .subscribe((catalog: NFCatalog) => this._activatedCatalog = catalog);

    this._subscription.add(subscription);
  }

  private initTenantForm() {
    this._tenantForm = this._fb.group({
      name: ['', Validators.required]
    });
  }

  private initNFForm() {
    this._nFForm = this._fb.group({
      vendor: [''],
      name: ['', Validators.required],
      controlImage: [''],
      controlManifest: [''],
      dataImage: [''],
      dataManifest: ['']
    });
  }

  public submitTenantForm() {
    const { value: { name }, invalid, pristine } = this.tenantForm;

    if (invalid || pristine) {
      return;
    }

    if (this._tenantSubscription) {
      this._tenantSubscription.unsubscribe();
    }

    const tenant: Tenant = cloneDeep(this._tenant);
    tenant.name = name;

    this._tenantSubscription = this._tenantService
      .editTenant(this._tenant)
      .subscribe(
        (_: string) => {
          this._tenant.name = tenant.name;

          this._alertService.info(AlertType.INFO_EDIT_TENANT_SUCCESS);
        },
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_EDIT_TENANT));
  }

  public onEditNF(nF: NetworkFunction) {
    this._selectedNF = nF;

    // used to run change detection to make sure vendor ElementRef is defined
    this._cd.detectChanges();
    this._vendor.nativeElement.focus();
  }

  public onRemoveNF() {
    this._selectedNF = undefined;

    this._nFForm.reset();
  }

  private findVendorByName(catalog: NFCatalog, vendorName: string): string {
    return Object
      .keys(catalog)
      .find((catalogKey: string): boolean =>
        catalogKey.toLowerCase() === vendorName.toLowerCase()
      );
  }

  public get tenantForm(): FormGroup {
    return this._tenantForm;
  }

  public get nFForm(): FormGroup {
    return this._nFForm;
  }

  public get tenant(): Tenant {
    return this._tenant;
  }

  public get selectedNF(): NetworkFunction {
    return this._selectedNF;
  }

  public set selectedNF(nF: NetworkFunction) {
    this._selectedNF = nF;
    this.nFForm.patchValue({
      name: nF.name,
      vendor: nF.local.vendor
    });
  }

  public get availableCatalog(): NFCatalog {
    return this._availableCatalog;
  }

  public get activatedCatalog(): NFCatalog {
    return this._activatedCatalog;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
