import { UserService } from 'src/app/users/user.service';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DecimalPipe, DOCUMENT } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TestRequest, HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { cloneDeep } from 'lodash-es';

import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { AlertService } from 'src/app/shared/alert.service';
import { STATUS, TenantStats, Tenant, TenantService } from '../tenant/tenant.service';

import { TenantsListComponent } from './tenants-list.component';
import { TableComponent, TableActions, TableActionEvent } from 'src/app/shared/table/table.component';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { EditTenantDialogComponent } from './edit-tenant-dialog/edit-tenant-dialog.component';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { User } from 'rest_client/heimdallr/model/user';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { TenantStatsRequest } from 'rest_client/pangolin/model/tenantStatsRequest';

describe('TenantsListComponent', () => {
  let component: TenantsListComponent;
  let fixture: ComponentFixture<TenantsListComponent>;
  let httpTestingController: HttpTestingController;
  let document: Document;
  let tableDe: DebugElement;
  let el: HTMLElement;
  let userService: UserService;
  let loader: HarnessLoader;

  enum UserType {
    SystemAdmin,
    SystemUser
  }

  const mockUsers: User[] = [
    {
      username: 'SystemAdmin',
      email: 'sysadmin@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    },
    {
      username: 'SystemUser',
      email: 'sysuser@nefeli.io',
      roles: {
        scope: 'system',
        id: '-3',
        roles: ['user']
      }
    }
  ];

  const mockTenants: Tenant[] = [
    {
      identifier: '-1',
      name: 'Tenant A'
    },
    {
      identifier: '-2',
      name: 'Tenant B'
    }
  ];

  const mockTenantsResponse: Tenants = {
    tenants: mockTenants,
    metadata: {
      count: 2,
      filter: [],
      search: [],
      sort: [],
      total: 2
    }
  };

  const mockStats1: TenantStats = {
    id: '1',
    name: 'customer0_pid4',
    services: 2,
    throughputOut: 0,
    throughputIn: 0,
    packetLoss: [],
    latency: [],
    coresUsed: 0,
    coresTotal: 0,
    memUsed: 0,
    memTotal: 0,
    throughputServiceOut: [],
    throughputServiceIn: [],
    capacity: 0,
    status: STATUS.GOOD
  }

  const mockStats2: TenantStats = {
    id: '2',
    name: 'ddp_pid2',
    services: 5,
    throughputOut: 0,
    throughputIn: 0,
    packetLoss: [],
    latency: [],
    coresUsed: 0,
    coresTotal: 0,
    memUsed: 0,
    memTotal: 0,
    throughputServiceOut: [],
    throughputServiceIn: [],
    capacity: 0,
    status: STATUS.GOOD
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        MatDialogModule,
        MatCardModule,
        MatInputModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        TenantsListComponent,
        TableComponent,
        AvatarComponent,
        EditTenantDialogComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        DecimalPipe,
        AlertService,
        TenantService,
        UserService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(TenantsListComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants?index=0&limit=50`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    const clonedMockTenantsResponse: Tenants = cloneDeep(mockTenantsResponse);
    req.flush(clonedMockTenantsResponse);

    const req1 = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/stats`);
    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-2/stats`);
    req1.flush(mockStats1);
    req2.flush(mockStats2);

    const body: TenantStatsRequest = {
      services: undefined,
      filter: {
        filter: []
      },
      range: {
        start: undefined,
        duration: 1,
        step: 2
      }
    };
    expect(req1.request.body).toEqual(body);
    expect(req2.request.body).toEqual(body);

    fixture.detectChanges();

    tableDe = fixture.debugElement.query(By.css('nef-table'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page heading', () => {
    expect(el.querySelector('header h2').textContent).toBe('Tenants');
  });

  it('should render \'Add New Tenant\' link', () => {
    expect(el.querySelector('header button.add span').textContent.trim()).toBe('Add New Tenant');
  });

  it('should render \'Add New Tenant\' only for System admin', () => {
    let addButton: HTMLButtonElement = el.querySelector('header button.add');
    expect(addButton).not.toBeNull();

    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    addButton = el.querySelector('header button.add');
    expect(addButton).toBeNull();
  });

  it('should open dialog window on \'Add New Tenant\' button click', () => {
    const addButton: HTMLButtonElement = el.querySelector('button.add');
    addButton.click();
    fixture.detectChanges();
    expect(el.querySelector('nef-edit-tenant-dialog')).toBeDefined();
  });

  it('should render tenants table', () => {
    expect(el.querySelectorAll('nef-table')).toBeTruthy();
    expect(el.querySelectorAll('nef-table .mat-header-cell').length).toBe(3);
  });

  it('should render tenants', () => {
    expect(component.dataSource.length).toBe(2);
    expect(el.querySelectorAll('nef-table .mat-row').length).toBe(2);
    const cells = el.querySelectorAll('nef-table .mat-row .mat-cell');
    expect(cells.length).toBe(6);
    expect(cells[0].textContent.trim()).toBe('Tenant A');
    expect(cells[1].textContent.trim()).toBe('2');
    expect(cells[3].textContent.trim()).toBe('Tenant B');
    expect(cells[4].textContent.trim()).toBe('5');
  });

  it('should have actions column in the tenants table', () => {
    const cellEls = el.querySelectorAll('nef-table .mat-row .mat-column-actions');

    expect(cellEls.length).toBe(2);

    const buttonEls = cellEls[0].querySelectorAll('button.action');

    expect(buttonEls.length).toBe(2);

    expect(buttonEls[0].textContent).toBe('Edit');
    expect(buttonEls[1].textContent).toBe('Remove');
  });

  it('should hide actions column for system user', () => {
    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    const cellEls = fixture.debugElement.nativeElement.querySelectorAll('nef-table .mat-row .mat-column-actions');
    expect(cellEls.length).toBe(0);
  });

  it('should dispatch action events', () => {
    const editEvent: TableActionEvent = {
      type: TableActions.Edit,
      data: {},
      event: new MouseEvent('mouseclick', {})
    };

    const removeEvent: TableActionEvent = {
      type: TableActions.Remove,
      data: {},
      event: new MouseEvent('mouseclick', {})
    };

    spyOn<any>(component, 'editTenant');
    spyOn<any>(component, 'removeTenant');

    component.onActionClick(editEvent);
    expect(component['editTenant']).toHaveBeenCalled();

    component.onActionClick(removeEvent);
    expect(component['removeTenant']).toHaveBeenCalled();
  });

  it('should be able to edit tenant', async () => {
    let rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));
    let tenantNameDe: DebugElement = rowDes[0].query(By.css('.mat-column-name'));

    expect(tenantNameDe.nativeElement.textContent).toBe(
      mockTenants[0].name,
      'render tenant name'
    );

    const editButtonDe = rowDes[0].queryAll(By.css('button.edit'))[0];

    spyOn<any>(component, 'editTenant').and.callThrough();

    editButtonDe.nativeElement.click();

    expect(component['editTenant']).toHaveBeenCalled();

    const dialogRef: MatDialogRef<
      EditTenantDialogComponent,
      Tenant
    > = component['_dialog'].openDialogs[0];

    spyOn(component['_dataSource'], 'update').and.callThrough();

    const editedTenant = {
      ...mockTenants[0],
      name: 'Tenant C'
    };

    dialogRef.close(editedTenant);

    await dialogRef
      .afterClosed()
      .toPromise();

    expect(component['_dataSource'].update).toHaveBeenCalledWith(editedTenant);

    fixture.detectChanges();

    rowDes = tableDe.queryAll(By.css('mat-row'));
    tenantNameDe = rowDes[0].query(By.css('.mat-column-name'));

    expect(tenantNameDe.nativeElement.textContent).toBe(
      editedTenant.name,
      'render edited tenant name'
    );
  });

  it('should remove tenant', () => {
    let rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    // save tenant row name to confirm it's removed later
    const tenantName: string = rowDes[0].query(
      By.css('.mat-column-name')
    ).nativeElement.textContent;

    const removeButtonEl: HTMLButtonElement = rowDes[0].query(
      By.css('button.remove')
    ).nativeElement;

    expect(rowDes.length).toBe(
      mockTenants.length,
      `render ${mockTenants.length} tenant rows`
    );

    expect(removeButtonEl).not.toBeNull('render Remove button');
    expect(removeButtonEl.textContent).toBe('Remove', 'render Remove button text');

    spyOn<any>(component, 'removeTenant').and.callThrough();

    removeButtonEl.click();
    expect(component['removeTenant']).toHaveBeenCalled();

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = component['_dialog'].openDialogs[0];
    dialogRef.afterOpened().subscribe(() => {
      const dialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
      const descriptionEls: NodeListOf<HTMLParagraphElement> = dialogEl.querySelectorAll('p');
      const removeTenantButtonEl: HTMLButtonElement = dialogEl.querySelectorAll('button')[1];
      expect(descriptionEls[0].textContent).toBe(
        `This will remove the tenant ${mockTenants[0].name}.`,
        'render dialog description'
      );
      expect(removeTenantButtonEl.textContent).toBe(
        'Remove Tenant',
        'render \'Remove Tenant\' button text'
      );
      removeTenantButtonEl.click();
    });

    dialogRef.afterClosed().subscribe(() => {
      const request: TestRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/auth/tenants/${mockTenants[0].identifier}`
      });
      request.flush({});

      fixture.detectChanges();
      rowDes = tableDe.queryAll(By.css('mat-row'));
      expect(rowDes.length).toEqual(
        mockTenants.length - 1,
        'render tenant rows without removed tenant'
      );
      const shiftedTenantRowName: string = rowDes[0].query(
        By.css('.mat-column-name')
      ).nativeElement.textContent;

      expect(shiftedTenantRowName).not.toBe(tenantName, 'render shifted tenant row name');
    });
  });

  it('should request sorted list', () => {
    const column: DebugElement = tableDe.query(By.css('mat-header-row .mat-sort-header-container'));
    column.nativeElement.click();
    fixture.detectChanges();
    const req = httpTestingController.expectOne({
      url: `${environment.restServer}/auth/tenants?sort=asc(name)&index=0&limit=50`,
      method: 'GET'
    });
    req.flush({});
  });
});
