import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';

import { Subscription, Observable } from 'rxjs';
import { cloneDeep } from 'lodash-es';

import { Column, ColumnType, TableActions, TableActionEvent } from '../shared/table/table.component';

import { AlertService, AlertType } from '../shared/alert.service';
import { TenantService, Tenant } from '../tenant/tenant.service';

import { EditTenantDialogComponent, TenantDialogData } from './edit-tenant-dialog/edit-tenant-dialog.component';
import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { UserService } from '../users/user.service';
import { filter, switchMap } from 'rxjs/operators';
import { TenantsDataSource } from './tenants.data-source';
import { TenantRow } from '../shared/table/row-types';

enum ResponseError {
  TENANT_HAS_ACTIVE_USERS = 'tenant has active users'
}

const COLUMNS: Column[] = [
  {
    name: 'name',
    sortable: true
  },
  {
    name: 'services',
    sortable: false
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS,
    sortable: false
  }
];

const DIALOG_CONFIG: MatDialogConfig<TenantDialogData | ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-tenants-list',
  templateUrl: './tenants-list.component.html',
  styleUrls: ['./tenants-list.component.less']
})
export class TenantsListComponent implements OnInit, OnDestroy {

  private _dataSource: TenantsDataSource;
  private _dialogSubscription: Subscription;

  constructor(
    private _dialog: MatDialog,
    private _alertService: AlertService,
    private _tenantService: TenantService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this._dataSource = new TenantsDataSource(this._tenantService);
    this._dataSource.fetch();
  }

  public ngOnDestroy() {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }
  }

  public onAddTenant() {
    const dialogRef: MatDialogRef<EditTenantDialogComponent, Tenant> = this._dialog.open(
      EditTenantDialogComponent,
      DIALOG_CONFIG
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .subscribe((tenant: Tenant) => {
        if (tenant) {
          this._dataSource.total += 1;
          this._dataSource.fetch();
        }
      });
  }

  private editTenant({ id }: TenantRow) {
    const editableTenant: Tenant = this._dataSource.tenants.find(
      (tenant: Tenant): boolean => tenant.identifier === id
    );

    const data: TenantDialogData = {
      tenant: cloneDeep(editableTenant)
    };

    const dialogRef: MatDialogRef<EditTenantDialogComponent, Tenant> = this._dialog.open(
      EditTenantDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((editedTenant: Tenant) => {
        this._dataSource.update(editedTenant);
      });
  }

  private removeTenant({ id, name }: TenantRow) {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: `This will remove the tenant <strong>${name}</strong>.`,
      action: 'Remove Tenant'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef.afterClosed().pipe(
      filter(Boolean),
      switchMap((_: boolean) => this._tenantService.removeTenant(id))
    ).subscribe(
      () => {
        this._dataSource.remove(id);
        this._alertService.info(AlertType.INFO_REMOVE_TENANT_SUCCESS);
      },
      (error: HttpErrorResponse) => {
        if (error.error.message.trim() === ResponseError.TENANT_HAS_ACTIVE_USERS) {
          this._alertService.error(AlertType.ERROR_REMOVE_TENANT_ACTIVE_USERS);
        } else {
          this._alertService.error(AlertType.ERROR_REMOVE_TENANT);
        }
      }
    );
  }

  public onActionClick(event: TableActionEvent) {
    switch (event.type) {
      case TableActions.Edit:
        this.editTenant(event.data);
        break;
      case TableActions.Remove:
        this.removeTenant(event.data);
        break;
    }
  }

  public get columns(): Column[] {
    return COLUMNS;
  }

  public get dataSource(): TenantsDataSource {
    return this._dataSource;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
