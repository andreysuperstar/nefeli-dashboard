import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormControl } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from '../../../environments/environment';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { AlertService } from 'src/app/shared/alert.service';
import { TenantService, Tenant } from '../../tenant/tenant.service';

import { EditTenantDialogComponent } from './edit-tenant-dialog.component';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';

describe('EditTenantDialogComponent', () => {
  let component: EditTenantDialogComponent;
  let fixture: ComponentFixture<EditTenantDialogComponent>;
  let httpTestingController: HttpTestingController;
  let tenantService: TenantService;

  const dialogSpy = jasmine.createSpyObj<MatDialogRef<EditTenantDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  const mockTenant: Tenant = {
    identifier: '4',
    name: 'Nefeli Networks'
  };

  const mockDialogData = {
    tenant: mockTenant
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatCardModule,
        MatInputModule
      ],
      declarations: [EditTenantDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: undefined
        },
        AlertService,
        TenantService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    tenantService = TestBed.inject(TenantService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTenantDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogSpy.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', () => {
    expect(
      fixture.debugElement.query(By.css('h1')).nativeElement.textContent
    ).toBe('Add New Tenant');

    component['_data'] = mockDialogData;
    component.ngOnInit();
    fixture.detectChanges();

    expect(
      fixture.debugElement.query(By.css('h1')).nativeElement.textContent
    ).toBe('Edit Tenant');
  });

  it('should render tenant form', () => {
    expect(fixture.debugElement.query(By.css('#tenant-form'))).not.toBeNull();
  });

  it('should validate name field to be required', () => {
    const nameControl: FormControl = component.tenantForm.get('name') as FormControl;

    expect(nameControl.invalid).toBeTruthy();

    component.tenantForm.patchValue({
      name: 'Nefeli Networks'
    });

    expect(nameControl.valid).toBeTruthy();
  });

  it('should create tenant', () => {
    spyOn(tenantService, 'createTenant').and.callThrough();

    component.submitTenantForm();

    expect(tenantService.createTenant).not.toHaveBeenCalled();

    const nameInputEl: HTMLInputElement = fixture.debugElement.query(
      By.css('input[formcontrolname="name"]')
    ).nativeElement;

    const mockTenantName = 'Nefeli Networks';

    nameInputEl.value = mockTenantName;
    nameInputEl.dispatchEvent(new Event('input'));
    component.tenantForm.markAsDirty();
    fixture.detectChanges();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[form="tenant-form"]')
    ).nativeElement;
    submitButtonEl.click();
    fixture.detectChanges();
    expect(tenantService.createTenant).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants`
    );

    expect(request.request.method).toBe('POST');

    request.flush({});

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should edit tenant', () => {
    component['_data'] = mockDialogData;
    component.ngOnInit();
    fixture.detectChanges();

    spyOn(tenantService, 'editTenant').and.callThrough();

    component.submitTenantForm();

    expect(tenantService.editTenant).not.toHaveBeenCalled();

    const nameInputEl: HTMLInputElement = fixture.debugElement.query(
      By.css('input[formcontrolname="name"')
    ).nativeElement;

    const mockTenantName = 'Tenant 4';

    nameInputEl.value = mockTenantName;
    nameInputEl.dispatchEvent(new Event('input'));
    component.tenantForm.markAsDirty();
    fixture.detectChanges();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[form="tenant-form"]')
    ).nativeElement;
    submitButtonEl.click();
    fixture.detectChanges();

    expect(tenantService.editTenant).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants/${mockTenant.identifier}`
    );

    expect(request.request.method).toBe('PUT');

    request.flush({});

    expect(request.request.body.name).toBe(mockTenantName, 'valid updated tenant name');

    expect(dialogSpy.close).toHaveBeenCalled();
  });

});
