import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Subscription } from 'rxjs';
import { cloneDeep } from 'lodash-es';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { TenantService, Tenant } from '../../tenant/tenant.service';

export interface TenantDialogData {
  tenant: Tenant;
}

const MIN_TENANT_NAME_LENGTH = 1;

@Component({
  selector: 'nef-edit-tenant-dialog',
  templateUrl: './edit-tenant-dialog.component.html',
  styleUrls: ['./edit-tenant-dialog.component.less']
})
export class EditTenantDialogComponent implements OnInit, OnDestroy {

  private _tenant: Tenant;
  private _tenantForm: FormGroup;
  private _tenantSubscription: Subscription;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<EditTenantDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: TenantDialogData,
    private _alertService: AlertService,
    private _tenantService: TenantService
  ) { }

  public ngOnInit() {
    if (this._data) {
      this._tenant = cloneDeep(this._data.tenant);
    }

    this.initForm();
  }

  public ngOnDestroy() {
    if (this._tenantSubscription) {
      this._tenantSubscription.unsubscribe();
    }
  }

  private initForm() {
    const name: string = this._tenant && this._tenant.name;

    this._tenantForm = this._fb.group({
      name: [
        name,
        [
          Validators.required,
          Validators.minLength(MIN_TENANT_NAME_LENGTH)
        ]
      ]
    });
  }

  public submitTenantForm() {
    const { value: { name }, invalid, pristine } = this._tenantForm;

    if (invalid || pristine) {
      return;
    }

    if (this._tenantSubscription) {
      this._tenantSubscription.unsubscribe();
    }

    this._tenant ? this.editTenant(name) : this.createTenant(name);
  }

  private createTenant(name: string) {
    this._tenantSubscription = this._tenantService
      .createTenant(name)
      .subscribe(
        (tenant: Tenant) => {
          this._alertService.info(AlertType.INFO_CREATE_TENANT_SUCCESS);

          this._dialogRef.close(tenant);
        },
        ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_CREATE_TENANT, message);
        }
      );
  }

  private editTenant(name: string) {
    const tenant: Tenant = cloneDeep(this._tenant);
    tenant.name = name;

    this._tenantSubscription = this._tenantService
      .editTenant(tenant)
      .subscribe(
        (_: string) => {
          this._alertService.info(AlertType.INFO_EDIT_TENANT_SUCCESS);

          this._dialogRef.close(tenant);
        },
        ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_EDIT_TENANT, message);
        }
      );
  }

  public get tenant(): Tenant {
    return this._tenant;
  }

  public get tenantForm(): FormGroup {
    return this._tenantForm;
  }

}
