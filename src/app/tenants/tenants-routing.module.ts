import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TenantsListComponent } from './tenants-list.component';
import { EditTenantComponent } from './edit-tenant/edit-tenant.component';

export const routes: Routes = [
  {
    path: '',
    component: TenantsListComponent
  },
  {
    path: ':id',
    component: EditTenantComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenantsRoutingModule { }
