import { Pipe, PipeTransform } from '@angular/core';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';
import { TitleCasePipe } from '@angular/common';

@Pipe({
  name: 'alertclass'
})
export class AlertClassPipe extends TitleCasePipe implements PipeTransform {
  public transform(value: null | undefined): null;

  public transform(value: string | null | undefined): string | null;

  public transform(value: AlertClass): string {
    if (!value) {
      return '';
    }
    switch (value) {
      case AlertClass.NF: return 'NF';
      case AlertClass.ETCD: return 'ETCD';
      default: return super.transform(value.substring('alert_class_'.length).replace(/_+/, ' '));
    }
  }
}
