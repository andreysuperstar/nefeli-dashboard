import { DurationPipe } from './duration.pipe';

describe('DurationPipe', () => {
  let pipe: DurationPipe;

  const minute: number = 60;
  const hour: number = 60 * minute;
  const day: number = 24 * hour;
  const month: number = 30 * day;
  const year: number = 365 * day;

  beforeEach(() => {
    pipe = new DurationPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('convert minutes to a minute string', () => {
    expect(pipe.transform(minute)).toBe('Minute');
    expect(pipe.transform(15 * minute)).toBe('15 Minutes');
  });

  it('convert hours to an hour string', () => {
    expect(pipe.transform(hour)).toBe('Hour');
    expect(pipe.transform(6 * hour)).toBe('6 Hours');
  });

  it('convert days to a day string', () => {
    expect(pipe.transform(day)).toBe('Day');
    expect(pipe.transform(2 * day)).toBe('2 Days');
  });

  it('convert months to a month string', () => {
    expect(pipe.transform(month)).toBe('Month');
    expect(pipe.transform(5 * month)).toBe('5 Months');
  });

  it('convert years to a year string', () => {
    expect(pipe.transform(year)).toBe('Year');
    expect(pipe.transform(7 * year)).toBe('7 Years');
  });
});
