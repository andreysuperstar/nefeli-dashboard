import { ByteConverterPipe, ConverterUnit } from './byte-converter.pipe';

describe('ByteConverterPipe', () => {
  let pipe: ByteConverterPipe;

  beforeEach(() => {
    pipe = new ByteConverterPipe();
  });

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('convert bytes to gigabytes', () => {
    expect(pipe.transform(1000000000)).toBe(1);
    expect(pipe.transform(9000000000)).toBe(9);
  });

  it('convert bytes to megabytes', () => {
    expect(pipe.transform(1000000)).toBe(1);
    expect(pipe.transform(100000000)).toBe(100);
  });

  it('convert bytes to kilobytes', () => {
    expect(pipe.transform(1000)).toBe(1);
    expect(pipe.transform(99999)).toBe(100);
  });

  it('perform no conversion and remain at bytes', () => {
    expect(pipe.transform(999)).toBe(999);
  });

  it('convert bytes to gigabytes and append unit', () => {
    expect(pipe.transform(1000000000, true)).toBe('1 GB');
    expect(pipe.transform(999999999999, true)).toBe('1000 GB');
  });

  it('converts bits to terabits and append unit', () => {
    expect(pipe.transform(8000000000000, true, ConverterUnit.BITS)).toBe('8 Tb');
  });

  it('converts bits to gigabits', () => {
    expect(pipe.transform(8000000000, false, ConverterUnit.BITS)).toBe(8);
  });

  it('converts bits to megabits and append unit', () => {
    expect(pipe.transform(1000000, true, ConverterUnit.BITS)).toBe('1 Mb');
  });

  it('converts bits to kilobits', () => {
    expect(pipe.transform(1000, false, ConverterUnit.BITS)).toBe(1);
  });

  it('converts bits to bits and append unit', () => {
    expect(pipe.transform(999, true, ConverterUnit.BITS)).toBe('999 b');
  });

  it('converts bits to bits with proper fractional length', () => {
    expect(pipe.transform(678.0562643983617, true, ConverterUnit.BITS, 4)).toBe('678.0563 b');
  });
});
