import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'duration'
})
export class DurationPipe implements PipeTransform {

  public transform(value: number): string {
    moment.updateLocale('en-duration', {
      relativeTime: {
        past: '%s Ago',
        s: 'Few Seconds',
        ss: '%d Seconds',
        m: 'Minute',
        mm: '%d Minutes',
        h: 'Hour',
        hh: '%d Hours',
        d: 'Day',
        dd: '%d Days',
        M: 'Month',
        MM: '%d Months',
        y: 'Year',
        yy: '%d Years',
        future: 'in %s'
      }
    });

    moment.locale('en-duration');

    return moment
      .duration(value, 'seconds')
      .humanize();
  }
}
