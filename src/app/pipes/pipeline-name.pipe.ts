import { Pipe, PipeTransform } from '@angular/core';

enum LABEL {
  THROUGHPUT_OUT = 'throughputOut',
  THROUGHPUT_IN = 'throughputIn'
}

@Pipe({
  name: 'pipelineName'
})
export class PipelineNamePipe implements PipeTransform {

  public transform(value: string, args?: any): string {
    if (!value) {
      return '';
    }

    switch (value) {
      case LABEL.THROUGHPUT_OUT:
        return 'Total Out';
      case LABEL.THROUGHPUT_IN:
        return 'Total In';
      default:
        return value;
    }
  }
}
