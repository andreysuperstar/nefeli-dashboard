import { PipelineNamePipe } from './pipeline-name.pipe';

describe('PipelineNamePipe', () => {
  it('create an instance', () => {
    const pipe = new PipelineNamePipe();
    expect(pipe).toBeTruthy();
  });

  it('should rename throughput labels', () => {
    const pipe = new PipelineNamePipe();
    expect(pipe.transform('throughputOut')).toBe('Total Out');
    expect(pipe.transform('throughputIn')).toBe('Total In');
  });

  it('should unalter label', () => {
    const pipe = new PipelineNamePipe();
    expect(pipe.transform('do not transform')).toBe('do not transform');
  })
});
