import { AlertClassPipe } from './alert-class.pipe';
import { AlertClass } from 'rest_client/mneme/model/alertClass';

describe('AlertClassPipe', () => {
  it('create an instance', () => {
    const pipe = new AlertClassPipe();
    expect(pipe).toBeTruthy();
  });

  it('should convert AlertClass to human string', () => {
    const pipe = new AlertClassPipe();
    expect(pipe.transform(AlertClass.ALL)).toBe('All');
    expect(pipe.transform(AlertClass.LICENSEPOOL)).toBe('License Pool');
    expect(pipe.transform(AlertClass.AUTH)).toBe('Auth');
    expect(pipe.transform(AlertClass.CRUD)).toBe('Crud');
    expect(pipe.transform(AlertClass.MACHINE)).toBe('Machine');
    expect(pipe.transform(AlertClass.NF)).toBe('NF');
    expect(pipe.transform(AlertClass.PROCESS)).toBe('Process');
    expect(pipe.transform(AlertClass.SERVICE)).toBe('Service');
    expect(pipe.transform(AlertClass.SITE)).toBe('Site');
    expect(pipe.transform(AlertClass.USER)).toBe('User');
    expect(pipe.transform(AlertClass.ETCD)).toBe('ETCD');
    expect(pipe.transform(AlertClass.TEST)).toBe('Test');
    expect(pipe.transform(AlertClass.PERFORMANCE)).toBe('Performance');
    expect(pipe.transform(AlertClass.LICENSE)).toBe('License');
  });
});
