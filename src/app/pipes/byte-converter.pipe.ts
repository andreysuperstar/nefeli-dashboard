import { Pipe, PipeTransform } from '@angular/core';

enum Bytes {
  PER_GB = 1000000000,
  PER_MB = 1000000,
  PER_KB = 1000
}

const BytesMap = new Array(
  ['GB', Bytes.PER_GB],
  ['MB', Bytes.PER_MB],
  ['KB', Bytes.PER_KB]
);

enum Bits {
  PER_Tb = 1000000000000,
  PER_Gb = 1000000000,
  PER_Mb = 1000000,
  PER_Kb = 1000,
  PER_b = 1
}

const BitsMap = new Array(
  ['Tb', Bits.PER_Tb],
  ['Gb', Bits.PER_Gb],
  ['Mb', Bits.PER_Mb],
  ['kb', Bits.PER_Kb],
  ['b', Bits.PER_b]
);

export enum ConverterUnit {
  BITS,
  BYTES
}

@Pipe({
  name: 'converter'
})
export class ByteConverterPipe implements PipeTransform {

  public transform(value: any, appendUnit = false, unitType = ConverterUnit.BYTES, fractionalLength = 0): number | string {
    const convertedValue = this.convert(value, unitType, fractionalLength);
    return (appendUnit) ? `${convertedValue[0]} ${convertedValue[1]}` : convertedValue[0];
  }

  public convert(value: any, unitType = ConverterUnit.BYTES, fractionalLength = 0): [number, string] {
    let unitLabel = (unitType === ConverterUnit.BYTES) ? 'B' : 'b';
    const unitMap: Array<any> = (unitType === ConverterUnit.BYTES) ? BytesMap : BitsMap;
    const ten = 10;
    if (!value) {
      return [0, unitLabel];
    }

    for (const unit of unitMap) {
      if (value >= unit[1]) {
        value = (fractionalLength > 0) ?
          Math.round((value / unit[1]) * Math.pow(ten, fractionalLength)) / Math.pow(ten, fractionalLength)
          : Math.round(value / unit[1]);
        unitLabel = unit[0];
        break;
      }
    }

    return [value, unitLabel];
  }
}
