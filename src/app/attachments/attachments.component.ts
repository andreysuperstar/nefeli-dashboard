import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialogConfig, MatDialog, MatDialogRef } from '@angular/material/dialog';

import { forkJoin, Observable, Subscription } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';

import { cloneDeep } from 'lodash-es';

import { Column, ColumnType, TableActionButton, TableActionEvent, TableActions } from 'src/app/shared/table/table.component';

import { Site } from 'rest_client/pangolin/model/site';
import { Attachment } from 'rest_client/pangolin/model/attachment';
import { Attachments } from 'rest_client/pangolin/model/attachments';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { UserService } from '../users/user.service';
import { ClusterService } from 'src/app/home/cluster.service';
import { AlertType, AlertService } from 'src/app/shared/alert.service';

import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { AttachmentDialogData, AttachmentDialogComponent } from './attachment-dialog/attachment-dialog.component';

export interface AttachmentRow {
  attachment: string;
  site: string;
  description: string;
  vid: number;
  actions: Array<TableActionButton>;
}

export const COLUMNS: Column[] = [
  {
    name: 'attachment',
    label: 'name'
  },
  {
    name: 'site'
  },
  {
    name: 'description'
  },
  {
    name: 'vid',
    label: 'VLAN ID'
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS
  }
];

const DIALOG_CONFIG: MatDialogConfig<AttachmentDialogData | ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-attachments',
  templateUrl: './attachments.component.html',
  styleUrls: ['./attachments.component.less']
})
export class AttachmentsComponent implements OnInit, OnDestroy {

  private _attachments: Attachment[];
  private _dataSource: AttachmentRow[] = [];
  private _sites: Site[];
  private _dialogSubscription: Subscription;

  constructor(
    private _dialog: MatDialog,
    private _attachmentService: AttachmentsService,
    private _userService: UserService,
    private _siteService: ClusterService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this.getAttachmentsAndSites();
  }

  public ngOnDestroy() {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }
  }

  private getAttachmentsAndSites() {
    forkJoin([
      this._attachmentService.getAttachments(),
      this._siteService.getSites()
    ]).subscribe(([{ attachments }, { sites }]) => {
      this._attachments = attachments;
      this._sites = sites;

      this.setDataSource();
    });
  }

  private setDataSource() {
    const actions: Array<TableActionButton> = [
      {
        action: TableActions.Edit,
        text: 'Edit',
        icon: 'edit'
      },
      {
        action: TableActions.Remove,
        text: 'Remove',
        icon: 'trash-blue'
      }
    ];

    this._dataSource = this._attachments.map(
      ({ identifier, name, siteId, description, encap }: Attachment): AttachmentRow => {
        const site = this._sites?.find(s => s.config.identifier === siteId);

        return {
          attachment: name,
          site: site?.config?.name,
          description,
          vid: encap?.ovid,
          actions
        };
      }
    );
  }

  public onAddAttachment() {
    const data: AttachmentDialogData = {
      sites: this._sites
    };

    const dialogRef: MatDialogRef<AttachmentDialogComponent> = this._dialog.open(
      AttachmentDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((attachment: Attachment) => {
        attachment.encap.ivid = attachment.encap.ivid ?? 0;
        this._attachments.push(attachment);
        this.setDataSource();
      });
  }

  // TODO(rstandt) switch to doing lookups by UUID rather than name.
  private editAttachment({ attachment: name }: AttachmentRow) {
    let editableAttachment: Attachment = this._attachments.find(
      (attachment: Attachment): boolean => attachment.name === name
    );

    editableAttachment = cloneDeep(editableAttachment);

    // server response sets ivid = 0 which is out of VLAN range;
    // encap component should derermine a proper vlanType using VLAN validators.
    if (editableAttachment.encap?.ivid === 0) {
      delete editableAttachment.encap.ivid;
    }

    const data: AttachmentDialogData = {
      attachment: editableAttachment,
      sites: this._sites
    };

    const dialogRef: MatDialogRef<AttachmentDialogComponent, Attachment> = this._dialog.open(
      AttachmentDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((editedAttachment: Attachment) => {
        const index: number = this._attachments.findIndex(
          (attachment: Attachment): boolean => attachment.name === name
        );

        this._attachments[index] = cloneDeep(editedAttachment);

        this.setDataSource();
      });
  }

  // TODO(rstandt) switch to doing lookups by UUID rather than name.
  private removeAttachment({ attachment: name }: AttachmentRow) {
    const data: ConfirmationDialogData = {
      description: `This will remove the attachment <strong>${name}</strong>.`,
      action: 'Remove Attachment'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const index: number = this._attachments.findIndex(
      (attachment: Attachment): boolean => attachment.name === name
    );

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        mergeMap((_: boolean) => this._attachmentService.deleteAttachment(this._attachments[index].identifier))
      )
      .subscribe(
        (_: any) => {

          this._attachments.splice(index, 1);

          this.setDataSource();

          this._alertService.info(AlertType.INFO_REMOVE_ATTACHMENT_SUCCESS);
        },
        (_: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_REMOVE_ATTACHMENT);
        }
      );
  }

  public onActionClick(event: TableActionEvent) {
    switch (event.type) {
      case TableActions.Edit:
        this.editAttachment(event.data);
        break;
      case TableActions.Remove:
        this.removeAttachment(event.data);
        break;
    }
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get columns(): Column[] {
    return COLUMNS;
  }

  public get dataSource(): AttachmentRow[] {
    return this._dataSource;
  }

  public get sites(): Site[] {
    return this._sites;
  }
}
