import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, flush } from '@angular/core/testing';
import { DOCUMENT, DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TestRequest, HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonToggleModule } from '@angular/material/button-toggle';

import { cloneDeep } from 'lodash-es';

import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { Attachment } from 'rest_client/pangolin/model/attachment';
import { Attachments } from 'rest_client/pangolin/model/attachments';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { UserService } from '../users/user.service';
import { AlertService } from 'src/app/shared/alert.service';
import { User } from 'rest_client/heimdallr/model/user';

import { COLUMNS, AttachmentRow, AttachmentsComponent } from './attachments.component';
import { BackButtonComponent } from 'src/app/shared/back-button/back-button.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { AttachmentDialogComponent } from './attachment-dialog/attachment-dialog.component';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { EncapComponent } from '../encap/encap.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Sites } from 'rest_client/pangolin/model/sites';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { Site } from 'rest_client/pangolin/model/site';

enum UserType {
  SystemAdmin,
  SystemUser
}

describe('AttachmentsComponent', () => {
  let component: AttachmentsComponent;
  let fixture: ComponentFixture<AttachmentsComponent>;
  let document: Document;
  let httpTestingController: HttpTestingController;
  let userService: UserService;
  let tableDe: DebugElement;

  const mockUsers: User[] = [
    {
      username: 'SystemAdmin',
      email: 'sysadmin@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    },
    {
      username: 'SystemUser',
      email: 'sysuser@nefeli.io',
      roles: {
        scope: 'system',
        id: '-3',
        roles: ['user']
      }
    }
  ];

  const mockAttachments: Attachment[] = [
    {
      identifier: '1',
      name: 'attachment1',
      siteId: 'eric_site',
      description: 'First Attachment',
      encap: {
        ovid: 1,
        ivid: 0
      }
    },
    {
      identifier: '2',
      name: 'attachment2',
      siteId: 'eric_site',
      description: 'Second Attachment',
      encap: {
        ovid: 2,
        ivid: 0
      }
    },
    {
      identifier: '3',
      name: 'attachment3',
      siteId: 'test_site',
      description: 'Third Attachment',
      encap: {
        ovid: 3,
        ivid: 0
      }
    }
  ];

  const mockSite1: Site = {
    config: {
      dataplaneVlanTag: 0,
      name: 'Eric\'s Site',
      identifier: 'eric_site',
      siteStatus: 'ACTIVE',
      siteType: 'CLUSTER',
      publicIp: '127.0.0.1',
      privateIp: '127.0.0.1',
      gatewayMac: '00:00:00:00:00:01',
      inboundVxlanPort: 13333,
      mgmtUrl: '',
      vniRangeEnd: 1,
      vniRangeStart: 0
    }
  };

  const mockSite2: Site = {
    config: {
      dataplaneVlanTag: 0,
      name: 'Test Site',
      identifier: 'test_site',
      siteStatus: 'ACTIVE',
      siteType: SiteConfigSiteType.UCPE,
      publicIp: '127.0.0.1',
      privateIp: '127.0.0.1',
      gatewayMac: '00:00:00:00:00:01',
      inboundVxlanPort: 13333,
      mgmtUrl: '',
      vniRangeEnd: 1,
      vniRangeStart: 0
    }
  };

  const mockSitesResponse: Sites = {
    sites: [
      mockSite1,
      mockSite2
    ]
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AttachmentsComponent,
        BackButtonComponent,
        TableComponent,
        AvatarComponent,
        AttachmentDialogComponent,
        ConfirmationDialogComponent,
        EncapComponent
      ],
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MatSnackBarModule,
        MatButtonModule,
        MatTableModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatExpansionModule,
        MatRadioModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatButtonToggleModule
      ],
      providers: [
        DecimalPipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        AttachmentsService,
        UserService,
        AlertService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);

    fixture = TestBed.createComponent(AttachmentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const attachmentsRequest: TestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/attachments`
    });

    expect(attachmentsRequest.request.responseType).toBe('json');

    attachmentsRequest.flush({
      attachments: cloneDeep(mockAttachments)
    } as Attachments);

    const sitesRequest: TestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/sites`
    });
    sitesRequest.flush(cloneDeep(mockSitesResponse));

    fixture.detectChanges();

    tableDe = fixture.debugElement.query(By.css('nef-table'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page heading', () => {
    expect(
      fixture.debugElement.query(By.css('header h2')).nativeElement.textContent
    ).toBe('Attachments', 'valid \'Attachments\' heading');
  });

  it('should render attachments table', () => {
    expect(tableDe).not.toBeNull('table is rendered');

    const headerCellDes: DebugElement[] = tableDe.queryAll(By.css('mat-header-cell'));

    expect(headerCellDes.length).toBe(5, 'table header cells are rendered');

    expect(headerCellDes[0].nativeElement.textContent).toContain(
      COLUMNS[0].label,
      `valid ${COLUMNS[0].name} column label`
    );
    expect(headerCellDes[1].nativeElement.textContent).toContain(
      COLUMNS[1].name,
      `valid ${COLUMNS[1].name} column label`
    );
    expect(headerCellDes[2].nativeElement.textContent).toContain(
      COLUMNS[2].name,
      `valid ${COLUMNS[2].name} column label`
    );
    expect(headerCellDes[3].nativeElement.textContent).toContain(
      COLUMNS[3].label,
      `valid ${COLUMNS[2].name} column label`
    );
  });

  it('should set attachments and data source', () => {
    const attachments: Attachment[] = component['_attachments'];
    const dataSource: AttachmentRow[] = component['_dataSource'];

    expect(attachments.length).toEqual(3);

    expect(attachments[0].name).toBe(mockAttachments[0].name, `valid ${mockAttachments[0].name} attachment name`);
    expect(attachments[1].siteId).toBe(mockAttachments[1].siteId, `valid ${mockAttachments[1].name} attachment site`);
    expect(attachments[0].description).toBe(mockAttachments[0].description, `valid ${mockAttachments[0].name} attachment description`);
    expect(attachments[1].encap.ovid).toBe(mockAttachments[1].encap.ovid, `valid ${mockAttachments[1].name} attachment VLAN id`);

    expect(dataSource.length).toEqual(3);

    expect(dataSource[1].attachment).toBe(mockAttachments[1].name, `valid data source ${mockAttachments[1].name} attachment name`);
    expect(dataSource[0].site).toBe(mockSite1.config.name, `valid data source ${mockAttachments[1].name} attachment site`);
    expect(dataSource[1].description).toBe(mockAttachments[1].description, `valid data source ${mockAttachments[1].name} attachment description`);
    expect(dataSource[1].vid).toBe(mockAttachments[1].encap.ovid, `valid data source ${mockAttachments[1].name} attachment VLAN id`);
  });

  it('should render attachments table rows', () => {
    const rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    expect(rowDes.length).toBe(mockAttachments.length, `render ${mockAttachments.length} rows`);

    const firstRowCellDes: DebugElement[] = rowDes[0].queryAll(By.css('mat-cell'));

    expect(firstRowCellDes[0].nativeElement.textContent).toBe(
      mockAttachments[0].name,
      `render ${mockAttachments[0].name} attachment name`
    );
    expect(firstRowCellDes[1].nativeElement.textContent).toBe(
      mockSite1.config.name,
      `render ${mockAttachments[0].name} attachment site`
    );
    expect(firstRowCellDes[2].nativeElement.textContent).toBe(
      mockAttachments[0].description,
      `render ${mockAttachments[0].name} attachment description`
    );
    expect(firstRowCellDes[3].nativeElement.textContent).toBe(
      mockAttachments[0].encap.ovid.toString(),
      `render ${mockAttachments[0].name} attachment VLAN id`
    );

    const secondRowCellDes: DebugElement[] = rowDes[1].queryAll(By.css('mat-cell'));

    expect(secondRowCellDes[0].nativeElement.textContent).toBe(
      mockAttachments[1].name,
      `render ${mockAttachments[1].name} attachment name`
    );
    expect(secondRowCellDes[1].nativeElement.textContent).toBe(
      mockSite1.config.name,
      `render ${mockAttachments[1].name} attachment site`
    );
    expect(secondRowCellDes[2].nativeElement.textContent).toBe(
      mockAttachments[1].description,
      `render ${mockAttachments[1].name} attachment description`
    );
    expect(secondRowCellDes[3].nativeElement.textContent).toBe(
      mockAttachments[1].encap.ovid.toString(),
      `render ${mockAttachments[1].name} attachment VLAN id`
    );
  });

  it('should edit an attachment', fakeAsync(() => {
    const rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    const editAttachmentButtonEl: HTMLButtonElement = rowDes[1].query(
      By.css('button.edit')
    ).nativeElement;

    spyOn<any>(component, 'editAttachment').and.callThrough();

    editAttachmentButtonEl.click();
    expect(component['editAttachment']).toHaveBeenCalled();
    tick();

    const attachmentDialogEl: HTMLUnknownElement = document.querySelector('nef-attachment-dialog');

    expect(attachmentDialogEl).not.toBeNull('render Attachment dialog');

    const nameInputEl: HTMLInputElement = attachmentDialogEl.querySelector(
      'input[formcontrolname="name"]'
    );

    const siteSelectEl: HTMLInputElement = attachmentDialogEl.querySelector(
      'mat-select[formcontrolname="site"]'
    );

    let descriptionInputEl: HTMLInputElement = attachmentDialogEl.querySelector(
      'input[formcontrolname="description"]'
    );

    const mockDescription: string = 'Common Attachment';

    descriptionInputEl.value = mockDescription;
    descriptionInputEl.dispatchEvent(new Event('input'));
    descriptionInputEl.dispatchEvent(new KeyboardEvent('keyup'));

    const submitButtonEl: HTMLButtonElement = document.querySelector('button[form="attachment-form"]');

    submitButtonEl.click();
    tick();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/attachments/${encodeURIComponent(mockAttachments[1].identifier)}`
    );

    const mockAttachment: Attachment = {
      name: nameInputEl.value,
      siteId: siteSelectEl.value,
      description: descriptionInputEl.value
    };

    request.flush(mockAttachment);
    fixture.detectChanges();
    tick();

    editAttachmentButtonEl.click();
    tick();

    descriptionInputEl = attachmentDialogEl.querySelector(
      'input[formcontrolname="description"]'
    );

    expect(descriptionInputEl.value).toBe(
      mockDescription,
      'render updated attachment description'
    );

    flush();
  }));

  it('should remove an attachment', fakeAsync(() => {
    let rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    // save 2nd attachment row rendered name to compare later
    const attachmentName: string = rowDes[1].query(
      By.css('.mat-column-attachment')
    ).nativeElement.textContent;

    const removeButtonEl: HTMLButtonElement = rowDes[0].query(
      By.css('button.remove')
    ).nativeElement;

    expect(rowDes.length).toBe(
      mockAttachments.length,
      `render ${mockAttachments.length} attachment rows`
    );

    expect(removeButtonEl).not.toBeNull('render Remove button');
    expect(removeButtonEl.textContent).toBe('Remove', 'render Remove button text');

    spyOn<any>(component, 'removeAttachment').and.callThrough();

    removeButtonEl.click();
    expect(component['removeAttachment']).toHaveBeenCalled();
    tick();

    const dialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
    const descriptionEls: NodeListOf<HTMLParagraphElement> = dialogEl.querySelectorAll('p');
    const removeAttachmentButtonEl: HTMLButtonElement = dialogEl.querySelectorAll('button')[1];

    expect(descriptionEls[0].textContent).toBe(
      `This will remove the attachment ${mockAttachments[0].name}.`,
      'render dialog description'
    );

    expect(removeAttachmentButtonEl.textContent).toBe(
      'Remove Attachment',
      'render \'Remove Attachment\' button text'
    );

    removeAttachmentButtonEl.click();
    tick();

    const request: TestRequest = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/attachments/${encodeURIComponent(String(mockAttachments[0].identifier))}`
    });

    request.flush({});
    fixture.detectChanges();

    rowDes = tableDe.queryAll(By.css('mat-row'));

    expect(rowDes.length).toEqual(
      mockAttachments.length - 1,
      'render attachment rows without removed attachment'
    );

    expect(
      rowDes[0].query(By.css('.mat-column-attachment')).nativeElement.textContent.trim()
    ).toBe(attachmentName, 'render shifted attachment row name');

    flush();
  }));
});
