import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';

import { SharedModule } from '../shared/shared.module';
import { EncapModule } from '../encap/encap.module';

import { AttachmentsRoutingModule } from './attachments-routing.module';

import { AttachmentsComponent } from './attachments.component';
import { AttachmentDialogComponent } from './attachment-dialog/attachment-dialog.component';

@NgModule({
  declarations: [
    AttachmentsComponent,
    AttachmentDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    SharedModule,
    EncapModule,
    AttachmentsRoutingModule
  ]
})
export class AttachmentsModule { }
