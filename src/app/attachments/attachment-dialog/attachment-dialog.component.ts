import { Component, OnInit, OnDestroy, ViewChild, Inject } from '@angular/core';
import { Site } from 'rest_client/pangolin/model/site';
import { Attachment } from 'rest_client/pangolin/model/attachment';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { Vxlan } from 'rest_client/pangolin/model/vxlan';
import { VxlanVtep } from 'rest_client/pangolin/model/vxlanVtep';
import { Encap } from 'rest_client/pangolin/model/encap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { VLANType, VLANMode, EncapComponent } from '../../encap/encap.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { HttpErrorResponse } from '@angular/common/http';

import { Subscription } from 'rxjs';

export interface AttachmentDialogData {
  attachment?: Attachment;
  sites: Site[];
}

@Component({
  selector: 'nef-attachment-dialog',
  templateUrl: './attachment-dialog.component.html',
  styleUrls: ['./attachment-dialog.component.less']
})
export class AttachmentDialogComponent implements OnInit, OnDestroy {

  private _attachmentForm: FormGroup;
  private _attachmentSubscription: Subscription;

  @ViewChild('encap', { static: true }) private _encapComponent: EncapComponent;

  constructor(
    private _fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private _data: AttachmentDialogData,
    private _attachmentsService: AttachmentsService,
    private _dialogRef: MatDialogRef<AttachmentDialogComponent>,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this.initAttachmentForm();
  }

  public ngOnDestroy() {
    if (this._attachmentSubscription) {
      this._attachmentSubscription.unsubscribe();
    }
  }

  private initAttachmentForm() {
    const disabled: boolean = Boolean(this._data.attachment);

    this._attachmentForm = this._fb.group({
      name: [
        {
          value: this._data?.attachment?.name,
          disabled
        },
        Validators.required
      ],
      site: [
        this._data?.attachment?.siteId,
        Validators.required
      ],
      description: this._data?.attachment?.description,
      encap: this._fb.group({})
    });
  }

  public submitAttachmentForm() {
    const { invalid, pristine } = this._attachmentForm;

    if (invalid || pristine) {
      if (this._attachmentForm.get('encap').invalid) {
        // open VLAN Configuration panels if VLAN groups have errors
        this._encapComponent.openInvalidPanels();
      }

      return;
    }

    const name: string = this._attachmentForm.get('name').value;
    const siteId: string = this._attachmentForm.get('site').value;
    const description: string = this._attachmentForm.get('description').value;

    const {
      encap: {
        vlan: {
          type: vlanType,
          vid
        },
        vxlan: { vni, mac, ip, udpPort }
      }
    }: {
      name: string;
      site: string;
      description: string;
      encap: {
        vlan: {
          type: string;
          vid: {
            inner: number;
            outer: number;
          }
        },
        vxlan: {
          vni: number;
          mac: string;
          ip: string;
          udpPort: number;
        }
      }
    } = this._attachmentForm.value;

    let ivid: number;
    let ovid: number;
    let vxlan: Vxlan;

    switch (vlanType) {
      case VLANType.None:
        vxlan = {
          vni,
          vtep: {
            ipAddr: ip,
            mac,
            udpPort
          } as VxlanVtep
        };
        break;
      case VLANType['802.1Q']:
        ivid = vid.inner;
        break;
      case VLANType['802.1AD']:
        ivid = vid.inner;
        ovid = vid.outer;
    }

    const encap: Encap = { ivid, ovid, vxlan };
    const attachment: Attachment = { name, siteId, description, encap };

    if (this._attachmentSubscription) {
      this._attachmentSubscription.unsubscribe();
    }

    if (this._data.attachment) {
      this.editAttachment(attachment);
    } else {
      this.postAttachment(attachment);
    }
  }

  private postAttachment(attachment: Attachment) {
    this._attachmentSubscription = this._attachmentsService
      .postAttachment(attachment)
      .subscribe(
        (createdAttachment: Attachment) => {
          attachment = createdAttachment;
          this._alertService.info(AlertType.INFO_CREATE_ATTACHMENT_SUCCESS);
          this._dialogRef.close(attachment);
        },
        (_: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_CREATE_ATTACHMENT);
        }
      );
  }

  private editAttachment(attachment: Attachment) {
    attachment.identifier = this._data.attachment.identifier;
    this._attachmentSubscription = this._attachmentsService
      .putAttachment(this._data.attachment.identifier, attachment)
      .subscribe(
        (_: any) => {
          this._alertService.info(AlertType.INFO_EDIT_ATTACHMENT_SUCCESS);

          this._dialogRef.close(attachment);
        },
        (_: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_EDIT_ATTACHMENT);
        }
      );
  }

  public get attachmentForm(): FormGroup {
    return this._attachmentForm;
  }

  public get encapForm(): FormGroup {
    return this._attachmentForm.get('encap') as FormGroup;
  }

  public get sites(): Site[] {
    return this._data.sites;
  }

  public get attachment(): Attachment {
    return this._data.attachment;
  }

  public get vlanMode(): VLANMode {
    return VLANMode['802.1ADOVIDOnly'];
  }
}
