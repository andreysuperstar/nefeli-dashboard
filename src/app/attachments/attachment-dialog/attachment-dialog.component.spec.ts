import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentDialogComponent } from './attachment-dialog.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule, MatSelect } from '@angular/material/select';
import { MatExpansionModule, MatExpansionPanel } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { EncapComponent } from '../../encap/encap.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { Site } from 'rest_client/pangolin/model/site';
import { Attachment } from 'rest_client/pangolin/model/attachment';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { MatOption } from '@angular/material/core';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FlexLayoutModule } from '@angular/flex-layout';
import { environment } from 'src/environments/environment';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonToggleHarness } from '@angular/material/button-toggle/testing';

describe('AttachmentDialogComponent', () => {
  let component: AttachmentDialogComponent;
  let fixture: ComponentFixture<AttachmentDialogComponent>;
  let httpTestingController: HttpTestingController;
  let loader: HarnessLoader;

  const dialogSpy = jasmine.createSpyObj<MatDialogRef<AttachmentDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  const mockSites: Site[] = [
    {
      config: {
        identifier: 'eric_site',
        name: 'Eric\'s Site'
      }
    },
    {
      config: {
        identifier: 'andrew_site',
        name: 'Andrew\'s Site'
      }
    },
    {
      config: {
        identifier: 'anton_site',
        name: 'Anton\'s Site'
      }
    },
    {
      config: {
        identifier: 'inactive_site',
        name: 'Inactive Site'
      }
    }
  ];

  const mockAttachment: Attachment = {
    identifier: '1',
    name: 'attachment1',
    siteId: mockSites[2].config.identifier,
    description: 'First Attachment'
  };

  const mockAttachmentEncap: Attachment = {
    identifier: '1',
    name: 'attachment1',
    siteId: mockSites[2].config.identifier,
    description: 'Second Attachment',
    encap: {
      ovid: 123
    }
  }

  const setNameSiteDescription = () => {
    const nameInputEl: DebugElement = fixture.debugElement.query(By.css('mat-form-field input[formcontrolname=name]'));
    const descInputEl: DebugElement = fixture.debugElement.query(By.css('mat-form-field input[formcontrolname=description]'));
    const siteSelectComp: MatSelect = fixture.debugElement.query(By.css('mat-form-field mat-select')).componentInstance;

    // set data for name, site & description
    nameInputEl.nativeElement.value = 'my-attach';
    nameInputEl.nativeElement.dispatchEvent(new Event('input'));
    descInputEl.nativeElement.value = 'My VLAN Attachment';
    descInputEl.nativeElement.dispatchEvent(new Event('input'));
    siteSelectComp.options.find((option: MatOption) => {
      return option.value === 'anton_site';
    }).select();
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MatSnackBarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatExpansionModule,
        MatRadioModule,
        MatButtonToggleModule
      ],
      declarations: [
        AttachmentDialogComponent,
        EncapComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            sites: mockSites
          }
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        AttachmentsService,
        AlertService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    let headingEl: HTMLHeadingElement = fixture.debugElement.query(
      By.css('[mat-dialog-title]')
    ).nativeElement;

    expect(headingEl.textContent).toBe('Add New Attachment', 'render a new attachment title');

    component['_data'].attachment = mockAttachment;
    fixture.detectChanges();

    headingEl = fixture.debugElement.query(By.css('[mat-dialog-title]')).nativeElement;

    expect(headingEl.textContent).toBe('Edit Attachment', 'render an editable attachment title');
  });

  it('should submit QinQ VLAN attachment', async () => {
    spyOn(component['_alertService'], 'error');
    setNameSiteDescription();

    // open VLAN panel
    const toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();

    fixture.detectChanges();

    // set Outer VID
    const ovidInputEl = fixture.debugElement.query(By.css('input[formcontrolname=outer]'));

    ovidInputEl.nativeElement.value = 123;
    ovidInputEl.nativeElement.dispatchEvent(new Event('input'));

    // submit form
    const submitButtonEl = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    submitButtonEl.click();

    // verify REST request
    let req = httpTestingController.expectOne(`${environment.restServer}/v1/attachments`);

    expect(req.request.method).toBe('POST');
    expect(req.request.body.description).toBe('My VLAN Attachment');
    expect(req.request.body.name).toBe('my-attach');
    expect(req.request.body.siteId).toBe('anton_site');
    expect(req.request.body.encap.ivid).toBeUndefined();
    expect(req.request.body.encap.ovid).toBe(123);
    expect(req.request.body.encap.vxlan).toBeUndefined();

    req.flush({});

    //checking error response
    submitButtonEl.click();

    req = httpTestingController.expectOne(`${environment.restServer}/v1/attachments`);

    req.flush({}, {
      status: 400,
      statusText: 'Bad Request'
    });

    expect(component['_alertService'].error).toHaveBeenCalledWith(
      AlertType.ERROR_CREATE_ATTACHMENT
    );
  });

  it('should hide VLAN type radio buttons and VXLAN panel', async () => {
    const toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();
    fixture.detectChanges();

    // hidden VLAN types
    const vlanTypeRadioGroupDe = fixture.debugElement.query(
      By.css('[formControlName=type]')
    );

    expect(vlanTypeRadioGroupDe).toBeNull('hide VLAN type radio buttons');

    // hidden inner VID
    const innerVIDDe = fixture.debugElement.query(
      By.css('[formControlName=inner]')
    );

    expect(innerVIDDe).toBeNull('hide inner VID');

    // hidden VXLAN panel
    const legends = fixture.debugElement.nativeElement.querySelectorAll('legend');
    expect(legends.length).toBe(1);
    expect(legends[0].textContent).toBe('VLAN');
  });

  // TODO(eric): re-enable when vxlan attachments are supported
  xit('should submit vxlan attachment', async () => {
    setNameSiteDescription();

    // open vxlan panel
    const [_, vxlanPanel] = fixture.debugElement.queryAll(
      By.directive(MatExpansionPanel)
    );

    vxlanPanel.componentInstance.open();
    fixture.detectChanges();

    // set vxlan
    const vniInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=vni]'));
    const macInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=mac]'));
    const ipInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=ip]'));
    const portInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=udpPort]'));
    vniInputEl.nativeElement.value = 123;
    vniInputEl.nativeElement.dispatchEvent(new Event('input'));
    macInputEl.nativeElement.value = '02:42:68:65:42:60';
    macInputEl.nativeElement.dispatchEvent(new Event('input'));
    ipInputEl.nativeElement.value = '127.0.0.1';
    ipInputEl.nativeElement.dispatchEvent(new Event('input'));
    portInputEl.nativeElement.value = 4789;
    portInputEl.nativeElement.dispatchEvent(new Event('input'));

    // submit form
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
    submitButtonEl.click();

    // verify REST request
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/attachments`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.description).toBe('My VLAN Attachment');
    expect(req.request.body.name).toBe('my-attach');
    expect(req.request.body.siteId).toBe('anton_site');
    expect(req.request.body.encap.vxlan.vni).toBe(123);
    expect(req.request.body.encap.vxlan.vtep.ipAddr).toBe('127.0.0.1');
    expect(req.request.body.encap.vxlan.vtep.mac).toBe('02:42:68:65:42:60');
    expect(req.request.body.encap.vxlan.vtep.udpPort).toBe(4789);

    const response: Attachment = {
      name: 'my-attacht',
      description: 'My VLAN Attachment',
      siteId: 'anton_site',
      encap: {
        ovid: 0,
        ivid: 123
      }
    };
    req.flush(response);
  });

  it('should require name and site', async () => {
    // submit form
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(By.css('button[type=submit]')).nativeElement;
    expect(submitButtonEl.disabled).toBeTruthy();
    fixture.detectChanges();
    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter attachment name'
    }));
    const siteSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=site]'
    }));

    await nameInput.blur();
    await siteSelect.blur();

    // check for errors
    const errorEl: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-form-field mat-error'));
    expect(errorEl[0].nativeElement.textContent.trim()).toBe('Attachment name is required');
    expect(errorEl[1].nativeElement.textContent.trim()).toBe('Site is required');
  });

  it('should fill form control values for Edit Attachment dialog', () => {
    component['_data'].attachment = mockAttachment;
    component['initAttachmentForm']();
    fixture.detectChanges();
    expect(
      component['_attachmentForm'].get('name').value
    ).toBe(mockAttachment.name, 'valid Name control value');
    expect(
      component['_attachmentForm'].get('site').value
    ).toBe(mockAttachment.siteId, 'valid Site control value');
    expect(
      component['_attachmentForm'].get('description').value
    ).toBe(mockAttachment.description, 'valid Description control value');
  });

  it('should display Outer VID field value when Inner VID is not set', async () => {
    component['_data'].attachment = mockAttachmentEncap;
    component['initAttachmentForm']();
    fixture.detectChanges();

    let toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();

    fixture.detectChanges();

    const outerVIDInput = await loader.getHarness(MatInputHarness.with({
      placeholder: 'Enter outer'
    }));

    expect(await outerVIDInput.getValue()).toBe('123');
  });

  it('should be able to edit attachment VLAN', async () => {
    spyOn(component['_alertService'], 'error');
    component['_data'].attachment = mockAttachmentEncap;
    component['initAttachmentForm']();
    fixture.detectChanges();

    let toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();

    fixture.detectChanges();

    const ovidInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter outer'
    }));
    expect(await ovidInput.getValue()).toBe('123');
    await ovidInput.setValue('111');
    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Complete'
    }));
    await submitButton.click();
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/attachments/1`);
    expect(req.request.method).toBe('PUT');
    //checking error response
    req.flush({}, {
      status: 400,
      statusText: 'Bad Request'
    });
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_EDIT_ATTACHMENT);
  });
});
