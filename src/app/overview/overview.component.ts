import { Component } from '@angular/core';

import { interval, Observable } from 'rxjs';
import { switchMap, tap, retryWhen, delay, startWith } from 'rxjs/operators';

import { cloneDeep } from 'lodash-es';

import { SystemStats } from 'rest_client/pangolin/model/systemStats';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { Sites } from 'rest_client/pangolin/model/sites';
import { LegendItem } from '../shared/legend/legend.component';

import { StatusType } from '../shared/status-item/status-item.component';

import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { StatsService } from 'rest_client/pangolin/api/stats.service';

enum SiteLegend {
  Active,
  Provisioning,
  Error,
  Inactive
}

interface SiteStatus {
  identifier: string;
  name: string;
  type?: StatusType;
  size?: number;
}

const POLL_INTERVAL = 5000;

export const SITE_LEGEND_ITEMS: LegendItem[] = [
  {
    label: 'Active',
    color: 'success'
  },
  {
    label: 'Provisioning',
    color: 'info'
  },
  {
    label: 'Error',
    color: 'error'
  },
  {
    label: 'Inactive',
    color: 'muted'
  }
];

export const SITE_STATUS_TYPES = new Map([
  [SiteConfigSiteStatus.ACTIVE, StatusType.Success],
  [SiteConfigSiteStatus.PROVISIONING, StatusType.Info],
  [SiteConfigSiteStatus.INACTIVE, StatusType.Muted],
  [SiteConfigSiteStatus.STAGED, StatusType.Muted],
  [SiteConfigSiteStatus.SITESTATUSNULL, StatusType.Error]
]);

@Component({
  selector: 'nef-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.less']
})
export class OverviewComponent {
  private _siteLegendItems: LegendItem[];
  private _siteStatuses: SiteStatus[];
  private _systemStats$ = this._statsService.postSystemStats();

  private _sites$ = interval(POLL_INTERVAL)
    .pipe(
      startWith(undefined as string),
      switchMap(() => this._sitesService.getSites()),
      tap((sites: Sites) => {
        this.setSiteLegend(sites);

        this.setSiteStatuses(sites);
      }),
      retryWhen(errors => errors.pipe(
        delay(POLL_INTERVAL)
      ))
    );

  constructor(private _sitesService: SitesService, private _statsService: StatsService) { }

  private setSiteLegend({ sites }: Sites) {
    const legendSitesCount: number[] = new Array(SITE_LEGEND_ITEMS.length)
      .fill(0);

    sites.forEach(({
      config: { siteStatus }
    }) => {
      let legendSiteIndex: number;

      switch (siteStatus) {
        case SiteConfigSiteStatus.ACTIVE:
          legendSiteIndex = SiteLegend.Active;
          break;
        case SiteConfigSiteStatus.PROVISIONING:
          legendSiteIndex = SiteLegend.Provisioning;
          break;
        case SiteConfigSiteStatus.SITESTATUSNULL:
          legendSiteIndex = SiteLegend.Error;
          break;
        case SiteConfigSiteStatus.INACTIVE:
        case SiteConfigSiteStatus.STAGED:
        default:
          legendSiteIndex = SiteLegend.Inactive;
      }

      legendSitesCount[legendSiteIndex]++;
    });

    const siteLegendItems = cloneDeep(SITE_LEGEND_ITEMS);

    legendSitesCount.forEach((sitesCount, index) => {
      siteLegendItems[index].label = `${siteLegendItems[index].label} (${sitesCount})`;
    });

    this._siteLegendItems = siteLegendItems;
  }

  private setSiteStatuses({ sites }: Sites) {
    this._siteStatuses = sites.map(({
      config: {
        identifier,
        name,
        siteStatus
      }
    }): SiteStatus => {
      const type = SITE_STATUS_TYPES.get(siteStatus);

      return { identifier, name, type };
    });
  }

  public get systemStats$(): Observable<SystemStats> {
    return this._systemStats$;
  }

  public get sites$(): Observable<Sites> {
    return this._sites$;
  }

  public get siteLegendItems(): LegendItem[] {
    return this._siteLegendItems;
  }

  public get siteStatuses(): SiteStatus[] {
    return this._siteStatuses;
  }
}
