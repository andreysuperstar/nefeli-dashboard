import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';

import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { Sites } from 'rest_client/pangolin/model/sites';
import { SystemStats } from 'rest_client/pangolin/model/systemStats';

import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { StatsService } from 'rest_client/pangolin/api/stats.service';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as STATS_BASE_PATH } from 'rest_client/pangolin/variables';

import { SITE_LEGEND_ITEMS, SITE_STATUS_TYPES, OverviewComponent } from './overview.component';
import { SummaryItemComponent } from '../summary/summary-item.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { StatusGridComponent } from '../shared/status-grid/status-grid.component';
import { StatusItemComponent } from '../shared/status-item/status-item.component';

describe('OverviewComponent', () => {
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;
  let httpTestingController: HttpTestingController;

  const mockStats: SystemStats = {
    servicesTotal: 46,
    clusterSites: {
      total: 325,
      active: 322
    },
    ucpeSites: {
      total: 193,
      active: 191
    }
  };

  const mockSites: Sites = {
    sites: [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric\'s Site',
          siteStatus: SiteConfigSiteStatus.PROVISIONING
        }
      },
      {
        config: {
          identifier: 'andrew_site',
          name: 'Andrew\'s Site',
          siteStatus: SiteConfigSiteStatus.ACTIVE
        }
      },
      {
        config: {
          identifier: 'anton_site',
          name: 'Anton\'s Site',
          siteStatus: SiteConfigSiteStatus.SITESTATUSNULL
        }
      },
      {
        config: {
          identifier: 'inactive_site',
          name: 'Inactive Site',
          siteStatus: SiteConfigSiteStatus.INACTIVE
        }
      },
      {
        config: {
          identifier: 'staged_site',
          name: 'Staged Site',
          siteStatus: SiteConfigSiteStatus.STAGED
        }
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatCardModule
      ],
      declarations: [
        OverviewComponent,
        SummaryItemComponent,
        LegendComponent,
        StatusGridComponent,
        StatusItemComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: STATS_BASE_PATH,
          useValue: '/v1'
        },
        SitesService,
        StatsService,
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const statsRequest = httpTestingController.expectOne(`${environment.restServer}/v1/stats`);

    const sitesRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites`
    );

    statsRequest.flush(mockStats);
    sitesRequest.flush(mockSites);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set site legend', () => {
    expect(component['_siteLegendItems'][0]?.label).toBe(
      `${SITE_LEGEND_ITEMS[0].label} (1)`,
      `valid ${SITE_LEGEND_ITEMS[0].label} legend item label`
    );

    expect(component['_siteLegendItems'][1]?.label).toBe(
      `${SITE_LEGEND_ITEMS[1].label} (1)`,
      `valid ${SITE_LEGEND_ITEMS[1].label} legend item label`
    );

    expect(component['_siteLegendItems'][2]?.label).toBe(
      `${SITE_LEGEND_ITEMS[2].label} (1)`,
      `valid ${SITE_LEGEND_ITEMS[2].label} legend item label`
    );

    expect(component['_siteLegendItems'][3]?.label).toBe(
      `${SITE_LEGEND_ITEMS[3].label} (2)`,
      `valid ${SITE_LEGEND_ITEMS[3].label} legend item label`
    );

    component['_siteLegendItems']?.forEach((legendItem, index) => {
      const { label, color } = SITE_LEGEND_ITEMS[index];

      expect(legendItem.color).toBe(color, `valid ${label} legend item color`);
    });
  });

  it('should set sites statuses', () => {
    component['_siteStatuses']?.forEach((status, index) => {
      const {
        config: { identifier, name, siteStatus }
      } = mockSites.sites[index];

      expect(status.identifier).toBe(
        identifier,
        `valid ${name} site status identifier`
      );

      expect(status.name).toBe(name, `valid ${name} site status name`);

      expect(status.type).toBe(
        SITE_STATUS_TYPES.get(siteStatus),
        `valid ${name} site status type`
      );
    });
  });

  it('should render summary tiles', () => {
    const summaryItemDes = fixture.debugElement.queryAll(By.css('nef-summary-item'));

    expect(summaryItemDes.length).toBe(3, 'render summary tiles');
  });

  it('should render system health', () => {
    const systemHealthDe = fixture.debugElement.query(By.css('.system-health'));
    const systemHealthHeadingDe = systemHealthDe.query(By.css('h1'));
    const sitesHeaderDe = systemHealthDe.query(By.css('mat-card header'));
    const sitesHeadingDe = sitesHeaderDe.query(By.css('h1'));
    const sitesLegendDe = sitesHeaderDe.query(By.css('nef-legend'));
    const statusGridDe = systemHealthDe.query(By.css('nef-status-grid'));
    const statusItemDes = statusGridDe.queryAll(By.css('nef-status-item'));

    expect(systemHealthHeadingDe).toBeDefined('render system health heading');

    expect(systemHealthHeadingDe.nativeElement.textContent).toBe(
      'System Health Overview',
      'render system health heading text'
    );

    expect(sitesHeadingDe).toBeDefined('render sites heading');

    expect(sitesHeadingDe.nativeElement.textContent).toBe(
      `${mockSites.sites.length} Sites`,
      'render sites heading text'
    );

    expect(sitesLegendDe).toBeDefined('render sites legend');
    expect((sitesLegendDe.nativeElement as HTMLElement).classList).toContain(
      'site-legend',
      'render sites legend'
    );

    expect(statusGridDe).toBeDefined('render Status Grid component');

    expect(statusItemDes.length).toBe(
      mockSites.sites.length,
      'render Status Item components'
    );
  });

  it('should render site status popup content', () => {
    const statusItemPopupDe = fixture.debugElement.query(By.css(
      'nef-status-item .status-popup'
    ));

    const siteHeadingDe = statusItemPopupDe.query(By.css('h1'));
    const siteAnchorDe = statusItemPopupDe.query(By.css('a'));

    expect(siteHeadingDe.nativeElement.textContent).toBe(
      `${mockSites.sites[0].config.name} Site`,
      'render site heading'
    );

    expect(siteAnchorDe.nativeElement.getAttribute('href')).toBe(
      `/sites/${mockSites.sites[0].config.identifier}`,
      'render site anchor'
    );

    expect(siteAnchorDe.nativeElement.textContent).toBe(
      'View Site',
      'render site anchor text'
    );
  });
});
