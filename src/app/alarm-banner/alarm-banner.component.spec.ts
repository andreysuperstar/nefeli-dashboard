import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { of } from 'rxjs';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { BASE_PATH } from 'rest_client/pangolin/variables';

import { AlarmService, AlarmsResponse, AlarmsCount, Alarm } from '../shared/alarm.service';
import { UserService } from '../users/user.service';
import { LocalStorageService } from '../shared/local-storage.service';

import { AlarmBannerComponent } from './alarm-banner.component';
import { AlarmListComponent } from './../shared/alarm-list/alarm-list.component';
import { MatChipsModule } from '@angular/material/chips';
import { RouterTestingModule } from '@angular/router/testing';

describe('AlarmBannerComponent', () => {
  let component: AlarmBannerComponent;
  let fixture: ComponentFixture<AlarmBannerComponent>;
  let document: Document;
  let httpTestingController: HttpTestingController;
  let alarmService: AlarmService;
  let el: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        OverlayModule,
        MatSnackBarModule,
        MatCardModule,
        MatListModule,
        HttpClientTestingModule,
        ScrollingModule,
        MatTooltipModule,
        MatChipsModule,
        RouterTestingModule
      ],
      declarations: [
        AlarmBannerComponent,
        AlarmListComponent
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: UserService,
          useValue: {
            user$: of({
              username: 'ecarino'
            })
          }
        },
        AlarmService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    alarmService = TestBed.inject(AlarmService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlarmBannerComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display only critical alerts', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 1
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 22
    } as AlarmsCount);

    fixture.detectChanges();
    const messageEl = el.querySelector('.message');
    expect(messageEl.textContent.trim()).toBe('You have 1 critical alert.');
  })

  it('should display message in plural form for multiple alerts', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 2
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 22
    } as AlarmsCount);

    fixture.detectChanges();
    const messageEl = el.querySelector('.message');
    expect(messageEl.textContent.trim()).toBe('You have 2 critical alerts.');
  })

  it('should have View Details button', () => {
    const detailsButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('.details')
    ).nativeElement;

    expect(detailsButtonEl).not.toBeNull();
    expect(detailsButtonEl.textContent).toBe('View Details');
  });

  it('should render alarm dropdown', () => {
    const detailsButtonDe: DebugElement = fixture.debugElement.query(By.css('.details'));

    detailsButtonDe.triggerEventHandler('click', {});
    fixture.detectChanges();

    const alarmDropdownDe: DebugElement = fixture.debugElement.query(
      By.css('.alarm-dropdown')
    );
    const alarmListDe: DebugElement = alarmDropdownDe.query(By.css('nef-alarm-list'));

    expect(alarmDropdownDe).not.toBeNull('render alarm dropdown');
    expect(alarmListDe).not.toBeNull('render Alarm List');
  });

  it('should retrieve critical alarms on dropdown overlay backdrop click', () => {
    const detailsButtonDe: DebugElement = fixture.debugElement.query(
      By.css('.details')
    );

    detailsButtonDe.triggerEventHandler('click', {});
    fixture.detectChanges();

    component.alarmsDataSource['_newAlarmsStream'].next([{
      _class: 'process',
      name: 'ProcessStarted',
      identity: {
        process: 'spy'
      },
      metadata: {},
      severity: 'info',
      timestamp: '1559686357',
      uuid: 'bcecf4b2-2d38-4319-9531-9ccf72a8be11',
      reason: ''
    }] as Alarm[]);

    const overflayBackdropEl: HTMLDivElement = document.querySelector(
      '.cdk-overlay-backdrop'
    );

    overflayBackdropEl.click();
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/read`);

    expect(req.request.body['uuids'][0]).toBe('bcecf4b2-2d38-4319-9531-9ccf72a8be11');

    req.flush({});
    fixture.detectChanges();

    expect(component.alarmsDataSource).toBeUndefined();
  });

  it('should hide alarm banner', () => {
    const localStorageService: LocalStorageService = TestBed.inject(LocalStorageService);
    localStorageService.delete('hide_alarm_banner');
    spyOn(component['_alarmService'], 'setBanner');

    const hideButton: DebugElement = fixture.debugElement.query(By.css('.hide'));
    hideButton.triggerEventHandler('click', {});
    fixture.detectChanges();

    expect(localStorageService.read('hide_alarm_banner')).toBe('true');
    expect(component['_alarmService'].setBanner).toHaveBeenCalledWith(false);
  });
});
