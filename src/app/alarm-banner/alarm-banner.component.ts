import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConnectedPosition } from '@angular/cdk/overlay';

import { Observable, Subscription } from 'rxjs';
import { pluck, mergeMap } from 'rxjs/operators';

import { AlarmDataSource } from '../shared/alarm.data-source';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';

import { AlarmService, Severity, Alarm, AlarmsCount } from '../shared/alarm.service';
import { UserService } from '../users/user.service';

export const Config = {
  streamInterval: 5000
};

@Component({
  selector: 'nef-alarm-banner',
  templateUrl: './alarm-banner.component.html',
  styleUrls: ['./alarm-banner.component.less']
})
export class AlarmBannerComponent implements OnInit, OnDestroy {

  private _alarmsCount: number;
  private _readAlarms: Alarm[];
  private _alarmsDataSource: AlarmDataSource;
  private _isAlarmDropdownOpen = false;
  private _alarmDropdownConnectedPositions: ConnectedPosition[] = [
    {
      originX: 'center',
      originY: 'bottom',
      overlayX: 'center',
      overlayY: 'top',
      offsetY: 15
    }
  ];
  private _alarmSubscription: Subscription;
  private _alarmsDataSourceSubscription: Subscription;

  constructor(
    private _alarmService: AlarmService,
    private _userService: UserService,
    private _localStorageService: LocalStorageService
  ) { }

  public ngOnInit() {
    this._alarmSubscription = this._alarmService.streamAlarmsCount$.subscribe(
      (count: AlarmsCount) => {
        this._alarmsCount = count.critical;
      }
    );
  }

  public ngOnDestroy() {
    if (this._alarmSubscription) {
      this._alarmSubscription.unsubscribe();
    }
  }

  private initAlarmsDataSource() {
    if (!this._alarmsDataSource) {
      this._alarmsDataSource = new AlarmDataSource(this._alarmService);
      this._alarmsDataSource.getAll = false;
      this._alarmsDataSource.severity = [Severity.Critical];

      this._alarmsDataSourceSubscription = this._alarmsDataSource
        .newAlarmStream$
        .subscribe((alarms: Alarm[]) => {
          if (alarms && alarms.length && alarms[0]) {
            if (!this._readAlarms) {
              this._readAlarms = alarms;
            } else {
              this._readAlarms.push(...alarms);
            }
          }
        });
    }
  }

  private destroyAlarmsDataSource() {
    if (this._alarmsDataSourceSubscription) {
      this._alarmsDataSourceSubscription.unsubscribe();
    }

    this._alarmsDataSource = undefined;

    if (this._readAlarms) {
      /* TODO(andrew): mark read API has been modified to only allow 100 alarms in a single request. This was done to fix another issue. */
      this._userService.user$.pipe(
        pluck('username'),
        mergeMap((username: string) => this._alarmService.markAsRead(username, this._readAlarms.slice(0, 100)))
      ).subscribe((_: Observable<any>) => {
        this._alarmsDataSource = undefined;
        this._readAlarms = undefined;
      });
    }
  }

  public onOpenAlarmDropdown() {
    this._isAlarmDropdownOpen = true;

    this.initAlarmsDataSource();
  }

  public onAlarmDropdownBackdropClick() {
    this._isAlarmDropdownOpen = false;

    this.destroyAlarmsDataSource();
  }

  public onHide() {
    this._localStorageService.write(LocalStorageKey.HIDE_ALARM_BANNER, true);
    this._alarmService.setBanner(false);
  }

  public get alarmsCount(): number {
    return this._alarmsCount;
  }

  public get isAlarmDropdownOpen(): boolean {
    return this._isAlarmDropdownOpen;
  }

  public get alarmDropdownConnectedPositions(): ConnectedPosition[] {
    return this._alarmDropdownConnectedPositions;
  }

  public get alarmsDataSource(): AlarmDataSource {
    return this._alarmsDataSource;
  }
}
