import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';

import { TenantRoutingModule } from './tenant-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ChartsModule } from '../charts/charts.module';
import { PipelineModule } from '../pipeline/pipeline.module';

import { TenantService } from './tenant.service';
import { TenantsComponent } from './tenants.component';
import { TenantInfoComponent } from './tenant-info.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ServiceDesignerModule } from '../service-designer/service-designer.module';

@NgModule({
  imports: [
    CommonModule,
    TenantRoutingModule,
    OverlayModule,
    FlexLayoutModule,
    MatTabsModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    SharedModule,
    ChartsModule,
    PipelineModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonToggleModule,
    MatTableModule,
    ServiceDesignerModule
  ],
  declarations: [
    TenantsComponent,
    TenantInfoComponent
  ],
  providers: [TenantService]
})
export class TenantModule { }
