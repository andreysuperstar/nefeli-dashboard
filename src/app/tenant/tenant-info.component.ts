import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { MatSelectionListChange, MatListOption } from '@angular/material/list';

import { Observable, Subscription, combineLatest, throwError, of } from 'rxjs';
import { tap, mergeMap, map, mergeMapTo, filter } from 'rxjs/operators';

import { saveAs } from 'file-saver';

import { ServiceConfig } from 'rest_client/pangolin/model/serviceConfig';
import { Pipeline, SERVICE_TOTAL } from '../pipeline/pipeline.model';
import { UnitType, ZoomEvent } from './../shared/chart-line/chart-line.component';
import { Range } from '../shared/date-range/date-range.component';
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { TenantService, Tenant } from './tenant.service';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { PipelineService } from '../pipeline/pipeline.service';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';
import { UserService } from '../users/user.service';
import { Column, ColumnType, Row } from '../shared/table/table.component';
import { LoggerService } from '../shared/logger.service';
import { Site } from 'rest_client/pangolin/model/site';
import { Service } from 'rest_client/pangolin/model/service';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { CreateServiceDialogComponent } from '../service-designer/create-service-dialog/create-service-dialog.component';
import { PipelineComponent } from '../pipeline/pipeline.component';

interface SiteRow {
  id: string;
  name: string;
}

// default to 15 minutes
const DEFAULT_CHART_DURATION = 900;
const msInSec = 1000;

const DEFAULT_CHART_RANGE: Range = {
  start: new Date().getTime() / msInSec,
  duration: DEFAULT_CHART_DURATION,
  period: true
};

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

export interface NFOption {
  id: string;
  name: string;
  selected?: boolean;
}

@Component({
  selector: 'nef-tenant-info',
  templateUrl: './tenant-info.component.html',
  styleUrls: ['./tenant-info.component.less']
})
export class TenantInfoComponent implements OnInit, OnDestroy {
  @ViewChild('pipeline') private _pipelineComponent: PipelineComponent;

  private _tenants: Tenant[];
  private _activeTenant: Tenant;
  private _chartRange: Range = DEFAULT_CHART_RANGE;
  private _chartStartTime: number;
  private _chartDuration: number = DEFAULT_CHART_DURATION;
  private _selectedMenuIndex = 0;
  private _subscription = new Subscription();
  private _tabTransitioning = false;
  private _selectedPipelines: Pipeline[] = [SERVICE_TOTAL];
  private _selectedNFs: string[] = [];
  private _selectedService: ServiceConfig;
  private _selectedPipeline: Pipeline;
  private _isLoading: boolean;
  private _isChangingService = false;
  private _sites: Site[];
  private _dataSource: SiteRow[] = [];
  private _perfTraceEnabled = false;
  private _nfOptions: NFOption[];
  private _serviceSubscription: Subscription;
  private _dialogSubscription: Subscription;
  private _canvasHeight = 370;

  constructor(
    private _servicesService: ServicesService,
    private _tenantService: TenantService,
    private _pipelineService: PipelineService,
    private _route: ActivatedRoute,
    private _store: LocalStorageService,
    private _userService: UserService,
    private _logger: LoggerService,
    private _router: Router,
    private _dialog: MatDialog
  ) { }

  public ngOnInit(): void {
    this._isLoading = true;
    const subscription = combineLatest([
      this._route.params,
      this._userService.isTenant$
    ])
      .pipe(
        tap(([
          { id },
          _
        ]) => {
          if (!id) {
            throwError('Tenant not specified');
          }
        }),
        mergeMap(([params, isTenantUser]: [Params, boolean]): Observable<[Tenant[], string, string]> => {
          const tenantId: string = params.id;
          const serviceId: string = params.serviceId;

          const obs: Observable<Tenant[]> = isTenantUser ?
            this._tenantService.getTenant(tenantId).pipe(map((t: Tenant): Tenant[] => [t]))
            : this._tenantService.getTenants().pipe(map(({ tenants }: Tenants): Tenant[] => tenants));

          return obs.pipe(
            map((t: Tenant[]): [Tenant[], string, string] => [t, tenantId, serviceId])
          );
        }),
        mergeMap(([tenants, tenantId, serviceId]) => {
          this._tenants = tenants;

          const tenant: Tenant = tenants.find(({ identifier }: Tenant): boolean => identifier === tenantId);

          this._activeTenant = { ...tenant };

          return this._tenantService.getTenantSites(tenant).pipe(
            map((sites): [Tenant, string, string] => {
              this._sites = sites;

              this.setDataSource();

              return [tenant, tenantId, serviceId];
            })
          );
        }),
        mergeMap(([tenant, tenantId, serviceId]) => {
          return this._pipelineService
            .getTenantPipelines(tenant)
            .pipe(
              map((pipelines: Service[]): [Pipeline[], string, string] => [
                pipelines?.map((service: Service): Pipeline => {
                  return {
                    ...service.config,
                    hasDraft: service.status?.hasDraft ?? false
                  };
                }),
                tenantId,
                serviceId
              ])
            );
        }),
        mergeMap(([pipelines, tenantId, serviceId]) => {
          return pipelines?.length && serviceId && serviceId !== 'overview'
            ? this._servicesService
              .getService(tenantId, serviceId)
              .pipe(
                tap(service => {
                  this._selectedService = service.config;
                }),
                map((_): [Pipeline[], string] => [pipelines, serviceId])
              )
            : of<[Pipeline[], string]>([pipelines, serviceId]);
        })
      )
      .subscribe(([pipelines, serviceId]: [Pipeline[], string]) => {
        this._isLoading = false;
        this._activeTenant.pipelines = pipelines;
        this._isChangingService = false;

        this._selectedPipeline = this._activeTenant.pipelines.find(
          ({ identifier }) => identifier === serviceId
        );

      }, (e) => {
        this._logger.error(e);
      });

    this._subscription.add(subscription);
    const pipelineNFsSubscription = this._pipelineService.pipelineNFs$.subscribe((nfs: string[]) => {
      this._nfOptions = [];
      this._selectedNFs = nfs;
      nfs.forEach((nf: string) => {
        this._nfOptions.push({
          id: nf,
          name: nf,
          selected: true
        });
      });
    });
    this._subscription.add(pipelineNFsSubscription);
    this.getChartRange();
  }

  private setDataSource() {
    this._dataSource = this._sites.map<SiteRow>(({
      config: { identifier, name }
    }) => ({
      id: identifier,
      name
    }));
  }

  public onDownloadPolicy() {
    this._serviceSubscription?.unsubscribe();

    this._serviceSubscription = this._servicesService
      .getService(this._activeTenant.identifier, this._selectedPipeline.identifier)
      .subscribe(service => {
        this._selectedService = service.config;

        const data = JSON.stringify(service.config, undefined, 2);

        const blob: Blob = new Blob([data], {
          type: 'application/json'
        });

        const filename = `${service.config.name}_policy.json`;

        saveAs(blob, filename);
      });
  }

  public filterChange(ev: MatSelectionListChange) {
    this._selectedPipelines = ev.source.selectedOptions.selected.map(
      (option: MatListOption): Pipeline => option.value
    );
  }

  public onZoomEvent(event: ZoomEvent) {
    const range: Range = {
      start: event.start.unix(),
      duration: event.end.diff(event.start) / msInSec,
      period: false
    };
    this._chartRange = range;
    this.onDateSelect(range);
  }

  public onAddService() {
    const dialogRef = this._dialog.open(CreateServiceDialogComponent, DIALOG_CONFIG);

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((templateId: string) => {
        this._router.navigate(['/tenants', this.activeTenantId, 'service-designer', templateId]);
      });
  }

  public updateCanvasHeight() {
    const padding = 60;
    this._canvasHeight = this._pipelineComponent?.getOptimalCanvasHeight() ?? this._canvasHeight;
    this._canvasHeight += padding;
  }

  public ngOnDestroy() {
    this._serviceSubscription?.unsubscribe();
    this._dialogSubscription?.unsubscribe();

    this._subscription.unsubscribe();
  }

  public get tenants(): Tenant[] {
    return this._tenants;
  }

  public get activeTenant(): Tenant {
    return this._activeTenant;
  }

  public get filterOptions(): Pipeline[] {
    if (this._activeTenant) {
      return [SERVICE_TOTAL, ...this._activeTenant.pipelines];
    } else {
      return undefined;
    }
  }

  public get activeTenantId(): string {
    return this._activeTenant && this.activeTenant.identifier;
  }

  public get selectedMenuIndex(): number {
    return this._selectedMenuIndex;
  }

  public set selectedMenuIndex(index: number) {
    this._selectedMenuIndex = index;
  }

  public get selectedService(): ServiceConfig {
    return this._selectedService;
  }

  public get chartRange(): Range {
    return this._chartRange;
  }

  public get chartStartTime(): number {
    return this._chartStartTime;
  }

  public get chartDuration(): number {
    return this._chartDuration;
  }

  private getChartRange(): void {
    const data: string | null = this._store.read(LocalStorageKey.TENANT_CHART_DATE_RANGE);

    if (data) {
      this._chartRange = JSON.parse(data);
    }
  }

  public onDateSelect({ start, duration, period }: Range): void {
    this._chartStartTime = (period) ? undefined : start;
    this._chartDuration = duration;
    this._store.write(
      LocalStorageKey.TENANT_CHART_DATE_RANGE,
      JSON.stringify({ start, duration, period })
    );
  }

  public get tabTransitioning(): boolean {
    return this._tabTransitioning;
  }

  public set tabTransitioning(val: boolean) {
    this._tabTransitioning = val;
  }

  public get selectedPipelines(): Pipeline[] {
    return this._selectedPipelines;
  }

  public get selectedNFs(): string[] {
    return this._selectedNFs;
  }

  public get UnitType(): typeof UnitType {
    return UnitType;
  }

  public get selectedPipeline(): Pipeline {
    return this._selectedPipeline;
  }

  public get isLoading(): boolean {
    return this._isLoading;
  }

  public get isChangingService(): boolean {
    return this._isChangingService;
  }

  public set isChangingService(val: boolean) {
    this._isChangingService = val;
  }

  public get sites(): Site[] {
    return this._sites;
  }

  public get columns(): Column[] {
    return [{ name: 'name', type: ColumnType.ICON }];
  }

  public get dataSource(): SiteRow[] {
    return this._dataSource;
  }

  public get perfTraceEnabled(): boolean {
    return this._perfTraceEnabled;
  }

  public onSiteClick(site: Row) {
    site = site as SiteRow;
    this._router.navigate(['sites', site.id]);
  }

  public onTogglePerfTrace(enabled: boolean) {
    this._perfTraceEnabled = enabled;
  }

  public get nfOptions(): NFOption[] {
    return this._nfOptions;
  }

  public onNFFilterChange(ev: MatSelectionListChange) {
    this._selectedNFs = ev.source.selectedOptions.selected.map(
      (option: MatListOption): string => option.value.name
    );
  }

  public get isTenantAdmin$(): Observable<boolean> {
    return this._userService.isTenantAdmin$;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get canvasHeight(): number {
    return this._canvasHeight;
  }
}
