import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { TenantService, Tenant, TenantStats } from './tenant.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { CommonChart } from '../charts/common-chart';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { BASE_PATH as HEIMDALLR_BASE_PATH } from 'rest_client/heimdallr/variables';
import { BASE_PATH as STATS_BASE_PATH } from 'rest_client/pangolin/variables';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { TenantStatsRequest } from 'rest_client/pangolin/model/tenantStatsRequest';

describe('TenantService', () => {
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: HEIMDALLR_BASE_PATH,
          useValue: '/auth'
        },
        {
          provide: STATS_BASE_PATH,
          useValue: '/v1'
        },
        TenantService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', inject([TenantService], (service: TenantService) => {
    expect(service).toBeTruthy();
  }));

  it('should get all tenants', inject([TenantService], (service: TenantService) => {
    service.getTenants().subscribe(({ tenants }: Tenants) => {
      expect(tenants[0].identifier).toBe('1');
      expect(tenants[0].name).toBe('Tenant A');
      expect(tenants[1]).toBeUndefined();
    });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush({
      tenants: [{
        identifier: '1',
        name: 'Tenant A'
      } as Tenant],
      pagination: {
        index: 0,
        size: 1
      }
    });
  }));

  it('should get tenant by id', inject([TenantService], (service: TenantService) => {
    service.getTenant('-1').subscribe((tenant: Tenant) => {
      expect(tenant.identifier).toBe('-1');
      expect(tenant.name).toBe('Tenant A');
    });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants/-1`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush({ identifier: '-1', name: 'Tenant A' } as Tenant);
  }));

  it('should stream tenant stats', inject([TenantService], (service: TenantService) => {
    service.streamTenantStats('-1', 1000).pipe(
      take(1)
    ).subscribe((stats: TenantStats) => {
      expect(stats.services).toBe(2);
      expect(stats.status).toBe('good');
      expect(stats.throughputIn).toBe(608399.6796541876);
      expect(stats.throughputOut).toBe(604789.4767936202);
      expect(stats.latency[0].name).toBe('ddp_pid2');
      expect(stats.latency[0].stats[0][0]).toBe(1530655649.912);
      expect(stats.latency[0].stats[0][1]).toBe('0');

      expect(stats.packetLoss[0].name).toBe('Unclassified');
      expect(stats.packetLoss[0].stats[0][0]).toBe(1530655649.912);
      expect(stats.packetLoss[0].stats[0][1]).toBe('262307389');
    });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/-1/stats`);

    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');
    req.flush({
      'id': '',
      'name': '',
      'services': 2,
      'throughputOut': 604789.4767936202,
      'throughputIn': 608399.6796541876,
      'packetLoss': [
        {
          'name': 'Unclassified',
          'site': 'eric_site',
          'stats': [[1530655649.912, '262307389']]
        },
        {
          'name': 'ddp_pid2',
          'site': 'eric_site',
          'stats': [[1530655649.912, '12584745']]
        },
        {
          'name': 'customer0_pid4',
          'site': 'eric_site',
          'stats': [[1530655649.912, '4500562940']]
        }
      ],
      'latency': [{
        'name': 'ddp_pid2',
        'site': 'eric_site',
        'stats': [[1530655649.912, '0']]
      }],
      'status': 'good'
    } as TenantStats);
  }));

  it('should poll from tenant throughput stats', fakeAsync(
    inject([TenantService], (service: TenantService) => {
      const rate = 21600;
      const step = CommonChart.getOptimalStep(rate);
      const pollInterval = CommonChart.getOptimalPollInterval(rate);

      const subscription = service.streamTenantStats(
        'my_tenant',
        pollInterval,
        StatFilter.THROUGHPUT,
        rate,
        step,
        ['my_service1', 'my_service2']
      ).subscribe();

      let req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/my_tenant/stats`);
      req.flush({});

      const body: TenantStatsRequest = {
        services: ['my_service1', 'my_service2'],
        filter: {
          filter: [StatFilter.THROUGHPUT]
        },
        range: {
          start: undefined,
          duration: rate,
          step
        }
      };
      expect(req.request.body).toEqual(body);

      tick(pollInterval);

      req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/my_tenant/stats`);
      req.flush({});
      body.range.duration = 22;
      expect(req.request.body).toEqual(body);

      subscription.unsubscribe();
    })));
});
