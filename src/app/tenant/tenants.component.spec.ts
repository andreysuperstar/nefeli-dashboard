import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { TenantService, Tenant, TenantStats } from './tenant.service';

import { ByteConverterPipe } from '../pipes/byte-converter.pipe';
import { DurationPipe } from '../pipes/duration.pipe';

import { TenantsComponent } from './tenants.component';
import { TenantInfoComponent } from './tenant-info.component';
import { TableComponent } from '../shared/table/table.component';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { PipelineComponent } from '../pipeline/pipeline.component';
import { DateRangeComponent } from '../shared/date-range/date-range.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { ToggleComponent } from '../shared/toggle/toggle.component';
import { ChartThroughputComponent } from '../charts/chart-throughput.component';
import { ChartLatencyComponent } from '../charts/chart-latency.component';
import { ChartPacketlossComponent } from '../charts/chart-packetloss.component';
import { ChartLineComponent } from '../shared/chart-line/chart-line.component';
import { MenuComponent } from '../shared/menu/menu.component';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { MatStepperModule } from '@angular/material/stepper';
import { BASE_PATH as HEIMDALLR_BASE_PATH } from 'rest_client/heimdallr/variables';
import { BASE_PATH as STATS_BASE_PATH } from 'rest_client/pangolin/variables';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TenantStatsRequest } from 'rest_client/pangolin/model/tenantStatsRequest';

describe('TenantsComponent', () => {
  let component: TenantsComponent;
  let fixture: ComponentFixture<TenantsComponent>;
  let httpTestingController: HttpTestingController;
  let el: HTMLElement;
  let router = {
    navigate: jasmine.createSpy('navigate')
  };
  const mockTenantStats1: TenantStats = {
    'id': '1',
    'services': 2,
    'throughputOut': 6382,
    'throughputIn': 6482,
    'status': 'good',
    'packetLoss': [
      {
        'name': 'Unclassified',
        'site': 'anton_site',
        'stats': [
          [
            1531154025,
            '356.6'
          ]
        ]
      },
      {
        'name': 'customer0_pid4',
        'site': 'eric_site',
        'stats': [
          [
            1531154025,
            '21400.6'
          ]
        ]
      },
      {
        'name': 'ddp_pid2',
        'site': 'andrew_site',
        'stats': [
          [
            1531154025,
            '0'
          ]
        ]
      }
    ],
    'latency': []
  } as TenantStats;

  const mockTenantStats2: TenantStats = {
    'id': '2',
    'services': 5,
    'throughputOut': 20000,
    'throughputIn': 20001,
    'status': 'good',
    'packetLoss': [
      {
        'name': 'Unclassified',
        'site': 'eric_site',
        'stats': [
          [
            1531154025,
            '1000'
          ]
        ]
      },
      {
        'name': 'customer0_pid4',
        'site': 'anton_site',
        'stats': [
          [
            1531154025,
            '20000'
          ]
        ]
      }
    ],
    'latency': []
  } as TenantStats;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        OverlayModule,
        MatTabsModule,
        MatMenuModule,
        MatSlideToggleModule,
        MatCardModule,
        MatTableModule,
        MatCheckboxModule,
        NgbDatepickerModule,
        MatListModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatSelectModule,
        MatStepperModule,
        ReactiveFormsModule,
        FormsModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        DurationPipe,
        TenantsComponent,
        TenantInfoComponent,
        TableComponent,
        AvatarComponent,
        PipelineComponent,
        DateRangeComponent,
        LegendComponent,
        ToggleComponent,
        ChartThroughputComponent,
        ChartLatencyComponent,
        ChartPacketlossComponent,
        ChartLineComponent,
        MenuComponent,
        ChartHeaderComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: HEIMDALLR_BASE_PATH,
          useValue: '/auth'
        },
        {
          provide: STATS_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: Router,
          useValue: router
        },
        DecimalPipe,
        ByteConverterPipe,
        TenantService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TenantsComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${environment.restServer}/auth/tenants`);
    expect(req.request.method).toBe('GET');
    req.flush({
      tenants: [
        {
          identifier: '1',
          name: 'Tenant A'
        },
        {
          identifier: '2',
          name: 'Tenant B'
        }
      ] as Tenant[],
      metadata: {
        count: 2,
        filter: [],
        search: [],
        sort: [],
        total: 2
      }
    });
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve all tenants on load', () => {
    expect(component.tenants[0].identifier).toBe('1');
    expect(component.tenants[0].name).toBe('Tenant A');
    expect(component.tenants[1].name).toBe('Tenant B');
    expect(component.tenants[1].identifier).toBe('2');
  });

  it('should start the stats stream', () => {
    const req1 = httpTestingController
      .expectOne(`${environment.restServer}/v1/tenants/1/stats`);
    const req2 = httpTestingController
      .expectOne(`${environment.restServer}/v1/tenants/2/stats`);

    const body: TenantStatsRequest = {
      services: undefined,
      filter: {
        filter: []
      },
      range: {
        start: undefined,
        duration: 1,
        step: 2
      }
    };

    expect(req1.request.method).toBe('POST');
    expect(req1.request.responseType).toBe('json');
    expect(req1.request.body).toEqual(body);
    expect(req2.request.method).toBe('POST');
    expect(req2.request.responseType).toBe('json');
    expect(req2.request.body).toEqual(body);

    req1.flush(mockTenantStats1);
    req2.flush(mockTenantStats2);

    expect(component.tenants[0].stats.id).toBe('1');
    expect(component.tenants[0].stats.packetLossTotal).toBe(21757.199999999997);
    expect(component.tenants[0].stats.services).toBe(2);
    expect(component.tenants[0].stats.status).toBe('good');
    expect(component.tenants[0].stats.throughputIn).toBe(6482);
    expect(component.tenants[0].stats.throughputOut).toBe(6382);

    expect(component.tenants[1].stats.id).toBe('2');
    expect(component.tenants[1].stats.packetLossTotal).toBe(21000);
    expect(component.tenants[1].stats.services).toBe(5);
    expect(component.tenants[1].stats.status).toBe('good');
    expect(component.tenants[1].stats.throughputIn).toBe(20001);
    expect(component.tenants[1].stats.throughputOut).toBe(20000);
  });

  it('should navigate to tenants info page if clicked', fakeAsync(() => {
    component.onRowClick({
      id: '34',
      name: 'Tenant A'
    });

    tick();

    expect(router.navigate).toHaveBeenCalledWith(['tenants', '34']);
  }));

  it('should render tenant stats in a tenants table', () => {
    const req1 = httpTestingController
      .expectOne(`${environment.restServer}/v1/tenants/1/stats`);
    const req2 = httpTestingController
      .expectOne(`${environment.restServer}/v1/tenants/2/stats`);
    req1.flush(mockTenantStats1);
    req2.flush(mockTenantStats2);

    const body: TenantStatsRequest = {
      services: undefined,
      filter: {
        filter: []
      },
      range: {
        start: undefined,
        duration: 1,
        step: 2
      }
    };
    expect(req1.request.body).toEqual(body);
    expect(req2.request.body).toEqual(body);

    fixture.detectChanges();
    const rows = el.querySelectorAll('mat-row');
    expect(rows.length).toBe(2);
    const cells = rows[0].querySelectorAll('mat-cell');
    expect(cells.length).toBe(4);
    expect(cells[1].textContent).toBe(`${mockTenantStats1.services}`);
    expect(cells[2].textContent).toBe('6 kb');
    expect(cells[3].textContent).toBe('21,757');
  });
});
