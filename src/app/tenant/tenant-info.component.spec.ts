import { User as RestUser } from 'rest_client/heimdallr/model/user';
import { BASE_PATH as HEIMDALLR_BASE_PATH } from 'rest_client/heimdallr/variables';
import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, inject } from '@angular/core/testing';
import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { of } from 'rxjs';

import * as fileSaver from 'file-saver';

import { environment } from '../../environments/environment';

import { ServiceConfig } from 'rest_client/pangolin/model/serviceConfig';

import { Tenant } from './tenant.service';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { Services } from "rest_client/pangolin/model/services";
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { UserService } from '../users/user.service';

import { DurationPipe } from '../pipes/duration.pipe';

import { TenantInfoComponent } from './tenant-info.component';
import { TableComponent } from '../shared/table/table.component';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { DateRangeComponent } from '../shared/date-range/date-range.component';
import { MenuComponent } from '../shared/menu/menu.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { ChartThroughputComponent } from '../charts/chart-throughput.component';
import { ChartLatencyComponent } from '../charts/chart-latency.component';
import { ChartPacketlossComponent } from '../charts/chart-packetloss.component';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { ChartLineComponent } from '../shared/chart-line/chart-line.component';
import { PipelineComponent } from '../pipeline/pipeline.component';
import { ToggleComponent } from '../shared/toggle/toggle.component';
import { ServicePortMenuComponent } from '../pipeline/service-port-menu/service-port-menu.component';
import { NetworkFunctionMenuComponent } from '../pipeline/network-function-menu.component';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { NetworkFunctionComponent } from '../pipeline/network-function.component';
import { MatStepperModule } from '@angular/material/stepper';
import { Site } from 'rest_client/pangolin/model/site';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { Service } from 'rest_client/pangolin/model/service';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { MatButtonHarness } from '@angular/material/button/testing';

describe('TenantInfoComponent', () => {
  let component: TenantInfoComponent;
  let userService: UserService;
  let fixture: ComponentFixture<TenantInfoComponent>;
  let debugEl: DebugElement;
  let httpTestingController: HttpTestingController;
  let documentRootLoader: HarnessLoader;
  let loader: HarnessLoader;

  const mockTenantPipelines: Service[] = [
    {
      config: {
        identifier: "1",
        name: "Service 1",
      },
    },
    {
      config: {
        identifier: "2",
        name: "Service 2",
      },
    },
    {
      config: {
        identifier: "3",
        name: "Service 3",
      },
      status: {
        hasDraft: true
      },
    },
  ];

  const mockService: ServiceConfig = {
    name: mockTenantPipelines[1].config.name
  };

  const getUserResponse = (scope = 'system'): RestUser => {
    return {
      username: 'Andrew',
      email: 'andrew@nefeli.io',
      roles: {
        scope,
        id: '-1',
        roles: ['admin', 'user']
      }
    };
  }

  enum UserType {
    SystemAdmin,
    TenantAdmin,
    SystemUser,
    TenantUser
  }

  const mockUsers: RestUser[] = [
    {
      username: 'SystemAdmin',
      email: 'admin@nefeli.io',
      roles: {
        scope: 'system',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'TenantAdmin',
      email: 'admin@nefeli.io',
      roles: {
        scope: 'tenant',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'SystemUser',
      email: 'user@nefeli.io',
      roles: {
        scope: 'system',
        id: 'Nefeli Networks',
        roles: ['user']
      }
    },
    {
      username: 'TenantUser',
      email: 'user@nefeli.io',
      roles: {
        scope: 'tenant',
        id: 'Nefeli Networks',
        roles: ['user']
      }
    }
  ]

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        OverlayModule,
        MatMenuModule,
        MatListModule,
        MatCardModule,
        MatTabsModule,
        MatTableModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatSnackBarModule,
        MatDialogModule,
        MatSelectModule,
        NgbDatepickerModule,
        MatStepperModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        MatIconModule,
        MatTooltipModule,
        MatDividerModule,
        MatPaginatorModule
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: HEIMDALLR_BASE_PATH,
          useValue: '/auth'
        },
        {
          provide: PANGOLIN_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              id: '2',
              serviceId: mockTenantPipelines[1].config.identifier
            }),
            snapshot: {
              params: {
                id: 12345
              }
            }
          }
        },
        ServicesService,
        DecimalPipe,
        UserService,
        PipelineNamePipe
      ],
      declarations: [
        DurationPipe,
        TenantInfoComponent,
        TableComponent,
        AvatarComponent,
        DateRangeComponent,
        MenuComponent,
        LegendComponent,
        ChartThroughputComponent,
        ChartLatencyComponent,
        ChartPacketlossComponent,
        ChartHeaderComponent,
        ChartLineComponent,
        PipelineComponent,
        ToggleComponent,
        ServicePortMenuComponent,
        NetworkFunctionMenuComponent,
        PipelineNamePipe,
        NetworkFunctionComponent
      ]
    });

    userService = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  const fetchTenantData = (params: any = {}) => {
    const { isTenant = false, setUser = true } = params;
    if (setUser) {
      userService.setUser(getUserResponse(isTenant ? 'tenant' : 'system'));
    }

    fixture = TestBed.createComponent(TenantInfoComponent);
    documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();

    if (isTenant) {
      const req = httpTestingController.expectOne(`${environment.restServer}/auth/tenants/2`);
      req.flush({
        identifier: '2',
        name: 'Tenant B'
      } as Tenant);
    } else {
      const req = httpTestingController.expectOne(`${environment.restServer}/auth/tenants`);

      expect(req.request.method).toBe('GET');

      req.flush({
        tenants: [{
          identifier: '1',
          name: 'Tenant A'
        }, {
          identifier: '2',
          name: 'Tenant B'
        }] as Tenant[],
        pagination: {
          index: 0,
          size: 2
        }
      });
    }

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/sites`);
    expect(req2.request.method).toBe('GET');
    req2.flush({ 'sites': ['eric_site', 'andrew_site', 'anton_site'] });

    const site1Request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/eric_site`
    );

    const site2Request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site`
    );

    const site3Request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/anton_site`
    );

    const mockSiteResponse: Site = {
      config: {
        identifier: 'eric_site',
        name: 'Eric Site'
      }
    };

    const mockSiteResponse2: Site = {
      config: {
        identifier: 'andrew_site',
        name: 'Andrew Site'
      }
    };

    const mockSiteResponse3: Site = {
      config: {
        identifier: 'anton_site',
        name: 'Anton Site'
      }
    };

    site1Request.flush(mockSiteResponse);
    site2Request.flush(mockSiteResponse2);
    site3Request.flush(mockSiteResponse3);
  };

  const fetchTenantServices = () => {
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/2/services?sort=asc(name)`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    const mockPipelinesMetadata: Services = {
      services: mockTenantPipelines
    };

    req.flush(mockPipelinesMetadata);
  };

  const fetchService = () => {
    const serviceRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/2/services/${mockTenantPipelines[1].config.identifier}`
    );

    serviceRequest.flush({ config: mockService });
  };

  it('should create', () => {
    fetchTenantData();
    expect(component).toBeTruthy();
  });

  it('should get list of tenants', () => {
    fetchTenantData();
    expect(component.tenants[0].identifier).toBe('1');
    expect(component.tenants[0].name).toBe('Tenant A');
    expect(component.tenants[1].identifier).toBe('2');
    expect(component.tenants[1].name).toBe('Tenant B');
  });

  it('should set active tenant based on param', () => {
    fetchTenantData();
    expect(component.activeTenant.name).toBe('Tenant B');
    expect(component.activeTenant.identifier).toBe('2');
  });

  it('should display active tenant', () => {
    fetchTenantData();
    fixture.detectChanges();
    expect(debugEl.query(
      By.css('button.menu-trigger > span')
    ).nativeElement.textContent.trim()).toBe('Tenant B');
    expect(debugEl.query(
      By.css('nef-avatar .cap-letter')
    ).nativeElement.textContent.trim()).toBe('T');
  });

  it('should get pipelines for active tenant', () => {
    fetchTenantData();
    fetchTenantServices();
    fetchService();

    expect(component.activeTenant.pipelines[0].identifier).toBe(
      mockTenantPipelines[0].config.identifier,
      `valid pipeline ${mockTenantPipelines[0].config.identifier} id`
    );

    expect(component.activeTenant.pipelines[0].name).toBe(
      mockTenantPipelines[0].config.name,
      `valid pipeline ${mockTenantPipelines[0].config.identifier} name`
    );

    expect(component.activeTenant.pipelines[1].identifier).toBe(
      mockTenantPipelines[1].config.identifier,
      `valid pipeline ${mockTenantPipelines[1].config.identifier} id`
    );

    expect(component.activeTenant.pipelines[1].name).toBe(
      mockTenantPipelines[1].config.name,
      `valid pipeline ${mockTenantPipelines[1].config.identifier} name`
    );

    expect(component.activeTenant.pipelines[2].identifier).toBe(
      mockTenantPipelines[2].config.identifier,
      `valid pipeline ${mockTenantPipelines[2].config.identifier} id`
    );

    expect(component.activeTenant.pipelines[2].name).toBe(
      mockTenantPipelines[2].config.name,
      `valid pipeline ${mockTenantPipelines[2].config.identifier} name`
    );
  });

  it('should go to proper tenant and pipeline based on route', fakeAsync(() => {
    fetchTenantData();
    fetchTenantServices();
    fetchService();
    tick();

    expect(component.selectedPipeline.name).toBe(
      mockTenantPipelines[1].config.name,
      `valid pipeline ${mockTenantPipelines[2].config.identifier} name`
    );
  }));

  it('should render overview tab', () => {
    fetchTenantData();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/services?sort=asc(name)`);
    const response: Services = {
      services: []
    };
    req.flush(response);

    expect(debugEl.queryAll(By.css('.mat-tab-labels')).length).toBe(0);
  });

  it('should display Overview charts', () => {
    fetchTenantData();
    fetchTenantServices();
    fetchService();
    fixture.detectChanges();

    const servicesEl: HTMLElement = debugEl.query(
      By.css('section.services')
    ).nativeElement;

    expect(servicesEl.querySelector('nef-chart-throughput')).toBeDefined();
    expect(servicesEl.querySelector('nef-chart-latency')).toBeDefined();
    expect(servicesEl.querySelector('nef-chart-packetloss')).toBeDefined();
  });

  it('should render welcome text when tenant has no services', inject([OverlayContainer], async (overlayContainer: OverlayContainer) => {
    userService.setUser(mockUsers[UserType.TenantAdmin]);
    fetchTenantData({ isTenant: true, setUser: false });
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/services?sort=asc(name)`);
    const response: Services = {
      services: []
    };
    req.flush(response);

    fixture.detectChanges();

    const addServiceEl: HTMLElement = debugEl.query(
      By.css('section.add-service')
    ).nativeElement;
    const headingEl: HTMLHeadingElement = debugEl.query(
      By.css('section.add-service h1')
    ).nativeElement;
    expect(addServiceEl).not.toBeNull();
    expect(headingEl.textContent.trim()).toBe('Add your first service');

    const addServiceButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add Service'
    }));

    await addServiceButton.click();

    const createServiceDialog = await documentRootLoader.getHarness(MatDialogHarness);

    await createServiceDialog.close();

    overlayContainer.ngOnDestroy();
  }));

  it('should show add service section only for tenant admin and system admin', () => {
    userService.setUser(mockUsers[UserType.TenantUser]);
    fetchTenantData({ isTenant: true, setUser: false });
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/services?sort=asc(name)`);
    const response: Services = {
      services: []
    };
    req.flush(response);

    fixture.detectChanges();

    let addServiceEl = debugEl.nativeElement.querySelector('section.add-service');
    let headingEl = addServiceEl.querySelector('h1');
    let addServiceButtonEl = addServiceEl.querySelector('button');

    expect(addServiceEl).not.toBeNull();
    expect(headingEl.textContent.trim()).toBe('No services');
    expect(addServiceButtonEl).toBeNull();

    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();

    addServiceEl = debugEl.nativeElement.querySelector('section.add-service');
    headingEl = addServiceEl.querySelector('h1');
    addServiceButtonEl = addServiceEl.querySelector('button');
    expect(headingEl.textContent.trim()).toBe('Add your first service');
    expect(addServiceButtonEl).not.toBeNull();

    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();

    addServiceEl = debugEl.nativeElement.querySelector('section.add-service');
    headingEl = addServiceEl.querySelector('h1');
    addServiceButtonEl = addServiceEl.querySelector('button');
    expect(headingEl.textContent.trim()).toBe('No services');
    expect(addServiceButtonEl).toBeNull();
  });

  it('should display active sites for tenant', () => {
    fetchTenantData();
    fetchTenantServices();
    fetchService();

    component['_selectedPipeline'] = undefined;
    fixture.detectChanges();

    const sites = debugEl.queryAll(By.css('.sites mat-row'));
    expect(sites.length).toBe(3);
    expect(sites[0].queryAll(By.css('span'))[1].childNodes[0].nativeNode.textContent).toBe('Eric Site');
    expect(sites[1].queryAll(By.css('span'))[1].childNodes[0].nativeNode.textContent).toBe('Andrew Site');
    expect(sites[2].queryAll(By.css('span'))[1].childNodes[0].nativeNode.textContent).toBe('Anton Site');
  });

  it('should display add service button in navigation tabs', () => {
    userService.setUser(mockUsers[UserType.TenantAdmin]);

    fetchTenantData({ isTenant: true, setUser: false });
    fetchTenantServices();
    fetchService();
    fixture.detectChanges();

    let addServiceButton = debugEl.nativeElement.querySelector('nav.add-service');
    expect(addServiceButton).not.toBeNull();

    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();
    addServiceButton = debugEl.nativeElement.querySelector('nav.add-service');
    expect(addServiceButton).not.toBeNull();

    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    addServiceButton = debugEl.nativeElement.querySelector('nav.add-service');
    expect(addServiceButton).toBeNull();

    userService.setUser(mockUsers[UserType.TenantUser]);
    fixture.detectChanges();
    addServiceButton = debugEl.nativeElement.querySelector('nav.add-service');
    expect(addServiceButton).toBeNull();
  });

  it('should go to site page when click on site', () => {
    fetchTenantData();
    spyOn(component['_router'], 'navigate');

    fetchTenantServices();
    fetchService();

    component['_selectedPipeline'] = undefined;
    fixture.detectChanges();

    const sites = debugEl.queryAll(By.css('.sites mat-row'));
    sites[1].nativeElement.click();
    expect(component['_router'].navigate).toHaveBeenCalledWith(['sites', 'andrew_site']);
  });

  it('should NOT query for tenant list if tenant user', () => {
    fetchTenantData({ isTenant: true });
    expect(component.tenants.length).toBe(1);
    expect(component.tenants[0].identifier).toBe("2");
    expect(component.tenants[0].name).toBe("Tenant B");
  });

  it('should download a policy file', () => {
    fetchTenantData();
    fetchTenantServices();
    fetchService();
    fixture.detectChanges();

    const downloadPolicyButtonDe = fixture.debugElement.query(By.css('.download-policy'));

    expect(downloadPolicyButtonDe).toBeDefined('render Download Policy button');

    expect(downloadPolicyButtonDe.nativeElement.textContent).toBe(
      'Download Policy',
      'render Download Policy button text'
    );

    spyOn(fileSaver, 'saveAs')
      .and.callFake((({ type }: Blob, filename: string) => {
        expect(type).toBe('application/json', 'correct policy file content type');

        expect(
          filename.startsWith(`${mockService.name}_policy`)
        ).toBeTruthy('correct policy file name');

        expect(filename.endsWith('.json')).toBeTruthy('correct policy file extension');
      }) as typeof fileSaver.saveAs);

    downloadPolicyButtonDe.nativeElement.click();

    const serviceRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/tenants/2/services/${mockTenantPipelines[1].config.identifier}`
    });

    serviceRequest.flush({ config: mockService });

    expect(fileSaver.saveAs).toHaveBeenCalled();
  });

  it('should display Edit Service button for Tenant Admin and System Admin', () => {
    userService.setUser(mockUsers[UserType.TenantAdmin]);

    fetchTenantData({ isTenant: true, setUser: false });
    fetchTenantServices();
    fetchService();
    fixture.detectChanges();

    let editButton = debugEl.nativeElement.querySelector('.edit-service');
    expect(editButton).not.toBeNull();
    expect(editButton.textContent).toBe('Edit Service');

    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();
    editButton = debugEl.nativeElement.querySelector('.edit-service');
    expect(editButton).not.toBeNull();
  });

  it('should hide Edit Service button for non Admin User', () => {
    userService.setUser(mockUsers[UserType.SystemUser]);

    fetchTenantData({ isTenant: false, setUser: false });
    fetchTenantServices();
    fetchService();
    fixture.detectChanges();

    let editButton = debugEl.nativeElement.querySelector('.edit-service');
    expect(editButton).toBeNull();

    userService.setUser(mockUsers[UserType.TenantUser]);
    fixture.detectChanges();
    editButton = debugEl.nativeElement.querySelector('.edit-service');
    expect(editButton).toBeNull();
  });

  it('should open create service dialog', async () => {
    userService.setUser(mockUsers[UserType.TenantAdmin]);
    fetchTenantData({ isTenant: true, setUser: false });
    spyOn(component['_router'], 'navigate');
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/services?sort=asc(name)`);
    const response: Services = {
      services: []
    };
    req.flush(response);
    fixture.detectChanges();

    const addServiceButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Add Service'
    }));

    await addServiceButton.click();
    await documentRootLoader.getHarness(MatDialogHarness);
  });
});
