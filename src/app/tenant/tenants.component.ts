import { Component, OnInit, OnDestroy } from '@angular/core';
import { TenantService, Tenant, TenantStats } from './tenant.service';
import { Subscription } from 'rxjs';
import { Column, ColumnType, Row } from '../shared/table/table.component';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { TenantRow } from '../shared/table/row-types';

import { ConverterUnit, ByteConverterPipe } from '../pipes/byte-converter.pipe';
import { Tenants } from 'rest_client/heimdallr/model/tenants';

const TenantColumns: Column[] = [
  {
    name: 'name',
    label: 'name',
    type: ColumnType.ICON
  },
  {
    name: 'services'
  },
  {
    label: 'сurrent throughtput',
    name: 'throughputOut',
  },
  {
    label: 'loss (packet/sec)',
    name: 'packetLossTotal',
  }
];

const Config = {
  POLL_INTERVAL: 5000,
  DURATION: 1,
  STEP: 2
};

@Component({
  selector: 'nef-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.less']
})
export class TenantsComponent implements OnInit, OnDestroy {
  private _tenants: Tenant[];
  private _dataSource: TenantRow[] = [];
  private _statStreamSubscriptions = new Array<Subscription>();

  constructor(
    private _router: Router,
    private _tenantService: TenantService,
    private _byteConverter: ByteConverterPipe
  ) { }

  public ngOnInit() {
    this._tenantService.getTenants().subscribe(({ tenants }: Tenants) => {
      this._tenants = tenants;
      this.setDataSource();
      this.enableTenantStatStream();
    });
  }

  public ngOnDestroy() {
    this.disableTenantStatStream();
  }

  private enableTenantStatStream() {
    this._tenants.forEach((tenant: Tenant) => {
      this._statStreamSubscriptions.push(this._tenantService.streamTenantStats(
        tenant.identifier,
        Config.POLL_INTERVAL,
        undefined,
        Config.DURATION,
        Config.STEP
      ).pipe(
        map((stats: any): TenantStats => {
          const newStats = { ...stats };

          if (Array.isArray(stats.throughputIn)) {
            newStats.throughputIn = stats.throughputIn[stats.throughputIn.length - 1][1];
          }

          if (Array.isArray(stats.throughputOut)) {
            newStats.throughputOut = stats.throughputOut[stats.throughputOut.length - 1][1];
          }

          return newStats;
        })
      ).subscribe((stats: TenantStats) => {
        const index = this._dataSource.findIndex((row: TenantRow) => row.id === tenant.identifier);
        this.setTenantStats(this._dataSource[index], stats);
        tenant.stats = stats;
      }));
    });
  }

  private disableTenantStatStream() {
    this._statStreamSubscriptions.forEach((statStreamSubscription: Subscription) => {
      statStreamSubscription.unsubscribe();
    });

    this._statStreamSubscriptions.length = 0;
  }

  public get tenants(): Tenant[] {
    return this._tenants;
  }

  public get dataSource(): TenantRow[] {
    return this._dataSource;
  }

  private setDataSource(): void {
    this._dataSource = this._tenants.map(({ identifier, name, stats }: Tenant): TenantRow => {
      const tenantRow: TenantRow = {
        id: identifier,
        name
      };
      this.setTenantStats(tenantRow, stats);
      return tenantRow;
    });
  }

  private setTenantStats(tenantRow: TenantRow, stats: TenantStats) {
    if (stats) {
      tenantRow.services = stats.services;
      tenantRow.packetLossTotal = stats.packetLossTotal;

      if (stats.throughputOut !== undefined) {
        const [throughputOut, unit]: [number, string] = this._byteConverter.convert(
          stats.throughputOut,
          ConverterUnit.BITS
        );

        tenantRow.throughputOut = `${throughputOut} ${unit}`;
      }
    }
  }

  public get columns(): Column[] {
    return TenantColumns;
  }

  public onRowClick(row: Row) {
    row = row as TenantRow;
    this._router.navigate(['tenants', row.id]);
  }
}
