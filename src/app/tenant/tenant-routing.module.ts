import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TenantUserGuard } from '../auth/tenant-user.guard';

import { TenantsComponent } from './tenants.component';
import { TenantInfoComponent } from './tenant-info.component';
import { ServiceDesignerComponent } from '../service-designer/service-designer/service-designer.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [TenantUserGuard],
    component: TenantsComponent
  },
  {
    path: ':id',
    children: [
      {
        path: '',
        redirectTo: 'service/overview',
        pathMatch: 'full'
      },
      {
        path: 'service/:serviceId',
        component: TenantInfoComponent
      },
      {
        path: 'service-designer/:templateId',
        canActivate: [TenantUserGuard],
        component: ServiceDesignerComponent,
        data: {
          isTemplateMode: true,
        }
      },
      {
        path: 'service-designer',
        canActivate: [TenantUserGuard],
        component: ServiceDesignerComponent
      },
      {
        path: 'edit-service-designer',
        canActivate: [TenantUserGuard],
        component: ServiceDesignerComponent,
        data: {
          isEditMode: true
        }
      },
      {
        path: 'service-template',
        canActivate: [TenantUserGuard],
        component: ServiceDesignerComponent,
        data: {
          isTemplateMode: true
        }
      },
      {
        path: 'service-template/:templateId',
        canActivate: [TenantUserGuard],
        component: ServiceDesignerComponent,
        data: {
          isTemplateMode: true,
          isEditMode: true
        }
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class TenantRoutingModule { }
