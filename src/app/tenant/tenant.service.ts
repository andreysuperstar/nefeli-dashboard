import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { Observable, of, forkJoin, concat, timer, defer } from 'rxjs';
import { mergeMap, map, catchError, skip, repeat, shareReplay, mapTo } from 'rxjs/operators';
import { Pipeline } from '../pipeline/pipeline.model';
import { NFCatalog, createNFCatalog } from '../pipeline/network-function.model';
import { Site } from 'rest_client/pangolin/model/site';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { TenantsService } from 'rest_client/pangolin/api/tenants.service';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { TenantsService as TenantsServiceHeimdallr } from 'rest_client/heimdallr/api/tenants.service';
import { TenantSites } from 'rest_client/pangolin/model/tenantSites';
import { Stats } from 'rest_client/pangolin/model/stats';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { StatsService as StatsServiceDashboard } from 'rest_client/pangolin/api/stats.service';
import { PacketLossStat } from '../charts/chart.model';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';
import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';

enum SortableColumn {
  name = 'name'
}

export const NO_SERVICES_SELECTED: Pipeline[] = [
  {
    identifier: '-none-',
    name: 'None',
  }
];

export enum STATUS {
  UNKNOWN = 'unknown',
  CRITICAL = 'critical',
  WARN = 'warn',
  GOOD = 'good'
}

export interface Tenant {
  identifier?: string;
  name?: string;
  stats?: TenantStats;
  pipelines?: Pipeline[];
}

export interface TenantsResponse {
  tenants: Array<Tenant>;
  pagination: {
    index: number;
    size: number;
  };
}

export interface TenantStats extends Stats {
  name?: string;
  packetLossTotal?: number;
}

type DeleteHttpResponse = object;

@Injectable({
  providedIn: 'root'
})
export class TenantService {
  private _nfCatalog: NFCatalog;

  constructor(
    private _restSitesService: SitesService,
    private _restTenantsService: TenantsService,
    private _restTenantsServiceHeimdallr: TenantsServiceHeimdallr,
    private _restStatsServiceDashboard: StatsServiceDashboard,
    private _restNfService: NetworkFunctionsService,
    private _http: HttpClient
  ) { }

  public getTenants(index?: number, size?: number, sortColumn?: string, sortDirection?: SortDirection): Observable<Tenants> {
    const sortField: SortableColumn = SortableColumn[sortColumn];

    const sort = sortField
      ? [`${sortDirection}(${sortField})`]
      : undefined;

    return this._restTenantsServiceHeimdallr.getTenants(undefined, undefined, sort, index, size);
  }

  public getTenant(id: string): Observable<Tenant> {
    return this._restTenantsServiceHeimdallr.getTenant(id);
  }

  public get nfCatalog(): NFCatalog {
    return this._nfCatalog;
  }

  public getNfCatalog(id?: string): Observable<NFCatalog> {

    if (!id) {
      return this._restNfService.getNfs().pipe(
        map((response: RESTNFCatalog): NFCatalog => createNFCatalog(response)),
        catchError(() => of({})));
    }

    // TODO(eric): need to figure out smart way to update the cache during run time
    return this.getUpdatedNfCatalog(id);
  }

  public getUpdatedNfCatalog(id: string): Observable<NFCatalog> {
    return this._restTenantsService.getTenantNfs(id).pipe(
      map((response: RESTNFCatalog): NFCatalog => {
        const catalog: NFCatalog = createNFCatalog(response);
        this._nfCatalog = catalog;

        return catalog;
      }),
      catchError(() => of({})));
  }

  public postTenantStats(
    id: string,
    filter?: Array<StatFilter>,
    duration?: number,
    step?: number,
    pipelines?: string[],
    start?: number
  ): Observable<TenantStats> {
    return this._restStatsServiceDashboard.postTenantStats(
      id,
      {
        services: pipelines,
        filter: {
          filter: filter,
        },
        range: {
          start: start,
          duration: duration,
          step: step
        }
      }
    );
  }

  public streamTenantStats(
    id: string,
    delay: number,
    filter?: StatFilter,
    duration?: number,
    step?: number,
    pipelines?: string[]
  ): Observable<TenantStats> {
    const msInSec = 1000;
    let lastFetchTime: number;

    return concat(
      defer(() => {
        if (!duration) {
          return of(duration);
        }

        const now: number = new Date().getTime();
        const newDuration = lastFetchTime ?
          Math.round((now - lastFetchTime) / msInSec) :
          duration;
        lastFetchTime = now;

        return of(newDuration);
      }).pipe(
        mergeMap((_duration: number) =>
          this.postTenantStats(
            id,
            (filter ? [filter] : []) as Array<StatFilter>,
            _duration,
            step,
            pipelines)
        )
      ),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      map((stats: TenantStats): TenantStats => {
        if (stats.packetLoss) {
          let packetLossTotal = 0;
          (stats.packetLoss as PacketLossStat[]).forEach((item: PacketLossStat) => {
            packetLossTotal += item.stats[0] ? +item.stats[0][1] : 0;
          });
          stats.packetLossTotal = packetLossTotal;
        }

        return stats;
      }),
      shareReplay({
        refCount: true
      })
    );
  }

  public removeTenant(id: string): Observable<DeleteHttpResponse> {
    return this._restTenantsServiceHeimdallr.deleteTenant(id);
  }

  public createTenant(name: string) {
    const body: Tenant = {
      identifier: '',
      name: name,
    };

    return this._restTenantsServiceHeimdallr.postTenant(body);
  }

  public editTenant({ identifier, name }: Tenant): Observable<string> {
    return this._restTenantsServiceHeimdallr.putTenant(identifier, { name });
  }

  public getTenantSites(tenant: Tenant): Observable<Site[]> {
    return this._restSitesService.getTenantSites(tenant.identifier).pipe(
      mergeMap((siteIDs: TenantSites = {
        sites: []
      }) => {
        const reqMap = {};

        siteIDs.sites.forEach((id: string) => {
          reqMap[id] = this._restSitesService.getSite(id);
        });

        return siteIDs.sites.length ? forkJoin(reqMap) : of({});
      }),
      map((sites: { [key: string]: Site }): Site[] => {
        return Object.values(sites);
      })
    );
  }
}
