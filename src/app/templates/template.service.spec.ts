import { inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import {
  extractTemplateVariables,
  getTemplateVariableText,
  isTemplateVariable,
  TemplateService
} from './template.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { ServiceTemplates } from 'rest_client/pangolin/model/serviceTemplates';
import { environment } from '../../environments/environment';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import templates from 'mock/templates';
import { SortDirection } from '@angular/material/sort';
import { CREATE_TEMPLATE_VARIABLE } from '../utils/codemirror';

describe('TemplateService', () => {
  let httpMock: HttpTestingController;
  let service: TemplateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    });
    service = TestBed.inject(TemplateService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get templates', () => {
    service.getTemplates().subscribe((res: ServiceTemplates) => {
      expect(res).toEqual(templates);
    });
    let req = httpMock.expectOne(`${environment.restServer}/v1/service_templates`);
    req.flush(templates);
    expect(req.request.method).toBe('GET');

    const index = 0;
    const pageSize = 50;
    service.getTemplates(index, pageSize).subscribe((res: ServiceTemplates) => {
      expect(res).toEqual(templates);
    });
    req = httpMock.expectOne(`${environment.restServer}/v1/service_templates?index=${index}&limit=${pageSize}`);
    req.flush(templates);
    expect(req.request.method).toBe('GET');

    const sortColumn = 'name';
    const sortDirection: SortDirection = 'desc';
    service.getTemplates(index, pageSize, sortColumn, sortDirection).subscribe((res: ServiceTemplates) => {
      expect(res).toEqual(templates);
    });
    req = httpMock.expectOne(`${environment.restServer}/v1/service_templates?sort=${sortDirection}(${sortColumn})&index=${index}&limit=${pageSize}`);
    req.flush(templates);
    expect(req.request.method).toBe('GET');
  });

  it('should delete template', () => {
    const templateId = '1';
    const response = 'OK';
    service.deleteTemplate(templateId).subscribe((res) => {
      expect(res).toEqual(response);
    });
    const req = httpMock.expectOne(`${environment.restServer}/v1/service_templates/${templateId}`);
    req.flush(response);
    expect(req.request.method).toBe('DELETE');
  });

  it('should get initial template variables', () => {
    expect(service.templateVariables).toEqual([]);
  });

  it('should add template variables', () => {
    service.addTemplateVariable('var2', 'var1', 'var1', 'var2');
    expect(service.templateVariables).toEqual(['var1', 'var2']);
  });

  it('should not add existing template variables', () => {
    const initVars = ['var1', 'var2'];
    service['_templateVariables'] = initVars;
    expect(service.templateVariables).toEqual(initVars);

    service.addTemplateVariable('var2', 'var1', 'var1', 'var3');
    expect(service.templateVariables).toEqual(['var1', 'var2', 'var3']);
  });

  it('should clear template variables', () => {
    const initVars = ['var1', 'var2'];
    service['_templateVariables'] = initVars;
    expect(service.templateVariables).toEqual(['var1', 'var2']);
    service.clearTemplateVariables();
    expect(service.templateVariables).toEqual([]);
  });

  it('should remove template variables', () => {
    const initVars = ['var1', 'var2'];
    service['_templateVariables'] = initVars;
    expect(service.templateVariables).toEqual(['var1', 'var2']);
    service.removeTemplateVariable('var1', 'var3');
    expect(service.templateVariables).toEqual(['var2']);
  });

  it('should check if string is template variable', () => {
    expect(isTemplateVariable(CREATE_TEMPLATE_VARIABLE('var1'))).toBeTruthy();
    expect(isTemplateVariable('{.var1}')).toBeFalsy();
    expect(isTemplateVariable('{var1}')).toBeFalsy();
    expect(isTemplateVariable('var1}')).toBeFalsy();
    expect(isTemplateVariable('')).toBeFalsy();
    expect(isTemplateVariable(undefined)).toBeFalsy();
  });

  it('should get name of template variable', () => {
    expect(getTemplateVariableText(CREATE_TEMPLATE_VARIABLE('var1'))).toEqual('var1');
  });

  it('should extract template variables from service template', () => {
    const vars: Map<string, Set<string>> = extractTemplateVariables(templates.serviceTemplates[0]);
    const expectedVars: Map<string, Set<string>> = new Map();
    expectedVars.set('SourceVNI', new Set(['config.nodes.portX.port.tunnel']));
    expectedVars.set('SourceMAC', new Set(['config.nodes.portX.port.tunnel']));
    expectedVars.set('SourceAddr', new Set(['config.nodes.portX.port.tunnel']));
    expectedVars.set('SinkVNI', new Set(['config.nodes.portY.port.tunnel']));
    expectedVars.set('SinkMAC', new Set(['config.nodes.portY.port.tunnel']));
    expectedVars.set('SinkAddr', new Set(['config.nodes.portY.port.tunnel']));
    expectedVars.set('ServiceName', new Set(['config.name']));
    expectedVars.set('TenantName', new Set(['config.tenantId']));
    expect(vars).toEqual(expectedVars);
  });
});
