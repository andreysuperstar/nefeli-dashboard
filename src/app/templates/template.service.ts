import { Injectable } from '@angular/core';
import { SortDirection } from '@angular/material/sort';
import { Observable, of } from 'rxjs';
import { ServiceTemplatesService } from 'rest_client/pangolin/api/serviceTemplates.service';
import { ServiceTemplates } from 'rest_client/pangolin/model/serviceTemplates';
import { ServiceTemplate } from 'rest_client/pangolin/model/serviceTemplate';
import { filter, toArray } from 'rxjs/operators';

import { TEMPLATE_VARIABLE_REGEXP, TEMPLATE_VARIABLE_MIN_LENGTH, TEMPLATE_VARIABLE_PREFIX, TEMPLATE_VARIABLE_SUFFIX } from '../utils/codemirror';

export const isTemplateVariable = (variable: string): boolean => {
  return variable?.length >= TEMPLATE_VARIABLE_MIN_LENGTH
    && variable.startsWith(TEMPLATE_VARIABLE_PREFIX)
    && variable.endsWith(TEMPLATE_VARIABLE_SUFFIX);
};

export const getTemplateVariableText = (variable: string): string => {
  if (!variable || variable.length < TEMPLATE_VARIABLE_MIN_LENGTH) {
    return;
  }

  return variable.substring(TEMPLATE_VARIABLE_PREFIX.length, variable?.length - TEMPLATE_VARIABLE_SUFFIX.length);
};

const propertiesToArray = (obj: Object): string[] => {
  const isObject = val => val && typeof val === 'object';
  const addDelimiter = (a, b) => a ? `${a}.${b}` : b;

  const paths = (o = {}, head = '') => {
    return Object.entries(o)
      .reduce((product, [key, value]) => {
        const fullPath = addDelimiter(head, key);
        if (isObject(value)) {
          return product.concat(paths(value, fullPath));
        } else {
          return product.concat(`${fullPath}|${value}`);
        }
      }, []);
  };

  return paths(obj);
};

export const extractTemplateVariables = (template: ServiceTemplate): Map<string, Set<string>> => {
  const props = propertiesToArray(template);
  const vars = new Map<string, Set<string>>();
  const splitLimit = 2;
  const varStartIndex = TEMPLATE_VARIABLE_PREFIX.length;
  const varEndOffset = TEMPLATE_VARIABLE_SUFFIX.length;

  props.forEach((prop: string) => {
    const [propString, value] = prop.split('|', splitLimit);
    if (propString && value) {
      const matches = value.match(TEMPLATE_VARIABLE_REGEXP);
      matches?.forEach((match: string) => {
        const varName = match.substring(varStartIndex, match.length - varEndOffset);

        if (vars.has(varName)) {
          vars.get(varName).add(propString);
        } else {
          const propSet = new Set<string>();
          propSet.add(propString);
          vars.set(varName, propSet);
        }
      });
    }
  });

  return vars;
};

enum SortableColumn {
  name = 'name',
  description = 'description'
}

type HttpDeleteResponse = any;

export enum TemplateErrorCodes {
  NotFound = 3201
}

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  private _templateVariables: string[] = [];

  constructor(
    private _restTemplatesService: ServiceTemplatesService,
  ) {
  }

  public getTemplates(index?: number, size?: number, sortColumn?: string, sortDirection?: SortDirection): Observable<ServiceTemplates> {
    const sortField: SortableColumn = SortableColumn[sortColumn];

    const sort = sortField
      ? [`${sortDirection}(${sortField})`]
      : undefined;

    return this._restTemplatesService.getServiceTemplates(undefined, sort, undefined, index, size);
  }

  public deleteTemplate(id: string): Observable<HttpDeleteResponse> {
    return this._restTemplatesService.deleteServiceTemplate(id);
  }

  public get templateVariables(): string[] {
    return this._templateVariables;
  }

  public clearTemplateVariables() {
    this._templateVariables = [];
  }

  public addTemplateVariable(...variable: string[]) {
    const vars: string[] = this._templateVariables;
    const newVars = Array.from(new Set(variable || [])).filter(v => vars.indexOf(v) === -1);
    this._templateVariables = [...vars, ...newVars].sort();
  }

  public removeTemplateVariable(...variable: string[]) {
    const removingVars: string[] = Array.from(new Set(variable || []));
    of(...this._templateVariables).pipe(
      filter(v => removingVars.indexOf(v) === -1),
      toArray(),
    ).subscribe(vars => {
      this._templateVariables = vars.sort();
    });
  }
}
