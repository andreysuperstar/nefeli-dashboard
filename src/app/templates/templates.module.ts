import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateListComponent } from './template-list/template-list.component';
import { TemplatesRoutingModule } from './templates-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { FlexModule } from '@angular/flex-layout';
import { PipelineModule } from '../pipeline/pipeline.module';

@NgModule({
  declarations: [TemplateListComponent],
  imports: [
    FlexLayoutModule,
    MatButtonModule,
    CommonModule,
    TemplatesRoutingModule,
    SharedModule,
    FlexModule,
    PipelineModule
  ]
})
export class TemplatesModule { }
