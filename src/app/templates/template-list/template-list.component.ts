import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscription } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { Column, ColumnType, TableActions, TableActionEvent, Row, CssClassNames } from '../../shared/table/table.component';
import { TemplatesDataSource } from './templates.data-source';
import { TenantService } from 'src/app/tenant/tenant.service';
import { UserService } from 'src/app/users/user.service';
import { TemplateService, TemplateErrorCodes } from '../template.service';
import { TemplateRow } from '../../shared/table/row-types';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ServiceTemplate } from '../../../../rest_client/pangolin/model/serviceTemplate';
import { Site } from 'rest_client/pangolin/model/site';
import { ClusterService } from 'src/app/home/cluster.service';
import { Sites } from 'rest_client/pangolin/model/sites';

const COLUMNS: Column[] = [
  {
    name: 'name',
    label: 'name',
    sortable: true,
    type: ColumnType.ICON
  },
  {
    name: 'description',
    sortable: true
  },
  {
    name: 'tenant',
    sortable: false
  },
  {
    name: 'actions',
    sortable: false,
    type: ColumnType.ACTIONS
  }
];

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

type SelectedTemplateRow = {
  row: TemplateRow,
  template?: ServiceTemplate
};

@Component({
  selector: 'nef-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.less']
})
export class TemplateListComponent implements OnInit {

  private _dataSource: TemplatesDataSource;
  private _columns: Column[] = COLUMNS;
  private _dialogSubscription: Subscription;
  private _selectedTemplateRow: SelectedTemplateRow;
  private _sites: Site[] = [];

  constructor(
    private _userService: UserService,
    private _tenantService: TenantService,
    private _templateService: TemplateService,
    private _dialog: MatDialog,
    private _alertService: AlertService,
    private _router: Router,
    private _clusterService: ClusterService
  ) { }

  public ngOnInit(): void {
    this.initSites();

    this._dataSource = new TemplatesDataSource(this._templateService, this._tenantService);
    this._dataSource.fetch();

    this.isSystemAdmin$
      .pipe(take(1))
      .subscribe((isAdmin: boolean) => {
        this._columns = this.filterColumns(COLUMNS, isAdmin, this.selectedTemplateRow !== undefined);
      });
  }

  private initSites() {
    this._clusterService.getSites().subscribe({
      next: (s: Sites) => this._sites = s.sites
    });
  }

  private filterColumns(columns: Column[], admin: boolean, details: boolean) {
    // 1. Hide the Actions column for non-admin users.
    // 2. When a row is selected, hide the description column
    return columns.filter((column: Column) => (admin || column.type !== ColumnType.ACTIONS) && (!details || column.name !== 'description'));
  }

  private editTemplate(id: string) {
    // for now, do not support tenant level templates
    const tenantID = -1;

    this._router.navigate(['tenants', tenantID, 'service-template', id]);
  }

  public onActionClick({ type, data }: TableActionEvent) {
    switch (type) {
      case TableActions.Edit:
        this.editTemplate(data.id);

        break;
      case TableActions.Remove:
        this.removeTemplate(data);

        break;
    }
  }

  public removeTemplate({ id, name }: TemplateRow) {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: `This will remove the template <strong>${name}</strong>.`,
      action: 'Remove Template'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef.afterClosed().pipe(
      filter(Boolean),
      switchMap((_: boolean) => this._templateService.deleteTemplate(id))
    ).subscribe(
      () => {
        this._dataSource.remove(id);
        this._alertService.info(AlertType.INFO_REMOVE_TEMPLATE_SUCCESS);
      },
      ({ error: { errorCode, message } }: HttpErrorResponse) => {
        switch (errorCode) {
          case TemplateErrorCodes.NotFound:
            this._alertService.error(AlertType.ERROR_REMOVE_TEMPLATE);
            break;
          default:
            this._alertService.error(AlertType.ERROR_REMOVE_TEMPLATE, message);
        }
      }
    );
  }

  public onAddTemplate() {
    // for now, do not support tenant level templates
    const tenantId = -1;
    this._router.navigate(['tenants', tenantId, 'service-template']);
  }

  public get columns(): Column[] {
    return this._columns;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get dataSource(): TemplatesDataSource {
    return this._dataSource;
  }

  public onRowClick(row: Row) {
    this.isSystemAdmin$
      .pipe(take(1))
      .subscribe((isAdmin: boolean) => {
        const templateRow: TemplateRow = (row === this._selectedTemplateRow?.row) ? undefined : row as TemplateRow;
        this._columns = this.filterColumns(COLUMNS, isAdmin, templateRow !== undefined);

        if (!templateRow) {
          this._selectedTemplateRow = undefined;
          return;
        }
        const template: ServiceTemplate = this._dataSource.templates.find((t: ServiceTemplate) => t.identifier === templateRow.id);

        this._selectedTemplateRow = {
          row: templateRow,
          template
        };
      });
  }

  public get selectedTemplateRow(): SelectedTemplateRow {
    return this._selectedTemplateRow;
  }

  public getRowClassNames = (row: Row): CssClassNames => {
    return {
      'selected-row': row === this._selectedTemplateRow?.row
    };
  }

  public get sites(): Site[] {
    return this._sites;
  }
}
