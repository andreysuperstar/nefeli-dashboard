import { AlertType } from 'src/app/shared/alert.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDividerModule } from '@angular/material/divider';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { TableComponent } from 'src/app/shared/table/table.component';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { DecimalPipe } from '@angular/common';

import { environment } from 'src/environments/environment';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { User } from 'rest_client/heimdallr/model/user';
import { ServiceTemplate } from 'rest_client/pangolin/model/serviceTemplate';
import { ServiceTemplates } from 'rest_client/pangolin/model/serviceTemplates';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { Tenant, TenantService } from 'src/app/tenant/tenant.service';
import { UserService } from 'src/app/users/user.service';
import { TemplateListComponent } from './template-list.component';
import { TemplateErrorCodes, TemplateService } from '../template.service';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatHeaderRowHarness, MatRowHarness, MatTableHarness } from '@angular/material/table/testing';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { MatDialogModule } from '@angular/material/dialog';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import sites from 'mock/sites';
import { ClusterService } from '../../home/cluster.service';
import { HttpError } from '../../error-codes';
import { Subscription } from 'rxjs';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCellHarness } from '@angular/material/table/testing/cell-harness';
import { By } from '@angular/platform-browser';
import { PipelineComponent } from '../../pipeline/pipeline.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { LegendComponent } from '../../shared/legend/legend.component';
import { ToggleComponent } from '../../shared/toggle/toggle.component';
import { InputDialogComponent } from '../../shared/input-dialog/input-dialog.component';
import { NfcDialogComponent } from '../../pipeline/nfc-dialog/nfc-dialog.component';

describe('TemplateListComponent', () => {
  let component: TemplateListComponent;
  let fixture: ComponentFixture<TemplateListComponent>;
  let httpTestingController: HttpTestingController;
  let router: Router;
  let userService: UserService;
  let templateService: TemplateService;
  let clusterService: ClusterService;
  let loader: HarnessLoader;
  let documentRootLoader: HarnessLoader;
  let overlayContainer: OverlayContainer;

  let navigateSpy: jasmine.Spy;

  const mockTemplates: ServiceTemplate[] = [
    {
      identifier: '1',
      description: 'ServiceTemplate1 description',
      name: 'ServiceTemplate 1',
      ownerTenantId: '-1'
    },
    {
      identifier: '2',
      description: 'ServiceTemplate2 description',
      name: 'ServiceTemplate 2',
      ownerTenantId: '-1'
    },
    {
      identifier: '3',
      description: 'ServiceTemplate3 description',
      name: 'ServiceTemplate 3',
      ownerTenantId: '-1'
    },
    {
      identifier: '4',
      description: 'ServiceTemplate4 description',
      name: 'ServiceTemplate 4',
      ownerTenantId: '-2'
    },
    {
      identifier: '5',
      description: 'ServiceTemplate5 description',
      name: 'ServiceTemplate 5',
      ownerTenantId: '-2'
    }
  ];

  const mockTenants: Tenant[] = [
    {
      identifier: '-1',
      name: 'Stumptown Coffee'
    },
    {
      identifier: '-2',
      name: 'World Market'
    },
  ];

  const mockTenantsResp: Tenants = {
    tenants: mockTenants,
    metadata: {
      index: 0,
      total: mockTenants.length
    }
  };

  const mockSystemAdmin: User = {
    username: 'Admin',
    firstName: 'System',
    lastName: 'Admin',
    email: 'admin@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        FlexLayoutModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule,
        MatDividerModule,
        MatTableModule,
        MatSortModule,
        MatPaginatorModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        OverlayModule,
        MatCardModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatTabsModule
      ],
      declarations: [
        TemplateListComponent,
        TableComponent,
        AvatarComponent,
        PipelineComponent,
        LegendComponent,
        ToggleComponent,
        InputDialogComponent,
        NfcDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        DecimalPipe,
        TenantService,
        TemplateService,
        ClusterService
      ]
    })
      .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
    router = TestBed.inject(Router);
    templateService = TestBed.inject(TemplateService);
    userService = TestBed.inject(UserService);
    overlayContainer = TestBed.inject(OverlayContainer);
    clusterService = TestBed.inject(ClusterService);

    navigateSpy = spyOn(router, 'navigate');
  });

  beforeEach(() => {
    userService.setUser(mockSystemAdmin);
    fixture = TestBed.createComponent(TemplateListComponent);
    documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const tenantsRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants`
    );

    expect(tenantsRequest.request.method).toBe('GET');
    expect(tenantsRequest.request.responseType).toBe('json');
    tenantsRequest.flush(mockTenantsResp);

    const templatesRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/service_templates?index=0&limit=50`
    );

    expect(templatesRequest.request.method).toBe('GET');
    templatesRequest.flush({
      serviceTemplates: mockTemplates,
      metadata: {
        count: mockTemplates.length,
        filter: [],
        search: [],
        sort: [],
        total: mockTemplates.length
      }
    } as ServiceTemplates);

    const sitesRequest: TestRequest = httpTestingController.expectOne(`${environment.restServer}/v1/sites`);
    expect(sitesRequest.request.method).toBe('GET');
    sitesRequest.flush(sites);
    fixture.detectChanges();
  });

  afterEach(async () => {
    const dialogs = await documentRootLoader.getAllHarnesses(MatDialogHarness);

    await Promise.all(
      dialogs.map(dialog => dialog.close())
    );

    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.sites).toBe(sites.sites);
    expect(component['_dataSource'].rows?.length).toBe(mockTemplates.length);
    expect(component['_dataSource'].tenants).toEqual(mockTenants);
  });

  it('should render valid templates table columns', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const headerRows = await table.getHeaderRows();
    const headerCells = await headerRows[0].getCellTextByIndex();
    expect(headerCells).toEqual(["name", "description", "tenant", "actions"]);
  });

  it('should render templates table rows', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows: MatRowHarness[] = await table.getRows();
    expect(rows.length).toBe(5);
    const row1 = await rows[0].getCellTextByColumnName();
    const row2 = await rows[1].getCellTextByColumnName();
    const row5 = await rows[4].getCellTextByColumnName();
    rows[0].getCells()
    expect(row1.name.endsWith(mockTemplates[0].name)).toBeTruthy();
    expect(row1.description).toBe(mockTemplates[0].description);
    expect(row1.tenant).toBe(mockTenants[0].name);
    expect(row2.name.endsWith(mockTemplates[1].name)).toBeTruthy();
    expect(row2.description).toBe(mockTemplates[1].description);
    expect(row2.tenant).toBe(mockTenants[0].name);
    expect(row5.name.endsWith(mockTemplates[4].name)).toBeTruthy();
    expect(row5.description).toBe(mockTemplates[4].description);
    expect(row5.tenant).toBe(mockTenants[1].name);
  });

  it('should remove templates', async () => {
    spyOn(component['_alertService'], 'info');
    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Remove'
    }));
    await removeButton.click();

    const confirmButton = await documentRootLoader.getHarness(MatButtonHarness.with({
      ancestor: 'nef-confirmation-dialog',
      text: 'Remove Template'
    }));
    await confirmButton.click();

    const deleteRequest = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/service_templates/${mockTemplates[0].identifier}`
    });
    deleteRequest.flush({});

    expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_REMOVE_TEMPLATE_SUCCESS);
    const table = await loader.getHarness(MatTableHarness);
    const rows = await table.getRows();
    expect(rows.length).toBe(4);
    const firstRow = await rows[0].getCellTextByColumnName();
    expect(firstRow.name.endsWith(mockTemplates[0].name)).toBeTruthy();
  });

  it('should clean previous subscription on remove template', async () => {
    const spy = jasmine.createSpyObj<Subscription>('Subscription', ['unsubscribe']);
    component['_dialogSubscription'] = spy;

    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Remove'
    }));
    await removeButton.click();

    const confirmButton = await documentRootLoader.getHarness(MatButtonHarness.with({
      ancestor: 'nef-confirmation-dialog',
      text: 'Remove Template'
    }));
    await confirmButton.click();
    expect(spy.unsubscribe).toHaveBeenCalled();
  });

  it('should show error on remove template', async () => {
    spyOn(component['_alertService'], 'error');

    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Remove'
    }));
    await removeButton.click();

    const confirmButton = await documentRootLoader.getHarness(MatButtonHarness.with({
      ancestor: 'nef-confirmation-dialog',
      text: 'Remove Template'
    }));
    await confirmButton.click();

    httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/service_templates/${mockTemplates[0].identifier}`
    }).flush({
      errorCode: TemplateErrorCodes.NotFound
    }, {
      status: HttpError.NotFound,
      statusText: 'Not Found'
    });

    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_TEMPLATE);
  });

  it('should show error message on remove template', async () => {
    spyOn(component['_alertService'], 'error');
    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Remove'
    }));
    await removeButton.click();

    const confirmButton = await documentRootLoader.getHarness(MatButtonHarness.with({
      ancestor: 'nef-confirmation-dialog',
      text: 'Remove Template'
    }));
    await confirmButton.click();

    const message = 'Bad Request';
    httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/service_templates/${mockTemplates[0].identifier}`
    }).flush({
      errorCode: HttpError.BadRequest,
      message
    }, {
      status: HttpError.BadRequest,
      statusText: ''
    });

    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_TEMPLATE, message);
  });

  it('should go to service designer on add new template', async () => {
    const addButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Add New Template'
    }));
    await addButton.click();

    expect(navigateSpy).toHaveBeenCalledWith(['tenants', -1, 'service-template']);
  });

  it('should display template details on row click', async () => {
    expect(fixture.debugElement.nativeElement.querySelectorAll('.selected-row').length).toBe(0);
    fixture.debugElement.nativeElement.querySelector('mat-row').click();
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('mat-row').className).toContain('selected-row');
    const table: MatTableHarness = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const headers: MatHeaderRowHarness[] = await table.getHeaderRows()
    expect(headers.length).toBe(1);
    const columns: MatCellHarness[] = await headers[0].getCells();
    expect(columns.length).toBe(3);
    expect(await columns[0].getColumnName()).toBe('name');
    expect(await columns[1].getColumnName()).toBe('tenant');
    expect(await columns[2].getColumnName()).toBe('actions');

    const pipelineComponent = fixture.debugElement.query(By.directive(PipelineComponent))?.componentInstance;
    expect(pipelineComponent).toBeTruthy();

    expect(component.selectedTemplateRow?.template).toEqual(mockTemplates[0]);

    expect(fixture.debugElement.nativeElement.querySelector('footer')?.textContent).toEqual(mockTemplates[0].description);
  });

  it('should return to full table view on row click', async () => {
    fixture.debugElement.nativeElement.querySelector('mat-row').click();
    fixture.detectChanges();
    let columns: MatCellHarness[] = await loader.getHarness<MatTableHarness>(MatTableHarness)
      .then(table => table.getHeaderRows())
      .then(headers => headers[0].getCells());
    expect(columns.length).toBe(3);
    expect(await columns[0].getColumnName()).toBe('name');
    expect(await columns[1].getColumnName()).toBe('tenant');
    expect(await columns[2].getColumnName()).toBe('actions');

    fixture.debugElement.nativeElement.querySelector('mat-row').click();
    fixture.detectChanges();
    columns = await loader.getHarness<MatTableHarness>(MatTableHarness)
      .then(table => table.getHeaderRows())
      .then(headers => headers[0].getCells());
    expect(columns.length).toBe(4);
    expect(await columns[0].getColumnName()).toBe('name');
    expect(await columns[1].getColumnName()).toBe('description');
    expect(await columns[2].getColumnName()).toBe('tenant');
    expect(await columns[3].getColumnName()).toBe('actions');

    const pipelineComponent = fixture.debugElement.query(By.directive(PipelineComponent))?.componentInstance;
    expect(pipelineComponent).toBeFalsy();
  });

  it('should navigate to edit template page', async () => {
    const editButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Edit'
    }));

    await editButton.click();

    expect(navigateSpy).toHaveBeenCalledWith(['tenants', -1, 'service-template', mockTemplates[0].identifier]);
  });
});
