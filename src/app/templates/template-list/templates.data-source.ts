import { map, catchError, take, mergeMap } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscription, of, ReplaySubject } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';

import { SortDirection } from '@angular/material/sort';
import { TableDataSource } from '../../shared/table/table.component';
import { TemplateRow } from '../../shared/table/row-types';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { ServiceTemplates } from 'rest_client/pangolin/model/serviceTemplates';
import { ServiceTemplate } from 'rest_client/pangolin/model/serviceTemplate';
import { TemplateService } from '../template.service';
import { TableActions } from '../../shared/table/table.component';
import { TenantService, Tenant } from 'src/app/tenant/tenant.service';

const DEFAULT_PAGE_SIZE = 50;

export class TemplatesDataSource implements TableDataSource<TemplateRow> {

  private _templateRows$ = new BehaviorSubject<TemplateRow[]>([]);
  private _loading$ = new BehaviorSubject<boolean>(false);
  private _templatesSubscription: Subscription;
  private _total: number;
  private _tenants: Tenant[];
  private _tenants$ = new ReplaySubject<boolean>();
  private _templates: ServiceTemplate[];
  private _rows: TemplateRow[];
  private _index = 0;
  private _pageSize = DEFAULT_PAGE_SIZE;

  constructor(
    private _templateService: TemplateService,
    private _tenantService: TenantService
  ) {
    this.fetchTenants();
  }

  public connect(collectionViewer: CollectionViewer): Observable<TemplateRow[]> {
    return this._templateRows$.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    if (this._templatesSubscription) {
      this._templatesSubscription.unsubscribe();
    }
    this._templateRows$.complete();
    this._loading$.complete();
  }

  public fetch(index?: number, pageSize?: number, sortColumn?: string, sortDirection?: SortDirection) {
    this._index = index ?? this._index;
    this._pageSize = pageSize ?? this._pageSize;
    this._loading$.next(true);

    if (this._templatesSubscription) {
      this._templatesSubscription.unsubscribe();
    }

    // sort column and direction should be defined together
    sortColumn = sortDirection ? sortColumn : undefined;

    this._templatesSubscription = this._tenants$.pipe(
      mergeMap(() => this._templateService.getTemplates(this._index, this._pageSize, sortColumn, sortDirection)),
      map(({ serviceTemplates, metadata }: ServiceTemplates): TemplateRow[] => {
        this._total = metadata?.total;
        this._templates = serviceTemplates;
        return serviceTemplates.map((template: ServiceTemplate): TemplateRow => this.getTemplateRow(template));
      }),
      catchError(() => {
        this._loading$.next(false);
        return of([]);
      })
    ).subscribe((templateRows: TemplateRow[]) => {
      this._rows = templateRows;
      this._templateRows$.next(templateRows);
      this._loading$.next(false);
    });
  }

  public remove(id: string) {
    const index: number = this._rows.findIndex(
      (template: TemplateRow): boolean => template.id === id
    );
    if (index !== -1) {
      this._templates.splice(index, 1);
      this._rows.splice(index, 1);
      this._templateRows$.next(this._rows);
      this._total -= 1;
    }
  }

  private fetchTenants() {
    this._tenantService.getTenants()
      .pipe(
        take(1)
      )
      .subscribe(({ tenants }: Tenants) => {
        this._tenants = tenants;
        this._tenants$.next(true);
      });
  }

  private getTemplateRow({ identifier, name, description, ownerTenantId }: ServiceTemplate): TemplateRow {

    const tenant: Tenant = this._tenants.find(
      ({ identifier: tenantId }: Tenant) => tenantId === ownerTenantId
    );

    return {
      id: identifier,
      name,
      description,
      tenant: tenant?.name,
      actions: [
        {
          action: TableActions.Edit,
          icon: 'edit',
          text: 'Edit'
        },
        {
          action: TableActions.Remove,
          icon: 'trash-blue',
          text: 'Remove'
        }
      ]
    };
  }

  public get rows(): TemplateRow[] {
    return this._rows;
  }

  public get total(): number {
    return this._total;
  }

  public get tenants(): Tenant[] {
    return this._tenants;
  }

  public get templates(): ServiceTemplate[] {
    return this._templates;
  }
}
