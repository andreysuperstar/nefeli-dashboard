import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemplateListComponent } from './template-list/template-list.component';

export const routes: Routes = [
  {
    path: '',
    component: TemplateListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplatesRoutingModule { }
