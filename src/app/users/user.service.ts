import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { map, catchError, mapTo } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { PostUserResp } from 'rest_client/heimdallr/model/postUserResp';
import { Roles } from 'rest_client/heimdallr/model/roles';
import { User } from 'rest_client/heimdallr/model/user';
import { Users } from 'rest_client/heimdallr/model/users';
import { UsersService } from 'rest_client/heimdallr/api/users.service';
import { SortDirection } from '@angular/material/sort';
import { HttpClient } from '@angular/common/http';

enum Scope {
  System = 'system',
  Tenant = 'tenant'
}

enum SortableColumn {
  user = 'username'
}

export interface Jwt {
  aud: string;
  exp: number;
  iat: number;
  iss: string;
  jti: string;
  ldap: boolean;
  nbf: number;
  roles: Roles;
  sub: string;
}

export interface CreateUserResponse {
  invite_token: string;
}

const ROOT_USER: User = {
  username: 'root',
  email: 'root@nefeli',
  roles: {
    scope: 'system',
    id: 'root',
    roles: []
  }
};

const NEFELI_USER: User = {
  username: 'nefeli',
  email: 'user@nefeli',
  roles: {
    scope: 'internal',
    id: 'nefeli',
    roles: []
  }
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private _user$ = new BehaviorSubject<User>(undefined);

  constructor(
    private _restUsersService: UsersService,
    private _httpClient: HttpClient
  ) { }

  public getUsers(index?: number, size?: number, sortColumn?: string, sortDirection?: SortDirection): Observable<Users> {
    const sortField: SortableColumn = SortableColumn[sortColumn];

    const sort = sortField
      ? [`${sortDirection}(${sortField})`]
      : undefined;

    return this._restUsersService.getUsers(index, undefined, sort, undefined, size);
  }

  public getUser(username: string, accessToken: string): Observable<User> {
    /* 'root' and 'nefeli' are special users to the system that
    cannot be retrieved from GET /auth/users/:id */
    if (username === ROOT_USER.username) {
      return of(ROOT_USER);
    } else if (username === NEFELI_USER.username) {
      return of(NEFELI_USER);
    }

    if (accessToken) {
      const decodedToken: Jwt = this.getDecodedToken(accessToken);

      if (decodedToken && decodedToken.ldap) {
        // TODO(ecarino): workaround until we can retrieve user data from ldap
        return of({
          username,
          // todo (anton): decodedToken has no stored email value
          email: '',
          roles: decodedToken.roles
        });
      }
    }
    return this._restUsersService.getUser(username);
  }

  private getDecodedToken(accessToken: string): Jwt {
    const helper = new JwtHelperService();
    const isExpired = helper.isTokenExpired(accessToken);
    return !isExpired ? helper.decodeToken(accessToken) : undefined;
  }

  public addUser(user: User): Observable<PostUserResp> {
    const body: User = {
      username: user.username,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      roles: user.roles
    };

    return this._restUsersService.postUser(body);
  }

  public editUser(username: string, user: Partial<User>): Observable<string> {
    const body: User = {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      roles: user.roles
    };

    return this._restUsersService.putUser(username, body);
  }

  public removeUser(username: string): Observable<string> {
    return this._restUsersService.deleteUser(username);
  }

  public setUser(response: User) {
    const user: User = response && response.roles
      ? response
      : undefined;

    this._user$.next(user);
  }

  public getGravatarUrl(emailHash: string): Observable<string> {
    const url = `https://www.gravatar.com/avatar/${emailHash}?d=404`;

    return this._httpClient.get(
      url,
      { responseType: 'text' }
    )
      .pipe(
        mapTo(url),
        catchError(err => of(undefined))
      );
  }

  public get user$(): Observable<User> {
    return this._user$.asObservable();
  }

  public get isAdmin$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { roles, scope } } = user;

        return roles.includes('admin');
      })
    );
  }

  public get isSystem$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { scope } } = user;

        return scope === 'system';
      })
    );
  }

  public get isSystemUser$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { roles, scope } } = user;

        return scope === 'system' && !roles.includes('admin') && roles.includes('user');
      })
    );
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { roles, scope } } = user;

        return scope === 'system' && roles.includes('admin');
      })
    );
  }

  public get isTenantUser$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { roles, scope } } = user;

        return scope === 'tenant' && !roles.includes('admin') && roles.includes('user');
      })
    );
  }

  public get isTenantAdmin$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { scope, roles } } = user;

        return scope === 'tenant' && roles.includes('admin');
      })
    );
  }

  public get isTenant$(): Observable<boolean> {
    return this.user$.pipe(
      map((user: User): boolean => {
        if (!user || !user.roles || !user.roles.roles) {
          return false;
        }

        const { roles: { scope } } = user;

        return scope === 'tenant';
      })
    );
  }
}
