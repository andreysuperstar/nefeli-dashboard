import { map, catchError, finalize, tap, take, mergeMap } from 'rxjs/operators';
import { BehaviorSubject, Subject, Observable, Subscription, forkJoin, of, ReplaySubject } from 'rxjs';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { User } from 'rest_client/heimdallr/model/user';
import { UserService } from 'src/app/users/user.service';
import { Users } from 'rest_client/heimdallr/model/users';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { TenantService, Tenant } from 'src/app/tenant/tenant.service';
import { SortDirection } from '@angular/material/sort';
import { TableDataSource } from '../shared/table/table.component';
import { UserRow } from '../shared/table/row-types';

export const ROLES = {
  cluster: {
    admin: 'Site Admin',
    user: 'Site User'
  },
  system: {
    admin: 'System Admin',
    user: 'System User'
  },
  tenant: {
    admin: 'Tenant Admin',
    user: 'Tenant User'
  }
};

const DEFAULT_PAGE_SIZE = 50;

export class UsersDataSource implements TableDataSource<UserRow> {

  private _userRows$ = new BehaviorSubject<UserRow[]>([]);
  private _loading$ = new BehaviorSubject<boolean>(false);
  private _usersSubscription: Subscription;
  private _tenants$ = new ReplaySubject<boolean>();
  private _total: number;
  private _users: User[];
  private _tenants: Tenant[];
  private _rows: UserRow[];
  private _index = 0;
  private _pageSize = DEFAULT_PAGE_SIZE;

  constructor(
    private _userService: UserService,
    private _tenantService: TenantService
  ) {
    this.fetchTenants();
  }

  public connect(collectionViewer: CollectionViewer): Observable<UserRow[]> {
    return this._userRows$.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    if (this._usersSubscription) {
      this._usersSubscription.unsubscribe();
    }
    this._userRows$.complete();
    this._loading$.complete();
  }

  public fetch(index?: number, pageSize?: number, sortColumn?: string, sortDirection?: SortDirection) {
    this._index = index ?? this._index;
    this._pageSize = pageSize ?? this._pageSize;
    this._loading$.next(true);

    if (this._usersSubscription) {
      this._usersSubscription.unsubscribe();
    }

    // sort column and direction should be defined together
    sortColumn = sortDirection ? sortColumn : undefined;

    this._usersSubscription = this._tenants$.pipe(
      mergeMap(() => this._userService.getUsers(this._index, this._pageSize, sortColumn, sortDirection)),
      map(({ users, metadata }: Users): UserRow[] => {
        this._total = metadata?.total;
        this._users = users;
        return users.map((u: User): UserRow => this.getUserRow(u));
      }),
      catchError(() => of([])),
      finalize(() => this._loading$.next(false)),
    ).subscribe((userRows: UserRow[]) => {
      this._rows = userRows;
      this._userRows$.next(userRows);
    });
  }

  private fetchTenants() {
    this._tenantService.getTenants()
      .pipe(
        take(1)
      )
      .subscribe(({ tenants = [] }) => {
        this._tenants = tenants;
        this._tenants$.next(true);
      });
  }

  private getUserRow({ username, firstName, lastName, email, roles }: User): UserRow {
    const data: { name: string; email: string; username: string } = {
      name: `${firstName ? firstName : ''} ${lastName ? lastName : ''}`,
      email,
      username,
    };

    const role: string[] = (!roles.roles) ? [] : roles.roles.map(
      (roleName: string) => ROLES[roles.scope][roleName]
    );

    const tenant = this._tenants?.find(({ identifier: tenantId }) => tenantId === roles.id);

    return {
      user: data,
      role,
      tenant: [
        [tenant?.name]
      ]
    } as UserRow;
  }

  public remove(usernames: string[]) {
    usernames.forEach((username) => {
      const index: number = this._users.findIndex(
        (user: User): boolean => user.username === username
      );

      this._users.splice(index, 1);
      this._rows.splice(index, 1);
    });
    this._userRows$.next(this._rows);
    this._total -= usernames.length;
  }

  public update(updatedUser: User) {
    const index: number = this._users.findIndex(
      (user: User): boolean => user.username === updatedUser.username
    );
    this._users[index] = updatedUser;
    this._rows[index] = this.getUserRow(updatedUser);
    this._userRows$.next(this._rows);
  }

  public set total(count: number) {
    this._total = count;
  }

  public get loading$(): Observable<boolean> {
    return this._loading$.asObservable();
  }

  public get length(): number {
    return this._userRows$.value.length;
  }

  public get total(): number {
    return this._total;
  }

  public get users(): User[] {
    return this._users;
  }

  public get rows(): UserRow[] {
    return this._rows;
  }

  public get tenants(): Tenant[] {
    return this._tenants;
  }
}
