import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Subscription, forkJoin } from 'rxjs';

import { UserDialogData } from '../users.component';
import { Tenant } from '../../tenant/tenant.service';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from '../user.service';

@Component({
  selector: 'nef-edit-user-roles-dialog',
  templateUrl: './edit-user-roles-dialog.component.html',
  styleUrls: ['./edit-user-roles-dialog.component.less']
})
export class EditUserRolesDialogComponent implements OnInit, OnDestroy {

  private _rolesForm: FormGroup;
  private _usersSubscription: Subscription;

  constructor(
    private _dialogRef: MatDialogRef<EditUserRolesDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: UserDialogData,
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _userService: UserService,
  ) { }

  public ngOnInit() {
    this.initRolesForm();
  }

  public ngOnDestroy() {
    if (this._usersSubscription) {
      this._usersSubscription.unsubscribe();
    }
  }

  private initRolesForm() {
    this._rolesForm = this._fb.group(
      {
        tenant: '',
        roles: this._fb.control([])
      }
    );
  }

  public submitRolesForm() {
    const { invalid, pristine } = this._rolesForm;

    if (invalid || pristine) {
      return;
    }

    this.editUserRoles();
  }

  private editUserRoles() {
    const { roles } = this._rolesForm.value;

    if (this._usersSubscription) {
      this._usersSubscription.unsubscribe();
    }

    this._usersSubscription = forkJoin(
      this._data.users.map((username: string) =>
        this._userService.editUser(username, { roles })
      )
    )
      .subscribe(
        (responses: string[]) => {
          this._alertService.info(AlertType.INFO_EDIT_USER_ROLES_SUCCESS);

          this._dialogRef.close(responses);
        },
        (_: HttpErrorResponse) =>
          this._alertService.error(AlertType.ERROR_EDIT_USER_ROLES_FAILED)
      );
  }

  public get rolesForm(): FormGroup {
    return this._rolesForm;
  }

  public get tenants(): Tenant[] {
    return this._data.tenants;
  }

}
