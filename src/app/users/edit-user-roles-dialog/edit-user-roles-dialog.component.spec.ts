import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatSelectHarness } from '@angular/material/select/testing';
import { environment } from 'src/environments/environment';

import { UserDialogData } from '../users.component';
import { Scope } from 'src/app/auth/authentication.service';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { AlertService } from 'src/app/shared/alert.service';
import { UserService } from '../user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { Roles } from 'rest_client/heimdallr/model/roles';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';

import { EditUserRolesDialogComponent } from './edit-user-roles-dialog.component';

describe('EditUserRolesDialogComponent', () => {
  let component: EditUserRolesDialogComponent;
  let fixture: ComponentFixture<EditUserRolesDialogComponent>;
  let httpTestingController: HttpTestingController;
  let userService: UserService;
  let el: HTMLElement;
  let loader: HarnessLoader;

  const dialogSpy = {
    close: jasmine.createSpy('close')
  };

  const mockUsers: User[] = [
    {
      username: 'Eric',
      email: 'eric@nefeli.io',
      roles: {
        scope: 'system',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'Andrew',
      email: 'andrew@nefeli.io',
      roles: {
        scope: 'system',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'Anton',
      email: 'anton@nefeli.io',
      roles: {
        scope: 'tenant',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    }
  ];

  const mockMatDialogData: UserDialogData = {
    users: ['Eric', 'Andrew', 'Anton'],
    tenants: [
      {
        identifier: "Nefeli Networks",
        name: "Nefeli Networks",
      },
      {
        identifier: "foobar",
        name: "foobar"
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule
      ],
      declarations: [EditUserRolesDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockMatDialogData
        },
        AlertService,
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserRolesDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();

    el = fixture.debugElement.nativeElement;

    dialogSpy.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(el.querySelector('[mat-dialog-title]').textContent).toBe('Edit Roles');
  });

  it('should render add roles form', () => {
    expect(el.querySelector('#roles-form')).toBeDefined();
  });

  it('should render form controls', () => {
    expect(
      el.querySelector('mat-select[formcontrolname=tenant]')
    ).toBeDefined('valid tenant select placeholder');

    expect(
      el.querySelector('mat-select[formcontrolname=roles]')
    ).toBeDefined('valid roles select placeholder');
  });

  it('should submit form', async () => {
    spyOn(component, 'submitRolesForm').and.callThrough();
    spyOn<any>(component, 'editUserRoles').and.callThrough();

    const submitButtonEl: HTMLButtonElement = el.querySelector('button[type=submit]');

    const roleSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=roles]'
    }));
    await roleSelect.clickOptions({
      text: 'Admin'
    });
    component['_rolesForm'].markAsDirty();
    submitButtonEl.click();

    expect(component['submitRolesForm']).toHaveBeenCalled();
    expect(component['editUserRoles']).toHaveBeenCalled();
  });

  // TODO(eric): re-enable when edit roles is re-enabled
  xit('should make a PUT requests on form submit', () => {
    const rolesForm = component['_rolesForm'];

    const mockFormData: { tenant: string, roles: string[] } = {
      tenant: 'Nefeli Networks',
      roles: ['admin', 'user']
    };

    rolesForm.setValue(mockFormData);

    const { tenant, roles } = mockFormData;

    const role: Roles = {
      scope: Scope.Tenant,
      id: tenant,
      roles
    };

    const usersData: Partial<User> = {
      roles: role
    };

    spyOn(userService, 'editUser').and.callThrough();

    component['editUserRoles']();

    expect(userService.editUser).toHaveBeenCalledWith(mockMatDialogData.users[0], usersData);
    expect(userService.editUser).toHaveBeenCalledWith(mockMatDialogData.users[1], usersData);
    expect(userService.editUser).toHaveBeenCalledWith(mockMatDialogData.users[2], usersData);
    expect(userService.editUser).toHaveBeenCalledTimes(3);

    const ericUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[0]}`
    );

    const andrewUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[1]}`
    );

    const antonUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[2]}`
    );

    expect(ericUserReq.request.method).toBe('PUT');

    expect(andrewUserReq.request.body.roles.id)
      .toBe(usersData.roles.id, `valid request body \'${usersData.roles.id}\' id`);
    expect(andrewUserReq.request.body.roles.roles)
      .toBe(usersData.roles.roles, 'valid request body roles');

    expect(antonUserReq.request.body.roles.roles)
      .toContain(usersData.roles.roles[0], `contains \'${usersData.roles.roles[0]}\' role`);
    expect(antonUserReq.request.body.roles.roles)
      .toContain(usersData.roles.roles[1], `contains \'${usersData.roles.roles[1]}\' role`);

    ericUserReq.flush(mockUsers[0]);
    andrewUserReq.flush(mockUsers[1]);
    antonUserReq.flush(mockUsers[2]);
  });

  it('should close the dialog only on successfull PUT responses', () => {
    component['editUserRoles']();

    const ericUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[0]}`
    );

    const andrewUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[1]}`
    );

    const antonUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[2]}`
    );

    ericUserReq.flush(mockUsers[0]);
    andrewUserReq.flush(mockUsers[1]);
    antonUserReq.flush(mockUsers[2]);

    expect(dialogSpy.close).toHaveBeenCalledWith(mockUsers);
  });

  it('should not close the dialog when getting error http response', () => {
    component['editUserRoles']();

    const ericUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[0]}`
    );

    const andrewUserReq = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.users[1]}`
    );

    const mockErrorResponseOptions: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };

    ericUserReq.flush(mockUsers[0]);
    andrewUserReq.flush({}, mockErrorResponseOptions);

    expect(dialogSpy.close).not.toHaveBeenCalled();
  });

});
