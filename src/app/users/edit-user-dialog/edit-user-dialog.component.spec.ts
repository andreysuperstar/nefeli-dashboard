import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputHarness } from '@angular/material/input/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';

import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { UserError } from 'rest_client/heimdallr/model/userError';

import { UserDialogData } from '../users.component';

import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { AlertService } from 'src/app/shared/alert.service';
import { UserService } from '../user.service';
import { User } from 'rest_client/heimdallr/model/user';

import { EditUserDialogComponent } from './edit-user-dialog.component';
import { HttpError } from 'src/app/error-codes';

describe('EditUserDialogComponent', () => {
  let component: EditUserDialogComponent;
  let fixture: ComponentFixture<EditUserDialogComponent>;
  let httpTestingController: HttpTestingController;
  let userService: UserService;
  let el: HTMLElement;
  let loader: HarnessLoader;

  const dialogSpy = {
    close: jasmine.createSpy('close')
  };

  const mockUser: User = {
    username: 'A-STAR',
    firstName: 'Andrew',
    lastName: 'Andrew',
    email: 'andrew@nefeli.io',
    roles: {
      scope: 'tenant',
      id: '-1',
      roles: ['admin']
    }
  };

  const mockMatDialogData: UserDialogData = {
    user: mockUser,
    tenants: [
      {
        identifier: "Nefeli Networks",
        name: "Nefeli Networks",
      },
      {
        identifier: "-1",
        name: "foobar"
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule
      ],
      declarations: [EditUserDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockMatDialogData
        },
        AlertService,
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUserDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();

    el = fixture.debugElement.nativeElement;

    dialogSpy.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(el.querySelector('[mat-dialog-title]').textContent).toBe('Edit Info');
  });

  it('should render add user form', () => {
    expect(el.querySelector('#user-form')).toBeDefined();
  });

  it('should fill form with user data', () => {
    expect(
      (el.querySelector('input[formcontrolname=username]') as HTMLInputElement).value
    ).toBe(mockMatDialogData.user.username, 'valid username input value');

    expect(
      (el.querySelector('input[formcontrolname=firstName]') as HTMLInputElement).value
    ).toBe(mockMatDialogData.user.firstName, 'valid first name input value');

    expect(
      (el.querySelector('input[formcontrolname=lastName]') as HTMLInputElement).value
    ).toBe(mockMatDialogData.user.lastName, 'valid last name input value');

    expect(
      (el.querySelector('input[formcontrolname=email]') as HTMLInputElement).value
    ).toBe(mockMatDialogData.user.email, 'valid email input value');

    expect(
      (el.querySelector('mat-select[placeholder="Tenant"]') as HTMLSelectElement).getAttribute('value')
    ).toBe('default', 'valid default tenant');

    expect(component.usersTenant.name).toBe('foobar');
  });

  it('should submit form if form is pristine', async () => {
    spyOn(component, 'submitUserForm').and.callThrough();
    spyOn<any>(component, 'editUser').and.callThrough();

    const submitButtonEl: HTMLButtonElement = el.querySelector('button[type=submit]');

    submitButtonEl.click();

    expect(component['submitUserForm']).not.toHaveBeenCalled();
    expect(component['editUser']).not.toHaveBeenCalled();

    const firstNameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter user first name'
    }));
    await firstNameInput.setValue('new value');
    component['_userForm'].markAsDirty();

    submitButtonEl.click();

    expect(component['submitUserForm']).toHaveBeenCalled();
    expect(component['editUser']).toHaveBeenCalled();
  });

  it('should make a PUT request on form submit', () => {
    const {
      username,
      firstName,
      lastName,
      email,
      roles: {
        id: tenant,
        roles,
        scope
      }
    } = mockMatDialogData.user;

    const userData: Partial<User> = {
      firstName,
      lastName,
      email,
      roles: {
        id: tenant,
        scope,
        roles
      }
    };

    spyOn(userService, 'editUser').and.callThrough();

    component['editUser']();

    expect(userService.editUser).toHaveBeenCalledWith(username, userData);

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.user.username}`
    );

    expect(req.request.method).toBe('PUT');

    req.flush(userData);
  });

  it('should close the dialog only on successful POST response', () => {
    component['editUser']();

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.user.username}`
    );

    req.flush(mockUser);

    expect(dialogSpy.close).toHaveBeenCalledWith({
      ...mockUser,
      roles: mockUser.roles
    });
  });

  it('should not close the dialog when getting error http response', () => {
    component['editUser']();

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockMatDialogData.user.username}`
    );

    const mockError: UserError = {
      httpStatus: HttpError.Conflict,
      message: 'invalid email: andrew@nefeli',
      errorCode: 2606
    };

    const mockErrorResponse: Partial<HttpErrorResponse> = {
      status: HttpError.Conflict,
      statusText: 'Bad Request'
    };

    req.flush(mockError, mockErrorResponse);

    expect(dialogSpy.close).not.toHaveBeenCalled();
  });
});
