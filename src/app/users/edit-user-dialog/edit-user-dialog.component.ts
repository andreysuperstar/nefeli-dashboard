import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Subscription } from 'rxjs';

import { UserDialogData } from '../users.component';
import { Tenant } from '../../tenant/tenant.service';
import { User } from 'rest_client/heimdallr/model/user';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from '../user.service';
import { Scope } from 'src/app/auth/authentication.service';

@Component({
  selector: 'nef-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.less']
})
export class EditUserDialogComponent implements OnInit, OnDestroy {

  private _userForm: FormGroup;
  private _userSubscription: Subscription;
  private _usersTenant: Tenant;

  constructor(
    private _dialogRef: MatDialogRef<EditUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: UserDialogData,
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _userService: UserService,
  ) { }

  public ngOnInit() {
    this.initUserForm();

    this._usersTenant = this._data.tenants.find((tenant: Tenant) => {
      return tenant.identifier === this._data.user.roles.id;
    });
  }

  public ngOnDestroy() {
    if (this._userSubscription) {
      this._userSubscription.unsubscribe();
    }
  }

  private initUserForm() {
    const {
      username,
      firstName,
      lastName,
      email,
      roles: {
        roles
      }
    } = this._data.user;

    this._userForm = this._fb.group(
      {
        username: [username, Validators.required],
        firstName,
        lastName,
        email: [
          email,
          [
            Validators.required,
            Validators.email
          ]
        ],
        roles: this._fb.control(roles)
      }
    );
  }

  public submitUserForm() {
    const { invalid, pristine } = this._userForm;

    if (invalid || pristine) {
      return;
    }

    this.editUser();
  }

  private editUser() {
    const {
      email,
      roles,
      firstName,
      lastName
    } = this._userForm.value;

    if (this._userSubscription) {
      this._userSubscription.unsubscribe();
    }

    const user: Partial<User> = {
      firstName,
      lastName,
      email,
      roles: {
        id: this._data.user.roles.id,
        scope: this._data.user.roles.scope,
        roles
      }
    };

    this._userSubscription = this._userService
      .editUser(this._data.user.username, user)
      .subscribe(
        () => {
          const updateUser: User = {
            ...user,
            username: this._data.user.username,
            email,
            // N.B.(mtovino):
            // updates the 'roles.roles' field,
            // and preserves the values of 'roles.scope' and
            // 'roles.id'
            roles: {
              ...this._data.user.roles,
              roles
            }
          };

          this._alertService.info(AlertType.INFO_EDIT_USER_SUCCESS);
          this._dialogRef.close(updateUser);
        },
        ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_EDIT_USER_FAILED, message);
        }
      );
  }

  public get userForm(): FormGroup {
    return this._userForm;
  }

  public get tenants(): Tenant[] {
    return this._data.tenants;
  }

  public get usersTenant(): Tenant {
    return this._usersTenant;
  }

  public get userScope(): string {
    return this._data.user.roles.scope;
  }

  public get Scope(): typeof Scope {
    return Scope;
  }
}
