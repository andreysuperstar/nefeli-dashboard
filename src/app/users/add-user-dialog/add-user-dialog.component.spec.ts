import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from 'src/environments/environment';

import { Site } from 'rest_client/pangolin/model/site';
import { Tenant } from 'src/app/tenant/tenant.service';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { AlertService } from '../../shared/alert.service';
import { UserService } from '../user.service';

import { AddUserDialogComponent } from './add-user-dialog.component';

import { Scope } from 'src/app/auth/authentication.service';
import { User } from 'rest_client/heimdallr/model/user';
import { BASE_PATH } from 'rest_client/heimdallr/variables';

describe('AddUserDialogComponent', () => {
  let component: AddUserDialogComponent;
  let fixture: ComponentFixture<AddUserDialogComponent>;
  let httpTestingController: HttpTestingController;
  let userService: UserService;
  let document: Document;
  let el: HTMLElement;

  const dialogSpy = jasmine.createSpyObj<MatDialogRef<AddUserDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  const mockSites: Site[] = [
    {
      config: {
        identifier: 'eric_site',
        name: 'Eric\'s Site'
      }
    },
    {
      config: {
        identifier: 'andrew_site',
        name: 'Andrew\'s Site'
      }
    },
    {
      config: {
        identifier: 'anton_site',
        name: 'Anton\'s Site'
      }
    },
    {
      config: {
        identifier: 'inactive_site',
        name: 'Inactive Site'
      }
    }
  ];

  const mockTenants: Tenant[] = [
    {
      identifier: '1',
      name: 'Tenant 1'
    },
    {
      identifier: '2',
      name: 'Tenant 2'
    },
    {
      identifier: '3',
      name: 'Tenant 3'
    },
    {
      identifier: '4',
      name: 'Nefeli Networks'
    }
  ];

  const mockUserFormData = {
    username: 'TestUser',
    firstName: 'A',
    lastName: 'B',
    email: 'a@b.c',
    scope: Scope.Tenant,
    id: 'Nefeli',
    roles: ['admin', 'user']
  };

  const mockUser: User = {
    username: mockUserFormData.username,
    email: mockUserFormData.email,
    roles: {
      scope: mockUserFormData.scope,
      id: mockUserFormData.id,
      roles: mockUserFormData.roles
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule
      ],
      declarations: [AddUserDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/auth'
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            sites: mockSites,
            tenants: mockTenants
          }
        },
        AlertService,
        UserService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    el = fixture.debugElement.nativeElement;

    dialogSpy.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(el.querySelector('[mat-dialog-title]').textContent).toBe('Add New User');
  });

  it('should render add user form', () => {
    expect(el.querySelector('#user-form')).toBeDefined();
  });

  it('should close dialog with form data only when form is valid', () => {
    spyOn<any>(component, 'createUser').and.callThrough();

    const form: FormGroup = component['_userForm'];
    const submitButton: HTMLButtonElement = el.querySelector('button[type=submit]');

    form.patchValue({
      username: 'test'
    });

    submitButton.click();

    expect(component['createUser']).not.toHaveBeenCalled();

    form.patchValue(mockUserFormData);
    form.markAsDirty();
    fixture.detectChanges();

    submitButton.click();

    expect(component['createUser']).toHaveBeenCalledWith();
  });

  it('should make a POST request on form submit', () => {
    component['_userForm'].patchValue(mockUserFormData);
    component['createUser']();

    const res = httpTestingController.expectOne(`${environment.restServer}/auth/users`);

    expect(res.request.method).toBe('POST');

    expect(res.request.body.username).toBe(mockUser.username);
    expect(res.request.body.email).toBe(mockUser.email);
    expect(res.request.body.roles.scope).toBe(mockUser.roles.scope);
    expect(res.request.body.roles.id).toBe(mockUser.roles.id);
    expect(res.request.body.roles.roles[0]).toBe(mockUser.roles.roles[0]);
    expect(res.request.body.roles.roles[1]).toBe(mockUser.roles.roles[1]);

    res.flush(mockUser);
  });

  it('should close the dialog only on successful POST response', () => {
    dialogSpy.close.calls.reset();

    component['_userForm'].patchValue(mockUserFormData);
    component['createUser']();
    const res = httpTestingController.expectOne(`${environment.restServer}/auth/users`);

    res.flush(mockUser);

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should not close the dialog when getting error http response', () => {
    component['_userForm'].patchValue(mockUserFormData);
    component['createUser']();
    const res = httpTestingController.expectOne(`${environment.restServer}/auth/users`);

    const mockErrorResponseOptions: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };

    res.flush({}, mockErrorResponseOptions);

    expect(dialogSpy.close).not.toHaveBeenCalled();
  });

  it('should select user scope, id and roles for system user', () => {
    const scopeRadioGroupDe: DebugElement = fixture.debugElement.query(
      By.css('mat-radio-group[formcontrolname="scope"]')
    );

    const scopeRadioButtonLabelDes: DebugElement[] = scopeRadioGroupDe.queryAll(
      By.css('mat-radio-button label')
    );

    let rolesSelectDe: DebugElement = fixture.debugElement.query(
      By.css('mat-select[formcontrolname="roles"]')
    );

    expect(scopeRadioGroupDe).not.toBeNull('scope radio group is rendered');
    expect(rolesSelectDe).toBeNull('Roles select is rendered');

    // select System scope
    scopeRadioButtonLabelDes[0].nativeElement.click();
    fixture.detectChanges();

    rolesSelectDe = fixture.debugElement.query(
      By.css('mat-select[formcontrolname="roles"]')
    );

    expect(rolesSelectDe).not.toBeNull('Roles select is rendered');

    // select roles
    rolesSelectDe.nativeElement.click();
    fixture.detectChanges();

    let optionEls: NodeListOf<HTMLUnknownElement> = document
      .querySelectorAll('.mat-select-panel')[0]
      .querySelectorAll('mat-option');

    expect(optionEls.length).toBe(2, 'render two role options');
    expect(optionEls[1].classList.contains('mat-option-disabled')).toBeTruthy;

    optionEls[0].click();
    fixture.detectChanges();

    expect(component.userForm.get('roles').value[0]).toBe(
      'admin',
      'valid admin role is selected'
    );
  });

  it('should select user scope, id for system admin', () => {
    const mockSiteAdmin: User = {
      username: 'Andrew',
      email: 'andrew@nefeli.io',
      roles: {
        scope: 'system',
        id: 'andrew_site',
        roles: ['admin', 'user']
      }
    };

    userService.setUser(mockSiteAdmin);
    component['setScope']();
    fixture.detectChanges();

    const scopeRadioGroupDe: DebugElement = fixture.debugElement.query(
      By.css('mat-radio-group[formcontrolname="scope"]')
    );

    const scopeRadioButtonLabelDes: DebugElement[] = scopeRadioGroupDe.queryAll(
      By.css('mat-radio-button label')
    );

    let rolesSelectDe: DebugElement = fixture.debugElement.query(
      By.css('mat-select[formcontrolname="roles"]')
    );

    expect(scopeRadioGroupDe).not.toBeNull('scope radio group is rendered');
    expect(rolesSelectDe).not.toBeNull('Roles select rendered');
    expect(component.userForm.get('scope').value).toBe('system');

    // select Site scope
    scopeRadioButtonLabelDes[0].nativeElement.click();
    fixture.detectChanges();

    rolesSelectDe = fixture.debugElement.query(
      By.css('mat-select[formcontrolname="roles"]')
    );

    expect(rolesSelectDe).not.toBeNull('Roles select is rendered');
  });

  it('should select tenant id for tenant admin', () => {
    const mockTenantAdmin: User = {
      username: 'Andrew',
      email: 'andrew@nefeli.io',
      roles: {
        scope: 'tenant',
        id: 'andrew_site',
        roles: ['admin', 'user']
      }
    };

    userService.setUser(mockTenantAdmin);
    component['setScope']();
    fixture.detectChanges();

    const scopeRadioGroupDe: DebugElement = fixture.debugElement.query(
      By.css('mat-radio-group[formcontrolname="scope"]')
    );

    const idSelectDe: DebugElement = fixture.debugElement.query(
      By.css('mat-select[formcontrolname="id"]')
    );

    const rolesSelectDe: DebugElement = fixture.debugElement.query(
      By.css('mat-select[formcontrolname="roles"]')
    );

    expect(scopeRadioGroupDe).toBeNull('scope radio group is hidden');
    expect(idSelectDe).toBeNull('ID select is not rendered for Tenant Admin');
    expect(rolesSelectDe).not.toBeNull('Roles select is rendered');

    fixture.detectChanges();

    expect(component.userForm.get('id').value).toBe(
      'andrew_site',
      'Tenant Admin role id is automatically used as tenant id'
    );
  });

});
