import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Observable, Subscription, combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';

import { UserDialogData } from '../users.component';
import { Scope } from 'src/app/auth/authentication.service';
import { Tenant } from 'src/app/tenant/tenant.service';

import { AlertService, AlertType } from '../../shared/alert.service';
import { UserService, CreateUserResponse } from '../user.service';
import { Roles } from 'rest_client/heimdallr/model/roles';
import { User } from 'rest_client/heimdallr/model/user';

const SCOPE_TITLES: Map<Scope, string> = new Map([
  [Scope.Tenant, 'Tenant'],
  [Scope.System, 'System']
]);

@Component({
  selector: 'nef-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.less']
})
export class AddUserDialogComponent implements OnInit, OnDestroy {

  private _userForm: FormGroup;
  private _userSubscription: Subscription;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<AddUserDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: UserDialogData,
    private _alertService: AlertService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this.initUserForm();
  }

  public ngOnDestroy() {
    if (this._userSubscription) {
      this._userSubscription.unsubscribe();
    }
  }

  private initUserForm() {
    this._userForm = this._fb.group(
      {
        username: ['', Validators.required],
        firstName: '',
        lastName: '',
        email: [
          '',
          [
            Validators.required,
            Validators.email
          ]
        ],
        scope: undefined,
        id: undefined,
        roles: this._fb.control(['user'], Validators.required)
      }
    );

    this.setScope();
  }

  private setScope() {
    combineLatest([
      this._userService.user$,
      this._userService.isSystem$,
      this._userService.isTenant$
    ])
      .pipe(take(1))
      .subscribe(([user, isSystem, isTenant]: [User, boolean, boolean]) => {
        if (isSystem) {
          this._userForm.get('scope').setValue(Scope.System);
        } else if (isTenant) {
          this._userForm
            .patchValue({
              scope: Scope.Tenant,
              id: user.roles.id
            });
        }
      });
  }

  public onScopeChange() {
    const idControl: FormControl = this._userForm.get('id') as FormControl;

    if (idControl) {
      // re-enable control
      if (idControl.disabled) {
        idControl.enable();
      }

      // reset ID
      idControl.setValue(undefined);
    }
  }

  public submitUserForm() {
    const { invalid, pristine } = this._userForm;

    if (invalid || pristine) {
      return;
    }

    this.createUser();
  }

  private createUser() {
    const { username, firstName, lastName, email, scope, id, roles }: {
      username: string,
      firstName: string,
      lastName: string,
      email: string,
      scope: Scope,
      id: string,
      roles: string[]
    } = this._userForm.value;

    const role: Roles = { scope, id, roles };

    const userData: User = {
      username,
      firstName,
      lastName,
      email,
      roles: role
    };

    if (this._userSubscription) {
      this._userSubscription.unsubscribe();
    }

    this._userSubscription = this._userService
      .addUser(userData)
      .subscribe(
        (response: CreateUserResponse) => {
          this._alertService.info(AlertType.INFO_CREATE_USER_SUCCESS);
          this._dialogRef.close(response);
        },
        ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_CREATE_USER_FAILED, message);
        }
      );
  }

  public get userForm(): FormGroup {
    return this._userForm;
  }

  public get isTenant$(): Observable<boolean> {
    return this._userService.isTenant$;
  }

  public get Scope(): typeof Scope {
    return Scope;
  }

  public get scopeTitles(): Map<Scope, string> {
    return SCOPE_TITLES;
  }

  public get userScope(): Scope {
    return this._userForm.get('scope').value;
  }

  public get tenants(): Tenant[] {
    return this._data.tenants;
  }

}
