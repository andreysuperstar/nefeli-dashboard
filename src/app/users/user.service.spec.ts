import { TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { UserService, CreateUserResponse, Jwt } from './user.service';
import { cloneDeep } from 'lodash-es';
import { HttpError } from '../error-codes';
import { Roles } from 'rest_client/heimdallr/model/roles';
import { User } from 'rest_client/heimdallr/model/user';
import { Users } from 'rest_client/heimdallr/model/users';
import { BASE_PATH as BASE_PATH_HEIMDALLR, } from 'rest_client/heimdallr/variables';
import { BASE_PATH as BASE_PATH_STATS } from 'rest_client/pangolin/variables';

describe('UserService', () => {
  let httpTestingController: HttpTestingController;
  let service: UserService;

  const mockUsers: User[] = [
    {
      username: 'Eric',
      email: 'eric@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    },
    {
      username: 'Andrew',
      email: 'andrew@nefeli.io',
      firstName: 'Andrew',
      lastName: 'Andrew',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'Anton',
      email: 'anton@nefeli.io',
      roles: {
        scope: 'tenant',
        id: '-1',
        roles: ['user']
      }
    }
  ];

  const nonLdapToken: Jwt = {
    aud: 'nefeli-heimdallr',
    exp: Math.round(new Date().getTime() / 1000) + 3600,
    iat: Math.round(new Date().getTime() / 1000),
    iss: 'weaver.tovino.com-heimdallr',
    jti: '86172465054303046557578602619063514986',
    ldap: false,
    nbf: Math.round(new Date().getTime() / 1000),
    roles: {
      scope: 'system',
      id: '',
      roles: [
        'admin'
      ]
    },
    sub: 'signing-key-ldap-admins'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        {
          provide: BASE_PATH_STATS,
          useValue: '/dashboard'
        },
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set user', () => {
    let subscription = service.user$.subscribe((user: User) => expect(user).toBeUndefined());

    subscription.unsubscribe();

    service.setUser(mockUsers[1]);

    subscription = service.user$.subscribe((user: User) => {
      expect(user.username).toBe(mockUsers[1].username, 'valid username');

      expect(user.email).toBe(mockUsers[1].email, 'valid email');

      const roles: Roles = mockUsers[1].roles;

      expect(user.roles.scope).toBe(roles.scope, 'valid scope');
      expect(user.roles.id).toBe(roles.id, 'valid id');

      expect(user.roles.roles).toContain(roles.roles[0], 'contain admin role');
      expect(user.roles.roles).toContain(roles.roles[1], 'contain user role');
    });

    subscription.unsubscribe();

    service.setUser(undefined);
    subscription = service.user$.subscribe((user: User) => expect(user).toBeUndefined());

    subscription.unsubscribe();
  });

  it('should return is admin and tenant', () => {
    service.setUser(mockUsers[1]);

    let adminSubscription = service.isAdmin$.subscribe(
      (isAdmin: boolean) => expect(isAdmin).toBeTruthy()
    );

    let tenantSubscription = service.isTenant$.subscribe(
      (isTenantUser: boolean) => expect(isTenantUser).toBeFalsy()
    );

    adminSubscription.unsubscribe();
    tenantSubscription.unsubscribe();

    service.setUser(mockUsers[2]);

    adminSubscription = service.isAdmin$.subscribe(
      (isAdmin: boolean) => expect(isAdmin).toBeFalsy()
    );

    tenantSubscription = service.isTenant$.subscribe(
      (isTenantUser: boolean) => expect(isTenantUser).toBeTruthy()
    );

    adminSubscription.unsubscribe();
    tenantSubscription.unsubscribe();
  });

  it('should get users', () => {
    service
      .getUsers()
      .subscribe(({ users }: Users) => {
        expect(users[0].username).toBe(mockUsers[0].username, 'valid 1st user name');
        expect(users[1].email).toBe(mockUsers[1].email, 'valid 2nd user email');

        let roles: Roles = mockUsers[1].roles;

        expect(users[1].roles.scope).toBe(roles.scope);
        expect(users[1].roles.id).toBe(roles.id);

        roles = mockUsers[2].roles;

        expect(users[2].roles.scope).toBe(roles.scope, 'valid 2nd user \'tenant-admin\' role 3rd tenant id');
        expect(users[2].roles.id).toBe(roles.id, 'valid 2nd user \'tenant-admin\' role 3rd tenant id');
        expect(users[2].roles.roles[0]).toBe(roles.roles[0], 'valid 2nd user \'tenant-admin\' role 3rd tenant id');
      });

    const req = httpTestingController.expectOne(`${environment.restServer}/auth/users`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    req.flush({
      users: mockUsers,
      pagination: {
        index: 0,
        size: mockUsers.length
      }
    } as Users);
  });

  it('should add user', () => {
    const userData: User = mockUsers[1];
    service
      .addUser(userData)
      .subscribe((response: CreateUserResponse) => {
        expect(response.invite_token).toBe("eyJhbGciOiJFUzI");
      });

    const req = httpTestingController.expectOne(`${environment.restServer}/auth/users`);

    expect(req.request.method).toBe('POST');
    expect(req.request.body.username).toBe('Andrew');

    req.flush({
      "invite_token": "eyJhbGciOiJFUzI"
    } as CreateUserResponse);
  });

  it('should edit user', () => {
    const { username } = mockUsers[1];
    const userData: Partial<User> = {
      username: 'Andrew Edited',
      email: 'andrew.edited@nefeli.io',
      roles: {
        scope: 'tenant',
        id: '-1',
        roles: []
      }
    };

    service
      .editUser(username, userData)
      .subscribe((response: string) => { });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/Andrew`
    );

    expect(req.request.method).toBe('PUT');
    expect(req.request.responseType).toBe('json');

    const mockUser: User = {
      username: userData.username,
      firstName: userData.firstName,
      lastName: userData.lastName,
      email: userData.email,
      roles: mockUsers[1].roles
    };

    req.flush(mockUser);
  });

  it('should remove user', () => {
    const { username } = mockUsers[1];

    service
      .removeUser(username)
      .subscribe((response: string) => {
        expect(response).toBeDefined('valid blank response');
      });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${username}`
    );

    expect(req.request.method).toBe('DELETE');

    const mockResponse: Partial<HttpResponse<any>> = {
      status: 200,
      statusText: 'Success Request'
    };

    req.flush({}, mockResponse);
  });

  it('should get user data and handle special users', () => {
    spyOn<any>(service, 'getDecodedToken').and.returnValue(nonLdapToken);

    service.getUser('root', '').subscribe((response: User) => {
      expect(response.username).toBe('root');
      expect(response.roles).toEqual({
        scope: 'system',
        id: 'root',
        roles: []
      });
    });

    service.getUser('nefeli', '').subscribe((response: User) => {
      expect(response.username).toBe('nefeli');
      expect(response.roles).toEqual({
        scope: 'internal',
        id: 'nefeli',
        roles: []
      });
    });

    service.getUser('ecarino', 'faketoken').subscribe();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/ecarino`
    );
    expect(req.request.method).toBe('GET');
  });

  it('should get ldap user', () => {
    const ldapToken = cloneDeep(nonLdapToken);
    ldapToken.ldap = true;
    spyOn<any>(service, 'getDecodedToken').and.returnValue(ldapToken);

    service.getUser('ecarino', 'faketoken').subscribe((response: User) => {
      expect(response.username).toBe('ecarino');
      expect(response.roles).toEqual(ldapToken.roles);
    });
  });

  it('should get error if ldap token is expired', () => {
    const getDecodedToken = service['getDecodedToken'].bind({});
    spyOn<any>(service, 'getDecodedToken').and.callFake((accessToken: string): Jwt => {
      return getDecodedToken(accessToken);
    });

    const expiredToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJuZWZlbGktaGVpbWRhbGxyIiwiZXhwIjoxNTc4OTYwMDAwLCJpYXQiOjE1Nzc4MzY4MDAsImlzcyI6ImZsdWZmeS1oZWltZGFsbHIiLCJqdGkiOiIyMzM1NTk4Njk1OTYwOTQ3MTc0MTI5MjcyOTIyNTM1MzQ5NzM3NjIiLCJuYmYiOjE1Nzc4MzY4MDAsInJvbGVzIjp7InNjb3BlIjoic3lzdGVtIiwiaWQiOiIiLCJyb2xlcyI6WyJ1c2VyIiwiYWRtaW4iXX0sInN1YiI6Im1vbnR5In0._MMnsBoRkQE7htTpl4EIkLCGWckFcW-tHtkKKCPPsFo';
    service.getUser('ecarino', expiredToken).subscribe(
      () => { },
      (response: HttpErrorResponse) => {
        expect(response.status).toBe(HttpError.Unauthorized);
        expect(response.statusText).toBe('Not Authenticated')
      });

    httpTestingController.expectOne(`${environment.restServer}/auth/users/ecarino`).flush({}, {
      status: HttpError.Unauthorized,
      statusText: 'Not Authenticated'
    });
  });
});
