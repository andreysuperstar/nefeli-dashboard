import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Subscription, forkJoin, Observable } from 'rxjs';

import { Site } from 'rest_client/pangolin/model/site';
import { User } from 'rest_client/heimdallr/model/user';
import { Column, ColumnType } from '../shared/table/table.component';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from './user.service';
import { TenantService, Tenant } from 'src/app/tenant/tenant.service';
import { ClusterService as SitesService } from 'src/app/home/cluster.service';

import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';
import { UsersDataSource } from './users.data-source';

export const COLUMNS: Column[] = [
  {
    name: 'select',
    type: ColumnType.SELECT
  },
  {
    label: 'name',
    name: 'user',
    type: ColumnType.USER,
    sortable: true
  },
  {
    name: 'role',
    type: ColumnType.ROLE,
    sortable: false
  },
  {
    name: 'tenant',
    type: ColumnType.TENANT,
    sortable: false
  }
];

export interface UserDialogData {
  user?: User;
  users?: string[];
  sites?: Site[];
  tenants?: Tenant[];
}

const DIALOG_CONFIG: MatDialogConfig<UserDialogData> = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit, OnDestroy {

  private _selectedUsers: string[];
  private _sites: Site[];
  private _dataSource: UsersDataSource;
  private _usersSubscription: Subscription;
  private _removeUsersSubscription: Subscription;
  private _dialogSubscription: Subscription;

  constructor(
    private _alertService: AlertService,
    private _userService: UserService,
    private _sitesService: SitesService,
    private _tenantService: TenantService,
    private _dialog: MatDialog
  ) { }

  public ngOnInit() {
    this._dataSource = new UsersDataSource(this._userService, this._tenantService);
    this._dataSource.fetch();

    this._sitesService
      .getSites()
      .subscribe(({ sites }) => {
        this._sites = sites;
      });
  }

  public ngOnDestroy() {
    if (this._usersSubscription) {
      this._usersSubscription.unsubscribe();
    }

    if (this._removeUsersSubscription) {
      this._removeUsersSubscription.unsubscribe();
    }

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }
  }

  public onSelectUsers(rows: string[]) {
    this._selectedUsers = rows;
  }

  public onAddUser() {
    const data: UserDialogData = {
      sites: this._sites,
      tenants: this._dataSource.tenants
    };

    const dialogRef = this._dialog.open(AddUserDialogComponent, { ...DIALOG_CONFIG, data });

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .subscribe((user: User) => {
        this._dataSource.total += 1;
        this._dataSource.fetch();
      });
  }

  public onEditUser(username: string) {
    const editableUser: User = this._dataSource.users.find(
      (user: User): boolean => user.username === username
    );

    const data: UserDialogData = {
      user: editableUser,
      tenants: this._dataSource.tenants
    };

    const dialogRef = this._dialog.open(EditUserDialogComponent, { ...DIALOG_CONFIG, data });

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .subscribe((editedUser: User) => {
        if (!editedUser) {
          return;
        }

        this._dataSource.update(editedUser);
      });
  }

  public onRemoveUsers() {
    if (this._removeUsersSubscription) {
      this._removeUsersSubscription.unsubscribe();
    }

    this._removeUsersSubscription = forkJoin(
      this._selectedUsers.map(
        (username: string): Observable<any> => this._userService.removeUser(username)
      )
    )
      .subscribe(
        (_: HttpResponse<any>[]) => {
          const numDeleted = this._selectedUsers.length;
          this._dataSource.remove(this._selectedUsers);

          if (!this._dataSource.length) {
            this._selectedUsers = undefined;
          }

          this._alertService.info(numDeleted > 1
            ? AlertType.INFO_REMOVE_USERS_SUCCESS
            : AlertType.INFO_REMOVE_USER_SUCCESS);
        },
        (_: HttpErrorResponse) =>
          this._alertService.error(AlertType.ERROR_REMOVE_USERS_FAILED)
      );
  }

  public get selectedUsers(): string[] {
    return this._selectedUsers;
  }

  public get columns(): Column[] {
    return COLUMNS;
  }

  public get dataSource(): UsersDataSource {
    return this._dataSource;
  }

  public get isSystem$(): Observable<boolean> {
    return this._userService.isSystem$;
  }

  public get isAdmin$(): Observable<boolean> {
    return this._userService.isAdmin$;
  }
}
