import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, flush } from '@angular/core/testing';
import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { environment } from 'src/environments/environment';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { User } from 'rest_client/heimdallr/model/user';
import { Users } from 'rest_client/heimdallr/model/users';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { AlertService } from '../shared/alert.service';
import { UserService } from './user.service';
import { Tenant, TenantService } from 'src/app/tenant/tenant.service';
import { ClusterService as SitesService } from 'src/app/home/cluster.service';

import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { UsersComponent, COLUMNS } from './users.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { EditUserDialogComponent } from './edit-user-dialog/edit-user-dialog.component';
import { EditUserRolesDialogComponent } from './edit-user-roles-dialog/edit-user-roles-dialog.component';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;
  let httpTestingController: HttpTestingController;
  let userService: UserService;

  let usersDe: DebugElement;
  let usersEl: HTMLElement;
  let tableEl: HTMLElement;

  enum UserType {
    SystemAdmin,
    TenantAdmin,
    SystemUser,
    TenantUser
  }

  const mockUsers: User[] = [
    {
      username: 'Eric',
      firstName: 'Eric',
      lastName: 'Carino',
      email: 'eric@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'Andrew',
      firstName: 'Andrew',
      lastName: 'Carino',
      email: 'andrew@nefeli.io',
      roles: {
        scope: 'tenant',
        id: 'Stumptown Coffee',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'Anton',
      firstName: 'Anton',
      lastName: 'Zhusman',
      email: 'anton@nefeli.io',
      roles: {
        scope: 'system',
        id: '-2',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'User',
      firstName: '',
      lastName: '',
      email: 'user@test',
      roles: {
        scope: 'tenant',
        id: '-4',
        roles: ['user']
      }
    },
    {
      username: 'root',
      firstName: 'System',
      lastName: 'User',
      email: 'sys@test',
      roles: {
        scope: 'system',
        id: '-5',
        roles: []
      }
    }
  ];

  const mockTenants: Tenant[] = [
    {
      identifier: '-1',
      name: 'Stumptown Coffee'
    },
    {
      identifier: '-2',
      name: 'World Market'
    },
    {
      identifier: '-3',
      name: 'Bed, Bath & Beyond'
    },
    {
      identifier: '-4',
      name: 'Costco'
    }
  ];

  const mockTenantsResp: Tenants = {
    tenants: mockTenants,
    metadata: {
      index: 0,
      total: mockTenants.length
    }
  };

  const mockEricSite: Site = {
    config: {
      identifier: 'eric_site',
      name: 'Eric Site'
    }
  };

  const mockAndrewSite: Site = {
    config: {
      identifier: 'andrew_site',
      name: 'Andrew Site'
    }
  };

  const mockAntonSite: Site = {
    config: {
      identifier: 'anton_site',
      name: 'Anton Site'
    }
  };

  const mockSiteResponse: Sites = {
    sites: [
      mockEricSite,
      mockAndrewSite,
      mockAntonSite
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule,
        MatDividerModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatRadioModule,
        MatSelectModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        AvatarComponent,
        UsersComponent,
        TableComponent,
        AddUserDialogComponent,
        EditUserDialogComponent,
        EditUserRolesDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        AlertService,
        UserService,
        SitesService,
        TenantService,
        DecimalPipe
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    usersDe = fixture.debugElement;
    usersEl = usersDe.nativeElement;
    fixture.detectChanges();

    const tenantsRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants`
    );

    expect(tenantsRequest.request.method).toBe('GET');
    expect(tenantsRequest.request.responseType).toBe('json');
    tenantsRequest.flush(mockTenantsResp);

    const usersRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/auth/users?index=0&limit=50`
    );
    expect(usersRequest.request.method).toBe('GET');
    usersRequest.flush({
      users: mockUsers,
      metadata: {
        count: mockUsers.length,
        filter: [],
        search: [],
        sort: [],
        total: mockUsers.length
      }
    } as Users);

    const sitesRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites`
    );
    expect(sitesRequest.request.method).toBe('GET');
    sitesRequest.flush(mockSiteResponse);

    fixture.detectChanges();
    tableEl = usersEl.querySelector('nef-table');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page heading', () => {
    expect(usersEl.querySelector('header h2').textContent).toBe('Users', 'valid heading');
  });

  it('should render \'Add New User\' button', () => {
    expect(usersEl.querySelector('header button.add span').textContent).toBe('Add New User', 'valid \'Add New User\' button text');
  });

  it('should open dialog window on \'Add New User\' button click', () => {
    const addButtonEl: HTMLButtonElement = usersDe.query(
      By.css('button.add')
    ).nativeElement;

    addButtonEl.click();

    expect(usersEl.querySelector('nef-add-user-dialog')).toBeDefined();
  });

  it('should render users table', () => {
    expect(usersEl.querySelector('nef-table')).toBeTruthy('table is rendered');
    expect(usersEl.querySelectorAll('nef-table mat-header-cell').length).toBe(4);
  });

  it('should set users and tenants', () => {
    const users: User[] = component.dataSource.users;
    const tenants: Tenant[] = component.dataSource.tenants;

    expect(users.length).toBe(mockUsers.length, 'valid quantity of users');
    expect(users[0].username).toBe(mockUsers[0].username, 'valid 1st user username');
    expect(users[1].username).toBe(mockUsers[1].username, 'valid 2nd user username');
    expect(users[2].username).toBe(mockUsers[2].username, 'valid 3rd user username');
    expect(users[1].roles.scope).toBe('tenant');
    expect(users[1].roles.id).toBe('Stumptown Coffee');
    expect(users[1].roles.roles[0]).toBe('admin');
    expect(users[1].roles.roles[1]).toBe('user');

    expect(tenants[0]).toBe(mockTenants[0], 'set tenants');
    expect(tenants[1]).toBe(mockTenants[1], 'set tenants');
    expect(tenants[2]).toBe(mockTenants[2], 'set tenants');
    expect(tenants[3]).toBe(mockTenants[3], 'set tenants');
    expect(tenants.length).toBe(mockTenants.length, 'valid quanitty of tenants');
    expect(tenants[1].identifier).toBe(mockTenants[1].identifier, 'valid 2nd tenant id');
    expect(tenants[3].name).toBe(mockTenants[3].name, 'valid 4th tenant name');
  });

  it('should set dataSource', () => {
    expect(component.dataSource.rows[1].user.username).toBe(mockUsers[1].username, 'valid 2nd user username');

    expect(component.dataSource.rows[2].role).toContain('System User', '3rd user contains \'System User\' role');

    expect(component.dataSource.rows[2].tenant[0][0]).toBe(mockTenants[1].name, 'valid 3rd user 3rd role 4th tenant name');
  });

  it('should render valid table columns', () => {
    const headerCellEls: NodeListOf<HTMLElement> = tableEl.querySelectorAll('mat-header-cell');

    expect(headerCellEls[0].querySelector('mat-checkbox')).toBeTruthy('1st header cell contains checkbox');

    expect(headerCellEls[1].textContent).toContain(COLUMNS[1].label, 'valid 2nd column name');
    expect(headerCellEls[2].textContent).toContain(COLUMNS[2].name, 'valid 3rd column name');
    expect(headerCellEls[3].textContent).toContain(COLUMNS[3].name, 'valid 4th column name');
  });

  it('should render valid rows', () => {
    const rowEls: NodeListOf<HTMLElement> = tableEl.querySelectorAll('mat-row');

    expect(rowEls.length).toBe(5, 'valid quantity of rows');

    const firstRowCellEls: NodeListOf<HTMLElement> = rowEls[0].querySelectorAll('mat-cell');
    expect(firstRowCellEls[1].querySelector('strong').textContent).toBe(`${mockUsers[0].firstName} ${mockUsers[0].lastName}`, 'valid 1st row 2nd cell name');
    expect(firstRowCellEls[1].querySelectorAll('li')[1].textContent).toBe(mockUsers[0].email, 'valid 1st row 2nd cell email');

    const secondRowCellEls: NodeListOf<HTMLElement> = rowEls[1].querySelectorAll('mat-cell');

    expect(secondRowCellEls[0].querySelector('mat-checkbox')).toBeTruthy('2nd row 1st cell contains checkbox');

    expect(secondRowCellEls[2].querySelector('li').textContent).toBe('Tenant Admin', 'valid 2nd row 3rd cell role');
    expect(secondRowCellEls[2].querySelectorAll('li')[1].textContent).toBe('Tenant User', 'valid 2nd row 3rd cell role');

    const thirdRowCellEls: NodeListOf<HTMLElement> = rowEls[2].querySelectorAll('mat-cell');

    expect(thirdRowCellEls[3].querySelector('li').textContent).toBe(mockTenants[1].name, 'valid 3rd row 4th cell tenant');
  });

  it('should emit edited username', () => {
    const userCellEl: HTMLElement = tableEl.querySelectorAll('mat-cell')[1] as HTMLElement;
    const editButtonEl: HTMLButtonElement = userCellEl.querySelector('button');

    spyOn(component, 'onEditUser');

    editButtonEl.click();

    expect(component.onEditUser).toHaveBeenCalledWith(mockUsers[0].username);
  });

  it('should display edit button for system and tenant admins only', () => {
    let userCellEl: HTMLElement = tableEl.querySelectorAll('mat-cell')[1] as HTMLElement;
    let editButtonEl: HTMLButtonElement = userCellEl.querySelector('button');
    expect(editButtonEl).not.toBeNull();

    userService.setUser(mockUsers[UserType.TenantAdmin]);
    fixture.detectChanges();
    editButtonEl = userCellEl.querySelector('button');
    expect(editButtonEl).not.toBeNull();

    userService.setUser(mockUsers[UserType.SystemUser]);
    userCellEl = tableEl.querySelectorAll('mat-cell')[0] as HTMLElement;
    fixture.detectChanges();
    editButtonEl = userCellEl.querySelector('button');
    expect(editButtonEl).toBeNull();
  });

  it('should show an alert if remove user action failed', fakeAsync(() => {
    spyOn(component['_alertService'], 'error');
    component['_selectedUsers'] = [mockUsers[1].username];
    fixture.detectChanges();
    let removeButtonEl: HTMLButtonElement = (
      usersEl.querySelectorAll('header button[mat-button]')[0] as HTMLButtonElement
    );
    removeButtonEl.click();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockUsers[1].username}`
    );
    req.flush({}, {
      status: 400,
      statusText: 'mock error'
    });

    // verify error alert is displayed
    expect(component['_alertService'].error).toHaveBeenCalled();
  }));

  it('should leave an empty space for the first name and last name if they are not defined', () => {
    const rowEls: NodeListOf<HTMLElement> = tableEl.querySelectorAll('mat-row');
    const firstRowCellEls: NodeListOf<HTMLElement> = rowEls[3].querySelectorAll('mat-cell');
    expect(firstRowCellEls[1].querySelector('strong').textContent.trim()).toBe('', 'valid empty cell name');
  });

  it('should edit user', fakeAsync(() => {
    const rows = fixture.debugElement.queryAll(By.css('mat-row'));
    const cell = rows[0].queryAll(By.css('mat-cell'));
    const editButton = cell[1].query(By.css('button'));
    editButton.nativeElement.click();
    fixture.detectChanges();
    tick();

    const dialogRef: MatDialogRef<EditUserDialogComponent> = component['_dialog'].openDialogs[0];
    const dialog: EditUserDialogComponent = dialogRef.componentInstance;

    dialogRef.afterOpened().subscribe(fakeAsync(() => {
      dialog.userForm.patchValue({ firstName: 'newName' });
      dialog.userForm.markAsDirty();
      dialog.submitUserForm();

      const req = httpTestingController.expectOne(`${environment.restServer}/auth/users/Eric`);
      req.flush({});
      fixture.detectChanges();
      flush();
    }));

    dialogRef.afterClosed().subscribe(fakeAsync(() => {
      tick();
      fixture.detectChanges();
      expect(usersEl.querySelectorAll('mat-row')[0].querySelectorAll('mat-cell')[1].querySelector('strong').textContent).toBe("newName Carino");
      flush();
    }));

  }));

  it('should remove users', () => {
    let rowEls: NodeListOf<HTMLElement> = tableEl.querySelectorAll('mat-row');
    expect(rowEls.length).toBe(5, 'valid quantity of rows');

    component['_selectedUsers'] = [mockUsers[1].username];
    fixture.detectChanges();

    let removeButtonEl: HTMLButtonElement = (
      usersEl.querySelectorAll('header button[mat-button]')[0] as HTMLButtonElement
    );

    expect(removeButtonEl).toBeDefined();
    expect(
      removeButtonEl.querySelector('span').textContent
    ).toBe('Remove', 'valid \'Remove\' button text');

    spyOn(component, 'onRemoveUsers').and.callThrough();
    removeButtonEl.click();
    expect(component.onRemoveUsers).toHaveBeenCalled();

    const req = httpTestingController.expectOne(
      `${environment.restServer}/auth/users/${mockUsers[1].username}`
    );

    expect(req.request.method).toBe('DELETE');
    req.flush({});

    fixture.detectChanges();

    rowEls = tableEl.querySelectorAll('mat-row');
    expect(rowEls.length).toEqual(4, 'valid quantity of rows');
  });

  it('should request sorted list', () => {
    const column: HTMLElement = tableEl.querySelector('mat-header-row .mat-sort-header-container');
    column.click();
    fixture.detectChanges();
    const req = httpTestingController.expectOne({
      url: `${environment.restServer}/auth/users?index=0&sort=asc(username)&limit=50`,
      method: 'GET'
    });
    req.flush({});
  });
});
