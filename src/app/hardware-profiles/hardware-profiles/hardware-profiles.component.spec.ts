import { OverlayModule } from '@angular/cdk/overlay';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatHeaderRowHarness, MatRowHarness, MatTableHarness } from '@angular/material/table/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HardwareService } from 'rest_client/pangolin/api/hardware.service';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { HardwareProfiles } from 'rest_client/pangolin/model/hardwareProfiles';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { AlertType } from 'src/app/shared/alert.service';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { environment } from 'src/environments/environment';

import { HardwareProfilesComponent } from './hardware-profiles.component';

describe('HardwareProfilesComponent', () => {
  let component: HardwareProfilesComponent;
  let fixture: ComponentFixture<HardwareProfilesComponent>;
  let el: HTMLElement;
  let tableEl: HTMLElement;
  let httpTestingController: HttpTestingController;
  let hardwareService: HardwareService;
  let loader: HarnessLoader;

  const hardwareProfiles: HardwareProfile[] = [{
    bonds: [
      {
        identifier: 'bond_1',
        bond: {
          purpose: SubnetType.WAN,
          devices: ['device1', 'device2', 'device3']
        }
      },
      {
        identifier: 'bond_2',
        bond: {}
      }
    ],
    description: '',
    name: 'Profile 1',
    identifier: 'profile_1'
  }, {
    bonds: [
      {
        identifier: 'bond_1',
        bond: {
          purpose: SubnetType.WAN,
          devices: ['device1', 'device2', 'device3']
        }
      },
      {
        identifier: 'bond_2',
        bond: {}
      }
    ],
    description: '',
    name: 'Profile 2',
    identifier: 'profile_2'
  }, {
    bonds: [
      {
        identifier: 'bond_1',
        bond: {
          purpose: SubnetType.WAN,
          devices: ['device1', 'device2', 'device3']
        }
      },
      {
        identifier: 'bond_2',
        bond: {}
      }
    ],
    description: '',
    name: 'Profile 3',
    identifier: 'profile_3'
  }];

  const mockProfiles: HardwareProfiles = { hardwareProfiles };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatDialogModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatTableModule,
        MatCardModule,
        OverlayModule
      ],
      declarations: [
        HardwareProfilesComponent,
        TableComponent
      ],
      providers: [
        DecimalPipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    })
      .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
    hardwareService = TestBed.inject(HardwareService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HardwareProfilesComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
    tableEl = el.querySelector('nef-table');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render valid table columns', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows: MatHeaderRowHarness[] = await table.getHeaderRows();
    const cells = await rows[0].getCellTextByIndex();
    expect(cells).toEqual(["name", "description", "actions"]);
  });

  it('should render hardware profiles table', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);

    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`).flush(mockProfiles);
    fixture.detectChanges();

    const rows: MatRowHarness[] = await table.getRows();
    expect(rows.length).toBe(3);
    const row1 = await rows[0].getCellTextByColumnName();
    const row2 = await rows[1].getCellTextByColumnName();
    const row3 = await rows[2].getCellTextByColumnName();
    expect(row1.name).toBe(mockProfiles.hardwareProfiles[0].name);
    expect(row2.name).toBe(mockProfiles.hardwareProfiles[1].name);
    expect(row3.name).toBe(mockProfiles.hardwareProfiles[2].name);
  });

  it('should remove profile', waitForAsync(() => {
    spyOn(component['_alertService'], 'info');

    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`).flush(mockProfiles);
    fixture.detectChanges();

    const removeButton: HTMLButtonElement = tableEl.querySelectorAll('mat-row')[2].querySelector('.action');
    removeButton.click();

    fixture.detectChanges();
    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    dialog.close(true);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const request = httpTestingController.expectOne({
        url: `${environment.restServer}/v1/hardware/profiles/profile_3`,
        method: 'DELETE'
      });
      request.flush({});
      expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_REMOVE_HARDWARE_PROFILE_SUCCESS);
    });
  }));

  it('should display profile details popup', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`).flush(mockProfiles);
    fixture.detectChanges();

    const detailButton = el.querySelectorAll<HTMLButtonElement>('nef-table mat-row button.details')[0];
    detailButton.click();
    fixture.detectChanges();

    const popupCard = document.querySelector('.cdk-overlay-container mat-card');
    expect(popupCard.querySelector('mat-card-title').textContent).toBe('Profile 1');
    expect(popupCard.querySelector('mat-card-content > div').textContent)
      .toBe('Interface bond_1Socket: Bandwidth: Driver: Connectivity: Kernel: Purpose: WANDevices: device1,device2,device3');
  });
});
