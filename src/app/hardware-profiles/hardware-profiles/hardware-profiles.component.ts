import { Subscription, of } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectedPosition } from '@angular/cdk/overlay';
import {
  TableActions,
  TableActionButton,
  TableComponent,
  TableActionEvent,
  ColumnType,
  Column
} from 'src/app/shared/table/table.component';
import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { AlertService, AlertType } from '../../shared/alert.service';
import { HardwareProfileExpDataplaneBondWithID } from 'rest_client/pangolin/model/hardwareProfileExpDataplaneBondWithID';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { NicBandwidth } from 'rest_client/pangolin/model/nicBandwidth';
import { HardwareService } from 'rest_client/pangolin/api/hardware.service';
import { CdkOverlayOrigin } from '@angular/cdk/overlay';
import { MatButton } from '@angular/material/button';

export const getBandwidthLabel = (bw: NicBandwidth): string => {
  switch (bw) {
    case NicBandwidth.MB100:
      return '100 Mb';
    case NicBandwidth.GB1:
      return '1 Gb';
    case NicBandwidth.GB10:
      return '10 Gb';
    case NicBandwidth.GB25:
      return '25 Gb';
    case NicBandwidth.GB40:
      return '40 Gb';
    case NicBandwidth.GB50:
      return '50 Gb';
    case NicBandwidth.GB100:
      return '100 Gb';
    case NicBandwidth.GB200:
      return '200 Gb';
    case NicBandwidth.GB400:
      return '400 Gb';
    default:
      return bw;
  }
};

const CONFIRM_DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

const COLUMNS: Column[] = [
  {
    name: 'name'
  },
  {
    name: 'description'
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS
  }
];

const ACTIONS = [{
  text: 'Remove',
  action: TableActions.Remove,
  icon: 'trash-blue'
}, {
  text: 'Details',
  action: TableActions.Details,
  icon: 'chevron-bottom-blue'
}];

interface HardwareProfileRow {
  id: string;
  name: string;
  description: string;
  bonds: Array<HardwareProfileExpDataplaneBondWithID>;
  actions: Array<TableActionButton>;
}

@Component({
  selector: 'nef-hardware-profiles',
  templateUrl: './hardware-profiles.component.html',
  styleUrls: ['./hardware-profiles.component.less']
})
export class HardwareProfilesComponent implements OnInit {
  private _hardwareProfiles: HardwareProfile[] = [];
  private _deleteDialogSubscription: Subscription;
  private _dataSource = new Array<HardwareProfileRow>();
  private _showDetails = false;
  private _detailsOverlayOrigin: CdkOverlayOrigin;
  private _selectedRow: HardwareProfileRow | undefined;
  private _detailsConnectedPositions: ConnectedPosition[] = [
    {
      originX: 'start',
      originY: 'bottom',
      overlayX: 'end',
      overlayY: 'bottom'
    }
  ];

  @ViewChild('hardwareProfilesTable', { static: true }) private _hardwareProfilesTable: TableComponent;

  constructor(
    private _dialog: MatDialog,
    private _hardwareService: HardwareService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this.setDataSource();
  }

  private setDataSource() {
    this._dataSource = [];

    this._hardwareService.getHardwareProfiles().subscribe(profiles => {

      profiles.hardwareProfiles.forEach(({ identifier, name, description, bonds }: HardwareProfile) => {
        this._dataSource.push({
          id: identifier,
          name: name,
          description: description,
          actions: ACTIONS,
          bonds
        } as HardwareProfileRow);
      });

      this._hardwareProfilesTable?.renderRows();
      this.sortTable();
    });
  }

  private sortTable() {
    this._hardwareProfilesTable?.setSortDetails({
      id: 'name',
      start: 'asc',
      disableClear: false
    });
  }

  private deleteHardwareProfile({ id, name }: HardwareProfileRow) {
    if (this._deleteDialogSubscription) {
      this._deleteDialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: `This will remove the hardware profile <strong>${name}</strong>.`,
      action: 'Remove Hardware Profile'
    };

    if (id) {
      const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
        ConfirmationDialogComponent,
        { ...CONFIRM_DIALOG_CONFIG, data }
      );

      this._deleteDialogSubscription = dialogRef.afterClosed().pipe(
        filter(Boolean),
        switchMap((_: boolean) => {
          if (id) {
            return this._hardwareService.deleteHardwareProfile(id);
          } else {
            return of({});
          }
        })
      ).subscribe(
        () => {
          const index: number = this._hardwareProfiles.findIndex(
            (hardwareProfile: HardwareProfile): boolean => hardwareProfile.identifier === id
          );
          this._hardwareProfiles.splice(index, 1);

          this.setDataSource();
          this._alertService.info(AlertType.INFO_REMOVE_HARDWARE_PROFILE_SUCCESS);
        },
        (error: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_REMOVE_HARDWARE_PROFILE);
        }
      );
    }
  }

  private showDetailsPopup(profileRow: HardwareProfileRow) {
    this._selectedRow = profileRow;
    this._showDetails = true;
  }

  public onActionClick(event: TableActionEvent) {
    if (event.type === TableActions.Remove) {
      this.deleteHardwareProfile(event.data as HardwareProfileRow);
    } else if (event.type === TableActions.Details) {
      const detailButton: MatButton = event.elem as MatButton;
      this._detailsOverlayOrigin = new CdkOverlayOrigin(detailButton._elementRef as ElementRef || event.elem as ElementRef);
      this.showDetailsPopup(event.data as HardwareProfileRow);
    }
  }

  public onBackdropClick() {
    this._showDetails = false;
  }

  public getBandwidthLabel(bw: NicBandwidth): string {
    return getBandwidthLabel(bw);
  }

  public addProfile({ identifier, name, description, bonds }: HardwareProfile) {
    this._dataSource.push({
      id: identifier,
      name,
      description: description,
      actions: ACTIONS,
      bonds
    });

    this._hardwareProfilesTable?.renderRows();
    this.sortTable();
  }

  public get columns(): Column[] {
    return COLUMNS;
  }

  public get dataSource(): HardwareProfileRow[] {
    return this._dataSource;
  }

  public get showDetails(): boolean {
    return this._showDetails;
  }

  public get detailsOverlayOrigin(): CdkOverlayOrigin {
    return this._detailsOverlayOrigin;
  }

  public get selectedRow(): HardwareProfileRow {
    return this._selectedRow;
  }

  public get detailsConnectedPositions(): ConnectedPosition[] {
    return this._detailsConnectedPositions;
  }
}
