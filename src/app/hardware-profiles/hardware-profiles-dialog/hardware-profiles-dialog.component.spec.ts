import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DecimalPipe } from '@angular/common';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { cloneDeep } from 'lodash-es';
import { NicBandwidth } from 'rest_client/pangolin/model/nicBandwidth';
import { BondMode } from 'rest_client/pangolin/model/bondMode';
import { BondModeXorPolicy } from 'rest_client/pangolin/model/bondModeXorPolicy';
import { TunnelType } from 'rest_client/pangolin/model/tunnelType';
import { NicDriver } from 'rest_client/pangolin/model/nicDriver';
import { KernelMode } from 'rest_client/pangolin/model/kernelMode';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';
import { TableComponent } from '../../shared/table/table.component';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { HardwareProfilesDialogComponent, HardwareProfileDialogData } from './hardware-profiles-dialog.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { HardwareProfilesComponent } from '../hardware-profiles/hardware-profiles.component';
import { HardwareProfilesFormComponent } from '../../hardware-profiles/hardware-profiles-form/hardware-profiles-form.component';
import { HardwareProfile } from 'rest_client/pangolin/model/models';
import { MatTableHarness } from '@angular/material/table/testing';

const mockDialogData: HardwareProfileDialogData = {
  hardwareProfiles: [
    {
      identifier: '-1',
      name: 'Profile 1',
      bonds: [
        {
          identifier: 'bond_1',
          bond: {
            bandwidth: NicBandwidth.GB1,
            bondMode: BondMode.TXLB,
            bondModeXorPolicy: BondModeXorPolicy.L2,
            connectivity: TunnelType.VLAN,
            devices: ['device 1', 'device 2', 'device 3'],
            driver: NicDriver.IGBUIO,
            kernel: KernelMode.BYPASS,
            macAddr: 'ac:de:48:00:11:22',
            purpose: SubnetType.GENERIC,
            socket: 'sock'
          }
        }
      ]
    },
    {
      identifier: '-2',
      name: 'Profile 2'
    },
    {
      identifier: '-3',
      name: 'Profile 3'
    }
  ]
};

describe('HardwareProfilesDialogComponent', () => {
  let component: HardwareProfilesDialogComponent;
  let fixture: ComponentFixture<HardwareProfilesDialogComponent>;
  let el: HTMLElement;
  let tableEl: HTMLElement;
  let loader: HarnessLoader;

  const mockProfile: HardwareProfile = {
    bonds: [],
    description: 'Mock Profile Description',
    identifier: 'mock-profile-id',
    name: 'Mock Profile'
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        ReactiveFormsModule,
        MatTableModule,
        MatButtonModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        OverlayModule
      ],
      declarations: [
        HardwareProfilesComponent,
        HardwareProfilesFormComponent,
        HardwareProfilesDialogComponent,
        TableComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        DecimalPipe,
        {
          provide: MAT_DIALOG_DATA,
          useValue: cloneDeep(mockDialogData)
        }
      ]
    })
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HardwareProfilesDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    tableEl = el.querySelector('nef-table');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render hardware profiles table', () => {
    const table = tableEl.querySelector('mat-table')
    expect(table).not.toBeNull()
  });

  it('should render a profile form', async () => {
    const addNewProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Profile'
    }));

    await addNewProfileButton.click();

    await loader.getHarness(MatCardHarness);

    const softwareProfileFormDe = fixture.debugElement.query(By.css('mat-dialog-content mat-card nef-hardware-profile-form'));

    expect(softwareProfileFormDe).toBeDefined('render a Hardware Profile Form component');

    const hideButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Hide'
    }));

    await hideButton.click();

    let profileFormCard: MatCardHarness;

    try {
      profileFormCard = await loader.getHarness(MatCardHarness);
    } catch { }

    expect(profileFormCard).toBeUndefined('hide profile form card');
  });

  it('should listen for new profiles', async () => {
    const addNewProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Profile'
    }));

    await addNewProfileButton.click();

    component['_hardwareProfilesForm']['_createdProfile'].next(mockProfile);

    const table = await loader.getHarness(MatTableHarness);
    const rows = await table.getRows();
    const cells = await rows[0].getCellTextByIndex();

    expect(cells[0]).toBe('Mock Profile');
    expect(cells[1]).toBe('Mock Profile Description');
  });
});
