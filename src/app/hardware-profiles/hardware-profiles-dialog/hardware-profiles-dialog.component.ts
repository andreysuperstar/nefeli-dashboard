import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { Subscription } from 'rxjs';
import { HardwareProfilesComponent } from '../hardware-profiles/hardware-profiles.component';
import { HardwareProfilesFormComponent } from '../hardware-profiles-form/hardware-profiles-form.component';

export interface HardwareProfileDialogData {
  hardwareProfiles: HardwareProfile[];
}

@Component({
  selector: 'nef-hardware-profiles-dialog',
  templateUrl: './hardware-profiles-dialog.component.html',
  styleUrls: ['./hardware-profiles-dialog.component.less']
})
export class HardwareProfilesDialogComponent implements OnInit, OnDestroy {
  private _showNewProfile = false;
  private _hardwareProfileSubscription: Subscription;

  @ViewChild('hardwareProfiles', { static: true }) private _hardwareProfilesTable: HardwareProfilesComponent;
  @ViewChild('profilesForm', { static: false }) protected _hardwareProfilesForm: HardwareProfilesFormComponent;

  constructor() { }

  public ngOnInit() { }

  public ngOnDestroy() {
    this._hardwareProfileSubscription?.unsubscribe();
  }

  private listenNewProfiles() {
    this._hardwareProfileSubscription?.unsubscribe();

    setTimeout(() =>
      this._hardwareProfileSubscription = this._hardwareProfilesForm?.createdProfile$.subscribe((profile: HardwareProfile) => {
        if (profile) {
          this._hardwareProfilesTable.addProfile(profile);
        }
      })
    );
  }

  public toggleNewProfileForm() {
    this._showNewProfile = !this._showNewProfile;

    if (this._showNewProfile) {
      this.listenNewProfiles();
    } else {
      this._hardwareProfileSubscription?.unsubscribe();
    }
  }

  public get showNewProfile(): boolean {
    return this._showNewProfile;
  }
}
