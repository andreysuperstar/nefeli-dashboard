import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SharedModule } from '../shared/shared.module';

import { HardwareProfilesComponent } from './hardware-profiles/hardware-profiles.component';
import { HardwareProfilesFormComponent } from './hardware-profiles-form/hardware-profiles-form.component';
import { HardwareProfilesDialogComponent } from './hardware-profiles-dialog/hardware-profiles-dialog.component';

@NgModule({
  declarations: [
    HardwareProfilesComponent,
    HardwareProfilesFormComponent,
    HardwareProfilesDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    OverlayModule,
    FlexLayoutModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    SharedModule
  ],
  exports: [
    HardwareProfilesComponent,
    HardwareProfilesFormComponent,
    HardwareProfilesDialogComponent
  ]
})
export class HardwareProfilesModule { }
