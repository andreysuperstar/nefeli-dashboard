import { Component, Input, OnInit } from '@angular/core';
import { NicBandwidth } from 'rest_client/pangolin/model/nicBandwidth';
import { TunnelType } from 'rest_client/pangolin/model/tunnelType';
import { NicDriver } from 'rest_client/pangolin/model/nicDriver';
import { KernelMode } from 'rest_client/pangolin/model/kernelMode';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';
import { AbstractControl, AbstractControlOptions, FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { PCI_PATTERN } from 'src/app/sites-overview/site.validation';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { Observable, Subject, Subscription } from 'rxjs';
import { HardwareProfileExpDataplaneBondWithID } from 'rest_client/pangolin/model/hardwareProfileExpDataplaneBondWithID';
import { ExpDataplaneBond } from 'rest_client/pangolin/model/expDataplaneBond';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { HardwareService } from 'rest_client/pangolin/api/hardware.service';
import { getBandwidthLabel } from '../hardware-profiles/hardware-profiles.component';

interface FormBond {
  socket: string;
  bandwidth: string;
  driver: string;
  device: string;
  kernel: string;
  subnet: string;
  connectivity: string;
}

@Component({
  selector: 'nef-hardware-profiles-form',
  templateUrl: './hardware-profiles-form.component.html',
  styleUrls: ['./hardware-profiles-form.component.less']
})
export class HardwareProfilesFormComponent implements OnInit {
  private _hardwareProfileForm: FormGroup;
  private _isProcessing = false;
  private _hardwareProfileSubscription: Subscription;
  private _hideSave = false;
  private _createdProfile = new Subject();

  @Input()
  public set hideSave(hide: boolean) {
    this._hideSave = hide;
  }

  constructor(
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _hardwareService: HardwareService
  ) { }

  public ngOnInit(): void {
    this.initHardwareProfileForm();
  }

  private initHardwareProfileForm() {
    this._hardwareProfileForm = this._fb.group(
      {
        name: ['', Validators.required],
        description: '',
        bonds: this._fb.array([
          this.createBondFormGroup({ subnet: SubnetType.LAN }),
          this.createBondFormGroup({ subnet: SubnetType.WAN })
        ])
      },
      {
        validators: this.bondsValidator.bind(this)
      } as AbstractControlOptions
    );
  }

  public submitForm() {
    const {
      invalid,
      pristine,
      value: {
        name,
        description,
        bonds
      }
    } = this._hardwareProfileForm;

    if (invalid || pristine) {
      return;
    }

    if (this._hardwareProfileSubscription) {
      this._hardwareProfileSubscription.unsubscribe();
    }

    const interfaceBonds: HardwareProfileExpDataplaneBondWithID[] = bonds.map(
      ({ socket, bandwidth, driver, device, kernel, subnet, connectivity }: FormBond): HardwareProfileExpDataplaneBondWithID => {
        return {
          bond: {
            socket: socket + '', // API expects socket as a string
            bandwidth: bandwidth as NicBandwidth,
            driver: driver as NicDriver,
            devices: device.split(/[\s,]+/),
            kernel: kernel as KernelMode,
            purpose: subnet as SubnetType,
            connectivity: connectivity as TunnelType
          } as ExpDataplaneBond
        };
      }
    );

    const newProfile: HardwareProfile = {
      name: name,
      description,
      bonds: interfaceBonds
    };

    this._isProcessing = true;
    this._hardwareProfileSubscription = this._hardwareService
      .postHardwareProfile(newProfile)
      .subscribe(
        (hardwareProfile: HardwareProfile) => {
          this._isProcessing = false;
          this._alertService.info(AlertType.INFO_CREATE_HARDWARE_PROFILE_SUCCESS);
          this._createdProfile.next(hardwareProfile);
        },
        () => {
          this._isProcessing = false;
          this._alertService.error(AlertType.ERROR_CREATE_HARDWARE_PROFILE_FAILED);
        }
      );
  }

  private createBondFormGroup(options?: Partial<FormBond>): FormGroup {
    return this._fb.group({
      socket: ['0', Validators.required],
      bandwidth: [NicBandwidth.GB1, Validators.required],
      driver: [NicDriver.VFIOPCI, Validators.required],
      device: [
        '',
        [
          Validators.required,
          this.bondedDeviceValidator.bind(this)
        ]
      ],
      kernel: [KernelMode.BYPASS, Validators.required],
      subnet: [options?.subnet ?? SubnetType.LAN, Validators.required],
      connectivity: [TunnelType.RAW, Validators.required]
    });
  }

  private bondsValidator(group: FormGroup): ValidationErrors {
    const bonds = group.get('bonds').value;
    const subnets: Array<string> = bonds.map(({ subnet }: FormBond) => subnet);
    const valid = bonds.length >= 2 && (subnets.includes(SubnetType.LAN) && subnets.includes(SubnetType.WAN));
    return valid ? undefined : { subnet: true };
  }

  private bondedDeviceValidator(control: FormControl): ValidationErrors {
    const { value } = control;
    const minVirtualLength = 4;
    const virtual = (value.startsWith('net_') || value.startsWith('eth_')) && value.length > minVirtualLength;
    const pci = PCI_PATTERN.test(value);
    return virtual || pci ? undefined : { pattern: true };
  }

  public onAddInterfaceBond() {
    this.interfaceBondsArray.push(this.createBondFormGroup());
  }

  public onRemoveInterfaceBond(index: number) {
    this.interfaceBondsArray.removeAt(index);
  }

  public getBandwidthLabel(bw: NicBandwidth): string {
    return getBandwidthLabel(bw);
  }

  public get bondControls(): AbstractControl[] {
    return (this._hardwareProfileForm.get('bonds') as FormArray).controls;
  }

  public get interfaceBondsArray(): FormArray {
    return this._hardwareProfileForm.controls.bonds as FormArray;
  }

  public get hardwareProfileForm(): FormGroup {
    return this._hardwareProfileForm;
  }

  public get isProcessing(): boolean {
    return this._isProcessing;
  }

  public get nicBandwidth(): typeof NicBandwidth {
    return NicBandwidth;
  }

  public get nicDriver(): typeof NicDriver {
    return NicDriver;
  }

  public get kernelMode(): typeof KernelMode {
    return KernelMode;
  }

  public get subnetType(): typeof SubnetType {
    return SubnetType;
  }

  public get connectivity(): typeof TunnelType {
    return TunnelType;
  }

  public get hideSave(): boolean {
    return this._hideSave;
  }

  public get createdProfile$(): Observable<HardwareProfile> {
    return this._createdProfile.asObservable();
  }
}
