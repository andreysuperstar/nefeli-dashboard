import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormArray, ReactiveFormsModule } from '@angular/forms';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { environment } from 'src/environments/environment';

import { HardwareProfilesFormComponent } from './hardware-profiles-form.component';

describe('HardwareProfilesFormComponent', () => {
  let component: HardwareProfilesFormComponent;
  let fixture: ComponentFixture<HardwareProfilesFormComponent>;
  let el: HTMLElement;
  let loader: HarnessLoader;
  let httpTestingController: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatOptionModule,
        MatSelectModule,
        MatInputModule
      ],
      declarations: [HardwareProfilesFormComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture = TestBed.createComponent(HardwareProfilesFormComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render form', () => {
    const matFormFields = el.querySelectorAll('mat-form-field');
    expect(matFormFields.length).toBe(16);
    expect(matFormFields[0].querySelector('input[formControlName="name"]')).toBeDefined();
    expect(matFormFields[1].querySelector('input[formControlName="description"]')).toBeDefined();
    expect(matFormFields[2].querySelector('input[formControlName="socket"]')).toBeDefined();
    expect(matFormFields[3].querySelector('mat-select[formControlName="bandwidth"]')).toBeDefined();
    expect(matFormFields[4].querySelector('mat-select[formControlName="driver"]')).toBeDefined();
    expect(matFormFields[5].querySelector('mat-select[formControlName="connectivity"]')).toBeDefined();
    expect(matFormFields[6].querySelector('mat-select[formControlName="kernel"]')).toBeDefined();
    expect(matFormFields[7].querySelector('mat-select[formControlName="subnet"]')).toBeDefined();
    expect(matFormFields[8].querySelector('input[formControlName="device"]')).toBeDefined();
    expect(matFormFields[9].querySelector('input[formControlName="socket"]')).toBeDefined();
    expect(matFormFields[10].querySelector('mat-select[formControlName="bandwidth"]')).toBeDefined();
    expect(matFormFields[11].querySelector('mat-select[formControlName="driver"]')).toBeDefined();
    expect(matFormFields[12].querySelector('mat-select[formControlName="connectivity"]')).toBeDefined();
    expect(matFormFields[13].querySelector('mat-select[formControlName="kernel"]')).toBeDefined();
    expect(matFormFields[14].querySelector('mat-select[formControlName="subnet"]')).toBeDefined();
    expect(matFormFields[15].querySelector('input[formControlName="device"]')).toBeDefined();
  });

  it('should submit new profile', async () => {
    const name = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=name]'
    }));
    const mockName = 'Test Profile';
    await name.setValue(mockName);

    const description = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=description]'
    }));
    const mockDescription = 'Test Description';
    await description.setValue(mockDescription);

    const sockets = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=socket]'
    }));
    const mockSocket1 = '123';
    await sockets[0].setValue(mockSocket1);
    const mockSocket2 = '321';
    await sockets[1].setValue(mockSocket2);

    const devices = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=device]'
    }));
    const mockDevice1 = '0000:0a:00.0';
    await devices[0].setValue(mockDevice1);
    const mockDevice2 = '0000:0e:00.0';
    await devices[1].setValue(mockDevice2);

    const form = fixture.debugElement.query(By.css('#hardware-profile-form'));
    expect(form.triggerEventHandler('submit', {})).toBeUndefined();

    const reqBody = httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`).request.body;
    expect(reqBody.name).toBe(mockName);
    expect(reqBody.description).toBe(mockDescription);
    const bonds = reqBody.bonds;
    expect(bonds.length).toBe(2);
    const devices1: string[] = bonds[0].bond.devices;
    const devices2: string[] = bonds[1].bond.devices;
    expect(devices1.length).toBe(1);
    expect(devices1[0]).toBe(mockDevice1);
    expect(devices2.length).toBe(1);
    expect(devices2[0]).toBe(mockDevice2);
    const sockets1 = bonds[0].bond.socket;
    const sockets2 = bonds[1].bond.socket;
    expect(sockets1).toBe(mockSocket1);
    expect(sockets2).toBe(mockSocket2);
  });

  it('should validate bonds', async () => {
    (await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=name]'
    }))).setValue('name');

    (await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=description]'
    }))).setValue('description');

    const sockets = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=socket]'
    }));
    await sockets[0].setValue('123');
    await sockets[1].setValue('321');

    const devices = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=device]'
    }));
    await devices[0].setValue('0000:0a:00.0');
    await devices[1].setValue('0000:0e:00.0');

    const subnet: MatSelectHarness[] = await loader.getAllHarnesses<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formcontrolname=subnet]'
    }));
    expect(subnet.length).toBe(2);
    await subnet[0].open()

    // verify form is valid
    expect(component.hardwareProfileForm.valid).toBe(true);

    // make form invalid by changing bond types
    await subnet[0].clickOptions({
      text: 'HA'
    });
    await subnet[1].clickOptions({
      text: 'HA'
    });

    component.hardwareProfileForm.get('bonds').updateValueAndValidity();

    // verify form is now invalid
    expect(component.hardwareProfileForm.valid).toBe(false);
  });

  it('should validate bonded device', async () => {
    const devices = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=device]'
    }));

    // by default, fields are invalid (required)
    const device0 = component.hardwareProfileForm.get('bonds.0.device');
    const device1 = component.hardwareProfileForm.get('bonds.1.device');
    expect(device0.valid).toBe(false);
    expect(device1.valid).toBe(false);
    expect(device0.errors).toEqual({ required: true });
    expect(device1.errors).toEqual({ required: true });

    // verify fields are invalid due to incorrect format
    await devices[0].setValue('something-wrong');
    await devices[1].setValue('something-invalid');
    expect(device0.valid).toBe(false);
    expect(device1.valid).toBe(false);
    expect(device0.errors).toEqual({ pattern: true });
    expect(device1.errors).toEqual({ pattern: true });

    // make fields valid
    await devices[0].setValue('0000:0a:00.0');
    await devices[1].setValue('0000:0e:00.0');
    expect(device0.valid).toBe(true);
    expect(device1.valid).toBe(true);
    expect(device0.errors).toBeNull();
    expect(device1.errors).toBeNull();
  });

  it('should add and remove interface bonds', async () => {
    expect((component.hardwareProfileForm.get('bonds') as FormArray).length).toBe(2);

    const addInfButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Add Interface Bond'
    }));
    await addInfButton.click();

    expect((component.hardwareProfileForm.get('bonds') as FormArray).length).toBe(3);

    const rmInfButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: "[title='Remove bond']"
    }));
    await rmInfButton.click();

    expect((component.hardwareProfileForm.get('bonds') as FormArray).length).toBe(2);
  });
});
