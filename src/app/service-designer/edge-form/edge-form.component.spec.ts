import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { EdgeFormComponent, EdgeFormData } from './edge-form.component';
import { SidebarData, SidebarService } from 'src/app/shared/sidebar.service';
import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { EdgeDirection } from 'src/app/pipeline/pipeline.service';

describe('EdgeFormComponent', () => {
  let component: EdgeFormComponent;
  let fixture: ComponentFixture<EdgeFormComponent>;
  let el: HTMLElement;
  let loader: HarnessLoader;

  const mockFormData: EdgeFormData = {
    direction: 2,
    edge: {
      a: { node: 'node1', interface: 'dp0' },
      b: { node: 'port', interface: '' },
      filterAb: {},
      filterBa: {},
      local: {
        id: 'a493a',
        status: 2,
        coordinates: {
          a: [100, 100],
          b: [200, 200]
        },
      },
    },
    filterAb: "AB-filter",
    filterBa: "BA-filter"
  };
  const sidebarData = new SidebarData();
  sidebarData.data = mockFormData;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatFormFieldModule,
        MatInputModule
      ],
      declarations: [
        EdgeFormComponent
      ],
      providers: [
        SidebarService,
        DecimalPipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: SidebarData,
          useValue: sidebarData
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdgeFormComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    const title = el.querySelector('h5');
    expect(title.textContent).toBe('Traffic Direction');
  });

  it('should have traffic direction buttons', async () => {
    const directionButtons: MatButtonHarness[] = await loader.getAllHarnesses<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button.direction'
    }));
    expect(directionButtons.length).toBe(3);
    const biDirectionButton = await directionButtons[0].host();
    expect(biDirectionButton.hasClass('active')).toBeTruthy();
  });

  it('should have traffic filter inputs', async () => {
    const atoBFilter: MatInputHarness = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=filterAtoB]'
    }));
    expect(await atoBFilter.getValue()).toBe(mockFormData.filterAb);

    const bToAFilter: MatInputHarness = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=filterBtoA]'
    }));
    expect(await bToAFilter.getValue()).toBe(mockFormData.filterBa);
  });

  it('should have delete button', async () => {
    spyOn(component['_sidebarService'], 'close');
    const deleteButton: MatButtonHarness = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Delete Edge'
    }));
    await deleteButton.click();

    expect(component['_sidebarService'].close).toHaveBeenCalledWith({ shouldDelete: true });
  });

  it('should save edge settings', async () => {
    spyOn(component['_sidebarService'], 'close');

    //change filter
    const atoBFilter: MatInputHarness = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=filterAtoB]'
    }));
    await atoBFilter.setValue('test');

    // change direction
    const directionButtons: MatButtonHarness[] = await loader.getAllHarnesses<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button.direction'
    }));
    await directionButtons[1].click();

    const saveButton: MatButtonHarness = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Update'
    }));
    await saveButton.click();

    expect(component['_sidebarService'].close).toHaveBeenCalledWith({
      filterAb: 'test',
      filterBa: mockFormData.filterBa,
      direction: EdgeDirection.Downward
    });
  });
});
