import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { PipelineEdge } from '../../pipeline/pipeline.model';
import { SidebarData, SidebarService } from 'src/app/shared/sidebar.service';
import { EdgeSidebarSettings } from 'src/app/pipeline/pipeline.component';
import { EdgeDirection } from 'src/app/pipeline/pipeline.service';

export interface EdgeFormData {
  edge: PipelineEdge;
  filterAb: string;
  filterBa: string;
  direction: EdgeDirection;
}

@Component({
  selector: 'nef-edge-form',
  templateUrl: './edge-form.component.html',
  styleUrls: ['./edge-form.component.less']
})
export class EdgeFormComponent implements OnInit {
  private _edge: PipelineEdge;
  private _data: EdgeFormData;
  private _form: FormGroup;
  private _trafficDirection: EdgeDirection;

  constructor(
    private _fb: FormBuilder,
    private _sidebarService: SidebarService,
    @Inject(SidebarData) sidebarData: SidebarData
  ) {
    this._data = sidebarData.data as EdgeFormData;
    this._edge = this._data?.edge;
  }

  public ngOnInit(): void {
    this.initForm();
    if (this._data) {
      const { filterAb, filterBa, direction } = this._data;
      this._form.patchValue({
        filterAtoB: filterAb,
        filterBtoA: filterBa
      });
      this._trafficDirection = direction;
    }
  }

  private initForm() {
    this._form = this._fb.group({
      filterAtoB: '',
      filterBtoA: ''
    });
  }

  public submitForm() {
    const { filterAtoB, filterBtoA } = this._form.value;
    const data: EdgeSidebarSettings = {
      filterAb: filterAtoB,
      filterBa: filterBtoA,
      direction: this._trafficDirection
    };
    this._sidebarService.close(data);
  }

  public onDeleteEdge() {
    const data: EdgeSidebarSettings = {
      shouldDelete: true
    };
    this._sidebarService.close(data);
  }

  public onChangeEdgeDirection(direction: EdgeDirection): void {
    this._trafficDirection = direction;
  }

  public get EdgeDirection(): typeof EdgeDirection {
    return EdgeDirection;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get activeEdge(): PipelineEdge {
    return this._edge;
  }

  public get trafficDirection(): EdgeDirection {
    return this._trafficDirection;
  }

}
