import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { HttpErrorResponse, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { RouterModule, ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { environment } from '../../../environments/environment';
import { of } from 'rxjs';

import { ServiceConfig } from 'rest_client/pangolin/model/serviceConfig';
import { Pipeline, PipelineNode } from '../../pipeline/pipeline.model';

import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';

import { Tenant } from '../../tenant/tenant.service';
import { LicensePool } from 'rest_client/pangolin/model/licensePool';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { User } from 'rest_client/heimdallr/model/user';
import { BASE_PATH as HEIMDALLR_BASE_PATH } from 'rest_client/heimdallr/variables';

import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { PoolDialogComponent } from 'src/app/shared/pool-dialog/pool-dialog.component';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { ServicePortMenuComponent } from 'src/app/pipeline/service-port-menu/service-port-menu.component';
import { NetworkFunctionMenuComponent } from 'src/app/pipeline/network-function-menu.component';
import { NetworkFunctionComponent } from 'src/app/pipeline/network-function.component';
import { PipelineComponent } from 'src/app/pipeline/pipeline.component';
import { LegendComponent } from 'src/app/shared/legend/legend.component';
import { ToggleComponent } from 'src/app/shared/toggle/toggle.component';

import { UserService } from 'src/app/users/user.service';

import { DragEventData, Mode, ServiceDesignerComponent } from './service-designer.component';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment, UnitTestElement } from '@angular/cdk/testing/testbed';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { Service } from 'rest_client/pangolin/model/service';
import { MatButtonToggleHarness } from '@angular/material/button-toggle/testing';
import { MatMenuItemHarness } from '@angular/material/menu/testing';
import { AlertType } from 'src/app/shared/alert.service';
import pool from 'mock/license_pool';
import nfs from 'mock/nf_catalog';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { SelectSiteDialogComponent } from '../select-site-dialog/select-site-dialog.component';
import sites from 'mock/sites';
import { MatTableModule } from '@angular/material/table';
import { MatRowHarness } from '@angular/material/table/testing';
import { NFConfiguration, PipelineService } from 'src/app/pipeline/pipeline.service';
import nfc from 'mock/nf_config';
import manifest from 'mock/nf_manifest';
import { cloneDeep } from 'lodash-es';
import { TemplateInputComponent } from 'src/app/shared/template-input/template-input.component';
import { CodemirrorComponent } from 'src/app/shared/codemirror/codemirror.component';
import template from 'mock/template';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import serviceChain from 'mock/service_chain';
import { TemplateService } from '../../templates/template.service';
import servers from 'mock/servers';
import { CREATE_TEMPLATE_VARIABLE } from 'src/app/utils/codemirror';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRadioModule } from '@angular/material/radio';
import { Site } from 'rest_client/pangolin/model/site';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { createNF } from 'src/app/pipeline/pipeline.component.spec';
import profiles from 'mock/hardware_profiles';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import tenants from 'mock/tenants';

describe('ServiceDesignerComponent', () => {
  let component: ServiceDesignerComponent;
  let fixture: ComponentFixture<ServiceDesignerComponent>;
  let document: Document;
  let httpTestingController: HttpTestingController;
  let router: Router;
  let route: ActivatedRoute;
  let userService: UserService;
  let loader: HarnessLoader;
  let rootLoader: HarnessLoader;
  let pipelineService: PipelineService;
  let templateService: TemplateService;

  let navigateSpy: jasmine.Spy;

  const mockUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  const mockTenant: Tenant = {
    identifier: '-1',
    name: 'Tenant A'
  };

  const mockPipeline: Pipeline = {
    identifier: '1',
    name: 'customer0_pid4'
  };

  const mockService: ServiceConfig = {
    name: 'customer0_pid4',
    description: 'Nefeli Service'
  };

  const mockLicensePools: LicensePool[] = [
    {
      identifier: '25310885-c544-4646-8cb8-4d8b46f9b223',
      name: 'Pool #1'
    },
    {
      identifier: '4d43a4ff-73cc-49fe-9c9a-0eb823230121',
      name: 'Pool #2'
    }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        RouterModule,
        OverlayModule,
        MatSnackBarModule,
        MatListModule,
        MatDividerModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatExpansionModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        MatSelectModule,
        MatStepperModule,
        FormsModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        MatMenuModule,
        MatIconModule,
        MatTooltipModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatTableModule,
        MatCheckboxModule,
        MatRadioModule
      ],
      declarations: [
        ServiceDesignerComponent,
        AvatarComponent,
        ServicePortMenuComponent,
        NetworkFunctionMenuComponent,
        NetworkFunctionComponent,
        PipelineComponent,
        LegendComponent,
        ToggleComponent,
        PoolDialogComponent,
        ConfirmationDialogComponent,
        SelectSiteDialogComponent,
        TemplateInputComponent,
        CodemirrorComponent
      ],
      providers: [
        PipelineService,
        UserService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HEIMDALLR_BASE_PATH,
          useValue: '/auth'
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              id: mockTenant.identifier
            }),
            snapshot: {
              queryParamMap: {
                get(name: string): string | null {
                  return null;
                }
              },
              routeConfig: {
                path: ''
              },
              params: {
                id: 12345
              },
              data: {}
            }
          }
        },
      ]
    })
      .compileComponents();

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    router = TestBed.inject(Router);
    route = TestBed.inject(ActivatedRoute);
    userService = TestBed.inject(UserService);
    pipelineService = TestBed.inject(PipelineService);
    templateService = TestBed.inject(TemplateService);

    navigateSpy = spyOn(router, 'navigate');
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceDesignerComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    rootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
    component = fixture.componentInstance;
    component['_licensePools'] = mockLicensePools;
    fixture.detectChanges();
    userService.setUser(mockUser);

    const req: TestRequest = httpTestingController.expectOne(`${environment.restServer}/auth/tenants/${mockTenant.identifier}`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    req.flush(mockTenant);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render control elements', async () => {
    await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Name'
    }));

    await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Description'
    }));

    await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Delete'
    }));

    expect(fixture.debugElement.query(By.css('.cancel-button'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('.mat-button-toggle-button'))).not.toBeNull();
  });

  it('should redirect back to tenant info page on cancel', () => {
    const cancelButton = fixture.debugElement.query(By.css('.cancel-button'));
    cancelButton.triggerEventHandler('click', {});
    expect(navigateSpy).toHaveBeenCalledWith(['/tenants', mockTenant.identifier, 'service', 'overview']);
  })

  it('should open license pool dropdown', () => {
    const licenseSelectButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('.license button')
    ).nativeElement;

    licenseSelectButtonEl.click();
    fixture.detectChanges();

    const licensePoolDropdownEl: HTMLUnknownElement = document.querySelector(
      'mat-card.license-pool-dropdown'
    );

    expect(licensePoolDropdownEl).not.toBeNull('license pool dropdown is open');
  });

  it('should render license pool dropdown list items', () => {
    const licenseSelectButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('.license button')
    ).nativeElement;

    licenseSelectButtonEl.click();
    fixture.detectChanges();

    const licensePoolItemEls: NodeListOf<HTMLButtonElement> = document.querySelectorAll(
      '.license-pool-dropdown mat-action-list button[mat-list-item]'
    );

    expect(licensePoolItemEls.length).toBe(
      mockLicensePools.length + 1,
      `render ${mockLicensePools.length + 1} license pool list items`
    );
    expect(licensePoolItemEls[1].textContent).toBe(
      mockLicensePools[0].name,
      `valid 2nd item id`
    );
    expect(licensePoolItemEls[2].textContent).toBe(
      mockLicensePools[1].name,
      `valid 1st item id`
    );
  });

  it('should select license pool and close dropdown', () => {
    let licenseSelectButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('.license button')
    ).nativeElement;

    licenseSelectButtonEl.click();
    fixture.detectChanges();

    const licensePoolItemEl: HTMLButtonElement = document.querySelectorAll<HTMLButtonElement>(
      '.license-pool-dropdown mat-action-list button[mat-list-item]'
    )[1];

    spyOn(component, 'onSelectLicensePool').and.callThrough();

    licensePoolItemEl.click();

    expect(component.onSelectLicensePool).toHaveBeenCalledWith(mockLicensePools[0]);

    fixture.detectChanges();

    licenseSelectButtonEl = fixture.debugElement.query(
      By.css('.license button')
    ).nativeElement;

    expect(licenseSelectButtonEl.textContent).toBe(
      mockLicensePools[0].name,
      'valid selected license pool id'
    );

    const backdropEl: HTMLDivElement = document.querySelector('.cdk-overlay-backdrop');

    backdropEl.click();
    fixture.detectChanges();

    const licensePoolDropdownEl: HTMLUnknownElement = document.querySelector(
      'mat-card.license-pool-dropdown'
    );

    expect(licensePoolDropdownEl).toBeNull('license pool dropdown is closed');
  });

  it('should display current license pool', () => {
    spyOn(route.snapshot.queryParamMap, 'get').and.callFake((key: string) => {
      return mockPipeline[key];
    });
    const licensePool: LicensePool = {
      identifier: 'my-pool',
      name: 'My Pool'
    };

    route.snapshot.data.isEditMode = true;
    component.ngOnInit();

    httpTestingController
      .expectOne(`${environment.restServer}/auth/tenants/${mockTenant.identifier}`)
      .flush(mockTenant);

    const serviceRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${encodeURIComponent(String(mockTenant.identifier))}/services/${encodeURIComponent(String(mockPipeline.identifier))}`
    );

    let serviceResponse: Service = {
      config: mockService
    }

    serviceRequest.flush(serviceResponse);

    serviceResponse = {}
    httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${encodeURIComponent(String(mockTenant.identifier))}/services/${encodeURIComponent(String(mockPipeline.identifier))}?status=true`
    ).flush(serviceResponse);

    httpTestingController
      .expectOne(
        `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipeline.identifier}/license_pool`
      )
      .flush(licensePool.identifier);

    httpTestingController
      .expectOne(`${environment.restServer}/v1/license_pools/${licensePool.identifier}`)
      .flush(licensePool);

    fixture.detectChanges();

    const licenseSelectButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('.license button')
    ).nativeElement;

    expect(licenseSelectButtonEl.textContent).toBe(licensePool.name);
  });

  it('should publish service', async () => {
    spyOn(component['_alert'], 'error');

    const descInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Description'
    }));
    await descInput.setValue('Hello World');

    const publishButton = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness);
    await publishButton.check();

    // display error because no name provided
    expect(component['_alert'].error).toHaveBeenCalledWith(AlertType.ERROR_SERVICE_NO_NAME);

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Name'
    }));
    await nameInput.setValue('My Service');
    await publishButton.uncheck();
    await publishButton.check();

    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`).flush(pool);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`).flush(nfs);

    const publishReq = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services?force=false&draft=false`);
    expect(publishReq.request.method).toBe('POST');
    expect(publishReq.request.body).toEqual({ name: 'My Service', description: 'Hello World' });
    publishReq.flush({});
  });

  it('should publish service with ucpe site', fakeAsync(async () => {
    component['_activePipeline'] = {
      identifier: '123'
    };
    component['_editMode'] = true;
    fixture.detectChanges();

    httpTestingController.expectOne(`${environment.restServer}/v1/sites`).flush(sites);

    const resp: Service = cloneDeep(serviceChain);
    resp.config.nodes.portX.site = 'andrew_site';
    resp.config.nodes.portX.port.tunnel = undefined;
    resp.config.nodes.portY.site = 'andrew_site';
    resp.config.nodes.portY.port.tunnel = undefined;
    resp.config.nodes.vsrxA.site = 'andrew_site';
    const getServiceReqs = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/tenants/-1/services/123`
    });
    getServiceReqs[0].flush(resp);

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services/123/nodes/vsrxA/manifest`).flush(manifest);
    tick();
    getServiceReqs[1].flush(resp);
    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services/123`).flush(resp);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services/123/nodes/vsrxA/manifest`).flush(manifest);

    const publishButton = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness);
    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Name'
    }));
    await nameInput.setValue('My Service');
    await publishButton.check();

    httpTestingController.expectOne(`${environment.restServer}/v1/sites/andrew_site/servers`).flush(servers);
    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`).flush(pool);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`).flush(nfs);

    const hwProfile: HardwareProfile = cloneDeep(profiles.hardwareProfiles[0]);
    hwProfile.bonds[0].identifier = 'portX';
    hwProfile.bonds[1].identifier = 'portY';
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles/1`).flush(hwProfile);

    // TODO (eric): verify post request on publish
    flush();
  }));

  it('should fail publish service due to lack of license', async () => {
    const nfName = 'nf-name';
    const publishButton = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness);
    const licensePool = cloneDeep(pool);
    component.name = 'My Service';

    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`).flush(licensePool);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`).flush(nfs);

    const config: NFConfiguration = {
      nf: {
        catalogId: 'nf-catalog-id',
        local: {
          name: nfName
        }
      },
      config: nfc,
      manifest: manifest
    };

    pipelineService.setNFConfiguration('nodeid', config);

    // fail due to no license pool selected
    await publishButton.check();
    let dialog = await rootLoader.getHarness<MatDialogHarness>(MatDialogHarness);
    let host = <UnitTestElement> await dialog.host();
    expect(host.element.querySelector('mat-dialog-content > p').textContent)
      .toBe(`License pool is not selected. The network function ${nfName} requires a license.`)
    await dialog.close();

    // fail due to no required license in the pool
    component.onSelectLicensePool(licensePool.licensePools[0]);
    await publishButton.uncheck();
    await publishButton.check();
    dialog = await rootLoader.getHarness<MatDialogHarness>(MatDialogHarness);
    host = <UnitTestElement> await dialog.host();
    expect(host.element.querySelector('mat-dialog-content > p').textContent)
      .toBe(`The selected license pool does not have an available license\n        for the network function ${nfName}.`);
    await dialog.close();

    // succeed if required license in the pool
    licensePool.licensePools[2].licenses['Name-XY-10-Z'].config.nfId = config.nf.catalogId;
    component.onSelectLicensePool(licensePool.licensePools[2]);
    await publishButton.uncheck();
    await publishButton.check();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services?pool-id=${licensePool.licensePools[2].identifier}&force=false&draft=false`);
  });

  it('should fail publish service with an error message', async () => {
    spyOn(component['_alert'], 'error');

    const descInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Description'
    }));
    await descInput.setValue('Hello World');

    const publishButton = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness);
    await publishButton.check();

    // display error because no name provided
    expect(component['_alert'].error).toHaveBeenCalledWith(AlertType.ERROR_SERVICE_NO_NAME);

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Name'
    }));
    await nameInput.setValue('My Service');
    await publishButton.uncheck();
    await publishButton.check();

    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`).flush(pool);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`).flush(nfs);

    const publishReq = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services?force=false&draft=false`);
    expect(publishReq.request.method).toBe('POST');
    expect(publishReq.request.body).toEqual({ name: 'My Service', description: 'Hello World' });

    const error = {
      message: 'Mocked error message',
      errorCode: '-1'
    };
    const errResponse: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };
    publishReq.flush(error, errResponse);

    expect(component['_alert'].error).toHaveBeenCalledWith(AlertType.ERROR_SAVING_CHANGES, error.message);
  });

  it('should force publish service', async () => {
    const buttons = await loader.getAllHarnesses<MatButtonToggleHarness>(MatButtonToggleHarness);
    const [publishButton, dropButton] = buttons;
    component.name = 'My Service';

    await dropButton.uncheck();
    await dropButton.check();
    fixture.detectChanges();

    const forcePublishButton = await rootLoader.getHarness<MatMenuItemHarness>(MatMenuItemHarness.with({
      text: 'Force publish'
    }));

    await forcePublishButton.click();
    await publishButton.check();

    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`).flush(pool);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`).flush(nfs);

    const publishReq = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services?force=true&draft=false`);
    expect(publishReq.request.method).toBe('POST');
    expect(publishReq.request.body).toEqual({ name: 'My Service', description: undefined });
    publishReq.flush({});
  });

  it('should save draft', async () => {
    const buttons = await loader.getAllHarnesses<MatButtonToggleHarness>(MatButtonToggleHarness);
    const [publishButton, dropButton] = buttons;
    component.name = 'My Service';

    await dropButton.uncheck();
    await dropButton.check();
    fixture.detectChanges();

    const saveDraftBbutton = await rootLoader.getHarness<MatMenuItemHarness>(MatMenuItemHarness.with({
      text: 'Save draft'
    }));

    await saveDraftBbutton.click();
    await publishButton.check();

    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`).flush(pool);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`).flush(nfs);

    const publishReq = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services?force=false&draft=true`);
    expect(publishReq.request.method).toBe('POST');
    expect(publishReq.request.body).toEqual({ name: 'My Service', description: undefined });
    publishReq.flush({});
  });

  it('should delete service', async () => {
    component['_activePipeline'] = {
      identifier: '123'
    };
    component['_editMode'] = true;
    fixture.detectChanges();

    const deleteButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Delete'
    }));

    const disabled = await deleteButton.isDisabled();
    await deleteButton.click();

    const confirmationDialog = await rootLoader.getHarness<MatDialogHarness>(MatDialogHarness);

    const host = <UnitTestElement> await confirmationDialog.host();
    expect(host.element.querySelector('mat-dialog-content').textContent).toBe(
      'This will immediately stop and delete the service undefined.Are you sure you want to continue?');

    const confirmDeleteBbutton = await rootLoader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Delete Service'
    }));
    await confirmDeleteBbutton.click();

    const deleteRequest = httpTestingController.match((req: HttpRequest<any>): boolean => {
      return req.url === `${environment.restServer}/v1/tenants/-1/services/123`
        && req.method === 'DELETE';
    });
    expect(deleteRequest).toBeDefined();
    deleteRequest[0].flush({});
  });

  it('should be able to add a site', async () => {
    expect(component.newSiteChosen).toBe(false);
    const addSiteButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'add_circle_outlineAdd Site'
    }));
    await addSiteButton.click();

    httpTestingController.expectOne(`${environment.restServer}/v1/sites`).flush(sites);
    await rootLoader.getHarness<MatDialogHarness>(MatDialogHarness);

    const rows = await rootLoader.getAllHarnesses<MatRowHarness>(MatRowHarness);
    expect(await rows[0].getCellTextByColumnName()).toEqual({ name: 'Eric', type: 'CLUSTER' });
    expect(await rows[1].getCellTextByColumnName()).toEqual({ name: 'Andrew', type: 'UCPE' });
    expect(await rows[2].getCellTextByColumnName()).toEqual({ name: 'Anton', type: 'CLUSTER' });
    expect(await rows[3].getCellTextByColumnName()).toEqual({ name: 'Inactive', type: 'CLUSTER' });

    const host = <UnitTestElement> await rows[1].host();
    await host.click();
    expect(component.newSiteChosen).toBe(true);
  });

  it('should be able to drag & drop NF', () => {
    expect(component.nfs.size).toBe(0);

    const dragData: DragEventData = {
      id: '123',
      type: 'nf',
      name: 'Router',
      nf: {
        ...nfs.descriptors[0],
        local: {
          catalogId: 'catalog-id'
        }
      },
      vendor: 'Arista',
      event: {
        dataTransfer: {
          setData: () => { }
        } as any,
        target: {
          id: '321'
        } as any
      } as DragEvent,
      coordinates: { x: 0, y: 0 }
    };

    component.onDragStart(dragData);
    component.onDrop({ x: 0, y: 0 });

    expect(component.nfs.size).toBe(1);
    const newNf: PipelineNode = component.nfs.values().next().value;
    expect(newNf.nf.catalogId).toBe('catalog-id');
    expect(newNf.local.name).toBe('Router');
    expect(newNf.nf.local.name).toBe('Router');
  });

  it('should render template mode', () => {
    route.snapshot.data.isTemplateMode = true;
    component.ngOnInit();
    fixture.detectChanges();

    const el = fixture.debugElement.nativeElement;
    expect(el.querySelectorAll('.service-name > mat-form-field input').length).toBe(2);
  });

  it('should save new template', async () => {
    route.snapshot.data.isTemplateMode = true;
    component.ngOnInit();
    fixture.detectChanges();

    const buttons = await loader.getAllHarnesses<MatButtonToggleHarness>(MatButtonToggleHarness);
    const [saveButton] = buttons;

    const name = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Name'
    }));
    name.setValue('full mock name');

    const description = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Description'
    }));
    description.setValue('full mock description');

    await saveButton.check();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/service_templates`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.description).toBe('full mock description');
    expect(req.request.body.name).toBe('full mock name');

    req.flush({});

    expect(navigateSpy).toHaveBeenCalledWith(['/templates']);
  });

  it('should save template with ucpe site', fakeAsync(async () => {
    route.snapshot.data.isTemplateMode = true;
    component.ngOnInit();
    fixture.detectChanges();

    const buttons = await loader.getAllHarnesses<MatButtonToggleHarness>(MatButtonToggleHarness);
    const [saveButton] = buttons;

    const name = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Name'
    }));
    name.setValue('full mock name');

    const adddSiteButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'add_circle_outlineAdd Site'
    }));
    await adddSiteButton.click();
    flush();

    await rootLoader.getHarness<MatDialogHarness>(MatDialogHarness);

    const siteId = `{{siteID "${CREATE_TEMPLATE_VARIABLE('SiteName')}"}}`;
    const mockSite: Site = {
      config: {
        identifier: siteId,
        name: siteId,
        siteType: SiteConfigSiteType.UCPE
      }
    };
    component['_dialog'].openDialogs[0].close([mockSite]);
    fixture.detectChanges();
    tick();
    fixture.detectChanges();
    flush();

    component['_pipelineComponent'].createNewSite(mockSite.config);
    const nfs = new Map();
    const nf = createNF(component['_pipelineComponent'], 'NF1', 100, 100, siteId);
    nfs.set('NF1', nf);
    component['_pipelineComponent'].nfs = nfs;

    await saveButton.check();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/service_templates`);
    const body = req.request.body;
    expect(req.request.method).toBe('POST');
    expect(body.name).toBe('full mock name');
    expect(body.nfConfigs['NF1']).toBe('{"configTemplate":""}');
    expect(body.config.name).toBe('{{.serviceName}}');
    expect(body.config.nodes['NF1'].siteType).toBe(SiteConfigSiteType.UCPE);
    expect(body.config.nodes['NF1'].site).toBe(siteId);
    req.flush({});

    flush();
  }));

  it('should edit a service template', async () => {
    route.snapshot.params.templateId = template.identifier;

    route.snapshot.data = {
      isTemplateMode: true,
      isEditMode: true
    };

    component.ngOnInit();
    fixture.detectChanges();

    httpTestingController
      .expectOne({
        method: 'GET',
        url: `${environment.restServer}/v1/service_templates/${template.identifier}`
      })
      .flush(template);

    expect(component.name).toBe(template.name, 'valid template name');
    expect(component.description).toBe(template.description, 'valid template description');

    // should not display the delete button in edit template mode
    const deleteButton: MatButtonHarness[] = await loader.getAllHarnesses<MatButtonHarness>(MatButtonHarness.with({
      text: 'Delete'
    }));
    expect(deleteButton.length).toEqual(0);

    const saveButton = await loader.getHarness(MatButtonToggleHarness);

    await saveButton.check();

    const saveTemplateRequest = httpTestingController.expectOne({
      method: 'PUT',
      url: `${environment.restServer}/v1/service_templates/${template.identifier}`
    });

    saveTemplateRequest.flush(template);

    expect(navigateSpy).toHaveBeenCalledWith(['/templates']);
  });

  it('should instantiate service from template', async () => {
    spyOn(component['_alert'], 'error');

    route.snapshot.params.templateId = template.identifier;
    route.snapshot.data.isTemplateMode = true;

    component.ngOnInit();
    fixture.detectChanges();

    httpTestingController.expectOne(`${environment.restServer}/v1/service_templates/1`).flush(template);

    fixture.detectChanges();

    const [saveButton] = await loader.getAllHarnesses<MatButtonToggleHarness>(MatButtonToggleHarness);
    const formFields = await loader.getAllHarnesses<MatFormFieldHarness>(MatFormFieldHarness);
    const varInputs = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness);
    expect(formFields.length).toBe(9);
    expect(varInputs.length).toBe(9);

    // Name
    component.name = 'My Service from Template';

    // formFields[0] is 'Name'
    // formFields[1] is 'ServiceName'

    // SinkAddr
    const host3 = await formFields[2].host();
    const control3 = await formFields[2].getControl();
    expect(await formFields[2].getLabel()).toBe('SinkAddr');
    expect(await formFields[2].getTextHints()).toEqual(['config.nodes.portY.port.tunnel']);

    // SinkMAC
    const host4 = await formFields[3].host();
    const control4 = await formFields[3].getControl();
    expect(await formFields[3].getLabel()).toBe('SinkMAC');
    expect(await formFields[3].getTextHints()).toEqual(['config.nodes.portY.port.tunnel']);

    // SinkVNI
    const host5 = await formFields[4].host();
    const control5 = await formFields[4].getControl();
    expect(await formFields[4].getLabel()).toBe('SinkVNI');
    expect(await formFields[4].getTextHints()).toEqual(['config.nodes.portY.port.tunnel']);

    // SourceAddr
    const host6 = await formFields[5].host();
    const control6 = await formFields[5].getControl();
    expect(await formFields[5].getLabel()).toBe('SourceAddr');
    expect(await formFields[5].getTextHints()).toEqual(['config.nodes.portX.port.tunnel']);

    // SourceMAC
    const host7 = await formFields[6].host();
    const control7 = await formFields[6].getControl();
    expect(await formFields[6].getLabel()).toBe('SourceMAC');
    expect(await formFields[6].getTextHints()).toEqual(['config.nodes.portX.port.tunnel']);

    // SourceVNI
    const host8 = await formFields[7].host();
    const control8 = await formFields[7].getControl();
    expect(await formFields[7].getLabel()).toBe('SourceVNI');
    expect(await formFields[7].getTextHints()).toEqual(['config.nodes.portX.port.tunnel']);

    // TenantName
    const host9 = await formFields[8].host();
    const control9 = await formFields[8].getControl();
    expect(await formFields[8].getLabel()).toBe('TenantName');
    expect(await formFields[8].getTextHints()).toEqual(['config.tenantId']);

    await varInputs[0].setValue('val1');
    await varInputs[1].setValue('val2');
    await varInputs[2].setValue('val3');
    await varInputs[3].setValue('val4');
    await varInputs[4].setValue('val5');
    await varInputs[5].setValue('val6');
    await varInputs[6].setValue('val7');

    // verify error because not all variables supplied
    await saveButton.check();
    expect(component['_alert'].error).toHaveBeenCalledWith(AlertType.ERROR_TEMPLATE_INCOMPLETE);

    // supply remaining variable so publish will succeed
    await varInputs[7].setValue('val8');
    await varInputs[8].setValue('val9');
    await saveButton.uncheck();
    await saveButton.check();

    const publishReq = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/service_templates/1/instantiate`);
    const { body } = publishReq.request;
    expect(publishReq.request.method).toBe('POST');

    expect(body.variableValues.ServiceName).toBe('val2');
    expect(body.variableValues.SinkAddr).toBe('val3');
    expect(body.variableValues.SinkMAC).toBe('val4');
    expect(body.variableValues.SinkVNI).toBe('val5');
    expect(body.variableValues.SourceAddr).toBe('val6');
    expect(body.variableValues.SourceMAC).toBe('val7');
    expect(body.variableValues.SourceVNI).toBe('val8');
    expect(body.variableValues.TenantName).toBe('val9');

    const config: ServiceConfig = {
      identifier: '123'
    };
    publishReq.flush(config);
    expect(navigateSpy).toHaveBeenCalledWith(['/tenants', '-1', 'service', config.identifier]);
  });

  it('should show big add button after deleting all sites', () => {
    component['_newSiteChosen'] = true;
    component['_addSite'] = true;
    component['_pipelineComponent'] = jasmine.createSpyObj('PipelineComponent', { isEmpty: true });
    component.onDeleteSite();
    expect(component['_newSiteChosen']).toBeFalsy();
    expect(component['_addSite']).toBeFalsy();
  });

  it('should not show big add button when not all sites are deleted', () => {
    component['_newSiteChosen'] = true;
    component['_addSite'] = true;
    component['_pipelineComponent'] = jasmine.createSpyObj('PipelineComponent', { isEmpty: false });
    component.onDeleteSite();
    expect(component['_newSiteChosen']).toBeTruthy();
    expect(component['_addSite']).toBeTruthy();
  });

  it('should get ignored sites list when click on add a site', async () => {
    expect(component.newSiteChosen).toBe(false);
    const addSiteButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'add_circle_outlineAdd Site'
    }));
    await addSiteButton.click();

    httpTestingController.expectOne(`${environment.restServer}/v1/sites`).flush(sites);
    await rootLoader.getHarness<MatDialogHarness>(MatDialogHarness);

    const rows = await rootLoader.getAllHarnesses<MatRowHarness>(MatRowHarness);
    expect(await rows[0].getCellTextByColumnName()).toEqual({ name: 'Eric', type: 'CLUSTER' });
    expect(await rows[1].getCellTextByColumnName()).toEqual({ name: 'Andrew', type: 'UCPE' });
    expect(await rows[2].getCellTextByColumnName()).toEqual({ name: 'Anton', type: 'CLUSTER' });
    expect(await rows[3].getCellTextByColumnName()).toEqual({ name: 'Inactive', type: 'CLUSTER' });

    const host = <UnitTestElement> await rows[0].host();
    await host.click();
    fixture.detectChanges();

    spyOn(component['_pipelineComponent'], 'getSiteGroups');
    await addSiteButton.click();

    expect(component['_pipelineComponent'].getSiteGroups).toHaveBeenCalledTimes(1);
  });

  it('should clear template variables on destroy', async () => {
    const clearSpy = spyOn(templateService, 'clearTemplateVariables');
    component.ngOnDestroy();
    expect(clearSpy).toHaveBeenCalledTimes(1);
  });

  it('should be in create new service mode', () => {
    spyOn(component['_route'].snapshot.queryParamMap, 'get').and.callFake((key: string) => {
      return undefined;
    });

    component['_route'].snapshot.params = { id: 'tenant-id', templateId: 'blank' };
    component['_route'].snapshot.data = { isServices: true, isTemplateMode: true };
    component.ngOnInit();

    expect(component.hasDraft).toBe(false);
    expect(component.mode).toBe(Mode.DEFAULT);
    expect(component.isTemplateMode).toBe(false);
    expect(component.editMode).toBe(false);
  });

  it('should be in edit service mode', () => {
    spyOn(component['_route'].snapshot.queryParamMap, 'get').and.callFake((key: string) => {
      switch (key) {
        case 'identifier':
          return 'service-id';
        case 'name':
          return 'arista';
        default:
          return undefined;
      }
    });

    component['_route'].snapshot.params = { id: 'tenant-id' };
    component['_route'].snapshot.data = { isServices: true, isEditMode: true };
    component.ngOnInit();

    expect(component.hasDraft).toBe(false);
    expect(component.mode).toBe(Mode.DEFAULT);
    expect(component.isTemplateMode).toBe(false);
    expect(component.editMode).toBe(true);
  });

  it('should be in create new template mode', () => {
    spyOn(component['_route'].snapshot.queryParamMap, 'get').and.callFake((key: string) => {
      return undefined;
    });

    component['_route'].snapshot.params = { id: '-1' };
    component['_route'].snapshot.data = { isServices: true, isTemplateMode: true };
    component.ngOnInit();

    expect(component.hasDraft).toBe(false);
    expect(component.mode).toBe(Mode.TEMPLATE);
    expect(component.isTemplateMode).toBe(true);
    expect(component.editMode).toBe(false);
  });

  it('should be in edit template mode', () => {
    spyOn(component['_route'].snapshot.queryParamMap, 'get').and.callFake((key: string) => {
      return undefined;
    });

    component['_route'].snapshot.params = { id: '-1', templateId: 'template-id' };
    component['_route'].snapshot.data = { isServices: true, isTemplateMode: true, isEditMode: true };
    component.ngOnInit();

    expect(component.hasDraft).toBe(false);
    expect(component.mode).toBe(Mode.TEMPLATE);
    expect(component.isTemplateMode).toBe(true);
    expect(component.editMode).toBe(true);
  });

  it('should be in template instantiate mode', () => {
    spyOn(component['_route'].snapshot.queryParamMap, 'get').and.callFake((key: string) => {
      return undefined;
    });

    component['_route'].snapshot.params = { id: 'tenant-id', templateId: 'template-id' };
    component['_route'].snapshot.data = { isServices: true, isTemplateMode: true };
    component.ngOnInit();

    expect(component.hasDraft).toBe(false);
    expect(component.mode).toBe(Mode.TEMPLATE_INSTANTIATE);
    expect(component.isTemplateMode).toBe(true);
    expect(component.editMode).toBe(false);
  });
});
