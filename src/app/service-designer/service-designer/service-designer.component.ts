import { isEmpty } from 'lodash-es';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { Observable, Subscription, of, forkJoin, EMPTY, Subject } from 'rxjs';
import {
  switchMap,
  tap,
  filter,
  catchError,
  mapTo,
  mergeMap,
  pluck,
  concatMap,
  takeUntil,
  defaultIfEmpty
} from 'rxjs/operators';
import { ConnectedPosition } from '@angular/cdk/overlay';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { LicenseService, UILicensePool } from 'src/app/licenses/license.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/users/user.service';
import {
  ConfirmationDialogComponent,
  ConfirmationDialogData
} from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { ServiceConfig } from 'rest_client/pangolin/model/serviceConfig';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { PipelineService } from '../../pipeline/pipeline.service';
import { AlertService, AlertType } from '../../shared/alert.service';
import { TenantService, Tenant } from '../../tenant/tenant.service';
import { Pipeline, Coordinates, PipelineNode, createPipelineNode, PipelineGraph } from '../../pipeline/pipeline.model';
import { randomID, NetworkFunction, NFCatalog } from '../../pipeline/network-function.model';
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { PoolDialogComponent, POOL_DIALOG_CONFIG } from 'src/app/shared/pool-dialog/pool-dialog.component';
import {
  trigger,
  state as animationState,
  style,
  animate,
  transition,
} from '@angular/animations';
import { SelectSiteDialogComponent, SelectSiteDialogData } from '../select-site-dialog/select-site-dialog.component';
import { PipelineComponent } from 'src/app/pipeline/pipeline.component';
import { LicenseState } from 'rest_client/pangolin/model/licenseState';
import { LicenseMode } from 'rest_client/pangolin/model/licenseMode';
import { Component as NFDescriptorComponent } from 'rest_client/pangolin/model/component';
import { HttpErrorResponse } from '@angular/common/http';
import { PipelineError } from 'src/app/error-codes';
import { NetworkFunctionService } from 'src/app/pipeline/network-function.service';
import { Site } from 'rest_client/pangolin/model/site';
import { ServiceTemplatesService } from 'rest_client/pangolin/api/serviceTemplates.service';
import { ServiceTemplate } from 'rest_client/pangolin/model/serviceTemplate';
import { ServiceConfigTemplate } from 'rest_client/pangolin/model/serviceConfigTemplate';
import { ServiceConfigTemplateNode } from 'rest_client/pangolin/model/serviceConfigTemplateNode';
import {
  extractTemplateVariables,
  getTemplateVariableText,
  isTemplateVariable,
  TemplateService
} from 'src/app/templates/template.service';
import { CREATE_TEMPLATE_VARIABLE, DocChangeEvent } from 'src/app/utils/codemirror';
import { ServersService } from 'src/app/cluster/servers.service';
import { RESTServerNetworkInterface } from 'rest_client/pangolin/model/rESTServerNetworkInterface';
import { Server } from '../../cluster/servers.service';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { ServiceTemplateInstantiation } from 'rest_client/pangolin/model/serviceTemplateInstantiation';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';

export enum Mode {
  DEFAULT,
  TEMPLATE,
  TEMPLATE_INSTANTIATE
}

export enum PublishType {
  GRACEFUL = 'Publish',
  FORCE = 'Force publish',
  DRAFT = 'Save draft',
  TEMPLATE = 'Save template'
}

export interface DragEventData {
  id: string;
  type: string;
  name: string;
  nf?: NetworkFunction;
  vendor?: string;
  event: DragEvent;
  coordinates?: Coordinates;
}

const DIALOG_CONFIG: MatDialogConfig<ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

const TEMPLATE_VAR_SERVICE_NAME = 'serviceName';

@Component({
  selector: 'nef-service-designer',
  templateUrl: './service-designer.component.html',
  styleUrls: ['./service-designer.component.less'],
  animations: [
    trigger('addSite', [
      animationState('slideDownMenu', style({
        transform: 'translateY(70px)'
      })),
      animationState('shrinkButton', style({
        transform: 'translateX(-305px)',
        height: '50px',
        width: '275px'
      })),
      animationState('shrinkIcon', style({
        height: '25px',
        width: '25px',
        'font-size': '25px'
      })),
      transition('* => shrinkButton', [
        animate('500ms')
      ]),
      transition('* => slideDownMenu', [
        animate('500ms')
      ]),
      transition('* => shrinkIcon', [
        animate('100ms')
      ])
    ]),
  ]
})
export class ServiceDesignerComponent implements OnInit, OnDestroy {
  @ViewChild('pipeline') private _pipelineComponent: PipelineComponent;

  private _activeTenant: Tenant;
  private _activePipeline: Pipeline;
  private _subscription = new Subscription();
  private _dragged: DragEventData;
  private _name: string;
  private _description: string;
  private _selectedLicensePool: UILicensePool;
  private _licensePools: UILicensePool[];
  private _isLicensePoolDropdownOpen: boolean;
  private _serviceCurrentLicensePoolId: string;
  private _editMode: boolean;
  private _dialogSubscription: Subscription;
  private _publishType: PublishType = PublishType.GRACEFUL;
  private _nfCatalog: NFCatalog;
  private _initialSettings = {
    name: '',
    description: '',
    licensePool: undefined as UILicensePool
  };
  private _destroy$: Subject<boolean> = new Subject<boolean>();
  private _hasDraft = false;
  private _isPublishing = false;
  private _licensePoolDropdownPositions: ConnectedPosition[] = [
    {
      originX: 'start',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: 14
    }
  ];
  private _ports = new Map<string, PipelineNode>();
  private _nfs = new Map<string, PipelineNode>();
  private _addSite = false;
  private _newSiteChosen = false;
  private _canvasHeight = 600;
  private _mode = Mode.DEFAULT;
  private _template: ServiceTemplate;
  private _templateVarsMap: Map<string, Set<string>>;
  private _templateValuesMap: { [key: string]: string; } = {};
  private _templateGraph: PipelineGraph;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _licensingService: LicenseService,
    private _pipelineService: PipelineService,
    private _userService: UserService,
    private _dialog: MatDialog,
    private _alert: AlertService,
    private _tenantService: TenantService,
    private _servicesService: ServicesService,
    private _nfService: NetworkFunctionService,
    private _serviceTemplateService: ServiceTemplatesService,
    private _templateService: TemplateService,
    private _serversService: ServersService
  ) {
  }

  public ngOnInit(): void {
    const serviceId = this._route.snapshot.queryParamMap.get('identifier');
    const name = this._route.snapshot.queryParamMap.get('name');
    const templateId = this._route.snapshot.params.templateId;

    if (this._route.snapshot.data.isTemplateMode && templateId !== 'blank') {
      this._publishType = PublishType.TEMPLATE;
      this._mode = Mode.TEMPLATE;
    }

    this._editMode = Boolean(this._route.snapshot.data.isEditMode);

    if (!this._editMode && templateId && templateId !== 'blank') {
      this._mode = Mode.TEMPLATE_INSTANTIATE;
    }

    this._activePipeline = {
      identifier: serviceId,
      name,
    };

    if (this._editMode) {
      this._addSite = true;
      this._newSiteChosen = true;
    }

    if (this._publishType === PublishType.TEMPLATE) {
      if (this._mode === Mode.TEMPLATE_INSTANTIATE) {
        this.initTenantsAndServices();
        this.initTemplate(templateId);
      } else {
        if (this._editMode) {
          this.initTemplate(templateId);
        }

        this._subscription.add(this.initNFCatalog());
        this._activeTenant = {};
      }
    } else {
      this.initTenantsAndServices(serviceId);
    }

    this.getLicensePools();
  }

  public ngOnDestroy() {
    this._templateService.clearTemplateVariables();
  }

  private initTemplate(templateId: string) {
    this._serviceTemplateService.getServiceTemplate(templateId).subscribe({
      next: (template: ServiceTemplate) => {
        this._template = template;
        this._activePipeline = template.config as any;
        this._templateVarsMap = extractTemplateVariables(template);

        this._templateGraph = {
          edges: template.config.edges,
          nodes: template.config.nodes as any
        };

        if (this._editMode) {
          this._name = template.name;
          this._description = template.description;
          this._initialSettings.name = template.name;
          this._initialSettings.description = template.description;

          const templateVars: string[] = Array.from(this._templateVarsMap.keys());
          this._templateService.addTemplateVariable(...templateVars);
        }
      }
    });
  }

  private initTenantsAndServices(serviceId?: string) {
    let tenantAndService$: Observable<Tenant | ServiceConfig> = this._route.params.pipe(
      switchMap(params => this._tenantService.getTenant(params.id)),
      tap<Tenant>(tenant => {
        this._activeTenant = tenant;

        this._subscription.add(this.initNFCatalog(tenant.identifier));

        if (this._editMode && serviceId) {
          this._pipelineService.hasDraft(tenant, serviceId)
            .pipe(takeUntil(this._destroy$))
            .subscribe((hasDraft: boolean) => {
              this._hasDraft = hasDraft;

              this.setServiceLicensePool(hasDraft);
            });
        }
      })
    );

    if (this._editMode) {
      tenantAndService$ = tenantAndService$.pipe(
        switchMap<Tenant, Observable<ServiceConfig>>(
          tenant => this._servicesService
            .getService(tenant.identifier, serviceId)
            .pipe(
              pluck('config')
            )
        ),
        // tslint:disable-next-line: no-shadowed-variable
        tap<ServiceConfig>(({ name, description }) => {
          this._name = name;
          this._description = description;
          this._initialSettings.name = name;
          this._initialSettings.description = description;
        })
      );
    }

    const tenantAndServiceSubscription = tenantAndService$.subscribe(() => this.updateCanvasHeight());

    this._subscription.add(tenantAndServiceSubscription);
  }

  public onServiceNameInput(event: Event) {
    const inputEl = event.target as HTMLInputElement;
    if (!inputEl.value) {
      return;
    }

    this.name = inputEl.value;
  }

  public onServiceDescriptionInput(event: Event) {
    const textAreaEl = event.target as HTMLTextAreaElement;

    this._description = textAreaEl.value;
  }

  public onServiceNameDocChange(event: DocChangeEvent) {
    const text = event?.change?.text[0];

    if (text) {
      this.handleTemplateText(text);
    }

    this.name = event.doc.getValue();
  }

  public onServiceDescriptionDocChange(event: DocChangeEvent) {
    const text = event?.change?.text[0];

    if (text) {
      this.handleTemplateText(text);
    }

    this._description = event.doc.getValue();
  }

  public onDeleteService() {
    const data: ConfirmationDialogData = {
      description: `This will immediately stop and delete the service <strong>${this._name}</strong>.`,
      action: 'Delete Service'
    };

    const dialogRef = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        switchMap(
          (_: boolean) => this._pipelineService.deletePipeline(
            this._activeTenant.identifier,
            this._activePipeline.identifier
          )
        )
      )
      .subscribe(
        () => {
          this._alert.info(AlertType.INFO_DELETE_SERVICE_SUCCESS);
          this._router.navigate(['/tenants', this._activeTenant.identifier, 'service', 'overview']);
        },
        () => this._alert.error(AlertType.ERROR_DELETE_SERVICE)
      );
  }

  private getNfConfigs(nodes: { [key: string]: ServiceConfigTemplateNode; }): { [key: string]: string; } {
    if (!nodes) {
      return;
    }

    const nodeKeys = Object.keys(nodes);
    const config = {};

    nodeKeys.forEach((key: string) => {
      const nfConfig = this._pipelineService.getNFConfiguration(key)?.config;

      if (nodes[key].nf) {
        const configTemplate = nfConfig?.configTemplate;
        config[key] = JSON.stringify({
          configTemplate: configTemplate ?? ''
        });
      }
    });

    return config;
  }

  private saveTemplate() {
    const graph = (this._pipelineComponent?.getGraph(true) ?? {}) as ServiceConfigTemplate;

    const template: ServiceTemplate = {
      config: {
        name: CREATE_TEMPLATE_VARIABLE(TEMPLATE_VAR_SERVICE_NAME),
        tenantId: this._activeTenant.identifier,
        edges: graph.edges,
        nodes: graph.nodes,
        licensePoolId: this._selectedLicensePool?.identifier,
      },
      description: this._description,
      name: this._name,
      nfConfigs: this.getNfConfigs(graph.nodes)
    };

    const serviceTemplate$ = this._editMode
      ? this._serviceTemplateService.putServiceTemplate(this._template.identifier, template)
      : this._serviceTemplateService.postServiceTemplate(template);

    serviceTemplate$.subscribe({
      next: () => {
        this._alert.info(AlertType.INFO_SAVE_TEMPLATE_SUCCESS);
        this._router.navigate(['/templates']);
      },
      error: ({ error }: HttpErrorResponse) => this._alert.error(AlertType.ERROR_SAVE_TEMPLATE, error?.message)
    });
  }

  private verifyTemplateVarsForm(): boolean {
    let valid = true;

    this._templateVarsMap.forEach((value: Set<string>, key: string) => {
      if (!this._templateValuesMap[key]) {
        valid = false;
      }
    });

    return valid;
  }

  private publishServiceFromTemplate() {
    this._templateValuesMap[TEMPLATE_VAR_SERVICE_NAME] = this.name;

    if (!this.verifyTemplateVarsForm()) {
      this._alert.error(AlertType.ERROR_TEMPLATE_INCOMPLETE);
      return;
    }

    const service: ServiceTemplateInstantiation = {
      variableValues: this._templateValuesMap
    };

    this._serviceTemplateService.postServiceFromTenantTemplate(
      this._activeTenant.identifier,
      this._template.identifier,
      service)
      .subscribe({
        next: (config: ServiceConfig) => {
          if (config?.identifier) {
            this._router.navigate(['/tenants', this._activeTenant.identifier, 'service', config.identifier]);
          }
          this._alert.info(AlertType.INFO_CREATE_SERVICE_SUCCESS);
        },
        error: () => this._alert.error(AlertType.ERROR_CREATE_SERVICE)
      });
  }

  public onPublishService() {
    if (!this.hasServiceChanges && this.publishType === PublishType.GRACEFUL) {
      return;
    }
    if (this.name) {
      if (this._publishType === PublishType.TEMPLATE) {
        if (this._mode === Mode.TEMPLATE_INSTANTIATE) {
          this.publishServiceFromTemplate();
        } else {
          this.saveTemplate();
        }
      } else {
        this.publishService(this._activePipeline?.identifier);
      }
    } else {
      this._alert.error(AlertType.ERROR_SERVICE_NO_NAME);
    }
  }

  public onDragStart(eventData: DragEventData): void {
    this._dragged = eventData;
    eventData.event.dataTransfer.setData(eventData.type, (eventData.event.target as any).id); // required for Firefox
  }

  public onCancel() {
    if (this._publishType === PublishType.TEMPLATE) {
      this._router.navigate(['/templates']);
    } else if (this.activeTenantId) {
      this._router.navigate(['/tenants', this.activeTenantId, 'service', this.activePipeline.identifier || 'overview']);
    } else {
      this._router.navigate(['/tenants']);
    }
  }

  /**
   * check unavailable NF licenses in selected license pool
   */
  private get licensesAvailable$(): Observable<undefined | never> {
    let unavailableLicenseNFName: string;

    for (const [nodeID, nfConfiguration] of this._pipelineService.nfConfigurations) {
      const isNFLicenseRequired: boolean = Object
        .values(nfConfiguration?.manifest?.components ?? {})
        .some((nfDescriptorComponent: NFDescriptorComponent) => {
          const licenseMode: LicenseMode = nfDescriptorComponent?.vmManifest?.licenseMode;

          return licenseMode !== LicenseMode.NOLICENSE;
        });

      if (isNFLicenseRequired) {
        // NF license is required but license pool is not selected
        if (!this._selectedLicensePool) {
          unavailableLicenseNFName = nfConfiguration.nf.local?.name;

          break;
        }

        // check if NF license is available in a selected license pool
        const hasNFLicenseAvailable = Object
          .values(this._selectedLicensePool?.licenses ?? {})
          .some(({
            config: { nfId },
            status: { state, user }
          }) => {
            const isLicenseAvailable = nfId === nfConfiguration.nf?.catalogId;

            let isUserNF: boolean;

            if (state === LicenseState.INUSE) {
              isUserNF = user.instanceId === nodeID;
            }

            const isStatusAvailable = state === LicenseState.AVAILABLE;

            return isLicenseAvailable && (isUserNF || isStatusAvailable);
          });

        // no NF license is available in a selected license pool
        if (!hasNFLicenseAvailable) {
          unavailableLicenseNFName = nfConfiguration.nf.local?.name;

          break;
        }
      }
    }

    // open licenses confirmation dialog
    if (unavailableLicenseNFName !== undefined) {
      const message: string = this._selectedLicensePool
        ? `The selected license pool does not have an available license
        for the network function <strong>${unavailableLicenseNFName}</strong>.`
        : `License pool is not selected. The network function <strong>${unavailableLicenseNFName}</strong> requires a license.`;

      const data: ConfirmationDialogData = {
        description: message,
        action: 'Continue Anyways'
      };

      const dialogRef: MatDialogRef<ConfirmationDialogData, boolean> = this._dialog.open(
        ConfirmationDialogComponent,
        { ...DIALOG_CONFIG, data }
      );

      return dialogRef
        .afterClosed()
        .pipe(
          // emit next for confirmation or complete for cancellation or closing the dialog
          mergeMap((isConfirmed: boolean) => isConfirmed ? of(undefined) : EMPTY)
        );
    }

    // emit next for all licenses available (no unavailable license)
    return of(undefined);
  }

  private postSavedNFConfigurations(serviceId: string, isDraft = false): Observable<any[]> {
    const configRequests: Observable<any>[] = [];

    for (const [nodeId, nfConfiguration] of this._pipelineService.nfConfigurations) {
      // add post config request
      if (nfConfiguration.config) {
        const request = isDraft
          ? this._servicesService.putServiceDraftNFConfig(
            this._activeTenant.identifier,
            serviceId,
            nodeId,
            nfConfiguration.config
          ) :
          this._nfService
            .postNfConfig(
              this._activeTenant.identifier,
              serviceId,
              nodeId,
              nfConfiguration.config
            )
            .pipe(
              catchError(({
                error: { message }
              }: HttpErrorResponse): Observable<any> => {
                this._alert.error(
                  `Could not configure ${nfConfiguration.nf.local.name} (${nodeId}), please try again.`,
                  message
                );

                return EMPTY;
              })
            );

        configRequests.push(request);
      }

      // add post manifest request
      if (nfConfiguration.manifest) {
        const request = isDraft
          ? this._servicesService.putServiceDraftNFManifest(
            this._activeTenant.identifier,
            serviceId,
            nodeId,
            nfConfiguration.manifest
          )
          : this._pipelineService
            .putNfManifest(
              this._activeTenant.identifier,
              serviceId,
              nodeId,
              nfConfiguration.manifest
            )
            .pipe(
              catchError(({
                error: { message }
              }: HttpErrorResponse): Observable<any> => {
                this._alert.error(
                  `Could not apply the manifest to ${nfConfiguration.nf.local.name} (${nodeId}), please try again.`,
                  message
                );

                return EMPTY;
              })
            );

        configRequests.push(request);
      }
    }

    return configRequests.length
      ? forkJoin(configRequests)
      : of(undefined);
  }

  private publishService(identifier: string) {
    if (this._name) {
      const saveDraftSelected: boolean = this._publishType === PublishType.DRAFT;
      const force: boolean = this._publishType === PublishType.FORCE;

      const uniqueUcpeSitesWithPorts = [];
      const getServersObs$: { [key: string]: Observable<Server[]> } = {};

      // request servers for sites with connected ports
      this._pipelineComponent?.ports?.forEach((node: PipelineNode, portName: string) => {
        const siteId = node.site;
        // make requests for unique site id's, prevent duplicate requests
        if (!uniqueUcpeSitesWithPorts.includes(siteId) && this._pipelineComponent?.getSiteType(siteId) === SiteConfigSiteType.UCPE) {
          uniqueUcpeSitesWithPorts.push(siteId);
          getServersObs$[siteId] = this._serversService.getServers(siteId);
        }
      });

      this._isPublishing = true;
      const graph = (this._pipelineComponent?.getGraph() ?? {}) as PipelineGraph;

      this.licensesAvailable$
        .pipe(
          mergeMap(() => forkJoin(getServersObs$).pipe(defaultIfEmpty({}))),
          tap((servers: { [key: string]: Server[] }) => {
            if (!isEmpty(graph) && !isEmpty(servers)) {
              this.addTunnelMacroToUcpePorts(graph, servers);
            }
          }),
          // publish service if NF licenses are available or continued by user
          mergeMap<undefined, Observable<ServiceConfig | Pipeline>>(() => {
            graph.name = this._name;
            graph.description = this._description;

            const poolID: string = this._selectedLicensePool?.identifier;

            if (!identifier) {
              // creating new service
              return this._servicesService.postService(
                this._activeTenant.identifier,
                graph,
                poolID,
                force,
                saveDraftSelected);
            } else {
              // modifying existing service
              if (saveDraftSelected || this._hasDraft) {
                return this._servicesService.putServiceDraft(
                  this._activeTenant.identifier, identifier, graph, poolID, (poolID === undefined)
                );
              } else {
                return force
                  || this.hasServiceChanges
                  ? this._pipelineService.patchTenantPipeline(this._activeTenant, identifier, graph, poolID, force)
                  : of(this._activePipeline);
              }
            }
          }),
          // post saved service license pool and NF configurations
          concatMap((service: ServiceConfig): Observable<ServiceConfig> => {
            return this.postSavedNFConfigurations(service?.identifier ?? identifier, saveDraftSelected || this._hasDraft).pipe(
              // pass on service value
              concatMap(() => {
                // no need to commit draft when "save draft" type selected
                return (this._hasDraft && !saveDraftSelected)
                  ? this._servicesService.postServiceDraftCommit(
                    this._activeTenant.identifier, identifier, force)
                  : of({});
              }),
              mapTo<any[] | ServiceConfig, ServiceConfig>(service)
            );
          })
        )
        .subscribe((service: ServiceConfig) => {
          this._pipelineComponent?.updateStoredNodePositions();
          this._isPublishing = false;

          let message: string;

          if (!saveDraftSelected) {
            if (service?.identifier) {
              this._router.navigate(['/tenants', this._activeTenant.identifier, 'service', service.identifier]);
            } else if (this._activePipeline?.identifier) {
              this._router.navigate(['/tenants', this._activeTenant.identifier, 'service', this._activePipeline.identifier]);
            } else {
              this._router.navigate(['/tenants', this._activeTenant.identifier, 'service', 'overview']);
            }
          }

          if (saveDraftSelected) {
            message = AlertType.INFO_SAVE_SERVICE_DRAFT_SUCCESS;
            this._hasDraft = true;
            if (this._pipelineComponent) {
              this._pipelineComponent.isDraft = true;
            }
          } else if (this.editMode) {
            message = AlertType.INFO_UPDATE_SERVICE_SUCCESS;
          } else {
            message = AlertType.INFO_CREATE_SERVICE_SUCCESS;
          }

          this._alert.info(message);
        }, ({ error: { message, errorCode } }: HttpErrorResponse) => {
          this._isPublishing = false;
          switch (errorCode) {
            case PipelineError.DuplicateName: {
              const alert = message ? AlertType.ERROR_CREATE_SERVICE : AlertType.ERROR_DUPLICATE_NAME;
              this._alert.error(alert, message);
              break;
            }
            default:
              if (message) {
                this._alert.error(AlertType.ERROR_SAVING_CHANGES, message);
              } else {
                this._alert.error(AlertType.ERROR_SAVING_CHANGES);
              }
          }
        },
          () => {
            // required for unavailable licenses
            this._isPublishing = false;
          }
        );
    }
  }

  private addTunnelMacroToUcpePorts(graph: PipelineGraph, servers: { [key: string]: Server[] }) {
    this._pipelineComponent.ports.forEach((node: PipelineNode, portName: string) => {
      if (!graph.nodes[portName].port.tunnel) {
        const purpose = node.local?.ctx?.data('purpose');
        const siteId = node.site;
        const interFace: RESTServerNetworkInterface = this.findUCPEInterfaceByPurpose(servers[siteId], purpose);
        const tunnel: TunnelConfig = {
          name: interFace?.name ?? '',
          siteId
        };
        const tunnelMacro = `{{createtunnel ${JSON.stringify(tunnel)} }}`;
        graph.nodes[portName].port.tunnel = tunnelMacro;
      }
    });
  }

  private findUCPEInterfaceByPurpose(servers: Server[], purpose: string): RESTServerNetworkInterface {
    for (const server of servers) {
      if (server?.sockets) {
        for (const socket of server?.sockets) {
          if (socket?.interfaces) {
            for (const interFace of socket?.interfaces) {
              if (interFace?.purpose === purpose) {
                return interFace;
              }
            }
          }
        }
      }
    }
  }

  private initNFCatalog(tenantId?: string): Subscription {
    return this._tenantService.getNfCatalog(tenantId).subscribe((nfs: NFCatalog) => {
      this._nfCatalog = nfs;
    });
  }

  private setServiceLicensePool(isDraft: boolean) {
    this._pipelineService
      .getServiceLicensePool(this._activeTenant.identifier, this._activePipeline.identifier, isDraft)
      .pipe(
        mergeMap((licensePoolID: string) => {
          this._serviceCurrentLicensePoolId = licensePoolID;
          return this._licensingService.getLicensePool(licensePoolID);
        })
      )
      .subscribe((licensePool: UILicensePool) => {
        this._selectedLicensePool = licensePool;
        this._initialSettings.licensePool = licensePool;
      });
  }

  private getLicensePools() {
    this._licensingService
      .getLicensePools()
      .subscribe((licensePools: UILicensePool[]) => {
        this._licensePools = licensePools.filter(({ licenseOwners }) => !licenseOwners?.length);
      });
  }

  public onOpenLicensePoolDropdown() {
    this._isLicensePoolDropdownOpen = true;
  }

  public closeLicensePoolDropdown() {
    this._isLicensePoolDropdownOpen = false;
  }

  public onClearSelectedLicensePool() {
    this._selectedLicensePool = undefined;
    this.closeLicensePoolDropdown();
  }

  public onSelectLicensePool(licensePool: UILicensePool) {
    this._selectedLicensePool = licensePool;
    this.closeLicensePoolDropdown();
  }

  public onCreateLicensePool() {
    this.closeLicensePoolDropdown();

    const dialogRef: MatDialogRef<PoolDialogComponent, string> = this._dialog.open(
      PoolDialogComponent,
      POOL_DIALOG_CONFIG
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((poolId: string) => {
        this.getLicensePools();
      });
  }

  private openSelectSiteDialog() {
    const data: SelectSiteDialogData = {
      isTemplateMode: this._publishType === PublishType.TEMPLATE,
      ignoredSites: this._pipelineComponent?.getSiteGroups()
    };

    const dialogRef = this._dialog.open(
      SelectSiteDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe(([selectedSite, hwProfile]: [Site, HardwareProfile]) => {
        this._newSiteChosen = true;

        setTimeout(() => {
          this._pipelineComponent?.createNewSite(selectedSite.config, hwProfile);
          this.updateCanvasHeight();
        });
      });
  }

  public onAddSiteClick() {
    this._addSite = true;
    this.openSelectSiteDialog();
  }

  public onDrop(ev: Coordinates) {
    this._dragged.id = randomID();
    this._dragged.coordinates = {
      x: ev.x,
      y: ev.y
    };
    switch (this._dragged.type) {
      case 'nf':
        this.addNF(this._dragged);
        break;
    }
    this._dragged = undefined;
  }

  public onDeleteSite() {
    if (this._pipelineComponent?.isEmpty()) {
      this._newSiteChosen = false;
      this._addSite = false;
    }
  }

  public onSetTemplateVariable(ev: Event) {
    this._templateValuesMap[(ev.target as any).name] = (ev.target as any).value;
  }

  private updateCanvasHeight() {
    const padding = 20;
    this._canvasHeight = this._pipelineComponent?.getOptimalCanvasHeight() ?? this._canvasHeight;
    this._canvasHeight += padding;
  }

  private addNF(dragged: DragEventData): void {
    const nodeId = `${dragged.nf.local.catalogId?.replace(/-/g, '_')}_${dragged.id}`;
    const newNode = createPipelineNode(dragged.nf, nodeId, dragged.name, dragged.coordinates);
    const newNFs = new Map(this._nfs);

    newNFs.set(nodeId, newNode);
    this._nfs = newNFs;
  }

  private handleTemplateText(text: string) {
    if (isTemplateVariable(text)) {
      const varText = getTemplateVariableText(text);
      this._templateService.addTemplateVariable(varText);
    }
  }

  public get name(): string {
    return this._name;
  }

  public set name(val: string) {
    this._name = val;
  }

  public get description(): string {
    return this._description;
  }

  public get initialSettings() {
    return this._initialSettings;
  }

  public get selectedLicensePool(): UILicensePool {
    return this._selectedLicensePool;
  }

  public get licensePools(): UILicensePool[] {
    return this._licensePools;
  }

  public get serviceCurrentLicensePoolId(): string {
    return this._serviceCurrentLicensePoolId;
  }

  public get isAdmin$(): Observable<boolean> {
    return this._userService.isAdmin$;
  }

  public get publishType(): PublishType {
    return this._publishType;
  }

  public set publishType(type: PublishType) {
    this._publishType = type;
  }

  public get PublishType(): typeof PublishType {
    return PublishType;
  }

  public get publishDescription(): string {
    return (this._publishType === PublishType.TEMPLATE && this._mode === Mode.TEMPLATE_INSTANTIATE)
      ? PublishType.GRACEFUL
      : this._publishType;
  }

  public get PublishTypes(): PublishType[] {
    switch (this._publishType) {
      case PublishType.TEMPLATE:
        if (this._mode === Mode.TEMPLATE_INSTANTIATE) {
          return [PublishType.GRACEFUL];
        } else {
          return [PublishType.TEMPLATE];
        }
      default:
        return [PublishType.GRACEFUL, PublishType.FORCE, PublishType.DRAFT];
    }
  }

  public get isPublishing(): boolean {
    return this._isPublishing;
  }

  public get hasServiceChanges(): boolean {
    const { name, description, licensePool } = this._initialSettings;
    if (name !== this._name ||
      description !== this._description ||
      licensePool !== this._selectedLicensePool) {
      return true;
    }

    // initial graph must be loaded before a comparison
    if (!this._pipelineComponent?.initialGraph) {
      return false;
    }

    const graph = this._pipelineComponent.getGraph();

    return this._pipelineComponent.hasNfConfigChanges() || this._pipelineComponent.hasGraphChanges(graph);
  }

  public get nfCatalog(): NFCatalog {
    return this._nfCatalog;
  }

  public get isLicensePoolDropdownOpen(): boolean {
    return this._isLicensePoolDropdownOpen;
  }

  public get licensePoolDropdownPositions(): ConnectedPosition[] {
    return this._licensePoolDropdownPositions;
  }

  public get activeTenantId(): string {
    return this._activeTenant && this._activeTenant.identifier || '';
  }

  public get activePipeline(): Pipeline {
    return this._activePipeline;
  }

  public get tenant(): Tenant {
    return this._activeTenant;
  }

  public get ports(): Map<string, PipelineNode> {
    return this._ports;
  }

  public get nfs(): Map<string, PipelineNode> {
    return this._nfs;
  }

  public get addSite(): boolean {
    return this._addSite;
  }

  public get newSiteChosen(): boolean {
    return this._newSiteChosen;
  }

  public get editMode(): boolean {
    return this._editMode;
  }

  public get canvasHeight(): number {
    return this._canvasHeight;
  }

  public get isTemplateMode(): boolean {
    return this._publishType === PublishType.TEMPLATE;
  }

  public get templateVars(): string[] {
    return this._templateService.templateVariables;
  }

  public get mode(): Mode {
    return this._mode;
  }

  public get Mode(): typeof Mode {
    return Mode;
  }

  public get templateVarsMap(): Map<string, Set<string>> {
    return this._templateVarsMap;
  }

  public get templateGraph(): PipelineGraph {
    return this._templateGraph;
  }

  public get TEMPLATE_VAR_SERVICE_NAME(): string {
    return TEMPLATE_VAR_SERVICE_NAME;
  }

  public get hasDraft(): boolean {
    return this._hasDraft;
  }
}
