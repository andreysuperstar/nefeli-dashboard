import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatRadioButtonHarness, MatRadioGroupHarness } from '@angular/material/radio/testing';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatTableModule } from '@angular/material/table';
import { MatTableHarness } from '@angular/material/table/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import profiles from 'mock/hardware_profiles';
import sites from 'mock/sites';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { Site } from 'rest_client/pangolin/model/site';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { CodemirrorComponent } from 'src/app/shared/codemirror/codemirror.component';
import { TemplateInputComponent } from 'src/app/shared/template-input/template-input.component';
import { environment } from 'src/environments/environment';

import { SelectSiteDialogComponent } from './select-site-dialog.component';

describe('SelectSiteDialogComponent', () => {
  let component: SelectSiteDialogComponent;
  let fixture: ComponentFixture<SelectSiteDialogComponent>;
  let httpTestingController: HttpTestingController;
  let loader: HarnessLoader;
  let el: HTMLElement;

  const mockDialogRef = {
    close: jasmine.createSpy('close')
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatTableModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatRadioModule,
        MatSelectModule
      ],
      declarations: [
        SelectSiteDialogComponent,
        TemplateInputComponent,
        CodemirrorComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: MatDialogRef,
          useValue: mockDialogRef
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {}
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectSiteDialogComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();

    httpTestingController.expectOne(`${environment.restServer}/v1/sites`).flush(sites);
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`).flush(profiles);
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;

    mockDialogRef.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render sites list', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);

    expect(el.querySelector('.mat-dialog-title').textContent).toBe('Select Site');

    // verify table header
    const header = await table.getHeaderRows();
    const headerCells = await header[0].getCellTextByIndex();
    expect(header.length).toBe(1);
    expect(headerCells).toEqual(['Name', 'Type']);

    // verify site rows
    const rows = await table.getRows();
    expect(rows.length).toBe(4);
    expect(await rows[0].getCellTextByIndex()).toEqual(['Eric', 'CLUSTER']);
    expect(await rows[1].getCellTextByIndex()).toEqual(['Andrew', 'UCPE']);
    expect(await rows[2].getCellTextByIndex()).toEqual(['Anton', 'CLUSTER']);
    expect(await rows[3].getCellTextByIndex()).toEqual(['Inactive', 'CLUSTER']);

    // template var checkbox hidden by default
    expect(el.querySelector('mat-checkbox')).toBeNull();
  });

  it('should render "No available sites"', async () => {
    const ignoredSites = [
      sites.sites[0].config.identifier,
      sites.sites[1].config.identifier,
      sites.sites[2].config.identifier,
      sites.sites[3].config.identifier
    ];
    component['_data'].ignoredSites = ignoredSites;
    fixture.detectChanges();

    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    expect(el.querySelector('.mat-dialog-title').textContent).toBe('Select Site');

    // verify table header
    const header = await table.getHeaderRows();
    const headerCells = await header[0].getCellTextByIndex();
    expect(header.length).toBe(1);
    expect(headerCells).toEqual(['Name', 'Type']);

    // verify site rows
    const rows = await table.getRows();
    expect(rows.length).toBe(1);
    expect(await rows[0].getCellTextByIndex()).toEqual(['No available sites']);
  });

  it('should render filtered sites list', async () => {
    let ignoredSites = [
      sites.sites[1].config.identifier,
      sites.sites[2].config.identifier,
      sites.sites[3].config.identifier
    ];
    component['_data'].ignoredSites = ignoredSites;
    fixture.detectChanges();

    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    expect(el.querySelector('.mat-dialog-title').textContent).toBe('Select Site');

    // verify table header
    const header = await table.getHeaderRows();
    const headerCells = await header[0].getCellTextByIndex();
    expect(header.length).toBe(1);
    expect(headerCells).toEqual(['Name', 'Type']);

    // verify site rows
    const rows = await table.getRows();
    expect(rows.length).toBe(1);
    expect(await rows[0].getCellTextByIndex()).toEqual(['Eric', 'CLUSTER']);
  });

  it('should close dialog on site select', async () => {
    mockDialogRef.close.and.callFake(([site]: Site[]) => {
      const { config: { identifier, name, siteType } } = site;
      expect(identifier).toBe('inactive_site');
      expect(name).toBe('Inactive');
      expect(siteType).toBe('CLUSTER');
    });

    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows = await table.getRows();
    await (await rows[3].host()).click();
    expect(mockDialogRef.close).toHaveBeenCalled();
  });

  it('should render template variable form', async () => {
    component['_data'].isTemplateMode = true;
    fixture.detectChanges();

    const checkbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    const label = await checkbox.getLabelText();
    expect(label).toBe('Use template variable');
    await checkbox.check();
    fixture.detectChanges();

    expect(el.querySelector('mat-select')).toBeNull();

    const typeGroup = await loader.getHarness<MatRadioGroupHarness>(MatRadioGroupHarness);
    const typeRadios = await typeGroup.getRadioButtons();
    expect(typeRadios.length).toBe(2);
    expect(await typeRadios[0].getLabelText()).toBe('CLUSTER');
    expect(await typeRadios[1].getLabelText()).toBe('UCPE');
    await typeRadios[1].check();
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.directive(CodemirrorComponent))).toBeDefined();
    await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Add'
    }));
  });

  it('should submit template variable form', async () => {
    mockDialogRef.close.and.callFake(([site]: [Site]) => {
      expect(site.config).toEqual({
        identifier: 'full mock var',
        name: 'full mock var',
        siteType: 'UCPE'
      });
    });

    component['_data'].isTemplateMode = true;
    fixture.detectChanges();

    const checkbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    await checkbox.check();
    fixture.detectChanges();

    const typeGroup = await loader.getHarness<MatRadioGroupHarness>(MatRadioGroupHarness);
    const typeRadios = await typeGroup.getRadioButtons();
    await typeRadios[1].check();
    fixture.detectChanges();

    const variable = fixture.debugElement.query(By.directive(CodemirrorComponent));
    variable.componentInstance.docChange.emit({
      change: {
        text: ['{.template-var-1}'],
        from: {}
      },
      doc: {
        getValue: () => 'full mock var',
        markText: (to, from) => undefined
      }
    });

    const submit = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Add'
    }));
    await submit.click();

    expect(mockDialogRef.close).toHaveBeenCalled();
  });
});
