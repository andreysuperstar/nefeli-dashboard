import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HardwareService } from 'rest_client/pangolin/api/hardware.service';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { HardwareProfiles } from 'rest_client/pangolin/model/hardwareProfiles';
import { Site } from 'rest_client/pangolin/model/site';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { Sites } from 'rest_client/pangolin/model/sites';
import { ClusterService } from 'src/app/home/cluster.service';
import { DocChangeEvent } from 'src/app/utils/codemirror';
import { getTemplateVariableText, isTemplateVariable, TemplateService } from '../../templates/template.service';
import { Observable } from 'rxjs';

const COLUMNS: string[] = ['name', 'type'];

export interface SelectSiteDialogData {
  isTemplateMode: boolean;
  ignoredSites?: string[];
}

@Component({
  selector: 'nef-select-site-dialog',
  templateUrl: './select-site-dialog.component.html',
  styleUrls: ['./select-site-dialog.component.less']
})
export class SelectSiteDialogComponent implements OnInit {
  private _form: FormGroup;
  private _dataSource = new Array<Site>();
  private _showTemplateVar = false;
  private _hardwareProfiles = new Array<HardwareProfile>();
  private _submitAttempted = false;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<SelectSiteDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: SelectSiteDialogData,
    private _clusterService: ClusterService,
    private _hardwareService: HardwareService,
    private _templateService: TemplateService
  ) { }

  public ngOnInit() {
    this.initForm();
    this.initSites();
    this.initHardwareProfles();
  }

  private initForm() {
    this._form = this._fb.group({
      type: SiteConfigSiteType.CLUSTER,
      variable: [undefined, Validators.required]
    });
  }

  private initSites() {
    this._clusterService.getSites().subscribe((sites: Sites) => {
      this._dataSource = sites.sites;
    });
  }

  private initHardwareProfles() {
    this._hardwareService.getHardwareProfiles().subscribe({
      next: ({ hardwareProfiles }: HardwareProfiles) => {
        this._hardwareProfiles = hardwareProfiles;
      }
    });
  }

  public onClickRow(site: Site) {
    const hardwareProfileId = site.config.hardwareProfileId;
    const profile = hardwareProfileId ? this._hardwareProfiles?.find((p: HardwareProfile) =>
      p.identifier === hardwareProfileId
    ) : undefined;

    this._dialogRef.close([site, profile]);
  }

  public submitForm() {
    this._submitAttempted = true;
    if (this._form.pristine || !this._form.valid) {
      return;
    }

    const site: Site = {
      config: {
        identifier: this._form.get('variable').value,
        name: this._form.get('variable').value,
        siteType: this._form.get('type').value
      }
    };

    this._dialogRef.close([site]);
  }

  public onSiteVarChange(event: DocChangeEvent) {
    const text = event?.change?.text[0];
    if (isTemplateVariable(text)) {
      const varText = getTemplateVariableText(text);
      this._templateService.addTemplateVariable(varText);
    }
    this._form.get('variable').patchValue(event.doc.getValue());
    this._form.markAsDirty();
  }

  public get displayedColumns(): string[] {
    return COLUMNS;
  }

  public get dataSource(): Site[] {
    return this._dataSource?.filter(site => !this._data.ignoredSites?.includes(site.config?.identifier));
  }

  public get isTemplate(): boolean {
    return this._data?.isTemplateMode;
  }

  public get showTemplateVar(): boolean {
    return this._showTemplateVar;
  }

  public set showTemplateVar(val: boolean) {
    this._showTemplateVar = val;
  }

  public get SiteConfigSiteType(): typeof SiteConfigSiteType {
    return SiteConfigSiteType;
  }

  public get hardwareProfiles(): HardwareProfile[] {
    return this._hardwareProfiles;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get submitAttempted(): boolean {
    return this._submitAttempted;
  }

  public get templateVars(): string[] {
    return this._templateService.templateVariables;
  }
}
