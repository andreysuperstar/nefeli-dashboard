import { MatExpansionModule } from '@angular/material/expansion';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { TenantService } from '../tenant/tenant.service';
import { PipelineModule } from '../pipeline/pipeline.module';
import { SharedModule } from '../shared/shared.module';
import { MatDividerModule } from '@angular/material/divider';

import { ServiceDesignerComponent } from './service-designer/service-designer.component';
import { CreateServiceDialogComponent } from './create-service-dialog/create-service-dialog.component';
import { SelectSiteDialogComponent } from './select-site-dialog/select-site-dialog.component';
import { TrafficPortFormComponent } from './traffic-port-form/traffic-port-form.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { EncapModule } from '../encap/encap.module';
import { NFFormComponent } from './nf-form/nf-form.component';
import { EdgeFormComponent } from './edge-form/edge-form.component';
import { MatRadioModule } from '@angular/material/radio';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    FlexLayoutModule,
    MatTabsModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatListModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    SharedModule,
    PipelineModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonToggleModule,
    MatTableModule,
    MatCheckboxModule,
    MatSelectModule,
    MatOptionModule,
    MatExpansionModule,
    MatRadioModule,
    EncapModule
  ],
  declarations: [
    ServiceDesignerComponent,
    CreateServiceDialogComponent,
    SelectSiteDialogComponent,
    TrafficPortFormComponent,
    NFFormComponent,
    EdgeFormComponent
  ],
  providers: [
    TenantService
  ],
  exports: [
    ServiceDesignerComponent,
    CreateServiceDialogComponent,
    TrafficPortFormComponent,
    NFFormComponent,
    EdgeFormComponent
  ]
})
export class ServiceDesignerModule { }
