import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateServiceDialogComponent } from './create-service-dialog.component';
import { MatTableModule } from '@angular/material/table';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatTableHarness, MatRowHarness, MatHeaderRowHarness } from '@angular/material/table/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { environment } from 'src/environments/environment';
import templates from 'mock/templates';

describe('CreateServiceDialogComponent', () => {
  let component: CreateServiceDialogComponent;
  let fixture: ComponentFixture<CreateServiceDialogComponent>;
  let loader: HarnessLoader;
  let httpTestingController: HttpTestingController;

  const dialogSpy = jasmine.createSpyObj<MatDialogRef<CreateServiceDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateServiceDialogComponent],
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatTableModule,
        MatDialogModule,
        RouterTestingModule
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }, {
          provide: MatDialogRef,
          useValue: dialogSpy
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateServiceDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    httpTestingController = TestBed.inject(HttpTestingController);
    component = fixture.componentInstance;
    fixture.detectChanges();

    httpTestingController.expectOne(`${environment.restServer}/v1/service_templates`).flush(templates);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render options table', async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const headerRows: MatHeaderRowHarness[] = await table.getHeaderRows();
    const rows: MatRowHarness[] = await table.getRows();

    const cells = await headerRows[0].getCellTextByIndex();
    expect(cells).toEqual(["Name", "Description"]);
    expect(rows.length).toBe(4);
  });

  it('should open service designer in selected mode', async () => {
    const createButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Create'
    }));
    await createButton.click();
    expect(component['_dialogRef'].close).toHaveBeenCalledWith('blank');
  });
});
