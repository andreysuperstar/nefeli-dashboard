import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { ServiceTemplatesService } from 'rest_client/pangolin/api/serviceTemplates.service';
import { ServiceTemplate } from 'rest_client/pangolin/model/serviceTemplate';
import { ServiceTemplates } from 'rest_client/pangolin/model/serviceTemplates';

export interface CreateServiceOption {
  id: string;
  name: string;
  description: string;
}

const COLUMNS: string[] = ['name', 'description'];

const ELEMENT_DATA: CreateServiceOption = {
  id: 'blank', name: 'Blank', description: 'New from scratch'
};

type Row = {
  id: string;
  name: string;
  description: string;
};

@Component({
  selector: 'nef-create-service-dialog',
  templateUrl: './create-service-dialog.component.html',
  styleUrls: ['./create-service-dialog.component.less']
})
export class CreateServiceDialogComponent implements OnInit {
  private _selectedRowId = ELEMENT_DATA.id;
  private _dataSource: CreateServiceOption[];

  constructor(
    private _dialogRef: MatDialogRef<CreateServiceDialogComponent>,
    private _templatesService: ServiceTemplatesService
  ) { }

  public ngOnInit() {
    this.initTemplates();
  }

  private initTemplates() {
    this._templatesService.getServiceTemplates().subscribe({
      next: (templates: ServiceTemplates) => {
        this._dataSource = [
          ELEMENT_DATA,
          ...templates?.serviceTemplates.map((template: ServiceTemplate): CreateServiceOption => {
            return {
              id: template.identifier,
              name: template.name,
              description: template.description
            };
          })
        ];
      }
    });
  }

  public onCreateButtonClick() {
    return this._dialogRef.close(this._selectedRowId);
  }

  public onClickRow(row: Row) {
    this._selectedRowId = row.id;
  }

  public isSelectedRow(row: Row): boolean {
    return this._selectedRowId === row.id;
  }

  public get displayedColumns(): string[] {
    return COLUMNS;
  }

  public get dataSource(): CreateServiceOption[] {
    return this._dataSource;
  }
}
