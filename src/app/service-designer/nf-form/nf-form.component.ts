import { Component, ViewChild, OnInit, Inject } from '@angular/core';
import { isEqual } from 'lodash-es';
import { NetworkFunctionService } from '../../pipeline/network-function.service';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { NFType } from 'rest_client/pangolin/model/nFType';
import { BESSInterface } from 'rest_client/pangolin/model/bESSInterface';
import { VMInterface } from 'rest_client/pangolin/model/vMInterface';
import { Tenant } from '../../tenant/tenant.service';
import { PipelineNF } from '../../pipeline/pipeline.model';
import { AlertType, AlertService } from '../../shared/alert.service';
import { PipelineService, NFConfiguration } from '../../pipeline/pipeline.service';
import { catchError } from 'rxjs/operators';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LoggerService } from 'src/app/shared/logger.service';
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { SidebarData, SidebarService } from 'src/app/shared/sidebar.service';
import { EditorConfiguration } from 'codemirror';
import { CodemirrorComponent } from 'src/app/shared/codemirror/codemirror.component';
import { DocChangeEvent } from 'src/app/utils/codemirror';
import { getTemplateVariableText, isTemplateVariable, TemplateService } from 'src/app/templates/template.service';
import { Observable } from 'rxjs';

const DEFAULT_ROLLBACK_POLICY = JSON.stringify({
  Rollback: {
    _comment: 'On config failure, reboot the instance and try again.',
    Policy: 'reboot'
  }
});

export const JSON_SPACES = 3;

export interface NFFormData {
  name: string;
  tenant?: Tenant;
  pipelineId?: string;
  nodeId?: string;
  nf?: PipelineNF;
  manifest?: string;
  configTemplate?: string;
  isEditMode?: boolean;
  isDraft?: boolean;
  isTemplate?: boolean;
}

@Component({
  selector: 'nef-nf-form',
  templateUrl: './nf-form.component.html',
  styleUrls: ['./nf-form.component.less']
})
export class NFFormComponent implements OnInit {
  private _form: FormGroup;
  private _initialManifestInterfaces: BESSInterface[] | VMInterface[];
  private _data: NFFormData;
  private _fileData: string[] = [];

  @ViewChild('codemirror') protected _codemirrorComponent: CodemirrorComponent;

  constructor(
    private _fb: FormBuilder,
    private _nfService: NetworkFunctionService,
    private _alertService: AlertService,
    private _pipelineService: PipelineService,
    private _servicesService: ServicesService,
    private _sidebarService: SidebarService,
    private _logger: LoggerService,
    private _templateService: TemplateService,
    @Inject(SidebarData) sidebarData: SidebarData
  ) {
    this._data = sidebarData.data as NFFormData;
  }

  public ngOnInit(): void {
    this.initNFForm();
    if (this._data) {
      const { name, manifest, configTemplate } = this._data;
      this.getNFConfig();
      this.getNFManifest();
      this._form.patchValue({
        name,
        manifest,
        configTemplate
      });
    }
  }

  private initNFForm() {
    this._form = this._fb.group({
      name: '',
      manifest: '',
      configTemplate: ''
    });
  }

  private setNFConfig(config: NFCConfig) {
    const value: NFCConfig = {
      filter: config.filter,
      mapping: config.mapping
    };

    value.configTemplate = this.beautifyJson(config.configTemplate);
    value.rollback = this.beautifyJson(config.rollback || DEFAULT_ROLLBACK_POLICY);

    this._form.patchValue(value);
  }

  private getNFConfig() {
    const nfConfiguration: NFConfiguration = this._pipelineService.getNFConfiguration(this._data.nodeId);

    if (nfConfiguration?.config) {
      // apply saved config
      this.setNFConfig(nfConfiguration.config);
    } else if (this._data.tenant?.identifier) {
      // get config
      const obs$ = this._data.isDraft ?
        this._servicesService.getServiceDraftNFConfig(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        ) :
        this._nfService.getNfConfig(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        );

      obs$.subscribe((config: NFCConfig) => this.setNFConfig(config));
    }
  }

  private getNFManifest() {
    const nfConfiguration: NFConfiguration = this._pipelineService.getNFConfiguration(this._data.nodeId);

    if (nfConfiguration?.manifest) {
      this._initialManifestInterfaces = this.getManifestIntefactes(nfConfiguration.manifest);

      /* FIXME: use raw manifest string value here because using
         JSON.stringify(nfConfiguration.manifest) leads to
         TypeError: Converting circular structure to JSON,
         issue could be related to PipelineComponent.updateNF method
         which is triggered on saving manifest and closing dialog */

      // apply saved manifest
      this._form.patchValue({
        manifest: nfConfiguration.manifestRaw
      });
    } else if (this._data.tenant?.identifier) {
      // get manifest
      const obs$ = this._data.isDraft ?
        this._servicesService.getServiceDraftNFManifest(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        ) :
        this._pipelineService.getNfManifest(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        );

      obs$.pipe(
        // if pipeline specific manifest doesn't exist, get global manifest
        catchError(() => this._nfService.getNfManifest(this._data.nf.catalogId))
      )
        .subscribe((manifest: NFDescriptor) => {
          this._initialManifestInterfaces = this.getManifestIntefactes(manifest);

          this._form.patchValue({
            manifest: JSON.stringify(manifest, undefined, JSON_SPACES)
          });
        });
    }
  }

  public submitNFForm() {
    if (this._form.pristine) {
      this._sidebarService.close();
      return;
    }

    const nfConfiguration: NFConfiguration = {
      nf: this._data.nf
    };
    let manifest: NFDescriptor;
    const controls = this._form.controls as { [key: string]: FormControl };
    const isDirty = controls.configTemplate.dirty || controls.manifest.dirty;

    if (controls.configTemplate.dirty) {
      const config: NFCConfig = {
        configTemplate: controls.configTemplate.value
      };

      nfConfiguration.config = config;
    }

    if (controls.manifest.dirty) {
      try {
        manifest = JSON.parse(controls.manifest.value);
      } catch (e) {
        this._logger.error(`Failed to parse json: ${controls.manifest.value}`);

        this._alertService.error(AlertType.ERROR_NF_CONFIG_MANIFEST);

        return;
      }

      nfConfiguration.manifest = manifest;
      nfConfiguration.manifestRaw = JSON.stringify(manifest, undefined, JSON_SPACES);
    }

    /* save NF configuration data for further posting while publishing the service
        and editing before publish */
    this._pipelineService.setNFConfiguration(this._data.nodeId, nfConfiguration);

    const manifestInterfaces = this.getManifestIntefactes(nfConfiguration.manifest);
    const hasManifestChanges = !isEqual(this._initialManifestInterfaces, manifestInterfaces);
    const sidebarResult = hasManifestChanges ? manifest : undefined;

    const { name } = this._form.value;
    this._sidebarService.close([name]);
  }

  public onFileSelected(fileEvent: Event, formProp: string) {
    const file: File = fileEvent.target['files'][0];
    const reader = new FileReader();

    reader.onload = (() => {
      return (ev: ProgressEvent) => {
        const data = ev.target['result'];
        this.setFormData(formProp, data);
        this._fileData[formProp] = data;
      };
    })();

    reader.readAsText(file);
  }

  public setFormData(prop: string, data: string) {
    const value = {};
    value[prop] = data;
    this._form.patchValue(value);
    this._form.controls[prop].markAsDirty();
  }

  private handleTemplateText(text: string) {
    if (isTemplateVariable(text)) {
      const varText = getTemplateVariableText(text);
      this._templateService.addTemplateVariable(varText);
    }
  }

  public setTemplateData(prop: string, event: DocChangeEvent) {
    const text = event?.change?.text[0];

    if (text) {
      this.handleTemplateText(text);
    }

    const data = event.doc.getValue();
    this._form.get(prop).patchValue(data);
    this._form.get(prop).markAsDirty();
  }

  private beautifyJson(jsonObj: string): string {
    let rt = jsonObj;

    try {
      rt = JSON.stringify(JSON.parse(jsonObj), undefined, JSON_SPACES);
    } catch (e) { }

    return rt;
  }

  private getManifestIntefactes(manifest: NFDescriptor): BESSInterface[] | VMInterface[] {
    let manifestType: string;

    if (manifest?.type) {
      manifestType = manifest.type === NFType.NATIVE ? 'bessManifest' : 'vmManifest';
    }

    return manifest?.components?.datapath?.[manifestType]?.interfaces;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get codeMirrorOptions(): EditorConfiguration {
    return {
      lineNumbers: false,
      mode: 'javascript',
      theme: 'idea',
      readOnly: !this._data?.isEditMode
    };
  }

  public get isEditMode(): boolean {
    return this._data?.isEditMode;
  }

  public get isTemplate(): boolean {
    return this._data?.isTemplate;
  }

  public get fileData(): string[] {
    return this._fileData;
  }

  public get initialName(): string {
    return this._data?.name;
  }

  public get templateVars(): string[] {
    return this._templateService.templateVariables;
  }
}
