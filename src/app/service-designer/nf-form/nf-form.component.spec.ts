import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CodemirrorComponent } from 'src/app/shared/codemirror/codemirror.component';

import { NFFormComponent, NFFormData } from './nf-form.component';
import { SidebarData } from 'src/app/shared/sidebar.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import nfc from 'mock/nf_config';
import { environment } from 'src/environments/environment';
import manifest from 'mock/nf_manifest';
import { MatInputHarness } from '@angular/material/input/testing';
import { TemplateInputComponent } from 'src/app/shared/template-input/template-input.component';
import { TemplateTextareaComponent } from 'src/app/shared/template-textarea/template-textarea.component';
import { By } from '@angular/platform-browser';
import { TemplateService } from '../../templates/template.service';
import { CREATE_TEMPLATE_VARIABLE } from 'src/app/utils/codemirror';

describe('NFFormComponent', () => {
  let component: NFFormComponent;
  let fixture: ComponentFixture<NFFormComponent>;
  let loader: HarnessLoader;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  let templateService: TemplateService;
  const sidebarData = new SidebarData();
  const mockFormData: NFFormData = {
    name: 'NF Form',
    pipelineId: 'service-id',
    nodeId: 'node-id',
    tenant: {
      identifier: 'tenant-id'
    },
    isEditMode: true,
    isTemplate: false
  };

  sidebarData.data = mockFormData;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatExpansionModule
      ],
      declarations: [
        NFFormComponent,
        CodemirrorComponent,
        TemplateInputComponent,
        TemplateTextareaComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: SidebarData,
          useValue: sidebarData
        }
      ]
    })
      .compileComponents();
    templateService = TestBed.inject(TemplateService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NFFormComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();

    el = fixture.debugElement.nativeElement;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/tenant-id/services/service-id/nodes/node-id/config`).flush(nfc);
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/tenant-id/services/service-id/nodes/node-id/manifest`).flush(manifest);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit form', async () => {
    const testNfName = 'Test NF';
    spyOn(component['_sidebarService'], 'close').and.callFake((val: string[]) => {
      expect(val[0]).toBe(testNfName);
    });

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness);
    await nameInput.setValue(testNfName);

    component.form.patchValue({
      manifest: JSON.stringify({ fake: 'manifest' }),
      configTemplate: 'fake-config-template'
    });
    component.form.controls.manifest.markAsDirty();
    component.form.controls.configTemplate.markAsDirty();

    // submit form
    (el.querySelector('#nf-form') as HTMLFormElement).dispatchEvent(new Event('ngSubmit'));

    expect(component['_sidebarService'].close).toHaveBeenCalled();

    const configuration = component['_pipelineService'].getNFConfiguration(mockFormData.nodeId);
    expect(configuration.config).toEqual({ configTemplate: 'fake-config-template' });
    expect(configuration.manifest).toEqual({ fake: 'manifest' } as any);
    expect(configuration.manifestRaw).toBe('{\n   "fake": "manifest"\n}');
  });

  it('should import nf config from file', () => {
    const prop = 'configTemplate';
    const data = 'this is the nf config file';
    const file: File = new File([data], "nf-config.txt", {
      type: "text",
    });

    const fileList: FileList = {
      length: 1,
      0: file,
      item: (index: number) => {
        return file;
      }
    };

    spyOn<any>(window, "FileReader").and.returnValue({
      onload: () => { },
      readAsText: (f: File) => {
        expect(f.name).toBe("nf-config.txt");
        expect(f.type).toBe("text");
      }
    });

    component.onFileSelected({
      target: {
        files: fileList
      }
    } as any, prop);

    component['setFormData'](prop, data);
    expect(component.form.controls.configTemplate.value).toBe('this is the nf config file');
  });

  it('should render template mode', async () => {
    component['_data'].isTemplate = true;
    fixture.detectChanges();

    const inputs = el.querySelectorAll('nef-template-input');
    expect(inputs[0].getAttribute('placeholder')).toBe('NF Name');
    expect(inputs[1]).toBeUndefined();

    const textareas = el.querySelectorAll('nef-template-textarea');
    expect(textareas[0].getAttribute('placeholder')).toBe('Enter NF Configuration');
    expect(textareas[1]).toBeUndefined();
  });

  it('should save template variables', () => {
    component['_data'].isTemplate = true;
    fixture.detectChanges();

    const [name, nfconfig] = fixture.debugElement.queryAll(By.directive(CodemirrorComponent));

    name.componentInstance.docChange.emit({
      change: {
        text: [CREATE_TEMPLATE_VARIABLE('template-var-1')],
        from: {}
      },
      doc: {
        getValue: () => 'full mock name',
        markText: (to, from) => undefined
      }
    });

    nfconfig.componentInstance.docChange.emit({
      change: {
        text: [CREATE_TEMPLATE_VARIABLE('template-var-2')],
        from: {}
      },
      doc: {
        getValue: () => 'full mock nf config',
        markText: (to, from) => undefined
      }
    });

    expect(component.templateVars).toEqual(['template-var-1', 'template-var-2']);
  });

  // fit('should save updates to template', () => {

  // });
});
