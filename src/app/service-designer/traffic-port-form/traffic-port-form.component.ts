import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { TunnelsService } from 'rest_client/pangolin/api/tunnels.service';
import { Attachment } from 'rest_client/pangolin/model/attachment';
import { Attachments } from 'rest_client/pangolin/model/attachments';
import { Encap } from 'rest_client/pangolin/model/encap';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { RemoteTunnel } from 'rest_client/pangolin/model/remoteTunnel';
import { Tenant } from 'rest_client/pangolin/model/tenant';
import { Tunnel } from 'rest_client/pangolin/model/tunnel';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { Vxlan } from 'rest_client/pangolin/model/vxlan';
import { VxlanVtep } from 'rest_client/pangolin/model/vxlanVtep';
import { VLANType } from 'src/app/encap/encap.component';
import { SidebarData, SidebarService } from 'src/app/shared/sidebar.service';
import { Column } from 'src/app/shared/table/table.component';
import { DocChangeEvent } from 'src/app/utils/codemirror';
import { getTemplateVariableText, isTemplateVariable, TemplateService } from 'src/app/templates/template.service';

enum Mode {
  NEW,
  EXISTING
}

const COLUMNS: Column[] = [
  {
    name: 'name',
    label: 'Presets'
  }
];

export interface TrafficPortFormData {
  site: SiteConfig;
  tenant: Tenant;
  isTemplate?: boolean;
  tunnelId?: string;
}

export interface TunnelRow {
  name: string;
  config: TunnelConfig;
}

@Component({
  selector: 'nef-traffic-port-form',
  templateUrl: './traffic-port-form.component.html',
  styleUrls: ['./traffic-port-form.component.less'],
})
export class TrafficPortFormComponent implements OnInit {
  private _mode = Mode.NEW;
  private _tunnelForm: FormGroup;
  private _dataSource: TunnelRow[] = [];
  private _attachments: Attachment[];
  private _data: TrafficPortFormData;
  private _tunnelMacro: TunnelConfig;
  private _encap: Encap;

  constructor(
    private _fb: FormBuilder,
    private _restTunnelsService: TunnelsService,
    private _sidebarService: SidebarService,
    private _attachmentService: AttachmentsService,
    @Inject(SidebarData) sidebarData: SidebarData,
    private _templateService: TemplateService
  ) {
    this._data = sidebarData.data as TrafficPortFormData;
  }

  public ngOnInit(): void {
    this.initTunnelForm();
    if (this._data) {
      this.fetchTunnels();
      this.fetchSiteAttachments();
      this.updateFormData();
    }
  }

  private initTunnelForm() {
    this._tunnelForm = this._fb.group({
      existingTunnel: '',
      name: [undefined, Validators.required],
      attachment: {
        value: undefined,
        disabled: true
      },
      encap: this._fb.group({})
    });
  }

  private updateFormData() {
    const { tunnelId } = this._data;
    if (tunnelId?.indexOf('createtunnel') !== -1) {
      try {
        this._tunnelMacro = JSON.parse(tunnelId?.replace('{{createtunnel ', '').replace(/}}$/, ''));
        this._tunnelForm.patchValue({
          name: this._tunnelMacro?.name
        });
        const { encap } = this._tunnelMacro.remoteTunnel;
        this._encap = encap;
        this._tunnelForm.get('encap').setValue(encap);
      } catch (e) { }
    }
  }

  private fetchTunnels() {
    const tunnels$ = this._data?.tenant?.identifier
      ? this._restTunnelsService.getTenantTunnels(this._data.tenant.identifier, true)
      : this._restTunnelsService.getTunnels(true);

    tunnels$.subscribe(({ tunnels }) => {
      const unusedTunnels = tunnels?.filter(({ status }) => !status?.usedBy);

      const localTunnel: Tunnel = tunnels.find(({ config }: Tunnel) => {
        return config.identifier === this._data.tunnelId;
      });
      if (localTunnel && localTunnel?.config?.name) {
        this._tunnelForm.get('name').setValue(localTunnel?.config?.name);
      }

      this.setDataSource(unusedTunnels);
    });
  }

  private fetchSiteAttachments() {
    this._attachmentService
      .getAttachments([`eq(site_id,${this._data.site?.identifier})`])
      .subscribe(({ attachments }: Attachments) => {
        this._attachments = attachments as Attachment[];
        const attachment = this._tunnelForm.get('attachment');
        attachment.enable();
        attachment.setValue(this._tunnelMacro?.attachmentId);
      });
  }

  private setDataSource(tunnels: Tunnel[]) {
    this._dataSource = tunnels?.map<TunnelRow>(({ config }) => ({
      name: config.name,
      config
    }));
  }

  public onUseTunnelChange(checked: boolean) {
    this._mode = checked ? Mode.EXISTING : Mode.NEW;
  }

  public onFormSubmit() {
    const { invalid, pristine } = this._tunnelForm;

    if (invalid || pristine) {
      return;
    }

    const {
      name,
      encap: {
        vlan: {
          type: vlanType,
          vid
        },
        vxlan: { vni, mac, ip, udpPort }
      }
    }: {
      name: string;
      encap: {
        vlan: {
          type: string;
          vid: {
            inner: number;
            outer: number;
          }
        },
        vxlan: {
          vni: number;
          mac: string;
          ip: string;
          udpPort: number;
        }
      }
    } = this._tunnelForm.value;

    const attachment = this._tunnelForm.get('attachment').value as string;

    let ivid: number;
    let ovid: number;
    let vxlan: Vxlan;
    switch (vlanType) {
      case VLANType.None:
        if (vni || mac || ip || udpPort) {
          vxlan = {
            vni,
            vtep: {
              ipAddr: ip,
              mac,
              udpPort
            } as VxlanVtep
          };
        } else {
          vxlan = undefined;
        }
        break;

      case VLANType['802.1Q']:
        ivid = vid.inner;

        break;

      case VLANType['802.1AD']:
        ivid = vid.inner;
        ovid = vid.outer;

        break;
    }

    const remoteTunnel: RemoteTunnel = {
      encap: { ivid, ovid }
    };

    if (vxlan) {
      remoteTunnel.encap.vxlan = vxlan;
    }

    const tunnel: TunnelConfig = {
      name,
      siteId: this._data.site.identifier,
      tenantId: this._data.tenant.identifier,
      attachmentId: attachment,
      remoteTunnel
    };

    const tunnelMacro = `{{createtunnel ${JSON.stringify(tunnel)} }}`;

    tunnel.identifier = tunnelMacro;

    this._sidebarService.close(tunnel);
  }

  public onSelectTunnel({ config }: TunnelRow) {
    this._sidebarService.close(config);
  }

  private handleTemplateText(text: string) {
    if (isTemplateVariable(text)) {
      const varText = getTemplateVariableText(text);
      this._templateService.addTemplateVariable(varText);
    }
  }

  public setFormData(prop: string, event: DocChangeEvent) {
    const text = event?.change?.text[0];

    if (text) {
      this.handleTemplateText(text);
    }

    const data = event.doc.getValue();
    const value = {};
    value[prop] = data;
    this._tunnelForm.patchValue(value);
    this._tunnelForm.controls[prop].markAsDirty();
  }

  public get encapForm(): FormGroup {
    return this._tunnelForm?.get('encap') as FormGroup;
  }

  public get tunnelEncap(): Encap {
    return this._encap;
  }

  public get attachments(): Attachment[] {
    return this._attachments;
  }

  public set mode(val: Mode) {
    this._mode = val;
  }

  public get mode(): Mode {
    return this._mode;
  }

  public get ModeType(): typeof Mode {
    return Mode;
  }

  public get tunnelForm(): FormGroup {
    return this._tunnelForm;
  }

  public get columns(): Column[] {
    return COLUMNS;
  }

  public get dataSource(): TunnelRow[] {
    return this._dataSource;
  }

  public get site(): SiteConfig {
    return this._data?.site;
  }

  public get isTemplate(): boolean {
    return this._data?.isTemplate;
  }

  public get templateVars(): string[] {
    return this._templateService.templateVariables;
  }
}
