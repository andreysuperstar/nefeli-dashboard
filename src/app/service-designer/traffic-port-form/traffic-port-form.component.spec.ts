import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatOptionModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatTableModule } from '@angular/material/table';
import { MatTableHarness } from '@angular/material/table/testing';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import attachments from 'mock/attachments';
import sites from 'mock/sites';
import tenants from 'mock/tenants';
import tunnels from 'mock/tunnels';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { EncapComponent } from 'src/app/encap/encap.component';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { CodemirrorComponent } from 'src/app/shared/codemirror/codemirror.component';
import { SidebarData, SidebarService } from 'src/app/shared/sidebar.service';
import { TableComponent } from 'src/app/shared/table/table.component';
import { TemplateInputComponent } from 'src/app/shared/template-input/template-input.component';
import { environment } from 'src/environments/environment';

import { TrafficPortFormComponent, TrafficPortFormData } from './traffic-port-form.component';
import { TemplateService } from '../../templates/template.service';
import { CREATE_TEMPLATE_VARIABLE } from 'src/app/utils/codemirror';

describe('TrafficPortFormComponent', () => {
  let component: TrafficPortFormComponent;
  let fixture: ComponentFixture<TrafficPortFormComponent>;
  let httpTestingController: HttpTestingController;
  let loader: HarnessLoader;
  let templateService: TemplateService;
  const sidebarData = new SidebarData();
  const mockFormData: TrafficPortFormData = {
    site: sites.sites[0].config,
    tenant: tenants.tenants[0]
  };

  sidebarData.data = mockFormData;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        FlexLayoutModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatSelectModule,
        MatOptionModule,
        MatTableModule,
        MatRadioModule,
        MatButtonToggleModule
      ],
      declarations: [
        TrafficPortFormComponent,
        TableComponent,
        EncapComponent,
        TemplateInputComponent,
        CodemirrorComponent
      ],
      providers: [
        SidebarService,
        DecimalPipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: SidebarData,
          useValue: sidebarData
        }
      ]
    })
      .compileComponents();
    templateService = TestBed.inject(TemplateService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrafficPortFormComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    httpTestingController = TestBed.inject(HttpTestingController);
    fixture.detectChanges();

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/tunnels?status=true`).flush(tunnels);
    httpTestingController.expectOne(`${environment.restServer}/v1/attachments?filter=eq(site_id,eric_site)`).flush(attachments);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render form description', () => {
    const desc = fixture.debugElement.nativeElement.querySelector('p').textContent.trim();
    expect(desc).toBe(`Packets entering the site ‘${sites.sites[0].config.name}’ matching these settings will be delivered to this service on this edge. Packets leaving the site through this edge will be encapsulated according to these settings.`);
  });

  it('should render form elements', async () => {
    const presetCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    expect(await presetCheckbox.getLabelText()).toBe('Use preset configuration');

    const encapForm = fixture.debugElement.nativeElement.querySelector('nef-encap');
    expect(encapForm).toBeDefined();

    const attachmentSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness);
    await attachmentSelect.open();
    const options = await attachmentSelect.getOptions();
    expect(await options[0].getText()).toBe('-- None --');
    expect(await options[1].getText()).toBe(attachments.attachments[0].name);
    expect(await options[2].getText()).toBe(attachments.attachments[1].name);
    expect(await options[3].getText()).toBe(attachments.attachments[2].name);
  });

  it('should display list of existing tunnels', async () => {
    spyOn(component['_sidebarService'], 'close').and.callFake((tunnel: TunnelConfig) => {
      expect(tunnel.name).toBe(tunnels.tunnels[3].config.name);
      expect(tunnel.identifier).toBe(tunnels.tunnels[3].config.identifier);
      expect(tunnel.localTunnel.peerTunnelId).toBe(tunnels.tunnels[3].config.localTunnel.peerTunnelId);
    });

    const presetCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    await presetCheckbox.check();
    fixture.detectChanges();

    const tunnelsTable = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows = await tunnelsTable.getRows();

    const unusedTunnels = tunnels.tunnels?.filter(({ status }) => !status?.usedBy);

    expect(rows.length).toBe(unusedTunnels.length);

    await (await rows[0].host()).click();

    expect(component['_sidebarService'].close).toHaveBeenCalled();
  });

  it('should create tunnel on the fly', async () => {
    const mock = {
      name: 'Mock Tunnel Name',
      tenantId: '1',
      siteId: 'eric_site',
      ivid: 123
    };

    spyOn(component['_sidebarService'], 'close').and.callFake(
      ({ attachmentId, identifier, name, remoteTunnel, siteId, tenantId }: TunnelConfig) => {
        expect(attachmentId).toBe(attachments.attachments[1].identifier);
        expect(identifier).toBe(`{{createtunnel {"name":"${mock.name}","siteId":"${mock.siteId}","tenantId":"${mock.tenantId}","attachmentId":"${attachments.attachments[1].identifier}","remoteTunnel":{"encap":{"ivid":${mock.ivid}}}} }}`);
        expect(name).toBe(mock.name);
        expect(siteId).toBe(mock.siteId);
        expect(tenantId).toBe(mock.tenantId);
        expect(remoteTunnel.encap.ivid).toBe(123);
        expect(remoteTunnel.encap.ovid).toBeUndefined();
      });

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter tunnel name'
    }));
    await nameInput.setValue(mock.name);

    const attachmentSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName="attachment"]'
    }));
    await attachmentSelect.open();
    const options = await attachmentSelect.getOptions();
    expect(await options[0].getText()).toBe('-- None --');
    expect(await options[1].getText()).toBe(attachments.attachments[0].name);
    expect(await options[2].getText()).toBe(attachments.attachments[1].name);
    expect(await options[3].getText()).toBe(attachments.attachments[2].name);
    await options[2].click();

    component.tunnelForm.patchValue({
      encap: {
        vlan: {
          type: '802.1Q',
          vid: {
            inner: mock.ivid,
            outer: 321
          }
        }
      }
    });

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Update'
    }));
    await submitButton.click();

    expect(component['_sidebarService'].close).toHaveBeenCalled();
  });

  it('should render template mode', async () => {
    component['_data'].isTemplate = true;
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;

    const [name, nothing] = el.querySelectorAll('nef-template-input');
    expect(nothing).toBeUndefined();
    expect(name.getAttribute('placeholder')).toBe('Enter tunnel name');
    expect(el.querySelector('nef-encap')).toBeDefined();

    const select = await loader.getHarness<MatSelectHarness>(MatSelectHarness);
    expect(await select.host()).toBeDefined();
  });

  it('should save template variables', () => {
    component['_data'].isTemplate = true;
    fixture.detectChanges();

    const name = fixture.debugElement.query(By.directive(CodemirrorComponent));
    name.componentInstance.docChange.emit({
      change: {
        text: [CREATE_TEMPLATE_VARIABLE('template-var-1')],
        from: {}
      },
      doc: {
        getValue: () => 'full mock name',
        markText: (to, from) => undefined
      }
    });

    expect(component.templateVars).toEqual(['template-var-1']);
  });

  it('should render createtunnel macro', async () => {
    const mockTunnel = {
      name: 'Mock Tunnel Name',
      tenantId: '1',
      siteId: 'eric_site',
      ivid: 123
    };
    const tunnelMacro = `{{createtunnel {"name":"${mockTunnel.name}","siteId":"${mockTunnel.siteId}","tenantId":"${mockTunnel.tenantId}","attachmentId":"${attachments.attachments[1].identifier}","remoteTunnel":{"encap":{"ivid":${mockTunnel.ivid}}}} }}`;

    component['_data'].tunnelId = tunnelMacro;
    component.ngOnInit();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/tunnels?status=true`).flush(tunnels);
    httpTestingController.expectOne(`${environment.restServer}/v1/attachments?filter=eq(site_id,eric_site)`).flush(attachments);
    fixture.detectChanges();

    const { name } = component.tunnelForm.value;
    expect(name).toBe(mockTunnel.name);

    const attachmentControl = component.tunnelForm.get('attachment');
    expect(attachmentControl.value).toBe(attachments.attachments[1].identifier);
  });
});
