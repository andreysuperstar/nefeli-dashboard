import { ComponentType } from '@angular/cdk/portal';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { take } from 'rxjs/operators';

export type SidebarResultData = any;

@Injectable({
  providedIn: 'root'
})
export class SidebarData {
  private _data: unknown;

  constructor() { }

  public get data(): unknown {
    return this._data;
  }

  public set data(val: unknown) {
    this._data = val;
  }
}

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  private _content: ComponentType<unknown>;
  private _data: SidebarData;
  private _title: string;
  private _opened: boolean;
  private _afterClosed = new Subject();

  constructor() { }

  public open(title?: string, content?: ComponentType<unknown>, data?: unknown) {
    this._opened = true;
    this._content = content;
    this._data = new SidebarData();
    this._data.data = data;
    this._title = title;
  }

  public close(obj?: SidebarResultData) {
    this._opened = false;
    this._content = undefined;
    this._data = undefined;

    if (obj) {
      this._afterClosed.next(obj);
    }
  }

  public get opened(): boolean {
    return this._opened;
  }

  public get content(): ComponentType<unknown> {
    return this._content;
  }

  public get title(): string {
    return this._title;
  }

  public get data(): SidebarData {
    return this._data;
  }

  public get afterClosed$(): Observable<SidebarResultData> {
    return this._afterClosed.asObservable().pipe(
      take(1)
    );
  }
}
