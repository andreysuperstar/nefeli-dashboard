import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { ConfirmationDialogComponent, ConfirmationDialogData } from './confirmation-dialog.component';

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let fixture: ComponentFixture<ConfirmationDialogComponent>;

  const mockDialogRef: { [key: string]: jasmine.Spy } = {
    close: jasmine.createSpy('close')
  };

  const mockDialogData: ConfirmationDialogData = {
    description: 'The following will be deleted',
    action: 'Delete'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [ConfirmationDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: mockDialogRef
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockDialogData
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render message', () => {
    const matDialogContentEl: HTMLUnknownElement = fixture.debugElement.query(
      By.css('mat-dialog-content')
    ).nativeElement;

    expect(matDialogContentEl.textContent).toContain(mockDialogData.description,
      'message contains description');

    expect(matDialogContentEl.textContent).toContain(
      'Are you sure you want to continue?',
      'message contains question');
  });

  it('should render action button', () => {
    const deleteButtonEl: HTMLButtonElement = fixture.debugElement.queryAll(
      By.css('mat-dialog-actions button')
    )[1].nativeElement;

    expect(deleteButtonEl.textContent).toBe(mockDialogData.action,
      'valid Delete button text');
  });

  it('should close and pass data on button click', () => {
    const cancelButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[mat-stroked-button]')
    ).nativeElement;

    const deleteButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[mat-flat-button]')
    ).nativeElement;

    cancelButtonEl.click();

    expect(mockDialogRef.close).toHaveBeenCalledWith(false);

    mockDialogRef.close.calls.reset();

    deleteButtonEl.click();

    expect(mockDialogRef.close).toHaveBeenCalledWith(true);
  });
});
