import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface ConfirmationDialogData {
  description: string;
  action: string;
}

@Component({
  selector: 'nef-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.less']
})
export class ConfirmationDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private _data: ConfirmationDialogData) { }

  public ngOnInit() {
  }

  public get description(): string {
    return this._data.description;
  }

  public get action(): string {
    return this._data.action;
  }

}
