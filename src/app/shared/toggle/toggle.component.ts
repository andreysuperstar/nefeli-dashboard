import { Component, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';

/**
 * Toggle Component
 *
 * Usage:
 * <nef-toggle leftLabel="" rightLabel="" [leftValue]="" [rightValue]="" (handleChange)=""></nef-toggle>
 */
@Component({
  selector: 'nef-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.less']
})
export class ToggleComponent {

  @ViewChild('left', { static: true }) private _left: ElementRef;
  @ViewChild('right', { static: true }) private _right: ElementRef;

  private _leftLabel: string;
  private _rightLabel: string;
  private _leftValue: string;
  private _rightValue: string;
  private _checkedValue: string;

  constructor() { }

  @Output() public handleChange = new EventEmitter();

  @Input('leftLabel')
  public set leftLabel(value: string) {
    this._leftLabel = value;
  }

  @Input('rightLabel')
  public set rightLabel(value: string) {
    this._rightLabel = value;
  }

  @Input('leftValue')
  public set leftValue(value: string) {
    this._leftValue = value;
  }

  @Input('rightValue')
  public set rightValue(value: string) {
    this._rightValue = value;
  }

  @Input('activeValue')
  public set activeValue(value: string) {
    switch (value) {
      case this._leftValue:
        this._left.nativeElement.checked = true;
        this._right.nativeElement.checked = false;
        break;
      case this._rightValue:
        this._left.nativeElement.checked = false;
        this._right.nativeElement.checked = true;
        break;
    }
  }

  public get leftLabel(): string {
    return this._leftLabel;
  }

  public get rightLabel(): string {
    return this._rightLabel;
  }

  public get leftValue(): string {
    return this._leftValue;
  }

  public get rightValue(): string {
    return this._rightValue;
  }

  public onToggle() {
    const prevLeft = this._left.nativeElement.checked;
    this._left.nativeElement.checked = this._right.nativeElement.checked;
    this._right.nativeElement.checked = prevLeft;
    this._checkedValue = prevLeft ? this.rightValue : this._leftValue;
    this.handleChange.emit(this._checkedValue);
  }

  public get left(): ElementRef {
    return this._left;
  }

  public get right(): ElementRef {
    return this._right;
  }

}
