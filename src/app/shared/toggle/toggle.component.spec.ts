import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ToggleComponent } from './toggle.component';

describe('ToggleComponent', () => {
  let component: ToggleComponent;
  let fixture: ComponentFixture<ToggleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ToggleComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should default to the left side being checked', () => {
    expect(component).toBeTruthy();
    expect(component.left.nativeElement.checked).toBe(true);
    expect(component.right.nativeElement.checked).toBe(false);
  });

  it('should toggle checked on click', () => {
    expect(component).toBeTruthy();
    expect(component.left.nativeElement.checked).toBe(true);
    expect(component.right.nativeElement.checked).toBe(false);

    component.onToggle();
    fixture.detectChanges();

    expect(component.left.nativeElement.checked).toBe(false);
    expect(component.right.nativeElement.checked).toBe(true);
  });

  it('should set labels and values from the parent input', () => {
    component.leftLabel = 'left';
    component.rightLabel = 'right';
    component.leftValue = 'leftVal';
    component.rightValue = 'rightVal';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('.left-label').textContent).toBe('left');
    expect(fixture.debugElement.nativeElement.querySelector('.right-label').textContent).toBe('right');
    expect(fixture.debugElement.nativeElement.querySelector('.left-value').value).toBe('leftVal');
    expect(fixture.debugElement.nativeElement.querySelector('.right-value').value).toBe('rightVal');
  });

  it('should set checked value from the parent input', () => {
    component.rightValue = 'right';
    component.leftValue = 'left';
    component.activeValue = 'right';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('.right-value').checked).toBeTruthy();
    component.activeValue = 'left';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('.left-value').checked).toBeTruthy();
  });

  it('should emit change event on toggle', () => {
    spyOn(component.handleChange, 'emit');
    component.leftValue = 'left';
    component.rightValue = 'right';
    component.activeValue = 'left';
    const toggleElement = fixture.debugElement.nativeElement.querySelector('.toggle');
    toggleElement.click();
    expect(component.handleChange.emit).toHaveBeenCalledWith('right');
  })
});
