import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

import { TEMPLATE_VARIABLE_NAME_PATTERN } from 'src/app/utils/codemirror';

@Component({
  selector: 'nef-template-select',
  templateUrl: './template-select.component.html',
  styleUrls: ['./template-select.component.less']
})
export class TemplateSelectComponent implements OnInit {
  private _control: FormControl;
  private _options$: Observable<string[]>;
  private _value: string;
  private _variables: string[] = [];
  private _autoActiveFirstOption = false;
  private _disabled = false;
  private _placeholder = 'Enter variable';
  private _hint: string;
  private _label: string;
  private _ariaLabel: string;
  private _input = new EventEmitter<string>();
  private _optionSelected = new EventEmitter<string>();

  @Input() public set ariaLabel(value: string) {
    this._ariaLabel = value;
  }

  @Input() public set label(value: string) {
    this._label = value;
  }

  @Input() public set hint(value: string) {
    this._hint = value;
  }

  @Input() public set placeholder(value: string) {
    this._placeholder = value;
  }

  @Input() public set disabled(value: boolean) {
    this._disabled = value;
  }

  @Input() public set autoActiveFirstOption(value: boolean) {
    this._autoActiveFirstOption = value;
  }

  @Input() public set variables(value: string[]) {
    this._variables = value;
  }

  @Input() public set value(value: string) {
    this._value = value;
  }

  @Output() public get input(): EventEmitter<string> {
    return this._input;
  }

  @Output() public get optionSelected(): EventEmitter<string> {
    return this._optionSelected;
  }

  public ngOnInit() {
    this._control = new FormControl(
      this._value,
      {
        validators: Validators.pattern(TEMPLATE_VARIABLE_NAME_PATTERN),
        updateOn: 'change'
      }
    );

    this._options$ = this._control.valueChanges.pipe(
      startWith(''),
      map(this.filter.bind(this))
    );
  }

  private filter(value: string): string[] {
    const filterValue = value?.toLocaleLowerCase();

    return this._variables.filter(
      variable => variable
        .toLocaleLowerCase()
        .includes(filterValue)
    );
  }

  public onInput() {
    this._input.emit(this._control.value);
  }

  public onEnter() {
    const { value, invalid } = this._control;

    if (invalid) {
      this._control.markAsTouched();

      return;
    }

    this._optionSelected.emit(value);
  }

  public onOptionSelected(value: string) {
    this._optionSelected.emit(value);
  }

  public onClose() {
    const { value, valid } = this._control;

    if (valid) {
      this._optionSelected.emit(value);
    }
  }

  public get control(): FormControl {
    return this._control;
  }

  public get options$(): Observable<string[]> {
    return this._options$;
  }

  public get ariaLabel(): string {
    return this._ariaLabel;
  }

  public get label(): string {
    return this._label;
  }

  public get hint(): string {
    return this._hint;
  }

  public get placeholder(): string {
    return this._placeholder;
  }

  public get disabled(): boolean {
    return this._disabled;
  }

  public get autoActiveFirstOption(): boolean {
    return this._autoActiveFirstOption;
  }

  public get variables(): string[] {
    return this._variables;
  }

  public get value(): string {
    return this._value;
  }
}
