import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateSelectComponent } from './template-select.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement, EventEmitter } from '@angular/core';
import { By } from '@angular/platform-browser';

const mockVariables = ['site-name', 'vm-server-address', 'tenant-id'];

describe('TemplateSelectComponent', () => {
  let component: TemplateSelectComponent;
  let fixture: ComponentFixture<TemplateSelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatIconModule,
        MatAutocompleteModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [TemplateSelectComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render field', () => {
    component.variables = mockVariables;
    component.ngOnInit();
    fixture.detectChanges();

    const formFieldDe: DebugElement = fixture.debugElement.query(
      By.css('mat-form-field')
    );

    expect(formFieldDe).not.toBeNull('render control');

    const prefixIconDe: DebugElement = formFieldDe.query(By.css('span[matPrefix]'));
    expect(prefixIconDe).not.toBeNull('render prefix icon');

    const inputFieldDe: DebugElement = formFieldDe.query(By.css('input'));
    expect(inputFieldDe).not.toBeNull('render input field');

    const arrowIconDe: DebugElement = formFieldDe.query(By.css('mat-icon[matSuffix]'));
    expect(arrowIconDe).not.toBeNull('render arrow icon');

    const matOptions = document.querySelectorAll('mat-option');
    expect(matOptions.length).toBe(0, 'should not render options');
  });

  it('should display list of options', () => {
    component.variables = mockVariables;
    component.ngOnInit();
    fixture.detectChanges();

    const inputFieldEl: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    inputFieldEl.dispatchEvent(new Event('focusin'));

    const matOptions: NodeListOf<HTMLUnknownElement> = document.querySelectorAll('mat-option');
    expect(matOptions.length).toBe(3, 'should show options');
    expect(matOptions[0].textContent.trim()).toBe(mockVariables[0]);
    expect(matOptions[1].textContent.trim()).toBe(mockVariables[1]);
    expect(matOptions[2].textContent.trim()).toBe(mockVariables[2]);
  });

  it('should emit onChange event when type', () => {

    spyOn<EventEmitter<string>>(component.input, 'emit').and.callThrough();

    const inputFieldEl: HTMLInputElement = fixture.debugElement.query(By.css('input')).nativeElement;
    const value = 'vm-server-address';
    inputFieldEl.value = value;
    inputFieldEl.dispatchEvent(new Event('input'));
    fixture.detectChanges();

    expect(component.input.emit).toHaveBeenCalledWith(value);
  });
});
