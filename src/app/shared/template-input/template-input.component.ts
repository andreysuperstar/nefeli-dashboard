import {
  AfterViewInit,
  Component,
  forwardRef,
  Input, Optional, Renderer2,
} from '@angular/core';
import { ControlContainer, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Editor, EditorChangeCancellable, EditorConfiguration, Pass, Pos, Position } from 'codemirror';
import { MatFormFieldAppearance } from '@angular/material/form-field/form-field';
import { TemplateTextareaComponent } from '../template-textarea/template-textarea.component';

@Component({
  selector: 'nef-template-input',
  templateUrl: '../template-textarea/template-textarea.component.html',
  styleUrls: ['./template-input.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TemplateInputComponent),
      multi: true,
    }
  ]
})
export class TemplateInputComponent extends TemplateTextareaComponent implements AfterViewInit {

  private _appearance: MatFormFieldAppearance = 'standard';

  @Input()
  public set appearance(appearance: MatFormFieldAppearance) {
    this._appearance = appearance;
  }

  public constructor(protected _renderer: Renderer2, @Optional() protected _controlContainer: ControlContainer) {
    super(_renderer, _controlContainer);
  }

  public ngAfterViewInit(): void {
    this._codemirrorComponent?.codeMirror.on('beforeChange', this.beforeChange.bind(this));
  }

  public get options(): EditorConfiguration {
    return {
      ... super.options,
      scrollbarStyle: 'null'
    };
  }

  public get appearance(): MatFormFieldAppearance {
    return this._appearance;
  }

  /*
   * Remove all new lines characters before inserting
   */
  private beforeChange(cm: Editor, change: EditorChangeCancellable): boolean {
    const newText = change.text.join('').replace(/\n|\r\n|\r/g, '');
    change.update(change.from, change.to, [newText]);
    return true;
  }
}
