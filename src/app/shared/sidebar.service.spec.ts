import { Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { take } from 'rxjs/operators';

import { SidebarResultData, SidebarService } from './sidebar.service';

@Component({
  selector: 'simple-test-component',
  template: 'Simple Test Component Example'
})
export class SimpleComponent { }

describe('SidebarService', () => {
  let service: SidebarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidebarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should open with title and custom content', () => {
    const title = 'My Mock Title';
    service.open(title, SimpleComponent);

    expect(service.title).toBe(title);
    expect(service.content.name).toBe('SimpleComponent');
  });

  it('should send data on close', () => {
    spyOn(service['_afterClosed'], 'next');

    const title = 'My Mock Title';
    service.open(title, SimpleComponent);

    service.afterClosed$.subscribe((obj: SidebarResultData) => {
      expect(obj.hello).toBe('world');
    });

    service.close({
      hello: 'world'
    });

    expect(service['_afterClosed'].next).toHaveBeenCalled();
  });
});
