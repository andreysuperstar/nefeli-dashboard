import { Injectable } from '@angular/core';

enum MetricPrefix {
  TERA = 1000000000000,
  GIGA = 1000000000,
  MEGA = 1000000,
  KILO = 1000,

  // below are relative to nano
  NONE = 1000000000,
  MILLI = 1000000,
  MICRO = 1000,
  NANO = 1
}

const PpsMap = new Array<[string, number]>(
  ['Tpps', MetricPrefix.TERA],
  ['Gpps', MetricPrefix.GIGA],
  ['Mpps', MetricPrefix.MEGA],
  ['kpps', MetricPrefix.KILO]
);

const NsMap = new Array<[string, number]>(
  ['s', MetricPrefix.NONE],
  ['ms', MetricPrefix.MILLI],
  ['μs', MetricPrefix.MICRO],
  ['ns', MetricPrefix.NANO]
);

const BpsMap = new Array<[string, number]>(
  ['Tbps', MetricPrefix.TERA],
  ['Gbps', MetricPrefix.GIGA],
  ['Mbps', MetricPrefix.MEGA],
  ['kbps', MetricPrefix.KILO]
);

@Injectable({
  providedIn: 'root'
})
export class ConverterService {
  constructor() { }

  public packetsPerSeconds(value: number, fractionalLength = 0): [number, string] {
    return this.convert(value, PpsMap, 'pps', fractionalLength);
  }

  public nanoSeconds(value: number, fractionalLength = 0): [number, string] {
    return this.convert(value, NsMap, 'ns', fractionalLength);
  }

  public bitsPerSeconds(value: number, fractionalLength = 0): [number, string] {
    return this.convert(value, BpsMap, 'bps', fractionalLength);
  }

  private convert(value: number, unitMap: Array<[string, number]>, defaultLabel: string, fractionalLength = 0): [number, string] {
    let unitLabel = defaultLabel;
    const ten = 10;
    if (!value) {
      return [0, unitLabel];
    }

    for (const unit of unitMap) {
      if (value >= unit[1]) {
        value = (fractionalLength > 0) ?
          Math.round((value / unit[1]) * Math.pow(ten, fractionalLength)) / Math.pow(ten, fractionalLength)
          : Math.round(value / unit[1]);
        unitLabel = unit[0];
        break;
      } else {
        value = (fractionalLength > 0) ?
          Math.round(value * Math.pow(ten, fractionalLength)) / Math.pow(ten, fractionalLength)
          : value;
      }
    }

    return [value, unitLabel];
  }
}
