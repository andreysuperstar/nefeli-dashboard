import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LegendComponent, LegendItem } from './legend.component';

describe('LegendComponent', () => {
  let component: LegendComponent;
  let fixture: ComponentFixture<LegendComponent>;

  let legendDe: DebugElement;
  let legendEl: HTMLElement;

  let itemEls: NodeListOf<HTMLSpanElement>;

  const mockItems: LegendItem[] = [
    {
      label: 'Palo Alto',
      color: 'blue'
    },
    {
      label: 'Road Balancing',
      muted: false
    },
    {
      label: 'Routing',
      muted: true
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LegendComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LegendComponent);
    component = fixture.componentInstance;

    legendDe = fixture.debugElement;
    legendEl = legendDe.nativeElement;

    component.items = mockItems;

    fixture.detectChanges();

    itemEls = legendEl.querySelectorAll('span');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have items', () => {
    expect(component.items.length).toBe(mockItems.length, `has ${mockItems.length} items`);
    expect(component.items).toBe(mockItems, 'has exact items');
    expect(component.items[0].label).toBe(mockItems[0].label, `1st items label is \'${mockItems[0].label}\'`);
    expect(component.items[1].muted).toBeFalsy('2nd item isn\'t muted');
    expect(component.items[2].muted).toBeTruthy('3rd item is muted');
  });

  it('should create item elements', () => {
    expect(itemEls.length).toBe(mockItems.length, `has ${mockItems.length} item elements`);
    expect(itemEls[1].textContent).toBe(mockItems[1].label, `2nd item element contains \'${mockItems[0].label}\'`);
  });

  it('item elements should have proper \'muted\' style class', () => {
    expect(itemEls[0].classList).not.toContain('muted', 'no \'muted\' class for 1st item element')
    expect(itemEls[2].classList).toContain('muted', '3rd item element contains \'muted\' class')
  });

  it('item elements should have proper color class', () => {
    expect(itemEls[0].classList).toContain(mockItems[0].color, `1st item element contains \'${mockItems[0].color}\' class`);

    expect(itemEls[1].classList).not.toContain('orange', 'no \'orange\' class for 2nd item element');
    expect(itemEls[2].classList).not.toContain('light-blue', 'no \'light-blue\' class for 3rd item element');
  });
});
