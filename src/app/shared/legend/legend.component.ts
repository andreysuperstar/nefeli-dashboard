import { Component, Input } from '@angular/core';

export interface LegendItem {
  label: string;
  muted?: boolean;
  color?: string;
}

@Component({
  selector: 'nef-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.less']
})
export class LegendComponent {

  @Input() public items: LegendItem[];

  constructor() { }

}
