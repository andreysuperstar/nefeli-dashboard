import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MatDialogConfig } from '@angular/material/dialog';

import { AlertService, AlertType } from '../alert.service';
import { LicensesService } from 'rest_client/pangolin/api/licenses.service';

export const POOL_DIALOG_CONFIG: MatDialogConfig = {
  width: '385px',
  restoreFocus: false
};

@Component({
  selector: 'nef-pool-dialog',
  templateUrl: './pool-dialog.component.html',
  styleUrls: ['./pool-dialog.component.less']
})
export class PoolDialogComponent implements OnInit {

  private _poolForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<PoolDialogComponent, string>,
    private _alertService: AlertService,
    private _licenseService: LicensesService
  ) { }

  public ngOnInit() {
    this.initPoolForm();
  }

  private initPoolForm() {
    this._poolForm = this._fb.group({
      name: ['', Validators.required]
    });
  }

  public submitPoolForm() {
    const { invalid, pristine } = this._poolForm;

    if (invalid || pristine) {
      return;
    }

    const { name }: {
      name: string
    } = this._poolForm.value;

    this._licenseService
      .postLicensePool(name)
      .subscribe(
        (_: any) => {
          this._alertService.info(AlertType.INFO_CREATE_LICENSE_POOL_SUCCESS);

          this._dialogRef.close(name);
        },
        (_: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_CREATE_LICENSE_POOL);
        }
      );
  }

  public get poolForm(): FormGroup {
    return this._poolForm;
  }

}
