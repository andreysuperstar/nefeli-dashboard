import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from 'src/environments/environment';

import { LicensesService } from 'rest_client/pangolin/api/licenses.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { AlertService } from '../alert.service';

import { PoolDialogComponent } from './pool-dialog.component';

describe('PoolDialogComponent', () => {
  let component: PoolDialogComponent;
  let fixture: ComponentFixture<PoolDialogComponent>;

  let httpTestingController: HttpTestingController;

  const dialogRefSpy = jasmine.createSpyObj<MatDialogRef<PoolDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatSnackBarModule,
        MatInputModule
      ],
      declarations: [PoolDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: MatDialogRef,
          useValue: dialogRefSpy
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        AlertService,
        LicensesService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoolDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    const headingEl: HTMLHeadingElement = fixture.debugElement.query(
      By.css('[mat-dialog-title]')
    ).nativeElement;

    expect(headingEl.textContent).toBe('Add New Pool', 'valid title');
  });

  it('should render form control', () => {
    const nameInputEl: DebugElement = fixture.debugElement.query(
      By.css('.mat-input-element[formcontrolname=name]')
    );

    expect(nameInputEl).not.toBeNull('render Name input');
  });

  it('should render action buttons', () => {
    const actionButtonDes: DebugElement[] = fixture.debugElement.queryAll(
      By.css('mat-dialog-actions button')
    );

    expect(actionButtonDes[0].nativeElement.textContent).not.toBeNull('Cancel');
    expect(actionButtonDes[1].nativeElement.textContent).not.toBeNull('Create Pool');
  });

  it('should submit Pool form', () => {
    component['_poolForm'].markAsDirty();

    const name = '25310885-c544-4646-8cb8-4d8b46f9b223';

    component['_poolForm'].patchValue({ name });

    component.submitPoolForm();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/license_pools?pool-name=${name}`
    );

    request.flush({});

    expect(request.request.method).toBe('POST');
    expect(request.request.url).toMatch(`${environment.restServer}/v1/license_pools`);

    expect(dialogRefSpy.close).toHaveBeenCalled();
  });
});
