import { DebugElement } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatCardModule } from '@angular/material/card';
import { ComponentFixture, TestBed, inject, waitForAsync } from '@angular/core/testing';

import { NFMemory } from 'src/app/cluster/socket/socket.component';

import { ByteConverterPipe } from '../../pipes/byte-converter.pipe';

import { CellGridComponent, CellGroup } from './cell-grid.component';

describe('CellGridComponent', () => {
  let component: CellGridComponent;
  let fixture: ComponentFixture<CellGridComponent>;
  let cellGridDe: DebugElement;
  let cellGridEl: HTMLElement;

  const cellGroup1: CellGroup = {
    indices: [0, 1, 2]
  };
  const cellGroup2: CellGroup = {
    indices: [5, 10, 15, 20]
  };
  const cellGroup3: CellGroup = {
    indices: [26, 27, 28, 29],
    color: 'orange'
  };
  const nFNames: string[] = ['arista_0', 'nat_0', 'pan_0'];
  const serviceNames: string[] = ['Service 1', 'Service 2', 'Service 3',];
  const nFsMemory: NFMemory[] = [
    {
      memory: 1073741824 * 4,
      color: 'purple',
    },
    {
      memory: 0,
      color: 'red'
    },
    {
      memory: 1073741824 * 10,
      color: 'green'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        MatCardModule,
      ],
      declarations: [
        ByteConverterPipe,
        CellGridComponent
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CellGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    cellGridDe = fixture.debugElement;
    cellGridEl = cellGridDe.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create grid with 45 cells', () => {
    component.totalCells = 45;
    fixture.detectChanges();
    expect(cellGridEl.querySelectorAll('.cell').length).toBe(45);
  });

  it('should appropriately color active cell groups', () => {
    component.totalCells = 30;
    component.activeCells = [cellGroup1, cellGroup2, cellGroup3];

    fixture.detectChanges();

    for (let i = 0; i < component.cells.length; i++) {
      if (cellGroup1.indices.includes(i)) {
        expect(cellGridEl.querySelectorAll('.cell')[i].className.indexOf('purple')).toBeGreaterThan(0);
      } else if (cellGroup2.indices.includes(i)) {
        expect(cellGridEl.querySelectorAll('.cell')[i].className.indexOf('deep-blue')).toBeGreaterThan(0);
      } else if (cellGroup3.indices.includes(i)) {
        expect(cellGridEl.querySelectorAll('.cell')[i].className.indexOf('orange')).toBeGreaterThan(0);
      } else {
        // check that there's no color css class
        expect(cellGridEl.querySelectorAll('.cell')[i].className).toBe('cell');
      }
    }
  });

  it('should appropriately set the size of the cell', () => {
    component.totalCells = 1;
    component.cellSize = 75;

    fixture.detectChanges();
    expect((cellGridEl.querySelectorAll('.cell')[0] as any).style.height).toBe('75px');
    expect((cellGridEl.querySelectorAll('.cell')[0] as any).style.width).toBe('75px');
  });

  it('should color all active cell groups the same color', () => {
    component.totalCells = 30;
    component.singleColorMode = true;
    component.activeCells = [cellGroup1, cellGroup2, cellGroup3];

    fixture.detectChanges();

    for (let i = 0; i < component.cells.length; i++) {
      if (cellGroup1.indices.includes(i) || cellGroup2.indices.includes(i) || cellGroup3.indices.includes(i)) {
        expect(cellGridEl.querySelectorAll('.cell')[i].className.indexOf('blue')).toBeGreaterThan(0);
      } else {
        // check that there's no color css class
        expect(cellGridEl.querySelectorAll('.cell')[i].className).toBe('cell');
      }
    }
  });

  it('should properly animate grid when active cell is hovered & left', waitForAsync(() => {
    component.totalCells = 30;
    spyOn(component, 'leaveCell').and.callThrough();
    component.activeCells = [cellGroup1, cellGroup2, cellGroup3];
    fixture.detectChanges();

    // simulate first hover over cellGroup2, leave, then hover cellGroup1
    const cellEls = cellGridEl.querySelectorAll('.cell');

    cellEls[10].dispatchEvent(new CustomEvent('mouseover'));
    cellEls[10].dispatchEvent(new CustomEvent('mouseout'));
    cellEls[0].dispatchEvent(new CustomEvent('mouseover'));

    expect(component.leaveCell).toHaveBeenCalledTimes(1);

    fixture.whenStable()
      .then(() => {
        fixture.detectChanges();

        for (let i = 0; i < component.cells.length; i++) {
          if (cellGroup1.indices.includes(i)) {
            expect(cellEls[i].className.indexOf('cell-active')).toBeGreaterThan(0);
          } else {
            expect(cellEls[i].className.indexOf('cell-active')).toBe(-1);
          }
        }
      });
  }));

  it('should not animate grid when active cell is hovered and hover mode is false', waitForAsync(() => {
    component.totalCells = 30;
    component.hoverMode = false;
    component.activeCells = [cellGroup1, cellGroup2, cellGroup3];
    fixture.detectChanges();

    const cellEls = cellGridEl.querySelectorAll('.cell');

    cellEls[0].dispatchEvent(new CustomEvent('mouseover'));

    fixture.whenStable()
      .then(() => {
        fixture.detectChanges();

        for (let i = 0; i < component.cells.length; i++) {
          expect(cellEls[i].className.indexOf('cell-active')).toBe(-1);
        }
      });
  }));

  it('popup should have proper NF name, tenant and service name, cores and memory', waitForAsync(() => inject([DOCUMENT], (document: any) => {
    component.totalCells = 30;
    component.activeCells = [cellGroup1, cellGroup2, cellGroup3];

    fixture.detectChanges();

    const cellEls = cellGridEl.querySelectorAll('.cell');

    cellEls[0].dispatchEvent(new CustomEvent('mouseover'));

    fixture.whenStable()
      .then(() => {
        fixture.detectChanges();

        const popupEl = document.querySelector('.cdk-overlay-pane');
        const popupTextEls = popupEl.querySelectorAll('span');

        expect(popupTextEls[0].textContent).toBe(nFNames[0], 'valid popup NF name');
        expect(popupTextEls[1].textContent).toBe(serviceNames[0], 'valid popup service name');
        expect(popupTextEls[2].textContent).toBe(cellGroup1.indices.length.toString(), 'valid popup cores quantity');
        expect(popupTextEls[3].textContent).toBe('4 GB', 'valid popup memory');
      });
  })));

  it('should properly render invalid cores', () => {
    component.totalCells = 30;
    component.activeCells = [cellGroup1, {
      indices: [26, 27, 28, 29],
      invalid: true
    }];

    fixture.detectChanges();

    const cellEls = cellGridEl.querySelectorAll('.cell');

    expect(cellEls[0].classList.contains('cell-invalid')).toBe(false);
    expect(cellEls[1].classList.contains('cell-invalid')).toBe(false);
    expect(cellEls[2].classList.contains('cell-invalid')).toBe(false);
    expect(cellEls[26].classList.contains('cell-invalid')).toBe(true);
    expect(cellEls[27].classList.contains('cell-invalid')).toBe(true);
    expect(cellEls[28].classList.contains('cell-invalid')).toBe(true);
    expect(cellEls[29].classList.contains('cell-invalid')).toBe(true);
  });

});
