import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CdkOverlayOrigin, ConnectedPosition } from '@angular/cdk/overlay';

export const Config = {
  cellColor: 'blue',
  cellColors: [
    'purple',
    'deep-blue',
    'light-blue',
    'light-pink',
    'bright-blue',
    'orange',
    'acid-green',
    'soft-blue',
    'gold'
  ],
  cellSizeLarge: 50
};

interface CellCSS {
  cell: boolean;
  'cell-inactive': boolean;
  'cell-active': boolean;
  'cell-invalid': boolean;
}

interface CellGroupPopupBadge {
  text: string;
  color: string;
}

// type CellGroup = Array<number>;
export interface CellGroup {
  indices: Array<number>;
  color?: string;
  popupBadge?: CellGroupPopupBadge;
  popupFields?: CellPopupField[];
  invalid?: boolean;
}

export interface CellPopupField {
  header: string;
  description: string | number;
}

/*
Usage Example:
<div style="width: 800px; height: 300px">
  <nef-cell-grid
    [activeCells]="[[0, 1, 2, 3], [15], [44, 45, 46, 47]]"
    [totalCells]="48"
    [cellSize]="20"
    [singleColorMode]="true"
    [hoverMode]="false">
  </nef-cell-grid>
</div>
*/

@Component({
  selector: 'nef-cell-grid',
  templateUrl: './cell-grid.component.html',
  styleUrls: ['./cell-grid.component.less']
})
export class CellGridComponent {

  private _cells = new Array<number>();
  private _hoveredCellGroup: CellGroup;
  private _isHoveredCell = false;
  private _isHoveredMode = true;
  private _activeCells = new Array<CellGroup>();
  private _cellSize = Config.cellSizeLarge;
  private _margin: number;
  private _isSingleColorMode = false;

  private _isOpen = false;
  private _overlayOrigin: CdkOverlayOrigin;
  private _connectedPositions: ConnectedPosition[] = [
    {
      originX: 'start',
      originY: 'center',
      overlayX: 'end',
      overlayY: 'center',
      offsetX: -10
    }
  ];

  // array of cell groups; cell group is an array of indices into the grid
  // the top-left cell is 0, then increments across and down
  @Input('activeCells') public set activeCells(val: Array<CellGroup>) {
    this._activeCells = val;
  }
  // length of the width/height of the cell
  @Input('cellSize') public set cellSize(val: number) {
    this._cellSize = val;
  }
  // size of the margin between cells
  @Input('margin') public set margin(val: number) {
    this._margin = val;
  }
  // whether or not each active cell group should have a different color
  @Input('singleColorMode') public set singleColorMode(val: boolean) {
    this._isSingleColorMode = val;
  }
  // whether or not hovering over active cell group causes popup effect
  @Input('hoverMode') public set hoverMode(val: boolean) {
    this._isHoveredMode = val;
  }
  // total number of cells to display in grid
  @Input('totalCells') public set totalCells(val: number) {
    val = +val;
    this._cells = new Array(val);

    for (let i = 0; i < val; i++) {
      this._cells[i] = i;
    }
  }
  // array of cell indexes
  @Input('cells') public set cells(val: Array<number>) {
    this._cells = val;
  }

  @Output() public cellHover = new EventEmitter<number>();

  public get cells(): Array<number> {
    return this._cells;
  }

  public get cellSize(): number {
    return this._cellSize;
  }

  public get margin(): number {
    const divider = 5;
    return this._margin || this.cellSize / divider;
  }

  private getHoveredCellGroup(cell: number): CellGroup {
    let hoveredCellGroup: CellGroup;

    this._activeCells.forEach((group: CellGroup) => {
      if (group.indices.find((num: number) => cell === num) !== undefined) {
        hoveredCellGroup = group;
      }
    });

    return hoveredCellGroup;
  }

  public hoverCell(cell: number, cellOverlayOrigin: CdkOverlayOrigin) {
    if (!this._isHoveredMode) {
      return;
    }

    this._hoveredCellGroup = this.getHoveredCellGroup(cell);
    this._isHoveredCell = true;

    if (this._hoveredCellGroup) {
      this.openPopup(cellOverlayOrigin, cell);
    }
  }

  public leaveCell() {
    if (!this._isHoveredMode) {
      return;
    }

    if (this._hoveredCellGroup) {
      this.closePopup();
    }

    this._hoveredCellGroup = undefined;
    this._isHoveredCell = false;
  }

  public isHoveredCellGroup(cell: number): boolean {
    if (this._hoveredCellGroup.indices.find((num: number) => cell === num) !== undefined) {
      return true;
    }

    return false;
  }

  private getCellProps(cell: number): {
    color: string;
    invalid: boolean;
  } {
    let color = '';
    let invalid = false;

    if (this._activeCells) {
      this._activeCells.forEach((group: CellGroup, index: number) => {
        if (group.indices.find((num: number) => cell === num) !== undefined) {
          if (this._isSingleColorMode) {
            color = Config.cellColor;
          } else if (group.color) {
            color = group.color;
          } else {
            color = Config.cellColors[index % Config.cellColors.length];
          }

          invalid = group.invalid;
        }
      });
    }

    return { color, invalid };
  }

  public getCellCSS(cell: number): CellCSS {
    let cellInactive = false;
    let cellActive = false;
    let css: CellCSS;
    const props = this.getCellProps(cell);

    if (this._hoveredCellGroup !== undefined) {
      cellInactive = !this.isHoveredCellGroup(cell);
      cellActive = this.isHoveredCellGroup(cell);
    }

    css = {
      cell: true,
      'cell-inactive': cellInactive,
      'cell-active': cellActive,
      'cell-invalid': props.invalid
    };
    css[props.color] = true;

    return css;
  }

  public get isOpen(): boolean {
    return this._isOpen;
  }

  public get overlayOrigin(): CdkOverlayOrigin {
    return this._overlayOrigin;
  }

  public get connectedPositions(): ConnectedPosition[] {
    return this._connectedPositions;
  }

  private openPopup(origin: CdkOverlayOrigin, cell: number) {
    this._overlayOrigin = origin;
    this.setPopupData(cell);
    this._isOpen = true;
  }

  private closePopup(): void {
    this._isOpen = false;
    this.resetPopupData();
  }

  private setPopupData(cell: number): void {
    const index: number = this._activeCells.findIndex(
      (cellGroup: CellGroup): boolean => cellGroup.indices.includes(cell)
    );

    this.cellHover.emit(index);
  }

  private resetPopupData(): void {
    this.cellHover.emit();
  }

  public get popupBadge(): CellGroupPopupBadge {
    return this._hoveredCellGroup ? this._hoveredCellGroup.popupBadge : undefined;
  }

  public get popupFields(): Array<CellPopupField> {
    return this._hoveredCellGroup.popupFields;
  }
}
