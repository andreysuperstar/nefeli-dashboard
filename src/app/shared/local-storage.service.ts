import { Injectable } from '@angular/core';

import { User } from 'rest_client/heimdallr/model/user';

import { UserService } from '../users/user.service';

export enum LocalStorageKey {
  PERFORMANCE_CHART_DATE_RANGE = 'performance_chart_date_range',
  TENANT_CHART_DATE_RANGE = 'tenant_chart_date_range',
  ACCESS_TOKEN = 'access_token',
  USER_NAME = 'user_name',
  HIDE_ALARM_BANNER = 'hide_alarm_banner',
  UNREAD_CRITICAL_ALARM_COUNT = 'unread_critical_alarm_count',
  SERVER_COLOR_INDEX = 'server_color_index',
  SERVER_COLOR_PREFIX = 'server_color_',
  VIEW_TENANT_NOTIFICATIONS = 'view_tenant_notifications',
  IS_SAAS = 'saas'
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  private _store = window.localStorage;
  private _username: string;

  constructor(
    private _userService: UserService
  ) {
    this.setUsername();
  }

  private setUsername() {
    this._userService.user$.subscribe((u: User) => {
      if (u) {
        this._username = u.username;
      }
    });
  }

  private getUserKey(key: LocalStorageKey | string): string {
    return `${this._username}_${key}`;
  }

  private isUserKey(key: LocalStorageKey | string): boolean {
    return (key === LocalStorageKey.HIDE_ALARM_BANNER)
      || (key === LocalStorageKey.UNREAD_CRITICAL_ALARM_COUNT);
  }

  public write(key: LocalStorageKey | string, data: any): any {
    if (this.isUserKey(key)) {
      key = this.getUserKey(key);
    }

    this._store.setItem(key, data);
  }

  public read(key: LocalStorageKey | string): any | null {
    if (this.isUserKey(key)) {
      key = this.getUserKey(key);
    }

    return this._store.getItem(key);
  }

  public delete(key: LocalStorageKey | string): any {
    if (this.isUserKey(key)) {
      key = this.getUserKey(key);
    }

    return this._store.removeItem(key);
  }

  public cleanUserSession() {
    this.delete(LocalStorageKey.ACCESS_TOKEN);
    this.delete(LocalStorageKey.USER_NAME);
    this.delete(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS);
    this.delete(LocalStorageKey.IS_SAAS);
  }
}
