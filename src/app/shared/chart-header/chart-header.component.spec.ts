import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartHeaderComponent } from './chart-header.component';
import { LegendComponent, LegendItem } from '../legend/legend.component';

describe('ChartHeaderComponent', () => {
  let component: ChartHeaderComponent;
  let fixture: ComponentFixture<ChartHeaderComponent>;

  let chartHeaderDe: DebugElement;
  let chartHeaderEl: HTMLElement;

  const mockLegendItems: LegendItem[] = [
    {
      label: 'Palo Alto',
      color: 'blue'
    },
    {
      label: 'Road Balancing',
      muted: false
    },
    {
      label: 'Routing',
      muted: true
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ChartHeaderComponent,
        LegendComponent
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartHeaderComponent);
    component = fixture.componentInstance;

    chartHeaderDe = fixture.debugElement;
    chartHeaderEl = chartHeaderDe.nativeElement;

    component.legendItems = mockLegendItems;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have legend items', () => {
    expect(component.legendItems.length).toBe(mockLegendItems.length, `has ${mockLegendItems.length} legend items`);
    expect(component.legendItems).toBe(mockLegendItems, 'has exact items');
    expect(component.legendItems[0].label).toBe(mockLegendItems[0].label, `1st legend item label is \'${mockLegendItems[0].label}\'`);
    expect(component.legendItems[1].muted).toBeFalsy('2nd legend item isn\'t muted');
    expect(component.legendItems[2].muted).toBeTruthy('3rd legend item is muted');
  });

  it('should render legend', () => {
    const legendEl: HTMLUnknownElement = chartHeaderEl.querySelector('nef-legend');

    expect(legendEl).toBeTruthy('legend element is rendered');
  });
});
