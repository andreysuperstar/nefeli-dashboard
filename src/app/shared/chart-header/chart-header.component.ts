import { Component, Input } from '@angular/core';
import { LegendItem } from '../legend/legend.component';

@Component({
  selector: 'nef-chart-header',
  templateUrl: './chart-header.component.html',
  styleUrls: ['./chart-header.component.less']
})
export class ChartHeaderComponent {

  @Input() public legendItems: LegendItem[];

  constructor() { }

}
