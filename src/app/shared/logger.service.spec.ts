import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import * as localForage from 'localforage';
import * as fileSaver from 'file-saver';

import { environment } from '../../environments/environment';

import { LOG_LEVEL, logLevelNameMap, LoggerService } from './logger.service';
import { LocalStorageService } from './local-storage.service';

describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        LoggerService,
        LocalStorageService
      ]
    });

    service = TestBed.inject(LoggerService);

    spyOn(console, 'debug');
    spyOn(console, 'info');
    spyOn(console, 'warn');
    spyOn(console, 'error');
  });

  beforeEach(async () => {
    await service['clearLogs']();
  });

  afterEach(async () => {
    await service['clearLogs']();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should match logs to the log file size', () => {
    const mockLogs = Array.from({
      length: 100
    }, () => {
      const date = new Date()
        .toISOString();

      const logLevels = Object.keys(LOG_LEVEL).length / 2;
      const randomLogLevel: LOG_LEVEL = Math.floor(Math.random() * logLevels);

      const logLevelName = logLevelNameMap
        .get(randomLogLevel)
        .toUpperCase();

      const message = logLevelNameMap.get(randomLogLevel);

      return `${date} ${logLevelName}: ${message}`;
    });

    service['_logs'] = mockLogs;

    const KB = 1024;
    const maxFileSize = KB * 3;

    service['matchLogsToFileSize'](maxFileSize);

    expect(service['_logs'].length).toBeLessThanOrEqual(83, 'match logs to file size');
  });

  it('should save logs', async () => {
    let message = 'Warn';

    await service['saveLog'](LOG_LEVEL.WARN, message);
    await service['setLogs']();

    expect(service['_logs'][service['_logs'].length - 1].endsWith(`${logLevelNameMap
      .get(LOG_LEVEL.WARN)
      .toUpperCase()
      }: ${message}`)).toBeTruthy('save a warn log');

    message = 'request error';

    await service['saveLog'](LOG_LEVEL.ERROR, message);
    await service['setLogs']();

    expect(service['_logs'][service['_logs'].length - 1].endsWith(`${logLevelNameMap
      .get(LOG_LEVEL.ERROR)
      .toUpperCase()
      }: ${message}`)).toBeTruthy('save an error log');
  });

  it('should only save logs of file log level', async () => {
    spyOn<any>(service, 'saveLog')
      .and.callThrough();

    await service.debug(logLevelNameMap.get(LOG_LEVEL.DEBUG));

    expect(service['saveLog']).toHaveBeenCalledWith(
      LOG_LEVEL.DEBUG,
      logLevelNameMap.get(LOG_LEVEL.DEBUG)
    );

    expect(service['_logs']).toBeUndefined();

    await service.info(logLevelNameMap.get(LOG_LEVEL.INFO));

    expect(service['saveLog']).toHaveBeenCalledWith(
      LOG_LEVEL.INFO,
      logLevelNameMap.get(LOG_LEVEL.INFO)
    );

    expect(service['_logs']).toBeUndefined();

    await service.warn(logLevelNameMap.get(LOG_LEVEL.WARN));

    expect(service['saveLog']).toHaveBeenCalledWith(
      LOG_LEVEL.WARN,
      logLevelNameMap.get(LOG_LEVEL.WARN)
    );

    expect(service['_logs'].length).toBe(1);

    await service.error(logLevelNameMap.get(LOG_LEVEL.ERROR));

    expect(service['saveLog']).toHaveBeenCalledWith(
      LOG_LEVEL.ERROR,
      logLevelNameMap.get(LOG_LEVEL.ERROR)
    );

    expect(service['_logs'].length).toBe(2);
  });

  it('should only log error level', async () => {
    environment.logLevel = LOG_LEVEL.ERROR;

    await service.debug('debug');
    await service.info('info');
    await service.warn('warn');
    await service.error('error');

    expect(console.debug).not.toHaveBeenCalled();
    expect(console.info).not.toHaveBeenCalled();
    expect(console.warn).not.toHaveBeenCalled();
    expect(console.error).toHaveBeenCalledWith('error');
  });

  it('should only log error & warn levels', async () => {
    environment.logLevel = LOG_LEVEL.WARN;

    await service.debug('debug');
    await service.info('info');
    await service.warn('warn');
    await service.error('error');

    expect(console.debug).not.toHaveBeenCalled();
    expect(console.info).not.toHaveBeenCalled();
    expect(console.warn).toHaveBeenCalledWith('warn');
    expect(console.error).toHaveBeenCalledWith('error');
  });

  it('should only log info, warn & error levels', async () => {
    environment.logLevel = LOG_LEVEL.INFO;

    await service.debug('debug');
    await service.info('info');
    await service.warn('warn');
    await service.error('error');

    expect(console.debug).not.toHaveBeenCalled();
    expect(console.info).toHaveBeenCalledWith('info');
    expect(console.warn).toHaveBeenCalledWith('warn');
    expect(console.error).toHaveBeenCalledWith('error');
  });

  it('should log all levels', async () => {
    environment.logLevel = LOG_LEVEL.DEBUG;

    await service.debug('debug');
    await service.info('info');
    await service.warn('warn');
    await service.error('error');

    expect(console.debug).toHaveBeenCalledWith('debug');
    expect(console.info).toHaveBeenCalledWith('info');
    expect(console.warn).toHaveBeenCalledWith('warn');
    expect(console.error).toHaveBeenCalledWith('error');
  });

  it('should download a log file', async () => {
    const mockLogs = [
      '2021-01-01T11:11:02.890Z WARN: Warn',
      '2021-01-01T11:11:03.033Z ERROR: Error'
    ];

    service['_logs'] = mockLogs;

    spyOn(fileSaver, 'saveAs')
      .and.callFake(((logBlob: Blob, filename: string) => {
        expect(logBlob.type).toBe('text/plain', 'correct log file content type');
        expect(logBlob.size).toBe(73, 'correct log file size size');

        expect(filename.startsWith('nefeli-ui-')).toBeTruthy('correct log file name');
        expect(filename.endsWith('.log')).toBeTruthy('correct log file extension');
      }) as typeof fileSaver.saveAs);

    await service.downloadLogFile();

    expect(fileSaver.saveAs).toHaveBeenCalled();
  });

  it('should clear logs', async () => {
    await service.warn('warn');

    spyOn(localForage, 'clear');

    await service.clearLogs();

    expect(localForage.clear).toHaveBeenCalled();

    expect(service['_logs']).toBeUndefined();
  });
});
