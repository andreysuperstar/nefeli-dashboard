import { Component, Input } from '@angular/core';
import { AlarmGroup, Alarm } from '../alarm.service';
import { AlarmDataSource } from '../alarm.data-source';

const Config = {
  tooltipDelay: 1000
};

@Component({
  selector: 'nef-alarm-list',
  templateUrl: './alarm-list.component.html',
  styleUrls: ['./alarm-list.component.less']
})
export class AlarmListComponent {

  private _dataSource: AlarmDataSource;

  @Input() public set dataSource(dataSource: AlarmDataSource) {
    this._dataSource = dataSource;
  }

  constructor() { }

  public get dataSource(): AlarmDataSource {
    return this._dataSource;
  }

  public get tooltipDelay(): number {
    return Config.tooltipDelay;
  }

  public getAlarmDescription(alarm: Alarm): string {
    let description = '';

    if (alarm) {
      description = alarm.reason ||
        `(${alarm.identity.process}) ${alarm.name || alarm['class']}`;
    }

    return description;
  }
}
