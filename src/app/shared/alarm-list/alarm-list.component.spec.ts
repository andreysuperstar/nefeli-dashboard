import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { AlertService } from '../alert.service';
import { AlarmService, AlarmsResponse } from '../alarm.service';
import { AlarmDataSource } from '../alarm.data-source';
import { AlarmListComponent } from './alarm-list.component';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';
import { UserService } from 'src/app/users/user.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { MatChipsModule } from '@angular/material/chips';
import { User } from 'rest_client/heimdallr/model/user';
import { RouterTestingModule } from '@angular/router/testing';

describe('AlarmListComponent', () => {
  let component: AlarmListComponent;
  let fixture: ComponentFixture<AlarmListComponent>;
  let alarmService: AlarmService;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  let userService: UserService;

  const mockTenantUser: User = {
    username: 'Anton',
    email: 'anton@nefeli.io',
    roles: {
      scope: 'tenant',
      id: '-1',
      roles: ['user']
    }
  };

  const mockAlarms: AlarmsResponse = {
    alerts: [
      {
        "name": "name",
        "uuid": "dc143517-757c-41f4-95a1-4e85e38ad832",
        "severity": "error",
        "timestamp": "1547563847",
        "identity": {
          "name": "pangolin"
        },
        "metadata": {},
        "_class": "pipeline",
        "reason": "User created"
      },
      {
        "name": "process",
        "uuid": "dc143517-757c-41f4-95a1-4e85e38ad833",
        "severity": "critical",
        "timestamp": "1547571234",
        "identity": {},
        "metadata": {
          "name": "test",
          "type": "authentication"
        },
        "_class": "user",
        "reason": "Machine is out of disk space"
      },
      {
        "name": "name",
        "uuid": "dc143517-757c-41f4-95a1-4e85e38ad834",
        "severity": "info",
        "timestamp": "1547671234",
        "identity": {
          "process": "heimdallr"
        },
        "metadata": {
          "name": "Other day alert",
          "type": "authentication"
        },
        "_class": "user",
        "reason": ""
      }],
    metadata: {
      "endTime": "",
      "count": 2,
      "startTime": ""
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        MatDividerModule,
        MatListModule,
        MatSnackBarModule,
        ScrollingModule,
        HttpClientTestingModule,
        MatTooltipModule,
        MatChipsModule,
        RouterTestingModule
      ],
      declarations: [AlarmListComponent],
      providers: [
        AlertService,
        AlarmService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        UserService,
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    });

    alarmService = TestBed.inject(AlarmService);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlarmListComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    userService.setUser(mockTenantUser);
    component.dataSource = new AlarmDataSource(alarmService);
    fixture.detectChanges();
  });

  const fetchAlarms = () => {
    const url = `${environment.restServer}/v1/alarms`;
    httpTestingController.expectOne((request: HttpRequest<any>) => request.url === url).flush(mockAlarms);
  };

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the list of alarms', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fetchAlarms();
      fixture.detectChanges();
      const alarms = el.querySelectorAll('mat-list-item');
      expect(alarms.length).toBe(3);
    });
  }));

  it('should render alarms properly', async () => {
    fixture.whenStable().then(() => {
      fetchAlarms();
      fixture.detectChanges();
      const alarms = el.querySelectorAll('mat-list-item');
      const timestamps = el.querySelectorAll('h3');

      // alarm 0
      expect(alarms[0].querySelector('.severity').classList).toContain('info');
      expect(alarms[0].querySelector('.description').textContent).toBe('(heimdallr) name');
      let chips = alarms[0].querySelectorAll('mat-chip');
      expect(chips[0].textContent.trim()).toBe('process:\u00a0heimdallr');
      expect(chips[1].textContent.trim()).toBe('name:\u00a0Other day alert');
      expect(chips[2].textContent.trim()).toBe('type:\u00a0authentication')

      // alarm 1
      expect(alarms[1].querySelector('.severity').classList).toContain('critical');
      expect(alarms[1].querySelector('.description').textContent.trim()).toBe('Machine is out of disk space');
      chips = alarms[1].querySelectorAll('mat-chip');
      expect(chips[0].textContent.trim()).toBe('name:\u00a0test');
      expect(chips[1].textContent.trim()).toBe('type:\u00a0authentication')

      // alarm 2
      expect(alarms[2].querySelector('.severity').classList).toContain('error');
      expect(alarms[2].querySelector('.description').textContent.trim()).toBe('User created');
      chips = alarms[2].querySelectorAll('mat-chip');
      expect(chips[0].textContent.trim()).toBe('name:\u00a0pangolin');
    });
  });

  it('should render virtual scroll list', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fetchAlarms();
      fixture.detectChanges();
      const viewportEl = el.querySelector('.scroll-viewport');
      expect(viewportEl).not.toBeNull();
    });
  }));

  it('should render loading label', waitForAsync(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const viewportEl = el.querySelector('.scroll-viewport');
      expect(viewportEl).not.toBeNull();
      expect(viewportEl.textContent).toBe('Loading...');

      fetchAlarms();
      fixture.detectChanges();
      const alarms = el.querySelectorAll('mat-list-item');
      expect(viewportEl.textContent).not.toBe('Loading...');
      expect(alarms.length).toBe(3);
    });
  }));

});
