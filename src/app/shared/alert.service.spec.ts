import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AlertService, AlertType } from './alert.service';

describe('AlertService', () => {
  let service: AlertService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        BrowserAnimationsModule
      ],
      providers: [
        AlertService
      ]
    });

    service = TestBed.inject(AlertService);
  });

  beforeEach(() => {
    /* TODO: Investigate why alert snack-bar elements are lingering around across tests.
       In the mean time, explicitly remove any snack-bar elements from the DOM. */

    // clean up any alerts that may still be on DOM
    [].forEach.call(
      document.querySelectorAll<HTMLUnknownElement>('simple-snack-bar'),
      (element: HTMLUnknownElement) => {
        element.parentNode.removeChild(element);
      }
    );
  });

  afterEach(() => {
    // clean up any alerts that may still be on DOM
    [].forEach.call(document.querySelectorAll('simple-snack-bar'), (elem) => {
      elem.parentNode.removeChild(elem);
    });
  });

  it('should be created', inject([AlertService], (service: AlertService) => {
    expect(service).toBeTruthy();
  }));

  it('should display the snack bar alert', () => {
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(0);
    service.error(AlertType.ERROR_SERVER_CONNECTION_RETRY);
    expect(service.currentAlertRef.instance.data.message).toBe(AlertType.ERROR_SERVER_CONNECTION_RETRY);
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(1);
  });

  it('should dismiss the alert', () => {
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(0);
    service.error(AlertType.ERROR_SERVER_CONNECTION_RETRY);
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(1);

    spyOn(service.currentAlertRef, 'dismiss');
    service.dismiss(AlertType.ERROR_SERVER_CONNECTION_RETRY);
    expect(service.currentAlertRef.dismiss).toHaveBeenCalled();
  });

  it('should not dismiss the alert because the type differs', () => {
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(0);
    service.error(AlertType.ERROR_SITE_CONNECTION_RETRY);
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(1);

    spyOn(service.currentAlertRef, 'dismiss');
    service.dismiss(AlertType.ERROR_SERVER_CONNECTION_RETRY);
    expect(service.currentAlertRef.dismiss).toHaveBeenCalledTimes(0);
    expect(document.querySelectorAll('simple-snack-bar').length).toBe(1);
  });

  it('should properly strip detected tenant prefixes from error message', () => {
    let msg = '';
    service.error(AlertType.ERROR_REMOVE_TUNNEL, msg);
    expect(service.currentAlertRef.instance.data.message)
      .toBe(`${AlertType.ERROR_REMOVE_TUNNEL}`);

    msg = 'tunnel eric_sink is being used by service: _378079698033015367272246518097500926700eric_test';
    service.error(AlertType.ERROR_REMOVE_TUNNEL, msg);
    expect(service.currentAlertRef.instance.data.message)
      .toBe(`${AlertType.ERROR_REMOVE_TUNNEL} (tunnel eric_sink is being used by service: eric_test)`);

    msg = '_378079698033015367272246518097500926700eric_test';
    service.error(AlertType.ERROR_REMOVE_TUNNEL, msg);
    expect(service.currentAlertRef.instance.data.message)
      .toBe(`${AlertType.ERROR_REMOVE_TUNNEL} (eric_test)`);

    msg = 'service: _36007969803301536727224651809750092670eric_test1 service: _378079698033015367272246518097500926700eric_test2 service: _3880796980330153679272246518097500926700eric_test3';
    service.error(AlertType.ERROR_REMOVE_TUNNEL, msg);
    expect(service.currentAlertRef.instance.data.message)
      .toBe(`${AlertType.ERROR_REMOVE_TUNNEL} (service: eric_test1 service: eric_test2 service: eric_test3)`);

    msg = 'tunnels _eric_sink and _25fake are being used by service: _378079698033015367272246518097500926700eric_test';
    service.error(AlertType.ERROR_REMOVE_TUNNEL, msg);
    expect(service.currentAlertRef.instance.data.message)
      .toBe(`${AlertType.ERROR_REMOVE_TUNNEL} (tunnels _eric_sink and _25fake are being used by service: eric_test)`);
  });
});
