/* tslint:disable:no-console */
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

import * as localForage from 'localforage';
import { saveAs } from 'file-saver';

import { environment } from '../../environments/environment';

import { LocalStorageService } from './local-storage.service';

export enum LOG_LEVEL {
  DEBUG,
  INFO,
  WARN,
  ERROR
}

export const logLevelNameMap = new Map([
  [LOG_LEVEL.DEBUG, 'Debug'],
  [LOG_LEVEL.INFO, 'Info'],
  [LOG_LEVEL.WARN, 'Warn'],
  [LOG_LEVEL.ERROR, 'Error']
]);

const MB = 1048576;

const FILE_LOG_LEVEL = LOG_LEVEL.WARN;
const MAX_FILE_SIZE = MB * 1;

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  private _logs: string[];

  constructor(private _localStorageService: LocalStorageService) {
    this.initLogsStorage();
  }

  private initLogsStorage() {
    localForage.config({
      name: 'logs'
    });
  }

  private async setLogs(): Promise<void> {
    const logs = await localForage.getItem<string[]>('logs');

    this._logs = logs ?? [];
  }

  private matchLogsToFileSize(maxFileSize = MAX_FILE_SIZE) {
    let blob: Blob;

    const parsedMaxFileSize = parseInt(this._localStorageService.read('logFileSize'), 10);

    maxFileSize = parsedMaxFileSize > 0 ? parsedMaxFileSize : maxFileSize;

    do {
      if (blob) {
        this._logs.shift();
      }

      const data = this._logs.join('\n');

      blob = new Blob([data], {
        type: 'text/plain'
      });
    } while (blob.size > maxFileSize);
  }

  public async saveLog(
    logLevel: LOG_LEVEL,
    message: string | HttpErrorResponse | Error
  ): Promise<void> {
    const parsedFileLogLevel = parseInt(this._localStorageService.read('logFileLevel'), 10);

    const fileLogLevel = parsedFileLogLevel in LOG_LEVEL
      ? parsedFileLogLevel as LOG_LEVEL
      : FILE_LOG_LEVEL;

    if (logLevel < fileLogLevel) {
      return;
    }

    if (!this._logs) {
      await this.setLogs();
    }

    const date = new Date()
      .toISOString();

    const logLevelName = logLevelNameMap
      .get(logLevel)
      .toUpperCase();

    if (typeof message !== 'string') {
      message = JSON.stringify(message);
    }

    const log = `${date} ${logLevelName}: ${message}`;

    this._logs.push(log);

    this.matchLogsToFileSize();

    await localForage.setItem('logs', this._logs);
  }

  public async downloadLogFile() {
    if (!this._logs) {
      await this.setLogs();
    }

    const data = this._logs.join('\n');

    const blob = new Blob([data], {
      type: 'text/plain'
    });

    const date = new Date()
      .toISOString();

    const filename = `nefeli-ui-${date}.log`;

    saveAs(blob, filename);
  }

  public async clearLogs(): Promise<void> {
    await localForage.clear();

    this._logs = undefined;
  }

  // production: disabled
  // development: enabled
  public async debug(message: string | HttpErrorResponse | Error) {
    if (environment.logLevel <= LOG_LEVEL.DEBUG) {
      console.debug(message);
    }

    await this.saveLog(LOG_LEVEL.DEBUG, message);
  }

  // production: disabled
  // development: enabled
  public async info(message: string | HttpErrorResponse | Error) {
    if (environment.logLevel <= LOG_LEVEL.INFO) {
      console.info(message);
    }

    await this.saveLog(LOG_LEVEL.INFO, message);
  }

  // production: disabled
  // development: enabled
  public async warn(message: string) {
    if (environment.logLevel <= LOG_LEVEL.WARN) {
      console.warn(message);
    }

    await this.saveLog(LOG_LEVEL.WARN, message);
  }

  // production: enabled
  // development: enabled
  // Only level enabled by default in production mode.
  public async error(message: string | Error) {
    if (environment.logLevel <= LOG_LEVEL.ERROR) {
      console.error(message);
    }

    await this.saveLog(LOG_LEVEL.ERROR, message);
  }
}
