import { Component, Input } from '@angular/core';

@Component({
  selector: 'nef-back-button',
  templateUrl: './back-button.component.html',
  styleUrls: ['./back-button.component.less']
})
export class BackButtonComponent {

  private _link = '../';

  @Input() public set link(link: string) {
    this._link = link;
  }

  constructor() { }

  public get link() {
    return this._link;
  }

}
