import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvatarComponent } from './avatar.component';

describe('AvatarComponent', () => {
  let component: AvatarComponent;
  let fixture: ComponentFixture<AvatarComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvatarComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate the color for the first letter of the name', () => {
    component.name = 'ABC';
    expect(component.getCapColor()).toBe('red');
  });

  it('should calculate different colors for strings with same first letter', () => {
    component.name = 'ABCDE';
    const color1 = component.getCapColor();
    component.name = 'ACEBD';
    const color2 = component.getCapColor();
    expect(color1).not.toEqual(color2);
  });

  it('should calculate constant color for the same string', () => {
    component.name = 'ABC';
    expect(component.getCapColor()).toBe(component.getCapColor());
  });

  it('should not break if default and empty name', () => {
    expect(component.getCapColor()).not.toBeNull();
  });

  it('should show an image if url is given', () => {
    component.name = 'Admin';
    component.url = 'assets/temp.admin-avatar.jpg';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('img')).toBeDefined();
    expect(
      fixture.debugElement.nativeElement.querySelector('img').getAttribute('src')
    ).toBe('assets/temp.admin-avatar.jpg');
    expect(fixture.debugElement.nativeElement.querySelector('.cap-avatar')).toBeNull();
  });

  it('should display the first letter of the name if no url is given', () => {
    component.name = 'Test';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('img')).toBeNull();
    expect(fixture.debugElement.nativeElement.querySelector('.cap-letter').textContent.trim()).toBe('T');
  });

  it('should set width and height for the icon container', () => {
    component.name = 'Test';
    component.height = 50;
    component.width = 50;
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('.cap-avatar').offsetWidth).toBe(50);
    expect(fixture.debugElement.nativeElement.querySelector('.cap-avatar').offsetHeight).toBe(50);
    component.url = 'assets/temp.admin-avatar.jpg';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('img').offsetWidth).toBe(50);
    expect(fixture.debugElement.nativeElement.querySelector('img').offsetHeight).toBe(50);
  });
});
