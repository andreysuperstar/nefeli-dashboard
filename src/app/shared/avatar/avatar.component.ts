import { Component, Input } from '@angular/core';

const CAP_COLORS = ['blue', 'red', 'green'];

export enum AVATAR_MODE {
  USER = 'user',
  DEFAULT = 'default'
}

@Component({
  selector: 'nef-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.less']
})
export class AvatarComponent {
  private _url: string;
  private _name = '';
  private _userName = '';
  private _firstName = '';
  private _lastName = '';
  private _width = 32;
  private _height = 32;
  private _mode: AVATAR_MODE = AVATAR_MODE.DEFAULT;

  @Input('url')
  public set url(value: string) {
    this._url = value;
  }
  @Input('name')
  public set name(value: string) {
    this._name = value?.toUpperCase();
  }

  @Input('mode')
  public set mode(value: AVATAR_MODE) {
    this._mode = value;
  }

  @Input('userName')
  public set userName(value: string) {
    this._userName = value?.toUpperCase();
  }
  @Input('firstName')
  public set firstName(value: string) {
    this._firstName = value?.toUpperCase();
  }
  @Input('lastName')
  public set lastName(value: string) {
    this._lastName = value?.toUpperCase();
  }
  @Input('width')
  public set width(value: number) {
    this._width = value;
  }
  @Input('height')
  public set height(value: number) {
    this._height = value;
  }

  public get url(): string {
    return this._url;
  }

  public get name(): string {
    return this._name;
  }

  public get userName(): string {
    return this._userName;
  }

  public get caption(): string {
    return this._mode === AVATAR_MODE.USER ? this.getInitials() : this._name[0];
  }

  public get width(): number {
    return this._width;
  }

  public get height(): number {
    return this._height;
  }

  public get mode(): AVATAR_MODE {
    return this._mode;
  }

  constructor() { }

  public getCapColor(): string {
    const index = this.getColorIndex(this.name);

    return CAP_COLORS[index];
  }

  private getInitials(): string {
    let initials = this.name[0];

    const hasFirstOrLastName = this._firstName || this._lastName;
    const hasFirstAndLastName = this._firstName && this._lastName;
    if (hasFirstAndLastName) {
      initials = this._firstName[0] + this._lastName[0];
    } else if (hasFirstOrLastName) {
      initials = hasFirstOrLastName[0];
    } else if (this._userName) {
      initials = this._userName[0];
    }

    return initials;
  }

  private getColorIndex(name: string): number {
    let sum = 0;
    let charCode: number;

    for (let i = 0, len = name?.length ?? 0; i < len; i++) {
      charCode = name[i].charCodeAt(0);
      sum += i > 0 ? charCode * i + i : charCode;
    }

    return sum % CAP_COLORS.length;
  }
}
