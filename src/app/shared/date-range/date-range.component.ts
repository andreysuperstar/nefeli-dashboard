import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ConnectedPosition } from '@angular/cdk/overlay';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

import * as moment from 'moment';
import { Moment } from 'moment';

interface DateFormatted {
  day: number;
  month: string;
  year: number;
  momentDate?: Moment;
}

export interface Range {
  start: number;
  duration: number;
  period?: boolean;
}

export enum Period {
  FifteenMinutes = 900,
  SixHours = 21600,
  Day = 86400,
  Week = 604800,
  Month = 2592000,
  Year = 31536000
}

const DEFAULT_PERIOD: Period = Period.SixHours;

@Component({
  selector: 'nef-date-range',
  templateUrl: './date-range.component.html',
  styleUrls: ['./date-range.component.less']
})
export class DateRangeComponent implements OnInit {

  private _fromDate: NgbDateStruct;
  private _toDate: NgbDateStruct;
  private _fromDateFormatted: DateFormatted;
  private _toDateFormatted: DateFormatted;
  private _period: Period;
  private _isDayHovered: NgbDateStruct;

  private _isOpen = false;
  private _connectedPositions: ConnectedPosition[] = [
    {
      originX: 'center',
      originY: 'bottom',
      overlayX: 'center',
      overlayY: 'top',
      offsetY: 8
    },
    {
      originX: 'start',
      originY: 'center',
      overlayX: 'end',
      overlayY: 'center',
      offsetX: -8,
    },
    {
      originX: 'center',
      originY: 'top',
      overlayX: 'center',
      overlayY: 'bottom',
      offsetY: -8
    }
  ];
  private _isButtonHovered = false;
  private _range: Range;

  @Input() public set range(range: Range) {
    this._range = range;

    if (range) {
      this.deserializeDates();
    }
  }

  @Output() public dateSelect = new EventEmitter<Range>();

  constructor(private _calendar: NgbCalendar) { }

  public ngOnInit() {
    if (this._range) {
      this.deserializeDates();
      this.dateSelect.emit(this.serializedRange);
    } else {
      this.setDefaultPeriod();
    }
  }

  public get today(): any {
    return this._calendar.getToday();
  }

  public get fromDate(): NgbDateStruct {
    return this._fromDate;
  }

  public get Period(): typeof Period {
    return Period;
  }

  private setDefaultPeriod() {
    this.setPeriodDates(DEFAULT_PERIOD);
  }

  private deserializeDates() {
    const { duration, period }: Range = this._range;

    if (period) {
      this.setPeriodDates(duration);
      return;
    }

    this._period = undefined;
    this.deserializeRange();
  }

  private deserializeRange() {
    const { start, duration }: Range = this._range;
    const end: number = start + duration;
    const momentStart: Moment = moment.unix(start);
    const momentEnd: Moment = moment.unix(end);

    const deserializeDate: Function = (date: Moment): number[] => date
      .format('YYYY-MM-DD')
      .split('-')
      .map(Number);

    const [startYear, startMonth, startDay]: number[] = deserializeDate(momentStart);
    const [endYear, endMonth, endDay]: number[] = deserializeDate(momentEnd);

    const fromDate = {
      year: startYear,
      month: startMonth,
      day: startDay
    };

    const toDate = {
      year: endYear,
      month: endMonth,
      day: endDay
    };

    this.setDates(fromDate, toDate, momentStart, momentEnd);
  }

  private setDates(
    fromDate: NgbDateStruct,
    toDate: NgbDateStruct,
    fromMoment?: Moment,
    toMoment?: Moment) {

    this.setFromDate(fromDate, fromMoment);
    this.setToDate(toDate, toMoment);
  }

  private setFromDate(date: NgbDateStruct, fromMoment?: Moment) {
    this._fromDate = date;
    this._fromDateFormatted = this.formatDateMonth(this._fromDate, fromMoment);
  }

  private setToDate(date: NgbDateStruct, toMoment?: Moment) {
    this._toDate = date;
    this._toDateFormatted = this.formatDateMonth(this._toDate, toMoment);
  }

  private formatDateMonth({ year, month, day }: NgbDateStruct, momentDate: Moment): DateFormatted {
    const monthName: string = moment(month, 'M').format('MMM');
    const formattedDate: DateFormatted = Object.assign({ day, year, month: monthName, momentDate });
    return formattedDate;
  }

  public get fromDateFormatted(): string {
    return this._fromDateFormatted.momentDate ?
      this._fromDateFormatted.momentDate.format('lll')
      : `${this._fromDateFormatted.day} ${this._fromDateFormatted.month} ${this._fromDateFormatted.year}`;
  }

  public get toDateFormatted(): string {
    return this._toDateFormatted.momentDate ?
      this._toDateFormatted.momentDate.format('lll')
      : `${this._toDateFormatted.day} ${this._toDateFormatted.month} ${this._toDateFormatted.year}`;
  }

  public onPeriodSelect(period: Period) {
    this.setPeriodDates(period);
  }

  private setPeriodDates(period: Period) {
    this._period = period;

    let fromDate;

    const isDays: boolean = period > Period.Day;

    if (isDays) {
      const daysAgo: number = period / Period.Day - 1;

      fromDate = this._calendar.getPrev(this.today, 'd', daysAgo);
    } else {
      fromDate = this.today;
    }

    this.setDates(fromDate, this.today);
    this.close();
  }

  public onDateSelect(date: NgbDateStruct) {
    this._period = undefined;

    if (!this._fromDate && !this._toDate) {
      this.setFromDate(date);
    } else if (this._fromDate && this._toDate === this._fromDate && this.after(date, this._fromDate)) {
      this.setToDate(date);
    } else {
      this.setDates(date, date);
    }
  }

  public get isOpen(): boolean {
    return this._isOpen;
  }

  public get isButtonHovered(): boolean {
    return this._isButtonHovered;
  }

  public onButtonHover() {
    this._isButtonHovered = !this._isButtonHovered;
  }

  public onButtonClick() {
    this._isOpen ? this.close() : this._isOpen = true;
  }

  public get connectedPositions(): ConnectedPosition[] {
    return this._connectedPositions;
  }

  public onBackdropClick() {
    this.close();
  }

  private close() {
    this._isOpen = false;
    this.dateSelect.emit(this.serializedRange);
  }

  private get serializedRange(): Range {
    return this.serializeDates();
  }

  private serializeDates(): Range {
    const dates: NgbDateStruct[] = [this._fromDate, this._toDate];
    const now: Date = new Date();
    const isDayRange = (this.timeOffset === Period.Day);

    const pad = (unit: number, digits: number = 2): string =>
      unit
        .toString()
        .padStart(digits, '0');

    let hours: string = pad(now.getHours());
    let minutes: string = pad(now.getMinutes());
    let seconds: string = pad(now.getSeconds());

    const unixDates: number[] = dates.map((date: NgbDateStruct, index: number): number => {
      const month: string = pad(date.month);
      const day: string = pad(date.day);

      if (isDayRange) {
        hours = Boolean(index) ? '23' : '00';
        minutes = Boolean(index) ? '59' : '00';
        seconds = Boolean(index) ? '59' : '00';
      }

      return moment(
        `${date.year}-${month}-${day}T${hours}:${minutes}:${seconds}`, moment.ISO_8601, true
      ).unix();
    });

    const [startDate, endDate]: number[] = unixDates;

    const start: number = isDayRange ? startDate : startDate - this.timeOffset;
    const duration: Period | number = endDate - start;

    const range: Range = { start, duration, period: Boolean(this._period) };

    return range;
  }

  private get timeOffset(): Period | number {
    if (this._period) {
      return this._period % Period.Day || Period.Day;
    }

    return Period.Day;
  }

  public onDayHover(date?: NgbDateStruct) {
    this._isDayHovered = date ? date : undefined;
  }

  public isDayHovered(date: NgbDateStruct): boolean {
    return this._fromDate && !this._toDate && this._isDayHovered
      && this.after(date, this._fromDate) && this.before(date, this._isDayHovered);
  }

  public isDayInside(date: NgbDateStruct): boolean {
    return this.after(date, this._fromDate) && this.before(date, this._toDate);
  }

  public isDayFrom(date: NgbDateStruct): boolean {
    return this.equals(date, this._fromDate);
  }

  public isDayTo(date: NgbDateStruct): boolean {
    return this.equals(date, this._toDate);
  }

  private equals(one: NgbDateStruct, two: NgbDateStruct): boolean {
    return one && two && two.year === one.year && two.month === one.month && two.day === one.day;
  }

  private before(one: NgbDateStruct, two: NgbDateStruct): boolean {
    return !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
      ? false : one.day < two.day : one.month < two.month : one.year < two.year;
  }

  private after(one: NgbDateStruct, two: NgbDateStruct): boolean {
    return !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
      ? false : one.day > two.day : one.month > two.month : one.year > two.year;
  }

  public get period(): Period {
    return this._period;
  }

}
