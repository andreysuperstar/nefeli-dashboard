import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatCardModule } from '@angular/material/card';
import { NgbDatepickerModule, NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

import { DurationPipe } from '../../pipes/duration.pipe';

import { DateRangeComponent, Range, Period } from './date-range.component';

class NgbDate {
  year: number;
  month: number;
  day: number;

  constructor(year: number, month: number, day: number) {
    this.year = year;
    this.month = month;
    this.day = day;
  };
}

type NgbPeriod = 'y' | 'm' | 'd';

class NgbCalendarMock {
  public getToday(): NgbDate {
    const year: number = 2018
    const month: number = 8;
    const day: number = 11;

    const today: NgbDate = new NgbDate(year, month, day);

    return today;
  }

  public getPrev(date: NgbDate, period: NgbPeriod = 'd', number: number = 1): NgbDate {
    return this.getNext(date, period, -number);
  }

  public getNext(date: NgbDate, period: NgbPeriod = 'd', number: number = 1): NgbDate {
    const toJSDate = (date): Date => {
      const hours: number = 12;
      const jsDate: Date = new Date(date.year, date.month - 1, date.day, hours);

      const hasTime = isNaN(jsDate.getTime());

      if (!hasTime) {
        jsDate.setFullYear(date.year);
      }

      return jsDate;
    }

    const fromJSDate = (jsDate: Date) => {
      return new NgbDate(jsDate.getFullYear(), jsDate.getMonth() + 1, jsDate.getDate());
    }

    let jsDate: Date = toJSDate(date);

    switch (period) {
      case 'y':
        return new NgbDate(date.year + number, 1, 1);
      case 'm':
        const hours: number = 12;
        const day: number = 1;
        jsDate = new Date(date.year, date.month + number - 1, day, hours);
        break;
      case 'd':
        jsDate.setDate(jsDate.getDate() + number);
        break;
      default:
        return date;
    }

    return fromJSDate(jsDate);
  };
}

describe('DateRangeComponent', () => {
  let component: DateRangeComponent;
  let fixture: ComponentFixture<DateRangeComponent>;
  let calendar: NgbCalendarMock;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbDatepickerModule,
        OverlayModule,
        MatCardModule
      ],
      declarations: [
        DurationPipe,
        DateRangeComponent
      ],
      providers: [
        {
          provide: NgbCalendar,
          useClass: NgbCalendarMock
        }
      ]
    });

    calendar = TestBed.inject(NgbCalendar);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateRangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#fromDateFormatted and #toDateFormatted should be formatted dates', () => {
    const today: NgbDate = calendar.getToday();
    const date: string = '11 Aug 2018';

    const setDates: Function = component['setDates'].bind(component);

    setDates(today, today);

    expect(component.fromDateFormatted).toBe(date, 'from date is formatted');
    expect(component.toDateFormatted).toBe(date, 'to date is formatted');
  });

  it('#onButtonClick() should toggle #isOpen', () => {
    expect(component.isOpen).toBe(false, 'closed at first');
    component.onButtonClick();
    expect(component.isOpen).toBe(true, 'opened after button click');
    component.onButtonClick();
    expect(component.isOpen).toBe(false, 'closed after second button click');
  });

  it('#onBackdropClick() should set #isOpen to closed state', () => {
    expect(component.isOpen).toBe(false, 'closed at first');
    component.onButtonClick();
    expect(component.isOpen).toBe(true, 'opened after button click');
    component.onBackdropClick();
    expect(component.isOpen).toBe(false, 'closed after backdrop click');
  });

  it('#deserializeDates should set valid #_fromDate and #_toDate for custom range', () => {
    const fromDate: NgbDateStruct = { year: 2018, month: 9, day: 21 };
    const toDate: NgbDateStruct = { year: 2018, month: 10, day: 2 };

    component['_period'] = undefined;
    component['_fromDate'] = fromDate;
    component['_toDate'] = toDate;

    const serializedRange: Range = component['serializedRange'];
    const customRange: Range = serializedRange;

    const deserializeDates: Function = component['deserializeDates'].bind(component);

    component['range'] = customRange;

    deserializeDates();

    const { year: fromYear, month: fromMonth, day: fromDay } = component['_fromDate'];
    const { year: toYear, month: toMonth, day: toDay } = component['_toDate'];

    expect(fromYear).toBe(fromDate.year, 'valid from year after custom range deserialization');
    expect(toYear).toBe(toDate.year, 'valid to year after custom range deserialization');

    expect(fromMonth).toBe(fromDate.month, 'valid from month after custom range deserialization');
    expect(toMonth).toBe(toDate.month, 'valid to month after custom range deserialization');

    expect(fromDay).toBe(fromDate.day, 'valid from day after custom range deserialization');
    expect(toDay).toBe(toDate.day, 'valid to day after custom range deserialization');
  });

  it('#deserializeDates should set valid #_fromDate and #_toDate for week period', () => {
    const weekPeriodRange: Range = { start: 1538520831, duration: Period.Week, period: true };

    const period: number = weekPeriodRange.duration;

    const today = calendar.getToday();
    let fromDate;

    const isDays: boolean = period > Period.Day;

    if (isDays) {
      const daysAgo: number = period / Period.Day - 1;

      fromDate = calendar.getPrev(today, 'd', daysAgo);
    } else {
      fromDate = today;
    }

    const deserializeDates: Function = component['deserializeDates'].bind(component);

    component['range'] = weekPeriodRange;

    deserializeDates();

    const { year: fromYear, month: fromMonth, day: fromDay } = component['_fromDate'];
    const { year: toYear, month: toMonth, day: toDay } = component['_toDate'];

    expect(fromYear).toBe(fromDate.year, 'valid from year after week period deserialization');
    expect(toYear).toBe(today.year, 'valid to year after week period deserialization');

    expect(fromMonth).toBe(fromDate.month, 'valid from month after week period deserialization');
    expect(toMonth).toBe(today.month, 'valid to month after week period deserialization');

    expect(fromDay).toBe(fromDate.day, 'valid from day after week period deserialization');
    expect(toDay).toBe(today.day, 'valid to day after week period deserialization');
  });

  it(`#timeOffset should return proper offsets`, () => {
    let timeOffset: Period | number = component['timeOffset'];

    expect(timeOffset).toBe(Period.SixHours, 'valid timeOffset for default period');

    component['_period'] = (Period.FifteenMinutes as Period);

    timeOffset = component['timeOffset'];

    expect(timeOffset).toBe(Period.FifteenMinutes, 'valid timeOffset after set fifteen minutes period');

    component['_period'] = (Period.SixHours as Period);

    timeOffset = component['timeOffset'];

    expect(timeOffset).toBe(Period.SixHours, 'valid timeOffset after set six hours period');

    component['_period'] = (Period.Week as Period);

    timeOffset = component['timeOffset'];

    expect(timeOffset).toBe(Period.Day, 'valid timeOffset after set week period');
  });

  it('#serializeDates should return valid start date and duration', () => {
    component['_period'] = Period.Day;
    const timeOffset: Period | number = component['timeOffset'];

    const fromDate: NgbDate = { year: 2018, month: 8, day: 11 };
    const toDate: NgbDate = { year: 2018, month: 9, day: 1 };

    const dates: NgbDate[] = [fromDate, toDate];
    const now: Date = new Date;
    const pad = (unit: number, digits: number = 2): string =>
      unit
        .toString()
        .padStart(digits, '0');

    const unixDates: number[] = dates.map((date, isToDate) => {
      const month: string = pad(date.month);
      const day: string = pad(date.day);
      const hours = isToDate ? '23' : '00';
      const minutes = isToDate ? '59' : '00';
      const seconds = isToDate ? '59' : '00';

      return moment(`${date.year}-${month}-${day}T${hours}:${minutes}:${seconds}`, moment.ISO_8601, true).unix();
    });

    const [startDate, endDate]: number[] = unixDates;

    const start: number = startDate;
    const duration: Period | number = endDate - start;

    component['_fromDate'] = fromDate;
    component['_toDate'] = toDate;

    const range: Range = component['serializedRange'];

    expect(range.start).toBe(start, 'valid start date after serialization');
    expect(range.duration).toBe(duration, 'valid duration after serialization');
  });

  it('should display the proper date range', () => {
    const start = 231413400; // 05/02/1977 @ 9:30am (UTC)
    const duration = 5400; // seconds
    const range: Range = { start, duration, period: false };
    component.range = range;

    fixture.detectChanges();

    const dates = fixture.debugElement.nativeNode.querySelectorAll('span .date');
    const displayedStart = Date.parse(dates[0].textContent) / 1000;
    const displayedEnd = Date.parse(dates[1].textContent) / 1000;

    expect(start).toBe(displayedStart);
    expect(duration).toBe(displayedEnd - displayedStart);
  });
});
