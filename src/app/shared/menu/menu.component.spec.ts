import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { MenuComponent } from './menu.component';
import { MatCardModule } from '@angular/material/card';

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        MatCardModule
      ],
      declarations: [
        MenuComponent
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display label', () => {
    component.label = 'Filters';
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('label').childNodes[0].nodeValue).toBe('Filters');
  });

  it('should display icons', () => {
    const icon = 'http://localhost/default-icon.svg';
    const activeIcon = 'http://localhost/active-icon.svg';

    component.icon = icon;
    component.activeIcon = activeIcon;
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('img').src).toBe(icon);

    component.isHovered = true;
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelector('img').src).toBe(activeIcon);
  });

  it('should properly show and hide menu', () => {
    // by default, the menu is not displayed
    expect(document.querySelectorAll('mat-card').length).toBe(0);

    // menu should now be displayed when clicking on button
    component.buttonClick();
    fixture.detectChanges();
    expect(document.querySelectorAll('mat-card').length).toBe(1);

    // menu should be dismissed when click on backdrop
    component.onBackdropClick();
    fixture.detectChanges();
    expect(document.querySelectorAll('mat-card').length).toBe(0);
  });
});
