import { Component, Input } from '@angular/core';
import { ConnectedPosition } from '@angular/cdk/overlay';

@Component({
  selector: 'nef-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})
export class MenuComponent {
  @Input() public label: string;
  @Input() public icon: string;
  @Input() public activeIcon: string;
  @Input() public disabled = false;

  private _isOpen = false;
  private _connectedPositions: ConnectedPosition[] = [
    {
      originX: 'center',
      originY: 'bottom',
      overlayX: 'center',
      overlayY: 'top',
      offsetY: 20
    }
  ];
  private _isHovered = false;

  constructor() { }

  public buttonClick() {
    this.isOpen = !this.isOpen;
  }

  public onBackdropClick() {
    this.isOpen = false;
  }

  public get isOpen(): boolean {
    return this._isOpen;
  }

  public set isOpen(val: boolean) {
    this._isOpen = val;
  }

  public get connectedPositions(): ConnectedPosition[] {
    return this._connectedPositions;
  }

  public get isHovered(): boolean {
    return this._isHovered;
  }

  public set isHovered(val: boolean) {
    this._isHovered = val;
  }

  public close(): void {
    this.isOpen = false;
  }
}
