/**
 * Status List component
 *
 * Usage:
 * <nef-status-list title="Title" [items]="someItems" (_handleClick)="handleClick($event)"></nef-status-list>
 */

import { Observable } from 'rxjs';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../../users/user.service';

export enum Status {
  GOOD = 'GOOD',
  WARNING = 'WARNING',
  CRITICAL = 'CRITICAL'
}

export interface Item {
  id: string;
  label: string;
  status: Status;
  progress?: number;
  link?: string | string[];
}

@Component({
  selector: 'nef-status-list',
  templateUrl: './status-list.component.html',
  styleUrls: ['./status-list.component.less']
})
export class StatusListComponent {
  private _title: string;
  private _items: Item[];
  private _showAddButton = false;
  private _handleClick: EventEmitter<Item> = new EventEmitter();
  private _handleAddClick: EventEmitter<undefined> = new EventEmitter();

  constructor(
    private _userService: UserService
  ) { }

  @Input('title')
  public set title(value: string) {
    this._title = value;
  }

  @Input('items')
  public set items(items: Item[]) {
    this._items = items;
  }

  @Input('add')
  public set add(add: boolean) {
    this._showAddButton = add;
  }

  @Output()
  public get handleClick(): EventEmitter<Item> {
    return this._handleClick;
  }

  @Output()
  public get handleAddClick(): EventEmitter<undefined> {
    return this._handleAddClick;
  }

  public get title() {
    return this._title;
  }

  public get items(): Item[] {
    return this._items;
  }

  public itemClick(item: Item): void {
    this._handleClick.emit(item);
  }

  public get showAddButton(): boolean {
    return this._showAddButton;
  }

  public onAddClick(): void {
    this._handleAddClick.emit();
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
