import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StatusListComponent, Item, Status } from './status-list.component';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { RouterTestingModule } from '@angular/router/testing';
import { UserService } from '../../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';

enum UserType {
  SystemAdmin,
  SystemUser
}

const mockUsers: User[] = [
  {
    username: 'SystemAdmin',
    email: 'admin@nefeli.io',
    roles: {
      scope: 'system',
      id: 'Nefeli Networks',
      roles: ['admin', 'user']
    }
  },
  {
    username: 'SystemUser',
    email: 'user@nefeli.io',
    roles: {
      scope: 'system',
      id: 'Nefeli Networks',
      roles: ['user']
    }
  }
];

describe('StatusListComponent', () => {
  let component: StatusListComponent;
  let fixture: ComponentFixture<StatusListComponent>;
  let el: HTMLElement;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatCardModule,
        MatDividerModule,
        MatListModule,
        MatProgressBarModule,
        RouterTestingModule
      ],
      declarations: [StatusListComponent],
      providers: [
        UserService
      ]
    });

    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusListComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display title', () => {
    component.title = 'Servers';
    fixture.detectChanges();
    expect(el.querySelector('mat-card-header').textContent).toBe('Servers');
  });

  it('should render empty list', () => {
    component.items = [];
    fixture.detectChanges();
    expect(el.querySelector('.status-list')).toBeTruthy();
    expect(el.querySelectorAll('mat-nav-list').length).toBe(0);
    expect(el.querySelectorAll('[mat-list-item]').length).toBe(0);
    expect(el.querySelectorAll('.empty-placeholder').length).toBe(1);
  });

  it('should render item list', () => {
    component.items = [
      { id: '-1', label: 'Server 1', status: Status.GOOD, progress: 80 },
      { id: '-2', label: 'Server 2', status: Status.WARNING, progress: 50 },
      { id: '-3', label: 'Server 3', status: Status.CRITICAL, progress: 10 },
      { id: '-4', label: 'Server 4', status: Status.GOOD, progress: 90 }
    ];
    fixture.detectChanges();
    expect(el.querySelectorAll('[mat-list-item]').length).toBe(4);
    expect(el.querySelectorAll('.empty-placeholder').length).toBe(0);
  });

  it('should display items status correctly', () => {
    component.items = [{ id: '-1', label: 'Server 1', status: Status.GOOD, progress: 80 }];
    fixture.detectChanges();
    expect(el.querySelectorAll('[mat-list-item] .status-indicator')[0].className).toContain('good');
  });

  it('should render progress bar', () => {
    component.items = [{ id: '-1', label: 'Server 1', status: Status.GOOD, progress: 80 }];
    fixture.detectChanges();
    expect(el.querySelectorAll('[mat-list-item] .mat-progress-bar').length).toBe(1);
  });

  it('should not render progress bar if item has no progress field', () => {
    component.items = [{ id: '-1', label: 'Server 1', status: Status.GOOD }];
    fixture.detectChanges();
    expect(el.querySelectorAll('[mat-list-item] .mat-progress-bar').length).toBe(0);
  });

  it('should emit event on click', () => {
    spyOn(component['_handleClick'], 'emit');
    const item = <Item> { label: 'Server 1', status: Status.GOOD };
    component.items = [item];
    fixture.detectChanges();
    const rowElem: HTMLAnchorElement = el.querySelector('[mat-list-item]');
    rowElem.click();

    expect(component['_handleClick'].emit).toHaveBeenCalledWith(item);
  });

  it('should render Add Server button only for System Admin', () => {
    component.add = true;
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();
    let addButton = el.querySelector('button');
    expect(addButton).not.toBeNull();

    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    addButton = el.querySelector('button');
    expect(addButton).toBeNull();
  });
});
