import { TestBed, inject } from '@angular/core/testing';

import { LocalStorageService, LocalStorageKey } from './local-storage.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('LocalStorageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [LocalStorageService]
    });
  });

  it('should be created', inject([LocalStorageService], (service: LocalStorageService) => {
    expect(service).toBeTruthy();
  }));

  it('should write to local storage', inject([LocalStorageService], (service: LocalStorageService) => {
    const testData = 'Hello World';
    service.write(LocalStorageKey.PERFORMANCE_CHART_DATE_RANGE, testData);
    const storedData = service.read(LocalStorageKey.PERFORMANCE_CHART_DATE_RANGE);

    expect(storedData).toBe(testData);
  }));

  it('should clean local storage from user specific data', inject([LocalStorageService], (service: LocalStorageService) => {

    service.write(LocalStorageKey.ACCESS_TOKEN, 'ACCESS_TOKEN');
    service.write(LocalStorageKey.USER_NAME, 'USER_NAME');
    service.write(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS, 'VIEW_TENANT_NOTIFICATIONS');
    service.write(LocalStorageKey.IS_SAAS, 'IS_SAAS');

    service.cleanUserSession();

    expect(service.read(LocalStorageKey.ACCESS_TOKEN)).toBeFalsy();
    expect(service.read(LocalStorageKey.USER_NAME)).toBeFalsy();
    expect(service.read(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS)).toBeFalsy();
    expect(service.read(LocalStorageKey.IS_SAAS)).toBeFalsy();
  }));
});
