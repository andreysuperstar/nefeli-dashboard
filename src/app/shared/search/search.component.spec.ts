import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement, EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';

import { SearchComponent } from './search.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;

  const mockChips: string[] = ['tenant', 'admin'];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatFormFieldModule,
        MatChipsModule,
        MatIconModule,
        MatProgressSpinnerModule
      ],
      declarations: [SearchComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render search field', () => {
    const searchFormFieldDe: DebugElement = fixture.debugElement.query(
      By.css('mat-form-field')
    );

    expect(searchFormFieldDe).not.toBeNull('render search field');

    const searchIconDe: DebugElement = searchFormFieldDe.query(
      By.css('mat-icon[matPrefix]'
      ));
    const searchChipListDe: DebugElement = searchFormFieldDe.query(By.css('mat-chip-list'));
    const queryInputDe: DebugElement = searchFormFieldDe.query(By.css('input'));

    expect(searchIconDe).not.toBeNull('render search icon');
    expect(searchChipListDe).not.toBeNull('render chip list');
    expect(queryInputDe).not.toBeNull('render query input');
  });

  it('should add chips', () => {
    const searchInputDe: DebugElement = fixture.debugElement.query(By.css('input'));
    const searchInputEl: HTMLInputElement = searchInputDe.nativeElement;

    spyOn(component, 'onAdd').and.callThrough();

    let query: string = 'Tenant';

    searchInputEl.value = query;
    searchInputEl.dispatchEvent(new Event('input'));

    const chipInputEvent: MatChipInputEvent = {
      input: searchInputEl,
      value: query
    };

    // add a chip
    searchInputDe.triggerEventHandler('matChipInputTokenEnd', chipInputEvent);
    fixture.detectChanges();

    expect(component.onAdd).toHaveBeenCalledWith(chipInputEvent);

    let chipDes: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes[0].nativeElement.textContent).toContain(
      query.toLowerCase(), `render ${query.toLowerCase()} chip`
    );

    query = 'Admin';

    searchInputEl.value = query;
    searchInputEl.dispatchEvent(new Event('input'));

    chipInputEvent.value = query;

    // add another chip
    searchInputDe.triggerEventHandler('matChipInputTokenEnd', chipInputEvent);
    fixture.detectChanges();

    expect(component.onAdd).toHaveBeenCalledWith(chipInputEvent);

    chipDes = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes[1].nativeElement.textContent).toContain(
      query.toLowerCase(), `render ${query.toLowerCase()} chip`
    );
    expect(chipDes.length).toBe(2, 'render 2 chips');
  });

  it('should remove chips', () => {
    component.chips = [...mockChips];
    fixture.detectChanges();

    spyOn(component, 'onRemove').and.callThrough();

    let chipDes: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-chip'));
    let removeIconEl: HTMLUnknownElement = chipDes[0].query(
      By.css('mat-icon')
    ).nativeElement;

    // remove the 1st chip
    removeIconEl.click();
    fixture.detectChanges();

    expect(component.onRemove).toHaveBeenCalledWith(mockChips[0]);

    chipDes = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes.length).toBe(1, `render 1 chip`);
    expect(
      chipDes[0].nativeElement.textContent
    ).not.toContain(mockChips[0], `removed ${mockChips[0]} chip`);
    expect(
      chipDes[0].nativeElement.textContent
    ).toContain(mockChips[1], `render shifted ${mockChips[1]} chip`);

    removeIconEl = chipDes[0].query(By.css('mat-icon')).nativeElement;

    // remove a remaining chip
    removeIconEl.click();
    fixture.detectChanges();

    expect(component.onRemove).toHaveBeenCalledWith(mockChips[1]);

    chipDes = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes.length).toBe(0, `render no chips`);
  });

  it('should emit enter event', () => {
    component.chips = [...mockChips];

    const searchInputEl: HTMLInputElement = fixture.debugElement.query(
      By.css('input')
    ).nativeElement;

    const query: string = 'System';

    const chipInputEvent: MatChipInputEvent = {
      input: searchInputEl,
      value: query
    };

    spyOn<EventEmitter<string[]>>(component.enter, 'emit').and.callThrough();

    // add a chips
    component.onAdd(chipInputEvent);

    let mockEditedChips: string[] = [...mockChips, query.toLowerCase()];

    expect(component.enter.emit).toHaveBeenCalledWith(mockEditedChips);

    // remove the 2nd chip
    component.onRemove(mockEditedChips[1]);

    mockEditedChips = [mockEditedChips[0], query.toLowerCase()];

    expect(component.enter.emit).toHaveBeenCalledWith(mockEditedChips);

    // remove the remaining chips
    component.onRemove(mockEditedChips[0]);
    component.onRemove(mockEditedChips[1]);

    expect(component.enter.emit).toHaveBeenCalledWith([]);
  });

  it('should show/hide progress indicator', () => {
    // by default, spinner his hidden
    const el = fixture.debugElement.nativeElement;
    expect(el.querySelector('mat-spinner')).toBeNull();

    // show progress spinner
    component.showProgress = true;
    fixture.detectChanges();
    expect(el.querySelector('mat-spinner')).toBeDefined();

    // hide progress spinner
    component.showProgress = false;
    fixture.detectChanges();
    expect(el.querySelector('mat-spinner')).toBeNull();
  })
});
