import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'nef-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.less']
})
export class SearchComponent {
  private _chips: string[] = [];
  private _enter = new EventEmitter<string[]>();
  private _showProgress = false;

  @Input() public set chips(chips: string[]) {
    this._chips = chips;
  }

  @Input() public set showProgress(show: boolean) {
    this._showProgress = show;
  }

  @Output() public get enter(): EventEmitter<string[]> {
    return this._enter;
  }

  public onAdd({ input, value }: MatChipInputEvent) {
    value = value
      ?.trim?.()
      ?.toLowerCase?.();

    // Add a query
    if (value) {
      const index: number = this._chips.indexOf(value);

      if (index >= 0) {
        this._chips.splice(index, 1);
      }

      this._chips.push(value);

      this._enter.emit(this._chips);
    }

    // Reset the query input value
    if (input) {
      input.value = '';
    }
  }

  public onRemove(chip: string) {
    const index: number = this._chips.indexOf(chip);

    if (index >= 0) {
      this._chips.splice(index, 1);

      this._enter.emit(this._chips);
    }
  }

  public get chips(): string[] {
    return this._chips;
  }

  public get separatorKeyCodes(): number[] {
    return [ENTER, COMMA];
  }

  public get showProgress(): boolean {
    return this._showProgress;
  }
}
