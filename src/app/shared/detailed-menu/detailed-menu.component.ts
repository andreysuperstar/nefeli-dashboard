import { Component, Input } from '@angular/core';

@Component({
  selector: 'nef-detailed-menu',
  templateUrl: './detailed-menu.component.html',
  styleUrls: ['./detailed-menu.component.less'],
})
export class DetailedMenuComponent {
  private _title: string;

  @Input() public set title(title: string) {
    this._title = title;
  }

  public get title(): string {
    return this._title;
  }
}
