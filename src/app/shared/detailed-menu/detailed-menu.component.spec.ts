import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule, MatCardTitle, MatCardContent } from '@angular/material/card';

import { DetailedMenuComponent } from './detailed-menu.component';

const mockContent = 'Detailed Menu Items';

@Component({
  selector: 'test',
  template: `<nef-detailed-menu>${mockContent}</nef-detailed-menu>`,
})
class TestComponent { }

describe('DetailedMenuComponent', () => {
  let component: DetailedMenuComponent;
  let fixture: ComponentFixture<DetailedMenuComponent>;

  const mockTitle = 'Settings';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatCardModule],
      declarations: [
        TestComponent,
        DetailedMenuComponent
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedMenuComponent);
    component = fixture.componentInstance;

    component.title = mockTitle;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    expect(component.title).toBe(mockTitle, 'set title');

    const titleDe = fixture.debugElement.query(By.directive(MatCardTitle));

    expect(titleDe.nativeElement.textContent).toBe(mockTitle, 'render title');
  });

  it('should render content', () => {
    const fixture = TestBed.createComponent(TestComponent);

    const cardContentDe = fixture.debugElement.query(By.directive(MatCardContent));

    expect(cardContentDe.nativeElement.textContent).toBe(mockContent, 'render content');
  });
});
