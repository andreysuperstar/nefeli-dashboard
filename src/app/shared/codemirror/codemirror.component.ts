import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  NgZone,
  OnDestroy,
  Optional,
  Output,
  Renderer2,
  Self,
  ViewChild
} from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';
import { Doc, Editor, EditorChange, EditorConfiguration, EditorFromTextArea, ScrollInfo } from 'codemirror';
import { Subject } from 'rxjs';
import { DocChangeEvent, getCodeMirrorGlobal, OptionName, OptionValue } from '../../utils/codemirror';

@Component({
  selector: 'nef-codemirror',
  templateUrl: './codemirror.component.html',
  styleUrls: ['./codemirror.component.less'],
  providers: [
    {
      provide: MatFormFieldControl,
      useExisting: CodemirrorComponent
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodemirrorComponent implements AfterViewInit, OnDestroy, ControlValueAccessor, MatFormFieldControl<string> {

  private static materialId = 0;

  private _required = false;
  private _focused = false;
  private _disabled = false;
  private _placeholder: string;
  private _value = '';

  /* class applied to the created textarea */
  private _className = '';
  /* name applied to the created textarea */
  private _name = 'codemirror';
  /* autofocus setting applied to the created textarea */
  private _autoFocus = false;
  /* preserve previous scroll position after updating value */
  private _preserveScrollPosition = false;

  private _options: EditorConfiguration;
  private _codeMirror: EditorFromTextArea;

  private _id = `mat-codemirror-component-${CodemirrorComponent.materialId++}`;
  private _stateChanges = new Subject<void>();

  @HostBinding('attr.aria-describedby') private _describedBy = '';

  @HostBinding('class.floating')
  public get shouldLabelFloat(): boolean {
    return this._focused || !this.empty;
  }

  @HostBinding()
  public get id(): string {
    return this._id;
  }

  /* called when the text cursor is moved */
  private _cursorActivity = new EventEmitter<Editor>();
  /* called when the editor is focused or loses focus */
  private _focusChange = new EventEmitter<boolean>();
  /* called when the editor is scrolled */
  private _scroll = new EventEmitter<ScrollInfo>();
  /* called when the document is changed */
  private _docChange = new EventEmitter<DocChangeEvent>();

  @Input()
  public set className(value: string) {
    this._className = value;
  }

  @Input()
  public set name(value: string) {
    this._name = value;
  }

  @Input()
  public set autoFocus(value: boolean) {
    this._autoFocus = value;
  }

  @Input()
  public get required(): boolean {
    return this._required;
  }

  @Input()
  public set preserveScrollPosition(value: boolean) {
    this._preserveScrollPosition = value;
  }

  @Input()
  public set placeholder(plh: string) {
    this._placeholder = plh;
    this._stateChanges.next();
  }

  @Input()
  public set disabled(dis: boolean) {
    this._disabled = coerceBooleanProperty(dis);
    this._stateChanges.next();
  }

  /**
   * set options for codemirror
   * @link http://codemirror.net/doc/manual.html#config
   */
  @Input()
  public set options(value: EditorConfiguration) {
    this._options = value;
  }

  @Input()
  public set value(value: string) {
    this.updateValueAndSignalChanges(value);
    this._codeMirror?.setValue(value);
  }

  @Output()
  public get cursorActivity(): EventEmitter<Editor> {
    return this._cursorActivity;
  }

  @Output()
  public get focusChange(): EventEmitter<boolean> {
    return this._focusChange;
  }

  @Output()
  public get scroll(): EventEmitter<ScrollInfo> {
    return this._scroll;
  }

  @Output()
  public get docChange(): EventEmitter<DocChangeEvent> {
    return this._docChange;
  }

  @ViewChild('ref', { static: true }) private _ref: ElementRef;

  private onChange = (_: string) => undefined;
  private onTouched = () => undefined;

  constructor(
    @Optional() @Self() private _ngControl: NgControl, // mat
    private _ngZone: NgZone,
    private _renderer: Renderer2
  ) {
    if (this._ngControl) {
      this._ngControl.valueAccessor = this;
    }
  }

  public get ngControl(): NgControl {
    return this._ngControl;
  }

  public get className(): string {
    return this._className;
  }

  public get name(): string {
    return this._name;
  }

  public get autoFocus(): boolean {
    return this._autoFocus;
  }

  public get errorState(): boolean {
    return this.ngControl && !this.ngControl.pristine && !this.ngControl.valid;
  }

  public get preserveScrollPosition(): boolean {
    return this._preserveScrollPosition;
  }

  public get placeholder(): string {
    return this._placeholder;
  }

  public get empty(): boolean {
    return !this.value;
  }

  public get value(): string {
    return this._value;
  }

  public get disabled(): boolean {
    return this._disabled;
  }

  public get stateChanges(): Subject<void> {
    return this._stateChanges;
  }

  public setDescribedByIds(ids: string[]) {
    this._describedBy = ids.join(' ');
  }

  public onContainerClick(event: MouseEvent) {
    // abstract method defined in the implemented class MatFormFieldControl
    // for now, do nothing when the container is clicked
  }

  public set required(req: boolean) {
    this._required = coerceBooleanProperty(req);
    this._stateChanges.next();
  }

  public get focused(): boolean {
    return this._focused;
  }

  public set focused(val: boolean) {
    this._focused = val;
    this._stateChanges.next();
  }

  public get codeMirror(): EditorFromTextArea {
    return this._codeMirror;
  }

  public ngAfterViewInit() {
    if (!this._ref) {
      return;
    }

    this._ngZone.runOutsideAngular(() => {
      this._codeMirror = (this._codeMirror ? this._codeMirror : getCodeMirrorGlobal()).fromTextArea(this._ref.nativeElement, this._options);
      this._codeMirror.on('change', this.codemirrorValueChanged.bind(this));
      this._codeMirror.on('cursorActivity', this.cursorActive.bind(this));
      this._codeMirror.on('focus', this.focusChanged.bind(this, true));
      this._codeMirror.on('blur', this.focusChanged.bind(this, false));
      this._codeMirror.on('scroll', this.scrollChanged.bind(this));
      this._codeMirror.getDoc().on('change', this.documentChange.bind(this));
    });

    if (this.value) {
      this._codeMirror.setValue(this.value);
    }
  }

  public ngOnDestroy() {
    // is there a lighter-weight way to remove the cm instance?
    if (this._codeMirror) {
      this._codeMirror.toTextArea();
    }
    this._stateChanges.complete();
  }

  public codemirrorValueChanged(cm: Editor, change: EditorChange) {
    if (change.origin !== 'setValue') {
      this.updateValueAndSignalChanges(cm.getValue());
      this.writeValue(cm.getValue());
    }
  }

  private updateValueAndSignalChanges(value: string) {
    this._value = value;
    this._stateChanges.next();
  }

  private focusChanged(focused: boolean) {
    this.onTouched();
    this._focused = focused;
    this._focusChange.emit(focused);
  }

  private scrollChanged(cm: Editor) {
    this._scroll.emit(cm.getScrollInfo());
  }

  private cursorActive(cm: Editor) {
    this._cursorActivity.emit(cm);
  }

  private documentChange(doc: Doc, change: EditorChange) {
    this._docChange.emit({ doc, change });
  }

  public writeValue(value: string): void {
    if (value === null) {
      return;
    }
    if (value && !this._codeMirror) {
      this.updateValueAndSignalChanges(value);
      return;
    }
    if (
      value &&
      value !== this._codeMirror.getValue() &&
      this.normalizeLineEndings(this._codeMirror.getValue()) !==
      this.normalizeLineEndings(value)
    ) {
      this.updateValueAndSignalChanges(value);
      if (this._preserveScrollPosition) {
        const prevScrollPosition = this._codeMirror.getScrollInfo();
        this._codeMirror.setValue(this.value);
        this._codeMirror.scrollTo(
          prevScrollPosition.left,
          prevScrollPosition.top,
        );
        return;
      }
      this._codeMirror.setValue(this.value);
      // Don't call onChange value is from ngModel
      return;
    }
    this.onChange(this.value);
  }

  public registerOnChange(fn: (value: string) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.setOptionIfChanged('readOnly', this.disabled);
  }

  private normalizeLineEndings(str: string): string {
    return (str || '').replace(/\r\n|\r/g, '\n');
  }

  private setOptionIfChanged(option: OptionName, value: OptionValue) {
    if (!this._codeMirror) {
      return;
    }
    this._codeMirror.setOption(option, value);
  }
}
