import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CodemirrorComponent } from './codemirror.component';

const placeholder = 'Test Code';
const options = {
  lineNumbers: false,
  mode: 'javascript',
  theme: 'idea'
};
const focusHandler = jasmine.createSpy('focusChange');
const scrollHandler = jasmine.createSpy('scroll');
const docChangeHandler = jasmine.createSpy('docChange');
const cursorActivityHandler = jasmine.createSpy('cursorActivity');
const onChange = jasmine.createSpy('onChange');

describe('CodemirrorComponent', () => {
  let component: CodemirrorComponent;
  let fixture: ComponentFixture<CodemirrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CodemirrorComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    focusHandler.calls.reset();
    scrollHandler.calls.reset();
    docChangeHandler.calls.reset();
    onChange.calls.reset();
    fixture = TestBed.createComponent(CodemirrorComponent);
    component = fixture.componentInstance;
    component.placeholder = placeholder;
    component.options = options;
    component.focusChange.subscribe(focusHandler);
    component.scroll.subscribe(scrollHandler);
    component.docChange.subscribe(docChangeHandler);
    component.cursorActivity.subscribe(cursorActivityHandler);
    component.registerOnChange(onChange);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.ngControl).toBeNull();
  });

  it('should set properties correctly', () => {
    const name = 'test-codemirror';
    const className = 'testClass';
    const autoFocus = true;
    const disabled = true;
    const required = true;
    const preserveScrollPosition = true;
    const focused = true;

    component.name = name;
    component.className = className;
    component.autoFocus = autoFocus;
    component.disabled = disabled;
    component.required = required;
    component.preserveScrollPosition = preserveScrollPosition;
    component.focused = focused;

    fixture.detectChanges();

    expect(component.name).toBe(name);
    expect(component.className).toBe(className);
    expect(component.autoFocus).toBe(autoFocus);
    expect(component.disabled).toBe(disabled);
    expect(component.required).toBe(required);
    expect(component.preserveScrollPosition).toBe(preserveScrollPosition);
    expect(component.focused).toBe(focused);
    expect(component.codeMirror.getOption('lineNumbers')).toBe(options.lineNumbers);
    expect(component.codeMirror.getOption('mode')).toBe(options.mode);
    expect(component.codeMirror.getOption('theme')).toBe(options.theme);
  });

  it('should ignore null in writeValue call', () => {
    component.writeValue(null);
    fixture.detectChanges();
    expect(onChange).not.toHaveBeenCalled();
  });

  it('should change codemirror value on writeValue call', () => {
    const value = 'test value';
    component.writeValue(value);
    fixture.detectChanges();
    expect(component.codeMirror.getValue()).toBe(value);
    expect(onChange).not.toHaveBeenCalled();
  });

  it('should call  onChange callback on writeValue call when codemirro value is not changed', () => {
    const value = 'test value';
    component.codeMirror.setValue(value);
    component.writeValue(value);
    fixture.detectChanges();
    expect(component.codeMirror.getValue()).toBe(value);
    expect(onChange).toHaveBeenCalledWith('');
  });

  it('should set readOnly properties on setDisabledState  call', () => {
    component.setDisabledState(true);
    fixture.detectChanges();
    expect(component.disabled).toBe(true);
    expect(component.codeMirror.getOption('readOnly')).toBe(true);
  });

  it('should display placeholder', () => {
    const { debugElement } = fixture;
    const textAreaEl = debugElement.query(By.css('textarea'));
    expect(textAreaEl).toBeTruthy();
    expect(textAreaEl.nativeElement.getAttribute('placeholder')).toBe(placeholder);
    expect(debugElement.nativeElement.querySelector('.CodeMirror-placeholder').textContent.trim()).toBe(placeholder);
  });

  it('should emit focus', () => {
    component.codeMirror.focus();
    component.codeMirror.getInputField().dispatchEvent(new Event('focus'));
    fixture.detectChanges();

    expect(focusHandler).toHaveBeenCalledWith(true);
  });

  it('should emit blur', () => {
    component.codeMirror.focus();
    component.codeMirror.getInputField().dispatchEvent(new Event('focus'));
    fixture.detectChanges();
    component.codeMirror.getInputField().blur();
    component.codeMirror.getInputField().dispatchEvent(new Event('blur'));
    fixture.detectChanges();

    expect(focusHandler).toHaveBeenCalledTimes(2);
    const callInfos = focusHandler.calls.all();
    expect(callInfos[0].args[0]).toBe(true);
    expect(callInfos[1].args[0]).toBe(false);
  });

  it('should emit scroll', () => {
    fixture.debugElement.nativeElement.querySelector('.CodeMirror-scroll').dispatchEvent(new CustomEvent('scroll'));
    expect(scrollHandler).toHaveBeenCalled();
  });

  it('should emit docChange', () => {
    component.codeMirror.setValue('test\n'.repeat(100));
    expect(docChangeHandler).toHaveBeenCalled();
  });

  it('should not emit docChange when the only text differences are line endings', () => {
    component.codeMirror.setValue('test\n'.repeat(100));
    component.writeValue('test\r'.repeat(100));
    component.writeValue('test\r\n'.repeat(100));
    component.writeValue('test\n\n'.repeat(100));
    expect(docChangeHandler).toHaveBeenCalledTimes(2);
  });

});
