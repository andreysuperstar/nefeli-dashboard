import { LegendItem } from '../legend/legend.component';
import { Component, OnInit, ElementRef, ViewChild, Input, OnDestroy, EventEmitter, Output, HostListener, AfterViewInit } from '@angular/core';
import { Chart, ChartPoint, ChartTooltipItem, ChartData, ChartTooltipModel } from 'chart.js';
import * as moment from 'moment';
import { ConverterService } from '../converter.service';
import { PipelineNamePipe } from '../../pipes/pipeline-name.pipe';
import 'chartjs-plugin-crosshair';
import { Moment } from 'moment';

export interface ChartLineColor {
  name: string;
  code: string;
}

export const ChartLineColors: ChartLineColor[] = [
  {
    name: 'bright-blue',
    code: '#5959ff'
  },
  {
    name: 'orange',
    code: '#ffa750'
  },
  {
    name: 'light-blue',
    code: '#67d7ff'
  },
  {
    name: 'light-pink',
    code: '#f58ab4'
  },
  {
    name: 'acid-green',
    code: '#63e0ae'
  },
  {
    name: 'purple',
    code: '#a366ff'
  },
  {
    name: 'soft-blue',
    code: '#4093ff'
  },
  {
    name: 'gold',
    code: '#eccc68'
  }
];

const Config = {
  timeFractionalLength: 3,
  numXTicks: 7,
  fontFamily: 'Roboto, Helvetica Neue, sans-serif',
  defaultFontColor: '#9AA7B8',
  chartPadding: {
    top: 75,
    bottom: 15,
    left: 30,
    right: 30,
  },
  chartLineColors: ChartLineColors,
  lineWidth: 2,
  pointRadius: 0,
  fill: false,
  crosshair: {
    sync: {
      enabled: true,
      group: 1,
      suppressTooltips: false
    },
    zoom: {
      enabled: true,
      zoomboxBackgroundColor: 'rgba(66,133,244,0.2)',
      zoomboxBorderColor: '#48F',
      zoomButtonText: 'Reset Zoom',
      zoomButtonClass: 'reset-zoom'
    }
  },
  tooltip: {
    maxItems: 8
  }
};

const SecToMsecFactor = 1000;

export interface ChartDataSet {
  label?: string;
  borderColor?: string;
  data?: ChartPoint[];
  fill?: boolean;
  borderWidth?: number;
  pointRadius?: number;
}

export enum UnitType {
  Time,
  PacketRate,
  BitRate
}

export interface ZoomEvent {
  start: Moment;
  end: Moment;
}

@Component({
  selector: 'nef-chart-line',
  templateUrl: './chart-line.component.html',
  styleUrls: ['./chart-line.component.less']
})
export class ChartLineComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('chartCanvas') public chartCanvas: ElementRef;

  @Input() public set dataSets(sets: ChartDataSet[]) {
    this.initDataSets(sets);
  }

  @Input() public set legendItems(items: LegendItem[]) {
    this._legendItems = items;
    this.updateChartLineColors();
    this.updateXAxes();
  }

  @Input() public set displayGrid(show: boolean) {
    this._displayGrid = show;
    this._addPadding = show;
  }

  @Input('enableTooltips') public set enableTooltips(enable: boolean) {
    this._enableTooltips = enable;
  }

  @Input('enableCrosshair') public set enableCrosshair(enable: boolean) {
    this._enableCrosshair = enable;
  }

  @Input() public set yAxesUnits(type: UnitType) {
    this._yAxesUnits = type;
  }

  @Output() public get beforeZoom(): EventEmitter<ZoomEvent> {
    return this._beforeZoom;
  }

  @Output() public get afterZoom(): EventEmitter<ZoomEvent> {
    return this._afterZoom;
  }

  private _enableTooltips = true;
  private _enableCrosshair = true;
  private _chart: Chart;
  private _chartType: Chart.ChartType = 'line';
  private _chartConfig: Chart.ChartConfiguration;
  private _chartData: Chart.ChartData;
  private _chartOptions: Chart.ChartOptions;
  private _dataSets: ChartDataSet[];
  private _displayGrid = true;
  private _addPadding = true;
  private _legendItems: LegendItem[];
  private _yAxesUnits: UnitType;
  private _beforeZoom = new EventEmitter<ZoomEvent>();
  private _afterZoom = new EventEmitter<ZoomEvent>();
  private _currentTooltipState = {
    dataSets: new Set<number>(),
    itemCount: 0
  };
  public _testMode = false;

  constructor(
    private _converter: ConverterService,
    private _pipelineNameService: PipelineNamePipe
  ) { }

  private initGlobalDefaults() {
    Chart.defaults.global.defaultFontFamily = Config.fontFamily;
    Chart.defaults.global.defaultFontColor = Config.defaultFontColor;
  }

  private initChartOptions() {
    const layout: Chart.ChartLayoutOptions = {
      padding: this._addPadding ? Config.chartPadding : 0
    };

    const legend: Chart.ChartLegendOptions = {
      display: false
    };

    const xAxes: Chart.ChartXAxe[] = this.getXAxeOptions();

    const yAxes: Chart.ChartYAxe[] = [{
      ticks: {
        display: this._displayGrid,
        maxTicksLimit: 4,
        callback: (value: number) => {
          let val: number;
          let unit: string;
          switch (this._yAxesUnits) {
            case UnitType.PacketRate: {
              [val, unit] = this._converter.packetsPerSeconds(value);
              break;
            }
            case UnitType.BitRate: {
              [val, unit] = this._converter.bitsPerSeconds(value);
              break;
            }
            case UnitType.Time: {
              [val, unit] = this._converter.nanoSeconds(value, Config.timeFractionalLength);
              break;
            }
          }
          return `${val} ${unit}`;
        },
        fontSize: 10,
        padding: 25,
        beginAtZero: true
      },
      position: 'right',
      gridLines: {
        display: this._displayGrid,
        drawTicks: false,
        drawBorder: false
      }
    }];

    const scales: Chart.ChartScales = {
      yAxes: yAxes,
      xAxes: xAxes
    };

    this._chartOptions = {
      layout: layout,
      legend: legend,
      scales: scales,
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        mode: 'x',
        intersect: false,
        enabled: this._enableTooltips,
        callbacks: {
          label: this.renderTooltipLabel.bind(this),
          beforeBody: (item: ChartTooltipItem[], data: ChartData) => {
            item.forEach((i: ChartTooltipItem) => this._currentTooltipState.dataSets.add(i.datasetIndex));
            return undefined;
          },
          afterBody: (item: ChartTooltipItem[], data: ChartData) => {
            const numHidden = data.datasets.length - Config.tooltip.maxItems;
            this._currentTooltipState.dataSets.clear();
            this._currentTooltipState.itemCount = 0;
            return (numHidden > 0) ? `${numHidden} items hidden` : '';
          }
        },
        custom: (model: ChartTooltipModel) => {
          // position tooltip the left or right of the caret
          if (model.xAlign === 'left' || model.xAlign === 'right') {
            return;
          }

          const x = model.x - (model.width / 2) - model.caretSize;
          model.yAlign = 'center';
          model.y = model.y - (model.height / 2);

          if (x < 0) {
            model.xAlign = 'left';
            model.x = model.x + (model.width / 2) + model.caretSize;
          } else {
            model.xAlign = 'right';
            model.x = x;
          }
        }
      },
      animation: {
        duration: 0
      },
      responsiveAnimationDuration: 0,
      elements: {
        line: {
          tension: 0
        }
      },
      plugins: {
        crosshair: this._enableCrosshair ? {
          ...Config.crosshair,
          callbacks: {
            beforeZoom: this.beforeZoomHandler.bind(this),
            afterZoom: this.afterZoomHandler.bind(this)
          }
        } : false
      }
    };
  }

  public ngOnInit() {

    this.initGlobalDefaults();

    this._chartData = {
      datasets: this._dataSets
    };
    this.initChartOptions();

    this._chartConfig = {
      type: this._chartType,
      data: this._chartData,
      options: this._chartOptions
    };
  }

  public ngAfterViewInit() {
    this.initChart();
  }

  public ngOnDestroy() {
    this._chart?.destroy();
  }

  private beforeZoomHandler(start: Moment, end: Moment) {
    this._beforeZoom.emit({ start, end });
    return true;
  }

  private afterZoomHandler(start: Moment, end: Moment) {
    this._afterZoom.emit({ start, end });
  }

  private renderTooltipLabel(tooltipItem: ChartTooltipItem, data: ChartData): string | string[] {

    // only display one item per dataset
    if (!this._currentTooltipState.dataSets.has(tooltipItem.datasetIndex)) {
      return;
    }
    this._currentTooltipState.dataSets.delete(tooltipItem.datasetIndex);

    if (++this._currentTooltipState.itemCount > Config.tooltip.maxItems) {
      return;
    }

    let label = data.datasets[tooltipItem.datasetIndex].label || '';
    let val: number;
    let unit: string;

    switch (this._yAxesUnits) {
      case UnitType.PacketRate: {
        [val, unit] = this._converter.packetsPerSeconds(tooltipItem.yLabel as number);
        break;
      }
      case UnitType.BitRate: {
        label = this._pipelineNameService.transform(label);
        [val, unit] = this._converter.bitsPerSeconds(tooltipItem.yLabel as number);
        break;
      }
      case UnitType.Time: {
        [val, unit] = this._converter.nanoSeconds(tooltipItem.yLabel as number, Config.timeFractionalLength);
        break;
      }
    }
    return `${label} : ${val} ${unit}`;
  }

  private initChart() {
    if (!this._testMode) {
      const ctx = this.chartCanvas.nativeElement.getContext('2d');
      this._chart = new Chart(ctx, this._chartConfig);
    }
  }

  private fitToTimeWindow(set: ChartDataSet, startTime: number, duration: number) {
    let firstPointTime = 0;

    if (set.data.length) {
      firstPointTime = (typeof (set.data[0].x as Date).getTime === 'function') ? (set.data[0].x as Date).getTime() : 0;
    }

    if (!startTime) {
      startTime = new Date().getTime() - (duration * SecToMsecFactor);
    }

    if (!firstPointTime) {
      // empty data set
      set.data.push({
        x: new Date(startTime),
        y: 0
      });

      set.data.push({
        x: new Date(startTime + (duration * SecToMsecFactor)),
        y: 0
      });
    } else if (firstPointTime < startTime) {
      // trim data outside of time window
      let index = 0;
      for (const point of set.data) {
        if ((point.x as Date).getTime() < startTime) {
          index++;
        } else {
          break;
        }
      }

      if (index > 1) {
        // preserve last data point right before startTime, this ensures we
        // never insert a fake zero'd point when there's a valid previous point
        // this may cause an issue if the time in between points are quite large
        set.data.splice(0, index - 1);
      }
    } else {
      firstPointTime = (typeof (set.data[0].x as Date).getTime === 'function') ? (set.data[0].x as Date).getTime() : 0;
      if (firstPointTime > startTime) {
        /* Insert chart points at beginning of dataset to ensure chart always matches
        the desired time duration.
        TODO(eric): Keep an eye on https://github.com/chartjs/Chart.js/issues/5720
        I have submitted a feature request to support this functionality w/in Chart.js */

        set.data.unshift({
          x: new Date(firstPointTime - 1),
          y: 0
        });

        set.data.unshift({
          x: new Date(startTime),
          y: 0
        });
      }
    }
  }

  private checkForDataGaps(set: ChartDataSet, step: number) {
    for (let index = 1; index < set.data.length; index++) {
      const point = set.data[index];
      const prevPoint = set.data[index - 1];

      if (prevPoint) {
        const diffSecs = ((point.x as number) - (prevPoint.x as number)) / SecToMsecFactor;
        if ((diffSecs > step)) {
          // missing data point, insert one at zero
          if (prevPoint.y || point.y) {
            set.data.splice(index, 0, {
              x: new Date(((prevPoint.y ? prevPoint.x : point.x) as Date).getTime()),
              y: 0
            } as ChartPoint);
          }
        }
      }
    }
  }

  public initDataSets(
    sets: ChartDataSet[],
    rate = 0,
    startTime?: number,
    step?: number) {

    sets.forEach((set: ChartDataSet, i: number) => {
      this.updateSetColor(set, i);
      set.borderWidth = Config.lineWidth;
      set.pointRadius = Config.pointRadius;
      set.fill = Config.fill;

      if (rate) {
        this.fitToTimeWindow(set, startTime, rate);
      }

      if (step) {
        this.checkForDataGaps(set, step);
      }
    });

    this._dataSets = sets;
    this.updateXAxes();
  }

  private updateChartLineColors() {
    this._dataSets.forEach((set: ChartDataSet, i: number) => {
      this.updateSetColor(set, i);
    });
  }

  private updateSetColor(set: ChartDataSet, index?: number) {
    const newColor = Config.chartLineColors[index % Config.chartLineColors.length];
    let color = set.borderColor || newColor.code;
    if (this._legendItems) {
      const legend: LegendItem = this._legendItems.find((item: LegendItem) => item.label === set.label);
      if (legend) {
        const chartLineColor = ChartLineColors.find((c: ChartLineColor) => c.name === legend.color);
        color = chartLineColor ? chartLineColor.code : newColor.code;
        legend.color = legend.color || newColor.name;
      }
    }
    set.borderColor = color;
  }

  private updateXAxes() {
    if (this._dataSets.length) {
      const xAxesOptions: Chart.ChartXAxe[] = this.getXAxeOptions();
      this._chartConfig.options.scales.xAxes = xAxesOptions;
    }
  }

  public get chart(): Chart {
    return this._chart;
  }

  private getXAxeOptions(): Chart.ChartXAxe[] {
    return [{
      type: 'time',
      time: {
        displayFormats: {
          second: 'h:mm A',
          minute: 'h:mm A',
          hour: 'MMM D hA'
        }
      },
      ticks: {
        display: this._displayGrid,
        fontSize: 10,
        padding: 15,
        maxTicksLimit: 6
      },
      gridLines: {
        display: this._displayGrid,
        drawTicks: false,
        drawBorder: false,
        color: 'rgba(0, 0, 0, 0.1)'
      }
    }];
  }

  public get dataSets(): ChartDataSet[] {
    return this._dataSets;
  }

  public get testMode(): boolean {
    return this._testMode;
  }

  @HostListener('document:keypress', ['$event'])
  private handleKeyboardEvent({ ctrlKey, shiftKey, key }: KeyboardEvent) {
    if (ctrlKey && shiftKey) {
      switch (key) {
        case 'K':
          this._testMode = !this._testMode;

          if (!this._testMode) {
            setTimeout(() => this.initChart());
          }
          break;
      }
    }
  }
}
