import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PipelineNamePipe } from '../../pipes/pipeline-name.pipe';
import { ChartLineComponent, ChartDataSet, ChartLineColors, UnitType } from './chart-line.component';
import { Chart, ChartData, ChartPoint, ChartTooltipItem } from 'chart.js';

describe('ChartLineComponent', () => {
  let component: ChartLineComponent;
  let fixture: ComponentFixture<ChartLineComponent>;
  const mockLegendItems = [
    {
      label: 'Service1',
      color: 'acid-green',
      muted: false
    },
    {
      label: 'Service2',
      color: 'gold',
      muted: false
    },
    {
      label: 'Service3',
      color: 'bright-blue',
      muted: false
    }
  ];

  const mockDataSets = [{
    label: 'Service1',
    data: [
      {
        x: Date.now(),
        y: 0
      },
      {
        x: Date.now(),
        y: 0
      },
      {
        x: Date.now(),
        y: 0
      }
    ]
  }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChartLineComponent],
      providers: [
        PipelineNamePipe
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartLineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create chart by default', () => {
    expect(component.chart).toBeDefined();
    expect(component.chart instanceof Chart).toBeTruthy();
    expect(component.chart.ctx).toBeTruthy();
    expect(component.chart.config.type).toBe('line');
  });

  it('should be able to toggle the grid & padding', () => {
    // by default, ticks and gridlines should be displayed
    expect(component.chart.config.options.scales.xAxes[0].ticks.display).toBe(true);
    expect(component.chart.config.options.scales.xAxes[0].gridLines.display).toBe(true);
    expect(component.chart.config.options.scales.yAxes[0].ticks.display).toBe(true);
    expect(component.chart.config.options.scales.yAxes[0].gridLines.display).toBe(true);
    expect(component.chart.config.options.layout.padding).toBeTruthy();

    // disable the display of the grid
    component.displayGrid = false;
    component.ngOnInit();
    component.ngAfterViewInit();
    expect(component.chart.config.options.scales.xAxes[0].ticks.display).toBe(false);
    expect(component.chart.config.options.scales.xAxes[0].gridLines.display).toBe(false);
    expect(component.chart.config.options.scales.yAxes[0].ticks.display).toBe(false);
    expect(component.chart.config.options.scales.yAxes[0].gridLines.display).toBe(false);
    expect(component.chart.config.options.layout.padding).toBe(0);
  });

  it('should be able to toggle the tooltips', () => {
    // by default, tooltips should be enabled
    expect(component.chart.config.options.tooltips.enabled).toBe(true);

    // disable tooltips
    component.enableTooltips = false;
    component.ngOnInit();
    component.ngAfterViewInit();
    expect(component.chart.config.options.tooltips.enabled).toBe(false);
  });

  it('should handle there is no data at the start', () => {
    const curTime = new Date().getTime();
    const duration = 5;
    const startTime = curTime - (duration * 1000);
    const step = 1;
    const points: ChartPoint[] = [{
      x: new Date(curTime - 3000),
      y: 10
    }, {
      x: new Date(curTime - 2000),
      y: 20
    }, {
      x: new Date(curTime - 1000),
      y: 30
    }, {
      x: new Date(curTime),
      y: 40
    }];

    const set: ChartDataSet[] = [
      {
        data: points.slice(0)
      }
    ];

    component.initDataSets(set, duration, startTime, step);

    expect((set[0].data[0].x as Date).getTime()).toBe(startTime);
    expect(set[0].data[0].y).toBe(0);
    expect((set[0].data[1].x as Date).getTime()).toBe((points[0].x as Date).getTime() - 1);
    expect(set[0].data[1].y).toBe(0);
    expect((set[0].data[2].x as Date).getTime()).toBe((points[0].x as Date).getTime());
    expect(set[0].data[2].y).toBe(10);
    expect((set[0].data[3].x as Date).getTime()).toBe((points[1].x as Date).getTime());
    expect(set[0].data[3].y).toBe(20);
    expect((set[0].data[4].x as Date).getTime()).toBe((points[2].x as Date).getTime());
    expect(set[0].data[4].y).toBe(30);
    expect((set[0].data[5].x as Date).getTime()).toBe((points[3].x as Date).getTime());
    expect(set[0].data[5].y).toBe(40);
  });

  it('should handle there is no data in the middle', () => {
    const curTime = new Date().getTime();
    const duration = 3;
    const startTime = curTime - (duration * 1000);
    const step = 1;
    const points: ChartPoint[] = [{
      x: new Date(curTime - (duration * 1000)),
      y: 10
    }, {
      x: new Date(curTime - ((duration - 2) * 1000)),
      y: 30
    }];

    const set: ChartDataSet[] = [
      {
        data: points.slice(0)
      }
    ];

    component.initDataSets(set, duration, startTime, step);

    expect((set[0].data[0].x as Date).getTime()).toBe((points[0].x as Date).getTime());
    expect(set[0].data[0].y).toBe(10);
    expect((set[0].data[1].x as Date).getTime()).toBe((points[0].x as Date).getTime());
    expect(set[0].data[1].y).toBe(0);
    expect((set[0].data[2].x as Date).getTime()).toBe((points[1].x as Date).getTime());
    expect(set[0].data[2].y).toBe(0);
    expect((set[0].data[3].x as Date).getTime()).toBe((points[1].x as Date).getTime());
    expect(set[0].data[3].y).toBe(30);
  });

  it('should match service line color with the legend', () => {
    component.dataSets = mockDataSets;
    component.legendItems = mockLegendItems;
    component.ngOnInit();
    const dataSet = component['_dataSets'][0];
    const legend = component['_legendItems'].find(legend => legend.label == dataSet.label);
    const chartLineColor = ChartLineColors.find(color => color.name === legend.color);
    expect(dataSet.borderColor).toEqual(chartLineColor.code);
  });

  it('crosshair plugin is properly enabled', () => {
    // by default, crosshair plugin should be enabled
    expect(component['_chart'].options.plugins.crosshair).toBeDefined();

    // disable crosshair plugin
    component.enableCrosshair = false;
    component.ngOnInit();
    component.ngAfterViewInit();
    expect(component['_chart'].options.plugins.crosshair).toBe(false);
  });

  it('should have zoom plugin enabled', () => {
    const zoomPlugin = component['_chart'].config.options.plugins.crosshair.zoom;
    expect(zoomPlugin.enabled).toBe(true);
    expect(zoomPlugin.zoomButtonClass).toBe('reset-zoom');
    expect(zoomPlugin.zoomButtonText).toBe('Reset Zoom');
    expect(zoomPlugin.zoomboxBackgroundColor).toBe('rgba(66,133,244,0.2)');
    expect(zoomPlugin.zoomboxBorderColor).toBe('#48F');
  });

  it('should render tooltips properly', () => {
    component.yAxesUnits = UnitType.BitRate;
    const tooltipCallbacks = component['_chartOptions'].tooltips.callbacks;
    const tooltipItems: ChartTooltipItem[] = [{
      datasetIndex: 0,
      index: 0,
      yLabel: 22861963219.2
    }, {
      datasetIndex: 0,
      index: 1,
      yLabel: 22899159029.99784
    }, {
      datasetIndex: 1,
      index: 0,
      yLabel: 24187568240
    }, {
      datasetIndex: 1,
      index: 1,
      yLabel: 24144738678.4
    }];
    const tooltipData: ChartData = {
      datasets: [
        {
          label: "throughputOut"
        }, {
          label: "throughputIn"
        }
      ]
    };

    /* since we can't test chart canvas, need to manually trigger callbacks
     * should be triggered in the order beforeBody, label(s), afterBody */
    tooltipCallbacks.beforeBody(tooltipItems, tooltipData);
    expect(tooltipCallbacks.label(tooltipItems[0], tooltipData)).toBe('Total Out : 23 Gbps');
    expect(tooltipCallbacks.label(tooltipItems[1], tooltipData)).toBeUndefined();
    expect(tooltipCallbacks.label(tooltipItems[2], tooltipData)).toBe('Total In : 24 Gbps');
    expect(tooltipCallbacks.label(tooltipItems[3], tooltipData)).toBeUndefined();
    expect(tooltipCallbacks.afterBody(tooltipItems, tooltipData)).toBe('');
  });

  it('should display chart in test mode', () => {
    const ev = new KeyboardEvent('keypress', {
      ctrlKey: true,
      shiftKey: true,
      key: 'K'
    });

    document.dispatchEvent(ev);
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.querySelector('table')).toBeDefined();
  });
});
