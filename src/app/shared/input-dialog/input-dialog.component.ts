import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface InputDialogData {
  title: string;
  placeholder: string;
  value: string;
}

@Component({
  selector: 'nef-input-dialog',
  templateUrl: './input-dialog.component.html',
  styleUrls: ['./input-dialog.component.less']
})
export class InputDialogComponent implements OnInit {
  private _title: string;
  private _placeholder: string;
  private _value: string;

  constructor(
    private _dialog: MatDialogRef<InputDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: InputDialogData
  ) {
    this._title = this._data.title;
    this._placeholder = this._data.placeholder;
    this._value = this._data.value;
  }

  public ngOnInit() {
  }

  public submit(value: any) {
    this._dialog.close(value);
  }

  public get title(): string {
    return this._title;
  }

  public get placeholder(): string {
    return this._placeholder;
  }

  public get value(): string {
    return this._value;
  }
}
