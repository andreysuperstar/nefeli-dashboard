import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InputDialogComponent } from './input-dialog.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

describe('InputDialogComponent', () => {
  let component: InputDialogComponent;
  let fixture: ComponentFixture<InputDialogComponent>;
  let dialogSpy = {
    close: jasmine.createSpy('close')
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        NoopAnimationsModule
      ],
      declarations: [InputDialogComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogSpy },
        {
          provide: MAT_DIALOG_DATA, useValue: {
            title: 'BPF Filter',
            placeholder: 'Enter port number'
          }
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;
    expect(el.querySelector('.h4-bold').textContent).toBe('BPF Filter');
  });

  it('should render the placeholder', () => {
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;
    expect(el.querySelector('input').placeholder).toBe('Enter port number');
  });

  it('submit should close the dialog', () => {
    const el = fixture.debugElement.nativeElement;
    el.querySelector('input').value = 'udp || http';
    el.querySelector('img').click();
    expect(dialogSpy.close).toHaveBeenCalledWith('udp || http');
  });
});
