import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateTextareaComponent } from './template-textarea.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CodemirrorComponent } from '../codemirror/codemirror.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { TemplateSelectComponent } from '../template-select/template-select.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { Component, DebugElement, OnInit } from '@angular/core';
import { CREATE_TEMPLATE_VARIABLE, DocChangeEvent } from '../../utils/codemirror';
import { FormBuilder, FormGroup, ReactiveFormsModule } from '@angular/forms';

const variables: string[] = ['var1', 'var2', 'var3'];
const docChangeHandler = jasmine.createSpy('docChange');

@Component({
  selector: '',
  template: `<nef-template-textarea [formGroup]="form"></nef-template-textarea>`,
})
class TestWrapper implements OnInit {
  private _form: FormGroup;

  constructor(private _fb: FormBuilder) { }

  public ngOnInit(): void {
    this._form = this._fb.group({
      testCodemirror: []
    });
  }

  public get form(): FormGroup {
    return this._form;
  }
}

describe('TemplateTextareaComponent', () => {
  let component: TemplateTextareaComponent;
  let fixture: ComponentFixture<TemplateTextareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        ReactiveFormsModule
      ],
      declarations: [
        TestWrapper,
        TemplateTextareaComponent,
        CodemirrorComponent,
        TemplateSelectComponent
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    docChangeHandler.calls.reset();
    fixture = TestBed.createComponent(TemplateTextareaComponent);
    component = fixture.componentInstance;
    component.variables = variables;
    component.docChange.subscribe(docChangeHandler);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(fixture.debugElement.query(By.css('.variable-button'))).toBeTruthy();
    const options = component.options;
    expect(options.lineNumbers).toBe(false);
    expect(options.mode).toBe('javascript');
    expect(options.theme).toBe('idea');
    expect(options.extraKeys).not.toBeNull();
    expect(component.formGroup).toBeFalsy();
  });

  it('should set properties correctly', () => {
    const fixture = TestBed.createComponent(TestWrapper);
    const component = fixture.debugElement.query(By.directive(TemplateTextareaComponent)).componentInstance;
    const placeHolder = 'Enter test text';
    const formControlName = 'testCodemirror';
    component.formControlName = formControlName;
    component.placeholder = placeHolder;

    fixture.detectChanges();
    expect(component.placeholder).toBe(placeHolder);
    expect(component.formControlName).toBe(formControlName);
  });

  it('should delegate ControlValueAccessor methods calls to codemirror component', () => {
    const testText = 'test text';
    const writeValueSpy = jasmine.createSpy('writeValue');
    const registerOnChangeSpy = jasmine.createSpy('registerOnChange');
    const registerOnTouchedSpy = jasmine.createSpy('registerOnTouched');
    const setDisabledStateSpy = jasmine.createSpy('setDisabledState');
    let codeMirrorComponent = fixture.debugElement.query(By.css('nef-codemirror')).componentInstance as CodemirrorComponent;
    codeMirrorComponent.writeValue = writeValueSpy;
    codeMirrorComponent.registerOnChange = registerOnChangeSpy;
    codeMirrorComponent.registerOnTouched = registerOnTouchedSpy;
    codeMirrorComponent.setDisabledState = setDisabledStateSpy;

    component.writeValue(testText);
    const onChangeHandler = () => { };
    const onTouchedHandler = () => { };
    component.registerOnChange(onChangeHandler);
    component.registerOnTouched(onTouchedHandler);
    component.setDisabledState(true);
    fixture.detectChanges();
    expect(writeValueSpy).toHaveBeenCalledWith(testText);
    expect(registerOnChangeSpy).toHaveBeenCalledWith(onChangeHandler);
    expect(registerOnTouchedSpy).toHaveBeenCalledWith(onTouchedHandler);
    expect(setDisabledStateSpy).toHaveBeenCalledWith(true);
  });

  it('should show template-select component after clicking on variable-button without text selection', () => {
    const variableBtn = fixture.debugElement.query(By.css('.variable-button'));
    variableBtn.triggerEventHandler('click', {});
    fixture.detectChanges();

    const variableSelect: DebugElement = fixture.debugElement.query(By.css('nef-template-select'));
    expect(variableSelect).toBeTruthy();
  });

  it('should add variable after option selection of template-select component', () => {
    const variableBtn = fixture.debugElement.query(By.css('.variable-button'));
    variableBtn.triggerEventHandler('click', {});
    fixture.detectChanges();

    const inputElement = fixture.debugElement.query(By.css('nef-template-select input')); // Returns DebugElement
    inputElement.nativeElement.focus();
    inputElement.nativeElement.dispatchEvent(new Event('focus'));
    inputElement.nativeElement.dispatchEvent(new Event('focusin'));
    inputElement.nativeElement.dispatchEvent(new Event('input'));
    inputElement.nativeElement.dispatchEvent(new Event('keydown'));
    inputElement.triggerEventHandler('focus', null);
    fixture.detectChanges();

    const matOptions: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-option'));
    expect(matOptions.length).toBe(3);
    component.onVariableSelectChange(variables[0]);
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('nef-template-select'))).not.toBeTruthy();
    expect(docChangeHandler).toHaveBeenCalled();
    const args: DocChangeEvent = docChangeHandler.calls.argsFor(0)[0];
    expect(args.change.text).toEqual([CREATE_TEMPLATE_VARIABLE(variables[0])]);
  });

  it('should convert selected text into variable', () => {
    component.value = 'test text';
    fixture.detectChanges();

    //TODO(dmitry) find a way to select text programmatically (.getSelection() should return 'test' instead of empty string)
    let codeMirrorComponent = fixture.debugElement.query(By.css('nef-codemirror')).componentInstance as CodemirrorComponent;
    codeMirrorComponent.codeMirror.setSelection({ ch: 4, line: 0 });
    fixture.detectChanges();

    codeMirrorComponent = fixture.debugElement.query(By.css('nef-codemirror')).componentInstance as CodemirrorComponent;
    const selectedText = codeMirrorComponent.codeMirror.getSelection();
    expect(selectedText).toBe('');
  });

  it('should show template button when the focus is set to textarea', () => {
    component.onVariableBtnClick(new Event('click'));
    fixture.detectChanges();

    expect(component.showVariableSelect).toBe(true);

    component.onFocusChange(true);
    fixture.detectChanges();

    expect(component.showVariableSelect).toBe(false);
  });

  it('should replace selection with variable when template button click', () => {
    const codeMirrorComponent = fixture.debugElement.query(By.css('nef-codemirror')).componentInstance as CodemirrorComponent;
    codeMirrorComponent.codeMirror.setValue('test');
    codeMirrorComponent.codeMirror.setSelection({ line: 0, ch: 0 }, { line: 0, ch: 4 });
    component.onVariableBtnClick(new Event('click'));
    fixture.detectChanges();

    expect(codeMirrorComponent.codeMirror.getValue()).toBe(CREATE_TEMPLATE_VARIABLE('test'));
  });
});
