import { Component, EventEmitter, forwardRef, Input, Optional, Output, Renderer2, ViewChild } from '@angular/core';
import { ControlContainer, ControlValueAccessor, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CodemirrorComponent } from '../codemirror/codemirror.component';
import { EditorConfiguration, Pass } from 'codemirror';
import {
  CodeMirrorInst,
  CREATE_TEMPLATE_VARIABLE,
  DocChangeEvent,
  markVariables,
  showVariablesHint,
  TEMPLATE_VARIABLE_NAME_PATTERN
} from '../../utils/codemirror';
import { MatFormFieldAppearance } from '@angular/material/form-field/form-field';
import { MatFormFieldControl } from '@angular/material/form-field';

@Component({
  selector: 'nef-template-textarea',
  templateUrl: './template-textarea.component.html',
  styleUrls: ['./template-textarea.component.less'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TemplateTextareaComponent),
      multi: true,
    },
    { provide: MatFormFieldControl, useExisting: TemplateTextareaComponent }
  ]
})
export class TemplateTextareaComponent implements ControlValueAccessor {

  protected _formControlName: string;
  protected _variables: string[] = [];
  protected _value = '';
  protected _showVariableSelect = false;
  protected _placeholder = '';
  protected _docChange = new EventEmitter<DocChangeEvent>();

  @Input() public set value(value: string) {
    this._value = value;
  }

  @Input() public set formControlName(formControlName: string) {
    this._formControlName = formControlName;
  }

  @Input() public set variables(variables: string[]) {
    this._variables = variables;
  }

  @Input() public set placeholder(plh: string) {
    this._placeholder = plh;
  }

  @ViewChild('codemirror') protected _codemirrorComponent: CodemirrorComponent;

  @Output() public get docChange(): EventEmitter<DocChangeEvent> {
    return this._docChange;
  }

  public constructor(protected _renderer: Renderer2, @Optional() protected _controlContainer: ControlContainer) {
  }

  public writeValue(obj: string): void {
    this._codemirrorComponent?.writeValue(obj);
  }

  public registerOnChange(fn: (value: string) => void): void {
    this._codemirrorComponent?.registerOnChange(fn);
  }

  public registerOnTouched(fn: () => void): void {
    this._codemirrorComponent?.registerOnTouched(fn);
  }

  public setDisabledState(isDisabled: boolean): void {
    this._codemirrorComponent?.setDisabledState(isDisabled);
  }

  public get appearance(): MatFormFieldAppearance {
    return 'outline';
  }
  public get formControlName(): string {
    return this._formControlName;
  }

  public get options(): EditorConfiguration {
    return {
      lineNumbers: false,
      mode: 'javascript',
      theme: 'idea',
      extraKeys: {
        '.': this.showHint.bind(this)
      }
    };
  }

  public get value(): string {
    return this._value;
  }

  public get formGroup(): FormGroup {
    return this._controlContainer?.control as FormGroup;
  }

  public get showVariableSelect(): boolean {
    return this._showVariableSelect;
  }

  public get variables(): string[] {
    return this._variables;
  }

  public get placeholder(): string {
    return this._placeholder;
  }

  public onDocChange(e: DocChangeEvent) {
    markVariables(e, this._renderer);
    this._docChange.emit(e);
  }

  public onVariableBtnClick(event: Event) {
    const selection = this._codemirrorComponent.codeMirror.getSelection();

    if (selection) {
      const isValidVariableName = TEMPLATE_VARIABLE_NAME_PATTERN.test(selection);

      if (isValidVariableName) {
        this._codemirrorComponent.codeMirror.replaceSelection(CREATE_TEMPLATE_VARIABLE(selection));
      }
    } else {
      this._showVariableSelect = true;
    }
  }

  public onFocusChange(focus: boolean) {
    if (focus) {
      this._showVariableSelect = false;
    }
  }

  public onVariableSelectChange(option: string) {
    this._showVariableSelect = false;

    if (option) {
      const pos = this._codemirrorComponent.codeMirror.getDoc().getCursor();
      this._codemirrorComponent.codeMirror.getDoc().replaceRange(CREATE_TEMPLATE_VARIABLE(option), pos);
      this._codemirrorComponent.codeMirror.focus();
    }
  }

  private showHint(cm: CodeMirrorInst): typeof Pass {
    return showVariablesHint(cm, this._variables);
  }

}
