import { By } from '@angular/platform-browser';
import { DebugElement, EventEmitter } from '@angular/core';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatChipInputEvent, MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonHarness } from '@angular/material/button/testing';

import { ModifyChipsComponent } from './modify-chips.component';

describe('ModifyChipsComponent', () => {
  let component: ModifyChipsComponent;
  let fixture: ComponentFixture<ModifyChipsComponent>;
  let loader: HarnessLoader;

  const mockTitle = 'Add Tags';
  const mockChips: string[] = ['tenant', 'admin'];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatCardModule,
        MatFormFieldModule,
        MatChipsModule,
        MatIconModule
      ],
      declarations: [ModifyChipsComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyChipsComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;

    component.title = mockTitle;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render popup', async () => {
    const titleDe: DebugElement = fixture.debugElement.query(By.css('mat-card-title'));
    const chipsFormFieldDe: DebugElement = fixture.debugElement.query(
      By.css('mat-form-field')
    );
    const buttons: MatButtonHarness[] = await loader.getAllHarnesses(MatButtonHarness);

    expect(titleDe.nativeElement.textContent).toBe(mockTitle, 'render title');
    expect(chipsFormFieldDe).not.toBeNull('render chip field');
    expect(await buttons[0].getText()).toBe('Cancel', 'render Cancel button');
    expect(await buttons[1].getText()).toBe('Save', 'render Save button');

    const chipListDe: DebugElement = chipsFormFieldDe.query(By.css('mat-chip-list'));

    expect(chipListDe).not.toBeNull('render chip list');

    const chipInputDe: DebugElement = chipListDe.query(By.css('input'));

    expect(chipInputDe).not.toBeNull('render chip input');
  });

  it('should add chips', () => {
    const chipInputDe: DebugElement = fixture.debugElement.query(By.css('input'));
    const chipInputEl: HTMLInputElement = chipInputDe.nativeElement;

    spyOn(component, 'onAddChip').and.callThrough();

    let chip: string = 'Tenant';

    chipInputEl.value = chip;
    chipInputEl.dispatchEvent(new Event('input'));

    const chipInputEvent: MatChipInputEvent = {
      input: chipInputEl,
      value: chip
    };

    // add a chip
    chipInputDe.triggerEventHandler('matChipInputTokenEnd', chipInputEvent);
    fixture.detectChanges();

    expect(component.onAddChip).toHaveBeenCalledWith(chipInputEvent);

    let chipDes: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes[0].nativeElement.textContent).toContain(chip, `render ${chip} chip`);

    chip = 'Admin';

    chipInputEl.value = chip;
    chipInputEl.dispatchEvent(new Event('input'));

    chipInputEvent.value = chip;

    // add another chip
    chipInputDe.triggerEventHandler('matChipInputTokenEnd', chipInputEvent);
    fixture.detectChanges();

    expect(component.onAddChip).toHaveBeenCalledWith(chipInputEvent);

    chipDes = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes[1].nativeElement.textContent).toContain(chip, `render ${chip} chip`);
    expect(chipDes.length).toBe(2, 'render 2 chips');
  });

  it('should remove chips', () => {
    component.chips = [...mockChips];
    fixture.detectChanges();

    spyOn(component, 'onRemoveChip').and.callThrough();

    let chipDes: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-chip'));
    let removeIconEl: HTMLUnknownElement = chipDes[0].query(
      By.css('mat-icon')
    ).nativeElement;

    // remove the 1st chip
    removeIconEl.click();
    fixture.detectChanges();

    expect(component.onRemoveChip).toHaveBeenCalledWith(mockChips[0]);

    chipDes = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes.length).toBe(1, `render 1 chip`);
    expect(
      chipDes[0].nativeElement.textContent
    ).not.toContain(mockChips[0], `removed ${mockChips[0]} chip`);
    expect(
      chipDes[0].nativeElement.textContent
    ).toContain(mockChips[1], `render shifted ${mockChips[1]} chip`);

    removeIconEl = chipDes[0].query(By.css('mat-icon')).nativeElement;

    // remove a remaining chip
    removeIconEl.click();
    fixture.detectChanges();

    expect(component.onRemoveChip).toHaveBeenCalledWith(mockChips[1]);

    chipDes = fixture.debugElement.queryAll(By.css('mat-chip'));

    expect(chipDes.length).toBe(0, `render no chips`);
  });

  it('should emit cancel event', async () => {
    const cancelButton: MatButtonHarness = await loader.getHarness(MatButtonHarness.with({
      text: 'Cancel'
    }));

    spyOn<EventEmitter<void>>(component.cancel, 'emit').and.callThrough();

    await cancelButton.click();

    expect(component.cancel.emit).toHaveBeenCalled();
  });

  it('should emit save event', async () => {
    component.chips = [...mockChips];
    fixture.detectChanges();

    const chipInputEl: HTMLInputElement = fixture.debugElement.query(
      By.css('input')
    ).nativeElement;
    const saveButton: MatButtonHarness = await loader.getHarness(MatButtonHarness.with({
      text: 'Save'
    }));

    const chip: string = 'System';

    const chipInputEvent: MatChipInputEvent = {
      input: chipInputEl,
      value: chip
    };

    // add a chip
    component.onAddChip(chipInputEvent);

    spyOn<EventEmitter<string[]>>(component.save, 'emit').and.callThrough();

    await saveButton.click();

    let mockEditedChips: string[] = [...mockChips, chip];

    expect(component.save.emit).toHaveBeenCalledWith(mockEditedChips);

    // remove chips
    component.onRemoveChip(mockEditedChips[0]);
    component.onRemoveChip(mockEditedChips[2]);

    await saveButton.click();

    expect(component.save.emit).toHaveBeenCalledWith([mockEditedChips[1]]);
  });
});
