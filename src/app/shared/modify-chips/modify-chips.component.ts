import { Component, Input, EventEmitter, Output } from '@angular/core';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';

@Component({
  selector: 'nef-modify-chips',
  templateUrl: './modify-chips.component.html',
  styleUrls: ['./modify-chips.component.less']
})
export class ModifyChipsComponent {
  private _title: string;
  private _chips: string[] = [];
  private _cancel = new EventEmitter<void>();
  private _save = new EventEmitter<string[]>();

  @Input() public set title(title: string) {
    this._title = title;
  }

  @Input() public set chips(chips: string[]) {
    this._chips = chips;
  }

  @Output() public get cancel(): EventEmitter<void> {
    return this._cancel;
  }

  @Output() public get save(): EventEmitter<string[]> {
    return this._save;
  }

  public onAddChip({ input, value }: MatChipInputEvent) {
    value = value?.trim?.();

    // Add a chip
    if (value) {
      const index: number = this._chips.indexOf(value);

      if (index >= 0) {
        this._chips.splice(index, 1);
      }

      this._chips.push(value);
    }

    // Reset the chip input value
    if (input) {
      input.value = '';
    }
  }

  public onRemoveChip(chip: string) {
    const index: number = this._chips.indexOf(chip);

    if (index >= 0) {
      this._chips.splice(index, 1);
    }
  }

  public onCancel() {
    this._cancel.emit();
  }

  public onSave() {
    this._save.emit(this._chips);
  }

  public get title(): string {
    return this._title;
  }

  public get chips(): string[] {
    return this._chips;
  }

  public get separatorKeyCodes(): number[] {
    return [ENTER, COMMA];
  }
}
