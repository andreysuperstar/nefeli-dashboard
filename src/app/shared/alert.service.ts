import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, SimpleSnackBar } from '@angular/material/snack-bar';

export enum AlertType {
  NONE = '',
  INFO_REGISTER_USER_SUCCESS = 'User successfully registered',
  INFO_RESET_PASSWORD = 'Password has been successfully reset.',
  INFO_EDIT_PASSWORD = 'Password has been successfully changed.',
  INFO_REQUEST_RESET_PASSWORD = 'Request to reset password sent, check your email for further instructions.',
  INFO_CREATE_USER_SUCCESS = 'User successfully created',
  INFO_CREATE_TENANT_SUCCESS = 'Tenant successfully created',
  INFO_REMOVE_TENANT_SUCCESS = 'Tenant successfully removed',
  INFO_ADD_TENANT_NF_SUCCESS = 'Network Function successfully added to Tenant',
  INFO_CREATE_CLUSTER_NF_SUCCESS = 'Network Function successfully created',
  INFO_EDIT_CLUSTER_NF_SUCCESS = 'Network Function successfully updated',
  INFO_CREATE_SITE_SUCCESS = 'Site successfully created.',
  INFO_REMOVE_SITE_SUCCESS = 'Site successfully removed.',
  INFO_REMOVE_SITES_SUCCESS = 'Sites successfully removed.',
  INFO_EDIT_SITE_SUCCESS = 'Site successfully updated.',
  INFO_ASSIGN_SITES_SOFTWARE_PROFILE_SUCCESS = 'Software profile successfully assigned for selected UCPE sites. Can not apply the software profile to sites of type CLUSTER. The following sites could not be assigned the software profile:',
  INFO_ASSIGN_UCPE_SITES_ONLY_SOFTWARE_PROFILE_SUCCESS = 'Software profile successfully assigned for selected sites.',
  INFO_EDIT_USER_SUCCESS = 'User successfully edited',
  INFO_EDIT_USER_ROLES_SUCCESS = 'User roles successfully edited',
  INFO_EDIT_TENANT_SUCCESS = 'Tenant successfully updated',
  INFO_EDIT_SMPT_SUCCESS = 'SNMP settings successfully edited.',
  INFO_UPDATE_TENANT_NF_SUCCESS = 'Network Function successfully updated',
  INFO_UPDATE_CLUSTER_NF_SUCCESS = 'Network Function successfully updated',
  INFO_REMOVE_USER_SUCCESS = 'User successfully removed',
  INFO_REMOVE_USERS_SUCCESS = 'Users successfully removed',
  INFO_REMOVE_NF_SUCCESS = 'Network function successfully removed.',
  INFO_REMOVE_TENANT_NF_SUCCESS = 'Network Function successfully removed from Tenant',
  INFO_CONNECT_EMAIL_SUCCESS = 'Email successfully added',
  INFO_EDIT_EMAIL_SUCCESS = 'Email successfully edited',
  INFO_DELETE_EMAIL_SUCCESS = 'Email successfully deleted',
  INFO_CONNECT_WEBHOOK_SUCCESS = 'Webhook successfully added',
  INFO_EDIT_WEBHOOK_SUCCESS = 'Webhook successfully edited',
  INFO_DELETE_WEBHOOK_SUCCESS = 'Webhook successfully deleted',
  INFO_CONNECT_SLACK_SUCCESS = 'Slack account successfully added',
  INFO_EDIT_SLACK_SUCCESS = 'Slack account successfully edited',
  INFO_DELETE_SLACK_SUCCESS = 'Slack account successfully deleted',
  INFO_CREATE_SERVER_SUCCESS = 'Server successfully created',
  INFO_CREATE_LICENSE_SUCCESS = 'License successfully created',
  INFO_DELETE_LICENSES_SUCCESS = 'Licenses successfully deleted',
  INFO_DELETE_LICENSE_POOLS_SUCCESS = 'License Pools successfully deleted',
  INFO_DELETE_LICENSES_AND_POOL_SUCCESS = 'Licenses and License Pools successfully deleted',
  INFO_CREATE_LICENSE_POOL_SUCCESS = 'Pool successfully created.',
  INFO_CREATE_TUNNEL_SUCCESS = 'Tunnel successfully created.',
  INFO_UPDATE_TUNNEL_SUCCESS = 'Tunnel successfully updated.',
  INFO_REMOVE_TUNNEL_SUCCESS = 'Tunnel successfully removed.',
  INFO_REMOVE_TUNNELS_SUCCESS = 'Tunnels successfully removed.',
  INFO_REMOVE_ATTACHMENT_SUCCESS = 'Attachment successfully removed.',
  INFO_CREATE_ATTACHMENT_SUCCESS = 'Attachment successfully created.',
  INFO_EDIT_ATTACHMENT_SUCCESS = 'Attachment successfully edited.',
  INFO_UPDATE_SITES_CONFIG_SUCCESS = 'Site configuration successfully updated',
  INFO_UPDATE_SYSTEM_CONFIGURATION = 'System configuration successfully updated',
  INFO_RESTORE_SYSTEM_CONFIGURATION = 'System configuration successfully restored.',
  INFO_DELETE_SERVICE_SUCCESS = 'Service chain was successfully deleted',
  INFO_FILE_UPLOADED = 'File has been successfully added',
  INFO_FILE_REMOVED = 'File successfully removed',
  INFO_GRANT_LICENSES_SUCCESS = 'Licenses successfully granted',
  INFO_REVOKE_LICENSE_SUCCESS = 'Licenses successfully revoked',
  INFO_CREATE_SERVICE_SUCCESS = 'Service successfully created.',
  INFO_UPDATE_SERVICE_SUCCESS = 'Service successfully updated.',
  INFO_SAVE_SERVICE_DRAFT_SUCCESS = 'Service draft successfully saved.',
  INFO_CREATE_PROVISIONING_SERVER = 'Provisioning server successfully created.',
  INFO_DELETE_PROVISIONING_SERVER = 'Provisioning server successfully removed.',
  INFO_CREATE_HARDWARE_PROFILE_SUCCESS = 'Hardware profile successfully created.',
  INFO_REMOVE_HARDWARE_PROFILE_SUCCESS = 'Hardware profile successfully removed.',
  INFO_CREATE_SOFTWARE_PROFILE = 'Software profile successfully created.',
  INFO_EDIT_SOFTWARE_PROFILE = 'Software profile successfully edited.',
  INFO_REMOVE_SOFTWARE_PROFILE = 'Software profile successfully removed.',
  INFO_EDIT_TACP_CONFIG_SUCCESS = 'TACACS+ config successfully updated',
  INFO_DELETE_SERVICE_DRAFT_SUCCESS = 'Service chain draft successfully deleted',
  INFO_REMOVE_TEMPLATE_SUCCESS = 'Template successfully removed.',
  INFO_SAVE_TEMPLATE_SUCCESS = 'Template successfully saved.',
  ERROR_EDIT_TACP_CONFIG = 'Could not update TACACS+ config, please try again',
  INFO_CREATE_TACACS_SERVER = 'TACACS+ server successfully created.',
  INFO_DELETE_TACACS_SERVER = 'TACACS+ server was successfully deleted',
  INFO_CREATE_WEAVER_CONFIG = 'Weaver config successfully created.',
  INFO_PROMOTE_WEAVER = 'Weaver successfully promoted.',
  INFO_DISABLE_WEAVER_SYNC = 'Weaver Sync successfully disabled',
  INFO_SUBMIT_SMTP_CONFIGURATION = 'SMTP configuration updated.',
  INFO_DELETE_SMTP_CONFIGURATION = 'SMTP configuration deleted.',
  ERROR_SERVER_CONNECTION_RETRY = 'Could not connect to server. Retrying...',
  ERROR_SITE_CONNECTION_RETRY = 'Can not connect to this site. Retrying...',
  ERROR_SAVING_CHANGES = 'Could not save changes, please try again.',
  ERROR_DUPLICATE_NAME = 'Name already taken, please try again.',
  ERROR_SERVICE_NO_NAME = 'Please provide a name for this service.',
  ERROR_REGISTER_USER_FAILED = 'Could not register user, please try again.',
  ERROR_REQUEST_RESET_PASSWORD = 'Request failed, please try again.',
  ERROR_RESET_PASSWORD = 'Could not reset password, please try again.',
  ERROR_EDIT_PASSWORD = 'Could not change password, please try again.',
  ERROR_CREATE_USER_FAILED = 'Could not create user, please try again.',
  ERROR_CREATE_TENANT = 'Could not create tenant, please try again.',
  ERROR_ADD_TENANT_NF = 'Could not add Network Function to Tenant, please try again.',
  ERROR_CREATE_CLUSTER_NF = 'Could not create Network Function, please try again.',
  ERROR_CREATE_SITE = 'Could not create site, please try again.',
  ERROR_UPDATE_SITE = 'Could not update site, please try again.',
  ERROR_ASSIGN_SITES_SOFTWARE_PROFILE = 'Could not assign software profile for selected sites, please try again.',
  ERROR_EDIT_USER_FAILED = 'Could not edit user, please try again.',
  ERROR_EDIT_TENANT = 'Could not edit tenant, please try again.',
  ERROR_EDIT_USER_ROLES_FAILED = 'Could not edit user roles, please try again.',
  ERROR_EDIT_SMPT = 'Could not save SNMP settings, please try again.',
  ERROR_UPDATE_TENANT_NF = 'Could not update Network Function, please try again.',
  ERROR_UPDATE_CLUSTER_NF = 'Could not update Network Function, please try again.',
  ERROR_UPDATE_CLUSTER_NF_NO_ID = 'Could not update Network Function, missing \'identifier\' or \'local.id\' property.',
  ERROR_UPDATE_CLUSTER_NF_IN_USE = 'Cannot modify Network Function that is currently in use by a service chain.',
  ERROR_REMOVE_USERS_FAILED = 'Could not remove users, please try again.',
  ERROR_REMOVE_TENANT = 'Could not remove tenant, please try again.',
  ERROR_REMOVE_TENANT_ACTIVE_USERS = 'Cannot remove a tenant that has active users.',
  ERROR_REMOVE_SITE = 'Could not remove site.',
  ERROR_REMOVE_SITES = 'Could not remove sites.',
  ERROR_REMOVE_ACTIVE_SITE = 'Could not remove a site with active service chains.',
  ERROR_REMOVE_SITE_RETRY = 'Could not remove site, please try again.',
  ERROR_REMOVE_TENANT_NF = 'Could not remove Network Function from Tenant, please try again.',
  ERROR_REMOVE_CLUSTER_NF_IN_USE = 'Cannot remove Network Function that is currently in use by a service chain or license.',
  ERROR_REMOVE_CLUSTER_NF = 'Cannot remove Network Function, please try again.',
  ERROR_REMOVE_TUNNEL_RETRY = 'Could not remove tunnel, please try again.',
  ERROR_REMOVE_TUNNELS_RETRY = 'Could not remove tunnels, please try again.',
  ERROR_REMOVE_TUNNELS_PARTIAL_RETRY = 'Could not delete all of the requested tunnels, please try again.',
  ERROR_REMOVE_ACTIVE_TUNNEL = 'Could not remove tunnel, currently in use.',
  ERROR_REMOVE_TUNNEL = 'Could not remove tunnel.',
  ERROR_REMOVE_ATTACHMENT = 'Could not remove attachment.',
  ERROR_START_STOP_PERFORMANCE_TRACE = 'Could not start/stop performance trace, please try again.',
  ERROR_GET_ALERTS = 'Could not fetch list of alerts, please try again.',
  ERROR_NODE_NAME_CONFLICT = 'Name conflicts with another node, please use a unique name.',
  ERROR_NODE_NAME_HAS_DASH = 'Name contains an invalid character "-".',
  ERROR_NO_NODE_NAME = 'A name for this node must be provided, reverting back to previous value.',
  ERROR_NF_MISSING_CONFIG = 'NF configuration is incomplete. Please complete before continuing.',
  ERROR_CONNECT_EMAIL = 'Could not add email, please try again.',
  ERROR_EDIT_EMAIL = 'Could not edit email, please try again.',
  ERROR_CONNECT_WEBHOOK = 'Could not add webhook, please try again.',
  ERROR_EDIT_WEBHOOK = 'Could not edit webhook, please try again.',
  ERROR_CONNECT_SLACK = 'Could not add slack account, please try again.',
  ERROR_EDIT_SLACK = 'Could not edit slack account, please try again.',
  ERROR_CREATE_SERVER = 'Could not create server, please try again.',
  ERROR_CREATE_LICENSE = 'Could not create license, please try again.',
  ERROR_NF_CONFIG = 'Could not configure Network Function, please try again.',
  ERROR_NF_CONFIG_MANIFEST = 'Manifest has invalid format, please try again.',
  ERROR_DELETE_LICENSES = 'Could not delete licenses, please try again.',
  ERROR_DELETE_LICENSE_POOLS = 'Could not delete license pools, please try again.',
  ERROR_DELETE_LICENSES_AND_POOL = 'Could not delete licenses and license pools, please try again',
  ERROR_CREATE_LICENSE_POOL = 'Could not create license pool, please try again.',
  ERROR_SELECT_LICENSE_POOL = 'Could not select license pool, please try again.',
  ERROR_POST_LICENSE_POOL = 'Could not apply license pool, please try again.',
  ERROR_CREATE_TUNNEL = 'Could not create tunnel, please try again.',
  ERROR_CREATE_TUNNEL_NO_ENCAP = 'Could not create tunnel, must specify valid VLAN, VXLAN, or Physical Port.',
  ERROR_UPDATE_TUNNEL = 'Could not update tunnel, please try again.',
  ERROR_RETRIEVE_TUNNELS = 'Could not retrieve tenant\'s tunnels, please try again.',
  ERROR_TENANT_NO_TUNNELS = 'This tenant does not have any tunnels configured.',
  ERROR_CREATE_ATTACHMENT = 'Could not create attachment, please try again.',
  ERROR_EDIT_ATTACHMENT = 'Could not edit attachment, please try again.',
  ERROR_UPDATE_SITES_CONFIG = 'Could not update site configuration, please try again.',
  ERROR_UPDATE_SYSTEM_CONFIGURATION = 'Could not update system configuration, please try again.',
  ERROR_RESTORE_SYSTEM_CONFIGURATION = 'Could not restore system configuration, please try again.',
  ERROR_DELETE_EMAIL_NOTIFICATION = 'Could not delete email notification, please try again.',
  ERROR_DELETE_SLACK_NOTIFICATION = 'Could not delete Slack notification, please try again.',
  ERROR_DELETE_WEBHOOK_NOTIFICATION = 'Could not delete webhook notification, please try again.',
  ERROR_RENDER_SERVICE = 'Could not properly render service.',
  ERROR_DELETE_SERVICE = 'Could not delete service chain, please try again.',
  ERROR_INVALID_MANIFEST = 'The manifest file is invalid. Please correct and try again.',
  ERROR_UPLOAD_FILE = 'Could not add new file, please try again.',
  ERROR_REMOVE_FILE = 'Could not remove file, please try again.',
  ERROR_GRANT_LICENSES = 'Could not grant licenses, please try again',
  ERROR_REVOKE_LICENSES = 'Could not revoke licenses, please try again',
  ERROR_CREATE_PROVISIONING_SERVER = 'Could not create provisioning server, please try again.',
  ERROR_DELETE_PROVISIONING_SERVER = 'Could not remove provisioning server, please try again.',
  ERROR_REMOVE_HARDWARE_PROFILE = 'Could not delete hardware profile, please try again.',
  ERROR_CREATE_HARDWARE_PROFILE_FAILED = 'Could not create hardware profile, please try again.',
  ERROR_CREATE_SOFTWARE_PROFILE = 'Could not create software profile, please try again.',
  ERROR_EDIT_SOFTWARE_PROFILE = 'Could not edit software profile, please try again.',
  ERROR_REMOVE_SOFTWARE_PROFILE = 'Could not remove software profile, please try again.',
  ERROR_CREATE_TACACS_SERVER = 'Could not create TACACS+ server, please try again.',
  ERROR_DELETE_TACACS_SERVER = 'Could not delete TACACS+ server, please try again.',
  ERROR_CREATE_WEAVER_CONFIG = 'Could not enable WeaverSync, please try again.',
  ERROR_PROMOTE_WEAVER = 'Could not promote weaver to active, please try again.',
  ERROR_DISABLE_WEAVER_SYNC = 'Could not disable WeaverSync, please try again.',
  ERROR_SUBMIT_SMTP_CONFIGURATION = 'Could not update SMTP configuration, please try again.',
  ERROR_DELETE_SMTP_CONFIGURATION = 'Could not delete SMTP configuration, please try again.',
  ERROR_LOGOUT = 'Unexpected error. Could not clean up user session, please try again.',
  ERROR_DELETE_SERVICE_DRAFT = 'Could not delete service chain draft, please try again',
  ERROR_GET_LOGS = 'Could not download log files, please try again.',
  ERROR_CREATE_SERVICE = 'Could not create service, please try again.',
  ERROR_REMOVE_TEMPLATE = 'Could not remove template, please try again.',
  ERROR_SAVE_TEMPLATE = 'Could not save template, please try again.',
  ERROR_TEMPLATE_INCOMPLETE = 'Could not create service, all template variables must have a value.',
  ERROR_DELETE_NOT_EMPTY_SITE_GROUP = 'Can not delete site when it contains one or more network functions.',
  ERROR_SITE_PORT_CONNECTION = 'Traffic is not allowed to be routed to another site\'s interface.'
}

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private _currentAlertType: AlertType | string = AlertType.NONE;
  private _currentAlertRef: MatSnackBarRef<SimpleSnackBar>;

  constructor(
    public _snackBar: MatSnackBar
  ) { }

  private openSnackBar(alert: AlertType | string, action: string, panelClass = 'alert-bar') {
    // do not 're-open' alert if desired type is already displayed
    if (this._currentAlertType === alert) {
      return;
    }

    this._currentAlertType = alert;
    this._currentAlertRef = this._snackBar.open(alert, action, {
      panelClass: panelClass,
      horizontalPosition: 'right',
      verticalPosition: 'bottom'
    });

    this._currentAlertRef
      .afterDismissed()
      .subscribe(() => {
        this._currentAlertType = AlertType.NONE;
      });
  }

  public error(alert: AlertType | string, message?: string) {
    this.openSnackBar(
      message ?
        `${alert} (${this.stripTenantPrefix(message)})` :
        alert, 'hide');
  }

  public info(alert: AlertType | string) {
    this.openSnackBar(alert, 'hide', 'info-bar');
  }

  public dismiss(alertType?: AlertType) {
    // if an alert type is specified, only dismiss if current alert is of that type
    if (this._currentAlertRef
      && (alertType === undefined || alertType === this._currentAlertType)) {
      this._currentAlertRef.dismiss();
    }
  }

  public get currentAlertRef(): MatSnackBarRef<SimpleSnackBar> {
    return this._currentAlertRef;
  }

  /* For a given alert message, search & destroy all tenant prefixes */
  private stripTenantPrefix(msg: string): string {
    if (!msg) {
      return msg;
    }

    const prefixLength = 3;
    const delimiter = ' ';
    return msg.split(delimiter).map((word: string): string => {
      if (word.startsWith('_') && word.length > prefixLength) {
        const tenantIdLength = +word.substr(1, 2);
        if (!Number.isInteger(tenantIdLength)) {
          return word;
        }

        const tenantId = +word.substr(prefixLength, tenantIdLength);
        if (!Number.isInteger(tenantId)) {
          return word;
        }

        return word.substr(tenantIdLength + prefixLength);
      } else {
        return word;
      }
    }).join(delimiter);
  }
}
