import { TestBed } from '@angular/core/testing';

import { ConverterService } from './converter.service';

describe('ConverterService', () => {
  let converter: ConverterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});

    converter = TestBed.inject(ConverterService);
  });

  it('should convert packets per second', () => {
    let value = converter.packetsPerSeconds(593);
    expect(value[0]).toBe(593);
    expect(value[1]).toBe('pps');

    value = converter.packetsPerSeconds(59388, 3);
    expect(value[0]).toBe(59.388);
    expect(value[1]).toBe('kpps');

    value = converter.packetsPerSeconds(7890306, 2);
    expect(value[0]).toBe(7.89);
    expect(value[1]).toBe('Mpps');

    value = converter.packetsPerSeconds(8660030040, 1);
    expect(value[0]).toBe(8.7);
    expect(value[1]).toBe('Gpps');

    value = converter.packetsPerSeconds(3900400070000);
    expect(value[0]).toBe(4);
    expect(value[1]).toBe('Tpps');
  });

  it('should convert nano seconds', () => {
    let value: number;
    let units: string;

    [value, units] = converter.nanoSeconds(593);
    expect(value).toBe(593);
    expect(units).toBe('ns');

    [value, units] = converter.nanoSeconds(5930, 1);
    expect(value).toBe(5.9);
    expect(units).toBe('μs');

    [value, units] = converter.nanoSeconds(958337420, 3);
    expect(value).toBe(958.337);
    expect(units).toBe('ms');

    [value, units] = converter.nanoSeconds(9583374201, 3);
    expect(value).toBe(9.583);
    expect(units).toBe('s');
  });

  it('should convert bits per second', () => {
    let value: number;
    let units: string;

    [value, units] = converter.bitsPerSeconds(593);
    expect(value).toBe(593);
    expect(units).toBe('bps');

    [value, units] = converter.bitsPerSeconds(59388, 3);

    expect(value).toBe(59.388);
    expect(units).toBe('kbps');

    [value, units] = converter.bitsPerSeconds(7890306, 2);
    expect(value).toBe(7.89);
    expect(units).toBe('Mbps');

    [value, units] = converter.bitsPerSeconds(8660030040, 1);
    expect(value).toBe(8.7);
    expect(units).toBe('Gbps');

    [value, units] = converter.bitsPerSeconds(3900400070000);
    expect(value).toBe(4);
    expect(units).toBe('Tbps');
  });
});
