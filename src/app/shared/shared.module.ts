import { NgModule } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { MatChipsModule } from '@angular/material/chips';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { ByteConverterPipe } from '../pipes/byte-converter.pipe';
import { DurationPipe } from '../pipes/duration.pipe';

import { ToolbarComponent } from '../toolbar/toolbar.component';
import { ChartLineComponent } from './chart-line/chart-line.component';
import { DetailedMenuComponent } from './detailed-menu/detailed-menu.component';
import { DetailedMenuItemComponent } from './detailed-menu-item/detailed-menu-item.component';
import { MenuComponent } from './menu/menu.component';
import { ToggleComponent } from './toggle/toggle.component';
import { TableComponent } from './table/table.component';
import { AvatarComponent } from './avatar/avatar.component';
import { DateRangeComponent } from './date-range/date-range.component';
import { CellGridComponent } from './cell-grid/cell-grid.component';
import { StatusListComponent } from './status-list/status-list.component';
import { ChartHeaderComponent } from './chart-header/chart-header.component';
import { LegendComponent } from './legend/legend.component';
import { InputDialogComponent } from './input-dialog/input-dialog.component';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { AlertClassPipe } from '../pipes/alert-class.pipe';
import { AlarmListComponent } from './alarm-list/alarm-list.component';
import { BackButtonComponent } from './back-button/back-button.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { PoolDialogComponent } from './pool-dialog/pool-dialog.component';
import { SearchComponent } from './search/search.component';
import { ModifyChipsComponent } from './modify-chips/modify-chips.component';
import { StatusGridComponent } from './status-grid/status-grid.component';
import { StatusItemComponent } from './status-item/status-item.component';
import { TemplateSelectComponent } from './template-select/template-select.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { TemplateTextareaComponent } from './template-textarea/template-textarea.component';
import { CodemirrorComponent } from './codemirror/codemirror.component';
import { TemplateInputComponent } from './template-input/template-input.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    OverlayModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatCheckboxModule,
    MatSortModule,
    MatSnackBarModule,
    MatDividerModule,
    MatListModule,
    MatProgressBarModule,
    MatSlideToggleModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatBadgeModule,
    NgbDatepickerModule,
    MatDialogModule,
    MatTooltipModule,
    MatChipsModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule
  ],
  declarations: [
    DurationPipe,
    ByteConverterPipe,
    PipelineNamePipe,
    AlertClassPipe,
    ToolbarComponent,
    ChartLineComponent,
    DetailedMenuComponent,
    DetailedMenuItemComponent,
    MenuComponent,
    ToggleComponent,
    TableComponent,
    AvatarComponent,
    CellGridComponent,
    StatusListComponent,
    DateRangeComponent,
    ChartHeaderComponent,
    LegendComponent,
    InputDialogComponent,
    AlarmListComponent,
    BackButtonComponent,
    ConfirmationDialogComponent,
    PoolDialogComponent,
    SearchComponent,
    ModifyChipsComponent,
    StatusGridComponent,
    StatusItemComponent,
    TemplateSelectComponent,
    TemplateTextareaComponent,
    CodemirrorComponent,
    TemplateInputComponent
  ],
  providers: [
    DecimalPipe,
    ByteConverterPipe,
    PipelineNamePipe,
    AlertClassPipe
  ],
  exports: [
    MatSlideToggleModule,
    DurationPipe,
    ByteConverterPipe,
    ToolbarComponent,
    ChartLineComponent,
    DetailedMenuComponent,
    DetailedMenuItemComponent,
    MenuComponent,
    ToggleComponent,
    TableComponent,
    AvatarComponent,
    CellGridComponent,
    StatusListComponent,
    DateRangeComponent,
    ChartHeaderComponent,
    LegendComponent,
    InputDialogComponent,
    AlarmListComponent,
    PipelineNamePipe,
    AlertClassPipe,
    BackButtonComponent,
    SearchComponent,
    ModifyChipsComponent,
    StatusGridComponent,
    StatusItemComponent,
    TemplateSelectComponent,
    TemplateTextareaComponent,
    CodemirrorComponent,
    TemplateInputComponent
  ]
})
export class SharedModule { }
