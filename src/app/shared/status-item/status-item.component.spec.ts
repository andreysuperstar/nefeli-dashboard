import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';

import { StatusType, StatusItemComponent } from './status-item.component';
import { Router } from '@angular/router';

describe('StatusItemComponent', () => {
  let component: StatusItemComponent;
  let fixture: ComponentFixture<StatusItemComponent>;
  let document: Document;
  let router: Router;

  const mockType = StatusType.Success;
  const mockSize = 5;
  const mockLink = ['/sites', 'andrew_site'];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatCardModule
      ],
      declarations: [StatusItemComponent]
    });

    document = TestBed.inject(DOCUMENT);
    router = TestBed.inject(Router);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StatusItemComponent);
    component = fixture.componentInstance;

    component.type = mockType;
    component.size = mockSize;
    component.link = mockLink;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set status type and color class, size, link and anchor', () => {
    const statusDe = fixture.debugElement.query(By.css('.status'));

    expect(component.type).toBe(mockType, 'set type');
    expect(component.size).toBe(mockSize, 'set size');
    expect(component.link).toEqual(mockLink, 'set path');

    expect(
      fixture.debugElement.nativeElement.classList.contains('success')
    ).toBeTruthy('contain color class');

    expect(statusDe.nativeElement.getAttribute('href')).toBe(
      mockLink.join('/'),
      'valid status anchor'
    );
  });

  it('should apply status background color', () => {
    const statusDe = fixture.debugElement.query(By.css('.status'));

    const { backgroundColor } = document.defaultView.getComputedStyle(statusDe.nativeElement);

    expect(backgroundColor).toBe('rgb(46, 213, 115)', 'render background color');
  });

  it('should render status popup hidden', () => {
    const statusPopupDe = fixture.debugElement.query(By.css('.status-popup'));

    const { display } = document.defaultView.getComputedStyle(statusPopupDe.nativeElement);

    expect(display).toBe('none', 'hide status popup');
  });
});
