import { Component, Input, HostBinding } from '@angular/core';

export enum StatusType {
  Success = 'success',
  Info = 'info',
  Muted = 'muted',
  Warn = 'warn',
  Error = 'error'
}

@Component({
  selector: 'nef-status-item',
  templateUrl: './status-item.component.html',
  styleUrls: ['./status-item.component.less']
})
export class StatusItemComponent {
  private _link: string[];

  /**
   * public type input sets a class to the component and is utilized for type-checking
   */
  @Input()
  @HostBinding('class')
  public type: StatusType;

  /**
   * public size input attribute is used in styles and for type-checking
   */
  @Input() public size: number;

  @Input() public set link(link: string[]) {
    this._link = link;
  }

  public get link(): string[] {
    return this._link;
  }
}
