import { HttpErrorResponse } from '@angular/common/http';
import { Observable, BehaviorSubject, of, Subscription } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { AlarmService, Config, Alarm, AlarmRequestOptions, Severity } from './alarm.service';

export class AlarmDataSource extends DataSource<Alarm | undefined> {
  private _length = 1;
  private _pageSize = Config.pageSize;
  private _cachedData = Array.from<Alarm>({ length: this.length });
  private _fetchedPages = new Set<number>();
  private _subscription = new Subscription();
  private _dataStream = new BehaviorSubject<(Alarm | undefined)[]>(this._cachedData);
  private _newAlarmsStream = new BehaviorSubject<Alarm[]>([]);
  private _lastPage: number;
  private _severity: Severity[] = [Severity.Critical, Severity.Error, Severity.Warn, Severity.Info];
  private _getAll = true;
  private _isLoading = true;
  private _verbose = false;

  public get length(): number {
    return this._length;
  }

  public get pageSize(): number {
    return this._pageSize;
  }

  constructor(
    private _alarmService: AlarmService) {
    super();
  }

  public connect(collectionViewer: CollectionViewer): Observable<(Alarm | undefined)[]> {
    this._subscription.add(collectionViewer.viewChange.subscribe(range => {
      const startPage = this.getPageForIndex(range.start);
      const endPage = this.getPageForIndex(range.end - 1);
      for (let i = startPage; i <= endPage; i++) {
        this.fetchPage(i);
      }
    }));
    return this._dataStream;
  }

  public disconnect(): void {
    this._dataStream.complete();
    this._subscription.unsubscribe();
  }

  private getPageForIndex(index: number): number {
    const page = Math.floor(index / (this.pageSize - 1));
    return page;
  }

  private fetchPage(page: number) {
    if (this._fetchedPages.has(page)) {
      return;
    }
    this._fetchedPages.add(page);

    if (page >= this._lastPage) {
      return;
    }

    const params: AlarmRequestOptions = {
      pageSize: this.pageSize,
      endTime: this.getEndTime(page),
      severity: this._severity,
      all: this._getAll,
      verbose: this._verbose
    };

    this._alarmService
      .getAlarms(params)
      .pipe(
        catchError(() => of([]))
      )
      .subscribe((alarms: Alarm[]) => {
        this._newAlarmsStream.next(alarms);
        this._cachedData.splice(page * this.pageSize, this.pageSize, ...alarms);
        this._length = this._cachedData.length;

        // TODO(ecarino): optimize, should only have to sort the newly retrieved page
        this._cachedData.sort((a: Alarm, b: Alarm) => {
          return parseInt(b.timestamp, 10) - parseInt(a.timestamp, 10);
        });

        this._dataStream.next(this._cachedData);
        if (alarms.length < this.pageSize) {
          this._lastPage = page;
        }
        this._isLoading = false;
      });
  }

  public get dataStream(): Observable<Alarm[]> {
    return this._dataStream.asObservable();
  }

  public get newAlarmStream$(): Observable<Alarm[]> {
    return this._newAlarmsStream.asObservable();
  }

  private getEndTime(page: number): Date {
    const msInSec = 1000;
    const index: number = (page * this.pageSize) - 1;
    let time: number = Date.now();

    if (this._cachedData && this._cachedData[index]) {
      time = parseInt(this._cachedData[index].timestamp, 10) * msInSec;
    }
    return new Date(time);
  }

  public set severity(s: Severity[]) {
    this._severity = s;
  }

  public set getAll(val: boolean) {
    this._getAll = val;
  }

  public get isLoading(): boolean {
    return this._isLoading;
  }

  public set verbose(val: boolean) {
    this._verbose = val;
  }
}
