import * as moment from 'moment';
import { Observable, BehaviorSubject, EMPTY, forkJoin, of, concat, timer } from 'rxjs';
import { map, tap, catchError, mergeMap, shareReplay, skip, repeat, mapTo, take } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { InlineResponse200 } from 'rest_client/pangolin/model/inlineResponse200';
import { Alert } from 'rest_client/pangolin/model/alert';
import { Alerts } from 'rest_client/pangolin/model/alerts';
import { AlarmsService } from 'rest_client/pangolin/api/alarms.service';
import { User } from 'rest_client/heimdallr/model/user';
import { UserService } from '../users/user.service';
import { AlertService, AlertType } from './alert.service';
import { HttpSuccess, HttpError } from '../error-codes';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalStorageService, LocalStorageKey } from './local-storage.service';
import { AuthenticationService } from '../auth/authentication.service';

export const Config = {
  pageSize: 100,
  streamInterval: 5000,
  unreadPageSize: 500
};

export enum Severity {
  Debug = 'debug',
  Critical = 'critical',
  Error = 'error',
  Warn = 'warning',
  Info = 'info'
}

// tslint:disable-next-line: no-empty-interface
export interface AlarmsResponse extends Alerts { }

// tslint:disable-next-line: no-empty-interface
export interface Alarm extends Alert { }

export interface AlarmGroup {
  time: string;
  alarms: Alarm[];
}

export interface AlarmRequestOptions {
  pageSize?: number;
  startTime?: Date;
  endTime?: Date;
  severity?: Severity[];
  all?: boolean; // get all alarms, not unread only
  verbose?: boolean;
}

export interface AlarmsCount extends InlineResponse200 {
  total: number;
  critical: number;
  nonCritical: number;
}

@Injectable({
  providedIn: 'root'
})
export class AlarmService {

  private _banner$ = new BehaviorSubject<boolean>(false);
  private _streamAlarmsCount$: Observable<AlarmsCount>;

  constructor(
    private _restAlarmService: AlarmsService,
    private _alert: AlertService,
    private _userService: UserService,
    private _localStorageService: LocalStorageService,
    private _authenticationService: AuthenticationService
  ) {
    const request$ = this._userService.user$.pipe(
      take(1),
      mergeMap((user: User) => {
        return forkJoin([
          this._restAlarmService.getAlarmsCountUser([Severity.Critical]),
          this._restAlarmService.getAlarmsCountUser([Severity.Error, Severity.Warn])
        ]).pipe(
          tap(() => this._alert.dismiss(AlertType.ERROR_SERVER_CONNECTION_RETRY)),
          catchError((e) => {
            switch ((e as HttpErrorResponse).status) {
              case HttpError.Canceled: {
                this._alert.error(AlertType.ERROR_SERVER_CONNECTION_RETRY);
                break;
              }
              case HttpError.Forbidden: {
                this._authenticationService.logout();
                break;
              }
            }

            return of([0, 0]);
          })
        );
      })
    );

    this._streamAlarmsCount$ = concat(
      request$,
      timer(Config.streamInterval).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      map(([critical, other]: [AlarmsCount, AlarmsCount]): AlarmsCount => {
        const newCriticalDetected = (this._localStorageService.read(LocalStorageKey.UNREAD_CRITICAL_ALARM_COUNT) < critical.count);

        if (newCriticalDetected) {
          this._localStorageService.delete(LocalStorageKey.HIDE_ALARM_BANNER);
        }

        this._localStorageService.write(LocalStorageKey.UNREAD_CRITICAL_ALARM_COUNT, critical.count);
        this.setBanner(critical.count > 0);

        return {
          critical: critical.count,
          nonCritical: other.count,
          total: (critical.count || 0) + (other.count || 0)
        } as AlarmsCount;
      }),
      shareReplay({
        refCount: true
      })
    );
  }

  public setBanner(visible: boolean) {
    if (visible && this._localStorageService.read(LocalStorageKey.HIDE_ALARM_BANNER) === 'true') {
      // user explicitly hid the alarm banner
      return;
    }

    this._banner$.next(visible);
  }

  public get banner$(): Observable<boolean> {
    return this._banner$.asObservable();
  }

  public getAlarms(params: AlarmRequestOptions = {}): Observable<Alarm[]> {
    const { pageSize, startTime, endTime, severity, all, verbose } = params;

    return this._userService.user$.pipe(
      mergeMap(() => this._restAlarmService.getAlarmsUser(startTime, endTime, all, severity, verbose, 0, pageSize)),
      map((response: AlarmsResponse) => response.alerts || []),
      catchError((e) => {
        if (e.status !== HttpSuccess.OK) {
          this.setBanner(false);
        }

        return EMPTY;
      })
    );
  }

  public get streamAlarmsCount$(): Observable<AlarmsCount> {
    return this._streamAlarmsCount$;
  }

  public groupByDate(alarms: Alarm[]): AlarmGroup[] {
    const msInSecond = 1000;
    const result = [];

    alarms.forEach((alarm: Alarm) => {
      const timeMs = parseInt(alarm.timestamp, 10) * msInSecond;
      const group = result.find(g => moment(g.time).isSame(timeMs, 'day'));
      if (group) {
        group.alarms.push(alarm);
      } else {
        result.push({ time: timeMs, alarms: [alarm] });
      }
    });
    return result;
  }

  public markAsRead(username: string, alarms: Alarm[]): Observable<any> {
    return this._restAlarmService.markAsRead({ uuids: alarms.map((a: Alarm) => a.uuid) });
  }
}
