import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { AlarmService, Alarm, Severity, AlarmsResponse, AlarmsCount } from './alarm.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { UserService } from '../users/user.service';
import { of } from 'rxjs';
import { AlertService, AlertType } from './alert.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { InlineResponse200 } from 'rest_client/pangolin/model/inlineResponse200';
import { LocalStorageService, LocalStorageKey } from './local-storage.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpError } from '../error-codes';

describe('AlarmService', () => {
  let service: AlarmService;
  let httpTestingController: HttpTestingController;

  const mockAlerts: AlarmsResponse = {
    alerts: [
      {
        timestamp: "1547563847",
        severity: "info",
        name: "User created",
        metadata: {},
        identity: {},
        uuid: "dc143517-757c-41f4-95a1-4e85e38ad834",
        _class: "pipeline",
        reason: ""
      },
      {
        timestamp: "1547571234",
        severity: "critical",
        name: "Machine is out of disk space",
        metadata: {},
        identity: {},
        uuid: "dc143517-757c-41f4-95a1-4e85e38ad834",
        _class: "pipeline",
        reason: ""
      }
    ],
    metadata: {
      count: 0,
      startTime: "2019-05-08T18:40:08.831Z",
      endTime: "2019-05-08T18:40:08.831Z"
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        NoopAnimationsModule,
        RouterTestingModule
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: UserService,
          useValue: {
            user$: of({
              username: 'ecarino'
            })
          }
        },
        AlarmService,
        AlertService
      ]
    });

    service = TestBed.inject(AlarmService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should fetch list of alerts', () => {
    service
      .getAlarms()
      .subscribe((alarms: Alarm[]) => {
        expect(alarms.length).toBe(2);
        expect(alarms[0].severity).toBe(Severity.Info);
        expect(alarms[1].severity).toBe(Severity.Critical);
        expect(alarms[0].timestamp).toBe('1547563847');
        expect(alarms[1].timestamp).toBe('1547571234');
      });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms?index=0`);
    expect(req.request.method).toBe('GET');
    req.flush(mockAlerts);
  });

  it('should fetch list of only critical alerts', () => {
    service
      .getAlarms({
        severity: [Severity.Critical]
      })
      .subscribe(() => { });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms?severity=critical&index=0`);
    expect(req.request.method).toBe('GET');
    req.flush(mockAlerts);
  });

  it('should display server connection error', () => {
    service.streamAlarmsCount$.subscribe(() => { });

    spyOn<any>(service['_alert'], 'error');

    httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`).flush({}, {
      status: 0,
      statusText: ''
    });

    expect(service['_alert'].error).toHaveBeenCalledWith(AlertType.ERROR_SERVER_CONNECTION_RETRY);
  });

  it('should display banner when new alarm detected, even if explicitly hidden', () => {
    const localStorageService: LocalStorageService = TestBed.inject(LocalStorageService);
    localStorageService.write(LocalStorageKey.HIDE_ALARM_BANNER, true);
    localStorageService.write(LocalStorageKey.UNREAD_CRITICAL_ALARM_COUNT, 33);

    service.streamAlarmsCount$.subscribe((count: AlarmsCount) => {
      expect(count.critical).toBe(34);
      expect(count.nonCritical).toBe(5);
      expect(count.total).toBe(39);
    });

    spyOn(service['_banner$'], 'next').and.callFake((visible: boolean) => {
      expect(visible).toBe(true);
    });

    const criticalReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    const criticalResp: InlineResponse200 = { count: 34 };
    const errorReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    const errorResp: InlineResponse200 = { count: 5 };

    criticalReq.flush(criticalResp);
    errorReq.flush(errorResp);
  });

  it('should not display banner when explicitly hidden', () => {
    const localStorageService: LocalStorageService = TestBed.inject(LocalStorageService);
    localStorageService.write(LocalStorageKey.HIDE_ALARM_BANNER, true);
    localStorageService.write(LocalStorageKey.UNREAD_CRITICAL_ALARM_COUNT, 34);

    service.streamAlarmsCount$.subscribe((count: AlarmsCount) => {
      expect(count.critical).toBe(34);
      expect(count.nonCritical).toBe(5);
      expect(count.total).toBe(39);
    });

    spyOn(service['_banner$'], 'next');

    const criticalReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    const criticalResp: InlineResponse200 = { count: 34 };
    const errorReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    const errorResp: InlineResponse200 = { count: 5 };

    criticalReq.flush(criticalResp);
    errorReq.flush(errorResp);

    expect(service['_banner$'].next).not.toHaveBeenCalled();
  });

  it('should display banner by default', () => {
    const localStorageService: LocalStorageService = TestBed.inject(LocalStorageService);
    localStorageService.delete(LocalStorageKey.HIDE_ALARM_BANNER);
    localStorageService.delete(LocalStorageKey.UNREAD_CRITICAL_ALARM_COUNT);

    service.streamAlarmsCount$.subscribe((count: AlarmsCount) => {
      expect(count.critical).toBe(34);
      expect(count.nonCritical).toBe(5);
      expect(count.total).toBe(39);
    });

    spyOn(service['_banner$'], 'next').and.callFake((visible: boolean) => {
      expect(visible).toBe(true);
    });

    const criticalReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    const criticalResp: InlineResponse200 = { count: 34 };
    const errorReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    const errorResp: InlineResponse200 = { count: 5 };

    criticalReq.flush(criticalResp);
    errorReq.flush(errorResp);
  });

  it('should periodically poll alarms count', fakeAsync(() => {
    let criticalCount: number;
    let nonCriticalCount: number;
    let totalCount: number;

    const subscription = service.streamAlarmsCount$.subscribe((count: AlarmsCount) => {
      criticalCount = count.critical;
      nonCriticalCount = count.nonCritical;
      totalCount = count.total;
    });

    let criticalReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    let errorReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    let criticalResp: InlineResponse200 = { count: 34 };
    let errorResp: InlineResponse200 = { count: 5 };

    criticalReq.flush(criticalResp);
    errorReq.flush(errorResp);
    expect(criticalCount).toBe(34);
    expect(nonCriticalCount).toBe(5);
    expect(totalCount).toBe(39);

    tick(5000);
    criticalReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    errorReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    criticalResp = { count: 20 };
    errorResp = { count: 10 };
    criticalReq.flush(criticalResp);
    errorReq.flush(errorResp);
    expect(criticalCount).toBe(20);
    expect(nonCriticalCount).toBe(10);
    expect(totalCount).toBe(30);

    subscription.unsubscribe();
  }));

  it('should kick user to login page if 403 received', () => {
    spyOn(service['_authenticationService'], 'logout');

    service.streamAlarmsCount$.subscribe();
    httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`).flush({}, {
      status: HttpError.Forbidden,
      statusText: ''
    });

    expect(service['_authenticationService'].logout).toHaveBeenCalled();
  });
});
