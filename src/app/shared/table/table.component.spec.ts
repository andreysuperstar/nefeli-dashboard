import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DecimalPipe } from '@angular/common';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { TableComponent, Column, ColumnType, BadgeColor, Row } from './table.component';
import { AvatarComponent } from '../avatar/avatar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { UserRow, TenantRow } from './row-types';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  const mockTenantRow: TenantRow = {
    id: '1',
    name: 'tenant'
  };

  const mockUserRow: UserRow = {
    user: {
      username: 'test',
      name: '',
      email: ''
    },
    role: [],
    tenant: []
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        RouterTestingModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        TableComponent,
        AvatarComponent
      ],
      providers: [DecimalPipe]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render data source', () => {
    component.data = [{ name: 'one' }, { name: 'two' }, { name: 'three' }];
    component.columns = [{ name: 'name' }];
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelectorAll('mat-row').length).toBe(3);
  });

  it('should render only columns from displayedColumns attribute', () => {
    component.data = [
      { num: 'one', age: 1, hidden: 'hidden' },
      { num: 'two', age: 2, hidden: 'hidden' },
      { num: 'three', age: 3, hidden: 'hidden' }
    ];

    component.columns = [{ name: 'num' }, { name: 'age' }];

    fixture.detectChanges();

    const lastRowEl = fixture.debugElement.nativeElement.querySelector('mat-row:last-child');
    const lastRowCellEls = lastRowEl.querySelectorAll('mat-cell');

    expect(lastRowCellEls.length).toBe(2);

    const lastCellEl = lastRowEl.querySelector('mat-cell:last-child');

    expect(lastCellEl.textContent.trim()).toBe('3');
  });

  it('should find correct class for the badge', () => {
    component.data = [{ state: 'good' }, { state: 'warning' }, { state: 'error' }];
    const column = <Column> { name: 'state', type: ColumnType.BADGE, classes: [['good', BadgeColor.green], ['warning', BadgeColor.yellow], ['error', BadgeColor.red]] };
    component.columns = [column];
    expect(component.getBadgeClassName(column, component.dataSource.data[0])).toBe('green');
  });

  it('should find badge class no matter the letter case', () => {
    component.data = [{ state: 'GooD' }, { state: 'wArnInG' }, { state: 'eRRoR' }];
    const column = <Column> { name: 'state', type: ColumnType.BADGE, classes: [['good', BadgeColor.green], ['warning', BadgeColor.yellow], ['error', BadgeColor.red]] };
    component.columns = [column];
    expect(component.getBadgeClassName(column, component.dataSource.data[0])).toBe('green');
  });

  it('should have cursor pointer when clickable', () => {
    component.data = [{ state: 'Good' }];

    fixture.detectChanges();

    const rowEl = fixture.debugElement.nativeElement.querySelector('mat-row');

    // by default, not clickable
    expect(rowEl.classList.contains('clickable')).toBe(false);

    // now make rows clickable
    component.clickable = true;

    fixture.detectChanges();

    expect(rowEl.classList.contains('clickable')).toBe(true);
  });

  it('should emit event when row is clicked', () => {
    spyOn(component['rowClick'], 'emit');

    const dataSource: Row = {
      id: '1'
    };

    component.data = [dataSource];
    component.clickable = true;

    fixture.detectChanges();

    const rowEl = fixture.debugElement.nativeElement.querySelector('mat-row');
    rowEl.click();

    expect(component['rowClick'].emit).toHaveBeenCalledWith(dataSource);
  });

  it('should set empty and undefined cell value', () => {
    component.data = [
      { text: '', num: undefined },
    ];

    component.columns = [{ name: 'text' }, { name: 'num' }];

    fixture.detectChanges();

    const col1 = fixture.debugElement.nativeElement
      .querySelector('mat-row')
      .querySelector('mat-cell:first-child');

    const col2 = fixture.debugElement.nativeElement
      .querySelector('mat-row')
      .querySelector('mat-cell:last-child');

    expect(col1.textContent.trim()).toBe('');
    expect(col2.textContent.trim()).toBe('');
  });

  it('should set cell value to 0', () => {
    component.data = [{ num: 0 }];
    component.columns = [{ name: 'num' }];

    fixture.detectChanges();

    const col = fixture.debugElement.nativeElement.querySelector('mat-cell');

    expect(col.textContent.trim()).toBe('0');
  });

  it('should round long float numbers', () => {
    component.data = [
      { num: 123.456789012345 },
      { num: 9.6789 }
    ];

    component.columns = [{ name: 'num' }];

    fixture.detectChanges();

    const col1 = fixture.debugElement.nativeElement
      .querySelector('mat-row')
      .querySelector('mat-cell');

    const col2 = fixture.debugElement.nativeElement
      .querySelector('mat-row:last-child')
      .querySelector('mat-cell');

    expect(col1.textContent.trim()).toBe('123');
    expect(col2.textContent.trim()).toBe('10');
  });

  it('should set string value', () => {
    component.data = [{ name: 'Service 1' }];
    component.columns = [{ name: 'name' }];

    fixture.detectChanges();

    const col = fixture.debugElement.nativeElement
      .querySelector('mat-row')
      .querySelector('mat-cell');

    expect(col.textContent.trim()).toBe('Service 1');
  });

  it('should handle row clicks for tenant or user rows', () => {
    component.data = [
      { name: 'one', id: '1' },
      { name: 'two', id: '2' },
      { name: 'three', id: '3' }
    ];
    component.columns = [{ name: 'select' }, { name: 'id' }, { name: 'name' }];
    component.clickable = true;

    spyOn<any>(component['_rowClick'], 'emit');
    component.onRowClick(mockTenantRow);
    expect(component['_rowClick'].emit).toHaveBeenCalledWith(mockTenantRow);

    component.ngOnInit();
    spyOn<any>(component['_selection'], 'toggle');
    component.onRowClick(mockUserRow);
    expect(component['_selection'].toggle).toHaveBeenCalledWith(mockUserRow.user.username);
  });

  it('should check if all the rows selected', () => {
    const mockData = [
      { name: 'one', id: '1' },
      { name: 'two', id: '2' },
      { name: 'three', id: '3' }
    ];
    component.data = mockData;
    component.columns = [{ name: 'select' }, { name: 'id' }, { name: 'name' }];
    component.ngOnInit();

    let isAllSelected = component.isAllSelected();
    expect(isAllSelected).toBe(false);

    component.selection.select(mockData[0]);
    component.selection.select(mockData[1]);
    component.selection.select(mockData[2]);
    isAllSelected = component.isAllSelected();
    expect(isAllSelected).toBe(true);
  });

  it('should toggle row selection', () => {
    component.columns = [{ name: 'select' }, { name: 'id' }, { name: 'name' }];
    component.clickable = true;
    component.ngOnInit();
    const spy = spyOn<any>(component['_selection'], 'toggle');
    component.toggleSelection(mockTenantRow);
    expect(component['_selection'].toggle).toHaveBeenCalledWith(mockTenantRow);

    spy.calls.reset();
    component.toggleSelection(mockUserRow);
    expect(component['_selection'].toggle).toHaveBeenCalledWith(mockUserRow.user.username);
  });

  it('should toggle all the rows', () => {
    component.data = [
      { name: 'one', id: '1' },
      { name: 'two', id: '2' },
      { name: 'three', id: '3' }
    ];
    component.columns = [{ name: 'select' }, { name: 'id' }, { name: 'name' }];
    component.clickable = true;
    component.ngOnInit();
    expect(component.isAllSelected()).toBe(false);

    component.masterToggle();
    expect(component.isAllSelected()).toBe(true);

    component.masterToggle();
    expect(component.isAllSelected()).toBe(false);
  });

  it('should render mat table rows', () => {
    spyOn<any>(component['_table'], 'renderRows');
    component.renderRows();
    expect(component['_table'].renderRows).toHaveBeenCalled();
  });
});
