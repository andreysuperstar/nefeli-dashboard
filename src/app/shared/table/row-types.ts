import { TableActionButton } from './table.component';

export interface UserRow {
  user: {
    username: string;
    name: string;
    email: string;
  };
  role: string[];
  tenant: string[][];
}

export interface TenantRow {
  id: string;
  name: string;
  services?: number;
  throughputOut?: number | string;
  packetLossTotal?: number;
  actions?: TableActionButton[];
}

export interface SiteRow {
  id: string;
  name?: string;
  site?: string;
  ip?: string;
  actions?: TableActionButton[];
}

export interface TemplateRow {
  id: string;
  name?: string;
  description: string;
  tenant: string;
  actions?: TableActionButton[];
}
