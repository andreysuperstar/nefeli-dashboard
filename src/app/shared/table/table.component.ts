/**
 * Table Component
 *
 * Usage
 *
 * <nef-table [columns]="columns"
 *            [data]="dataSource"
 *            [clickable]="true"
 *            (rowClick)="onRowClick($event)"
 *            (rowsSelect)="onRowsSelect($event)"
 *            (userEdit)="onUserEdit($event)"
 *            (actionClick)="onActionClick($event)">
 * </nef-table>
 *
 * Attributes
 *
 * [columns]: Column[] - array of columns
 * [data]: any - array of data sets for each row, interface should match the set of column names
 * clickable: boolean - enable click event emit and makes table rows pointer to be cursor on hover
 *
 * Events
 *
 * (rowClick): TenantRow - click handler for the row, applied when [clickable]="true"
 * (rowsSelect): any[] - row selection handler
 * (userEdit): string - click handler for edit user
 * (actionClick): ActionEvent - click handler for action buttons (for ColumnType.ACTION)
 */
import { Component, OnInit, OnDestroy, Input, ViewChild, Output, EventEmitter, AfterViewInit } from '@angular/core';
import { SelectionModel, SelectionChange, DataSource } from '@angular/cdk/collections';
import { MatSort, MatSortable, Sort } from '@angular/material/sort';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { DecimalPipe } from '@angular/common';

import { Subscription, merge } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import { SortDirection } from '@angular/material/sort';
import { TenantRow, UserRow, SiteRow, TemplateRow } from './row-types';

export enum ColumnType {
  ICON = 'icon',
  BADGE = 'badge',
  SELECT = 'select',
  USER = 'user',
  ROLE = 'role',
  TENANT = 'tenant',
  ACTIONS = 'actions',
  LINK = 'link',
  TOOLTIP = 'tooltip',
  LIST = 'list'
}

export interface Column {
  label?: string;
  name: string;
  sortable?: boolean;
  type?: ColumnType;
  classes?: Array<[string, BadgeColor]>;
}

export enum BadgeColor {
  green = 'green',
  yellow = 'yellow',
  red = 'red',
  blue = 'blue',
  grey = 'grey'
}

export enum TableActions {
  Edit = 'edit',
  Remove = 'remove',
  Package = 'package',
  Details = 'details'
}

export interface TableActionButton {
  action: TableActions;
  icon?: string;
  text: string;
}

export interface TableActionEvent<D = any> {
  type: TableActions;
  data: D;
  event: MouseEvent;
  elem?: unknown;
}

export interface Link {
  label: string;
  route?: (string | number)[];
  tooltip?: string;
  icon?: string;
  classes?: string[];
}

type SelectionRow = string[] | any;

export type Row = UserRow | TenantRow | SiteRow | TemplateRow;

export type CssClassNames = {
  [klass: string]: boolean
};

export type RowClassNamesFunc = (row: Row) => CssClassNames;

export interface TableDataSource<R> extends DataSource<R> {
  total: number;
  rows: R[];
  fetch(index?: number, pageSize?: number, sortColumn?: string, sortDirection?: SortDirection): void;
}

@Component({
  selector: 'nef-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.less']
})
export class TableComponent implements OnInit, OnDestroy, AfterViewInit {

  private _columns: Column[] = [];
  private _displayedColumns: string[];
  private _dataSource = new MatTableDataSource();
  private _serverDataSource: TableDataSource<Row>;
  private _clickable: boolean;
  private _selection: SelectionModel<SelectionRow>;
  private _selectionSubscription: Subscription;
  private _rowClassNames: RowClassNamesFunc;

  @Input('data')
  public set data(data: object[]) {
    this._dataSource.data = data;

    if (this._dataSource.data) {
      this._dataSource.sort = this.sort;
    }

    if (this.hasSelectColumn && this._selection) {
      this._selection.clear();
    }
  }

  @Input('columns')
  public set columns(columns: Column[]) {
    this._columns = columns;
    this._displayedColumns = this._columns.map((column: Column): string => column.name);
  }

  @Input('clickable')
  public set clickable(clickable: boolean) {
    this._clickable = clickable;
  }

  @Input('canEditUser')
  public set canEditUser(canEdit: boolean) {
    this._canEditUser = canEdit;
  }

  @Input('pagination')
  public set pagination(paginate: boolean) {
    this._pagination = paginate;
  }

  @Input('dataSource')
  public set serverDataSource(dataSource: TableDataSource<Row>) {
    this._serverDataSource = dataSource;
  }

  @Input('sortable')
  public set sortable(sortable: boolean) {
    this._sortable = sortable;
  }

  @Input('rowClassNames')
  public set rowClassNames(val: RowClassNamesFunc) {
    this._rowClassNames = val;
  }

  @ViewChild('table', { static: true }) private _table: MatTable<any>;
  @ViewChild(MatSort, { static: true }) public sort: MatSort;
  @ViewChild(MatPaginator) private _paginator: MatPaginator;

  private _rowClick = new EventEmitter<Row>();
  private _rowsSelect = new EventEmitter<SelectionRow[]>();
  private _userEdit = new EventEmitter<string>();
  private _actionClick = new EventEmitter<TableActionEvent>();
  private _canEditUser = false;
  private _pagination = false;
  private _sortable = true;
  private _sortSubscription: Subscription;
  private _pageSubscription: Subscription;

  @Output() public get rowClick(): EventEmitter<Row> {
    return this._rowClick;
  }

  @Output() public get rowsSelect(): EventEmitter<SelectionRow[]> {
    return this._rowsSelect;
  }

  @Output() public get userEdit(): EventEmitter<string> {
    return this._userEdit;
  }

  @Output() public get actionClick(): EventEmitter<TableActionEvent> {
    return this._actionClick;
  }

  constructor(private _formatNumber: DecimalPipe) { }

  public ngOnInit() {
    this._dataSource.paginator = this._paginator;
    this.initRowSelection();
  }

  public ngAfterViewInit() {
    // reset the paginator after sorting
    if (this.sort && this._paginator) {
      this._sortSubscription = this.sort.sortChange.subscribe(() => this._paginator.pageIndex = 0);

      this._pageSubscription = merge(
        this.sort.sortChange,
        this._paginator.page
      )
        .pipe(
          tap(() => this.loadPage())
        )
        .subscribe();
    }
  }

  public ngOnDestroy() {
    if (this._selectionSubscription) {
      this._selectionSubscription.unsubscribe();
    }

    if (this._sortSubscription) {
      this._sortSubscription.unsubscribe();
    }

    if (this._pageSubscription) {
      this._pageSubscription.unsubscribe();
    }
  }

  private loadPage() {
    const { pageIndex, pageSize } = this._paginator;
    this._selection?.clear?.();
    this.serverDataSource.fetch(
      pageIndex >= 1 ? pageIndex * pageSize : pageIndex,
      pageSize,
      this.sort.active,
      this.sort.direction
    );
  }

  private initRowSelection() {
    if (this.hasSelectColumn) {
      this._selection = new SelectionModel<SelectionRow>(true, []);

      this._selectionSubscription = this._selection.changed
        .pipe(debounceTime(0))
        .subscribe(
          (_: SelectionChange<SelectionRow>) => this._rowsSelect.emit(this.selection.selected)
        );
    }
  }

  public onRowClick(row: Row) {
    if (this._clickable) {
      if (this._selection) {
        if (row.hasOwnProperty('user')) {
          const userRow: UserRow = row as UserRow;
          this._selection.toggle(userRow.user.username);
          return;
        }
      }
      this._rowClick.emit(row);
    }
  }

  public onEditUser(event: MouseEvent, username: string) {
    event.stopPropagation();
    this._userEdit.emit(username);
  }

  public onActionClick(e: TableActionEvent) {
    e.event.stopPropagation();
    this._actionClick.emit(e);
  }

  public cellValue(value: string | number): string {
    if (isNaN(Number(value))) {
      return value ? String(value) : '';
    } else {
      return this._formatNumber.transform(value, '1.0-0');
    }
  }

  public getBadgeClassName(column: Column, row: object): string {
    const columnClass = column.classes?.find(el => {
      const value = row[column.name];
      return el[0].toLowerCase() === (value && value.toString().toLowerCase());
    });
    return columnClass ? columnClass[1] : '';
  }

  public getLinkClassName(column: Column, row: object) {
    return row[column.name]?.classes ?? '';
  }

  /** Whether the number of selected elements matches the total number of rows. */
  public isAllSelected(): boolean {
    const numSelected = this._selection.selected.length;
    const numRows = this._pagination ? this._serverDataSource.rows.length : this._dataSource.data.length;
    return numSelected === numRows;
  }

  public setSortDetails(details: MatSortable, disableClear = false) {
    if (this.sort) {
      this.sort.disableClear = disableClear;
      this.sort.sort(details);
    }
  }

  private compare(a: number | string, b: number | string, isAsc: boolean): number {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  public sortData(sort: Sort) {
    const column = this._columns.find((col: Column) => col.name === sort.active);

    if (column?.type === ColumnType.LINK) {
      this._dataSource.data.sort((a, b): number => {
        const isAsc = sort.direction === 'asc';
        return this.compare(a[sort.active].label, b[sort.active].label, isAsc);
      });
    }
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  public masterToggle() {
    const rows = this._pagination ? this._serverDataSource.rows : this._dataSource.data;
    const isAllSelected = this.isAllSelected();
    isAllSelected
      ? this._selection.clear()
      : rows.forEach((row: SelectionRow) => {
        if (row.hasOwnProperty('user')) {
          this._selection.select(row.user.username);
        } else {
          this._selection.select(row as SelectionRow);
        }
      });
  }

  public renderRows() {
    this._table.renderRows();
  }

  public scrollToRow(index: number) {
    const rowElements: Array<HTMLElement> = this._table['_elementRef'].nativeElement.querySelectorAll('mat-row');

    if (rowElements.length <= index) {
      return;
    }

    const rowElement: HTMLElement = rowElements[index];

    if (rowElement) {
      rowElement.scrollIntoView();
    }
  }

  private get hasSelectColumn(): boolean {
    return this._columns.some(
      ({ name }: Column): boolean => name === 'select'
    );
  }

  public get ColumnType(): typeof ColumnType {
    return ColumnType;
  }

  public get dataSource(): MatTableDataSource<any> {
    return this._dataSource;
  }

  public get serverDataSource(): TableDataSource<Row> {
    return this._serverDataSource;
  }

  public get columns(): Column[] {
    return this._columns;
  }

  public get displayedColumns(): string[] {
    return this._displayedColumns;
  }

  public get clickable(): boolean {
    return this._clickable;
  }

  public get selection(): SelectionModel<SelectionRow> {
    return this._selection;
  }

  public get canEditUser(): boolean {
    return this._canEditUser;
  }

  public get pagination(): boolean {
    return this._pagination;
  }

  public get sortable(): boolean {
    return this._sortable;
  }

  public isSelected(row: SelectionRow) {
    if (row.hasOwnProperty('user')) {
      return this._selection.isSelected(row.user.username);
    } else {
      return this._selection.isSelected(row);
    }
  }

  public toggleSelection(row: SelectionRow) {
    if (row.hasOwnProperty('user')) {
      return this._selection.toggle(row.user.username);
    } else {
      return this._selection.toggle(row);
    }
  }

  public getRowClassNames(row: Row): CssClassNames {
    return {
      ...this._rowClassNames?.(row),
      clickable: this.clickable
    };
  }
}
