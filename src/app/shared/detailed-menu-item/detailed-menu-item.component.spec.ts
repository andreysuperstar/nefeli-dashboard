import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { DetailedMenuItemComponent } from './detailed-menu-item.component';

describe('DetailedMenuItemComponent', () => {
  let component: DetailedMenuItemComponent;
  let fixture: ComponentFixture<DetailedMenuItemComponent>;

  const mockTitle = 'Users';
  const mockDescription = 'Add or remove Tapestry user accounts';
  const mockIcon = 'assets/icon-panel-users.svg';
  const mockIconWidth = 32;
  const mockIconHeight = 35;
  const mockLink = ['/users'];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [DetailedMenuItemComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailedMenuItemComponent);
    component = fixture.componentInstance;

    component.title = mockTitle;
    component.description = mockDescription;
    component.icon = mockIcon;
    component.iconWidth = mockIconWidth;
    component.iconHeight = mockIconHeight;
    component.link = mockLink;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title, description, icon and link', () => {
    expect(component.title).toBe(mockTitle, 'set title');
    expect(component.description).toBe(mockDescription, 'set description');
    expect(component.icon).toBe(mockIcon, 'set icon');
    expect(component.iconWidth).toBe(mockIconWidth, 'set width');
    expect(component.iconHeight).toBe(mockIconHeight, 'set height');
    expect(component.link).toEqual(mockLink, 'set link');

    const linkDe = fixture.debugElement.query(By.css('a'));

    const iconEl = linkDe.query(By.css('img')).nativeElement as HTMLImageElement;
    const titleDe = linkDe.query(By.css('h1'));
    const descriptionDe = linkDe.query(By.css('p'));

    expect(linkDe.nativeElement.getAttribute('href')).toBe(
      mockLink.join('/'),
      'valid link reference'
    );

    expect(iconEl.src.endsWith(mockIcon)).toBeTruthy('valid icon source');
    expect(iconEl.width).toBe(mockIconWidth, 'valid icon width');
    expect(iconEl.height).toBe(mockIconHeight, 'valid icon height');
    expect(iconEl.alt).toBe(mockTitle, 'valid icon alternate text');
    expect(titleDe.nativeElement.textContent).toBe(mockTitle, 'render title text');

    expect(descriptionDe.nativeElement.textContent)
      .toBe(mockDescription, 'render description text');
  });
});
