import { Component, Input } from '@angular/core';

@Component({
  selector: 'nef-detailed-menu-item',
  templateUrl: './detailed-menu-item.component.html',
  styleUrls: ['./detailed-menu-item.component.less']
})
export class DetailedMenuItemComponent {
  private _title: string;
  private _description: string;
  private _icon: string;
  private _iconWidth: number;
  private _iconHeight: number;
  private _link: string[];

  @Input() public set title(title: string) {
    this._title = title;
  }

  @Input() public set description(description: string) {
    this._description = description;
  }

  @Input() public set icon(icon: string) {
    this._icon = icon;
  }

  @Input() public set iconWidth(width: number) {
    this._iconWidth = width;
  }

  @Input() public set iconHeight(height: number) {
    this._iconHeight = height;
  }

  @Input() public set link(link: string[]) {
    this._link = link;
  }

  public get title(): string {
    return this._title;
  }

  public get description(): string {
    return this._description;
  }

  public get icon(): string {
    return this._icon;
  }

  public get iconWidth(): number {
    return this._iconWidth;
  }

  public get iconHeight(): number {
    return this._iconHeight;
  }

  public get link(): string[] {
    return this._link;
  }
}
