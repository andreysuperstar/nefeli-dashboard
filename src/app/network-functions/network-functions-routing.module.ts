import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NetworkFunctionsComponent } from './network-functions.component';

const routes: Routes = [
  {
    path: '',
    component: NetworkFunctionsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NetworkFunctionsRoutingModule { }
