import * as yaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';

import { Subscription, Observable } from 'rxjs';

import { NFCatalog } from '../pipeline/network-function.model';
import { NetworkFunction } from '../pipeline/network-function.model';

import { AlertService, AlertType } from '../shared/alert.service';
import { NetworkFunctionService } from '../pipeline/network-function.service';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { UserService } from '../users/user.service';
import { HttpError } from 'src/app/error-codes';

export enum NFFormType {
  None,
  New,
  Edit
}

const defaultVendor = 'Nefeli Networks';

@Component({
  selector: 'nef-network-functions',
  templateUrl: './network-functions.component.html',
  styleUrls: ['./network-functions.component.less']
})
export class NetworkFunctionsComponent implements OnInit, OnDestroy {

  private _catalog: NFCatalog;
  private _nFFormType: NFFormType;
  private _nFForm: FormGroup;
  private _selectedNF: NetworkFunction;
  private _editNFSubscription: Subscription;
  private _updateNFSubscription: Subscription;
  private _fetchCatalogSubscription: Subscription;
  private _viewFileManager = false;

  constructor(
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _nFService: NetworkFunctionService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this.fetchNFCatalog();
    this.initNFForm();
  }

  public ngOnDestroy() {
    if (this._editNFSubscription) {
      this._editNFSubscription.unsubscribe();
    }

    if (this._updateNFSubscription) {
      this._updateNFSubscription.unsubscribe();
    }

    if (this._fetchCatalogSubscription) {
      this._fetchCatalogSubscription.unsubscribe();
    }
  }

  private fetchNFCatalog() {
    if (this._fetchCatalogSubscription) {
      this._fetchCatalogSubscription.unsubscribe();
    }

    this._fetchCatalogSubscription = this._nFService
      .getSystemNFs()
      .subscribe((catalog: NFCatalog) => {
        return this._catalog = catalog;
      });
  }

  private initNFForm() {
    this._nFForm = this._fb.group({
      manifest: ['', Validators.required]
    });
  }

  public onAddNF() {
    this._viewFileManager = false;
    this._nFFormType = NFFormType.New;
    this._nFForm.reset();
  }

  public onEditNF(nF: NetworkFunction) {
    this._selectedNF = nF;
    this._nFFormType = NFFormType.Edit;
    try {
      const manifest = this.getNFManifest(nF);
      this._nFForm.get('manifest').setValue(manifest);
    } catch (e) {
      this._alertService.error(AlertType.ERROR_INVALID_MANIFEST);
    }
  }

  private getNFManifest(NF: NetworkFunction): string {
    const nF: NetworkFunction = this.getNFById(NF.local.id);
    return yaml.safeDump(nF, { indent: 4, skipInvalid: true });
  }

  public onCancelNF() {
    this._nFForm.reset();

    this._nFFormType = NFFormType.None;
  }

  public submitNFForm() {
    const { value, invalid } = this._nFForm;
    const { manifest } = value;
    let nF: NetworkFunction;
    let vendor: string;

    if (invalid) {
      return;
    }

    try {
      // parse nf and nf vendor from manifest file
      [nF, vendor] = this.parseManifest(manifest);
    } catch (e) {
      this._alertService.error(AlertType.ERROR_INVALID_MANIFEST);
      return;
    }

    if (!nF.name) {
      this.nFForm.get('manifest').setErrors({ manifest: true });
      return;
    }

    if (this._nFFormType === NFFormType.New) {
      this._nFService.getNF(nF.name).subscribe(
        (response: NFDescriptor) => {
          this._alertService.error(AlertType.ERROR_DUPLICATE_NAME);
        },
        (error: HttpErrorResponse) => {
          if (error.status === HttpError.NotFound) {
            this.createNF(nF, manifest, vendor);
          } else {
            this._alertService.error(AlertType.ERROR_CREATE_CLUSTER_NF);
          }
        }
      );
    } else if (this._nFFormType === NFFormType.Edit) {
      this.editNF(nF);
    }
  }

  private editNF(nF: NetworkFunction) {
    // initial ID of selected NF is utilized as NF manifest containing ID is editable input
    const id = this._selectedNF.local?.id ?? this._selectedNF.identifier;

    if (!id) {
      this._alertService.error(AlertType.ERROR_UPDATE_CLUSTER_NF_NO_ID);
    } else {
      const manifest: NFDescriptor = this.getNFDescriptor(nF);
      this._nFService.putNF(id, manifest)
        .subscribe(
          () => {
            this._alertService.info(AlertType.INFO_EDIT_CLUSTER_NF_SUCCESS);
            this.fetchNFCatalog();
          },
          ({ status }: HttpErrorResponse) => {
            if (status === HttpError.Conflict) {
              this._alertService.error(AlertType.ERROR_UPDATE_CLUSTER_NF_IN_USE);
            } else {
              this._alertService.error(AlertType.ERROR_UPDATE_CLUSTER_NF);
            }
          }
        );
    }
  }

  // TODO (anton): putNF requires a manifest of the type NFDescriptor and doesn't accept NetworkFunction fields
  // Refactor to put NetworkFunction fields under local pattern. JIRA: https://nefeli.atlassian.net/browse/NEF-4917
  private getNFDescriptor(nF: NetworkFunction): NFDescriptor {
    const manifest = cloneDeep(nF);
    delete manifest.local;
    return manifest as NFDescriptor;
  }

  private createNF(nF: NetworkFunction, manifest: string, vendor: string) {
    const updated: NetworkFunction = yaml.safeLoad(manifest) as object;
    delete updated.local;
    manifest = yaml.safeDump(updated, { indent: 2, skipInvalid: true });
    this._nFService
      .postNf(manifest)
      .subscribe(
        (nFDescriptor: NFDescriptor) => {
          this._nFFormType = NFFormType.None;
          nF.local = nF.local ?? { vendor };
          nF.local.id = nFDescriptor.identifier;

          if (!this._catalog[vendor]) {
            this._catalog[vendor] = [];
          }

          this._catalog[vendor].push(nF);
          this._nFForm.reset();
          this._alertService.info(AlertType.INFO_CREATE_CLUSTER_NF_SUCCESS);
        },
        ({ error }: HttpErrorResponse) => {
          if (error.message) {
            this._alertService.error(`Could not create network function (${error.message})`);
          } else {
            this._alertService.error(AlertType.ERROR_CREATE_CLUSTER_NF);
          }
        }
      );
  }

  private parseManifest(manifest: string): [NetworkFunction, string] {
    const parsed: NetworkFunction = yaml.safeLoad(manifest) as object;
    return [parsed, parsed.local?.vendor ?? parsed['vendor'] ?? parsed.components?.datapath?.vendor ?? defaultVendor];
  }

  public onFileSelected(fileEvent: Event) {
    const file: File = fileEvent.target['files'][0];
    const reader = new FileReader();

    reader.onload = (() => {
      return (ev: ProgressEvent) => {
        const data = ev.target['result'];
        this._nFForm.get('manifest').setValue(data);
        this._nFForm.get('manifest').markAsDirty();
      };
    })();

    reader.readAsText(file);
  }

  public get catalog(): NFCatalog {
    return this._catalog;
  }

  public get NFFormType(): typeof NFFormType {
    return NFFormType;
  }

  public get nFFormType(): NFFormType {
    return this._nFFormType;
  }

  public get nFForm(): FormGroup {
    return this._nFForm;
  }

  public get selectedNF(): NetworkFunction {
    return this._selectedNF;
  }

  public get viewFileManager(): boolean {
    return this._viewFileManager;
  }

  public set viewFileManager(val: boolean) {
    this._viewFileManager = val;
  }

  public onRemove({ local: { id } }: NetworkFunction) {
    if (id) {
      for (const vendor of Object.keys(this._catalog)) {
        const index = this._catalog[vendor].findIndex(
          (catalogNf: NetworkFunction): boolean => catalogNf.local.id === id
        );

        if (index !== -1) {
          this._catalog[vendor].splice(index, 1);

          // remove the empty vendor item from the list
          if (!this._catalog[vendor].length) {
            delete this._catalog[vendor];
          }

          this._nFFormType = NFFormType.None;

          this._alertService.info(AlertType.INFO_REMOVE_NF_SUCCESS);

          break;
        }
      }
    }
  }

  private getNFById(id: string): NetworkFunction {
    for (const vendor of Object.keys(this._catalog)) {
      const nF: NetworkFunction = this._catalog[vendor].find(
        (catalogNf: NetworkFunction): boolean => catalogNf.local.id === id
      );
      if (nF) {
        return nF;
      }
    }
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
