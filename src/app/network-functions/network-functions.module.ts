import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';

import { PipelineModule } from '../pipeline/pipeline.module';

import { NetworkFunctionsRoutingModule } from './network-functions-routing.module';
import { NetworkFunctionsComponent } from './network-functions.component';

@NgModule({
  declarations: [NetworkFunctionsComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    PipelineModule,
    NetworkFunctionsRoutingModule
  ]
})
export class NetworkFunctionsModule { }
