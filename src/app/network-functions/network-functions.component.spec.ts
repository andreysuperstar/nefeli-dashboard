import { MatInputHarness } from '@angular/material/input/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { of } from 'rxjs';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { NetworkFunction, NFCatalogResponse } from '../pipeline/network-function.model';

import { NetworkFunctionService } from '../pipeline/network-function.service';
import { ClusterService } from '../home/cluster.service';
import { TenantService } from '../tenant/tenant.service';
import { AlertService, AlertType } from '../shared/alert.service';

import { NetworkFunctionsComponent, NFFormType } from './network-functions.component';
import { NetworkFunctionMenuComponent } from '../pipeline/network-function-menu.component';
import { NetworkFunctionComponent } from '../pipeline/network-function.component';
import { NfType } from 'src/app/pipeline/pipeline.model';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { TableComponent } from 'src/app/shared/table/table.component';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { DecimalPipe } from '@angular/common';
import { UserService } from '../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { MatButtonHarness } from '@angular/material/button/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';

describe('NetworkFunctionsComponent', () => {
  let component: NetworkFunctionsComponent;
  let fixture: ComponentFixture<NetworkFunctionsComponent>;
  let httpTestingController: HttpTestingController;
  let userService: UserService;
  let el: HTMLElement;
  let loader: HarnessLoader;

  const mockUsers: User[] = [
    {
      username: 'Eric',
      email: 'eric@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    }
  ];

  const catalogResponse: RESTNFCatalog = {
    descriptors: [
      {
        identifier: 'arista-router',
        name: 'arista-router',
        type: NfType.NATIVE,
        controllerType: 'NF_CONTROLLER',
        components: {
          datapath: {
            vendor: 'arista'
          }
        }
      },
      {
        identifier: 'pan-fw',
        name: 'pan-fw',
        type: NfType.NATIVE,
        controllerType: 'NF_CONTROLLER',
        components: {
          datapath: {
            vendor: 'pan'
          }
        }
      },
      {
        identifier: 'f5-bigip',
        name: 'f5-bigip',
        type: NfType.NATIVE,
        controllerType: 'NF_CONTROLLER',
        components: {
          datapath: {
            vendor: 'f5'
          }
        }
      }
    ]
  };

  const mockNFData = {
    name: 'Router2',
    manifest: `
name: bess_dpi
type: NATIVE
vendor: Nefeli Networks
wrapper: NONE
maxKppsPerInstance: 1000
useCpu: false
supportsScaling: true
controllerType: NO_CONTROLLER
components:
  datapath:
    vendor: arista`
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatExpansionModule,
        MatCardModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        MatDialogModule,
        MatTooltipModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatTableModule,
        MatCheckboxModule,
        MatDividerModule,
        MatPaginatorModule
      ],
      declarations: [
        NetworkFunctionsComponent,
        NetworkFunctionMenuComponent,
        NetworkFunctionComponent,
        TableComponent,
        AvatarComponent
      ],
      providers: [
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({})
          }
        },
        DecimalPipe,
        NetworkFunctionService,
        ClusterService,
        TenantService,
        AlertService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkFunctionsComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;

    userService.setUser(mockUsers[0]);
    fixture.detectChanges();

    httpTestingController.expectOne(`${environment.restServer}/v1/nfs`).flush(catalogResponse);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page heading', () => {
    expect(el.querySelector('header h2').textContent).toBe('Network Functions');
  });

  it('should have \'Add New Network Function\' link', () => {
    expect(el.querySelector('header button.add').textContent).toBe('Add New Network Function');
  });

  it('should have NF List Menu', () => {
    expect(el.querySelector('nef-network-function-menu')).toBeTruthy();
  });

  it('should not render any form initially', () => {
    expect(component['_nFFormType']).toBeUndefined('undefined form type');
    expect(el.querySelector('.content mat-card')).toBeNull('form card isn\'t rendered');
  });

  it('should render Edit NF form for selected NF', async () => {
    const editButtonEl: HTMLButtonElement = el.querySelector('nef-network-function .edit');

    editButtonEl.click();
    fixture.detectChanges();

    expect(el.querySelector('.content mat-card mat-card-header').textContent).toBe('Edit arista-router');
    expect(el.querySelector('#nf-form')).toBeDefined();
    expect((el.querySelector('#nf-form .mat-input-element') as HTMLInputElement).value).toContain('name: arista-router');
    component.nFForm.markAsDirty();

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Save'
    }));
    await submitButton.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/nfs/arista-router`);
    expect(req.request.method).toBe('PUT');
  });

  it('should show alert if edited NF in use', async () => {
    spyOn(component['_alertService'], 'error').and.callThrough();
    const editButtonEl: HTMLButtonElement = el.querySelector('nef-network-function .edit');

    editButtonEl.click();
    fixture.detectChanges();
    component.nFForm.markAsDirty();

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Save'
    }));
    await submitButton.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/nfs/arista-router`);
    req.flush({}, {
      status: 409,
      statusText: 'error',
    });

    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_UPDATE_CLUSTER_NF_IN_USE);
  });

  it('should fetch NF list after successful NF edit', async () => {
    const editButtonEl: HTMLButtonElement = el.querySelector('nef-network-function .edit');
    editButtonEl.click();
    fixture.detectChanges();
    component.nFForm.markAsDirty();

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Save'
    }));
    await submitButton.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/nfs/arista-router`);
    req.flush({});

    httpTestingController.expectOne(`${environment.restServer}/v1/nfs`).flush(catalogResponse);
  });

  it('should render New NF form on add new NF', () => {
    const addNewNFButtonEl: HTMLButtonElement = el.querySelector('header button.add');

    addNewNFButtonEl.click();

    expect(component['_nFFormType']).toBe(NFFormType.New, 'valid \'New\' form type');

    expect(el.querySelector('.content mat-card')).toBeDefined('form card is rendered');

    expect(el.querySelector('#nf-form')).toBeDefined('New NF form is rendered');
    expect(el.querySelector('#nf-form mat-select')).toBeDefined('vendor select is rendered');
  });

  it('shouldn\'t make request on submit invalid New NF form', async () => {
    component.onAddNF();

    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter manifest or import from file'
    }));
    await manifestInput.setValue('Invalid manifest');
    fixture.detectChanges();
    fixture.detectChanges();

    const submitButtonEl: HTMLButtonElement = el.querySelector('button[form=nf-form]');
    expect(submitButtonEl.textContent).toBe('Save', 'valid submit button text');

    spyOn<any>(component, 'submitNFForm').and.callThrough();

    submitButtonEl.click();

    expect(component.submitNFForm).toHaveBeenCalled();

    httpTestingController.expectNone({
      url: `${environment.restServer}/v1/nfs`,
      method: 'POST'
    }, 'expect no request');
  });

  it('should make a request on submit valid New NF form', async () => {
    component.onAddNF();

    fixture.detectChanges();

    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter manifest or import from file'
    }));
    await manifestInput.setValue(mockNFData.manifest);
    fixture.detectChanges();

    const submitButtonEl: HTMLButtonElement = el.querySelector('button[form=nf-form]');
    spyOn<any>(component, 'submitNFForm').and.callThrough();
    submitButtonEl.click();

    let nfRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/nfs/bess_dpi`
    }, 'expect nf get name request');
    nfRequest.flush({}, { status: 404, statusText: 'Not Found' });

    expect(component.submitNFForm).toHaveBeenCalled();

    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/nfs`
    }, 'expect post NF request');

    expect(req.request.method).toBe('POST', 'valid request method');

    expect(req.request.body.trim())
      .toBe(mockNFData.manifest.trim(), 'valid request manifest');

    expect(req.request.headers.get('Content-Type')).toBe('application/x-yaml');

    req.flush(mockNFData);

    fixture.detectChanges();

    expect(component['_nFFormType']).toBe(NFFormType.None, 'valid \'None\' form type after successful request');

    expect(el.querySelector('.content mat-card')).toBeNull('form card isn\'t rendered');
    expect(el.querySelector('#nf-form')).toBeNull('New NF form isn\'t rendered');

    component.onAddNF();
    fixture.detectChanges();

    expect(component.nFForm.get('manifest').value).toBeNull();
    expect(el.querySelector('textarea').textContent).toBe('');
  });

  it('should create NF with default vendor if not present in manifest', async () => {
    component.onAddNF();
    fixture.detectChanges();

    const mockNFData = {
      name: 'Router2',
      manifest: `
name: bess_dpi
type: NATIVE
metadataOptions:
  wrapper: NONE
scalingOptions:
  maxKppsPerInstance: 1000
  useCpu: false
  supportsScaling: true
controllerType: NO_CONTROLLER
`
    };

    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter manifest or import from file'
    }));
    await manifestInput.setValue(mockNFData.manifest);
    fixture.detectChanges();
    const submitButtonEl: HTMLButtonElement = el.querySelector('button[form=nf-form]');
    spyOn<any>(component, 'submitNFForm').and.callThrough();
    submitButtonEl.click();

    let nfRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/nfs/bess_dpi`
    }, 'expect nf get name request');
    nfRequest.flush({}, { status: 404, statusText: 'Not Found' });

    expect(component.submitNFForm).toHaveBeenCalled();
    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/nfs`
    }, 'expect post NF request');

    expect(req.request.method).toBe('POST', 'valid request method');
    req.flush(mockNFData);

    fixture.detectChanges();
    const nfMenuItemHeaders = el.querySelectorAll('nef-network-function-menu mat-panel-title');
    expect(nfMenuItemHeaders[0].textContent).toBe('arista');
    expect(nfMenuItemHeaders[1].textContent).toBe('pan');
    expect(nfMenuItemHeaders[2].textContent).toBe('f5');
    expect(nfMenuItemHeaders[3].textContent).toBe('Nefeli Networks');
  });

  it('should validate NF name duplicate', async () => {
    component.onAddNF();
    fixture.detectChanges();

    const mockNF = {
      name: 'Router2',
      manifest: `
name: bess_dpi
type: NATIVE
vendor: Nefeli Networks
metadataOptions:
  wrapper: NONE
scalingOptions:
  maxKppsPerInstance: 1000
  useCpu: false
  supportsScaling: true
controllerType: NO_CONTROLLER
components:
  datapath:
    vendor: arista`
    };

    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter manifest or import from file'
    }));
    await manifestInput.setValue(mockNF.manifest);
    fixture.detectChanges();
    const submitButtonEl: HTMLButtonElement = el.querySelector('button[form=nf-form]');
    submitButtonEl.click();
    fixture.detectChanges();

    let nfRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/nfs/bess_dpi`
    }, 'expect get NF request');
    nfRequest.flush({} as NFDescriptor);
    httpTestingController.expectNone(`${environment.restServer}/v1/nfs/bess_dpi`);

    await manifestInput.setValue(mockNF.manifest);
    submitButtonEl.click();

    nfRequest = httpTestingController.expectOne(`${environment.restServer}/v1/nfs/bess_dpi`);
    nfRequest.flush({}, { status: 404, statusText: 'Not Found' });
    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/nfs`
    }, 'expect post NF request');

    expect(req.request.body.trim()).toBe(mockNF.manifest.trim());
  });

  it('should make a request on submit Edit NF form', async () => {
    const editButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: 'nef-network-function .edit'
    }));
    await editButton.click();

    fixture.detectChanges();

    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter manifest or import from file'
    }));

    await manifestInput.setValue(mockNFData.manifest);

    const submitButtonEl: HTMLButtonElement = el.querySelector('button[form=nf-form]');
    submitButtonEl.click();

    const editNFRequest = httpTestingController.expectOne({
      method: 'PUT',
      url: `${environment.restServer}/v1/nfs/${catalogResponse.descriptors[0].identifier}`
    });

    expect(editNFRequest.request.headers.get('content-type')).toBe('application/json');

    const jsonBodyKeys = Object.keys(editNFRequest.request.body);
    expect(jsonBodyKeys).toContain('name');
    expect(jsonBodyKeys).toContain('vendor');
    expect(jsonBodyKeys).toContain('type');
    expect(jsonBodyKeys).toContain('controllerType');
    expect(jsonBodyKeys).toContain('components');

    expect(jsonBodyKeys).not.toContain('id');
    expect(jsonBodyKeys).not.toContain('capability');
    expect(jsonBodyKeys).not.toContain('catalogId');
    expect(jsonBodyKeys).not.toContain('tenant');
    expect(jsonBodyKeys).not.toContain('clusterId');
  });

  it('should update nf list after deleting NF', () => {
    const nfLength = el.querySelectorAll('nef-network-function').length;
    const titleLength = el.querySelectorAll('mat-panel-title').length;

    const nF: NetworkFunction = component.catalog['arista'][0];
    component.onRemove(nF);
    fixture.detectChanges();

    expect(el.querySelectorAll('nef-network-function').length).toBe(nfLength - 1);
    expect(el.querySelectorAll('mat-panel-title').length).toBe(titleLength - 1);
  });

  it('should insert manifest from file', waitForAsync(() => {
    const file: File = new File(["this is the manifest file"], "manifest.txt", {
      type: "text",
    });

    const fileList: FileList = {
      length: 1,
      0: file,
      item: (index: number) => {
        return file;
      }
    };

    spyOn<any>(window, "FileReader").and.returnValue({
      onload: () => { },
      readAsText: (f: File) => {
        expect(f.name).toBe("manifest.txt");
        expect(f.type).toBe("text");
      }
    });

    component.onFileSelected({
      target: {
        files: fileList
      }
    } as any);
  }));

  it('should cancel form', () => {
    const addNewNFButtonEl: HTMLButtonElement = el.querySelector('header button.add');
    addNewNFButtonEl.click();
    expect(component['_nFFormType']).toBe(NFFormType.New);
    fixture.detectChanges();

    const textArea: HTMLTextAreaElement = fixture.debugElement.nativeElement.querySelector('textarea');
    expect(textArea).toBeDefined();
    const manifestData = 'My new manifest file';
    textArea.value = manifestData;
    textArea.dispatchEvent(new Event('input'));
    expect(component.nFForm.get('manifest').value).toBe(manifestData);
    expect(component.nFForm.pristine).toBe(false);

    const cancelButton: HTMLButtonElement = fixture.debugElement.nativeElement.querySelectorAll('mat-card-actions button')[0];
    cancelButton.click();
    fixture.detectChanges();

    expect(component.nFForm.pristine).toBe(true);
    expect(component.nFForm.get('manifest').value).toBeFalsy();
    expect(fixture.debugElement.nativeElement.querySelector('textarea')).toBeFalsy();
  });
});
