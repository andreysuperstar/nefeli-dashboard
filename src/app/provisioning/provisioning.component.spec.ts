import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { OverlayContainer, OverlayModule } from '@angular/cdk/overlay';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatRadioModule } from '@angular/material/radio';

import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';

import { ProvisioningComponent } from './provisioning.component';
import { ProvisioningServersComponent } from './provisioning-servers/provisioning-servers.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { HardwareProfilesComponent } from '../hardware-profiles/hardware-profiles/hardware-profiles.component';
import { AddHardwareProfileDialogComponent } from './add-hardware-profile-dialog/add-hardware-profile-dialog.component';
import { HardwareProfilesFormComponent } from '../hardware-profiles/hardware-profiles-form/hardware-profiles-form.component';
import { SoftwareProfilesComponent } from '../software-profiles/software-profiles.component';
import { SoftwareProfileFormComponent } from '../software-profiles/software-profile-form/software-profile-form.component';
import { SoftwareProfileDialogComponent } from '../software-profiles/software-profile-dialog/software-profile-dialog.component';

describe('ProvisioningComponent', () => {
  let component: ProvisioningComponent;
  let fixture: ComponentFixture<ProvisioningComponent>;
  let overlayContainer: OverlayContainer;
  let documentRootLoader: HarnessLoader;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          HttpClientTestingModule,
          ReactiveFormsModule,
          OverlayModule,
          MatCardModule,
          MatFormFieldModule,
          MatInputModule,
          MatButtonModule,
          MatTableModule,
          MatSnackBarModule,
          MatDialogModule,
          MatSelectModule,
          MatNativeDateModule,
          MatDatepickerModule,
          MatRadioModule
        ],
        declarations: [
          ProvisioningComponent,
          ProvisioningServersComponent,
          TableComponent,
          HardwareProfilesComponent,
          AddHardwareProfileDialogComponent,
          HardwareProfilesFormComponent,
          SoftwareProfilesComponent,
          SoftwareProfileFormComponent,
          SoftwareProfileDialogComponent
        ],
        providers: [DecimalPipe]
      })
      .compileComponents();

    overlayContainer = TestBed.inject(OverlayContainer);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvisioningComponent);
    component = fixture.componentInstance;
    documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  afterEach(async () => {
    const dialogs = await documentRootLoader.getAllHarnesses(MatDialogHarness);

    await Promise.all(
      dialogs.map(dialog => dialog.close())
    );

    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render cards', async () => {
    const cards = await loader.getAllHarnesses(MatCardHarness);

    expect(cards.length).toBe(3);

    const [serversTitle, hardwareProfilesTitle, softwareProfilesTitle] = await Promise.all(
      cards.map(card => card.getTitleText())
    );

    expect(serversTitle).toBe('Servers');
    expect(hardwareProfilesTitle).toBe('Hardware Profiles');
    expect(softwareProfilesTitle).toBe('Software Profiles');
  });

  it('should add hardware profile', async () => {
    spyOn(component['_hardwareProfilesComponent'], 'addProfile');

    const addHardwareProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Hardware Profile'
    }));

    await addHardwareProfileButton.click();

    await documentRootLoader.getHarness(MatDialogHarness);

    const mockHardwareProfile: HardwareProfile = {
      identifier: 'profile_1',
      name: 'Profile 1',
      description: '',
      bonds: [
        {
          identifier: 'bond_1',
          bond: {
            purpose: SubnetType.WAN,
            devices: ['device1', 'device2', 'device3']
          }
        },
        {
          identifier: 'bond_2',
          bond: {}
        }
      ]
    };

    component['_dialog'].openDialogs[0].close(mockHardwareProfile);
    fixture.detectChanges();

    fixture
      .whenStable()
      .then(() => {
        expect(component['_hardwareProfilesComponent'].addProfile).toHaveBeenCalledWith(mockHardwareProfile);
      });
  });

  it('should add software profile', async () => {
    spyOn(component['_softwareProfilesComponent'], 'addProfile');

    const addSoftwareProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Software Profile'
    }));

    await addSoftwareProfileButton.click();

    await documentRootLoader.getHarness(MatDialogHarness);

    const mockSoftwareProfile: SoftwareProfile = {
      identifier: '1',
      name: 'Andrew Profile',
      description: 'Andrew\'s profile',
      notBefore: new Date(2030, 1, 2, 11, 24),
      distroVersion: '1'
    };

    component['_dialog'].openDialogs[0].close(mockSoftwareProfile);
    fixture.detectChanges();

    fixture
      .whenStable()
      .then(() => {
        expect(component['_softwareProfilesComponent'].addProfile).toHaveBeenCalledWith(mockSoftwareProfile);
      });
  });
});
