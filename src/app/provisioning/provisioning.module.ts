import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';

import { SharedModule } from '../shared/shared.module';
import { HardwareProfilesModule } from '../hardware-profiles/hardware-profiles.module';
import { SoftwareProfilesModule } from '../software-profiles/software-profiles.module';

import { ProvisioningRoutingModule } from './provisioning-routing.module';

import { ProvisioningComponent } from './provisioning.component';
import { ProvisioningServersComponent } from './provisioning-servers/provisioning-servers.component';
import { AddHardwareProfileDialogComponent } from './add-hardware-profile-dialog/add-hardware-profile-dialog.component';

@NgModule({
  declarations: [
    ProvisioningComponent,
    ProvisioningServersComponent,
    AddHardwareProfileDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    MatDialogModule,
    MatInputModule,
    SharedModule,
    HardwareProfilesModule,
    SoftwareProfilesModule,
    ProvisioningRoutingModule
  ]
})
export class ProvisioningModule { }
