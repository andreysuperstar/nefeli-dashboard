import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

import { HardwareProfilesComponent } from '../hardware-profiles/hardware-profiles/hardware-profiles.component';
import { AddHardwareProfileDialogComponent } from './add-hardware-profile-dialog/add-hardware-profile-dialog.component';
import { SoftwareProfilesComponent } from '../software-profiles/software-profiles.component';
import { SoftwareProfileDialogComponent } from '../software-profiles/software-profile-dialog/software-profile-dialog.component';

const DIALOG_CONFIG: MatDialogConfig = {
  autoFocus: false,
  restoreFocus: false
};

@Component({
  selector: 'nef-provisioning',
  templateUrl: './provisioning.component.html',
  styleUrls: ['./provisioning.component.less']
})
export class ProvisioningComponent {
  private _dialogSubscription: Subscription;

  @ViewChild('hardwareProfiles', {
    static: true
  }) protected _hardwareProfilesComponent: HardwareProfilesComponent;

  @ViewChild('softwareProfiles', {
    static: true
  }) protected _softwareProfilesComponent: SoftwareProfilesComponent;

  constructor(private _dialog: MatDialog) { }

  public onAddHardwareProfile() {
    const dialogRef = this._dialog.open<AddHardwareProfileDialogComponent, unknown, HardwareProfile>(
      AddHardwareProfileDialogComponent,
      {
        ...DIALOG_CONFIG,
        width: '750px'
      }
    );

    this._dialogSubscription?.unsubscribe();

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe(profile => {
        this._hardwareProfilesComponent.addProfile(profile);
      });
  }

  public onAddSoftwareProfile() {
    const dialogRef = this._dialog.open<SoftwareProfileDialogComponent, undefined, SoftwareProfile>(
      SoftwareProfileDialogComponent,
      {
        ...DIALOG_CONFIG,
        width: '510px'
      }
    );

    this._dialogSubscription?.unsubscribe();

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe(profile => {
        this._softwareProfilesComponent.addProfile(profile);
      });
  }
}
