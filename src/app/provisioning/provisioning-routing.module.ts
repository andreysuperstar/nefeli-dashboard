import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProvisioningComponent } from './provisioning.component';

const routes: Routes = [
  {
    path: '',
    component: ProvisioningComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProvisioningRoutingModule { }
