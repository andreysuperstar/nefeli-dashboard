import { Component, Inject, OnDestroy, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { HardwareProfilesFormComponent } from '../../hardware-profiles/hardware-profiles-form/hardware-profiles-form.component';

@Component({
  selector: 'nef-add-hardware-profile-dialog',
  templateUrl: './add-hardware-profile-dialog.component.html',
  styleUrls: ['./add-hardware-profile-dialog.component.less']
})
export class AddHardwareProfileDialogComponent implements OnDestroy {
  private _hardwareProfilesFormSubscription: Subscription;

  @ViewChild('profilesForm', { static: true }) protected _hardwareProfilesForm: HardwareProfilesFormComponent;

  constructor(
    private _dialogRef: MatDialogRef<AddHardwareProfileDialogComponent>
  ) { }

  public ngOnDestroy() {
    this._hardwareProfilesFormSubscription?.unsubscribe();
  }

  public onSave() {
    this._hardwareProfilesFormSubscription?.unsubscribe();

    this._hardwareProfilesFormSubscription = this._hardwareProfilesForm.createdProfile$.pipe(
      take(1)
    ).subscribe((profile: HardwareProfile) => {
      this._dialogRef.close(profile);
    });

    this._hardwareProfilesForm.submitForm();
  }
}
