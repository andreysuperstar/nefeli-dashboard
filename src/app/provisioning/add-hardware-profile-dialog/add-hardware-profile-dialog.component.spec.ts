import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';
import { HardwareProfilesFormComponent } from '../../hardware-profiles/hardware-profiles-form/hardware-profiles-form.component';

import { AddHardwareProfileDialogComponent } from './add-hardware-profile-dialog.component';

describe('AddHardwareProfileDialogComponent', () => {
  let component: AddHardwareProfileDialogComponent;
  let fixture: ComponentFixture<AddHardwareProfileDialogComponent>;
  let loader: HarnessLoader;

  const dialogRefSpy = jasmine.createSpyObj<MatDialogRef<AddHardwareProfileDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  const mockHardwareProfile: HardwareProfile = {
    bonds: [
      {
        identifier: 'bond_1',
        bond: {
          purpose: SubnetType.WAN,
          devices: ['device1', 'device2', 'device3']
        }
      },
      {
        identifier: 'bond_2',
        bond: {}
      }
    ],
    description: '',
    name: 'Profile 1',
    identifier: 'profile_1'
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        MatSelectModule,
        MatOptionModule,
        MatFormFieldModule,
        MatInputModule
      ],
      declarations: [
        AddHardwareProfileDialogComponent,
        HardwareProfilesFormComponent
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: dialogRefSpy
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHardwareProfileDialogComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render dialog', () => {
    const el = fixture.debugElement.nativeElement;

    expect(el.querySelector('h1').textContent).toBe('Add New Hardware Profile');
    expect(el.querySelector('mat-dialog-content > nef-hardware-profiles-form')).toBeDefined();

    const buttons = el.querySelectorAll('mat-dialog-actions > button');
    expect(buttons.length).toBe(2);
    expect(buttons[0].textContent).toBe('Save');
    expect(buttons[1].textContent).toBe('Close');
  });

  it('should submit form on save', async () => {
    spyOn(component['_hardwareProfilesForm'], 'submitForm');

    const saveButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Save'
    }));
    await saveButton.click();

    expect(component['_hardwareProfilesForm'].submitForm).toHaveBeenCalled();
    component['_hardwareProfilesForm']['_createdProfile'].next(mockHardwareProfile);
    expect(component['_dialogRef'].close).toHaveBeenCalledWith(mockHardwareProfile);
  });
});
