import { Component, OnInit } from '@angular/core';
import { ProvisioningService } from 'rest_client/pangolin/api/provisioning.service';
import { ProvisioningURLs } from 'rest_client/pangolin/model/provisioningURLs';
import { ProvisioningURL } from 'rest_client/pangolin/model/provisioningURL';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { URL_PATTERN, HTTPS_PATTERN } from '../../sites-overview/site.validation';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from '../../users/user.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'nef-provisioning-servers',
  templateUrl: './provisioning-servers.component.html',
  styleUrls: ['./provisioning-servers.component.less']
})
export class ProvisioningServersComponent implements OnInit {
  private _urls = new Array<ProvisioningURL>();
  private _form: FormGroup;

  constructor(
    private _provisioningServices: ProvisioningService,
    private _alertService: AlertService,
    private _userService: UserService,
    private _fb: FormBuilder
  ) { }

  public ngOnInit(): void {
    this.getProvisioningServers();
    this.initForm();
  }

  private initForm() {
    this._form = this._fb.group({
      url: ['', [Validators.pattern(URL_PATTERN), Validators.pattern(HTTPS_PATTERN)]]
    });
  }

  private getProvisioningServers() {
    this._provisioningServices.getProvisioningURLs().subscribe((urls: ProvisioningURLs) => {
      this._urls = urls?.provisioningUrls;
    });
  }

  public deleteServer(id: string) {
    this._provisioningServices.deleteProvisioningURL(id).subscribe(
      () => {
        const index = this._urls.findIndex((url: ProvisioningURL) => url.identifier === id);

        if (index >= 0) {
          this._urls.splice(index, 1);
        }

        this._alertService.info(AlertType.INFO_DELETE_PROVISIONING_SERVER);
      },
      () => this._alertService.error(AlertType.ERROR_DELETE_PROVISIONING_SERVER)
    );
  }

  public saveServer(url: string) {
    this._provisioningServices.postProvisioningURL({ url }).subscribe(
      (newUrl: ProvisioningURL) => {
        this._alertService.info(AlertType.INFO_CREATE_PROVISIONING_SERVER);
        this._urls.push(newUrl);
        this._form.get('url').reset();
      },
      () => this._alertService.error(AlertType.ERROR_CREATE_PROVISIONING_SERVER)
    );
  }

  public onSubmit() {
    const { invalid, pristine } = this._form;

    if (invalid || pristine) {
      return;
    }

    const { url } = this._form.value;

    if (url) {
      this.saveServer(url);
    }
  }

  public get urls(): ProvisioningURL[] {
    return this._urls;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
