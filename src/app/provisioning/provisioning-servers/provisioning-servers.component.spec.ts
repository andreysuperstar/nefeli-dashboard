import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProvisioningServersComponent } from './provisioning-servers.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { environment } from 'src/environments/environment';
import { ProvisioningURLs } from 'rest_client/pangolin/model/provisioningURLs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ProvisioningURL } from 'rest_client/pangolin/model/provisioningURL';
import { cloneDeep } from 'lodash-es';
import { UserService } from '../../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';

describe('ProvisioningServersComponent', () => {
  let component: ProvisioningServersComponent;
  let fixture: ComponentFixture<ProvisioningServersComponent>;
  let httpTestingController: HttpTestingController;
  let userService: UserService;

  const mockServers: ProvisioningURLs = {
    provisioningUrls: [{
      identifier: '1',
      url: 'https://provisioning.server.com'
    }, {
      identifier: '2',
      url: 'https://192.168.4.100:2300'
    }, {
      identifier: '3',
      url: 'https://ztp.nefeli.io'
    }]
  };

  const mockSystemAdmin: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule
      ],
      declarations: [ProvisioningServersComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        }, {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProvisioningServersComponent);
    component = fixture.componentInstance;
    userService = TestBed.inject(UserService);
    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display existing provisioning servers', () => {
    const el = fixture.debugElement.nativeElement;

    httpTestingController.expectOne(`${environment.restServer}/v1/provisioning/urls`).flush(mockServers);
    fixture.detectChanges();

    const servers = el.querySelectorAll('span:not(.mat-form-field-label-wrapper):not(.mat-form-field-ripple)');
    expect(servers[0].textContent).toBe(mockServers.provisioningUrls[0].url);
    expect(servers[1].textContent).toBe(mockServers.provisioningUrls[1].url);
    expect(servers[2].textContent).toBe(mockServers.provisioningUrls[2].url);
    expect(servers[3]).toBeUndefined();
  });

  it('should add new provisioning server', () => {
    const mockServer: ProvisioningURL = {
      identifier: '1',
      url: 'https://new.provisioning-server.com'
    };

    const urlInput = component.form.get('url');
    urlInput.patchValue(mockServer.url);
    urlInput.markAsDirty();
    fixture.detectChanges();

    const el = fixture.debugElement.nativeElement;
    el.querySelector('button').click();

    // verify POST request to add server
    const req = httpTestingController.match((request: HttpRequest<any>) => {
      return (request.url === `${environment.restServer}/v1/provisioning/urls`)
        && (request.method === 'POST')
    })[0];
    req.flush(mockServer);
    expect(req.request.body.url).toBe(mockServer.url);
    fixture.detectChanges();

    // verify added to list
    expect(el.querySelectorAll('span:not(.mat-form-field-label-wrapper):not(.mat-form-field-ripple)')[0].textContent).toBe(mockServer.url);
  });

  it('should delete existing provisioning server', () => {
    const el = fixture.debugElement.nativeElement;
    const removeServerIndex = 1;
    const provisioningServers = cloneDeep(mockServers);

    httpTestingController.expectOne(`${environment.restServer}/v1/provisioning/urls`).flush(provisioningServers);
    fixture.detectChanges();

    // verify all three servers listed
    let servers = el.querySelectorAll('span:not(.mat-form-field-label-wrapper):not(.mat-form-field-ripple)');
    expect(servers.length).toBe(provisioningServers.provisioningUrls.length);

    // delete the second server on list
    const removeButton = el.querySelectorAll('button[title="Remove Server"]')[removeServerIndex];
    removeButton.click();

    // verify delete request is issued
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/provisioning/urls/${provisioningServers.provisioningUrls[removeServerIndex].identifier}`);
    expect(req.request.method).toBe('DELETE');
    req.flush({});

    fixture.detectChanges();

    // verify displayed list is updated with server removed
    servers = el.querySelectorAll('span:not(.mat-form-field-label-wrapper):not(.mat-form-field-ripple)');
    expect(servers[0].textContent).toBe(provisioningServers.provisioningUrls[0].url);
    expect(servers[1].textContent).toBe(provisioningServers.provisioningUrls[1].url);
    expect(servers[2]).toBeUndefined();
  });
});
