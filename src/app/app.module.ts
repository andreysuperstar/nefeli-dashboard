import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AppRoutingModule } from './app-routing.module';
import { RequestUrlInterceptor } from './http-interceptors/request-url.interceptor';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { BASE_PATH as ALARMS_BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as MNEME_BASE_PATH } from 'rest_client/mneme/variables';
import { BASE_PATH as HEIMDALLR_BASE_PATH } from 'rest_client/heimdallr/variables';
import { BASE_PATH as STATS_BASE_PATH } from 'rest_client/pangolin/variables';
import { ResponseInterceptor } from './http-interceptors/response.interceptor.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FlexLayoutModule,
    MatSnackBarModule,
    MatListModule,
    AppRoutingModule,
    SharedModule
  ],
  providers: [
    {
      // base path for auto-generated alarms rest_client
      provide: ALARMS_BASE_PATH,
      useValue: '/v1'
    },
    {
      // base path for auto-generated pangolin rest_client
      provide: PANGOLIN_BASE_PATH,
      useValue: '/v1'
    },
    {
      // base path for auto-generated mneme rest_client
      provide: MNEME_BASE_PATH,
      useValue: '/v1'
    },
    {
      // base path for auto-generated heimdallr rest_client
      provide: HEIMDALLR_BASE_PATH,
      useValue: '/auth'
    },
    {
      // base path for auto-generated dashboard rest_client
      provide: STATS_BASE_PATH,
      useValue: '/v1'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestUrlInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
