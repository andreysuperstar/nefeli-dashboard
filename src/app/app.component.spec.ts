import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AlarmService } from './shared/alarm.service';
import { AppComponent } from './app.component';
import { AlarmBannerComponent } from './alarm-banner/alarm-banner.component';
import { AlarmListComponent } from './shared/alarm-list/alarm-list.component';
import { MatChipsModule } from '@angular/material/chips';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        AlarmBannerComponent,
        AlarmListComponent,
      ],
      imports: [
        HttpClientTestingModule,
        RouterModule.forRoot([], {
          relativeLinkResolution: 'legacy'
        }),
        MatSnackBarModule,
        MatCardModule,
        MatListModule,
        ScrollingModule,
        MatTooltipModule,
        MatChipsModule
      ],
      providers: [
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        },
        AlarmService
      ]
    });

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should download log file', () => {
    spyOn(component['_loggerService'], 'downloadLogFile');

    const ev = new KeyboardEvent('keypress', {
      ctrlKey: true,
      shiftKey: true,
      key: 'L'
    });

    document.dispatchEvent(ev);
    expect(component['_loggerService'].downloadLogFile).toHaveBeenCalled();
  });

  it('should display version', () => {
    spyOn(component['_snackBar'], 'open');

    const ev = new KeyboardEvent('keypress', {
      ctrlKey: true,
      shiftKey: true,
      key: 'V'
    });

    document.dispatchEvent(ev);
    expect(component['_snackBar'].open).toHaveBeenCalledWith((window as any).nefeliVersion, 'hide');
  });
});
