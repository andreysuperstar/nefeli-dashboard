import { Injectable } from '@angular/core';
import { HttpEvent, HttpEventType, HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../environments/environment';

import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';
import { LOG_LEVEL, LoggerService } from '../shared/logger.service';

const API_VERSION_MAP = {
  tenants: 'v1',
  sites: 'v1',
  nfs: 'v1',
  license_pools: 'v1'
};

const exceptionList = [
  'www.gravatar.com'
];

@Injectable({
  providedIn: 'root'
})
export class RequestUrlInterceptor implements HttpInterceptor {

  constructor(
    private _localStorageService: LocalStorageService,
    private _loggerService: LoggerService
  ) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const urlPrefix = req.url.split(/\?|\/+/)[1];
    if (exceptionList.includes(urlPrefix)) {
      return next.handle(req);
    }
    const api = environment.restServer;
    const reqUrl = req.url;
    const apiVersion = API_VERSION_MAP[urlPrefix] ? `/${API_VERSION_MAP[urlPrefix]}` : '';
    let headers = req.headers;

    if (!req.headers.has('Content-Type')) {
      headers = req.headers.set('Content-Type', 'application/json');
    }

    if (!req.headers.has('Authorization')) {
      const token = this._localStorageService.read(LocalStorageKey.ACCESS_TOKEN);
      headers = req.headers.set('Authorization', `Bearer ${token}`);
    }

    req = req.clone({
      url: `${api}${apiVersion}${reqUrl}`,
      headers: headers
    });

    return next
      .handle(req)
      .pipe(
        tap(({ type }) => {
          if (type === HttpEventType.Sent) {
            const message = `${req.method} ${req.urlWithParams}`;

            this._loggerService.saveLog(LOG_LEVEL.INFO, message);
          }
        })
      );
  }
}
