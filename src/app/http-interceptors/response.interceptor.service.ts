import { Injectable } from '@angular/core';
import { HttpEvent, HttpEventType, HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';

import { HttpError } from '../error-codes';

import { LOG_LEVEL, LoggerService } from '../shared/logger.service';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ResponseInterceptor implements HttpInterceptor {

  constructor(
    private _loggerService: LoggerService,
    private _authenticationService: AuthenticationService
  ) { }

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap({
        next: event => {
          if (event.type === HttpEventType.Response) {
            const bodySize = event.headers.get('Content-Length');

            const message = `${req.method} ${req.urlWithParams} ${event.status} ${bodySize} B`;

            this._loggerService.saveLog(LOG_LEVEL.INFO, message);
          }
        },
        error: ({ status, error }: HttpErrorResponse) => {
          const errorJSON = JSON.stringify(error);

          const message = `${req.method} ${req.urlWithParams} ${status} ${errorJSON}`;

          this._loggerService.saveLog(LOG_LEVEL.ERROR, message);
        }
      }),
      catchError((event: HttpErrorResponse) => {
        if (event?.url?.includes('change-password') || event?.url?.includes('login')) {
          return throwError(event);
        }

        switch (event.status) {
          case HttpError.Forbidden: {
            // TODO(eric): what to do with 403 errors?
            break;
          }
          case HttpError.Unauthorized: {
            // redirect to login
            this._authenticationService.logout();
            break;
          }
        }

        return throwError(event);
      })
    );
  }
}
