import { TestBed } from '@angular/core/testing';

import { ResponseInterceptor } from './response.interceptor.service';
import { AuthenticationService } from '../auth/authentication.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { LoggerService } from '../shared/logger.service';
import { UserService, Jwt } from '../users/user.service';
import { Component } from '@angular/core';
import { HttpError } from '../error-codes';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { BASE_PATH as BASE_PATH_STATS } from 'rest_client/pangolin/variables';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@Component({
  selector: 'blank',
  template: ''
})
export class BlankComponent { }

describe('ResponseInterceptor', () => {
  let httpTestingController: HttpTestingController;
  let userService: UserService;
  let authenticationService: AuthenticationService;

  const nonLdapToken: Jwt = {
    aud: 'nefeli-heimdallr',
    exp: Math.round(new Date().getTime() / 1000) + 3600,
    iat: Math.round(new Date().getTime() / 1000),
    iss: 'weaver.tovino.com-heimdallr',
    jti: '86172465054303046557578602619063514986',
    ldap: false,
    nbf: Math.round(new Date().getTime() / 1000),
    roles: {
      scope: 'system',
      id: '',
      roles: [
        'admin'
      ]
    },
    sub: 'signing-key-ldap-admins'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        RouterTestingModule.withRoutes([
          {
            path: 'auth/login',
            component: BlankComponent
          }
        ])
      ],
      providers: [
        LoggerService,
        AuthenticationService,
        UserService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: ResponseInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        {
          provide: BASE_PATH_STATS,
          useValue: '/dashboard'
        },
      ],
      declarations: [
        BlankComponent
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
    authenticationService = TestBed.inject(AuthenticationService);

    spyOn<any>(userService, 'getDecodedToken').and.returnValue(nonLdapToken);
  });

  it('should be created', () => {
    const service: ResponseInterceptor = TestBed.inject(ResponseInterceptor);
    expect(service).toBeTruthy();
  });

  it('should logout user on 401 error', () => {
    userService.getUser('ecarino', 'faketoken').subscribe(
      () => { },
      () => { }
    );

    spyOn<any>(authenticationService, 'logout').and.callThrough();

    const req = httpTestingController.expectOne('/auth/users/ecarino');
    req.flush({}, {
      status: HttpError.Unauthorized,
      statusText: 'Not Authenticated'
    });

    expect(authenticationService.logout).toHaveBeenCalled();
  });

  it('should not logout user on 403 error', () => {
    userService.getUser('ecarino', 'faketoken').subscribe(
      () => { },
      (response: HttpErrorResponse) => {
        expect(response.status).toBe(HttpError.Forbidden);
        expect(response.statusText).toBe('Not Authorized');
      }
    );

    spyOn<any>(authenticationService, 'logout').and.callThrough();

    const req = httpTestingController.expectOne('/auth/users/ecarino');
    req.flush({}, {
      status: HttpError.Forbidden,
      statusText: 'Not Authorized'
    });

    expect(authenticationService.logout).not.toHaveBeenCalled();
  });

  it('should not logout user on change-password request', () => {
    authenticationService.editPassword('ecarino', 'pwd', 'new-pwd').subscribe(
      () => { },
      (response: HttpErrorResponse) => {
        expect(response.status).toBe(HttpError.Forbidden);
        expect(response.statusText).toBe('Not Authorized');
      }
    );

    spyOn<any>(authenticationService, 'logout').and.callThrough();

    const req = httpTestingController.expectOne('/auth/change-password');
    req.flush({}, {
      status: HttpError.Forbidden,
      statusText: 'Not Authorized'
    });

    expect(authenticationService.logout).not.toHaveBeenCalled();
  });

  it('should not logout user on login request', () => {
    authenticationService.login('ecarino', 'pwd').subscribe(
      () => { },
      (response: HttpErrorResponse) => {
        expect(response.status).toBe(HttpError.Forbidden);
        expect(response.statusText).toBe('Not Authorized');
      }
    );

    spyOn<any>(authenticationService, 'logout').and.callThrough();

    const req = httpTestingController.expectOne('/auth/login');
    req.flush({}, {
      status: HttpError.Forbidden,
      statusText: 'Not Authorized'
    });

    expect(authenticationService.logout).not.toHaveBeenCalled();
  });
});
