import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';

import { SharedModule } from '../shared/shared.module';

import { SummaryComponent } from './summary.component';
import { SummaryItemComponent } from './summary-item.component';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    FlexLayoutModule,
    SharedModule
  ],
  declarations: [
    SummaryComponent,
    SummaryItemComponent
  ],
  exports: [
    SummaryComponent,
    SummaryItemComponent
  ]
})
export class SummaryModule { }
