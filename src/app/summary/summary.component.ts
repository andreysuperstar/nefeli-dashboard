import { Component, Input, OnDestroy } from '@angular/core';
import { Subscription, of } from 'rxjs';
import { ClusterService } from '../home/cluster.service';
import { Stats } from 'rest_client/pangolin/model/stats';
import { Cluster } from '../home/cluster.service';
import { LoggerService } from '../shared/logger.service';
import { ConverterUnit, ByteConverterPipe } from '../pipes/byte-converter.pipe';
import { catchError } from 'rxjs/operators';
import { ServersService, Server, ServerState } from '../cluster/servers.service';

const Config = {
  POLL_INTERVAL: 2000
};

interface State {
  active: number;
  total: number;
}

@Component({
  selector: 'nef-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.less']
})
export class SummaryComponent implements OnDestroy {

  @Input() public set cluster(val: Cluster) {
    this._cluster = val;

    if (this._isActive) {
      this.disableStatsStream();
      this.updateStatsStream(true);
    }
  }

  @Input() public set active(val: boolean) {
    this._isActive = val;
    this.updateStatsStream(val);
  }

  private _cluster: Cluster;
  private _clusterStats: Stats;
  private _statSubscription: Subscription;
  private _serverSubscription: Subscription;
  private _isActive = false;
  private _cores: State = {
    active: 0,
    total: 0
  };
  private _memory: State = {
    active: 0,
    total: 0
  };

  constructor(
    private _clusterService: ClusterService,
    private _log: LoggerService,
    private _serversService: ServersService,
    private _converter: ByteConverterPipe
  ) { }

  public ngOnDestroy() {
    this.disableStatsStream();
    this._isActive = false;
  }

  private enableStatsStream() {
    if (this._cluster && this._isActive) {
      this._statSubscription = this._clusterService.streamClusterStats(this._cluster.id, Config.POLL_INTERVAL)
        .subscribe((stats: Stats) => {
          this._clusterStats = stats;
        }, (error: Error) => {
          this._log.error(error);
          setTimeout(() => {
            this.enableStatsStream();
          }, Config.POLL_INTERVAL);
        });
    }
  }

  private enableServerStream() {
    if (this._cluster && this._isActive) {
      this._serverSubscription = this._serversService.streamServers(this._cluster.id).pipe(
        catchError(() => {
          setTimeout(() => {
            this.enableServerStream();
          }, Config.POLL_INTERVAL);
          return of([]);
        })
      ).subscribe((servers: Server[]) => {
        this.setTotalCores(servers);
      });
    }
  }

  private setTotalCores(servers: Server[]) {
    const cores = {
      total: 0,
      active: 0
    };
    const memory = {
      total: 0,
      used: 0
    };

    servers.forEach((server: Server) => {
      const state: ServerState = this._serversService.getServerState(server);
      cores.total += state.cores.indices.length;
      cores.active += state.cores.active.size;
      memory.total += state.memory.total;
      memory.used += state.memory.used;
    });

    this._cores.total = cores.total;
    this._cores.active = cores.active;
    this._memory.total = memory.total;
    this._memory.active = memory.used;
  }

  private updateStatsStream(enable: boolean) {
    if (enable) {
      if (!this._statSubscription) {
        // enable stats stream
        this.enableStatsStream();
      }

      if (!this._serverSubscription) {
        this.enableServerStream();
      }
    } else {
      this.disableStatsStream();
    }
  }

  private disableStatsStream() {
    if (this._statSubscription) {
      this._statSubscription.unsubscribe();
      this._statSubscription = undefined;
    }

    if (this._serverSubscription) {
      this._serverSubscription.unsubscribe();
      this._serverSubscription = undefined;
    }
  }

  public get clusterStats(): Stats {
    return this._clusterStats;
  }

  public get throughput(): [number, string] {
    const fractionalLength = 2;
    let desc = '';

    if (!this._clusterStats) {
      return [0, ''];
    }

    const value: [number, string] = this._converter.convert(
      this._clusterStats.throughputOut,
      ConverterUnit.BITS,
      fractionalLength);

    desc = `${value[1]}ps`;

    return [value[0], desc];
  }

  public get services(): number {
    return this._clusterStats && this._clusterStats.services || 0;
  }

  public get coresUsed(): number {
    return this._clusterStats && this._clusterStats.coresUsed || 0;
  }

  public get memUsed(): number {
    return this._clusterStats && this._clusterStats.memUsed || 0;
  }

  public get coresTotal(): number {
    return this._cores.total;
  }

  public get coresActive(): number {
    return this._cores.active;
  }

  public get memoryTotal(): number {
    return this._memory.total;
  }

  public get memoryActive(): number {
    return this._memory.active;
  }
}
