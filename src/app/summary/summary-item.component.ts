import { Component, Input } from '@angular/core';

@Component({
  selector: 'nef-summary-item',
  templateUrl: './summary-item.component.html',
  styleUrls: ['./summary-item.component.less']
})
export class SummaryItemComponent {

  private _icon: string;
  private _description: string;
  private _value: number | string;
  private _total: number | string;
  private _unit: string;

  @Input() public set icon(val: string) {
    this._icon = val;
  }

  @Input() public set description(val: string) {
    this._description = val;
  }

  @Input() public set value(val: number | string) {
    this._value = val;
  }

  @Input() public set total(val: number | string) {
    this._total = val;
  }

  @Input() public set unit(val: string) {
    this._unit = val;
  }

  constructor() { }

  public get icon(): string {
    return this._icon;
  }

  public get description(): string {
    return this._description;
  }

  public get value(): number | string {
    return this._value;
  }

  public get total(): number | string {
    return this._total;
  }

  public get unit(): string {
    return this._unit;
  }
}
