import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SummaryComponent } from './summary.component';
import { SummaryItemComponent } from './summary-item.component';
import { MatCardModule } from '@angular/material/card';
import { ClusterService } from '../home/cluster.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { Cluster } from '../home/cluster.service';
import { environment } from '../../environments/environment';
import { ByteConverterPipe } from '../pipes/byte-converter.pipe';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { LoggerService } from '../shared/logger.service';
import { AlertService } from '../shared/alert.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { RESTServerList } from 'rest_client/pangolin/model/rESTServerList';

describe('SummaryComponent', () => {
  let component: SummaryComponent;
  let fixture: ComponentFixture<SummaryComponent>;
  let httpTestingController: HttpTestingController;

  const mockCluster: Cluster = {
    id: '-1',
    name: 'Mock Cluster'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatCardModule,
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      declarations: [
        SummaryComponent,
        SummaryItemComponent,
        ByteConverterPipe
      ],
      providers: [
        ClusterService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: PANGOLIN_BASE_PATH,
          useValue: '/v1'
        },
        LoggerService,
        AlertService,
        ByteConverterPipe
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have summary items', () => {
    expect(fixture.debugElement.nativeElement.querySelectorAll('nef-summary-item').length).toBe(4);
  });

  it('should subscribe to new cluster stat stream when active', waitForAsync(() => {
    component.cluster = mockCluster;
    component.active = true;

    fixture.whenStable().then(() => {
      const req = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/stats`);
      expect(req.request.method).toBe('POST');
      expect(req.request.responseType).toBe('json');
    });

    component.active = false;
  }));

  it('should unsubscribe from cluster stat stream when inactive', waitForAsync(() => {
    component.cluster = mockCluster;
    component.active = true;
    component.active = false;

    fixture.whenStable().then(() => {
      // expect the two initial requests when component was first made active
      httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/stats`);
      httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/servers`);

      // but do not expect any other outstanding requests since component is inactive
      httpTestingController.verify();
    });
  }));

  it('should subscribe to new cluster stat stream when active', fakeAsync(() => {
    component.cluster = mockCluster;
    component.active = true;
    tick();
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/stats`);
    req.flush({
      id: -1,
      memUsed: 55,
      memTotal: 100,
      throughputOut: 100,
      throughputIn: 100,
      capacity: 1,
      packetLoss: [],
      latency: []
    });
    fixture.detectChanges();
    const summaryItems = fixture.debugElement.nativeElement.querySelectorAll('nef-summary-item');
    expect(summaryItems.length).toBe(4);
    expect(summaryItems[0].querySelector('.value').textContent).toBe('0');
    expect(summaryItems[1].querySelector('.value').textContent).toBe('0');
    component.active = false;
  }));

  const fetchStats = () => {
    const statsReq = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/stats`);
    statsReq.flush({});

    const mockServersResponse: RESTServerList = {
      "servers": [
        {
          "identifier": "sylvester",
          "name": "sylvester",
          "sockets": [
            {
              "id": 0,
              "memory": "33612103680",
              "hugepages": [
                {
                  "size": "1073741824",
                  "count": 20
                }
              ],
              "cores": [0, 1, 2, 3]
            },
            {
              "id": 1,
              "memory": "33612103680",
              "hugepages": [
                {
                  "size": "1073741824",
                  "count": 20
                }
              ],
              "cores": [4, 5, 6, 7]
            }
          ],
          "nfs": {
            "nfInstances": [
              {
                "metadata": {
                  "logicalNf": "filter",
                  "name": "filter_0",
                  "machine": "sylvester",
                  "service": "sample_1",
                  "tenant": "Tenant 1",
                  "nfType": "",
                  "draining": false,
                },
                "resources": {
                  "coresBySocket": {
                    "0": {
                      "cores": [2, 3]
                    }
                  },
                  "hugepagesBySocket": {
                    "0": {
                      "hugepages": {}
                    }
                  }
                }
              },
              {
                "metadata": {
                  "logicalNf": "filter",
                  "name": "filter_1",
                  "machine": "sylvester",
                  "service": "sample_1",
                  "tenant": "Tenant 1",
                  "nfType": "",
                  "draining": false
                },
                "resources": {
                  "coresBySocket": {
                    "0": {
                      "cores": [5, 6, 7]
                    }
                  },
                  "hugepagesBySocket": {
                    "0": {
                      "hugepages": { "1073741824": 2 }
                    }
                  }
                }
              }
            ]
          },
          "control": {
            "numaResources": {
              "0": {
                "cores": [0],
                "hugepages": {
                  "1073741824": 1
                }
              }
            }
          },
          "bess": {
            "numaResources": {
              "0": {
                "cores": [1],
                "hugepages": {}
              }
            }
          }
        },
        {
          "identifier": "felix",
          "name": "felix",
          "sockets": [
            {
              "id": 0,
              "memory": "33612103680",
              "hugepages": [
                {
                  "size": "1073741824",
                  "count": 20
                }
              ],
              "cores": [0, 1]
            },
            {
              "id": 1,
              "memory": "33612103680",
              "hugepages": [
                {
                  "size": "1073741824",
                  "count": 20
                }
              ],
              "cores": [2, 3]
            }
          ],
          "control": {
            "numaResources": {
              "0": {
                "cores": [0],
                "hugepages": {
                  "1073741824": 1
                }
              }
            }
          },
          "bess": {
            "numaResources": {
              "0": {
                "cores": [2],
                "hugepages": {}
              }
            }
          }
        }
      ]
    };

    const serversReq = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/servers`);
    serversReq.flush(mockServersResponse);
  };

  it('should display proper core count', () => {
    component.cluster = mockCluster;
    component.active = true;
    fetchStats();
    fixture.detectChanges();

    const el = fixture.debugElement.nativeElement;
    const cores = el.querySelector('nef-summary-item[description="CORES USED"] .value').textContent;
    const memory = el.querySelector('nef-summary-item[description="NF MEMORY USED"] .value').textContent;

    expect(cores).toBe("9/12");
    expect(memory).toBe("4/86 GB");
  });

  it('should update stat stream when cluster is modified', fakeAsync(() => {
    component.cluster = mockCluster;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/stats`).flush({});

    component.cluster = {
      id: '-2',
      name: 'New Site'
    };
    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/sites/-2/stats`).flush({});

    component.active = false;
  }));
});
