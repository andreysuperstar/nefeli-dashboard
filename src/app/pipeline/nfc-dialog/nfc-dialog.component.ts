import { Component, OnInit, Inject } from '@angular/core';
import { isEqual } from 'lodash-es';
import { NetworkFunctionService } from '../network-function.service';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { NFType } from 'rest_client/pangolin/model/nFType';
import { BESSInterface } from 'rest_client/pangolin/model/bESSInterface';
import { VMInterface } from 'rest_client/pangolin/model/vMInterface';
import { Tenant } from '../../tenant/tenant.service';
import { PipelineNF } from '../pipeline.model';
import { AlertType, AlertService } from '../../shared/alert.service';
import { PipelineService, NFConfiguration } from '../pipeline.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { catchError } from 'rxjs/operators';
import { LoggerService } from 'src/app/shared/logger.service';
import { ServicesService } from 'rest_client/pangolin/api/services.service';

const DEFAULT_ROLLBACK_POLICY = JSON.stringify({
  Rollback: {
    _comment: 'On config failure, reboot the instance and try again.',
    Policy: 'reboot'
  }
});

export const JSON_SPACES = 3;

export interface NfcDialogData {
  tenant: Tenant;
  pipelineId: string;
  nodeId: string;
  nf: PipelineNF;
  isDraft: boolean;
}

@Component({
  selector: 'nef-nfc-dialog',
  templateUrl: './nfc-dialog.component.html',
  styleUrls: ['./nfc-dialog.component.less']
})
export class NfcDialogComponent implements OnInit {
  private _form: FormGroup;
  private _initialManifestInterfaces: BESSInterface[] | VMInterface[];

  constructor(
    private _nfService: NetworkFunctionService,
    private _dialogRef: MatDialogRef<NfcDialogComponent, [NFDescriptor, boolean]>,
    private _alertService: AlertService,
    private _pipelineService: PipelineService,
    private _servicesService: ServicesService,
    private _fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) private _data: NfcDialogData,
    private _logger: LoggerService
  ) { }

  public ngOnInit() {
    this.initForm();
    this.getNFConfig();
    this.getNFManifest();
  }

  private initForm() {
    this._form = this._fb.group({
      manifest: '',
      configTemplate: ''
    });
  }

  private setNFConfig(config: NFCConfig) {
    const value: NFCConfig = {
      filter: config.filter,
      mapping: config.mapping
    };

    value.configTemplate = this.beautifyJson(config.configTemplate);
    value.rollback = this.beautifyJson(config.rollback || DEFAULT_ROLLBACK_POLICY);

    this._form.patchValue(value);
  }

  private getNFConfig() {
    const nfConfiguration: NFConfiguration = this._pipelineService.getNFConfiguration(this._data.nodeId);

    if (nfConfiguration?.config) {
      // apply saved config
      this.setNFConfig(nfConfiguration.config);
    } else {
      // get config
      const obs$ = this._data.isDraft ?
        this._servicesService.getServiceDraftNFConfig(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        ) :
        this._nfService.getNfConfig(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        );

      obs$.subscribe((config: NFCConfig) => this.setNFConfig(config));
    }
  }

  private getNFManifest() {
    const nfConfiguration: NFConfiguration = this._pipelineService.getNFConfiguration(this._data.nodeId);

    if (nfConfiguration?.manifest) {
      this._initialManifestInterfaces = this.getManifestIntefactes(nfConfiguration.manifest);

      /* FIXME: use raw manifest string value here because using
         JSON.stringify(nfConfiguration.manifest) leads to
         TypeError: Converting circular structure to JSON,
         issue could be related to PipelineComponent.updateNF method
         which is triggered on saving manifest and closing dialog */

      // apply saved manifest
      this._form.patchValue({
        manifest: nfConfiguration.manifestRaw
      });
    } else {
      // get manifest
      const obs$ = this._data.isDraft ?
        this._servicesService.getServiceDraftNFManifest(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        ) :
        this._pipelineService.getNfManifest(
          this._data.tenant.identifier,
          this._data.pipelineId ?? '0',
          this._data.nodeId
        );

      obs$.pipe(
        // if pipeline specific manifest doesn't exist, get global manifest
        catchError(() => this._nfService.getNfManifest(this._data.nf.catalogId))
      )
        .subscribe((manifest: NFDescriptor) => {
          this._initialManifestInterfaces = this.getManifestIntefactes(manifest);

          this._form.patchValue({
            manifest: JSON.stringify(manifest, undefined, JSON_SPACES)
          });
        });
    }
  }

  public submitNFConfigurationForm() {
    if (this._form.pristine) {
      this._dialogRef.close();

      return;
    }

    const nfConfiguration: NFConfiguration = {
      nf: this._data.nf
    };
    let manifest: NFDescriptor;
    const controls = this._form.controls as { [key: string]: FormControl };
    const isDirty = controls.configTemplate.dirty || controls.manifest.dirty;

    if (controls.configTemplate.dirty) {
      const config: NFCConfig = {
        configTemplate: controls.configTemplate.value
      };

      nfConfiguration.config = config;
    }

    if (controls.manifest.dirty) {
      try {
        manifest = JSON.parse(controls.manifest.value);
      } catch (e) {
        this._logger.error(`Failed to parse json: ${controls.manifest.value}`);

        this._alertService.error(AlertType.ERROR_NF_CONFIG_MANIFEST);

        return;
      }

      nfConfiguration.manifest = manifest;
      nfConfiguration.manifestRaw = JSON.stringify(manifest, undefined, JSON_SPACES);
    }

    /* save NF configuration data for further posting while publishing the service
        and editing before publish */
    this._pipelineService.setNFConfiguration(this._data.nodeId, nfConfiguration);

    const manifestInterfaces = this.getManifestIntefactes(nfConfiguration.manifest);
    const hasManifestChanges = !isEqual(this._initialManifestInterfaces, manifestInterfaces);
    const dialogResult = hasManifestChanges ? manifest : undefined;

    this._dialogRef.close([dialogResult, isDirty]);
  }

  public onFileSelected(fileEvent: Event, formProp: string) {
    const file: File = fileEvent.target['files'][0];
    const reader = new FileReader();

    reader.onload = (() => {
      return (ev: ProgressEvent) => {
        const data = ev.target['result'];
        this.setFormData(formProp, data);
      };
    })();

    reader.readAsText(file);
  }

  private setFormData(prop: string, data: string) {
    const value = {};
    value[prop] = data;
    this._form.patchValue(value);
    this._form.controls[prop].markAsDirty();
  }

  private beautifyJson(jsonObj: string): string {
    let rt = jsonObj;

    try {
      rt = JSON.stringify(JSON.parse(jsonObj), undefined, JSON_SPACES);
    } catch (e) { }

    return rt;
  }

  private getManifestIntefactes(manifest: NFDescriptor): BESSInterface[] | VMInterface[] {
    let manifestType: string;

    if (manifest?.type) {
      manifestType = manifest.type === NFType.NATIVE ? 'bessManifest' : 'vmManifest';
    }

    return manifest?.components?.datapath?.[manifestType]?.interfaces;
  }

  public get form(): FormGroup {
    return this._form;
  }
}
