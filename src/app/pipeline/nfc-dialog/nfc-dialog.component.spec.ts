import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { JSON_SPACES, NfcDialogComponent, NfcDialogData } from './nfc-dialog.component';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonHarness } from '@angular/material/button/testing';
import { environment } from '../../../environments/environment';
import { NFType } from 'rest_client/pangolin/model/nFType';
import { VMInterfaceType } from 'rest_client/pangolin/model/vMInterfaceType';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { PipelineService } from '../pipeline.service';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { NetworkFunctionService } from '../network-function.service';
import { ReactiveFormsModule, FormsModule, AbstractControl } from '@angular/forms';
import { MatInputHarness } from '@angular/material/input/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { cloneDeep } from 'lodash';

const dialogSpy = {
  close: jasmine.createSpy('close')
};

const mockMatDialogData: NfcDialogData = {
  tenant: {
    identifier: '-1',
    name: 'Tenant 1'
  },
  pipelineId: 'service-id',
  nodeId: 'bess_url_filter-f5403',
  nf: {
    catalogId: 'nf-catalog-id',
    local: {
      name: 'nf-name',
      vendor: 'vendor',
      instances: []
    }
  },
  isDraft: false
};

const mockManifest: NFDescriptor = {
  name: 'arista_floating',
  type: NFType.THIRDPARTY,
  components: {
    datapath: {
      vmManifest: {
        interfaces: [
          {
            id: 'mgmt0',
            type: VMInterfaceType.MGMT,
            numTxQueues: 0,
            numRxQueues: 0,
            hardwareMac: '',
            mac: '',
            subnet: '',
            pciDevice: null,
            controlPorts: [
              22,
              80,
              437,
              443
            ],
            checksum: false,
            gso: false,
            tso4: false,
            tso6: false,
            ecn: false,
            mergeRxBuffers: false,
            noMultiQueue: false
          },
          {
            id: 'dp0',
            type: VMInterfaceType.DATA,
            numTxQueues: 0,
            numRxQueues: 0,
            hardwareMac: '',
            mac: '00:16:3d:22:33:57',
            subnet: '',
            pciDevice: null,
            controlPorts: [],
            checksum: false,
            gso: false,
            tso4: false,
            tso6: false,
            ecn: false,
            mergeRxBuffers: false,
            noMultiQueue: false
          },
          {
            id: 'dp1',
            type: VMInterfaceType.DATA,
            numTxQueues: 0,
            numRxQueues: 0,
            hardwareMac: '',
            mac: '00:16:3d:22:33:58',
            subnet: '',
            pciDevice: null,
            controlPorts: [],
            checksum: false,
            gso: false,
            tso4: false,
            tso6: false,
            ecn: false,
            mergeRxBuffers: false,
            noMultiQueue: false
          }
        ]
      }
    }
  }
};

describe('NfcDialogComponent', () => {
  let component: NfcDialogComponent;
  let fixture: ComponentFixture<NfcDialogComponent>;
  let httpTestingController: HttpTestingController;
  let nfService: NetworkFunctionService;
  let pipelineService: PipelineService;
  let loader: HarnessLoader;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatInputModule,
        MatTabsModule,
        MatFormFieldModule,
        MatDialogModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule
      ],
      declarations: [NfcDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockMatDialogData
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        NetworkFunctionService,
        PipelineService
      ]
    });

    nfService = TestBed.inject(NetworkFunctionService);
    pipelineService = TestBed.inject(PipelineService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NfcDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be rendered correctly', () => {
    const el = fixture.debugElement.nativeElement;
    expect(el.querySelectorAll('mat-tab-body').length).toBe(2);
    expect(el.querySelectorAll('.mat-tab-label-content')[0].textContent).toBe("Manifest");
    expect(el.querySelectorAll('.mat-tab-label-content')[1].textContent).toBe('NF Configuration');

    const buttons = el.querySelectorAll('button');
    expect(buttons[0].textContent).toBe("Import from File");
    expect(buttons[1].textContent).toBe("Cancel");
    expect(buttons[2].textContent).toBe("Save");
  });

  it('should get NF config and manifest for a existing service', () => {
    const reqConfig = httpTestingController.expectOne((req: HttpRequest<any>) => {
      const api = `${environment.restServer}/v1/tenants/-1/services/service-id/nodes/bess_url_filter-f5403/config`;
      return req.url === api;
    });

    const reqManifest = httpTestingController.expectOne((req: HttpRequest<any>) => {
      const api = `${environment.restServer}/v1/tenants/-1/services/service-id/nodes/bess_url_filter-f5403/manifest`;
      return req.url === api;
    });

    expect(reqConfig.request.method).toBe('GET');
    expect(reqConfig.request.responseType).toBe('json');
    reqConfig.flush({
      configTemplate: 'test config template'
    } as NFCConfig)

    expect(reqManifest.request.method).toBe('GET');
    expect(reqManifest.request.responseType).toBe('json');

    reqManifest.flush(mockManifest);

    expect(component.form.controls.configTemplate.value).toBe("test config template");
    expect(component.form.controls.manifest.value).toBe(JSON.stringify(mockManifest, undefined, JSON_SPACES));
  });

  it('should emit manifest if interfaces has a change', async () => {
    const manifestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/tenants/-1/services/service-id/nodes/bess_url_filter-f5403/manifest`
    });

    manifestRequest.flush(mockManifest);

    const saveButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Save'
    }));

    await saveButton.click();

    const manifestInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formcontrolname="manifest"]'
    }));

    const manifest = cloneDeep(mockManifest);

    manifest.components.datapath.vmManifest.interfaces[0].controlPorts[0] = 21;

    const manifestValue = JSON.stringify(manifest, undefined, JSON_SPACES);

    await manifestInput.setValue(manifestValue);

    await saveButton.click();

    expect(dialogSpy.close).toHaveBeenCalledWith([manifest, true]);
  });

  it('should get the global nf manifest if pipeline copy does not exist', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/-1/services/service-id/nodes/bess_url_filter-f5403/manifest`);
    expect(req.request.method).toBe("GET");
    expect(req.request.responseType).toBe("json");
    req.flush({}, { status: 404, statusText: 'Not Found' });

    const globalReq = httpTestingController.expectOne(`${environment.restServer}/v1/nfs/nf-catalog-id`);
    expect(globalReq.request.method).toBe("GET");
    expect(globalReq.request.responseType).toBe("json");
  });

  it('should save service NF configuration', async () => {
    const manifest = JSON.stringify({
      foo: 'bar'
    }, undefined, JSON_SPACES);

    component.form.patchValue({
      configTemplate: 'config template'
    });

    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: 'textarea[formcontrolname=manifest]'
    }));
    await manifestInput.setValue(manifest);

    const configTemplateControl: AbstractControl = component.form.get('configTemplate');

    // make the config field on hidden tab dirty
    configTemplateControl.markAsDirty();

    const saveButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[form="nfc-form"]')
    ).nativeElement;

    spyOn(pipelineService, 'setNFConfiguration').and.callThrough();

    saveButtonEl.click();

    expect(pipelineService.setNFConfiguration).toHaveBeenCalled();
    expect(
      pipelineService['_nfConfigurations'].has(component['_data'].nodeId)
    ).toBeTruthy('has saved NF configuration');

    expect(component['_dialogRef'].close).toHaveBeenCalled();
  });

  it('should get service NF configuration', async () => {
    const configTemplate = 'config template';
    const manifest: string = JSON.stringify({
      foo: 'bar'
    }, undefined, JSON_SPACES);

    // save NF configuration
    component.form.patchValue({
      configTemplate,
      manifest
    });

    // make the manifest field dirty
    const manifestInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: 'textarea[formcontrolname=manifest]'
    }));
    await manifestInput.setValue(manifest);

    let configTemplateControl: AbstractControl = component.form.get('configTemplate');

    // make the config field on hidden tab dirty
    configTemplateControl.markAsDirty();

    const saveButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[form="nfc-form"]')
    ).nativeElement;

    saveButtonEl.click();

    // get saved NF configuration
    spyOn(nfService, 'getNfConfig').and.callThrough();
    spyOn(pipelineService, 'getNfManifest').and.callThrough();
    spyOn<any>(component, 'setNFConfig').and.callThrough();

    component.ngOnInit();

    fixture.detectChanges();

    expect(nfService.getNfConfig).not.toHaveBeenCalled();
    expect(component['setNFConfig']).toHaveBeenCalled();
    expect(pipelineService.getNfManifest).not.toHaveBeenCalled();

    configTemplateControl = component.form.get('configTemplate');

    expect(configTemplateControl.value).toBe(configTemplate, 'valid config template value');
    expect(await manifestInput.getValue()).toBe(manifest, 'valid manifest value');
  });

  it('should properly import data from file', waitForAsync(() => {
    const fileData = 'this is an nfc config template file';
    const file: File = new File([fileData], "config_template.yml", {
      type: "text",
    });

    const fileList: FileList = {
      length: 1,
      0: file,
      item: (index: number) => {
        return file;
      }
    };

    spyOn(component, <any> 'setFormData').and.callThrough();
    spyOn(component.form, 'patchValue').and.callFake((value) => {
      expect(value.configTemplate).toBe('this is an nfc config template file');
    });

    component.onFileSelected({
      target: {
        files: fileList
      }
    } as any, 'configTemplate');
  }));
});
