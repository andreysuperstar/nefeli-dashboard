import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { NFCatalog, NetworkFunction } from './network-function.model';

@Component({
  selector: 'nef-network-function-menu',
  templateUrl: './network-function-menu.component.html',
  styleUrls: ['./network-function-menu.component.less']
})
export class NetworkFunctionMenuComponent implements OnInit, OnDestroy {
  private _subscription = new Subscription();
  private _catalog: NFCatalog;
  private _draggable = false;
  private _displayTenant = false;
  private _title: string;
  private _canEdit = false;
  private _canActivate = false;
  private _handleDragStart: EventEmitter<any> = new EventEmitter();
  private _editClick: EventEmitter<NetworkFunction> = new EventEmitter();
  private _activate: EventEmitter<NetworkFunction> = new EventEmitter();
  private _remove: EventEmitter<NetworkFunction> = new EventEmitter();

  @Output() public get editClick(): EventEmitter<NetworkFunction> {
    return this._editClick;
  }

  @Output() public get activate(): EventEmitter<NetworkFunction> {
    return this._activate;
  }

  @Output() public get handleDragStart(): EventEmitter<NetworkFunction> {
    return this._handleDragStart;
  }

  @Output() public get remove(): EventEmitter<NetworkFunction> {
    return this._remove;
  }

  @Input() public set draggable(value: boolean) {
    this._draggable = value;
  }

  @Input() public set displayTenant(value: boolean) {
    this._displayTenant = value;
  }

  @Input() public set title(value: string) {
    this._title = value;
  }

  @Input() public set canEdit(value: boolean) {
    this._canEdit = value;
  }

  @Input() public set canActivate(value: boolean) {
    this._canActivate = value;
  }

  @Input() public set catalog(nfs: NFCatalog) {
    this._catalog = nfs;
  }

  constructor(
    private _route: ActivatedRoute,
  ) { }

  public get draggable(): boolean {
    return this._draggable;
  }

  public get displayTenant(): boolean {
    return this._displayTenant;
  }

  public ngOnInit() { }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  public get vendors(): Array<string> {
    return (this._catalog) ? Object.keys(this._catalog) : [];
  }

  public getNfs(vendor: string): Array<NetworkFunction> {
    return this._catalog[vendor];
  }

  public onDragStart(event: any) {
    this._handleDragStart.emit(event);
  }

  public onRemoveNF(nf: NetworkFunction) {
    return this._remove.emit(nf);
  }

  public get title(): string {
    return this._title;
  }

  public get canEdit(): boolean {
    return this._canEdit;
  }

  public get canActivate(): boolean {
    return this._canActivate;
  }

  public onEdit(nF: NetworkFunction) {
    this._editClick.emit(nF);
  }

  public onActivate(nF: NetworkFunction) {
    this._activate.emit(nF);
  }
}
