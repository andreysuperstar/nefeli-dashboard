import { Tenant } from '../tenant/tenant.service';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { BESSInterface } from 'rest_client/pangolin/model/bESSInterface';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';
import { VMInterface } from 'rest_client/pangolin/model/vMInterface';
import * as SVG from 'svg.js';

export interface NFCatalogResponse {
  descriptors: NFCatalog;
}

export interface NFCatalog {
  [key: string]: Array<NetworkFunction>;
}

export interface NFCapability {
  kppsPerCore: string;
  scaling: boolean;
  pollToScale: boolean;
  metadata: boolean;
  wrapper: string;
}

export interface NFComponents {
  datapath: {
    name: string;
    vendor: string;
    type: string;
    bessManifest?: {
      mclass: string;
      initialState: string;
      numTasks: number;
      unknownState: boolean;
      interfaces: NFInterface[];
    },
    vmManifest?: {
      images: [];
      interfaces: NFInterface[];
      extraQemuCpuFlags: string;
      extraQemuFlags: string;
      licenseMode: string;
    }
    execManifest: string;
    cpuCores: string;
    memoryMb: string;
  };
}

export interface NFInterface extends BESSInterface, VMInterface {
  local?: {
    ctx?: SVG.G;
    connectingNodes?: Array<string>;
  };
}

export interface NetworkFunction extends NFDescriptor {
  local?: {
    id?: string;
    vendor?: string;
    capability?: NFCapability;
    catalogId?: string;
    tenant?: Tenant;
    clusterId?: number;
  };
}

const NF_COLOR_DEFAULT = 'dark-grey';
const NF_COLORS = [
  'blue',
  'orange',
  'green',
  'deep-blue',
  'pink',
  'soft-blue',
  'yellow',
  'acid-green',
  'purple',
  'light-blue',
  'gold'
];

export const GetNFColor = (name: string, vendor = '', colorIndex?: number): string => {
  const token = name + vendor;
  let val = 0;

  if (colorIndex !== undefined) {
    val = colorIndex;
  } else {
    for (let i = 0; i < token.length; i++) {
      val += token.charCodeAt(i);
    }
  }

  return NF_COLORS[val % NF_COLORS.length] || NF_COLOR_DEFAULT;
};

export const GetServerColor = (name: string): string => {
  return GetNFColor(name);
};

export const GetNFNodeName = (logicalNf: string, nfType: string): string => {
  return logicalNf.replace(`user_${nfType}_`, '');
};

export const randomID = (): string => {
  const multiple = 0x99999;
  const len = 16;
  return Math.floor((1 + Math.random()) * multiple).toString(len);
};

export const createNFCatalog = ({ descriptors: dataList }: RESTNFCatalog): NFCatalog => {
  const catalog: NFCatalog = {};

  for (const data of dataList) {
    const vendor = data.components?.datapath?.vendor ?? 'Network Function';
    const capability: NFCapability = {
      kppsPerCore: data.scalingOptions?.maxKppsPerInstance,
      scaling: data.scalingOptions?.supportsScaling,
      wrapper: data.metadataOptions?.wrapper,
      pollToScale: !data.scalingOptions?.useCpu,
      metadata: !!data.metadataOptions ?? false
    };

    const nf: NetworkFunction = {
      local: {
        id: data.identifier,
        vendor: vendor,
        catalogId: data.identifier,
        capability
      },
      ...data
    };

    if (!catalog[vendor]) {
      catalog[vendor] = new Array<NetworkFunction>();
    }

    catalog[vendor].push(nf);
  }

  return catalog;
};

export const getNFInterfaces = (nf: NetworkFunction): NFInterface[] => {
  let interfaces = [];

  if (nf.components.datapath.bessManifest) {
    interfaces = nf.components.datapath.bessManifest.interfaces;
  } else if (nf.components.datapath.vmManifest) {
    interfaces = nf.components.datapath.vmManifest.interfaces;
  }

  return interfaces;
};
