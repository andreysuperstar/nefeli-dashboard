import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick, flush, inject } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentType, OverlayModule } from '@angular/cdk/overlay';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import {
  Pipeline,
  PipelineNode,
  PipelineNodeStatus,
  PipelineNF,
  NfType,
  PipelineGraph,
  PipelineEdge,
  PipelineNfInstance,
  Coordinates
} from './pipeline.model';

import { LoggerService } from '../shared/logger.service';
import { LocalStorageService } from '../shared/local-storage.service';
import { AlertService, AlertType } from '../shared/alert.service';
import { TenantService, Tenant } from '../tenant/tenant.service';
import { EdgeDirection, PipelineService } from './pipeline.service';

import { ByteConverterPipe } from '../pipes/byte-converter.pipe';

import { PipelineComponent } from './pipeline.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { ToggleComponent } from '../shared/toggle/toggle.component';
import { InputDialogComponent } from '../shared/input-dialog/input-dialog.component';
import { ClusterService } from '../home/cluster.service';
import { NfcDialogComponent } from './nfc-dialog/nfc-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { VMInterfaceType } from 'rest_client/pangolin/model/vMInterfaceType';
import { VMInterface } from 'rest_client/pangolin/model/vMInterface';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { ServiceStatus } from 'rest_client/pangolin/model/serviceStatus';
import { InstanceStatusState } from 'rest_client/pangolin/model/instanceStatusState';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { MatStepperModule, MatStepper } from '@angular/material/stepper';
import { By } from '@angular/platform-browser';
import { cloneDeep } from 'lodash-es';
import { Tunnels } from 'rest_client/pangolin/model/tunnels';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpError } from '../error-codes';
import { Service } from 'rest_client/pangolin/model/service';
import { SvgSiteGroup } from './svg-elements/svg-site-group';
import sites from 'mock/sites';
import { TrafficPortFormData } from '../service-designer/traffic-port-form/traffic-port-form.component';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import tunnels from 'mock/tunnels';
import template from 'mock/template';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { Point, RBox } from 'svg.js';
import { DebugElement } from '@angular/core';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import manifest from 'mock/nf_manifest';
import { ServiceConfigTemplate } from 'rest_client/pangolin/model/serviceConfigTemplate';
import servers from 'mock/servers';
import profiles from 'mock/hardware_profiles';

function mouseClickEvent(): MouseEvent {
  return new MouseEvent('mousedown', {
    clientX: 0,
    clientY: 0,
    screenX: 0,
    screenY: 0
  });
}

function dragNf(fixture: ComponentFixture<PipelineComponent>, component: PipelineComponent, nf: PipelineNode, deltaX: number, deltaY: number) {
  nf.local.svgNF.nativeElement.move(deltaX, deltaY);
  component['onDragEnd'](nf);
  tick(1000);
  fixture.detectChanges();
}

function createPort(component: PipelineComponent, id: string, x: number, y: number, site: string, index = 0): PipelineNode {
  const newPort: PipelineNode = {
    identifier: id,
    site,
    port: {
      tunnel: '',
      local: {},
    },
    local: {
      id: id,
      name: id,
      status: PipelineNodeStatus.NEW,
      coordinates: {
        x: x,
        y: y
      }
    }
  };

  component['addNodeToSiteMap'](newPort, index);
  return newPort;
}

export function createNF(
  component: PipelineComponent,
  id: string,
  x: number,
  y: number,
  site: string,
  ports?: string[],
  type: NfType = NfType.NATIVE
): PipelineNode {
  const newNF: PipelineNF = {
    catalogId: '',
    local: {
      interfaces: [],
      type,
      instances: [
        {
          identity: {
            instanceId: "filter_1"
          },
          location: {
            site: "eric-exp",
            placement: {
              machineId: "sylvester"
            }
          },
          state: "CONFIGURED",
          local: { key: '' }
        }
      ],
      vendor: 'vendor',
      name: 'name',
      config: {}
    }
  };

  ports = ports || ['abc123'];

  for (const port of ports) {
    newNF.local.interfaces.push({
      id: port,
      type: VMInterfaceType.DATA
    });
  }

  const newNode: PipelineNode = {
    identifier: id,
    site,
    nf: newNF,
    local: {
      id: id,
      name: id,
      coordinates: {
        x: x,
        y: y
      },
      status: PipelineNodeStatus.NEW
    }
  };

  component['addNodeToSiteMap'](newNode, 0);

  return newNode;
}

function connectNFs(
  component: PipelineComponent,
  nf1: PipelineNode,
  nf2: PipelineNode
) {
  const interface1 = nf1.nf.local.interfaces[0].local.ctx;
  const interface2 = nf2.nf.local.interfaces[0].local.ctx;
  const event = mouseClickEvent();
  component['onNfInterfaceClick'](event, interface1, nf1.site, 0);
  component['onNfInterfaceClick'](event, interface2, nf2.site, 0);
}

function connectWorldPortToNF(
  svgSite: SvgSiteGroup,
  nf: PipelineNode,
  component: PipelineComponent
) {
  const interface1 = svgSite.worldPort;
  const interface2 = nf.nf.local.interfaces[0].local.ctx;

  const event = mouseClickEvent();
  component['onNfInterfaceClick'](event, interface1, nf.site, 1);
  component['onNfInterfaceClick'](event, interface2, nf.site, 1);
}

function createPairedNFs(fixture: ComponentFixture<PipelineComponent>, component: PipelineComponent) {
  const nfs = new Map();
  let type: NfType;

  const nf1 = createNF(component, 'NF1', 100, 100, 'test-site-nf', ['port1'], type);
  const nf2 = createNF(component, 'NF2', 200, 100, 'test-site-nf', ['port2'], type);
  nfs.set('NF1', nf1);
  nfs.set('NF2', nf2);

  component['drawSiteGroups']();

  component.nfs = nfs;
  fixture.detectChanges();
  connectNFs(component, nf1, nf2);
  return [nf1, nf2];
}

function createSite(fixture: ComponentFixture<PipelineComponent>, component: PipelineComponent, ...sites: SiteConfig[]) {
  component['drawSiteGroups']();
  fixture.detectChanges();
  sites?.forEach(site => {
    component.createNewSite(site);
    tick(1000);
    fixture.detectChanges();
  });
}

describe('PipelineComponent', () => {
  let component: PipelineComponent;
  let fixture: ComponentFixture<PipelineComponent>;
  let httpTestingController: HttpTestingController;
  let tenantService: TenantService;
  let mockPipelineGraph: PipelineGraph & { hasDraft?: boolean; };
  let mockServiceResponse: Service;
  let el: HTMLElement;
  let log: LoggerService;

  const mockPipelines: Array<Pipeline> = [{
    identifier: '1',
    name: '1'
  }];

  const mockTenant: Tenant = {
    identifier: '1',
    name: 'Test Tenant'
  };

  const mockServiceStatus: ServiceStatus = {
    "currentSiteVersions": {},
    "currentVersion": "new-version",
    "nodes": {
      "123": {
        "instances": {
          "filter_1": {
            "identity": {
              "instanceId": "filter_1"
            },
            "location": {
              "site": "eric-exp",
              "placement": {
                "machineId": "sylvester",
                "draining": false,
                "coresBySocket": {
                  "0": {
                    "cores": [
                      2
                    ]
                  }
                },
                "license": null
              }
            },
            "metadata": null,
            "state": "CONFIGURED",
            "launchSpec": null,
            "systemdUnit": "",
            "controlPorts": {},
            "telnetConsole": "",
            "placementStatus": {
              "blockedOnMachineResources": true,
              "blockedOnLicense": false
            },
            "launchStatus": {
              "blockedOnFiles": true,
              "blockedOnMemory": true,
              "blockedOnHypervisor": true
            }
          }
        }
      }
    },
    "previousSiteVersions": {},
    "requestedVersion": "requested-version"
  };

  const mockEdges: PipelineEdge[] = [
    {
      "a": {
        "interface": "",
        "node": "left"
      },
      "b": {
        "interface": "left",
        "node": "123"
      },
      "filterAb": {},
      "filterBa": {}
    },
    {
      "a": {
        "interface": "right",
        "node": "123"
      },
      "b": {
        "interface": "",
        "node": "right"
      },
      "filterAb": {},
      "filterBa": {}
    }
  ];

  const mockNfInstances: PipelineNfInstance[] = [
    {
      "location": {
        "placement": {
          "machineId": "fluffy"
        }
      },
      "identity": {
        "instanceId": "filter_0"
      },
      local: { key: '' }
    }, {
      "location": {
        "placement": {
          "machineId": "fluffy"
        }
      },
      "identity": {
        "instanceId": "filter_1"
      },
      local: { key: '' }
    }
  ];

  const mockPipelineManifest: NFDescriptor = {
    identifier: '123',
    name: 'bess_bypass_noop_bidir',
    type: NfType.THIRD_PARTY,
    "controllerType": "NO_CONTROLLER",
    "components": {
      "datapath": {
        "name": "",
        "vendor": "Nefeli Networks",
        "type": "CONTROLLER",
        "bessManifest": {
          "mclass": "UrlFilter",
          "numTasks": 0,
          "unknownState": false,
          "interfaces": [
            {
              "id": "left"
            },
            {
              "id": "right"
            }
          ]
        },
        "execManifest": null,
        "cpuCores": "0",
        memoryMb: '0',
        vmManifest: {
          interfaces: [
            {
              id: 'left',
              type: VMInterfaceType.DATA
            },
            {
              id: 'right',
              type: VMInterfaceType.DATATAP
            }
          ]
        }
      }
    }
  }

  const mockSites: Site[] = [
    {
      config: {
        identifier: 'test-site-nf',
        name: 'NF Site',
        siteType: SiteConfigSiteType.CLUSTER
      }
    },
    {
      config: {
        identifier: 'test-site-port-left',
        name: 'Left Site',
        siteType: SiteConfigSiteType.CLUSTER
      }
    },
    {
      config: {
        identifier: 'test-site-port-right',
        name: 'Right Site',
        siteType: SiteConfigSiteType.CLUSTER
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        FormsModule,
        OverlayModule,
        MatSnackBarModule,
        MatCardModule,
        MatDialogModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        MatExpansionModule,
        MatInputModule,
        MatSelectModule,
        MatStepperModule,
        MatDialogModule,
        MatTabsModule
      ],
      declarations: [
        PipelineComponent,
        LegendComponent,
        ToggleComponent,
        InputDialogComponent,
        NfcDialogComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {
                id: 12345
              },
              data: {
                isEditMode: true
              }
            }
          }
        },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        AlertService,
        LoggerService,
        LocalStorageService,
        PipelineService,
        TenantService,
        ByteConverterPipe,
        ClusterService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    tenantService = TestBed.inject(TenantService);
    log = TestBed.inject(LoggerService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PipelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
    component.sites = mockSites;

    mockPipelineGraph = {
      "edges": mockEdges as PipelineEdge[],
      nodes: {
        '123': {
          identifier: '123',
          name: 'filter',
          nf: {
            catalogId: '123',
            local: {}
          },
          "site": "test-site-nf",
          local: {}
        },
        left: {
          identifier: 'left',
          name: 'left',
          port: {
            tunnel: "tunnel1",
            local: {}
          },
          site: "test-site-port-left",
          local: {}
        },
        right: {
          identifier: 'right',
          name: 'right',
          port: {
            tunnel: "tunnel2",
          },
          site: "test-site-port-right",
          local: {}
        }
      },
      hasDraft: true
    };

    mockServiceResponse = {
      config: mockPipelineGraph
    }
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.sites).toBe(mockSites);
    expect(component.isDraft).toBeFalsy();
    expect(component['_tunnels'].length).toBe(0);
    expect(component.isInactiveDraft).toBeFalsy();
    expect(component.activeEdge).not.toBeDefined();
    expect(component.initialGraph).toBeFalsy();
    expect(component.handleDrop).toBeDefined();
    expect(component.onServiceRendered).toBeDefined();
    expect(component.nodeForm).toBeDefined();
  });

  it('should change states', () => {
    component.isDraft = true;
    expect(component.isDraft).toBeTruthy();
    const allowDrop = component.allowDrop(new CustomEvent('drop'));
    expect(allowDrop).toBeTruthy();
  });

  it('should start pipeline graph stream', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    component.active = false;
  }));

  it('should render pipeline graph', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    let nfs = el.querySelectorAll('.nf');
    let nfInstances = el.querySelectorAll('.nf-instance');
    let edges = el.querySelectorAll('.edge-container');
    let trafficPorts = el.querySelectorAll('.edge-container > .port');
    expect(nfs.length).toBe(1);
    expect(nfInstances.length).toBe(1);
    expect(edges.length).toBe(4);
    expect(trafficPorts.length).toBe(2);

    expect(el.querySelectorAll('#drawing > svg').length).toBe(1);
    expect(edges[0].querySelector('.marker-text tspan').textContent).toBe('ALL');
    expect(edges[1].querySelector('.marker-text tspan').textContent).toBe('ALL');
    expect(nfInstances[0].querySelector('.title tspan').textContent).toBe('Nefeli Networks');
    expect(nfInstances[0].querySelector('.content tspan').textContent).toBe('filter');
    expect(nfInstances[0].querySelectorAll('.nf .lop-stats tspan')[0].textContent).toBe('-');
    expect(nfInstances[0].querySelectorAll('.nf .lop-stats tspan')[1].textContent).toBe('-');
    expect(nfInstances[0].querySelectorAll('.nf .lop-stats tspan')[2].textContent).toBe('-');
    expect(nfs[0].querySelectorAll('.nf-port tspan')[0].textContent).toBe('LEFT');
    expect(nfs[0].querySelectorAll('.nf-port tspan')[1].textContent).toBe('RIGHT');
    component.active = false;
    tick();
  }));

  it('should remove stale nfs and traffic ports', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    tick();

    const deleteNFSpy = spyOn<any>(component, 'deleteNF').and.callThrough();
    const deletePortSpy = spyOn<any>(component, 'deletePort').and.callThrough();
    const mockGraph = cloneDeep(mockPipelineGraph);
    const mockNFID = '321'
    mockGraph.nodes[mockNFID] = {
      identifier: mockNFID,
      name: 'filter2',
      nf: {
        catalogId: mockNFID,
        local: {}
      },
      site: 'test-site-nf',
      local: {}
    };
    mockGraph.nodes['newTrafficPort'] = {
      identifier: 'newTrafficPort',
      port: {
        tunnel: 'tunnel2',
        local: {}
      },
      site: 'test-site-port-right',
      local: {}
    };
    mockGraph.edges.push(
      {
        a: {
          interface: 'right',
          node: mockNFID
        },
        b: {
          interface: '',
          node: 'newTrafficPort'
        },
        filterAb: {},
        filterBa: {}
      }
    );

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    let req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`);
    req.flush({ config: mockGraph });
    let manifestReq1 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    let manifestReq2 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${mockNFID}/manifest`
    );
    manifestReq1.flush(mockPipelineManifest);
    const manifest = cloneDeep(mockPipelineManifest);
    manifest.identifier = mockNFID;
    manifestReq2.flush(manifest);

    let nfs = el.querySelectorAll('.nf');
    let edges = el.querySelectorAll('.edge-container');
    let trafficPorts = el.querySelectorAll('.edge-container > .port');
    expect(nfs.length).toBe(2);
    expect(edges.length).toBe(6);
    expect(trafficPorts.length).toBe(3);

    tick(10000);

    const removedNF = component.nfs.get(mockNFID);
    const removedTrafficPort = component.ports.get('newTrafficPort');

    req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);
    manifestReq1 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq1.flush(mockPipelineManifest);

    nfs = el.querySelectorAll('.nf');
    edges = el.querySelectorAll('.edge-container');
    trafficPorts = el.querySelectorAll('.edge-container > .port');
    expect(nfs.length).toBe(1);
    expect(edges.length).toBe(4);
    expect(trafficPorts.length).toBe(2);

    expect(deleteNFSpy).toHaveBeenCalledWith(removedNF);
    expect(deletePortSpy).toHaveBeenCalledWith(removedTrafficPort);

    component.active = false;
    tick();
  }));

  it('should stop polling status when service doesn\'t exist', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);

    component.toggleView('instance');
    tick();

    const statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    statusReq.flush({}, {
      status: 404,
      statusText: '',
    });
    tick(10000);
    httpTestingController.expectNone(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );

    component.active = false;
    flush();
  }));

  it('should properly scale up', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);

    component.toggleView('instance');

    tick();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    statusReq.flush({ status: mockServiceStatus });

    let nfs = el.querySelectorAll('.nf');
    let nfInstances = el.querySelectorAll('.nf-instance');
    let edges = el.querySelectorAll('.edge-container');
    let trafficPorts = el.querySelectorAll('.edge-container > .port');

    expect(nfs.length).toBe(1);
    expect(nfInstances.length).toBe(2); // there's always an extra hidden instance
    expect(edges.length).toBe(4);
    expect(trafficPorts.length).toBe(2);

    tick(10000);

    const req2 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req2.request.method).toBe('GET');
    expect(req2.request.responseType).toBe('json');
    req2.flush(mockServiceResponse);

    const manifestReq2 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq2.flush(mockPipelineManifest);

    const statusReq2 = httpTestingController.match(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    )[0];
    const scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_2'] = {
      "identity": {
        "instanceId": "filter_2"
      },
      "location": {
        "site": "eric-exp",
        "placement": {
          "machineId": "sylvester",
          "draining": false,
          "coresBySocket": {
            "0": {
              "cores": [
                2
              ]
            }
          },
          "license": null
        }
      },
      "metadata": null,
      "state": "CONFIGURED",
      "launchSpec": null,
      "systemdUnit": "",
      "controlPorts": {},
      "telnetConsole": "",
      "placementStatus": {},
      "launchStatus": {}
    }

    statusReq2.flush({ status: scaleUpStatus });

    nfs = el.querySelectorAll('.nf');
    nfInstances = el.querySelectorAll('.nf-instance');
    edges = el.querySelectorAll('.edge-container');
    trafficPorts = el.querySelectorAll('.edge-container > .port');

    expect(nfs.length).toBe(1);
    expect(nfInstances.length).toBe(3);
    expect(edges.length).toBe(4);
    expect(trafficPorts.length).toBe(2);
    expect(nfInstances[0].getAttribute('opacity')).toBe('0.5');
    expect(nfInstances[1].getAttribute('opacity')).toBe('1');
    expect(nfInstances[2].getAttribute('opacity')).toBe('1');

    component.active = false;
    flush();
  }));

  it('should properly scale down', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);

    component.toggleView('instance');

    tick();
    let req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);
    req = undefined;

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    const scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_2'] = {
      "identity": {
        "instanceId": "filter_2"
      },
      "location": {
        "site": "eric-exp",
        "placement": {
          "machineId": "sylvester",
          "draining": false,
          "coresBySocket": {
            "0": {
              "cores": [
                2
              ]
            }
          },
          "license": null
        }
      },
      "metadata": null,
      "state": "CONFIGURED",
      "launchSpec": null,
      "systemdUnit": "",
      "controlPorts": {},
      "telnetConsole": "",
      "placementStatus": {},
      "launchStatus": {}
    }
    statusReq.flush({ status: scaleUpStatus });

    fixture.detectChanges();
    let nfs = el.querySelectorAll('.nf');
    let nfInstances = el.querySelectorAll('.nf-instance');
    let edges = el.querySelectorAll('.edge-container');
    let trafficPorts = el.querySelectorAll('.edge-container > .port');

    expect(nfs.length).toBe(1);
    expect(nfInstances.length).toBe(3); // extra hidden instance
    expect(edges.length).toBe(4);
    expect(trafficPorts.length).toBe(2);

    expect(edges[0].querySelector('.marker-text tspan').textContent).toBe('ALL');
    expect(edges[1].querySelector('.marker-text tspan').textContent).toBe('ALL');

    expect(nfInstances[0].querySelector('.title tspan').textContent).toBe('Nefeli Networks');
    expect(nfInstances[0].querySelector('.content tspan').textContent).toBe('filter');
    expect(nfInstances[0].getAttribute('opacity')).toBe('0.5');
    expect(nfInstances[1].querySelector('.title tspan').textContent).toBe('Nefeli Networks');
    expect(nfInstances[1].querySelector('.content tspan').textContent).toBe('filter');
    expect(nfInstances[1].getAttribute('opacity')).toBe('1');
    expect(nfInstances[2].querySelector('.title tspan').textContent).toBe('Nefeli Networks');
    expect(nfInstances[2].querySelector('.content tspan').textContent).toBe('filter');
    expect(nfInstances[2].getAttribute('opacity')).toBe('1');
    expect(nfs[0].querySelectorAll('.nf-port tspan')[0].textContent).toBe('LEFT');
    expect(nfs[0].querySelectorAll('.nf-port tspan')[1].textContent).toBe('RIGHT');

    tick(10000);
    const req2 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req2.request.method).toBe('GET');
    expect(req2.request.responseType).toBe('json');
    req2.flush(mockServiceResponse);

    const manifestReq2 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq2.flush(mockPipelineManifest);

    const statusReq2 = httpTestingController.match(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    )[0];
    statusReq2.flush({ status: mockServiceStatus });

    tick(5000);

    nfs = el.querySelectorAll('.nf');
    nfInstances = el.querySelectorAll('.nf-instance');
    edges = el.querySelectorAll('.edge-container');
    trafficPorts = el.querySelectorAll('.edge-container > .port');

    expect(nfs.length).toBe(1);
    expect(nfInstances.length).toBe(2);
    expect(edges.length).toBe(4);
    expect(trafficPorts.length).toBe(2);

    expect(edges[0].querySelector('.marker-text tspan').textContent).toBe('ALL');
    expect(edges[1].querySelector('.marker-text tspan').textContent).toBe('ALL');

    expect(nfInstances[0].querySelector('.title tspan').textContent).toBe('Nefeli Networks');
    expect(nfInstances[0].querySelector('.content tspan').textContent).toBe('filter');
    expect(nfInstances[0].getAttribute('opacity')).toBe('0.5');
    expect(nfInstances[1].querySelector('.title tspan').textContent).toBe('Nefeli Networks');
    expect(nfInstances[1].querySelector('.content tspan').textContent).toBe('filter');
    expect(nfInstances[1].getAttribute('opacity')).toBe('1');
    expect(nfs[0].querySelectorAll('.nf-port tspan')[0].textContent).toBe('LEFT');
    expect(nfs[0].querySelectorAll('.nf-port tspan')[1].textContent).toBe('RIGHT');

    component.active = false;
    flush();
  }));

  it('should create temp edge on interface click', () => {
    component.active = true;
    const port = createPort(component, 'left', 0, 0, mockSites[1].config.identifier);
    const portMap = new Map<string, PipelineNode>();
    portMap.set('left', port);
    component['drawSiteGroups']();
    component.ports = portMap;
    fixture.detectChanges();

    const worldPort = el.querySelectorAll('.port')[0] as SVGGElement;
    expect(worldPort).toBeDefined();
    const event = mouseClickEvent();
    component['onNfInterfaceClick'](event, undefined, undefined);

    fixture.detectChanges();
    expect(el.querySelectorAll('.edge.temp').length).toBe(1);
  });

  xit('should create edge when two interfaces are connected', () => {
    component.active = true;
    const port1 = createPort(component, 'left', 0, 0, mockSites[1].config.identifier, 0);
    const port2 = createPort(component, 'right', 100, 100, mockSites[2].config.identifier, 1);
    const portMap = new Map<string, PipelineNode>();
    portMap.set('left', port1);
    portMap.set('right', port2);

    component['addNodeToSiteMap'](port1, 0);
    component['addNodeToSiteMap'](port2, 1);
    component['drawSiteGroups']();
    component.ports = portMap;
    fixture.detectChanges();

    expect(el.querySelectorAll('.edge').length).toBe(0);
    expect(el.querySelectorAll('.port').length).toBe(2);

    const port1Interface = component.ports.get("left").port;
    const port2Interface = component.ports.get("right").port;

    const event = mouseClickEvent();
    component['onInterfaceMouseClick'](event, port1Interface.local.svgPort.element);
    fixture.detectChanges();
    component['onInterfaceMouseClick'](event, port2Interface.local.svgPort.element);
    fixture.detectChanges();

    expect(el.querySelectorAll('.edge.temp').length).toBe(0);
    expect(el.querySelectorAll('polyline.edge').length).toBe(1);

  });

  it('should create edge between nf and port', () => {
    component.editMode = true;
    component.active = true;
    const nfs = new Map();
    const ports = new Map();
    const port = createPort(component, 'Port1', 0, 0, mockSites[0].config.identifier);
    const nf = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier);
    ports.set('Port1', port);
    nfs.set('NF1', nf);

    component['drawSiteGroups']();

    component.ports = ports;
    component.nfs = nfs;
    const siteSvgGroup = component['_sitesNodesMap'].get(nf.site).svgGroup;
    fixture.detectChanges();
    expect(el.querySelectorAll('.edge').length).toBe(0);
    expect(el.querySelectorAll('.port').length).toBe(1);
    expect(el.querySelectorAll('.nf').length).toBe(1);
    connectWorldPortToNF(siteSvgGroup, nf, component);
    expect(el.querySelectorAll('.edge.temp').length).toBe(0);
    expect(el.querySelectorAll('polyline.edge').length).toBe(1);
  });

  it('should delete nf node', () => {
    component.active = true;
    const nf = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier);
    const nfs = new Map();
    nfs.set('NF1', nf);

    component['drawSiteGroups']();

    component.nfs = nfs;
    fixture.detectChanges();
    expect(el.querySelectorAll('.nf').length).toBe(1);
    component.deleteNF(nfs.get('NF1'));
    fixture.detectChanges();
    expect(el.querySelectorAll('.nf').length).toBe(0);
  });

  it('should delete port node', () => {
    component.active = true;
    const port = createPort(component, 'port1', 100, 100, mockSites[0].config.identifier);
    const ports = new Map();
    ports.set('port1', port);

    component['drawSiteGroups']();

    component.ports = ports;
    expect(component.ports.size).toBe(1);
    component.deletePort(ports.get('port1'));
    expect(component.ports.size).toBe(0);
  });

  it('should remove all nf edges when nf is deleted', () => {
    component.editMode = true;
    component.active = true;
    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier, ['port1']);
    const nf2 = createNF(component, 'NF2', 200, 100, mockSites[0].config.identifier, ['port2']);
    const nf3 = createNF(component, 'NF3', 300, 300, mockSites[0].config.identifier, ['port3']);
    nfs.set('NF1', nf1);
    nfs.set('NF2', nf2);
    nfs.set('NF3', nf3);

    component['drawSiteGroups']();

    component.nfs = nfs;
    fixture.detectChanges();
    connectNFs(component, nf1, nf2);
    connectNFs(component, nf1, nf3);
    // created edges between nf1 -> nf2 and nf1 -> nf3
    expect(el.querySelectorAll('polyline.edge').length).toBe(2);
    component.deleteNF(nf1);
    fixture.detectChanges();
    // all edges should be deleted
    expect(el.querySelectorAll('polyline.edge').length).toBe(0);
  });

  it('should remove all port edges when port is deleted', () => {
    component.editMode = true;
    component.active = true;
    const nfs = new Map();
    const ports = new Map();
    const nf1 = createNF(component, 'NF1', 300, 300, mockSites[0].config.identifier, ['port3']);
    nfs.set('NF1', nf1);

    component['drawSiteGroups']();
    component.nfs = nfs;
    component.ports = ports;
    fixture.detectChanges();

    const siteSvgGroup = component['_sitesNodesMap'].get(nf1.site).svgGroup;
    connectWorldPortToNF(siteSvgGroup, nf1, component);
    expect(el.querySelectorAll('polyline.edge').length).toBe(1);
    component.deletePort(component.ports.values().next().value);
    fixture.detectChanges();
    expect(el.querySelectorAll('polyline.edge').length).toBe(0);
  });

  it('should properly get graph back', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    component['_sites'] = mockSites;
    tick();

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    tick();

    const generatedGraph = component.getGraph() as PipelineGraph;

    expect(generatedGraph.edges.length).toBe(mockPipelineGraph.edges.length);
    expect(generatedGraph.edges[0].a.interface).toBe(mockPipelineGraph.edges[0].a.interface);
    expect(generatedGraph.edges[0].a.node).toBe(mockPipelineGraph.edges[0].a.node);
    expect(generatedGraph.edges[0].b.interface).toBe(mockPipelineGraph.edges[0].b.interface);
    expect(generatedGraph.edges[0].b.node).toBe(mockPipelineGraph.edges[0].b.node);
    expect(generatedGraph.edges[1].a.interface).toBe(mockPipelineGraph.edges[1].a.interface);
    expect(generatedGraph.edges[1].a.node).toBe(mockPipelineGraph.edges[1].a.node);
    expect(generatedGraph.edges[1].b.interface).toBe(mockPipelineGraph.edges[1].b.interface);
    expect(generatedGraph.edges[1].b.node).toBe(mockPipelineGraph.edges[1].b.node);
    expect(generatedGraph.nodes['123'].nf.catalogId).toBe(mockPipelineGraph.nodes['123'].nf.catalogId);
    expect(generatedGraph.nodes['123'].site).toBe('test-site-nf');

    expect(generatedGraph.nodes.left.port).toBeDefined();
    expect(generatedGraph.nodes.right.port).toBeDefined();
    expect(generatedGraph.nodes.left.site).toBe('test-site-port-left');
    expect(generatedGraph.nodes.right.site).toBe('test-site-port-right');

    component.active = false;
    flush();
  }));

  it('should change edge direction to the opposite', () => {
    component.editMode = true;
    component.active = true;
    createPairedNFs(fixture, component);
    let activeEdgeGroup = component.edges[0].local.svgEdge;
    let edge = component['getEdgeByElement'](activeEdgeGroup.element);

    // by default, edge should be bi-directional
    expect(edge.filterAb).toBeDefined();
    expect(edge.filterBa).toBeDefined();

    // changing edge direction to Downward
    component['_activeEdge'] = {
      a: {
        interface: '',
        node: ''
      },
      b: {
        interface: '',
        node: ''
      },
      local: {
        svgEdge: activeEdgeGroup
      }
    };

    component.onChangeEdgeDirection(EdgeDirection.Downward);
    fixture.detectChanges();
    activeEdgeGroup = component.edges[0].local.svgEdge;
    edge = component['getEdgeByElement'](activeEdgeGroup.element);
    expect(edge.filterBa).toBeDefined();
    expect(edge.filterAb).not.toBeDefined();

    // changing to Upward
    component['_activeEdge'] = {
      a: {
        interface: '',
        node: ''
      },
      b: {
        interface: '',
        node: ''
      },
      local: {
        svgEdge: activeEdgeGroup
      }
    };

    component.onChangeEdgeDirection(EdgeDirection.Upward);
    fixture.detectChanges();
    activeEdgeGroup = component.edges[0].local.svgEdge;;
    edge = component['getEdgeByElement'](activeEdgeGroup.element);
    expect(edge.filterAb).toBeDefined();
    expect(edge.filterBa).not.toBeDefined();
  });

  it('should change edge direction to both', () => {
    component.editMode = true;
    const el = fixture.debugElement.nativeElement;
    component.active = true;

    // create paired NFs supported bi-directionality
    createPairedNFs(fixture, component);

    let activeEdgeGroup = component.edges[0].local.svgEdge;
    let edge = component['getEdgeByElement'](activeEdgeGroup.element);
    expect(edge.filterAb).toBeDefined();
    expect(edge.filterBa).toBeDefined();

    // changing edge direction to Both
    component['_activeEdge'] = {
      a: {
        interface: '',
        node: ''
      },
      b: {
        interface: '',
        node: ''
      },
      local: {
        svgEdge: component.edges[0].local.svgEdge
      }
    };

    component.onChangeEdgeDirection(EdgeDirection.Both);
    fixture.detectChanges();
    edge = component['getEdgeByElement'](component.edges[0].local.svgEdge.element);
    expect(edge.filterAb).toBeDefined();
    expect(edge.filterBa).toBeDefined();
  });

  it('should remove edge', () => {
    component.editMode = true;
    component.active = true;
    const el = fixture.debugElement.nativeElement;
    createPairedNFs(fixture, component);
    const edge = component['getEdgeByElement'](component.edges[0].local.svgEdge.element);
    component['_activeEdge'] = {
      a: {
        interface: '',
        node: ''
      },
      b: {
        interface: '',
        node: ''
      },
      local: {
        svgEdge: edge.local.svgEdge
      }
    };
    component.onRemoveEdgeGroup();
    fixture.detectChanges();
    expect(el.querySelectorAll('polyline.edge').length).toBe(0);
  });

  it('should set draggable when edit mode', () => {
    spyOn<any>(component, 'initDrag').and.callThrough();
    component.active = true;
    component.editMode = true;

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier, ['port1']);
    nfs.set('NF1', nf1);

    component['drawSiteGroups']();
    component.nfs = nfs;
    fixture.detectChanges();
    expect(component['initDrag']).toHaveBeenCalled();
  });

  it('should set draggable when read-only mode', () => {
    spyOn<any>(component, 'initDrag').and.callThrough();
    component.active = true;
    component.editMode = false;

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier, ['port1']);
    nfs.set('NF1', nf1);

    component['drawSiteGroups']();
    component.nfs = nfs;
    fixture.detectChanges();
    expect(component['initDrag']).toHaveBeenCalled();
  });

  it('should not allow to connect nfs when not in edit mode', () => {
    component.active = true;
    component.editMode = false;

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier, ['port1']);
    nfs.set('NF1', nf1);

    component['drawSiteGroups']();
    component.nfs = nfs;
    fixture.detectChanges();

    nf1.nf.local.interfaces[0].local.ctx.fire('click');
    expect(component['_tempEdge']).toBeUndefined();
  });

  it('should display port labels when drawing edge', () => {
    component.active = true;
    component.editMode = true;
    const nfs = new Map();
    const nf = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier, ['port1', 'port2']);
    nfs.set('NF1', nf);

    component['drawSiteGroups']();
    component.nfs = nfs;
    fixture.detectChanges();
    const labels = el.querySelectorAll('.inf-label');
    expect(labels.length).toBe(2);
    expect(labels[0].classList.contains('hidden')).toBe(true);
    expect(labels[1].classList.contains('hidden')).toBe(true);

    const interface1 = nf.nf.local.interfaces[0];
    const event = mouseClickEvent();
    component['onNfInterfaceClick'](event, interface1.local.ctx, nf.site, 0);
    fixture.detectChanges();

    expect(labels[0].classList.contains('hidden')).toBe(false);
    expect(labels[1].classList.contains('hidden')).toBe(false);
  });

  it('should render and position up to eight NF ports', () => {
    component.active = true;
    component.editMode = false;
    const nfPorts = ['port1', 'port2', 'port3', 'port4', 'port5', 'port6', 'port7', 'port8'];
    const nfs = new Map();
    const nf = createNF(component, 'id1', 300, 300, mockSites[0].config.identifier, nfPorts);
    nfs.set('id1', nf);

    component['drawSiteGroups']();
    component.nfs = nfs;
    fixture.detectChanges();
    const renderedPorts = el.querySelectorAll('.nf-port');
    expect(renderedPorts.length).toBe(8);

    let hasSameCoordinates = false;
    for (let i = 0; i < renderedPorts.length; i++) {
      for (let j = i + 1; j < renderedPorts.length; j++) {
        if (renderedPorts[i].getAttribute('transform') === renderedPorts[j].getAttribute('transform')) {
          hasSameCoordinates = true;
        }
      }
    }
    expect(hasSameCoordinates).toBeFalsy();
  });

  it('should render Data and Data Tap ports only', () => {
    component.active = true;
    component.editMode = false;

    const nfNodes = new Map();
    const nfNode = createNF(component, '1', 300, 300, mockSites[0].config.identifier);

    nfNode.nf.local.type = NfType.THIRD_PARTY;
    const interfaces = [
      {
        id: 'Management Port',
        type: VMInterfaceType.MGMT
      },
      {
        id: 'Data Port',
        type: VMInterfaceType.DATA
      },
      {
        id: 'Data Tap Port',
        type: VMInterfaceType.DATATAP
      },
      {
        id: 'External Management Port',
        type: VMInterfaceType.EXTERNALMGMT
      }
    ];

    nfNode.nf.local.interfaces = interfaces;
    nfNodes.set('1', nfNode);

    component['drawSiteGroups']();
    component.nfs = nfNodes;
    fixture.detectChanges();

    const portDes = fixture.debugElement.queryAll(By.css('.nf-port'));
    expect(portDes.length).toBe(2, 'render Data ports only');
    expect(portDes[0].nativeElement.textContent).toBe(
      interfaces[1].id.toUpperCase(),
      'render Data port'
    );
    expect(portDes[1].nativeElement.textContent).toBe(
      interfaces[2].id.toUpperCase(),
      'render Data Tap port'
    );
  });

  it('should display ALL traffic filter by default', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const markerTexts = el.querySelectorAll('.marker-text');
    expect(markerTexts[0].querySelector('tspan').textContent).toBe('ALL');
    expect(markerTexts[1].querySelector('tspan').textContent).toBe('ALL');

    component.active = false;
    tick();
  }));

  it('should display specified traffic filter', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    const graph = cloneDeep(mockPipelineGraph);
    graph.edges[0].filterAb.bpf = 'http';
    graph.edges[1].filterBa.bpf = 'not http';

    req.flush({ config: graph });

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const markerTexts = el.querySelectorAll('.marker-text');
    expect(markerTexts[0].querySelector('tspan').textContent).toBe('not http ⟷ ALL');
    expect(markerTexts[1].querySelector('tspan').textContent).toBe('ALL ⟷ http');

    component.active = false;
    tick();
  }));

  it('should display both traffic filters if exists', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    const graph = cloneDeep(mockPipelineGraph);
    graph.edges[0].filterAb.bpf = 'http';
    graph.edges[0].filterBa.bpf = 'http';
    graph.edges[1].filterAb.bpf = 'tcp';
    graph.edges[1].filterBa.bpf = 'udp';

    req.flush({ config: graph });

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    expect(el.querySelectorAll('.marker-text')[0].querySelector('tspan').textContent).toBe('udp ⟷ tcp');
    expect(el.querySelectorAll('.marker-text')[1].querySelector('tspan').textContent).toBe('http');

    component.active = false;
    tick();
  }));

  it('should update line when modifying filter', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();
    tick(1000);

    const markerText = el.querySelectorAll('.marker-text')[1];
    let markerArrow = el.querySelectorAll('.marker-arrow')[1];
    expect(el.querySelectorAll('.marker-text')[0].querySelector('tspan').textContent).toBe('ALL');

    // make sure line and text are green (no filter mode)
    expect(markerText.querySelector('tspan').textContent).toBe('ALL');
    expect(markerText.classList.contains('label')).toBe(true);
    expect(markerArrow.classList.contains('edge')).toBe(true);

    component.updateEdgeFilter(component.edges[0], 'arp');

    // make sure line and text are blue (filter mode)
    markerArrow = el.querySelectorAll('.marker-arrow')[1];
    expect(markerText.querySelector('tspan').textContent).toBe('ALL ⟷ arp');
    expect(markerArrow.classList.contains('edge-filter')).toBe(true);
    expect(markerText.querySelector('tspan').classList.contains('label-filter')).toBe(true);

    component.updateEdgeFilter(component.edges[0], '');
    markerArrow = el.querySelectorAll('.marker-arrow')[1];
    // make sure line and text go back to green for emptied value (no filter mode)
    expect(markerText.querySelector('tspan').textContent).toBe('ALL');
    expect(markerArrow.classList.contains('edge')).toBe(true);
    expect(markerText.querySelector('tspan').classList.contains('label')).toBe(true);

    component.updateEdgeFilter(component.edges[0], 'arp');
    component.updateEdgeFilter(component.edges[0], 'ALL');

    // make sure line and text go back to green from filter for 'ALL' value (no filter mode)
    expect(markerText.querySelector('tspan').textContent).toBe('ALL');
    expect(markerArrow.classList.contains('edge')).toBe(true);
    expect(markerText.querySelector('tspan').classList.contains('label')).toBe(true);

    component.active = false;
    tick();
  }));

  it('should toggle instance / logical view', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const el = fixture.debugElement.nativeElement;
    expect(el.querySelectorAll('.nf-instance').length).toBe(1);

    // in logical view, only one instance should be visible
    expect(el.querySelectorAll('.nf-instance')[0].getAttribute('opacity')).toBe('1');
    expect(el.querySelectorAll('.nf-instance')[1]).toBeUndefined();

    // toggle to instance view
    component.toggleView('instance');

    const statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    const scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_2'] = {
      "identity": {
        "instanceId": "filter_2"
      },
      "location": {
        "site": "eric-exp",
        "placement": {
          "machineId": "sylvester",
          "draining": false,
          "coresBySocket": {
            "0": {
              "cores": [
                2
              ]
            }
          },
          "license": null
        }
      },
      "metadata": null,
      "state": "CONFIGURED",
      "launchSpec": null,
      "systemdUnit": "",
      "controlPorts": {},
      "telnetConsole": "",
      "placementStatus": {},
      "launchStatus": {}
    }
    statusReq.flush({ status: scaleUpStatus });

    tick(5000);

    // in instance view, both instances should be visible
    expect(el.querySelectorAll('.nf-instance')[0].getAttribute('opacity')).toBe('1');
    expect(el.querySelectorAll('.nf-instance')[1].getAttribute('opacity')).toBe('1');
    expect(el.querySelectorAll('.nf-instance')[2].getAttribute('opacity')).toBe('1');

    // in instance view, verify there's a legend
    expect(component.serverLegendItems.length).toBe(1);

    // toggle back to logical view
    component.toggleView('logical');
    tick(5000);

    // in logical view, only one instance should be visible
    expect(el.querySelectorAll('.nf-instance')[0].getAttribute('opacity')).toBe('1');
    expect(el.querySelectorAll('.nf-instance')[1].getAttribute('opacity')).toBe('0');
    expect(el.querySelectorAll('.nf-instance')[2].getAttribute('opacity')).toBe('0');

    // in logical view, verify there's no legend
    expect(component.serverLegendItems.length).toBe(0);

    component.active = false;
  }));

  it('should toggle performance trace on / off', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const traceReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}/trace`
    );
    traceReq.flush({
      trace: {
        bpf: {
          value: "udp"
        }
      }
    });

    const el = fixture.debugElement.nativeElement;

    // by default, performance trace set to 'udp'
    expect(component.perfTrace.filter).toBe('udp');
    fixture.detectChanges();
    tick(5000);
    expect(el.querySelectorAll('.lop-stats')[0].getAttribute('opacity')).toBe('1');
    expect(el.querySelector('.perf-trace-legend')).toBeDefined();

    // disable performance trace and verify lop stats are hidden
    component.enablePerformanceTrace(false);
    fixture.detectChanges();
    tick(5000);
    expect(el.querySelectorAll('.lop-stats')[0].getAttribute('opacity')).toBe('0');
    expect(el.querySelector('.perf-trace-legend')).toBeNull();

    // enable performance trace and verify lop stats are visible
    component.enablePerformanceTrace(true, 'udp');
    fixture.detectChanges();
    tick(5000);
    expect(el.querySelectorAll('.lop-stats')[0].getAttribute('opacity')).toBe('1');
    expect(el.querySelector('.perf-trace-legend')).toBeDefined();

    // switch to instance view and verify lop stats are hidden
    component.toggleView('instance');
    fixture.detectChanges();
    tick(5000);
    expect(el.querySelectorAll('.lop-stats')[0].getAttribute('opacity')).toBe('0');
    expect(el.querySelector('.perf-trace-legend')).toBeNull();

    component.active = false;
  }));

  it('should display performance trace stats', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const traceReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}/trace`
    );
    traceReq.flush({
      trace: {
        bpf: {
          value: "udp"
        }
      }
    });

    tick();
    const perfTraceReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}/stats`
    );
    perfTraceReq.flush({
      "123": {
        "loss": "4",
        "throughput": "19870000000",
        "latency": "123"
      }
    } as any);

    const el = fixture.debugElement.nativeElement;
    expect(el.querySelectorAll('.lop-stats')[0].querySelectorAll('tspan')[0].textContent).toBe("19.87Gbps");
    expect(el.querySelectorAll('.lop-stats')[0].querySelectorAll('tspan')[1].textContent).toBe("123ns");
    expect(el.querySelectorAll('.lop-stats')[0].querySelectorAll('tspan')[2].textContent).toBe("4pps");

    component.active = false;
  }));

  it('should convert life of packets numbers to fit the containers', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    const traceReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}/trace`
    );
    traceReq.flush({
      trace: {
        bpf: {
          value: "udp"
        }
      }
    });

    tick();

    const perfTraceReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}/stats`
    );
    perfTraceReq.flush({
      "123": {
        "loss": "999999999999999999999999",
        "throughput": "19870000002349234923940",
        "latency": "50009999000000009900"
      }
    } as any);

    const el = fixture.debugElement.nativeElement;
    expect(el.querySelectorAll('.lop-stats')[0].querySelectorAll('tspan')[0].textContent).toBe("2e+10Tbps");
    expect(el.querySelectorAll('.lop-stats')[0].querySelectorAll('tspan')[1].textContent).toBe("5e+10s");
    expect(el.querySelectorAll('.lop-stats')[0].querySelectorAll('tspan')[2].textContent).toBe("1e+12Tpps");

    component.active = false;
  }));

  it('should not render lop containers in the edit mode', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component.editMode = true;
    tick();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    req.flush(mockPipelineGraph);

    tick();
    httpTestingController.expectNone(
      `${environment.restServer}/dashboard/tenants/1/services/${mockPipelines[0].identifier}/stats`
    );
    const el = fixture.debugElement.nativeElement;
    const lopContainer = el.querySelector('.lop-stats');
    expect(lopContainer).toBeNull();
    component.active = false;
  }));

  it('should modify the bpf of the performance trace', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    const el = fixture.debugElement.nativeElement;
    const editButton = el.querySelector('.bpf > .edit');

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    const graph = cloneDeep(mockPipelineGraph);
    req.flush({ config: graph });
    fixture.detectChanges();

    // verify by default, performance trace is disabled and filter is not defined
    expect(component.perfTrace.filter).toBeUndefined();
    expect(component.perfTrace.enabled).toBe(false);

    // open bpf input dialog and enter 'http' as bpf
    editButton.click();
    tick();
    const input = (document.querySelector('nef-input-dialog input') as any);
    const dialogButton = (document.querySelector('nef-input-dialog img') as any);
    input.value = 'http';
    dialogButton.click();
    tick();

    // verify bpf filter has changed
    expect(component.perfTrace.filter).toBe('http');
    expect(component.perfTrace.enabled).toBe(false);

    // enable performance trace
    component.enablePerformanceTrace(true);

    const req2 = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}/trace`
    );
    expect(req2.request.method).toBe('POST');
    expect(req2.request.responseType).toBe('json');
    expect(req2.request.body).toBe('http');

    component.active = false;
    tick(2000);
  }));

  const setupNfConfig = (component: PipelineComponent, editMode = true) => {
    const mockTunnels: Tunnels = {
      tunnels: [
        {
          config: {
            name: 'Tunnel 1',
            siteId: 'eric_site'
          }
        },
        {
          config: {
            name: 'Tunnel 2',
            siteId: 'eric_site'
          }
        }
      ]
    };

    httpTestingController
      .expectOne(`${environment.restServer}/v1/tenants/12345/tunnels?status=true`)
      .flush(mockTunnels);

    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component.editMode = editMode;
    component.ngOnInit();

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush({}, {
      status: 404,
      statusText: 'Not found'
    });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    if (editMode) {
      const sitesReq = httpTestingController.expectOne(`${environment.restServer}/v1/sites`);
      expect(sitesReq.request.method).toBe('GET');
      expect(sitesReq.request.responseType).toBe('json');

      const mockSitesResponse: Sites = {
        sites: [
          {
            config: {
              identifier: 'eric_site',
              name: 'Eric Site'
            }
          },
          ...mockSites
        ]
      };

      sitesReq.flush(mockSitesResponse);
    }
  };

  it('should display NF config when click on legacy NF', () => {
    spyOn(component['_sidebarService'], 'open').and.callFake((title?: string, content?: ComponentType<unknown>, data?: unknown) => {
      debugger;
    });

    setupNfConfig(component);
    fixture.detectChanges();

    const nfElem: SVGGElement = el.querySelector('.nf');
    nfElem.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    // expect(component['_sidebarService'].open).toHaveBeenCalled();

    // TODO(ecarino): update for new sidebar
  });

  it('should not display nf config when click on nf but not in edit mode', () => {
    setupNfConfig(component, false);

    const nfElem: SVGGElement = el.querySelector('.nf');
    nfElem.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(document.querySelector('.popup.node')).toBeNull();
  });

  it('should display NF config dialog for legacy NF', () => {
    setupNfConfig(component);
    const nfElem: SVGGElement = el.querySelector('.nf');
    nfElem.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    // TODO(ecarino): update for new sidebar
  });

  it('should not show scaled nfs when in logical view mode', fakeAsync(() => {
    const graph = cloneDeep(mockPipelineGraph);
    graph.nodes['123'].nf.local.instances = mockNfInstances;

    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component['_isLogicalView'] = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    let req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    expect(req.request.method).toBe('GET');
    req.flush({ config: graph });

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    httpTestingController.expectNone(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );

    expect(el.querySelectorAll('.nf-instance').length).toBe(1);
    let mainInstance = el.querySelectorAll('.main-instance');
    let scaledInstances = el.querySelectorAll('.scaled-instance');
    expect(scaledInstances.length).toBe(0);
    expect(mainInstance.length).toBe(1);
    expect(mainInstance[0].getAttribute('opacity')).toBe('1');
    component.active = false;
    flush();
  }));

  it('should display empty bpf input if filter value is undefined', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    const el = fixture.debugElement.nativeElement;
    const editButton = el.querySelector('.bpf > .edit');

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );

    const graph = cloneDeep(mockPipelineGraph);
    req.flush({ config: graph });

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);
    fixture.detectChanges();

    editButton.click();
    tick();
    const input: HTMLInputElement = document.querySelector('nef-input-dialog input');
    expect(input.value).not.toBe('undefined');
    component.active = false;
  }));

  it('should display traffic port configuration popup', fakeAsync(() => {
    spyOn(component['_sidebarService'], 'open').and.callFake((title?: string, content?: ComponentType<unknown>, data?: TrafficPortFormData) => {
      expect(title).toBe('Encapsulation Settings');
      expect(content.name).toBe('TrafficPortFormComponent');
      expect(data.site).toEqual({ identifier: 'test-site-port-right', name: 'Right Site', siteType: 'CLUSTER' });
      expect(data.tenant).toEqual({ identifier: '1', name: 'Test Tenant' });

      const tunnel: TunnelConfig = {
        identifier: '123',
        name: 'Updated Tunnel'
      };

      component['_sidebarService'].close(tunnel);
    });

    setupNfConfig(component, true);

    const encapPort = el.querySelector('.edge-container > .port');
    encapPort.dispatchEvent(new Event('click'));

    expect(component['_sidebarService'].open).toHaveBeenCalled();

    fixture.detectChanges();
    tick();

    // TODO(ecarino): update to sideabar
  }));

  it('should modify nf interfaces',
    fakeAsync(inject([TenantService], (tenantService: TenantService) => {
      const manifest: NFDescriptor = {
        identifier: '123',
        name: 'Router',
        type: NfType.THIRD_PARTY,
        controllerType: 'NF_CONTROLLER',
        components: {
          datapath: {
            name: 'Router',
            vendor: 'Arista',
            type: 'CONTROLLER',
            vmManifest: {
              images: [],
              interfaces: [
                {
                  id: 'left',
                  type: VMInterfaceType.DATA
                },
                {
                  id: 'right',
                  type: VMInterfaceType.DATATAP
                }
              ],
              extraQemuCpuFlags: '',
              extraQemuFlags: '',
              licenseMode: 'NO_LICENSE'
            },
            execManifest: {},
            cpuCores: '',
            memoryMb: ''
          }
        }
      };
      let interfaces: VMInterface[] = manifest.components.datapath.vmManifest.interfaces;

      component.pipeline = mockPipelines;
      component.tenant = mockTenant;
      component.active = true;
      component.editMode = true;

      tenantService['_nfCatalog'] = {
        'Arista': [{
          local: {
            catalogId: 'bess_bypass_noop_bidir',
            id: 'bess_bypass_noop_bidir',
            vendor: 'Arista',
          },
          ...manifest
        }]
      };

      tick();
      httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush({}, {
        status: 404,
        statusText: 'Not found'
      });

      const req = httpTestingController.expectOne(
        `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
      );

      expect(req.request.method).toBe('GET');
      expect(req.request.responseType).toBe('json');
      req.flush(mockServiceResponse);

      const manifestReq = httpTestingController.expectOne(
        `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
      );
      manifestReq.flush(mockPipelineManifest);

      tick();

      const node: PipelineNode = component.nfs.get('123');

      // verify two interfaces are rendered
      expect(el.querySelectorAll('.nf .nf-port').length).toBe(2);
      expect(el.querySelectorAll('.nf .nf-port tspan')[0].textContent).toBe("LEFT");
      expect(el.querySelectorAll('.nf .nf-port tspan')[1].textContent).toBe("RIGHT");

      // remove 'left' interface
      interfaces.splice(0, 1);
      component['updateNF'](node, manifest);
      expect(el.querySelectorAll('.nf .nf-port').length).toBe(1);
      expect(el.querySelector('.nf .nf-port tspan').textContent).toBe("RIGHT");

      // put back 'left' interface
      interfaces.unshift({
        id: 'left',
        type: VMInterfaceType.DATA
      } as VMInterface);

      component['updateNF'](node, manifest);
      expect(el.querySelectorAll('.nf .nf-port').length).toBe(2);
      expect(el.querySelectorAll('.nf .nf-port tspan')[0].textContent).toBe("LEFT");
      expect(el.querySelectorAll('.nf .nf-port tspan')[1].textContent).toBe("RIGHT");

      component.active = false;
      flush();
    })));

  it('should display interface name on hover', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    const infEl: HTMLDivElement = fixture.debugElement.nativeElement;
    const infs = infEl.querySelectorAll('.nf .nf-port');

    // by default, label is hidden
    let leftHidden = infs[0].querySelector('text').classList.contains('hidden');
    expect(leftHidden).toBe(true);

    // hover over left port and label should be visible
    infs[0].dispatchEvent(new CustomEvent('mouseenter'));
    leftHidden = infs[0].querySelector('text').classList.contains('hidden');
    expect(leftHidden).toBe(false);

    // right label should remain hidden
    const rightHidden = infs[1].querySelector('text').classList.contains('hidden');
    expect(rightHidden).toBe(true);

    // mouse leave left port and label should be hidden again
    infs[0].dispatchEvent(new CustomEvent('mouseleave'));
    leftHidden = infs[0].querySelector('text').classList.contains('hidden');
    expect(leftHidden).toBe(true);
  });

  it('should display edge config popup when click on edge', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component.editMode = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush({}, {
      status: 404,
      statusText: 'Not found'
    });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    const edge: HTMLElement = fixture.debugElement.nativeElement.querySelector('.edge-container');
    edge.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    // TODO(eric): handle new sidebar
  });

  it('should update nodes and edges when changing name', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    const oldGraph = component.getGraph();
    expect(oldGraph.edges[0].b.node).toBe('123');
    expect(oldGraph.edges[1].a.node).toBe('123');

    const node = component.nfs.get('123');
    node.local.id = '123';
    component.onNodeChange(node, 'newNodeName');

    const newGraph = component.getGraph();
    expect(newGraph.edges[0].b.node).toBe('123');
    expect(newGraph.edges[1].a.node).toBe('123');
    expect(newGraph.nodes['123'].name).toBe('newNodeName');

    component.onNodeChange(node, 'filter');
  });

  it('should display error when failed to draw graph', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    spyOn<any>(component['_log'], 'error').and.callFake((err) => {
      expect(err.message).toBeDefined();
    });

    spyOn<any>(component['_alert'], 'error').and.callFake((msg: string) => {
      expect(msg).toBe('Could not properly render service.');
    });

    spyOn<any>(component, 'drawEdges').and.throwError('error');

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    expect(component['_log'].error).toHaveBeenCalled();
    expect(component['_alert'].error).toHaveBeenCalled();
  });

  it('should properly display nf instance status indicator', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    component.toggleView('instance');

    tick();

    let statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    const scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.LAUNCHING;
    scaleUpStatus.nodes['123'].instances['filter_2'] = {
      "identity": {
        "instanceId": "filter_2"
      },
      "location": {
        "site": "eric-exp",
        "placement": {
          "machineId": "sylvester"
        }
      },
      "state": "CONFIGURED",
      "placementStatus": {},
      "launchStatus": {}
    }
    statusReq.flush({ status: scaleUpStatus });

    let mainDotEl: SVGElement = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[0];
    let scaledDotEl: SVGElement = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[1];
    expect((mainDotEl.querySelector('.error') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.success') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('');
    expect((scaledDotEl.querySelector('.error') as HTMLElement).style.display).toBe('none');
    expect((scaledDotEl.querySelector('.success') as HTMLElement).style.display).toBe('');
    expect((scaledDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('none');

    tick(2000);

    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.FAILED;
    statusReq.flush({ status: scaleUpStatus });

    expect((mainDotEl.querySelector('.error') as HTMLElement).style.display).toBe('');
    expect((mainDotEl.querySelector('.success') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('none');

    tick(2000);

    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.LAUNCHING;
    statusReq.flush({ status: scaleUpStatus });
    fixture.detectChanges();
    expect((mainDotEl.querySelector('.error') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.success') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('');

    tick(2000);

    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.LAUNCHED;
    statusReq.flush({ status: scaleUpStatus });
    fixture.detectChanges();
    expect((mainDotEl.querySelector('.error') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.success') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('');

    tick(2000);

    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.CONFIGURED;
    statusReq.flush({ status: scaleUpStatus });
    fixture.detectChanges();
    expect((mainDotEl.querySelector('.error') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.success') as HTMLElement).style.display).toBe('');
    expect((mainDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('none');

    // NF instance status indicator should be hidden in logical view
    component.toggleView('logical');
    tick(5000);

    mainDotEl = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[0];
    scaledDotEl = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[1];

    expect(mainDotEl.style.display).toBe('none');
    expect(scaledDotEl.style.display).toBe('none');

    // NF instance status indicator should be shown back in instance view
    component.toggleView('instance');

    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    statusReq.flush({ status: scaleUpStatus });
    fixture.detectChanges();

    mainDotEl = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[0];
    scaledDotEl = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[1];

    expect(mainDotEl.style.display).toBe('');
    expect(scaledDotEl.style.display).toBe('');

    component.active = false;
    tick();
    flush();
  }));

  it('should render tooltip when error indicator is hovered', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    component.toggleView('instance');

    const statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    const scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.FAILED;
    statusReq.flush({ status: scaleUpStatus });
    fixture.detectChanges();

    const mainDotEl: SVGElement = <SVGElement> el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[0];
    mainDotEl.dispatchEvent(new Event('mouseover'));
    fixture.detectChanges();
    const tooltip: SVGAElement = <SVGAElement> el.querySelectorAll('.svg-tooltip')[1];
    expect(tooltip.style.display).toBe('none');

    component.active = false;
  });

  it('should display current nf instance state popup', fakeAsync(() => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    component.toggleView('instance');

    tick();

    let statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    let scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.NULLINSTANCESTATE;
    statusReq.flush({ status: scaleUpStatus });

    const mainDotEl = el.querySelectorAll('.nf-instance.scaled-instance .status-dot')[0];
    expect((mainDotEl.querySelector('.error') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.success') as HTMLElement).style.display).toBe('none');
    expect((mainDotEl.querySelector('.in-progress') as HTMLElement).style.display).toBe('');

    mainDotEl.dispatchEvent(new Event('mousedown'));
    fixture.detectChanges();

    let stepper = fixture.debugElement.query(By.directive(MatStepper));
    let substeps = fixture.debugElement.queryAll(By.css('.substep-label'));

    // first step is selected "Placing"
    tick(2000);
    expect(stepper.componentInstance.selectedIndex).toBe(0);
    expect(substeps[0].classes.active).toBeUndefined();
    expect(substeps[0].nativeNode.textContent).toBe('Machine resources');
    expect(substeps[1].classes.active).toBe(true);
    expect(substeps[1].nativeNode.textContent.trim()).toBe('Licenses allocated');
    expect(substeps[2].classes.active).toBeUndefined();
    expect(substeps[2].nativeNode.textContent).toBe('Files downloaded');
    expect(substeps[3].classes.active).toBeUndefined();
    expect(substeps[3].nativeNode.textContent.trim()).toBe('Memory allocated');
    expect(substeps[4].classes.active).toBeUndefined();
    expect(substeps[4].nativeNode.textContent).toBe('Instance started');
    expect(substeps[5].classes.active).toBeUndefined();
    expect(substeps[5].nativeNode.textContent).toBe('');

    // trigger second step "Launching" w/ qemu error and waiting on files
    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.LAUNCHING;
    scaleUpStatus.nodes['123'].instances['filter_1'].placementStatus.blockedOnMachineResources = false;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockedOnMemory = false;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockedOnHypervisor = true;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockedOnFiles = true;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.hypervisorStatus = 'Test Hypervisor Status';
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockingFiles = ['file1', 'file2', 'file3'];
    statusReq.flush({ status: scaleUpStatus });

    fixture.detectChanges();
    tick(2000);
    expect(stepper.componentInstance.selectedIndex).toBe(1);
    substeps = fixture.debugElement.queryAll(By.css('.substep-label'));
    expect(substeps[0].classes.active).toBe(true);
    expect(substeps[0].nativeNode.textContent).toBe('Machine resources');
    expect(substeps[1].classes.active).toBe(true);
    expect(substeps[1].nativeNode.textContent.trim()).toBe('Licenses allocated');
    expect(substeps[2].classes.active).toBeUndefined();
    expect(substeps[2].nativeNode.textContent).toBe('Waiting on file(s)...');
    expect(substeps[3].classes.active).toBeUndefined();
    expect(substeps[3].nativeNode.textContent.trim()).toBe('• file1');
    expect(substeps[4].classes.active).toBeUndefined();
    expect(substeps[4].nativeNode.textContent.trim()).toBe('• file2');
    expect(substeps[5].classes.active).toBeUndefined();
    expect(substeps[5].nativeNode.textContent.trim()).toBe('• file3');
    expect(substeps[6].classes.active).toBe(true);
    expect(substeps[6].nativeNode.textContent.trim()).toBe('Memory allocated');
    expect(substeps[7].classes.active).toBeUndefined();
    expect(substeps[7].nativeNode.textContent).toBe('Launch failure');
    expect(substeps[8].classes.active).toBeUndefined();
    expect(substeps[8].nativeNode.textContent).toBe('Test Hypervisor Status');

    // trigger second step "Launching" completes
    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.LAUNCHING;
    scaleUpStatus.nodes['123'].instances['filter_1'].placementStatus.blockedOnMachineResources = false;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockedOnMemory = false;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockedOnHypervisor = false;
    scaleUpStatus.nodes['123'].instances['filter_1'].launchStatus.blockedOnFiles = false;
    statusReq.flush({ status: scaleUpStatus });

    fixture.detectChanges();
    tick(2000);
    expect(stepper.componentInstance.selectedIndex).toBe(1);
    substeps = fixture.debugElement.queryAll(By.css('.substep-label'));
    expect(substeps[0].classes.active).toBe(true);
    expect(substeps[0].nativeNode.textContent).toBe('Machine resources');
    expect(substeps[1].classes.active).toBe(true);
    expect(substeps[1].nativeNode.textContent.trim()).toBe('Licenses allocated');
    expect(substeps[2].classes.active).toBe(true);
    expect(substeps[2].nativeNode.textContent).toBe('Files downloaded');
    expect(substeps[3].classes.active).toBe(true);
    expect(substeps[3].nativeNode.textContent.trim()).toBe('Memory allocated');
    expect(substeps[4].classes.active).toBe(true);
    expect(substeps[4].nativeNode.textContent).toBe('Instance started');
    expect(substeps[5].classes.active).toBeUndefined();
    expect(substeps[5].nativeNode.textContent).toBe('');

    // trigger third step "Configuring"
    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.LAUNCHED;
    statusReq.flush({ status: scaleUpStatus });

    fixture.detectChanges();
    tick(2000);
    expect(stepper.componentInstance.selectedIndex).toBe(2);

    // trigger fourth step "Active"
    statusReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances['filter_1'].state = InstanceStatusState.CONFIGURED;
    statusReq.flush({ status: scaleUpStatus });

    fixture.detectChanges();
    tick(2000);
    expect(stepper.componentInstance.selectedIndex).toBe(4);

    // dismiss popup
    component.onBackdropClick();

    component.active = false;
    flush();
  }));

  it('should have proper color based on hosting server', () => {
    window.localStorage.clear();

    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    component.toggleView('instance');

    const statusReq = httpTestingController.match(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    )[0];
    const scaleUpStatus = cloneDeep(mockServiceStatus);
    scaleUpStatus.nodes['123'].instances.filter_1.location.placement.machineId = 'fluffy';
    scaleUpStatus.nodes['123'].instances['filter_2'] = {
      "identity": {
        "instanceId": "filter_2"
      },
      "location": {
        "site": "eric-exp",
        "placement": null
      },
      "state": "CONFIGURED"
    };
    statusReq.flush({ status: scaleUpStatus });

    const nfInstances = component.nfs.get('123').nf.local.instances;
    const inst1Border = nfInstances[0].local.topBorderPattern.select('.top-border').members[0];
    const inst2Border = nfInstances[1].local.topBorderPattern.select('.top-border').members[0];

    expect(inst1Border.classes()[1]).toBe('blue');
    expect(inst2Border.classes()[1]).toBe('dark-grey');

    component.active = false;
  });

  it('should not show legend in edit mode', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component.editMode = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush({}, {
      status: 404,
      statusText: 'Not found'
    });

    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    );
    req.flush(mockServiceResponse);

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();
    const legends = el.querySelectorAll('nef-legend');
    expect(legends.length).toBe(0);
  });

  it('should validate nf name input', () => {
    setupNfConfig(component);
    fixture.detectChanges();
    const nfElem: SVGGElement = el.querySelector('.nf');
    nfElem.dispatchEvent(new Event('click'));
    fixture.detectChanges();

    // TODO(eric): should update to use sidebar

    // const popupEl: HTMLElement = document.querySelector('.popup.node');
    // const nameInputEl: HTMLInputElement = popupEl.querySelector('input[formcontrolname="name"]');
    // expect(nameInputEl).not.toBeNull();
    // expect(nameInputEl.value).toBe('filter');

    // nameInputEl.value = 'filter-1';
    // nameInputEl.dispatchEvent(new Event('input'));
    // nameInputEl.dispatchEvent(new Event('blur'));
    // fixture.detectChanges();
    // let errorEl = popupEl.querySelector<HTMLUnknownElement>('mat-form-field mat-error');
    // expect(errorEl).not.toBeNull();
    // expect(errorEl.textContent.trim()).toBe('Invalid format, please try again');

    // component.onBackdropClick();
    // expect(component['_popups'].node.isOpen).toBe(true);

    // nameInputEl.value = '';
    // nameInputEl.dispatchEvent(new Event('input'));
    // nameInputEl.dispatchEvent(new Event('blur'));
    // fixture.detectChanges();
    // errorEl = popupEl.querySelector('mat-form-field mat-error');
    // expect(errorEl).not.toBeNull();
    // expect(errorEl.textContent.trim()).toBe('Name is required');

    // component.onBackdropClick();
    // expect(component['_popups'].node.isOpen).toBe(true);

    // nameInputEl.value = 'Valid_NF_Name_1234567890';
    // nameInputEl.dispatchEvent(new Event('input'));
    // nameInputEl.dispatchEvent(new Event('blur'));
    // fixture.detectChanges();
    // errorEl = popupEl.querySelector('input[formcontrolname="name"] + mat-error');
    // expect(errorEl).toBeNull();

    // component.onBackdropClick();
    // expect(component['_popups'].node.isOpen).toBe(false);
  });

  it('should not open NF after deleting it', () => {
    setupNfConfig(component);
    fixture.detectChanges();
    let nfElem: SVGGElement = el.querySelector('.nf');
    expect(nfElem).not.toBeNull();

    const nfRemoveIcon: SVGGElement = nfElem.querySelector('.remove-icon');
    expect(nfRemoveIcon).not.toBeNull();
    nfRemoveIcon.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    nfElem = el.querySelector('.nf');
    expect(nfElem).toBeNull();

    const popupElem: HTMLElement = document.querySelector('.popup.node');
    expect(popupElem).toBeNull();
  });

  it('should properly update edges when moving nf', () => {
    setupNfConfig(component);
    fixture.detectChanges();

    const deltaX = 42;
    const deltaY = 77;
    const edges = component.nfs.get('123').local.edges;
    const initialEdge1A = edges[0].local.coordinates.a;
    const initialEdge1B = edges[0].local.coordinates.b;
    const initialEdge2A = edges[1].local.coordinates.a;
    const initialEdge2B = edges[1].local.coordinates.b;
    const nfElem = fixture.debugElement.nativeElement.querySelector('.nf');
    const event = new CustomEvent('dragmove', {
      detail: {
        delta: {
          movedX: deltaX,
          movedY: deltaY
        }
      }
    });
    nfElem.dispatchEvent(event);
    nfElem.dispatchEvent(new CustomEvent('dragend'));

    const deltaEdge1aX = edges[0].local.coordinates.a[0] - initialEdge1A[0];
    const deltaEdge1aY = edges[0].local.coordinates.a[1] - initialEdge1A[1];
    const deltaEdge1bX = edges[0].local.coordinates.b[0] - initialEdge1B[0];
    const deltaEdge1bY = edges[0].local.coordinates.b[1] - initialEdge1B[1];
    const deltaEdge2aX = edges[1].local.coordinates.a[0] - initialEdge2A[0];
    const deltaEdge2aY = edges[1].local.coordinates.a[1] - initialEdge2A[1];
    const deltaEdge2bX = edges[1].local.coordinates.b[0] - initialEdge2B[0];
    const deltaEdge2bY = edges[1].local.coordinates.b[1] - initialEdge2B[1];

    expect(deltaEdge1aX).toBe(deltaX);
    expect(deltaEdge1aY).toBe(deltaY);
    expect(deltaEdge1bX).toBe(0);
    expect(deltaEdge1bY).toBe(0);
    expect(deltaEdge2aX).toBe(0);
    expect(deltaEdge2aY).toBe(0);
    expect(deltaEdge2bX).toBe(deltaX);
    expect(deltaEdge2bY).toBe(deltaY);
  });

  it('should draw edge between port and nf', () => {
    component.active = true;
    component.editMode = true;

    const nfs = new Map();
    const nf = createNF(component, 'NF1', 100, 100, mockSites[1].config.identifier, ['port1']);
    nfs.set('NF1', nf);
    component['drawSiteGroups']();
    component.nfs = nfs;

    const portMap = new Map<string, PipelineNode>();
    portMap.set('left', createPort(component, 'left', 0, 0, mockSites[1].config.identifier));
    component['drawSiteGroups']();
    component.ports = portMap;

    const worldPort = el.querySelectorAll('.port')[0] as SVGGElement;
    expect(worldPort).toBeDefined();

    // by default, port label is hidden
    let nfPortLabel = el.querySelector('.inf-label');
    expect(nfPortLabel.classList).toContain('hidden');

    // click on world port interface
    const siteNode = component['_sitesNodesMap'].get(mockSites[1].config.identifier);
    const event = mouseClickEvent();
    component['onNfInterfaceClick'](event, siteNode.svgGroup.worldPort, mockSites[1].config.identifier, 1);

    // port label is now visible
    nfPortLabel = el.querySelector('.inf-label');
    expect(nfPortLabel.classList).not.toContain('hidden');

    // click on nf interface to draw edge
    const infPorts = el.querySelectorAll('.nf-port');
    infPorts[0].dispatchEvent(new MouseEvent('click'));
    component['onNfInterfaceClick'](event, nf.nf.local.interfaces[0].local.ctx, mockSites[1].config.identifier, 0);
    expect(el.querySelector('polyline.edge')).toBeDefined();
  });

  it('should not draw edge between port and nf of different sites', fakeAsync(() => {
    component.active = true;
    component.editMode = true;

    const alertSpy = spyOn<any>(component['_alert'], 'error');

    component.active = true;
    component.editMode = true;
    createSite(fixture, component, mockSites[0].config);

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 300, 100, mockSites[0].config.identifier, ['port1']);
    nfs.set('NF1', nf1);
    component.nfs = nfs;
    tick(1000);
    fixture.detectChanges();

    component.createNewSite(mockSites[1].config);
    tick(1000);
    fixture.detectChanges();

    const portMap = new Map<string, PipelineNode>();
    portMap.set('left', createPort(component, 'left', 0, 0, mockSites[1].config.identifier));
    component.ports = portMap;
    tick(1000);
    fixture.detectChanges();

    // click on world port interface
    const siteNode = component['_sitesNodesMap'].get(mockSites[1].config.identifier);
    const event = mouseClickEvent();
    component['onNfInterfaceClick'](event, siteNode.svgGroup.worldPort, mockSites[1].config.identifier, 1);

    // click on nf interface to draw edge
    const infPorts = el.querySelectorAll('.nf-port');
    infPorts[0].dispatchEvent(new MouseEvent('click'));
    component['onNfInterfaceClick'](event, nf1.nf.local.interfaces[0].local.ctx, mockSites[0].config.identifier);

    // should show an error
    expect(alertSpy).toHaveBeenCalledWith(AlertType.ERROR_SITE_PORT_CONNECTION);
  }));

  it('should show/hide packet capture button', () => {
    setupNfConfig(component, false);
    component.toggleView('instance');
    fixture.detectChanges();

    const nfInfPorts = el.querySelectorAll('.nf .nf-port');

    // by default, capture button is hidden
    expect((nfInfPorts[0].querySelector('.gear') as any).style.display).toBe('none');
    nfInfPorts[0].dispatchEvent(new MouseEvent('mouseenter'));

    // capture button should now be visible
    expect((nfInfPorts[0].querySelector('.gear') as any).style.display).not.toBe('none');
    nfInfPorts[0].dispatchEvent(new MouseEvent('mouseleave'));

    // capture button hidden again
    expect((nfInfPorts[0].querySelector('.gear') as any).style.display).toBe('none');
  });

  it('should properly truncate long labels', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`);
    const response = cloneDeep(mockPipelineGraph);

    const leftLongName = 'LeftNefeliNetworksTunnel';
    const rightLongName = 'RightNefeliNetworksTunnel';

    response.nodes[leftLongName] = response.nodes.left;
    response.nodes[rightLongName] = response.nodes.right;
    response.nodes[leftLongName].identifier = leftLongName;
    response.nodes[rightLongName].identifier = rightLongName;
    response.nodes[leftLongName].name = leftLongName;
    response.nodes[rightLongName].name = rightLongName;
    delete response.nodes.left;
    delete response.nodes.right;
    response.edges[0].a.node = leftLongName;
    response.edges[1].b.node = rightLongName;

    req.flush({ config: response });

    const manifestReq = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    );
    manifestReq.flush(mockPipelineManifest);

    fixture.detectChanges();

    // TODO(eric): update to test on NF (not port)
  });

  it('should stop polling a deleted service chain and go to Overview', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;

    spyOn(component['_router'], 'navigate').and.callFake((url: string[]) => {
      expect(url[0]).toBe('/tenants');
      expect(url[1]).toBe('1');

      return Promise.resolve(true);
    });

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush(mockPipelineGraph);
    httpTestingController
      .expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`)
      .flush({}, {
        status: HttpError.NotFound,
        statusText: 'Not Found'
      });

    expect(component['_router'].navigate).toHaveBeenCalled();
  });

  it('should request draft service in the edit mode', () => {
    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component.editMode = true;

    const isDraftRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`
    );
    isDraftRequest.flush(mockServiceResponse);

    fixture.detectChanges();

    const serviceRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/1`
    );
    serviceRequest.flush(mockServiceResponse);

    fixture.detectChanges();
  });

  xit('should properly space interface ports', () => {
    const nf = createNF(component, 'NF1', 100, 100, mockSites[0].config.identifier);

    enum TextAnchor {
      Start = 'start',
      Middle = 'middle',
      End = 'end'
    }

    let settings: {
      dx?: number;
      dy?: number;
      anchor?: TextAnchor;
    } = nf.local.svgNF['getPortTranslateSettings'](0, 1);
    expect(settings.dx).toBe(115);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Middle);

    settings = nf.local.svgNF['getPortTranslateSettings'](0, 2);
    expect(settings.dx).toBe(95);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.End);

    settings = nf.local.svgNF['getPortTranslateSettings'](1, 2);
    expect(settings.dx).toBe(135);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Start);

    settings = nf.local.svgNF['getPortTranslateSettings'](0, 3);
    expect(settings.dx).toBe(20);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.End);

    settings = nf.local.svgNF['getPortTranslateSettings'](1, 3);
    expect(settings.dx).toBe(115);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Middle);

    settings = nf.local.svgNF['getPortTranslateSettings'](2, 3);
    expect(settings.dx).toBe(210);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Start);

    settings = nf.local.svgNF['getPortTranslateSettings'](0, 4);
    expect(settings.dx).toBe(20);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.End);

    settings = nf.local.svgNF['getPortTranslateSettings'](1, 4);
    expect(settings.dx).toBe(115);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Middle);

    settings = nf.local.svgNF['getPortTranslateSettings'](2, 4);
    expect(settings.dx).toBe(210);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Start);

    settings = nf.local.svgNF['getPortTranslateSettings'](3, 4);
    expect(settings.dx).toBe(115);
    expect(settings.dy).toBe(-91);
    expect(settings.anchor).toBe(TextAnchor.Middle);

    settings = nf.local.svgNF['getPortTranslateSettings'](0, 5);
    expect(settings.dx).toBe(20);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.End);

    settings = nf.local.svgNF['getPortTranslateSettings'](1, 5);
    expect(settings.dx).toBe(115);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Middle);

    settings = nf.local.svgNF['getPortTranslateSettings'](2, 5);
    expect(settings.dx).toBe(210);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.Start);

    settings = nf.local.svgNF['getPortTranslateSettings'](3, 5);
    expect(settings.dx).toBe(95);
    expect(settings.dy).toBe(-131);
    expect(settings.anchor).toBe(TextAnchor.End);

    settings = nf.local.svgNF['getPortTranslateSettings'](4, 5);
    expect(settings.dx).toBe(135);
    expect(settings.dy).toBe(-131);
    expect(settings.anchor).toBe(TextAnchor.Start);

    // test the default case when >5 ports
    settings = nf.local.svgNF['getPortTranslateSettings'](0, 6);
    expect(settings.dx).toBe(20);
    expect(settings.dy).toBeUndefined();
    expect(settings.anchor).toBe(TextAnchor.End);
  });

  it('should create new cluster site', () => {
    component.active = true;
    const site = sites.sites[0];
    component.createNewSite(site.config);

    // verify site rect
    expect(el.querySelectorAll('.site').length).toBe(1);
    const siteElement = el.querySelector('.site');
    expect(siteElement.querySelector(':scope > .title > text').textContent).toBe('Eric');

    // verify instructions
    const arrowImage = siteElement.querySelector(':scope > g > image');
    expect(arrowImage.getAttribute('class')).toBe('drag-drop-arrow');
    const instructions = siteElement.querySelector(':scope > g:not(.title) > text');
    expect(instructions.textContent).toBe('Drag Network Functions&Drop HereDirect traffic by connecting edges between networkfunction interfaces and to the WORLD');

    // verify world port
    expect(siteElement.querySelector(':scope > .port > text').textContent).toBe('WORLD');

    // verify adding another site
    const newSite = sites.sites[2];
    component.createNewSite(newSite.config);
    expect(el.querySelectorAll('.site').length).toBe(2);
    expect(el.querySelectorAll('.site')[1].querySelector(':scope > .title > text').textContent).toBe('Anton');
  });

  it('should update edge encap settings to pre-existing tunnel', () => {
    spyOn(component['_sidebarService'], 'open').and.callFake(
      (title: string, content: ComponentType<unknown>, data: TrafficPortFormData) => {
        expect(title).toBe('Encapsulation Settings');
        expect(content.name).toBe('TrafficPortFormComponent');
        expect(data.site).toEqual({ identifier: 'test-site-port-right', name: 'Right Site', siteType: 'CLUSTER' });
        expect(data.tenant).toEqual({ identifier: '1', name: 'Test Tenant' });
      }
    );

    component.pipeline = mockPipelines;
    component.tenant = mockTenant;
    component.active = true;
    component.editMode = true;

    const serviceResponse = cloneDeep(mockServiceResponse);
    serviceResponse.config.nodes['left'].port = {};
    serviceResponse.config.nodes['right'].port = {};

    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}?status=true`).flush({}, {
      status: 404,
      statusText: 'Not found'
    });
    httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/1/services/${mockPipelines[0].identifier}`
    ).flush(mockServiceResponse);
    httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockPipelines[0].identifier}/nodes/${Object.keys(mockPipelineGraph.nodes)[0]}/manifest`
    ).flush(mockPipelineManifest);
    httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/12345/tunnels?status=true`
    ).flush(tunnels);

    fixture.detectChanges();

    const encapElem: SVGGElement = el.querySelector('.edge-container > .port');

    // verify encap is not configured
    expect(el.querySelector('.edge-container > .port > circle').getAttribute('class')).toBeNull();
    expect(el.querySelectorAll('.svg-tooltip > text')[1].textContent).toBe('Encapsulation not configured');

    // assign encap to local tunnel
    encapElem.dispatchEvent(new Event('click'));
    expect(component['_sidebarService'].open).toHaveBeenCalled();
    component['_sidebarService'].close(tunnels.tunnels[1].config);
    fixture.detectChanges();
    expect(el.querySelector('.edge-container > .port > circle').getAttribute('class')).toBe('green');
    expect(el.querySelectorAll('.svg-tooltip > text')[1].textContent).toBe('ENCAPSULATIONLocal Peer: None');

    // assign encap to raw tunnel
    encapElem.dispatchEvent(new Event('click'));
    expect(component['_sidebarService'].open).toHaveBeenCalled();
    component['_sidebarService'].close(tunnels.tunnels[0].config);
    fixture.detectChanges();
    expect(el.querySelector('.edge-container > .port > circle').getAttribute('class').trim()).toBe('purple');
    expect(el.querySelectorAll('.svg-tooltip > text')[1].textContent).toBe('ENCAPSULATIONPort: 1');

    // assign encap to vxlan tunnel
    encapElem.dispatchEvent(new Event('click'));
    expect(component['_sidebarService'].open).toHaveBeenCalled();
    component['_sidebarService'].close(tunnels.tunnels[2].config);
    fixture.detectChanges();
    expect(el.querySelector('.edge-container > .port > circle').getAttribute('class').trim()).toBe('blue');
    expect(el.querySelectorAll('.svg-tooltip > text')[1].textContent).toBe('ENCAPSULATIONVXLANVNI: 1IP: 1.1.1.1MAC: 00-00-00-00-00-00PORT: 1024');

    // assign encap to vlan tunnel
    encapElem.dispatchEvent(new Event('click'));
    expect(component['_sidebarService'].open).toHaveBeenCalled();
    component['_sidebarService'].close(tunnels.tunnels[4].config);
    fixture.detectChanges();
    expect(el.querySelector('.edge-container > .port > circle').getAttribute('class').trim()).toBe('orange');
    expect(el.querySelectorAll('.svg-tooltip > text')[1].textContent).toBe('ENCAPSULATIONVLANIVID: 1OVID: 2035');
  });

  it('should change site of nf on drag the nf from one site to another', fakeAsync(() => {
    component.active = true;
    component.editMode = true;
    el.style.width = '900px';
    createSite(fixture, component, mockSites[0].config, mockSites[1].config);

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 300, 100, mockSites[0].config.identifier, []);
    nfs.set('NF1', nf1);

    component.nfs = nfs;
    fixture.detectChanges();
    expect(nf1.site).toBe(mockSites[1].config.identifier);

    const siteEls = el.querySelectorAll('.site > .title > text');
    expect(siteEls.length).toBe(2);

    dragNf(fixture, component, nf1, 700, 0);
    expect(nf1.site).toBe(mockSites[0].config.identifier);
  }));

  it('should show confirmation dialog during the site changing of nf', fakeAsync(() => {
    component.active = true;
    component.editMode = true;
    el.style.width = '900px';
    createSite(fixture, component, mockSites[0].config, mockSites[1].config);

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 300, 100, mockSites[1].config.identifier, ['port']);
    nfs.set('NF1', nf1);
    component.nfs = nfs;
    fixture.detectChanges();

    const portMap = new Map<string, PipelineNode>();
    portMap.set('world', createPort(component, 'world', 0, 0, mockSites[1].config.identifier));
    component.ports = portMap;
    tick(1000);
    fixture.detectChanges();

    const siteNode = component['_sitesNodesMap'].get(mockSites[1].config.identifier);
    connectWorldPortToNF(siteNode.svgGroup, nf1, component);
    expect(component.edges.length).toBe(1);

    dragNf(fixture, component, nf1, 700, 0);

    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    expect(dialog).toBeDefined();
    dialog.afterOpened().subscribe(() => {
      const confirmDialog: ConfirmationDialogComponent = dialog.componentInstance;
      expect(confirmDialog.action).toBe('Proceed');
      const confirmDialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
      const descriptionEls: NodeListOf<HTMLParagraphElement> = confirmDialogEl.querySelectorAll('p');
      expect(descriptionEls[0].textContent).toBe(`This NF will be moved to the new site. All edges to the site's port will be removed.`);

      const confirmButton: HTMLButtonElement = confirmDialogEl.querySelectorAll('button')[1];
      confirmButton.click();
    });
    dialog.afterClosed().subscribe(() => {
      tick(1000);
      fixture.detectChanges();
      // should change site of nf
      expect(nf1.site).toBe(mockSites[0].config.identifier);
      // should remove site edge
      expect(component.edges.length).toBe(0);
    });
  }));

  it('should not change site of nf when user cancel the operation', fakeAsync(() => {
    component.active = true;
    component.editMode = true;
    el.style.width = '900px';
    createSite(fixture, component, mockSites[0].config, mockSites[1].config);

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 300, 100, mockSites[1].config.identifier, ['port']);
    nfs.set('NF1', nf1);
    component.nfs = nfs;
    fixture.detectChanges();

    const nf1Pos: Coordinates = nf1.local.coordinates;

    const portMap = new Map<string, PipelineNode>();
    portMap.set('world', createPort(component, 'world', 0, 0, mockSites[1].config.identifier));
    component.ports = portMap;
    tick(1000);
    fixture.detectChanges();

    const siteNode = component['_sitesNodesMap'].get(mockSites[1].config.identifier);
    connectWorldPortToNF(siteNode.svgGroup, nf1, component);
    expect(component.edges.length).toBe(1);

    dragNf(fixture, component, nf1, 700, 0);

    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    expect(dialog).toBeDefined();
    dialog.afterOpened().subscribe(() => {
      const confirmDialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
      const cancelButton: HTMLButtonElement = confirmDialogEl.querySelectorAll('button')[0];
      cancelButton.click();
    });
    dialog.afterClosed().subscribe(() => {
      tick(1000);
      fixture.detectChanges();
      // should not change site of nf
      expect(nf1.site).toBe(mockSites[1].config.identifier);
      // should not remove site edge
      expect(component.edges.length).toBe(1);
      // should restore the previous position
      expect(nf1.local.coordinates).toEqual(nf1Pos);
    });
  }));

  it('should calculate site box', fakeAsync(() => {
    let siteBox: RBox = component.getSiteBox('');
    expect(siteBox).not.toBeDefined();
    siteBox = component.getSiteBox(mockSites[0].config.identifier);
    expect(siteBox).not.toBeDefined();

    component.active = true;
    component.editMode = true;
    component['drawSiteGroups']();
    fixture.detectChanges();
    component.createNewSite(mockSites[0].config);
    tick(1000);
    fixture.detectChanges();
    siteBox = component.getSiteBox(mockSites[0].config.identifier);
    expect(siteBox).toBeDefined();
  }));

  it('should get site name by site id', () => {
    const site: string = component.getSiteName(mockSites[0].config.identifier);
    expect(site).toBe(mockSites[0].config.name);
  });

  it('should emit on drop event', () => {
    component.active = true;
    component.editMode = true;
    let point: Point;
    component.handleDrop.subscribe(p => point = p);
    fixture.detectChanges();
    expect(point).not.toBeDefined();
    component.onDrop(new DragEvent('drop', {
      clientX: 100,
      clientY: 100
    }))
    fixture.detectChanges();
    expect(point).toBeDefined();
  });

  it('should recalculate node coordinates on zooming', fakeAsync(() => {
    component.active = true;
    component.editMode = true;
    fixture.detectChanges();
    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 200, 100, mockSites[0].config.identifier, []);
    nfs.set('NF1', nf1);

    component['drawSiteGroups']();
    component.nfs = nfs;
    tick(1000);
    fixture.detectChanges();

    expect(nf1.local.coordinates.x).toBe(75);
    expect(nf1.local.coordinates.y).toBe(62.5);

    const viewElem = fixture.debugElement.nativeElement.querySelector('.svg-viewport');
    expect(viewElem).toBeDefined();
    viewElem.dispatchEvent(new WheelEvent('wheel', {
      clientX: 10,
      clientY: 10,
      deltaY: 200,
    }));
    tick(1000);
    fixture.detectChanges();

    expect(nf1.local.coordinates.x).not.toBe(75);
    expect(nf1.local.coordinates.y).not.toBe(62.5);
  }));

  it('should delete site', fakeAsync(() => {
    let deletedSite;
    component.onDeleteSite.subscribe((site) => {
      deletedSite = site;
    });

    component.active = true;
    component.editMode = true;
    createSite(fixture, component, mockSites[0].config);

    const removeIconDe: DebugElement = fixture.debugElement.query(By.css('.remove-icon'));
    removeIconDe.nativeElement.dispatchEvent(new Event('click'));
    removeIconDe.triggerEventHandler('click', {});
    tick(1000);
    fixture.detectChanges();

    expect(deletedSite).toEqual(mockSites[0].config);
    expect(el.querySelectorAll('.site').length).toBe(0);
    expect(component.isEmpty()).toBeTruthy();
  }));

  it('should delete first site', fakeAsync(() => {
    let deletedSite;
    component.onDeleteSite.subscribe((site) => {
      deletedSite = site;
    });

    component.active = true;
    component.editMode = true;
    createSite(fixture, component, mockSites[0].config, mockSites[1].config);

    const removeIconDe: DebugElement = fixture.debugElement.queryAll(By.css('.remove-icon'))[1];
    removeIconDe.nativeElement.dispatchEvent(new Event('click'));
    removeIconDe.triggerEventHandler('click', {});
    tick(1000);
    fixture.detectChanges();

    expect(deletedSite).toEqual(mockSites[1].config);
    expect(el.querySelectorAll('.site').length).toBe(1);
    expect(component.isEmpty()).toBeFalsy();
  }));

  it('should not delete site if it is not empty', fakeAsync(() => {
    let deletedSite;
    component.onDeleteSite.subscribe((site) => {
      deletedSite = site;
    });
    const alertSpy = spyOn<any>(component['_alert'], 'error');

    component.active = true;
    component.editMode = true;
    createSite(fixture, component, mockSites[0].config);

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 300, 100, mockSites[0].config.identifier, []);
    nfs.set('NF1', nf1);
    component.nfs = nfs;
    fixture.detectChanges();

    const removeIconDe: DebugElement = fixture.debugElement.query(By.css('.remove-icon'));
    removeIconDe.nativeElement.dispatchEvent(new Event('click'));
    removeIconDe.triggerEventHandler('click', {});
    tick(1000);
    fixture.detectChanges();

    expect(deletedSite).toBeUndefined();
    expect(el.querySelectorAll('.site').length).toBe(1);
    expect(alertSpy).toHaveBeenCalledWith(AlertType.ERROR_DELETE_NOT_EMPTY_SITE_GROUP);
  }));

  it('should not delete site if mode is not edit', fakeAsync(() => {
    let deletedSite;
    component.onDeleteSite.subscribe((site) => {
      deletedSite = site;
    });
    const alertSpy = spyOn<any>(component['_alert'], 'error');

    component.active = true;
    component.editMode = true;
    createSite(fixture, component, mockSites[0].config);

    component.editMode = false;
    const removeIconDe: DebugElement = fixture.debugElement.query(By.css('.remove-icon'));
    removeIconDe.nativeElement.dispatchEvent(new Event('click'));
    removeIconDe.triggerEventHandler('click', {});
    tick(1000);
    fixture.detectChanges();

    expect(deletedSite).toBeUndefined();
    expect(el.querySelectorAll('.site').length).toBe(1);
    !expect(alertSpy).not.toHaveBeenCalled();
  }));

  it('should clean component', fakeAsync(() => {
    component.active = true;
    component.editMode = true;
    createSite(fixture, component, mockSites[0].config);

    const nfs = new Map();
    const nf1 = createNF(component, 'NF1', 300, 100, mockSites[0].config.identifier, []);
    nfs.set('NF1', nf1);
    component.nfs = nfs;
    tick(1000);
    fixture.detectChanges();
    expect(component.isEmpty()).toBeFalsy();
    component.clear();
    expect(component.isEmpty()).toBeTruthy();
    expect(component['_sitesNodesMap'].size).toEqual(0);
    expect(component['_nfs'].size).toEqual(0);
    expect(component['_ports'].size).toEqual(0);
    expect(component['_edges'].length).toEqual(0);
  }));

  it('should get added site groups', () => {
    component.active = true;
    component.editMode = true;
    component['drawSiteGroups']();
    fixture.detectChanges();

    expect(component.getSiteGroups()).toEqual([]);

    component.createNewSite(mockSites[0].config);
    fixture.detectChanges();

    expect(component.getSiteGroups()).toEqual([mockSites[0].config?.identifier]);

    component.createNewSite(mockSites[1].config);
    fixture.detectChanges();

    expect(component.getSiteGroups()).toEqual([mockSites[0].config?.identifier, mockSites[1].config?.identifier]);

    component.clear();
    fixture.detectChanges();

    expect(component.getSiteGroups()).toEqual([]);
  });

  it('should render template on cluster site', () => {
    component.sites = sites.sites;
    component.active = true;
    component.template = cloneDeep(template.config);

    const nfId = template.config.nodes['panA'].nf.catalogId;
    httpTestingController.expectOne(`${environment.restServer}/v1/nfs/${nfId}`).flush(manifest);

    // verify site rect
    expect(el.querySelector('svg .site > .title').textContent).toBe('Eric');

    // verify NF
    expect(el.querySelectorAll('svg .site > .nf').length).toBe(1);

    // verify edges
    expect(el.querySelectorAll('svg .site > .edge-container').length).toBe(2);

    // verify world port
    expect(el.querySelectorAll('svg .site > .port').length).toBe(1);
    expect(el.querySelector('svg .site > .port > .title').textContent).toBe('WORLD');
  });

  it('should render template on ucpe site', () => {
    const templateConfig: ServiceConfigTemplate = cloneDeep(template.config);
    const siteId = sites.sites[1].config.identifier;
    templateConfig.nodes['panA'].site = `${siteId}`;
    templateConfig.nodes['portX'].site = `${siteId}`;
    templateConfig.nodes['portX'].port.tunnel = `{{createtunnel {\"name\":\"WAN\",\"remoteTunnel\":{\"physicalPort\":\"{{findPortWithPurpose \"WAN\"}}\"}}}}`;
    templateConfig.nodes['portY'].site = `${siteId}`;
    templateConfig.nodes['portY'].port.tunnel = `{{createtunnel {\"name\":\"LAN\",\"remoteTunnel\":{\"physicalPort\":\"{{findPortWithPurpose \"LAN\"}}\"}}}}`;

    component.sites = sites.sites;
    component.active = true;
    component.template = templateConfig;

    const nfId = template.config.nodes['panA'].nf.catalogId;
    httpTestingController.expectOne(`${environment.restServer}/v1/nfs/${nfId}`).flush(manifest);
    httpTestingController.expectOne(`${environment.restServer}/v1/sites/${siteId}/servers`).flush(servers);
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles/${sites.sites[1].config.hardwareProfileId}`).flush(profiles.hardwareProfiles[0]);

    // verify site rect
    expect(el.querySelector('svg .site > .title').textContent).toBe('Andrew');

    // verify NF
    expect(el.querySelectorAll('svg .site > .nf').length).toBe(1);

    // verify edges
    expect(el.querySelectorAll('svg .site > .edge-container').length).toBe(2);

    // verify ports
    const wan = el.querySelectorAll('svg .site .port[data-purpose="WAN"]');
    expect(wan.length).toBe(1);
    expect(wan[0].textContent).toBe('WAN');
    const lan = el.querySelectorAll('svg .site .port[data-purpose="LAN"]');
    expect(lan.length).toBe(1);
    expect(lan[0].textContent).toBe('LAN');
  });

  it('should render template on ucpe site with no server', () => {
    const templateConfig: ServiceConfigTemplate = cloneDeep(template.config);
    const siteId = sites.sites[1].config.identifier;
    templateConfig.nodes['panA'].site = `${siteId}`;
    templateConfig.nodes['portX'].site = `${siteId}`;
    templateConfig.nodes['portX'].port.tunnel = `{{createtunnel {\"name\":\"WAN\",\"remoteTunnel\":{\"physicalPort\":\"{{findPortWithPurpose \"WAN\"}}\"}}}}`;
    templateConfig.nodes['portY'].site = `${siteId}`;
    templateConfig.nodes['portY'].port.tunnel = `{{createtunnel {\"name\":\"LAN\",\"remoteTunnel\":{\"physicalPort\":\"{{findPortWithPurpose \"LAN\"}}\"}}}}`;

    component.sites = sites.sites;
    component.active = true;
    component.template = templateConfig;

    const nfId = template.config.nodes['panA'].nf.catalogId;
    httpTestingController.expectOne(`${environment.restServer}/v1/nfs/${nfId}`).flush(manifest);
    httpTestingController.expectOne(`${environment.restServer}/v1/sites/${siteId}/servers`).flush({}, {
      status: 400,
      statusText: 'Bad Request'
    });
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles/${sites.sites[1].config.hardwareProfileId}`).flush(profiles.hardwareProfiles[0]);

    // verify site rect
    expect(el.querySelector('svg .site > .title').textContent).toBe('Andrew');

    // verify NF
    expect(el.querySelectorAll('svg .site > .nf').length).toBe(1);

    // verify edges
    expect(el.querySelectorAll('svg .site > .edge-container').length).toBe(2);

    // verify ports
    const wan = el.querySelectorAll('svg .site .port[data-purpose="WAN"]');
    expect(wan.length).toBe(1);
    expect(wan[0].textContent).toBe('WAN');
    const lan = el.querySelectorAll('svg .site .port[data-purpose="LAN"]');
    expect(lan.length).toBe(1);
    expect(lan[0].textContent).toBe('LAN');
  });
});
