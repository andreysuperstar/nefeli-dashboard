import { HttpError } from './../error-codes';
import { HttpErrorResponse } from '@angular/common/http';
import { cloneDeep, isEqual, orderBy } from 'lodash-es';
import {
  Component,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { CdkOverlayOrigin, ConnectedPosition } from '@angular/cdk/overlay';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { Subscription, Observable, Subject, BehaviorSubject, of, forkJoin } from 'rxjs';

import { VMInterfaceType } from 'rest_client/pangolin/model/vMInterfaceType';
import { LegendItem } from '../shared/legend/legend.component';
import { GetNFColor, randomID, NFInterface } from './network-function.model';
import {
  Pipeline,
  PipelineGraph,
  PipelineNode,
  PipelineNF,
  PipelineEdge,
  PipelineNfInstance,
  PipelineNodeStatus,
  PipelineNfStats,
  NfType,
  createPipelineNode,
  Coordinates,
  PathCoordinates,
  TemplateNode
} from './pipeline.model';

import { AlertService, AlertType } from '../shared/alert.service';
import { LoggerService } from '../shared/logger.service';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';
import { ConverterService } from '../shared/converter.service';
import { Tenant } from '../tenant/tenant.service';
import { EdgeDirection, PipelineService, Tunnel, VALID_SERVICE_NAME_FORMAT } from './pipeline.service';

import { Mode } from '../service-designer/service-designer/service-designer.component';

import { InputDialogComponent } from '../shared/input-dialog/input-dialog.component';
import { PacketCaptureDialogComponent, PacketDialogData } from './packet-capture-dialog/packet-capture-dialog.component';
import { ClusterService } from '../home/cluster.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { LaunchStatus } from 'rest_client/pangolin/model/launchStatus';
import { PlacementStatus } from 'rest_client/pangolin/model/placementStatus';
import { InstanceStatusState } from 'rest_client/pangolin/model/instanceStatusState';
import { InstanceStatus } from 'rest_client/pangolin/model/instanceStatus';
import { ServiceStatus } from 'rest_client/pangolin/model/serviceStatus';
import { RESTGetTraceResponse } from 'rest_client/pangolin/model/rESTGetTraceResponse';
import { RESTDashboardGraphTrace } from 'rest_client/pangolin/model/rESTDashboardGraphTrace';
import { ServiceNodeStatus } from 'rest_client/pangolin/model/serviceNodeStatus';
import { MatStepper } from '@angular/material/stepper';
import { catchError, concatMap, map, mergeMap, take } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { SiteGroupMode, SvgSiteGroup } from './svg-elements/svg-site-group';
import { NFPortTranslateSettings, NFRectSettings, SvgNetworkFunction } from './svg-elements/svg-network-function';
import * as dagre from 'dagre';
import * as SVG from 'svg.js';
import 'svg.draggy.js';
import 'svg.draw.js';
import 'svg.pan-zoom.js';
import { SidebarService } from '../shared/sidebar.service';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { TrafficPortFormComponent } from '../service-designer/traffic-port-form/traffic-port-form.component';
import { TrafficPortFormData } from '../service-designer/traffic-port-form/traffic-port-form.component';
import { NFType } from 'rest_client/pangolin/model/nFType';
import { SvgTrafficPort, SvgTrafficPortEncapType, SvgTrafficPortMode } from './svg-elements/svg-traffic-port';
import { Direction, SvgEdge } from './svg-elements/svg-edge';
import { NFFormComponent, NFFormData } from '../service-designer/nf-form/nf-form.component';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { RBox, Transform } from 'svg.js';
import { HardwareProfileExpDataplaneBondWithID } from 'rest_client/pangolin/model/hardwareProfileExpDataplaneBondWithID';
import { HardwareService } from 'rest_client/pangolin/api/hardware.service';
import { ServiceConfigTemplate } from 'rest_client/pangolin/model/serviceConfigTemplate';
import { EdgeFormComponent, EdgeFormData } from '../service-designer/edge-form/edge-form.component';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { ServersService, Socket } from 'src/app/cluster/servers.service';
import { Server } from 'src/app/cluster/servers.service';
import { RESTServerNetworkInterface } from 'rest_client/pangolin/model/rESTServerNetworkInterface';
import {
  ConfirmationDialogComponent,
  ConfirmationDialogData
} from '../shared/confirmation-dialog/confirmation-dialog.component';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';

interface PerformanceTrace {
  filter: string;
  enabled: boolean;
}

interface Popup {
  edge: PopupState;
  node: PopupState;
  port: PopupState;
  instanceStatus: PopupState;
}

interface ConnectionInterface {
  svg: SVG.G;
  worldPortDirection?: WorldPortDirection;
  siteConfig: SiteConfig;
}

type PopupSelected = (PipelineNode | PipelineNfInstance) & {
  nf?: PipelineNF;
  site?: string;
  launchStatus?: LaunchStatus;
  placementStatus?: PlacementStatus;
};

interface PopupState {
  isOpen: boolean;
  selected: PopupSelected;
  connectPosition: ConnectedPosition[];
}

interface SiteNodeValue {
  nodes: PipelineNode[];
  siteConfig?: SiteConfig;
  svgGroup?: SvgSiteGroup;
  position?: {
    col: number,
    row: number
  };
}

interface UcpePortData {
  purpose?: string;
  identifier?: string;
}

enum WorldPortDirection {
  NONE,
  SRC,
  DEST
}

interface UcpeSiteInfo {
  graph: PipelineGraph;
  ucpeSites: Site[];
  servers: Server[][];
  hardwareProfile: HardwareProfile[];
}

export interface EdgeSidebarSettings {
  shouldDelete?: boolean;
  filterAb?: string;
  filterBa?: string;
  direction?: EdgeDirection;
}

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

export const Config = {
  POLL_INTERVAL: 10000,
  STATUS_POLL_INTERVAL: 2000,
  MAX_ZOOM_RATIO: 1.3,
  COLUMN_COUNT: 2,
  Edge: {
    markerWidth: 0.4,
    popupConnectPosition: [
      {
        originX: 'center',
        originY: 'center',
        overlayX: 'center',
        overlayY: 'top',
        offsetY: 20
      }
    ] as ConnectedPosition[]
  },
  NF: {
    width: 250,
    smHeight: 75,
    lgHeight: 115,
    maxTitleSize: 220,
    serverDefaultColor: 'dark-grey',
    popupConnectPosition: [
      {
        originX: 'center',
        originY: 'center',
        overlayX: 'center',
        overlayY: 'top',
        offsetY: 40
      }
    ] as ConnectedPosition[],
    status: {
      popupConnectPosition: [
        {
          originX: 'center',
          originY: 'center',
          overlayX: 'center',
          overlayY: 'top'
        }
      ] as ConnectedPosition[],
    }
  },
  Interface: {
    cx: 10,
    labelToEdgeGap: 5,
    arrowYPosition: -4,
  },
  Port: {
    maxLabelSize: 80
  },
  PacketDialog: {
    width: '370px'
  },
  SiteGroup: {
    height: 400,
    widthFactor: 0.48,
    padding: 10,
    maxColumns: 2,
    animateTime: 300
  }
};

export const forbiddenNameValidator = (): ValidatorFn => {
  return (control: AbstractControl): { [key: string]: any } => {
    const forbidden = control.value && !Boolean(control.value.match(VALID_SERVICE_NAME_FORMAT));

    return forbidden
      ? {
        forbiddenName: {
          value: control.value
        }
      }
      : undefined;
  };
};

@Component({
  selector: 'nef-pipeline',
  templateUrl: './pipeline.component.html',
  styleUrls: ['./pipeline.component.less']
})
export class PipelineComponent implements OnChanges, OnDestroy, OnInit {
  @ViewChild('stepper') private _instanceStateStepper: MatStepper;

  @Input() public set active(val: boolean) {
    this._active = val;

    if (this._init) {
      this.updateGraph();
    }
  }

  @Input() public set tenant(val: Tenant) {
    this._tenant = val;
  }

  @Input() public set pipeline(val: Array<Pipeline>) {
    if (val.length) {
      this._pipelineId = val[0].identifier;
    }
  }

  @Input() public set template(val: ServiceConfigTemplate) {
    this._template = val;
    this.clear();

    if (!this._pipelineId) {
      this.updateGraph();
    }
  }

  @Input() public set ports(ports: Map<string, PipelineNode>) {
    this._ports = ports;
    this.drawPorts();
  }

  @Input() public set nfs(nfs: Map<string, PipelineNode>) {
    this._nfs = nfs;
    this.drawNFs();
  }

  @Input('edit') public set editMode(isEditable: boolean) {
    this._editMode = isEditable;
  }

  @Input('serviceMode') public set serviceMode(mode: Mode) {
    this._serviceMode = mode;
  }

  @Input('sites') public set sites(s: Site[]) {
    this._sites = s;
  }

  private _handleDrop = new EventEmitter();
  private _togglePerfTrace = new EventEmitter<boolean>();
  private _onDeleteSite = new EventEmitter<SiteConfig>();

  @Output() public get handleDrop(): EventEmitter<any> {
    return this._handleDrop;
  }

  @Output() public get togglePerfTrace(): EventEmitter<boolean> {
    return this._togglePerfTrace;
  }

  @Output() public get onServiceRendered(): EventEmitter<void> {
    return this._onServiceRendered;
  }

  @Output() public get onDeleteSite(): EventEmitter<SiteConfig> {
    return this._onDeleteSite;
  }

  public get ports(): Map<string, PipelineNode> {
    return this._ports;
  }

  public get nfs(): Map<string, PipelineNode> {
    return this._nfs;
  }

  public get edges(): Array<PipelineEdge> {
    return this._edges;
  }

  public get serverLegendItems(): LegendItem[] {
    return this._isLogicalView ? [] : this._serverLegendItems;
  }

  private _init: boolean;
  private _active = false;
  private _initialGraph: PipelineGraph | ServiceConfigTemplate;
  private _drawing: SVG.Doc;
  private _nfs = new Map<string, PipelineNode>();
  private _ports = new Map<string, PipelineNode>();
  private _edges = new Array<PipelineEdge>();
  private _pipelineId: string;
  private _tenant: Tenant;
  private _streamSubscription: Subscription;
  private _streamTimer: number;
  private _pipelineBpfSubscription: Subscription;
  private _perfStatStreamSubscription: Subscription;
  private _traceStreamSubscription: Subscription;
  private _statusSubscription: Subscription;
  private _statusTimer: number;
  private _view: SVG.G;
  private _servers: string[] = [];
  private _serverLegendItems: LegendItem[];
  private _editMode = false;
  private _serviceMode = Mode.DEFAULT;
  private _isDraft: boolean;
  private _isDragging = false;
  private _edgeGroups: any;
  private _activeEdge: PipelineEdge;
  private _popups: Popup = {
    edge: {
      isOpen: false
    } as PopupState,
    node: {
      isOpen: false,
      selected: undefined
    } as PopupState,
    port: {
      isOpen: false,
      selected: undefined
    } as PopupState,
    instanceStatus: {
      isOpen: false,
      selected: undefined
    } as PopupState
  };
  private _overlayOrigin: CdkOverlayOrigin;
  private _isLogicalView = true;
  private _perfTrace: PerformanceTrace = {
    filter: undefined,
    enabled: false
  };
  private _perfTraceLegend: LegendItem[] = [{
    label: 'Throughput',
    color: 'bright-blue'
  }, {
    label: 'Latency',
    color: 'orange'
  }, {
    label: 'Loss',
    color: 'acid-green'
  }];
  private _sites = new Array<Site>();
  private _subscriptions = new Subscription();
  private _tunnels: Tunnel[] = [];
  private _nfInstanceStatusTimers = new Map<string, number>();
  private _nodeForm: FormGroup;
  private _destroy$: Subject<boolean> = new Subject<boolean>();
  private _isInactiveDraft = false;
  private _graph: PipelineGraph;
  private _graphModels = new Map<string, dagre.graphlib.Graph>();
  private _isInactiveDraft$ = new BehaviorSubject<boolean>(true);
  private _isUnpublishedGraph = false;
  private _hasNfConfigChanges = false;
  private _sitesNodesMap = new Map<string, SiteNodeValue>();
  private _sourceInterface: ConnectionInterface;
  private _destInterface: ConnectionInterface;
  private _tempEdge: SVG.PolyLine;
  private _onServiceRendered = new EventEmitter<void>();
  private _template: ServiceConfigTemplate;
  private _ucpeSiteServersMap = new Map<string, {
    servers: Server[],
    hardwareProfile: HardwareProfile
  }>();

  constructor(
    private _pipelineService: PipelineService,
    private _clusterService: ClusterService,
    private _alert: AlertService,
    private _log: LoggerService,
    private _store: LocalStorageService,
    private _converter: ConverterService,
    private _dialog: MatDialog,
    private _activatedRoute: ActivatedRoute,
    private _fb: FormBuilder,
    private _router: Router,
    private _sidebarService: SidebarService,
    private _hardwareService: HardwareService,
    private _serversService: ServersService
  ) { }

  public ngOnInit() {
    this._init = true;

    this.initNodeForm();
    this.initTunnels();

    if (this._editMode) {
      this._clusterService.getSites().subscribe(({ sites: sites }: Sites) => {
        this._sites = sites;
      });
    }
  }

  private initNodeForm() {
    this._nodeForm = this._fb.group({
      name: ['',
        [
          Validators.required,
          forbiddenNameValidator()
        ]
      ],
      site: ''
    });
  }

  public get nodeForm(): FormGroup {
    return this._nodeForm;
  }

  public ngOnChanges(changes: SimpleChanges) {
    if (changes.tenant && changes.tenant.currentValue) {
      this.updateGraph();
    }
  }

  public ngOnDestroy() {
    if (this._edgeGroups && this._edgeGroups.size) {
      this._edgeGroups.off('click.edge');
    }

    if (this._streamSubscription) {
      this._streamSubscription.unsubscribe();
      this._streamSubscription = undefined;
    }

    if (this._streamTimer) {
      clearTimeout(this._streamTimer);
    }

    this._subscriptions.unsubscribe();

    this.stopPerformanceStatStream();
    this.stopPipelineStatusStream();
    this.stopTraceStream();
    this._nfInstanceStatusTimers.forEach((timerId: number) => {
      clearTimeout(timerId);
    });

    this._destroy$.next(true);
  }

  private initTunnels() {
    const tenantId = this._activatedRoute.snapshot.params.id;
    if (tenantId) {
      this._pipelineService
        .getTunnels(tenantId).pipe(
          map((tunnels: Tunnel[]): Tunnel[] => orderBy(tunnels, ['name', 'displayName', 'identifier']))
        ).subscribe((tunnels: Tunnel[]) => {
          this._tunnels = tunnels;
        });
    }
  }

  private isEmptyGraph(graph: PipelineGraph): boolean {
    const nodes = graph?.nodes ? Object.keys(graph.nodes) : [];

    return ((graph?.edges?.length === undefined || graph?.edges?.length === 0)
      && (nodes.length === 0));
  }

  private initTemplateGraph(template: ServiceConfigTemplate) {
    this._pipelineService.convertTemplateToPipeline(template).pipe(
      mergeMap((graph: PipelineGraph): Observable<UcpeSiteInfo> => this.getUcpeSiteInfo(graph))
    ).subscribe({
      next: ({ graph, ucpeSites, servers, hardwareProfile }: UcpeSiteInfo) => {
        ucpeSites?.forEach((site: Site, index: number) => {
          this._ucpeSiteServersMap.set(site.config.identifier,
            {
              servers: servers[index],
              hardwareProfile: hardwareProfile[index]
            });
        });

        this.setGraph(graph);
      },
      error: () => this._alert.error(AlertType.ERROR_RENDER_SERVICE)
    });
  }

  private initPipelineGraph(): void {
    if (this._pipelineId) {

      if (this._streamSubscription) {
        this._streamSubscription.unsubscribe();
      }

      if (this._editMode) {
        // Need to check if a draft graph was not published in Edit mode
        this._pipelineService.getTenantPipeline(this._tenant, this._pipelineId)
          .pipe(take(1))
          .subscribe((graph: PipelineGraph) => {
            this._isUnpublishedGraph = this.isEmptyGraph(graph);
          });
      }

      const obs$ = this._pipelineService.hasDraft(this._tenant, this._pipelineId)
        .pipe(
          mergeMap((isDraft: boolean) => {
            this._isDraft = isDraft;
            return this._editMode
              ? this._pipelineService.getTenantPipeline(this._tenant, this._pipelineId, isDraft)
              : this._pipelineService.streamTenantPipeline(this._tenant, this._pipelineId, Config.POLL_INTERVAL);
          }),
          concatMap((graph: PipelineGraph): Observable<UcpeSiteInfo> => this.getUcpeSiteInfo(graph))
        );

      this._streamSubscription = obs$.subscribe(
        ({ graph, ucpeSites, servers, hardwareProfile }: UcpeSiteInfo) => {
          ucpeSites?.forEach((site: Site, index: number) => {
            this._ucpeSiteServersMap.set(site.config.identifier,
              {
                servers: servers[index],
                hardwareProfile: hardwareProfile[index]
              });
          });
          // in editMode a "graph" argument will not be empty even for the inactive draft because of "isDraft" request argument.
          // _isUnpublishedGraph stores the result of checking whether the graph is empty in editMode.
          this._isInactiveDraft = this._editMode
            ? this._isUnpublishedGraph && this._isDraft
            : this.isEmptyGraph(graph) && this._isDraft;

          this._isInactiveDraft$.next(this._isInactiveDraft);
          this.setGraph(graph);
        }, (error: HttpErrorResponse) => {
          this._log.error(error);

          if (error.status === HttpError.NotFound) {
            // pipeline no longer exists, go to Tenant Overview tab
            this._router.navigate(['/tenants', this._tenant.identifier]);
          } else {
            this._streamTimer = window.setTimeout(() => {
              this.initPipelineGraph();
            }, Config.POLL_INTERVAL);
          }
        });
    }
  }

  private getUcpeSiteInfo(graph: PipelineGraph): Observable<UcpeSiteInfo> {
    const ucpeSites = Object.values(graph.nodes)?.reduce((prev: Site[], node: PipelineNode): Site[] => {
      const site = node.nf ? this.getSiteEntry(node.site) : undefined;
      return site?.config.siteType === SiteConfigSiteType.UCPE ? [...prev, site] : prev;
    }, []);

    const serverRequests: Observable<Server[]>[] =
      ucpeSites?.map((site: Site): Observable<Server[]> => this._serversService.getServers(site.config.identifier).pipe(
        catchError(() => {
          return of([]);
        })
      ));

    const hwProfileRequests: Observable<HardwareProfile>[] =
      ucpeSites?.map((site: Site): Observable<HardwareProfile> =>
        this._hardwareService.getHardwareProfile(site.config.hardwareProfileId).pipe(
          catchError(() => {
            return of({});
          })
        ));

    return forkJoin({
      graph: of(graph),
      ucpeSites: of(ucpeSites),
      servers: serverRequests?.length > 0 ? forkJoin(serverRequests) : of([]),
      hardwareProfile: hwProfileRequests?.length > 0 ? forkJoin(hwProfileRequests) : of([])
    });
  }

  public createNewSite(selectedSite: SiteConfig, hwProfile?: HardwareProfile) {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    // shift all of the existing sites
    const siteNodes: SiteNodeValue[] = [...this._sitesNodesMap.values()]
      .sort((val1, val2) => {
        const { position: pos1 } = val1;
        const { position: pos2 } = val2;
        return (pos1.row - pos2.row) || (pos1.col - pos2.col);
      }).reverse();

    let newPos: Coordinates;

    siteNodes.forEach((siteNode: SiteNodeValue) => {
      const newPosition: number = Config.COLUMN_COUNT * siteNode.position?.row ?? 0 + siteNode.position?.col ?? 0 + 1;
      const newRow: number = Math.floor(newPosition / Config.COLUMN_COUNT);
      const newCol: number = newPosition % Config.COLUMN_COUNT;
      const siteElement: SVG.G = siteNode.svgGroup.nativeElement;
      const rbox: SVG.RBox = siteElement.rbox(this._view);
      if (!newPos) {
        const isNewLine: boolean = newRow > siteNode.position.row;
        newPos = {
          x: isNewLine ? x : rbox.x + rbox.width + (2 * x),
          y: isNewLine ? rbox.y + rbox.height + (2 * y) : rbox.y
        };
      }
      siteElement.animate(Config.SiteGroup.animateTime).move(newPos.x, newPos.y);
      newPos = {
        x: rbox.x,
        y: rbox.y
      };
      siteNode.position.row = newRow;
      siteNode.position.col = newCol;
    });

    const siteGroup = new SvgSiteGroup(this._view,
      {
        x,
        y,
        width,
        height,
      },
      selectedSite,
      SiteGroupMode.NEW,
      {
        editMode: this._editMode,
        onDeleteClick: this.deleteSiteGroup.bind(this)
      }
    );

    if (selectedSite.siteType === SiteConfigSiteType.CLUSTER) {
      this.drawWorldPort(siteGroup);
    } else if (selectedSite.siteType === SiteConfigSiteType.UCPE) {
      this.drawUcpeSiteTrafficPorts(siteGroup, hwProfile, true);
    }

    this._sitesNodesMap.set(selectedSite.identifier, {
      nodes: [],
      siteConfig: selectedSite,
      svgGroup: siteGroup,
      position: { col: 0, row: 0 }
    });
  }

  private drawUcpeSiteTrafficPorts(siteGroup: SvgSiteGroup, hwProfile?: HardwareProfile, isNewSite = false) {
    if (hwProfile) {
      // add ports based off the hardware profile
      this.addUcpeSitePorts(siteGroup, hwProfile);
    } else if (isNewSite) {
      // creating a new site, default to WAN & LAN ports
      this.addUcpeSitePorts(siteGroup, undefined, [
        {
          purpose: SubnetType.LAN,
          identifier: SubnetType.LAN
        },
        {
          purpose: SubnetType.WAN,
          identifier: SubnetType.WAN
        }
      ]);
    }
    else {
      // add ports based off the server's interfaces
      const siteId = siteGroup.site.identifier;
      const { servers, hardwareProfile } = this._ucpeSiteServersMap.get(siteId) ?? {};
      const ucpePorts: UcpePortData[] = [];
      for (const { site, port } of this._ports.values()) {
        if (site === siteId) {
          const tunnel = this._tunnels.find((t: Tunnel) => t.identifier === port.tunnel);
          const physicalPort = tunnel?.remoteTunnel?.physicalPort;
          let interFace: RESTServerNetworkInterface;
          servers?.forEach((s: Server) => {
            s.sockets.forEach((socket: Socket) => {
              interFace = socket?.interfaces?.find((inf: RESTServerNetworkInterface) => inf.name === physicalPort);
            });
          });

          if (interFace) {
            ucpePorts.push({
              purpose: interFace?.purpose,
              identifier: interFace?.name
            });
          }
        }
      }

      this.addUcpeSitePorts(siteGroup, hardwareProfile, ucpePorts);
    }
  }

  private addUcpeSitePorts(siteGroup: SvgSiteGroup, siteHWProfile?: HardwareProfile, ports?: UcpePortData[]) {
    const svgSiteGroup = siteGroup.nativeElement;
    const padding = 10;
    const portsGroup: SVG.G = svgSiteGroup.group();

    siteGroup.trafficPortsGroup = portsGroup;

    if (ports?.length > 0) {
      ports.forEach(({ purpose, identifier }: UcpePortData, index: number) => {
        portsGroup.add(this.renderUcpeTrafficPort(portsGroup, siteGroup, purpose, identifier, index, padding));
      });
    } else if (siteHWProfile) {
      siteHWProfile.bonds.forEach(({ bond, identifier }: HardwareProfileExpDataplaneBondWithID, index: number) => {
        portsGroup.add(this.renderUcpeTrafficPort(portsGroup, siteGroup, bond.purpose, identifier, index, padding));
      });
    }

    // place on bottom center of site rect
    const [siteWidth, siteHeight, portWidth, portHeight] = [
      svgSiteGroup.rbox().w,
      svgSiteGroup.rbox().h,
      portsGroup.rbox().width,
      portsGroup.rbox().height
    ];

    svgSiteGroup.add(portsGroup);
    portsGroup.translate(
      (siteWidth / 2) - (portWidth / 2),
      siteHeight - portHeight - padding
    );

    this.updatePortsPositions(siteGroup);
  }

  private updatePortsPositions(siteGroup: SvgSiteGroup) {
    this._ports.forEach((port: PipelineNode) => {

      const tpElement = siteGroup.trafficPorts.find((tp: SVG.G) =>
        tp?.data('purpose')?.toLowerCase() === port.identifier.toLowerCase());

      if (tpElement) {
        const { cx, cy } = tpElement.rbox(siteGroup.nativeElement);
        port.local.ctx?.center(cx, cy);
      }
    });
  }

  private deleteSiteGroup(site: string) {
    if (!this._editMode) {
      return;
    }
    if (!this.isSiteEmpty(site)) {
      this._alert.error(AlertType.ERROR_DELETE_NOT_EMPTY_SITE_GROUP);
      return;
    }
    const siteNode: SiteNodeValue = this._sitesNodesMap.get(site);
    if (!siteNode) {
      return;
    }
    const { svgGroup: removedSvgGroup, position: removedPos } = siteNode;
    const { site: siteConfig } = removedSvgGroup;
    this._sitesNodesMap.delete(site);
    const removedRbox: SVG.RBox = removedSvgGroup.nativeElement.rbox(this._view);
    removedSvgGroup?.nativeElement.remove();

    if (this._sitesNodesMap.size === 0) {
      this._onDeleteSite.emit(siteConfig);
      return;
    }

    // shift left all sites after the removed site
    const siteNodes: SiteNodeValue[] = [...this._sitesNodesMap.values()]
      .filter(value => {
        const { position: pos } = value;
        return (Config.COLUMN_COUNT * pos.row + pos.col) > (Config.COLUMN_COUNT * removedPos.row + removedPos.col);
      }).sort((val1, val2) => {
        const { position: pos1 } = val1;
        const { position: pos2 } = val2;
        return (pos1.row - pos2.row) || (pos1.col - pos2.col);
      });

    let newPos: Coordinates = { x: removedRbox.x, y: removedRbox.y };
    siteNodes.forEach(value => {
      const siteElement: SVG.G = value.svgGroup?.nativeElement;
      const rbox: SVG.RBox = siteElement.rbox(this._view);
      siteElement.animate(Config.SiteGroup.animateTime).move(newPos.x, newPos.y);
      newPos = {
        x: rbox.x,
        y: rbox.y
      };
      const newPosition: number = Config.COLUMN_COUNT * value.position.row + value.position.col - 1;
      const newRow: number = Math.floor(newPosition / Config.COLUMN_COUNT);
      const newCol: number = newPosition % Config.COLUMN_COUNT;
      value.position.row = newRow;
      value.position.col = newCol;
    });
    this._onDeleteSite.emit(siteConfig);
  }

  private renderUcpeTrafficPort(
    portsGroup: SVG.G,
    siteGroup: SvgSiteGroup,
    purpose: string,
    identifier: string,
    index: number,
    padding: number
  ): SVG.G {
    const trafficPort = (new SvgTrafficPort(
      portsGroup,
      purpose,
      SvgTrafficPortMode.DEFAULT
    )).element;
    trafficPort.data('purpose', purpose, true);
    trafficPort.id(identifier);
    siteGroup.trafficPorts.push(trafficPort);

    const portWidth = trafficPort.rbox().width;
    trafficPort.translate((portWidth + padding) * index, 0);
    trafficPort.on('click.inf', (e: MouseEvent) => {
      e.stopPropagation();
      this.onNfInterfaceClick(
        e,
        trafficPort,
        siteGroup.site,
        this._tempEdge ? WorldPortDirection.DEST : WorldPortDirection.SRC,
        purpose
      );
    });

    return trafficPort;
  }

  private goToInstanceStepIndex(index: number) {
    if (this._instanceStateStepper && this._instanceStateStepper.selectedIndex < index) {
      setTimeout(() => this._instanceStateStepper.selectedIndex = index);
    }
  }

  public isInstanceStepCompleted(state: InstanceStatusState, instance: PipelineNfInstance, index: number) {
    if (instance.state) {
      const instanceState: InstanceStatusState = instance.state;

      switch (instanceState) {
        case InstanceStatusState.CONFIGURED:
          if (state === InstanceStatusState.CONFIGURED) {
            this.goToInstanceStepIndex(index + 1);
            return true;
          }
        // tslint:disable-next-line: no-switch-case-fall-through
        case InstanceStatusState.LAUNCHED:
          if (state === InstanceStatusState.LAUNCHED) {
            this.goToInstanceStepIndex(index);
            return true;
          }
        // tslint:disable-next-line: no-switch-case-fall-through
        case InstanceStatusState.LAUNCHING:
          if (state === InstanceStatusState.LAUNCHING) {
            this.goToInstanceStepIndex(index);
            return true;
          }
        // tslint:disable-next-line: no-switch-case-fall-through
        case InstanceStatusState.NULLINSTANCESTATE:
          if (state === InstanceStatusState.NULLINSTANCESTATE) {
            this.goToInstanceStepIndex(index);
            return true;
          }
          break;
        default:
          // draining, failed
          return false;
      }
    }

    return false;
  }

  private addNodeToSiteMap(node: PipelineNode, index: number) {
    if (node?.nf || node?.port) {
      const siteId = node.site;

      if (this._sitesNodesMap.has(siteId)) {
        this._sitesNodesMap.get(siteId).nodes.push(node);
      } else {
        this._sitesNodesMap.set(siteId, {
          nodes: [node],
          position: {
            col: index % Config.COLUMN_COUNT,
            row: Math.floor(index / Config.COLUMN_COUNT)
          },
          siteConfig: {
            identifier: siteId,
            name: node.site,
            siteType: (node as unknown as TemplateNode).siteType
          }
        });
      }
    }
  }

  private setGraph(graph: PipelineGraph) {
    this._graph = graph;
    let update = false;

    if (!graph?.nodes) {
      return;
    }

    for (const [index, key] of (Object.keys(graph.nodes)).entries()) {
      const node = cloneDeep(graph.nodes[key]);
      this.addNodeToSiteMap(node, index);

      if (node.nf) {
        const curNode = this._nfs.get(key);

        if (!node.nf.local) {
          node.nf.local = {};
        }

        if (!node.nf.local.config) {
          node.nf.local.config = {};
        }

        if (!node.nf.local.name) {
          node.nf.local.name = curNode?.name || node.name;
        }

        if (!curNode) {
          // new NF detected
          update = true;
          this._nfs.set(key, node);
        }
        // initialize empty connectingNodes array for the edges rendering
        graph.nodes[key].nf.local.interfaces?.forEach((nfInterface: NFInterface) => {
          nfInterface.local = nfInterface.local || {};
          nfInterface.local.connectingNodes = [];
        });
      } else if (node.port) {
        const portId = node.identifier;

        if (!this._ports.get(portId)) {
          // new port detected
          update = true;
          node.local.status = PipelineNodeStatus.NEW;
          // initialize empty connectingNodes array for the edges rendering
          graph.nodes[portId].local.connectingNodes = [];
          this._ports.set(portId, node);
        }
      }
    }
    this.removeStaleNodes(graph);

    update = this.renderEdges(graph.edges, true) || update;

    if (update) {
      this.drawGraph();

      this._initialGraph = cloneDeep(this.getGraph());

      this.showNFLopStats();
    }

    this.startTraceStream();
  }

  private removeStaleNodes(graph: PipelineGraph) {
    const graphNodes = Object.keys(graph.nodes);

    for (const key of this._nfs.keys()) {
      if (!graphNodes.includes(key)) {
        this.deleteNF(this._nfs.get(key));
      }
    }

    for (const key of this._ports.keys()) {
      if (!graphNodes.includes(key)) {
        this.deletePort(this._ports.get(key));
      }
    }
  }

  private collectServers(node: ServiceNodeStatus) {
    if (node.instances) {
      const status: ServiceNodeStatus = node;
      for (const key of Object.keys(status.instances)) {
        const instance = status.instances[key];
        const machine = instance?.location?.placement?.machineId;
        if (machine && !this._servers.includes(machine)) {
          this._servers.push(machine);
        }
      }
    }

    this.setServerLegendItems();
  }

  private startTraceStream() {
    if (!this._tenant || !this._pipelineId) {
      return;
    }

    this.stopTraceStream();
    this._traceStreamSubscription = this._pipelineService.streamPipelineTrace(this._tenant.identifier, this._pipelineId).subscribe(
      (response: RESTGetTraceResponse) => {
        const performanceTrace: RESTDashboardGraphTrace = response.trace;

        if (!this._perfTrace.enabled && performanceTrace && performanceTrace.bpf) {
          // enable performance trace
          this.enablePerformanceTrace(true, performanceTrace.bpf.value);
        } else if (this._perfTrace.enabled) {
          if (performanceTrace?.bpf) {
            // update the filter in case it changed
            this._perfTrace.filter = performanceTrace.bpf.value;
          } else {
            // disable performance trace
            this.enablePerformanceTrace(false);
          }
        }
      }, (error: Error) => {
        this._log.error(error);
        setTimeout(() => {
          this.startTraceStream();
        }, Config.POLL_INTERVAL);
      }
    );
  }

  private stopTraceStream() {
    if (this._traceStreamSubscription) {
      this._traceStreamSubscription.unsubscribe();
    }
  }

  private setServerLegendItems() {
    this._serverLegendItems = this._servers.map((name: string, id: number): LegendItem => {
      const color: string = this.getServerColor(name);

      return {
        label: name,
        color
      } as LegendItem;
    });
  }

  /**
   * Synchronizes the given list of edges with the global list this._edges
   *
   * @param edges list of edge(s) to sync
   * @param cleanOld (boolean) remove edges from the global list that is not passed in the `edges` param
   * @returns (boolean) whether the global list this._edges has been updated
   */
  private renderEdges(edges: Array<PipelineEdge>, cleanOld = false): boolean {
    let update = false;

    if (cleanOld) {
      this._edges.map((e: PipelineEdge) => e.local.status = PipelineNodeStatus.UNKNOWN);
    }

    for (const edge of edges) {
      const foundEdge = this._edges.find((e: PipelineEdge) => {
        return edge.a.interface === e.a.interface
          && edge.a.node === e.a.node
          && edge.b.interface === e.b.interface
          && edge.b.node === e.b.node;
      });

      if (!edge.local) {
        edge.local = {};
      }

      edge.local.id = edge.local.id || randomID();
      if (foundEdge) {
        foundEdge.local.status = PipelineNodeStatus.CURRENT;
      } else {
        update = true;
        edge.local.status = PipelineNodeStatus.NEW;
        this._edges.push(edge);
      }
    }

    if (cleanOld) {
      // remove any old edges
      this._edges.forEach((e: PipelineEdge) => {
        if (e.local.status === PipelineNodeStatus.UNKNOWN) {
          this.removeEdge(e);
        }
      });
    }

    return update;
  }

  private removeSVGElement(elem: SVG.G): void {
    (elem.animate().attr({ opacity: 0 }) as any).afterAll(() => {
      elem.remove();
    });
  }

  private drawGraph(): void {
    try {
      this.initGraphLayout();
      this.drawSiteGroups();
      this.drawNFs();
      this.drawPorts();
      this.drawEdges();
      this.drawWorldEdges();
      this.align();
      this._onServiceRendered.emit();
    } catch (e) {
      this._alert.error(AlertType.ERROR_RENDER_SERVICE);
      this._log.error(e);
    }
  }

  public getSiteGroups(): string[] {
    return [...this._sitesNodesMap?.keys()];
  }

  private drawSiteGroups() {
    const numColumns = this._sitesNodesMap.size > 1 ? Config.SiteGroup.maxColumns : 1;
    const numRows = Math.ceil(this._sitesNodesMap.size / numColumns);
    const width = (this._drawing.rbox().w - (Config.SiteGroup.padding * 2)) / numColumns;
    const height = Config.SiteGroup.height;
    const position: { row: number; column: number } = { row: 0, column: 0 };

    this._sitesNodesMap.forEach((value: SiteNodeValue, siteId: string) => {
      const x = Config.SiteGroup.padding + (position.column * width);
      const y = Config.SiteGroup.padding + (position.row * height);
      const site = this.getSiteEntry(siteId);
      const siteConfig: SiteConfig = site?.config ?? {
        identifier: siteId,
        siteType: value.siteConfig?.siteType
      };

      const siteGroup = new SvgSiteGroup(
        this._view, {
        x,
        y,
        width,
        height
      },
        siteConfig,
        SiteGroupMode.DEFAULT,
        {
          editMode: this._editMode,
          isTemplateInstantiateMode: this._serviceMode === Mode.TEMPLATE_INSTANTIATE,
          onDeleteClick: this.deleteSiteGroup.bind(this)
        }
      );

      value.svgGroup = siteGroup;
      if (siteConfig.siteType === SiteConfigSiteType.CLUSTER) {
        this.drawWorldPort(siteGroup);
      } else if (siteConfig.siteType === SiteConfigSiteType.UCPE) {
        this.drawUcpeSiteTrafficPorts(siteGroup, undefined, site === undefined);
      }

      if (position.column < Config.SiteGroup.maxColumns - 1) {
        // render next site on same row, new column
        position.column++;
      } else {
        // render next site on new row, first column
        position.row++;
        position.column = 0;
      }
    });
  }

  private drawWorldPort(siteGroup: SvgSiteGroup) {
    const svgGroup = siteGroup.nativeElement;
    const padding = 10;
    const trafficPort = (new SvgTrafficPort(
      svgGroup,
      'WORLD',
      SvgTrafficPortMode.DEFAULT
    )).element;
    siteGroup.worldPort = trafficPort;

    // place on bottom center of site rect
    const [siteWidth, siteHeight, portWidth, portHeight] = [
      svgGroup.rbox().w,
      svgGroup.rbox().h,
      trafficPort.rbox().width,
      trafficPort.rbox().height
    ];
    trafficPort.translate(
      (siteWidth / 2) - (portWidth / 2),
      siteHeight - portHeight - padding
    );

    trafficPort.on('click.inf', (e) => {
      e.stopPropagation();
      this.onNfInterfaceClick(e, trafficPort, siteGroup.site, this._tempEdge ? WorldPortDirection.DEST : WorldPortDirection.SRC, 'WORLD');
    });
  }

  private initGraphLayout() {
    // update node edges
    this._edges.forEach((edge: PipelineEdge) => {
      if (edge.a?.interface) {
        this.addNfInterfaceRelation(edge.a?.node, edge.a?.interface, edge.b?.node);
      } else if (edge.b?.interface) {
        this.addNfInterfaceRelation(edge.b?.node, edge.b?.interface, edge.a?.node);
      } else {
        // for the case of when two ports are connected directly
        if (edge.b.node !== edge.a.node && !this._graph.nodes[edge.a.node].local.connectingNodes.includes(edge.b.node)) {
          this._graph.nodes[edge.a.node].local.connectingNodes.push(edge.b.node);
        }
      }
    });

    // for each site node create a parent node
    this._sitesNodesMap.forEach((siteNode: SiteNodeValue, siteId: string) => {
      const siteGraphModel = new dagre.graphlib.Graph({ compound: true, multigraph: true });
      this._graphModels.set(siteId, siteGraphModel);
      siteGraphModel.setGraph({
        width: this._drawing.rbox().w * Config.SiteGroup.widthFactor,
        height: Config.SiteGroup.height
      });
      // set site's parent node
      siteGraphModel.setNode(siteId, {
        label: siteId
      });

      // add each node in sitegroup add to model node
      siteNode.nodes.forEach((node: PipelineNode) => {
        siteGraphModel.setNode(node.identifier, {
          label: node.name,
          width: Config.NF.width,
          height: Config.NF.lgHeight
        });
        siteGraphModel.setParent(node.identifier, siteId);
        this._graph.nodes[node.identifier]?.nf?.local?.interfaces?.forEach(({ local }: NFInterface) => {
          local.connectingNodes?.forEach((id: string) => {
            siteGraphModel.setEdge(node.identifier, id, {});
          });
        });
      });

      dagre.layout(siteGraphModel);
    });
  }

  private addNfInterfaceRelation(sourceNodeId: string, interfaceId: string, targetNodeId: string) {
    const interfaceIndex = this.findInterfaceIndexById(sourceNodeId, interfaceId);
    const sourceNf = this._graph.nodes[sourceNodeId].nf;
    const targetNf = this._graph.nodes[targetNodeId].nf;
    const nfInterface = sourceNf.local.interfaces[interfaceIndex];
    if (
      nfInterface &&
      sourceNodeId !== targetNodeId &&
      !sourceNf.local.interfaces[interfaceIndex].local.connectingNodes.includes(targetNodeId)
    ) {
      sourceNf.local.interfaces[interfaceIndex].local.connectingNodes.push(targetNodeId);
    }
  }

  private findInterfaceIndexById(node: string, interfaceId: string): number {
    return this._graph.nodes[node]?.nf?.local.interfaces.findIndex(({ id }: NFInterface) => id === interfaceId);
  }

  private isInside(item: Coordinates, box: SVG.Box): boolean {
    return item.x >= box.x
      && item.x <= box.x2
      && item.y >= box.y
      && item.y <= box.y2;
  }

  public getSiteBox(site: string): RBox {
    const siteNode: SiteNodeValue = this._sitesNodesMap.get(site);
    if (!siteNode) {
      return undefined;
    }
    return siteNode.svgGroup?.border?.rbox(this._drawing);
  }

  private getInsideSite(item: Coordinates): string | undefined {
    return [...this._sitesNodesMap.keys()].find(site => this.isInside(item, this.getSiteBox(site)));
  }

  private changeSite(node: PipelineNode, site: string) {
    if (node.site === site) {
      return;
    }
    const oldSite: string = node.site;
    const oldSiteNode: SiteNodeValue = this._sitesNodesMap.get(oldSite);
    const newSiteNode: SiteNodeValue = this._sitesNodesMap.get(site);
    if (!oldSiteNode || !newSiteNode) {
      return;
    }
    const nodeNativeElement: SVG.Element = node.local.svgNF.nativeElement;
    oldSiteNode.svgGroup.nativeElement.removeElement(nodeNativeElement);
    node.local.status = undefined;
    this.drawNFs();
    if (this.isSiteEmpty(oldSite)) {
      oldSiteNode.svgGroup?.showNewSiteDescription();
    }
  }

  private isSiteEmpty(site: string): boolean {
    return [...this._nfs.values()].find((node: PipelineNode) => node.site === site) === undefined;
  }

  private drawNFs(): void {
    this._nfs.forEach((node: PipelineNode) => {
      if (!node.local.status || node.local.status === PipelineNodeStatus.NEW) {
        // hide NF management ports
        node.nf.local.interfaces = node.nf.local?.interfaces?.filter(
          ({ type }) => [VMInterfaceType.DATA, VMInterfaceType.DATATAP].includes(type) || node.nf.local.type === NFType.NATIVE
        );

        this._sitesNodesMap.forEach((value: SiteNodeValue, siteId: string) => {
          if (node.local.coordinates
            && this.isInside(node.local.coordinates, value.svgGroup.border.rbox(this._drawing))) {
            node.site = siteId;
          }
        });
        const elem = this.drawNF(node);
        node.local.ctx = elem;
        node.local.status = PipelineNodeStatus.CURRENT;
        if (this._editMode) {
          this.listenNodeGroup(node);

          if (node.local.isAdded) {
            node.local.isAdded = false;
            this.onClickNode(node);
          }
        }
      }
    });
  }

  private drawPorts(): void {
    const portRefs = new Array<SVG.G>();
    if (this._ports) {
      this._ports.forEach((node: PipelineNode, portName: string) => {
        if (node.local.status === PipelineNodeStatus.NEW) {
          const elem = this.drawTrafficPort(node, portName);
          portRefs.push(elem);
          node.local.ctx = elem;
          node.local.status = PipelineNodeStatus.CURRENT;
        }
      });
    }
  }

  private drawEdges(): void {
    for (let index = this._edges.length - 1; index >= 0; index--) {
      const edge = this._edges[index];
      if (edge.local.status === PipelineNodeStatus.NEW) {
        this.drawEdge(edge);
      } else if (edge.local.status === PipelineNodeStatus.CURRENT) {
        this.drawEdge(edge, undefined, true);
      }
    }
  }

  public updateStoredNodePositions(): void {
    if (this._nfs) {
      this._nfs.forEach((node: PipelineNode) => {
        this.storeNodePosition(node);
      });
    }

    if (this._ports) {
      this._ports.forEach((node: PipelineNode) => {
        this.storeNodePosition(node);
      });
    }
  }

  private updateGraph(): void {
    if (this._active) {
      if (!this._drawing) {
        // init SVG object
        this._drawing = SVG('drawing').size('100%', '100%');
        this._view = this._drawing.group().addClass('svg-viewport');
        (this._view as any).panZoom();
        this._view.on('zoom', this.onZoom.bind(this));
        this._edgeGroups = this._drawing.group();
        this._drawing.on('dblclick', (ev: any) => {
          this.align();
        });
      }

      if (this._pipelineId) {
        this.initPipelineGraph();
      } else if (this._template) {
        this.initTemplateGraph(this._template);
      }
    } else {
      if (this._streamSubscription) {
        this._streamSubscription.unsubscribe();
        this._streamSubscription = undefined;
      }

      this.stopTraceStream();
      this.stopPipelineStatusStream();
      this.stopPerformanceStatStream();
    }
  }

  private onZoom(e: CustomEvent): void {
    this._nfs.forEach((node: PipelineNode) => {
      const nodeRbox: RBox = this.getNodeBox(node);
      node.local.coordinates = {
        x: nodeRbox.x,
        y: nodeRbox.y
      };
    });
  }

  private align(): void {
    // TODO: (ecarino) need to figure out how pan & zoom will work with new design (NEF-7637)
    return;
  }

  private getPipelineNode(nodeName: string): PipelineNode {
    let node = this._nfs.get(nodeName);

    if (!node) {
      node = this._ports.get(nodeName);
    }

    return node;
  }

  private findInterfaceById(id: string, interfaces: NFInterface[]): NFInterface {
    return interfaces.find((inf: NFInterface) => inf.id === id);
  }

  private drawEdge(edge: PipelineEdge, dir = Direction.Bi, update = false) {
    const aNode = this.getPipelineNode(edge.a.node);
    const bNode = this.getPipelineNode(edge.b.node);

    if (!aNode || !bNode) {
      // The policy appears to be incomplete. Do not attempt to render this edge.
      this.removeEdge(edge);
      return;
    }

    const aNodeCtx = aNode.nf ?
      this.findInterfaceById(edge.a.interface, aNode.nf.local.interfaces).local.ctx.select('.interface-box').first() :
      aNode.local.ctx;
    const bNodeCtx = bNode.nf ?
      this.findInterfaceById(edge.b.interface, bNode.nf.local.interfaces).local.ctx.select('.interface-box').first() :
      bNode.local.ctx;

    let view = this._view;
    let siteNode: SiteNodeValue;
    if (aNode.site === bNode.site) {
      siteNode = this._sitesNodesMap.get(aNode.site);
      view = siteNode.svgGroup.nativeElement;
    }

    edge.local.coordinates = {
      a: [aNodeCtx.rbox(view).cx, aNodeCtx.rbox(view).cy],
      b: [bNodeCtx.rbox(view).cx, bNodeCtx.rbox(view).cy]
    };

    const coordinates: PathCoordinates = {
      a: [
        (edge.filterAb) ? edge.local.coordinates.a[0] : edge.local.coordinates.b[0],
        (edge.filterAb) ? edge.local.coordinates.a[1] : edge.local.coordinates.b[1]
      ],
      b: [
        (edge.filterAb) ? edge.local.coordinates.b[0] : edge.local.coordinates.a[0],
        (edge.filterAb) ? edge.local.coordinates.b[1] : edge.local.coordinates.a[1]
      ]
    };

    if (update) {
      edge.local.svgEdge.moveEdge(coordinates, !!edge.filterAb);
      return;
    }

    if (!aNode.local.edges) {
      aNode.local.edges = new Array<PipelineEdge>();
    }
    aNode.local.edges.push(edge);

    if (!bNode.local.edges) {
      bNode.local.edges = new Array<PipelineEdge>();
    }
    bNode.local.edges.push(edge);

    if (!edge.filterAb || !edge.filterBa) {
      dir = Direction.Uni;
    }

    const tunnelNode = aNode.port ? aNode : bNode;
    const hasTunnel = !!(aNode.port || bNode.port);
    const hasEdgeHandler = this._editMode && this._serviceMode !== Mode.TEMPLATE_INSTANTIATE;

    // create visible edge polyline
    const svgEdge = new SvgEdge(
      view,
      this._editMode,
      coordinates,
      this.getEdgeFilter(edge),
      hasTunnel,
      dir,
      (e) => {
        if (this._isDragging) {
          return;
        }

        const tunnelId = hasTunnel ? aNode.port?.tunnel ?? bNode.port?.tunnel : undefined;
        this.onTrafficPortClick(e, tunnelNode, tunnelId);
      },
      hasEdgeHandler
    );

    edge.local.svgEdge = svgEdge;
    edge.local.status = PipelineNodeStatus.CURRENT;

    if (hasEdgeHandler) {
      svgEdge.onClick((e) => {

        const data: EdgeFormData = {
          edge,
          filterAb: this.getEdgeFilter(edge, true, true),
          filterBa: this.getEdgeFilter(edge, false, true),
          direction: this.getEdgeDirection(edge)
        };

        const aName = aNode.local.worldEdge ? 'WORLD' : aNode.local.ctx.data('purpose') || aNode.local?.name;
        const bName = bNode.local.worldEdge ? 'WORLD' : bNode.local.ctx.data('purpose') || bNode.local?.name;
        this._sidebarService.open(`Edge between ${aName} and ${bName}`, EdgeFormComponent, data);
        this._sidebarService.afterClosed$.subscribe(({
          shouldDelete,
          filterAb,
          filterBa,
          direction
        }: EdgeSidebarSettings) => {
          this._activeEdge = edge;
          if (shouldDelete) {
            this.onRemoveEdgeGroup(edge);
            return;
          }

          this.updateEdgeFilter(edge, filterAb, true);
          this.updateEdgeFilter(edge, filterBa, false);
          this.onChangeEdgeDirection(direction);
        });
      });
    }

    siteNode?.svgGroup.portsToFront();

    if (hasTunnel) {
      const tunnelId = aNode.port?.tunnel ?? bNode.port?.tunnel;
      this.updateEdgeTunnel(svgEdge, tunnelId);
    }
  }

  private getEdgeDirection(edge: PipelineEdge): EdgeDirection {
    let direction: EdgeDirection = EdgeDirection.Both;
    if (edge.filterAb && edge.filterBa) {
      direction = EdgeDirection.Both;
    } else if (edge.filterAb) {
      direction = EdgeDirection.Upward;
    } else if (edge.filterBa) {
      direction = EdgeDirection.Downward;
    }
    return direction;
  }

  private updateEdgeTunnel(svgEdge: SvgEdge, tunnelId: string) {
    const tunnel = this._tunnels.find((t: Tunnel) => {
      return t.identifier === tunnelId;
    });

    svgEdge.encapIndicator.encapType = SvgTrafficPortEncapType.UNDEFINED;
    if (tunnel?.localTunnel) {
      svgEdge.encapIndicator.encapType = SvgTrafficPortEncapType.LOCAL;

      const localTunnel: Tunnel = this._tunnels.find((t: Tunnel) => {
        return t.identifier === tunnel.localTunnel.peerTunnelId;
      });

      svgEdge.encapIndicator.tooltip.text([
        'ENCAPSULATION',
        '',
        `Local Peer: ${localTunnel?.name ? localTunnel.name : 'None'}`
      ]);
    } else if (tunnel?.remoteTunnel) {
      if (tunnel.remoteTunnel.encap?.vxlan) {
        const vxlan = tunnel.remoteTunnel.encap.vxlan;

        svgEdge.encapIndicator.encapType = SvgTrafficPortEncapType.VXLAN;
        svgEdge.encapIndicator.tooltip.text([
          'ENCAPSULATION',
          '',
          'VXLAN',
          `VNI: ${vxlan.vni}`,
          `IP: ${vxlan.vtep.ipAddr}`,
          `MAC: ${vxlan.vtep.mac}`,
          `PORT: ${vxlan.vtep.udpPort}`
        ]);
      } else if (tunnel.remoteTunnel.encap?.ivid || tunnel.remoteTunnel.encap?.ovid) {
        const encap = tunnel.remoteTunnel.encap;
        const text = ['ENCAPSULATION', '', 'VLAN'];
        svgEdge.encapIndicator.encapType = SvgTrafficPortEncapType.VLAN;

        if (encap.ivid) {
          text.push(`IVID: ${encap.ivid}`);
        }

        if (encap.ovid) {
          text.push(`OVID: ${encap.ovid}`);
        }

        svgEdge.encapIndicator.tooltip.text(text);
      } else if (tunnel.remoteTunnel.physicalPort) {
        svgEdge.encapIndicator.encapType = SvgTrafficPortEncapType.RAW;

        svgEdge.encapIndicator.tooltip.text([
          'ENCAPSULATION',
          '',
          `Port: ${tunnel.remoteTunnel.physicalPort}`
        ]);
      }
    } else {
      // TODO(eric): for UCPE sites, just say 'None', since no extra configuration is valid
      svgEdge.encapIndicator.tooltip.text('Encapsulation not configured');
    }
  }

  private drawWorldEdges(ports?: Map<string, PipelineNode> | PipelineNode[]) {
    if (!ports) {
      ports = this._ports;
    }

    ports.forEach((port: PipelineNode) => {
      const svgSiteGroup = this._sitesNodesMap.get(port.site).svgGroup;

      if (svgSiteGroup.type === SiteConfigSiteType.CLUSTER) {
        const x1 = svgSiteGroup.worldPort.rbox(this._view).cx;
        const y1 = svgSiteGroup.worldPort.rbox(this._view).cy;
        const x2 = port.local.ctx.rbox(this._view).cx;
        const y2 = port.local.ctx.rbox(this._view).cy;

        // create visible edge polyline
        const group: SVG.G = this._view.group();
        group.addClass('edge-container');
        const xMid = (x1 + x2) / 2;
        const yMid = (y1 + y2) / 2;
        const path = this._drawing.polyline([[x1, y1], [xMid, yMid], [x2, y2]]);
        path.addClass('edge');

        group.add(path);

        port.local.worldEdge = group;
      }
    });
  }

  private getStoreName(node: PipelineNode): string {
    return this._pipelineId + '-' + node.local.id;
  }

  private onDragEnd(node: PipelineNode): void {
    if (node) {
      const prevCoordinates: Coordinates = node.local.coordinates;
      node.local.coordinates = this.storeNodePosition(node);
      const siteRbox: RBox = this.getSiteBox(node.site);
      const nodeRbox: RBox = this.getNodeBox(node);
      const nodeCenterCoordinates: Coordinates = {
        x: nodeRbox.cx,
        y: nodeRbox.cy
      };
      if (!this.isInside(nodeCenterCoordinates, siteRbox)) {
        // check if node has connection with a site's port
        const newSite: string = this.getInsideSite(nodeCenterCoordinates);
        const siteEdges: PipelineEdge[] = this.getEdgesConnectedToPort(node);
        if (siteEdges) {
          const data: ConfirmationDialogData = {
            description: `This NF will be moved to the new site. All edges to the site's port will be removed.`,
            action: 'Proceed'
          };

          const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
            ConfirmationDialogComponent,
            { ...DIALOG_CONFIG, data }
          );

          dialogRef
            .afterClosed()
            .subscribe(proceed => {
              if (proceed) {
                siteEdges.forEach(edge => this.removeEdge(edge));
                this.changeSite(node, newSite);
              } else {
                const movedX = prevCoordinates.x - node.local.coordinates.x;
                const movedY = prevCoordinates.y - node.local.coordinates.y;
                this.adjustEdges(movedX, movedY, node);
                node.local.svgNF.nativeElement.dmove(movedX, movedY);
                node.local.coordinates = this.storeNodePosition(node);
              }
            });
        } else {
          this.changeSite(node, newSite);
        }
      }
    }
  }

  private getEdgesConnectedToPort(node: PipelineNode): PipelineEdge[] {
    const ports: string[] = [... this._ports.keys()];
    return node.local.edges?.filter(edge => ports.find(id => id === edge.a.node || id === edge.b.node));
  }

  private getNodeNativeElement(node: PipelineNode): SVG.Element {
    return node.nf ? node.local.ctx.first() : node.local.ctx;
  }

  private getNodeBox(node: PipelineNode): RBox {
    const ctx: SVG.Element = this.getNodeNativeElement(node);
    return ctx.rbox(this._drawing);
  }

  private storeNodePosition(node: PipelineNode): Coordinates {
    const rbox: RBox = this.getNodeBox(node);
    const storeName: string = this.getStoreName(node);
    const coordinates: Coordinates = {
      x: rbox.x,
      y: rbox.y
    };

    this._store.write(storeName, JSON.stringify({
      id: node.local.id,
      ...coordinates
    }));

    if (node.local.edges) {
      node.local.edges.forEach((edge) => {
        if (edge.local.dragCoordinates) {
          edge.local.coordinates = edge.local.dragCoordinates;
          edge.local.dragCoordinates = undefined;
        }
      });
    }
    return coordinates;
  }

  private onDrag(event: any, node: PipelineNode): void {
    event.stopPropagation();

    if (node) {
      this.adjustEdges(event.detail.delta.movedX, event.detail.delta.movedY, node);
    }
  }

  private adjustEdges(movedX: number, movedY: number, node: PipelineNode) {
    if (node.local.edges) {
      node.local.edges.forEach((edge: PipelineEdge) => {
        let x1 = edge.local.coordinates.a[0];
        let y1 = edge.local.coordinates.a[1];
        let x2 = edge.local.coordinates.b[0];
        let y2 = edge.local.coordinates.b[1];
        const movedNodeA = node.local.id === edge.a.node;
        const movedNodeB = node.local.id === edge.b.node;

        if (movedNodeA) {
          x1 = x1 + movedX;
          y1 = y1 + movedY;
        } else if (movedNodeB) {
          x2 = x2 + movedX;
          y2 = y2 + movedY;
        } else {
          this._log.warn('Edge not associated with this NF');
        }

        const updatedCoordinates: PathCoordinates = {
          a: [x1, y1],
          b: [x2, y2]
        };

        edge.local.svgEdge.moveEdge(updatedCoordinates, !!edge.filterAb);
        edge.local.dragCoordinates = updatedCoordinates;
      });
    }
  }

  private scaleUpNf(node: PipelineNode, instance: PipelineNfInstance, index: number) {
    const settings: NFRectSettings = {
      node,
      index,
      instance,
      animate: true
    };

    const nFRect: SVG.G = node.local.svgNF?.getNFRect(node.local.svgNF?.nativeElement, settings);
    node.local.ctx.add(nFRect);
    node.nf.local.instances.push(instance);
  }

  private scaleDownNf(node: PipelineNode, instance: PipelineNfInstance) {
    if (!node.nf.local.instances || !node.nf.local.instances.length) {
      // there is nothing to scale down
      return;
    }

    let index = 0;
    const foundInst = node.nf.local.instances.find((inst: PipelineNfInstance, i: number) => {
      index = i;
      return inst.identity.instanceId === instance.identity.instanceId;
    });

    if (foundInst) {
      this.removeSVGElement(foundInst.local.ctx);

      node.nf.local.instances.splice(index, 1);
    }
  }

  private getServerColor(serverName: string, vendor?: string): string {
    if (!serverName) {
      return Config.NF.serverDefaultColor;
    }

    let color = this._store.read(`${LocalStorageKey.SERVER_COLOR_PREFIX}${serverName}`);

    if (!color) {
      const colorIndex: number = +(this._store.read(LocalStorageKey.SERVER_COLOR_INDEX) ?? 0);
      color = GetNFColor(serverName, vendor, colorIndex);
      this._store.write(`${LocalStorageKey.SERVER_COLOR_PREFIX}${serverName}`, color);
      this._store.write(LocalStorageKey.SERVER_COLOR_INDEX, colorIndex + 1);
    }

    return color;
  }

  private drawNF(node: PipelineNode): SVG.G {
    const siteSvgGroup = this._sitesNodesMap.get(node.site);

    const networkFunction = new SvgNetworkFunction(
      siteSvgGroup?.svgGroup.nativeElement,
      node,
      {
        editMode: this._editMode,
        isTemplateInstantiateMode: this._serviceMode === Mode.TEMPLATE_INSTANTIATE,
        isLogicalView: this._isLogicalView,
        isPerformanceTraceView: this._perfTrace.enabled,
        getBorderColor: this.getServerColor.bind(this),
        onInterfaceClick: this.onNfInterfaceClick.bind(this),
        onInterfaceSettingsClick: this.onInterfaceSettingsMouseDown.bind(this),
        onSettingsClick: this.onSettingsMouseDown.bind(this),
        onDeleteNF: this.deleteNF.bind(this),
        onStatusDotClick: this.onStatusDotClick.bind(this)
      }
    );
    node.local.svgNF = networkFunction;
    siteSvgGroup.svgGroup.hideNewSiteDescription();
    this.initDrag(networkFunction.nativeElement, node);

    if (node.local.coordinates) {
      const nodeBorder: RBox = networkFunction.border.rbox(this._drawing);

      const transform: Transform = this._view.transform();
      const point: Coordinates = {
        x: node.local.status === PipelineNodeStatus.NEW ? nodeBorder.x + nodeBorder.w / 2 : nodeBorder.x,
        y: node.local.status === PipelineNodeStatus.NEW ? nodeBorder.y + nodeBorder.h / 2 : nodeBorder.y
      };
      const delta: Coordinates = {
        x: (node.local.coordinates.x - point.x) / (transform.scaleX || 1),
        y: (node.local.coordinates.y - point.y) / (transform.scaleY || 1)
      };
      const nodeModel = this._graphModels[node.site]?.node(node.identifier);
      if (nodeModel) {
        networkFunction.nativeElement.translate(nodeModel.x, nodeModel.y);
      } else {
        networkFunction.nativeElement.translate(delta.x, delta.y);
      }

      const nodeRbox: RBox = networkFunction.nativeElement.rbox(this._drawing);
      node.local.coordinates = {
        x: nodeRbox.x,
        y: nodeRbox.y
      };
    } else {
      const nodeModel = this._graphModels.get(node.site)?.node(node.identifier);
      if (nodeModel) {
        networkFunction.nativeElement.translate(nodeModel.x, nodeModel.y);
      }
    }
    node.local?.edges?.forEach(edge => edge.local?.svgEdge?.element.front());

    return networkFunction.nativeElement;
  }

  private onStatusDotClick(instance: PipelineNfInstance) {
    if (instance.state !== InstanceStatusState.FAILED) {
      this._popups.instanceStatus.isOpen = false;
      this._popups.instanceStatus.selected = undefined;
      this.openInstanceStatusPopup(instance);
    }
  }

  private onNfInterfaceClick(
    e: Event,
    inf: SVG.G,
    site: SiteConfig | string,
    worldPortDirection?: WorldPortDirection,
    label?: string) {
    let siteConfig: SiteConfig = (typeof site === 'string') ? (this.getSiteEntry(site))?.config : site;

    if (!siteConfig) {
      siteConfig = {
        identifier: site as string
      };
    }

    if (this._tempEdge) {
      // clicked on interface
      this.removeTempEdge();
      this._destInterface = {
        svg: inf,
        worldPortDirection,
        siteConfig
      };
      if (this._destInterface.svg !== this._sourceInterface.svg) {
        if (this.isValidConnection(this._sourceInterface, this._destInterface)) {
          const edge = this.connectInterfaces(
            this._destInterface.svg,
            this._sourceInterface.svg,
            siteConfig,
            undefined,
            worldPortDirection || this._sourceInterface.worldPortDirection,
            label);
          this._activeEdge = edge;
        }
      }
      (SVG.select('.inf-label') as unknown as SVG.Text).addClass('hidden');
      this._sourceInterface.svg.parent()['node']?.instance.front();
      this._destInterface.svg.parent()['node']?.instance.front();
    } else {
      // no temp edge in progress, create one
      this._sourceInterface = { svg: inf, worldPortDirection, siteConfig };
      this._tempEdge = this._drawing.polyline([]).addClass('edge temp');

      (this._tempEdge as any).draw(e, { drawCircles: false });
      this._drawing.on('click.removeTmpEdge', () => {
        this.removeTempEdge();
        (SVG.select('.inf-label') as unknown as SVG.Text).addClass('hidden');
      });
      (SVG.select('.inf-label') as unknown as SVG.Text).removeClass('hidden');
    }
  }

  private isValidConnection(src: ConnectionInterface, dest: ConnectionInterface): boolean {
    if (src.siteConfig.identifier !== dest.siteConfig.identifier
      && (src.worldPortDirection || dest.worldPortDirection)) { // connection to/from different site's port
      this._alert.error(AlertType.ERROR_SITE_PORT_CONNECTION);
      return false;
    }
    return true;
  }

  private removeTempEdge(): void {
    if (this._tempEdge) {
      (this._tempEdge
        .front() as any)
        .draw('stop')
        .remove();
      this._tempEdge = undefined;
      this._drawing.off('click.removeTmpEdge');
    }
  }

  private initDrag(element: SVG.G, node?: PipelineNode) {
    (element as any).draggy();

    const namespace: string = node?.nf ? 'nf' : 'port';

    element.on(`dragmove.${namespace}`, (event: CustomEvent) => {
      this._isDragging = true;

      this.onDrag(event, node);
    });

    element.on(`dragend.${namespace}`, (event) => {
      event.stopPropagation();
      /* timeout helps to skip unwanted click event after dragend event
         because mouseup happens before dragend leading to click */
      setTimeout(() => this._isDragging = false);

      this.onDragEnd(node);
    });
  }

  private drawTrafficPort(node: PipelineNode, portName: string): SVG.G {
    const siteSvgGroup = this._sitesNodesMap.get(node.site);
    const siteEntry = this.getSiteEntry(node.site);
    const portMode = !siteEntry
      || siteEntry.config.siteType === SiteConfigSiteType.CLUSTER
      || siteEntry.config.siteType === SiteConfigSiteType.UCPE
      ? SvgTrafficPortMode.HIDDEN
      : SvgTrafficPortMode.DEFAULT;

    const trafficPort = new SvgTrafficPort(
      siteSvgGroup?.svgGroup.nativeElement,
      portName,
      portMode,
      this._editMode
    );
    node.port.local.svgPort = trafficPort;
    this.initDrag(trafficPort.element, node);

    const { worldPort, trafficPorts } = siteSvgGroup.svgGroup;
    if (worldPort) {
      trafficPort.element.center(
        siteSvgGroup.svgGroup.worldPort.cx(),
        siteSvgGroup.svgGroup.worldPort.cy()
      );
    } else if (trafficPorts.length) {
      const tunnel = this._tunnels.find((t: Tunnel) => t.identifier === node.port?.tunnel);
      const tpElement = trafficPorts.find((tp: SVG.G) => {
        return tp.id() === tunnel?.remoteTunnel?.physicalPort
          || tp.id() === portName
          || node.port?.tunnel?.match(`findPortWithPurpose.*${tp.data('purpose')}`)?.length > 0
          || tunnel?.name === tp.data('purpose');
      });
      const { cx, cy } = tpElement.rbox(siteSvgGroup?.svgGroup.nativeElement);
      trafficPort.element.center(cx, cy);

      // TODO (anton): move traffic port purpose assignment to a more appropriate place
      const purpose = tpElement.data('purpose');
      trafficPort.element.data('purpose', purpose, true);
    }

    return trafficPort.element;
  }

  private onTrafficPortClick(e: Event, node: PipelineNode, tunnelId?: string) {

    const data: TrafficPortFormData = {
      site: this.getSiteEntry(node.site)?.config,
      tenant: this._tenant,
      isTemplate: [Mode.TEMPLATE, Mode.TEMPLATE_INSTANTIATE].includes(this._serviceMode),
      tunnelId
    };

    this._sidebarService.open(`Encapsulation Settings`, TrafficPortFormComponent, data);

    this._sidebarService.afterClosed$.subscribe(({ identifier, name }: TunnelConfig) => {
      node.port.tunnel = identifier;

      this.onNodeChange(node, name);
    });
  }

  private onInterfaceSettingsMouseDown(event: MouseEvent, node: PipelineNode, instanceIndex: number, interfaceName: string) {
    event.stopPropagation();
    this.openPacketDialog(node, instanceIndex, interfaceName);
  }

  private onSettingsMouseDown(event: MouseEvent, node: PipelineNode, instanceIndex: number, interfaceName: string) {
    const nodeName = node.name || node.local.name;
    const data: NFFormData = {
      name: nodeName,
      tenant: this._tenant,
      pipelineId: this._pipelineId,
      nodeId: node.local.id,
      nf: node.nf,
      isEditMode: this._editMode && this._serviceMode !== Mode.TEMPLATE_INSTANTIATE,
      isDraft: this._isDraft,
      isTemplate: [Mode.TEMPLATE, Mode.TEMPLATE_INSTANTIATE].includes(this._serviceMode)
    };

    this._sidebarService.open(`${nodeName} Settings`, NFFormComponent, data);
    this._sidebarService.afterClosed$.subscribe(([name]: [string]) => {
      node.name = name;
      node.nf.local.name = name;
      this.onNodeChange(node, name);
    });
  }

  private openPacketDialog({ identifier, nf }: PipelineNode, instanceIndex: number, port: string) {
    const data: PacketDialogData = {
      tenant: this._tenant.identifier,
      service: this._pipelineId,
      node: identifier,
      instance: nf.local.instances[instanceIndex]?.identity.instanceId,
      port
    };

    this._dialog.open(PacketCaptureDialogComponent, {
      width: Config.PacketDialog.width,
      data
    });
  }

  private connectInterfaces(
    dest: SVG.G,
    source: SVG.G,
    site?: SiteConfig,
    direction?: number,
    worldPortDirection = WorldPortDirection.NONE,
    label?: string
  ): PipelineEdge {
    if (worldPortDirection === WorldPortDirection.DEST) {
      dest = this.addPort(site, label)?.local.ctx;
    } else if (worldPortDirection === WorldPortDirection.SRC) {
      source = this.addPort(site, label)?.local.ctx;
    }

    const edge: PipelineEdge = this.createEdge(dest, source, direction);
    this.renderEdges([edge]);
    if (edge.local.status === PipelineNodeStatus.NEW) {
      this.drawEdge(edge);
    }

    return edge;
  }

  private addPort(site: SiteConfig, name?: string): PipelineNode {
    const portId = `port_${randomID()}`;
    const portName = name ?? '⚠️';
    const tunnel = site.siteType === SiteConfigSiteType.UCPE
      ? `{{createtunnel {\"name\":\"${name}\",\"remoteTunnel\":{\"physicalPort\":\"{{findPortWithPurpose \"${name}\"}}\"}}}}`
      : '';

    const newPort: PipelineNode = {
      site: site.identifier,
      port: {
        tunnel,
        local: {}
      },
      local: {
        id: portId,
        name: portName,
        status: PipelineNodeStatus.NEW
      }
    };

    const newPorts = new Map(this._ports);
    newPorts.set(portId, newPort);
    this._ports = newPorts;

    const elem = this.drawTrafficPort(newPort, portName);
    const group = elem;

    newPort.local.ctx = group;
    newPort.local.status = PipelineNodeStatus.CURRENT;

    return newPort;
  }

  private createEdge(destInterface: SVG.G, sourceInterface: SVG.G, direction?: number): PipelineEdge {
    const destGroup: ParentNode = destInterface.node.parentNode.parentNode;
    const srcGroup: ParentNode = sourceInterface.node.parentNode.parentNode;
    const destNode: PipelineNode = this.getNodeByElement(destGroup) ?? this.getNodeByElement(destInterface.node);
    const srcNode: PipelineNode = this.getNodeByElement(srcGroup) ?? this.getNodeByElement(sourceInterface.node);

    const edge: PipelineEdge = {
      a: {
        node: srcNode.local.id,
        interface: srcNode.nf ? this.getNFPortName(sourceInterface.node.id, srcNode) : ''
      },
      b: {
        node: destNode.local.id,
        interface: destNode.nf ? this.getNFPortName(destInterface.node.id, destNode) : ''
      },
      local: {
        id: randomID()
      }
    };

    switch (direction) {
      case EdgeDirection.Upward:
        edge.filterAb = {};
        break;
      case EdgeDirection.Downward:
        edge.filterBa = {};
        break;
      case EdgeDirection.Both:
      default:
        edge.filterAb = {};
        edge.filterBa = {};
    }

    return edge;
  }

  private getEdgeByElement(el: SVG.G): PipelineEdge {

    return this._edges.find((e: PipelineEdge) => {
      return e.local.svgEdge.element.id() === (el as any).id || e.local.svgEdge.element.id() === el.id();
    });
  }

  private getNodeByElement(node: ParentNode): PipelineNode {
    let ret: PipelineNode;

    this._ports.forEach((port: PipelineNode) => {
      if (port.local.ctx.id() === (node as any).id) {
        ret = port;
      }
    });
    if (!ret) {
      this._nfs.forEach((nf: PipelineNode) => {
        if (nf.local.ctx.id() === (node as any).id) {
          ret = nf;
        }
      });
    }
    return ret;
  }

  private getNFPortName(portSVGId: string, node: PipelineNode): string {
    const nfPorts: NFInterface[] = node.nf.local.interfaces;
    return nfPorts.find((port: NFInterface) => portSVGId === port.local.ctx.id()).id;
  }

  public onDrop(event: any): void {
    event.preventDefault();
    event.stopPropagation();
    const point = this._drawing.point(event.clientX, event.clientY);
    const matrix = new SVG.Matrix(this._drawing);
    point.transform(matrix.inverse());
    this.handleDrop.emit(point);
  }

  public allowDrop(event: Event): boolean {
    event.preventDefault();
    event.stopPropagation();
    return true;
  }

  private getSiteId(siteName: string): string {
    const foundSite = this._sites.find(site => site.config.name === siteName);
    return foundSite ? foundSite.config.identifier : siteName;
  }

  public getSiteName(siteId: string): string {
    const foundSite = this._sites.find(site => site.config.identifier === siteId);
    return foundSite ? foundSite.config.name : siteId;
  }

  public getSiteEntry(siteId: string): Site {
    return this._sites.find(site => site.config.identifier === siteId);
  }

  public getGraph(isTemplate = false): PipelineGraph | ServiceConfigTemplate {
    const graph: PipelineGraph | ServiceConfigTemplate = {
      edges: new Array<PipelineEdge>(),
      nodes: {}
    };

    this._edges.forEach((edge: PipelineEdge, index: number) => {
      graph.edges.push({
        a: edge.a,
        b: edge.b,
        filterAb: edge.filterAb,
        filterBa: edge.filterBa
      });
    });

    this._nfs.forEach((node: PipelineNode, key: string) => {
      const siteType = isTemplate && node.nf ? this._sitesNodesMap.get(node.site)?.siteConfig?.siteType : undefined;

      graph.nodes[key] = {
        name: node.nf.local.name,
        nf: {
          catalogId: node.nf.catalogId
        },
        site: this.getSiteId(node.site),
        siteType
      };
    });

    this._ports.forEach((node: PipelineNode, key: string) => {
      const siteType = this._sitesNodesMap.get(node.site)?.siteConfig?.siteType;

      graph.nodes[key] = {
        port: {
          tunnel: node.port ? node.port.tunnel : undefined
        },
        site: node.site
      };
    });

    return graph;
  }

  /**
   * compare given graph with initial graph for changes
   *
   * @param graph graph to compare
   */
  public hasGraphChanges(graph: PipelineGraph | ServiceConfigTemplate): boolean {
    return !isEqual(graph, this._initialGraph);
  }

  public hasNfConfigChanges(): boolean {
    return this._hasNfConfigChanges;
  }

  public revertToActive() {
    this._pipelineService.getTenantPipeline(this._tenant, this._pipelineId).subscribe(
      (graph: PipelineGraph) => this.setGraph(graph),
      (error: HttpErrorResponse) => this._log.error(error));
  }

  public deletePort(portNode: PipelineNode) {
    const [port, key] = this.findInCollection(portNode.local.id || portNode.local.name, this._ports);
    this.removeNodeEdges(port, key);
    this._ports.delete(key);
    port.local.ctx.remove();
    portNode.local.ctx.off('click');
  }

  public deleteNF(nfNode: PipelineNode, cleanConfig = true) {
    const [nf, key] = this.findInCollection(nfNode.local.id, this._nfs);
    this.removeNodeEdges(nf, key);
    this.removeDefs(nf);
    this._nfs.delete(key);
    nf.local.ctx.remove();

    if (cleanConfig) {
      this._pipelineService.deleteNFConfiguration(nfNode.local.id);
    }

    if (nfNode.nf.local.instances[0]) {
      nfNode.nf.local.instances[0].local.ctx.off('click');
    }
    const site: string = nfNode.site;
    if (this.isSiteEmpty(site)) {
      const siteNode = this._sitesNodesMap.get(site);
      siteNode?.svgGroup?.showNewSiteDescription();
    }
  }

  private updateNF(nfNode: PipelineNode, updatedManifest: NFDescriptor) {
    const siteId = nfNode.site;
    this.deleteNF(nfNode, false);

    const newNode = createPipelineNode({
      local: {
        id: nfNode.nf.local.name,
        vendor: nfNode.nf.local.vendor,
        catalogId: nfNode.nf.catalogId,
      },
      ...updatedManifest
    }, nfNode.local.id, nfNode.nf.local.name, nfNode.local.coordinates);
    newNode.site = siteId;

    this._nfs.set(nfNode.local.id, newNode);

    this.drawNFs();
  }

  private removeDefs(node: PipelineNode) {
    node.local.defs.forEach(def => def.remove());
  }

  private removeNodeEdges(node: PipelineNode, key: string): void {
    // delete all edge pointers from all adjacent nodes
    if (node.local.edges) {
      const deletedIds = [];
      const edges = node.local.edges;
      let i = edges.length;
      let adjacentNode: PipelineNode;

      while (i--) {
        const edge = edges[i];
        [adjacentNode] = edge.a.node === key ? this.getNodeById(edge.b.node) : this.getNodeById(edge.a.node);
        this.removeEdgePointer(edge, adjacentNode);
        this.removeEdgePointer(edge, node);
        edge.local.svgEdge.element.remove();
        deletedIds.push(edge.local.id);
      }
      this._edges = this._edges.filter(e => !deletedIds.includes(e.local.id));
    }

    node.local.worldEdge?.remove();
  }

  private findInCollection(id: string, collection): [PipelineNode, string] {
    let node: PipelineNode;
    let name: string;
    collection.forEach((item: PipelineNode, key: string) => {
      if (item.local.id === id) {
        node = item;
        name = key;
      }
    });
    return [node, name];
  }

  private getNodeById(id: string): [PipelineNode, string] {
    let node: PipelineNode;
    let name: string;
    [node, name] = this.findInCollection(id, this._nfs);
    if (!node || !name) {
      [node, name] = this.findInCollection(id, this._ports);
    }
    return [node, name];
  }

  private removeEdgePointer(edge: PipelineEdge, node: PipelineNode): void {
    if (node.local.edges) {
      const index = node.local.edges.findIndex((e, idx) =>
        (edge.local.id && e.local.id === edge.local.id) ||
        (edge.a.node === e.a.node && edge.b.node === e.b.node)
      );
      node.local.edges.splice(index, 1);
    }
  }

  private listenEdgeGroup(edge: PipelineEdge): void {
    // TODO(eric): clean up - I disabled this because was causing problem when trying to add
    // edge to the site's svg group. Why is _edgeGroups needed in order to handle edge clicks?
    // If this is needed, let's discuss for possible alternatives

    // const group = edge.local.svgEdge.element;
    // this._edgeGroups.add(group);
    // group.on('click.edge', () => this.onClickEdge(edge));
  }

  private listenNodeGroup(node: PipelineNode): void {
    const group = node.nf && node.nf.local?.instances[0] ? node.nf.local?.instances[0].local?.ctx : node.local?.ctx;
    group.on('click', () => this.onClickNode(node));
  }

  private onClickEdge(edge: PipelineEdge): void {
    this._activeEdge = edge;
  }

  private onClickNode(node: PipelineNode): void {
    if (this._isDragging) {
      return;
    }

    this._popups.node.selected = node;
  }

  public onNodeChange(node: PipelineNode, newName: string) {
    const oldName = node.local.name;
    if (!newName) {
      return;
    }
    if (node.nf) {
      // network function
      node.nf.local.name = newName;
      node.local.svgNF?.setName(newName);
    } else {
      // traffic port
      node.local.name = newName;
      this.updateEdgeTunnel(
        node.local.edges[0]?.local.svgEdge,
        node.port.tunnel
      );
    }
  }

  public get EdgeDirection(): typeof EdgeDirection {
    return EdgeDirection;
  }

  public onChangeEdgeDirection(direction: number): void {
    const edge: PipelineEdge = this.getEdgeByElement(this._activeEdge.local.svgEdge.element);
    const [aNode] = this.getNodeById(edge.a.node);
    const [bNode] = this.getNodeById(edge.b.node);
    const aGate: SVG.G = edge.a.interface ? this.getNFGate(edge.a.interface, aNode) : undefined;
    const bGate: SVG.G = edge.b.interface ? this.getNFGate(edge.b.interface, bNode) : undefined;
    const sourceInterface: SVG.G = aGate || aNode.port.local.svgPort.element;
    const destInterface: SVG.G = bGate || bNode.port.local.svgPort.element;
    this.removeEdge(edge);
    this.connectInterfaces(destInterface, sourceInterface, undefined, direction);

    this.closePopups();
  }

  private getNFGate(gateId: string, node: PipelineNode): SVG.G {
    const interfaces = node.nf.local.interfaces;
    return interfaces.find((inf: NFInterface) => inf.id === gateId).local.ctx;
  }

  public onRemoveEdgeGroup(e?: PipelineEdge): void {
    const pipelineEdge = e || this._activeEdge;
    pipelineEdge.local.svgEdge.element.off('click.edge');
    this._edgeGroups.remove(pipelineEdge.local.svgEdge.element);
    const edge = this.getEdgeByElement(pipelineEdge.local.svgEdge.element);
    this.removeEdge(edge);

    this.closePopups();
  }

  private removeEdge(edge: PipelineEdge) {
    const [aNode] = this.getNodeById(edge.a.node);
    const [bNode] = this.getNodeById(edge.b.node);

    if (aNode && bNode) {
      this.removeEdgePointer(edge, aNode);
      this.removeEdgePointer(edge, bNode);
      edge.local.svgEdge.element?.remove();
    }

    this._edges = this._edges.filter(e => e.local.id !== edge.local.id);
  }

  private openInstanceStatusPopup(instance: PipelineNfInstance) {
    const { instanceStatus } = this._popups;

    if (instanceStatus.isOpen && instanceStatus.selected === instance) {
      return;
    }

    const elem = new ElementRef<SVG.LinkedHTMLElement>(instance.local.statusIndicator.nativeElement.node);

    this._overlayOrigin = new CdkOverlayOrigin(elem);
    instanceStatus.isOpen = true;
    instanceStatus.selected = instance;
  }

  private closePopups(): void {
    this._popups.edge.isOpen = false;
    this._popups.node.isOpen = false;
    this._popups.port.isOpen = false;
    this._popups.instanceStatus.isOpen = false;
    this._activeEdge = undefined;
    this._popups.node.selected = undefined;
    this._popups.port.selected = undefined;
    this._popups.instanceStatus.selected = undefined;
  }

  public onBackdropClick(): void {
    let hasErrors = false;

    if (this._popups.node.isOpen || this._popups.port.isOpen) {
      hasErrors = !!this.nodeForm.get('name').errors;
    }

    if (!hasErrors) {
      this.closePopups();
    }
  }

  public isLegacyNF(node: PipelineNode): boolean {
    return node?.nf?.local?.type === NfType.THIRD_PARTY;
  }

  /**
   * Get filter edge direction
   *
   * @param edge pipeline edge
   * @param aToB if bi-directional, set to true to get filter for direction A -> B,
   * or false for B -> A
   * @param showInputValue show as an input value
   */
  public getEdgeFilter(
    edge: PipelineEdge,
    aToB?: boolean,
    showInputValue?: boolean
  ): string {
    let result: string;
    const { filterAb, filterBa } = edge;

    let abFilter = filterAb ? filterAb.bpf ?? 'ALL' : undefined;
    let baFilter = filterBa ? filterBa.bpf ?? 'ALL' : undefined;

    abFilter = (abFilter === '') ? undefined : abFilter;
    baFilter = (baFilter === '') ? undefined : baFilter;

    if ((abFilter && !baFilter) || (aToB === true)) {
      result = abFilter;
    } else if ((baFilter && !abFilter) || (aToB === false)) {
      result = baFilter;
    } else {
      if (abFilter === baFilter) {
        result = abFilter;
      } else {
        result = `${baFilter} ⟷ ${abFilter}`;
      }
    }

    result = result || 'ALL';

    if (showInputValue && result === 'ALL') {
      result = '';
    }

    return result;
  }

  public updateEdgeFilter(edge: PipelineEdge, newFilter: string, aToB = true) {
    const edgeFilter: string = this.getEdgeFilter(edge, aToB);
    const isFilterChanged: boolean = newFilter !== edgeFilter;

    if (!isFilterChanged) {
      return;
    }

    const line = (edge.local.svgEdge.element.select('polyline') as any).members[0] as SVG.PolyLine;
    const markerTspan = edge.local.svgEdge.element.select('.marker-text tspan').first() as SVG.Tspan;
    const markerArrow = edge.local.svgEdge.element.select('.marker-arrow').first();

    const isFilter: boolean = newFilter && newFilter !== 'ALL';

    if (isFilter) {
      if (edge.filterAb && edge.filterBa) {
        if (aToB && edge.filterAb) {
          edge.filterAb.bpf = newFilter;
        } else if (!aToB && edge.filterBa) {
          edge.filterBa.bpf = newFilter;
        }
      } else {
        if (edge.filterAb) {
          edge.filterAb.bpf = newFilter;
        }
        if (edge.filterBa) {
          edge.filterBa.bpf = newFilter;
        }
      }

      line.removeClass('edge').addClass('edge-filter');
      markerTspan.removeClass('label').addClass('label-filter');
      markerArrow.removeClass('edge').addClass('edge-filter');

    } else {
      if (edge.filterAb) {
        edge.filterAb.bpf = undefined;
      }
      if (edge.filterBa) {
        edge.filterBa.bpf = undefined;
      }

      line.removeClass('edge-filter').addClass('edge');
      markerTspan.removeClass('label-filter').addClass('label');
      markerArrow.removeClass('edge-filter').addClass('edge');
    }

    markerTspan.clear();
    markerTspan.text(this.getEdgeFilter(edge));
  }

  public enablePerformanceTrace(enable: boolean, perfTraceFilter?: string) {
    if (enable && (!this._isLogicalView || this._editMode)) {
      // can only enable when in logical view and not in edit mode
      return;
    }

    const updated = this._perfTrace.enabled !== enable;
    this._perfTrace.enabled = enable;

    if (perfTraceFilter) {
      this._perfTrace.filter = perfTraceFilter;
    }

    if (updated) {
      if (enable) {
        this.startPerformanceStatStream();
      } else {
        this.stopPerformanceStatStream();

        this._pipelineService.deletePipelineBpf(this._tenant, this._pipelineId).subscribe(() => {
          // TODO(eric): handle error case
        });
      }

      this.showNFLopStats();
      this.togglePerfTrace.emit(enable);
    }
  }

  private showNFLopStats() {
    this._nfs.forEach((nf: PipelineNode) => {
      (nf.local.ctx?.select('.nf-rect') as any)?.members.forEach((nfRect: SVG.Rect) => {
        nfRect.height(this._perfTrace.enabled ? Config.NF.lgHeight : Config.NF.smHeight);
      });
      const nfPorts: NFInterface[] = nf.nf.local.interfaces;
      const portsCount = nfPorts?.length;
      nfPorts?.forEach((nfPort: NFInterface, index) => {
        if (nfPort?.local?.ctx) {
          const settings: NFPortTranslateSettings = nf.local.svgNF?.getPortTranslateSettings(index, portsCount);
          nfPort.local.ctx.translate(settings.dx, settings.dy);
        }
      });
    });

    (SVG.select('.nf-instance.main-instance') as any).members.forEach((nf: SVG.G) => {
      if (this._perfTrace.enabled) {
        nf.translate(0, -(Config.NF.lgHeight - Config.NF.smHeight));
      } else {
        nf.translate(0, 0);
      }
    });

    (SVG.select('.lop-stats') as any).members.forEach((nf: SVG.G) => {
      if (this._perfTrace.enabled) {
        nf.animate().attr({ opacity: 1 });
      } else {
        nf.attr({ opacity: 0 });
      }
    });
  }

  private updatePipelineBpf(bpf: string) {
    if (this._perfTrace.enabled) {
      this._pipelineBpfSubscription = this._pipelineService.postPipelineBpf(
        this._tenant,
        this._pipelineId,
        bpf ? bpf : ' '
      ).subscribe(() => {
      },
        () => this._alert.error(AlertType.ERROR_START_STOP_PERFORMANCE_TRACE));
    }
  }

  private startPerformanceStatStream(skipSetup = false) {
    if (!skipSetup) {
      // if there's already a current stream, stop it before starting a new one
      this.stopPerformanceStatStream();
      this.updatePipelineBpf(this._perfTrace.filter);
    }
    this._perfStatStreamSubscription = this._pipelineService
      .streamTenantPipelineStats(this._tenant, this._pipelineId, Config.POLL_INTERVAL)
      .subscribe((stats: PipelineNfStats) => {
        const nfs = Object.keys(stats);
        const maxFixedValue = 1.0e+6;
        nfs.forEach((nfName: string) => {
          const fractionalLength = 2;
          // TODO(anton): remove this after the "user_" prefix will get removed on the backend.
          let nfNode = this._nfs.get(nfName);
          if (!nfNode) {
            const name = nfName.split('user_')[1];
            nfNode = this._nfs.get(name);
          }
          const throughputVal = stats[nfName].throughput ? stats[nfName].throughput : 0;
          const throughput = this._converter.bitsPerSeconds(throughputVal, fractionalLength);
          const latencyVal = stats[nfName].latency ? stats[nfName].latency : 0;
          const latency = this._converter.nanoSeconds(latencyVal, fractionalLength);
          const lossVal = stats[nfName].loss ? stats[nfName].loss : 0;
          const loss = this._converter.packetsPerSeconds(lossVal, fractionalLength);

          const latencyValue = latency[0] > maxFixedValue ? latency[0].toExponential(0) : latency[0];
          const throughputLabel = throughput[0] > maxFixedValue ? throughput[0].toExponential(0) : throughput[0];
          const lossLabel = loss[0] > maxFixedValue ? loss[0].toExponential(0) : loss[0];

          nfNode.local.lop.latency.updateLabel(`${latencyValue}${latency[1]}`);
          nfNode.local.lop.throughput.updateLabel(`${throughputLabel}${throughput[1]}`);
          nfNode.local.lop.loss.updateLabel(`${lossLabel}${loss[1]}`);
        });
      }, (error: Error) => {
        this._log.error(error);
        setTimeout(() => {
          this.startPerformanceStatStream(true);
        }, Config.POLL_INTERVAL);
      });
  }

  public get perfStatStreamSubscription$(): Subscription {
    return this._perfStatStreamSubscription;
  }

  private stopPerformanceStatStream() {
    if (this._pipelineBpfSubscription) {
      this._pipelineBpfSubscription.unsubscribe();
      this._pipelineBpfSubscription = undefined;
    }

    if (this._perfStatStreamSubscription) {
      this._perfStatStreamSubscription.unsubscribe();
      this._perfStatStreamSubscription = undefined;
    }
  }

  private compareNfInstances(nfNode: PipelineNode, curInstances: { [key: string]: InstanceStatus; }) {
    const prevInstances = nfNode.nf.local.instances || [];

    // look for newly scaled instances
    if (curInstances) {
      let index = 0;
      for (const key of Object.keys(curInstances)) {
        const checkInstance: PipelineNfInstance = curInstances[key];
        const found = prevInstances.find((prevInstance: PipelineNfInstance) =>
          checkInstance.identity.instanceId === prevInstance.identity.instanceId);

        if (!found) {
          // new instance
          checkInstance.local = { key };
          this.scaleUpNf(nfNode, checkInstance, index);
        }

        index++;
      }
    }

    // look for old instances
    if (prevInstances) {
      const curInstancesValues = curInstances ? Object.values(curInstances) : [];
      for (const prevInstance of prevInstances) {
        const found = curInstancesValues.find((instance: InstanceStatus) =>
          prevInstance.identity.instanceId === instance.identity.instanceId);

        if (!found) {
          // old instance
          this.scaleDownNf(nfNode, prevInstance);
        }
      }
    }
  }

  private updateNfInstanceStatus(nfNode: PipelineNode, curInstances: { [key: string]: InstanceStatus; }) {
    if (!nfNode.nf.local.instances) {
      return;
    }

    for (const instance of nfNode.nf.local.instances) {
      const newInstanceStatus = curInstances[instance.local.key];
      switch (newInstanceStatus.state) {
        case InstanceStatusState.FAILED:
          const message = instance?.nfcStatus?.errorMessage ?? 'Instance failed to launch';
          instance.local.statusIndicator.errorTooltip.text(message);
          instance.local.statusIndicator.showError();
          break;
        case InstanceStatusState.CONFIGURED:
          instance.local.statusIndicator.showSuccess();
          instance.local.statusIndicator.errorTooltip.text('');
          break;
        default:
          instance.local.statusIndicator.showInProgress();
          instance.local.statusIndicator.errorTooltip.text('');
      }

      instance.placementStatus = newInstanceStatus.placementStatus;
      instance.launchStatus = newInstanceStatus.launchStatus;
      instance.state = newInstanceStatus.state;
    }
  }

  private startPipelineStatusStream() {
    this.stopPipelineStatusStream();
    this._statusSubscription = this._pipelineService.streamPipelineStatus(
      this._tenant.identifier, this._pipelineId).subscribe(
        (status: ServiceStatus) => {
          for (const key of Object.keys(status.nodes)) {
            const nfNode = this._nfs.get(key);
            const statusNode = status.nodes[key];
            this.collectServers(statusNode);
            this.compareNfInstances(nfNode, statusNode.instances);
            this.updateNfInstanceStatus(nfNode, statusNode.instances);
          }

          this._statusTimer = window.setTimeout(() => {
            this.startPipelineStatusStream();
          }, Config.STATUS_POLL_INTERVAL);
        }, (error: HttpErrorResponse) => {

          this._log.error(error);
          if (error.status === HttpError.NotFound) {
            this.stopPipelineStatusStream();
          } else {
            this._statusTimer = window.setTimeout(() => {
              this.startPipelineStatusStream();
            }, Config.STATUS_POLL_INTERVAL);
          }

        });
  }

  private stopPipelineStatusStream() {
    if (this._statusSubscription) {
      this._statusSubscription.unsubscribe();
    }

    if (this._statusTimer) {
      clearTimeout(this._statusTimer);
    }
  }

  public toggleView(mode: string) {
    const orig = this._isLogicalView;

    this._isLogicalView = (mode === 'logical');

    const updated = (orig !== this._isLogicalView);

    if (updated) {
      if (this._isLogicalView) {
        this.stopPipelineStatusStream();
      } else {
        this.startPipelineStatusStream();
        this.enablePerformanceTrace(false);
      }

      (SVG.select('.scaled-instance') as any).members.forEach((nf: SVG.G) => {
        nf.animate().attr({ opacity: this._isLogicalView ? 0 : 1 });
      });

      this._nfs.forEach((node: PipelineNode) => {
        if (node.local.svgNF) {
          node.local.svgNF.options.isLogicalView = this._isLogicalView;
        }

        if (node.nf.local.instances) {
          node.nf.local.instances.forEach((instance: PipelineNfInstance) => {
            if (this._isLogicalView) {
              instance.local.statusIndicator.hideCurrentStatus();
            } else {
              instance.local.statusIndicator.showCurrentStatus();
            }
          });
        }

        if (node.local.statusIndicator) {
          if (this._isLogicalView) {
            node.local.statusIndicator.hideCurrentStatus();
          } else {
            node.local.statusIndicator.showCurrentStatus();
          }
        }
      });
    }
  }

  public modifyPerfTraceFilter() {
    const dialogRef = this._dialog.open(InputDialogComponent, {
      width: '384px',
      data: {
        title: 'BPF Filter',
        placeholder: 'Enter filter',
        value: this._perfTrace.filter || ''
      }
    });

    dialogRef.afterClosed().subscribe((bpf) => {
      if (this._perfTrace.filter !== bpf) {
        this._perfTrace.filter = bpf;
        this.updatePipelineBpf(bpf);
      }
    });
  }

  public getOptimalCanvasHeight(): number {
    const numSites = this._sitesNodesMap?.size;
    const rows = numSites ? Math.ceil(numSites / 2) : 1;
    return (Config.SiteGroup.height + (Config.SiteGroup.padding * 2)) * rows;
  }

  public getSiteType(siteId: string): SiteConfigSiteType {
    return this._sites.find(({ config }: Site) => config.identifier === siteId)?.config?.siteType;
  }

  public get isLogicalView(): boolean {
    return this._isLogicalView;
  }

  public get perfTrace(): PerformanceTrace {
    return this._perfTrace;
  }

  public get perfTraceLegend(): LegendItem[] {
    return this._perfTraceLegend;
  }

  public get editMode(): boolean {
    return this._editMode;
  }

  public get isDraft(): boolean {
    return this._isDraft;
  }

  public set isDraft(val: boolean) {
    this._isDraft = val;
  }

  public get sites(): Site[] {
    return this._sites;
  }

  public get InstanceStatusState(): typeof InstanceStatusState {
    return InstanceStatusState;
  }

  public get isInactiveDraft(): boolean {
    return this._isInactiveDraft;
  }

  public get isInactiveDraft$(): Observable<boolean> {
    return this._isInactiveDraft$;
  }

  public get initialGraph(): PipelineGraph | ServiceConfigTemplate {
    return this._initialGraph;
  }

  public get popups(): Popup {
    return this._popups;
  }

  public get overlayOrigin(): CdkOverlayOrigin {
    return this._overlayOrigin;
  }

  public get edgeConnectPositions(): ConnectedPosition[] {
    return Config.Edge.popupConnectPosition;
  }

  public get nfConnectPositions(): ConnectedPosition[] {
    return Config.NF.popupConnectPosition;
  }

  public get instanceStatusConnectPositions(): ConnectedPosition[] {
    return Config.NF.status.popupConnectPosition;
  }

  public get activeEdge(): PipelineEdge {
    return this._activeEdge;
  }

  public get showHeader(): boolean {
    return this._template === undefined;
  }

  public isEmpty(): boolean {
    return this._sitesNodesMap.size === 0;
  }

  public clear() {
    this._sitesNodesMap.clear();
    this._nfs?.clear();
    this._ports?.clear();
    this._edges = [];
    this._view?.clear();
    this._edgeGroups?.clear();
  }
}
