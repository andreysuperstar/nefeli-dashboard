import { SvgPill } from './svg-elements/svg-pill';
import { NFInterface, NetworkFunction, getNFInterfaces } from './network-function.model';
import { InstanceStatus } from 'rest_client/pangolin/model/instanceStatus';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { NFType } from 'rest_client/pangolin/model/nFType';
import { Edge } from 'rest_client/pangolin/model/edge';
import { ServiceConfigNode } from 'rest_client/pangolin/model/serviceConfigNode';
import { ServiceConfigNodePort } from 'rest_client/pangolin/model/serviceConfigNodePort';
import { NF } from 'rest_client/pangolin/model/nF';
import { ServiceConfig } from 'rest_client/pangolin/model/serviceConfig';
import { SvgStatusDot } from './svg-elements/svg-status-dot';
import { cloneDeep } from 'lodash-es';
import * as SVG from 'svg.js';
import { SvgNetworkFunction } from './svg-elements/svg-network-function';
import { SvgTrafficPort } from './svg-elements/svg-traffic-port';
import { SvgEdge } from './svg-elements/svg-edge';
import { ServiceConfigTemplateNode } from 'rest_client/pangolin/model/serviceConfigTemplateNode';
import { NFTemplate } from 'rest_client/pangolin/model/nFTemplate';
import { ServiceConfigTemplate } from 'rest_client/pangolin/model/serviceConfigTemplate';

export interface Pipeline extends ServiceConfig {
  hasDraft?: boolean;
  selected?: boolean;
}

export const SERVICE_AGGREGATE = '-1';

export const SERVICE_TOTAL: Pipeline = {
  identifier: SERVICE_AGGREGATE,
  name: 'Aggregate',
  selected: true
};

export type PipelineNodes = { [key: string]: PipelineNode; };
export type TemplateNodes = { [key: string]: TemplateNode; };

export interface PipelineGraph extends ServiceConfig, ServiceConfigTemplate {
  edges?: Array<PipelineEdge>;
  nodes?: PipelineNodes & TemplateNodes;
}
export interface PipelineEdge extends Edge {
  a?: {
    interface?: string;
    node?: string;
  };
  b?: {
    interface?: string;
    node?: string;
  };
  local?: {
    id?: string;
    status?: PipelineNodeStatus;
    svgEdge?: SvgEdge;
    coordinates?: PathCoordinates;
    dragCoordinates?: PathCoordinates;
  };
}

export interface PipelineTrafficPort extends ServiceConfigNodePort {
  local?: {
    svgPort?: SvgTrafficPort;
    topBorderPattern?: SVG.G;
  };
}

interface LocalNode {
  local?: {
    id?: string;
    name?: string;
    statusIndicator?: SvgStatusDot;
    edges?: Array<PipelineEdge>;
    worldEdge?: SVG.G;
    ctx?: SVG.G;
    coordinates?: Coordinates;
    status?: PipelineNodeStatus;
    defs?: Array<any>;
    lop?: {
      throughput?: SvgPill;
      latency?: SvgPill;
      loss?: SvgPill;
    };
    isAdded?: boolean;
    connectingNodes?: Array<string>;
    svgNF?: SvgNetworkFunction;
  };
}

export interface PipelineNode extends ServiceConfigNode, LocalNode {
  nf?: PipelineNF;
  port?: PipelineTrafficPort;
}

export interface TemplateNode extends ServiceConfigTemplateNode, LocalNode {
  nf?: TemplateNF;
  port?: PipelineTrafficPort;
}

export enum NfType {
  NATIVE = 'NATIVE',
  THIRD_PARTY = 'THIRD_PARTY'
}

interface NFLocal {
  local?: {
    vendor?: string;
    name?: string;
    config?: NFCConfig;
    interfaces?: NFInterface[];
    instances?: Array<PipelineNfInstance>;
    type?: NFType;
  };
}

export interface PipelineNF extends NF, NFLocal { }
export interface TemplateNF extends NFTemplate, NFLocal { }

export interface PortProperties {
  ctx?: any;
}

export interface PipelineNfInstance extends InstanceStatus {
  local?: {
    key?: string;
    statusIndicator?: SvgStatusDot;
    topBorderPattern?: any;
    ctx?: any;
    id?: string;
  };
}

export interface PathCoordinates {
  a: [number, number];
  b: [number, number];
}

export enum PipelineNodeStatus {
  UNKNOWN,
  NEW,
  CURRENT
}

export interface Coordinates {
  x: number;
  y: number;
}

export interface PipelineNfStats {
  [key: string]: {
    throughput?: number;
    latency?: number;
    loss?: number;
  };
}

export const createPipelineNode = (nf: NetworkFunction, id: string, name: string, coordinates?: Coordinates): PipelineNode => {
  const newNode: PipelineNode = {
    nf: {
      catalogId: nf.local.catalogId,
      local: {
        type: nf.type,
        vendor: nf.local.vendor,
        interfaces: cloneDeep(getNFInterfaces(nf)),
        name,
        config: {},
        instances: []
      }
    },
    local: {
      id,
      name,
      coordinates,
      status: PipelineNodeStatus.NEW,
      isAdded: true
    }
  };

  return newNode;
};
