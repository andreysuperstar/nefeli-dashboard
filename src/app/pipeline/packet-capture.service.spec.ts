import { TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PacketCaptureRequestBackend } from 'rest_client/pangolin/model/packetCaptureRequestBackend';
import { PacketCaptureRequestDirection } from 'rest_client/pangolin/model/packetCaptureRequestDirection';
import { PacketCaptureRequestTcpdumpInterpret } from 'rest_client/pangolin/model/packetCaptureRequestTcpdumpInterpret';
import { PacketCaptureRequest } from 'rest_client/pangolin/model/packetCaptureRequest';

import { BASE_PATH } from 'rest_client/pangolin/variables';
import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { Format, PacketCaptureService } from './packet-capture.service';

describe('PacketCaptureService', () => {
  let service: PacketCaptureService;
  let httpTestingController: HttpTestingController;

  const mockTenant = 'andrew';

  const mockPacketCaptureRequest: PacketCaptureRequest = {
    appendCmd: '-v',
    direction: PacketCaptureRequestDirection.OUT,
    instance: 'arista_1',
    interpret: PacketCaptureRequestTcpdumpInterpret.NOINTERPRET,
    node: 'arista',
    packetLimit: 50,
    port: 'right',
    service: 'test',
    sizeLimit: 100
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(PacketCaptureService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should request pcap file', () => {
    const mockPacketCaptureRequest: PacketCaptureRequest = {
      appendCmd: '-v -s 0 -A',
      backend: PacketCaptureRequestBackend.TCPDUMP,
      direction: PacketCaptureRequestDirection.OUT,
      instance: 'arista_1',
      interpret: PacketCaptureRequestTcpdumpInterpret.NOINTERPRET,
      node: 'arista',
      packetLimit: 25,
      port: 'right',
      service: 'test',
      sizeLimit: 100
    };

    const mockResponseData = 'streamofbytes';

    service
      .trace(mockTenant, mockPacketCaptureRequest, Format.PCAP)
      .subscribe(({ size }: Blob) => {
        expect(size).toBe(mockResponseData.length);
      });

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    const { appendCmd, backend, instance, direction, interpret, node, packetLimit, port, sizeLimit } = mockPacketCaptureRequest;

    expect(packetCaptureRequest.request.body.appendCmd).toBe(appendCmd);
    expect(packetCaptureRequest.request.body.backend).toBe(backend);
    expect(packetCaptureRequest.request.body.instance).toBe(instance);
    expect(packetCaptureRequest.request.body.direction).toBe(direction);
    expect(packetCaptureRequest.request.body.interpret).toBe(interpret);
    expect(packetCaptureRequest.request.body.node).toBe(node);
    expect(packetCaptureRequest.request.body.packetLimit).toBe(packetLimit);
    expect(packetCaptureRequest.request.body.port).toBe(port);
    expect(packetCaptureRequest.request.body.service).toBe(mockPacketCaptureRequest.service);
    expect(packetCaptureRequest.request.body.sizeLimit).toBe(sizeLimit);

    packetCaptureRequest.flush(new Blob([mockResponseData]));
  });

  it('should request tcpdump capture', () => {
    const mockPacketCaptureRequest: PacketCaptureRequest = {
      appendCmd: '-v',
      backend: PacketCaptureRequestBackend.TCPDUMP,
      direction: PacketCaptureRequestDirection.IN,
      instance: 'filter_0',
      interpret: PacketCaptureRequestTcpdumpInterpret.NOINTERPRET,
      node: 'filter',
      packetLimit: 50,
      port: 'left',
      service: 'sample',
      sizeLimit: 25,
      timeLimit: 30
    };

    const mockTCPDUMP = `17:54:45.818593 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:45.918741 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.018837 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.118959 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.219086 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.319208 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.419337 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.519446 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.619560 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
17:54:46.719694 IP 10.0.0.1.12345 > 10.0.0.2.80: UDP, length 22
Running: tcpdump -r /tmp/tmpbOgWLr -c 10 -B 10`;

    const mockResponse = new Blob([mockTCPDUMP]);

    service
      .trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP)
      .subscribe(response => {
        expect(response).toBe(mockResponse);
      });

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    const { appendCmd, backend, instance, direction, interpret, node, packetLimit, port, sizeLimit, timeLimit } = mockPacketCaptureRequest;

    expect(packetCaptureRequest.request.body.appendCmd).toBe(appendCmd);
    expect(packetCaptureRequest.request.body.backend).toBe(backend);
    expect(packetCaptureRequest.request.body.instance).toBe(instance);
    expect(packetCaptureRequest.request.body.direction).toBe(direction);
    expect(packetCaptureRequest.request.body.interpret).toBe(interpret);
    expect(packetCaptureRequest.request.body.node).toBe(node);
    expect(packetCaptureRequest.request.body.packetLimit).toBe(packetLimit);
    expect(packetCaptureRequest.request.body.port).toBe(port);
    expect(packetCaptureRequest.request.body.service).toBe(mockPacketCaptureRequest.service);
    expect(packetCaptureRequest.request.body.sizeLimit).toBe(sizeLimit);
    expect(packetCaptureRequest.request.body.timeLimit).toBe(timeLimit);

    packetCaptureRequest.flush(mockResponse);
  });

  it('should request tshark capture', () => {
    const mockPacketCaptureRequest: PacketCaptureRequest = {
      backend: PacketCaptureRequestBackend.TSHARK,
      direction: PacketCaptureRequestDirection.IN,
      instance: 'pan_1',
      interpret: PacketCaptureRequestTcpdumpInterpret.NOINTERPRET,
      node: 'pan',
      packetLimit: 50,
      port: 'left',
      service: 'pipeline',
      sizeLimit: 25,
      timeLimit: 100
    };

    const mockTSHARK = 'streamofbytes';
    const mockResponse = new Blob([mockTSHARK]);

    service
      .trace(mockTenant, mockPacketCaptureRequest, Format.TSHARK)
      .subscribe(response => {
        expect(response).toBe(mockResponse);
      });

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    const { backend, instance, direction, interpret, node, packetLimit, port, sizeLimit, timeLimit } = mockPacketCaptureRequest;

    expect(packetCaptureRequest.request.body.backend).toBe(backend);
    expect(packetCaptureRequest.request.body.instance).toBe(instance);
    expect(packetCaptureRequest.request.body.direction).toBe(direction);
    expect(packetCaptureRequest.request.body.interpret).toBe(interpret);
    expect(packetCaptureRequest.request.body.node).toBe(node);
    expect(packetCaptureRequest.request.body.packetLimit).toBe(packetLimit);
    expect(packetCaptureRequest.request.body.port).toBe(port);
    expect(packetCaptureRequest.request.body.service).toBe(mockPacketCaptureRequest.service);
    expect(packetCaptureRequest.request.body.sizeLimit).toBe(sizeLimit);
    expect(packetCaptureRequest.request.body.timeLimit).toBe(timeLimit);

    packetCaptureRequest.flush(mockResponse);
  });

  it('should be able to create and get trace request', () => {
    const request = service.trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP);

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    const savedRequest = service.getTrace(mockPacketCaptureRequest);

    expect(savedRequest).toEqual(request);

    const mockResponse = new Blob(['']);

    packetCaptureRequest.flush(mockResponse);
  });

  it('should be possible to cancel request', () => {
    service.trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP);

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    service.cancel(mockPacketCaptureRequest);

    const savedRequest = service.getTrace(mockPacketCaptureRequest);

    expect(savedRequest).toBeUndefined();
    expect(packetCaptureRequest.cancelled).toBeTruthy();
  });

  it('should return same request if one is already in progress', () => {
    const request = service.trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP);
    const request2 = service.trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP);

    const packetCaptureRequests = httpTestingController.match({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    expect(request).toEqual(request2);

    const mockResponse = new Blob(['']);

    packetCaptureRequests[0].flush(mockResponse);
    packetCaptureRequests[1].flush(mockResponse);
  });

  it('should create different requests for different packet capture requests', () => {
    service.trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP);

    service.trace(
      mockTenant,
      {
        ...mockPacketCaptureRequest,
        port: 'left'
      },
      Format.TCPDUMP
    );

    const packetCaptureRequests = httpTestingController.match({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    expect(packetCaptureRequests.length).toBe(2);

    const mockResponse = new Blob(['']);

    packetCaptureRequests[0].flush(mockResponse);
    packetCaptureRequests[1].flush(mockResponse);
  });

  it('should be possible to cancel separate requests', () => {
    service.trace(mockTenant, mockPacketCaptureRequest, Format.TCPDUMP);

    service.trace(
      mockTenant,
      {
        ...mockPacketCaptureRequest,
        port: 'left'
      },
      Format.TCPDUMP
    );

    service.cancel(mockPacketCaptureRequest);

    const packetCaptureRequests = httpTestingController.match({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockTenant}/services/${mockPacketCaptureRequest.service}/packetcapture`
    });

    expect(packetCaptureRequests[0].cancelled).toBeTruthy();
    expect(packetCaptureRequests[1].cancelled).toBeFalsy();

    const mockResponse = new Blob(['']);

    packetCaptureRequests[1].flush(mockResponse);
  });
});
