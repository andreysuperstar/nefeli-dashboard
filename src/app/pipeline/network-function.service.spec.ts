import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { environment } from '../../environments/environment';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { NetworkFunctionService } from './network-function.service';
import { NFCatalog, NetworkFunction, NFCatalogResponse } from './network-function.model';
import { Cluster } from '../home/cluster.service';
import { TenantService } from '../tenant/tenant.service';
import { ClusterService } from '../home/cluster.service';
import { AlertService } from '../shared/alert.service';
import { NfType } from './pipeline.model';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';

describe('NetworkFunctionService', () => {
  let service: NetworkFunctionService;
  let httpMock: HttpTestingController;

  const mockCluster: Cluster[] = [
    { id: '-1', name: 'Cluster A' }
  ];

  const mockClusters: Cluster[] = [
    { id: '-1', name: 'Cluster 1' },
    { id: '-2', name: 'Cluster 2' }
  ];

  const mockTenantNFs: RESTNFCatalog = {
    descriptors: [{
      identifier: "pan-fw",
      name: "Firewall",
      type: NfType.THIRD_PARTY,
      controllerType: "NF_PROXY",
      components: {
        datapath: {
          name: "",
          vendor: "Palo Alto Networks",
          type: "CONTROLLER",
          bessManifest: undefined,
          execManifest: null,
          cpuCores: "0",
          memoryMb: "0"
        }
      }
    }]
  };

  const mockClusterNFs: RESTNFCatalog = {
    descriptors: [
      {
        identifier: "pan-fw",
        name: "Firewall",
        type: NfType.THIRD_PARTY,
        components: {
          datapath: {
            name: "",
            vendor: "Palo Alto Networks",
            type: "CONTROLLER",
            bessManifest: undefined,
            execManifest: null,
            cpuCores: "0",
            memoryMb: "0"
          }
        }
      },
      {
        identifier: "pan-bw",
        name: "Firewall 2",
        type: NfType.THIRD_PARTY,
        controllerType: "NF_PROXY",
        components: {
          datapath: {
            name: "",
            vendor: "Palo Alto Networks",
            type: "CONTROLLER",
            bessManifest: undefined,
            execManifest: null,
            cpuCores: "0",
            memoryMb: "0"
          }
        }
      }
    ]
  };

  const mockClusterNFs1: RESTNFCatalog = {
    descriptors: [
      {
        identifier: "pan-fw",
        name: "Firewall",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: {
          datapath: {
            name: "",
            vendor: "pan",
            type: "CONTROLLER",
            bessManifest: undefined,
            execManifest: null,
            cpuCores: "0",
            memoryMb: "0"
          }
        }
      }
    ]
  };

  const mockClusterNFs2: RESTNFCatalog = {
    descriptors: [
      {
        identifier: "f5-bigip",
        name: "Big IP",
        type: NfType.THIRD_PARTY,
        controllerType: "NF_CONTROLLER",
        components: {
          datapath: {
            name: "",
            vendor: "f5",
            type: "CONTROLLER",
            bessManifest: undefined,
            execManifest: null,
            cpuCores: "0",
            memoryMb: "0"
          }
        }
      }
    ]
  };

  const mockClusterNFs12: RESTNFCatalog = {
    descriptors: [
      {
        identifier: "pan-bw",
        name: "Firewall test",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: {
          datapath: {
            name: "",
            vendor: "pan",
            type: "CONTROLLER",
            bessManifest: undefined,
            execManifest: null,
            cpuCores: "0",
            memoryMb: "0"
          }
        }
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      providers: [
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        NetworkFunctionService,
        TenantService,
        ClusterService,
        AlertService,
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    })

    service = TestBed.inject(NetworkFunctionService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  xit('should be able to filter nfs for available catalog', fakeAsync(() => {
    const tenantId = "-1";
    // service.getAvailableNFs(tenantId).subscribe((catalog: NFCatalog) => {
    //   const vendors = Object.keys(catalog);
    //   expect(vendors.length).toBe(1);
    //   const vendor = vendors[0];
    //   expect(vendor).toBe('Palo Alto Networks');
    //   expect(catalog[vendor].length).toBe(2);
    //   const nF: NetworkFunction = catalog[vendor][0];
    //   expect(nF.local.id).toBe('pan-fw');
    // });

    const res1 = httpMock.expectOne(`${environment.restServer}/v1/tenants/-1/nfs`);
    res1.flush(mockTenantNFs);
    tick(100);
    const res3 = httpMock.expectOne(`${environment.restServer}/v1/nfs`);
    res3.flush(mockClusterNFs);
  }));

  it('should get clusters nfs', () => {
    service.getSystemNFs().subscribe((catalog: NFCatalog) => {
      const vendors = Object.keys(catalog);
      expect(vendors.length).toBe(1);
      expect(vendors[0]).toBe('pan');
    });

    const req1 = httpMock.expectOne(`${environment.restServer}/v1/nfs`);
    req1.flush(mockClusterNFs1)
  });

  it('should post nf', () => {
    service.postNf('nf manifest data').subscribe();
    const req = httpMock.expectOne(`${environment.restServer}/v1/nfs`);
    expect(req.request.method).toBe('POST');
    expect(req.request.headers.get('Content-type')).toBe('application/x-yaml');
    expect(req.request.body).toBe('nf manifest data');
  });

  it('should post nf config', () => {
    service.postNfConfig('9', 'myPipeline', 'myNF', {
      configTemplate: 'nf-config-template',
      mapping: 'nf-mapping',
      filter: 'nf-filter',
      rollback: 'nf-rollback'
    } as NFCConfig).subscribe((res: any) => {
      expect(res.status).toBe('success');
    });

    const req = httpMock.expectOne((r: HttpRequest<any>) => {
      const url = `${environment.restServer}/v1/tenants/9/services/myPipeline/nodes/myNF/config`;
      return r.url === url;
    });
    expect(req.request.body.mapping).toBe('nf-mapping');
    expect(req.request.body.rollback).toBe('nf-rollback');
    expect(req.request.body.configTemplate).toBe('nf-config-template');
    expect(req.request.body.filter).toBe('nf-filter');

    req.flush({
      status: 'success'
    });
  });
});
