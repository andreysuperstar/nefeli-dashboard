import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NetworkFunctionMenuComponent } from './network-function-menu.component';
import { PipelineModule } from './pipeline.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { ClusterService } from '../home/cluster.service';
import { NFCatalog } from './network-function.model';
import { NfType } from './pipeline.model';

describe('NetworkFunctionMenuComponent', () => {
  let component: NetworkFunctionMenuComponent;
  let fixture: ComponentFixture<NetworkFunctionMenuComponent>;
  let httpTestingController: HttpTestingController;
  let el: HTMLElement;

  const mockNfCatalog: NFCatalog = {
    "Nefeli Networks": [
      {
        local: {
          id: "nat",
          vendor: "Nefeli Networks",
          catalogId: "nat",
          capability: undefined
        },
        name: "NAT",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: undefined
      },
      {
        local: {
          id: "exact_match",
          vendor: "Nefeli Networks",
          catalogId: "exact_match",
          capability: undefined
        },
        name: "Exact Match",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: undefined
      }
    ],
    "F5": [
      {
        local: {
          id: "legacy_bigip",
          vendor: "F5",
          catalogId: "legacy_bigip",
          capability: undefined
        },
        name: "BIG-IP",
        type: NfType.NATIVE,
        controllerType: "NF_CONTROLLER",
        components: undefined
      }
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        PipelineModule,
        NoopAnimationsModule,
        HttpClientTestingModule
      ],
      providers: [
        ClusterService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 2 })
          }
        }
      ]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkFunctionMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get nf catalog on input', () => {
    component.catalog = mockNfCatalog;

    const vendors = component.vendors;
    expect(vendors[0]).toBe('Nefeli Networks');
    expect(vendors[1]).toBe('F5');

    let nfs = component.getNfs(vendors[0]);
    expect(nfs[0].local.id).toBe('nat');
    expect(nfs[0].name).toBe('NAT');
    expect(nfs[0].local.catalogId).toBe('nat');
    expect(nfs[0].local.vendor).toBe('Nefeli Networks');

    expect(nfs[1].local.id).toBe('exact_match');
    expect(nfs[1].name).toBe('Exact Match');
    expect(nfs[1].local.catalogId).toBe('exact_match');
    expect(nfs[1].local.vendor).toBe('Nefeli Networks');

    nfs = component.getNfs(vendors[1]);
    expect(nfs[0].local.id).toBe('legacy_bigip');
    expect(nfs[0].name).toBe('BIG-IP');
    expect(nfs[0].local.catalogId).toBe('legacy_bigip');
    expect(nfs[0].local.vendor).toBe('F5');
  });

  it('should render the menu', () => {
    component.catalog = mockNfCatalog;

    fixture.detectChanges();

    expect(el.querySelectorAll('mat-expansion-panel').length).toBe(2);
    expect(el.querySelectorAll('mat-panel-title')[0].textContent).toBe('Nefeli Networks');
    expect(el.querySelectorAll('mat-panel-title')[1].textContent).toBe('F5');
    expect(el.querySelectorAll('mat-expansion-panel > .mat-expansion-panel-content')[0].querySelectorAll('nef-network-function').length).toBe(2);
    expect(el.querySelectorAll('mat-expansion-panel > .mat-expansion-panel-content')[1].querySelectorAll('nef-network-function').length).toBe(1);
  });

  it('should render custom title', () => {
    const defaultTitle = 'NETWORK FUNCTIONS';
    const customTitle = 'Test';
    expect(el.querySelector('.title').textContent.trim()).toBe(defaultTitle);
    component.title = customTitle;
    fixture.detectChanges();
    expect(el.querySelector('.title').textContent.trim()).toBe(customTitle);
  })
});
