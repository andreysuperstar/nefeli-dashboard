import { Component, Input, Output, EventEmitter } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpError } from 'src/app/error-codes';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { filter, switchMap } from 'rxjs/operators';
import { NetworkFunction, GetNFColor } from './network-function.model';
import { DragEventData } from '../service-designer/service-designer/service-designer.component';
import { Tenant } from '../tenant/tenant.service';
import { ConfirmationDialogComponent, ConfirmationDialogData } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { NetworkFunctionService } from './network-function.service';
import { AlertService, AlertType } from '../shared/alert.service';

const DIALOG_CONFIG: MatDialogConfig<ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-network-function',
  templateUrl: './network-function.component.html',
  styleUrls: ['./network-function.component.less']
})
export class NetworkFunctionComponent {
  private _vendor: string;
  private _nf: NetworkFunction;
  private _draggable = false;
  private _displayTenant: boolean;
  private _showEdit = false;
  private _showActivate = false;

  private _handleDragStart: EventEmitter<DragEventData> = new EventEmitter();
  private _editClick: EventEmitter<NetworkFunction> = new EventEmitter();
  private _activate: EventEmitter<NetworkFunction> = new EventEmitter();
  private _remove: EventEmitter<NetworkFunction> = new EventEmitter();

  constructor(
    private _dialog: MatDialog,
    private nFService: NetworkFunctionService,
    private _alertService: AlertService
  ) { }

  @Output() public get handleDragStart(): EventEmitter<DragEventData> {
    return this._handleDragStart;
  }

  @Output() public get editClick(): EventEmitter<NetworkFunction> {
    return this._editClick;
  }

  @Output() public get activate(): EventEmitter<NetworkFunction> {
    return this._activate;
  }

  @Output() public get remove(): EventEmitter<NetworkFunction> {
    return this._remove;
  }

  @Input() public set draggable(value: boolean) {
    this._draggable = value;
  }

  @Input('vendor')
  public set vendor(val: string) {
    this._vendor = val;
  }

  @Input('nf')
  public set nf(n: NetworkFunction) {
    this._nf = n;
  }

  @Input('displayTenant')
  public set displayTenant(display: boolean) {
    this._displayTenant = display;
  }

  @Input() public set showEdit(value: boolean) {
    this._showEdit = value;
  }

  @Input() public set showActivate(value: boolean) {
    this._showActivate = value;
  }

  public get displayTenant(): boolean {
    return this._displayTenant;
  }

  public get vendor(): string {
    return this._vendor;
  }

  public get nf(): NetworkFunction {
    return this._nf;
  }

  public get color(): string {
    return GetNFColor(this._nf.name, this._vendor);
  }

  public get draggable(): boolean {
    return this._draggable;
  }

  public get showEdit(): boolean {
    return this._showEdit;
  }

  public get showActivate(): boolean {
    return this._showActivate;
  }

  public onDragStart(event: DragEvent) {
    const eventData: DragEventData = {
      event: event,
      id: this.nf.local.id,
      nf: this.nf,
      type: 'nf',
      vendor: this._vendor,
      name: this.nf.name,
    };

    this._handleDragStart.emit(eventData);
  }

  public get tenant(): Tenant {
    return this._nf?.local?.tenant;
  }

  public onEdit() {
    if (!this._nf.local.vendor) {
      this._nf.local.vendor = this.vendor;
    }
    this._editClick.emit(this._nf);
  }

  public onActivateClick() {
    this._activate.emit(this._nf);
  }

  public onRemove() {
    const data: ConfirmationDialogData = {
      description: `This will remove the network function <strong>${this.nf.name}</strong>.`,
      action: 'Delete'
    };

    const dialogRef = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        switchMap((_: boolean) => this.nFService.deleteNf(this.nf.local.id))
      )
      .subscribe(
        () => this._remove.emit(this._nf),
        ({ status }: HttpErrorResponse) => {
          if (status === HttpError.Conflict) {
            this._alertService.error(AlertType.ERROR_REMOVE_CLUSTER_NF_IN_USE);
          } else {
            this._alertService.error(AlertType.ERROR_REMOVE_CLUSTER_NF);
          }
        }
      );
  }
}
