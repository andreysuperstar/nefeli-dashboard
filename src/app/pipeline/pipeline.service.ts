import { isEqual } from 'lodash';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Tenant } from '../tenant/tenant.service';
import { Observable, Subject, forkJoin, concat, timer, EMPTY, of, throwError } from 'rxjs';
import { PipelineGraph, PipelineNfStats, PipelineNF, PipelineNode, TemplateNodes, PipelineNodes } from './pipeline.model';
import { mergeMap, tap, shareReplay, map, skip, repeat, mapTo, catchError, pluck } from 'rxjs/operators';
import { NumberSymbol } from '@angular/common';
import { InstanceStatus } from 'rest_client/pangolin/model/instanceStatus';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { NFType } from 'rest_client/pangolin/model/nFType';
import { ServiceConfig } from 'rest_client/pangolin/model/serviceConfig';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { ServiceStatus } from 'rest_client/pangolin/model/serviceStatus';
import { Service } from 'rest_client/pangolin/model/service';
import { RESTGetTraceResponse } from 'rest_client/pangolin/model/rESTGetTraceResponse';
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { TunnelsService } from 'rest_client/pangolin/api/tunnels.service';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { StatsService } from 'rest_client/pangolin/api/stats.service';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { LoggerService } from '../shared/logger.service';
import { HttpError } from '../error-codes';
import { Services } from 'rest_client/pangolin/model/services';
import { cloneDeep } from 'lodash-es';
import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';
import { ServiceTemplatesService } from 'rest_client/pangolin/api/serviceTemplates.service';
import { ServiceConfigTemplate } from 'rest_client/pangolin/model/serviceConfigTemplate';
import { ServiceTemplate } from 'rest_client/pangolin/model/serviceTemplate';

export const VALID_SERVICE_NAME_FORMAT = /^[a-zA-Z\d_]{1,64}$/;

// tslint:disable-next-line: no-empty-interface
export interface Tunnel extends TunnelConfig { }

export interface NFConfiguration {
  nf: PipelineNF;
  config?: NFCConfig;
  manifest?: NFDescriptor;
  manifestRaw?: string;
}

export interface ClusterPipeline {
  weaverService: string;
  expService: string;
  weaverTenant: string;
  expTenant: string;
}

export enum EdgeDirection {
  Upward,
  Downward,
  Both
}

export interface ClusterPipelinesResponse {
  services: ClusterPipeline[];
}

@Injectable({
  providedIn: 'root'
})
export class PipelineService {

  private _nfConfigurations: Map<string, NFConfiguration> = new Map();
  private _pipelineNFs$ = new Subject<string[]>();
  private _pipelineNFs: string[] = [];

  constructor(
    private _restServicesService: ServicesService,
    private _restStatsService: StatsService,
    private _restTunnelsService: TunnelsService,
    private _restSitesService: SitesService,
    private _nfService: NetworkFunctionsService,
    private _templateService: ServiceTemplatesService,
    private _log: LoggerService
  ) { }

  public getClusterPipelines(id: string): Observable<ClusterPipelinesResponse> {
    return this._restSitesService.getClusterServices(id);
  }

  public getTenantPipelines(tenant: Tenant): Observable<Service[]> {
    return this._restServicesService.getServices(tenant.identifier, undefined, ['asc(name)']).pipe(
      pluck<Services, Service[]>('services')
    );
  }

  public checkTenantPipeline(tenant: Tenant, id: string): Observable<ServiceConfig> {
    return this._restServicesService
      .getService(tenant.identifier, id)
      .pipe(
        pluck('config')
      );
  }

  public convertTemplateToPipeline(template: ServiceConfigTemplate): Observable<PipelineGraph> {
    const graph: PipelineGraph = {
      edges: template.edges,
      nodes: template.nodes as PipelineNodes & TemplateNodes
    };

    const nfRequests = new Array<Observable<NFDescriptor>>();
    const keys = template?.nodes ? Object.keys(template?.nodes) : [];

    for (const key of keys) {
      const node = template?.nodes[key];

      if (node?.nf) {
        nfRequests.push(this._nfService.getNf(node.nf.catalogId));
      }
    }

    return (nfRequests.length ? forkJoin(nfRequests) : of({})).pipe(
      map((manifests: NFDescriptor[]): ServiceConfig | ServiceConfigTemplate =>
        this.initNodeNfs(template, manifests))
    ) as Observable<PipelineGraph>;
  }

  public getServiceTemplate(templateId: string): Observable<PipelineGraph> {
    return this._templateService.getServiceTemplate(templateId).pipe(
      pluck<ServiceTemplate, ServiceConfigTemplate>('config'),
      map(serviceConfig => {
        serviceConfig.edges = serviceConfig.edges.filter((edge, index, edges) => {
          const nextIndex = index + 1;

          const reverseEdge = edges
            .slice(nextIndex)
            .find(({ a, b }) => {
              const hasCommonNodes = a.node === edge.b.node && b.node === edge.a.node;
              const hasCommonInterfaces = a['interface'] === edge.b['interface'] && a['interface'] === edge.b['interface'];

              return hasCommonNodes && hasCommonInterfaces;
            });

          if (reverseEdge) {
            reverseEdge.filterBa = edge.filterAb;
          }

          return !reverseEdge;
        });

        return serviceConfig;
      }),
      // TODO(ecarino): mergeMap with a resultSelector has been deprecated
      // tslint:disable-next-line: deprecation
      mergeMap((service: ServiceConfigTemplate) => {
        const nfRequests = new Array<Observable<NFDescriptor>>();
        const keys = service?.nodes ? Object.keys(service?.nodes) : [];

        for (const key of keys) {
          const node = service?.nodes[key];

          if (node?.nf) {
            this._nfService.getNf(node.nf.catalogId);
          }
        }

        return nfRequests.length ? forkJoin(nfRequests) : of({});
      }, (service: PipelineGraph, manifests: NFDescriptor[]): ServiceConfig => {
        for (const key of Object.keys(service.nodes)) {
          const node = service.nodes[key];

          // TODO(eric): cleanup, don't need two properties that have the same exact value
          node.local = {
            id: key,
            name: key
          };

          if (node.nf) {
            node.nf.local = {};
            const catalogId = node.nf.catalogId;
            const manifest = manifests.find((m: NFDescriptor) => m.identifier?.toUpperCase() === catalogId?.toUpperCase());

            if (manifest) {
              if (manifest.type === NFType.NATIVE) {
                node.nf.local.interfaces = cloneDeep(manifest.components.datapath.bessManifest.interfaces);
              } else {
                node.nf.local.interfaces = cloneDeep(manifest.components.datapath.vmManifest.interfaces);
              }

              node.nf.local.name = manifest.name;
              node.nf.local.vendor = manifest.components.datapath.vendor;
              node.nf.local.type = manifest.type;
              node.nf.local.instances = [];
            } else {
              this._log.error(`Could not find manifest for nf: ${catalogId}`);
            }
          } else if (node.port) {
            node.port.local = {};
          }
        }

        return service;
      })) as Observable<PipelineGraph>;
  }

  public getTenantPipeline(tenant: Tenant, id: string, isDraft = false): Observable<PipelineGraph> {
    const getPipelineId$ = isDraft
      ? this._restServicesService.getServiceDraft(tenant.identifier, id)
      : this._restServicesService.getService(tenant.identifier, id);

    return getPipelineId$.pipe(
      pluck<Service, ServiceConfig>('config'),
      map(serviceConfig => {
        serviceConfig.edges = serviceConfig.edges.filter((edge, index, edges) => {
          const nextIndex = index + 1;

          const reverseEdge = edges
            .slice(nextIndex)
            .find(({ a, b }) => {
              const hasCommonNodes = a.node === edge.b.node && b.node === edge.a.node;
              const hasCommonInterfaces = a['interface'] === edge.b['interface'] && a['interface'] === edge.b['interface'];

              return hasCommonNodes && hasCommonInterfaces;
            });

          if (reverseEdge) {
            reverseEdge.filterBa = edge.filterAb;
          }

          return !reverseEdge;
        });

        return serviceConfig;
      }),
      // TODO(ecarino): mergeMap with a resultSelector has been deprecated
      // tslint:disable-next-line: deprecation
      mergeMap((service: ServiceConfig) => {
        const nfRequests = new Array<Observable<NFDescriptor>>();
        const keys = service?.nodes ? Object.keys(service?.nodes) : [];

        for (const key of keys) {
          const node = service?.nodes[key];

          if (node?.nf) {
            if (isDraft) {
              nfRequests.push(this._restServicesService.getServiceDraftNFManifest(tenant.identifier, id, key));
            } else {
              nfRequests.push(this._restServicesService.getServiceNFManifest(tenant.identifier, id, key));
            }
          }
        }

        return nfRequests.length ? forkJoin(nfRequests) : of({});
      }, (service: PipelineGraph, manifests: NFDescriptor[]): ServiceConfig | ServiceConfigTemplate => {
        return this.initNodeNfs(service, manifests);
      })) as Observable<PipelineGraph>;
  }

  private initNodeNfs(service: ServiceConfig | ServiceConfigTemplate, manifests: NFDescriptor[]): ServiceConfig | ServiceConfigTemplate {
    if (!service.nodes) {
      return;
    }

    for (const key of Object.keys(service.nodes)) {
      const node = service.nodes[key] as PipelineNode;

      // TODO(eric): cleanup, don't need two properties that have the same exact value
      node.local = {
        id: key,
        name: key
      };

      if (node.nf) {
        node.nf.local = {};
        const catalogId = node.nf.catalogId;
        const manifest = manifests.find((m: NFDescriptor) => m.identifier?.toUpperCase() === catalogId?.toUpperCase());

        if (manifest) {
          if (manifest.type === NFType.NATIVE) {
            node.nf.local.interfaces = cloneDeep(manifest.components.datapath.bessManifest.interfaces);
          } else {
            node.nf.local.interfaces = cloneDeep(manifest.components.datapath.vmManifest.interfaces);
          }

          node.nf.local.name = manifest.name;
          node.nf.local.vendor = manifest.components.datapath.vendor;
          node.nf.local.type = manifest.type;
          node.nf.local.instances = [];
        } else {
          this._log.error(`Could not find manifest for nf: ${catalogId}`);
        }
      } else if (node.port) {
        node.port.local = {};
      }
    }

    return service;
  }

  public postTenantPipeline(
    tenant: Tenant,
    graph: PipelineGraph,
    licensePool?: string,
    force = false
  ): Observable<ServiceConfig> {
    return this._restServicesService.postService(tenant.identifier, graph, licensePool, force);
  }

  public patchTenantPipeline(
    tenant: Tenant,
    id: string,
    graph: PipelineGraph,
    licensePool?: string,
    force = false): Observable<ServiceConfig> {
    return this._restServicesService.putService(tenant.identifier, id, graph, licensePool, force, (licensePool === undefined));
  }

  public streamTenantPipeline(tenant: Tenant, id: string, delay: NumberSymbol, isDraft = false): Observable<PipelineGraph> {
    return concat(
      this.getTenantPipeline(tenant, id, isDraft),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat()
    );
  }

  private postTenantPipelineStats(tenant: Tenant, serviceId: string): Observable<PipelineNfStats> {
    return this._restStatsService.postServiceStats(tenant.identifier, serviceId);
  }

  public streamTenantPipelineStats(tenant: Tenant, id: string, delay = 2000): Observable<PipelineNfStats> {
    return concat(
      this.postTenantPipelineStats(tenant, id),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      tap((stats: PipelineNfStats) => {
        const NFnames = Object.keys(stats).sort();
        if (!isEqual(NFnames, this._pipelineNFs)) {
          this._pipelineNFs$.next(NFnames);
          this._pipelineNFs = NFnames;
        }
      }),
      shareReplay({
        refCount: true
      })
    );
  }

  public streamPipelineTrace(tenantId: string, serviceId: string, delay = 2000): Observable<RESTGetTraceResponse> {
    return concat(
      this._restServicesService.getServiceTrace(tenantId, serviceId),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat()
    );
  }

  public postPipelineBpf(tenant: Tenant, id: string, bpf: string): Observable<any> {
    return this._restServicesService.postServiceTrace(tenant.identifier, id, bpf);
  }

  public putPipelineLicensePool(tenantId: string, serviceId: string, poolId: string): Observable<any> {
    return this._restServicesService.putServiceLicensePool(tenantId, serviceId, poolId);
  }

  public deletePipelineLicensePool(tenantId: string, serviceId: string): Observable<any> {
    return this._restServicesService.deleteServiceLicensePool(tenantId, serviceId);
  }

  public deletePipeline(tenantId: string, serviceId: string): Observable<any> {
    return this._restServicesService.deleteService(tenantId, serviceId);
  }

  public deletePipelineBpf(tenant: Tenant, id: string): Observable<any> {
    return this._restServicesService.deleteServiceTrace(tenant.identifier, id);
  }

  public getTunnels(tenantId: string): Observable<Tunnel[]> {
    return this._restTunnelsService
      .getTenantTunnels(tenantId, true)
      .pipe(
        map(({ tunnels }) => tunnels.map(({ config }) => config))
      );
  }

  public getNfManifest(tenantId: string, serviceId: string, node: string): Observable<NFDescriptor> {
    return this._restServicesService.getServiceNFManifest(tenantId, serviceId, node);
  }

  public putNfManifest(
    tenantId: string,
    serviceId: string,
    node: string,
    manifest: NFDescriptor): Observable<ServiceConfig> {
    return this._restServicesService.putServiceNFManifest(tenantId, serviceId, node, manifest);
  }

  public streamNfInstanceStatus(
    tenantId: string,
    serviceId: string,
    nfId: string,
    instanceId: string,
    delay = 2000): Observable<InstanceStatus> {
    return concat(
      this._restServicesService
        .getInstance(tenantId, serviceId, nfId, instanceId, true)
        .pipe(
          pluck('status')
        ),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat()
    );
  }

  public streamPipelineStatus(
    tenantId: string,
    serviceId: string): Observable<ServiceStatus> {
    return this._restServicesService.getService(tenantId, serviceId, true).pipe(
      map((service: Service) => {
        if (!service.status.nodes) {
          return service.status;
        }

        // TODO(ecarino): workaround because node[].instance is null from API
        for (const node of Object.values(service.status.nodes)) {
          for (const instanceId of Object.keys(node.instances)) {
            const instance = node.instances[instanceId];
            instance.identity = { instanceId };
          }
        }

        return service.status;
      }),
      catchError((error: Error) => EMPTY)
    );
  }

  public getNFConfiguration(nodeId: string): NFConfiguration {
    return this._nfConfigurations.get(nodeId);
  }

  public deleteNFConfiguration(nodeId: string): boolean {
    return this._nfConfigurations.delete(nodeId);
  }

  public setNFConfiguration(nodeId: string, nfConfiguration: NFConfiguration) {
    this._nfConfigurations.set(nodeId, nfConfiguration);
  }

  public clearNFConfigurations() {
    this._nfConfigurations.clear();
  }

  public get nfConfigurations(): IterableIterator<[string, NFConfiguration]> {
    return this._nfConfigurations.entries();
  }

  public get pipelineNFs$(): Subject<string[]> {
    return this._pipelineNFs$;
  }

  public getServiceLicensePool(tenantId: string, serviceId: string, isDraft = false): Observable<string> {
    return isDraft
      ? this._restServicesService.getServiceDraftLicensePool(tenantId, serviceId)
      : this._restServicesService.getServiceLicensePool(tenantId, serviceId);
  }

  public deleteDraftPipeline(tenantId: string, serviceId: string): Observable<string> {
    return this._restServicesService.deleteServiceDraft(tenantId, serviceId);
  }

  public hasDraft(tenant: Tenant, serviceId: string): Observable<boolean> {
    return this._restServicesService.getService(tenant.identifier, serviceId, true)
      .pipe(
        pluck<Service, ServiceStatus>('status'),
        map((service: ServiceStatus): boolean => service?.hasDraft),
        catchError((error: HttpErrorResponse) => {
          if (error.status === HttpError.NotFound) {
            return of(false);
          } else {
            return throwError(error);
          }
        })
      );
  }
}
