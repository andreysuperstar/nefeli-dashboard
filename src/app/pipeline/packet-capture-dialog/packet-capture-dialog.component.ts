import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';

import { PacketCaptureRequestBackend } from 'rest_client/pangolin/model/packetCaptureRequestBackend';
import { PacketCaptureRequestDirection } from 'rest_client/pangolin/model/packetCaptureRequestDirection';
import { PacketCaptureRequest } from 'rest_client/pangolin/model/packetCaptureRequest';

import { Format, PacketCaptureService } from '../packet-capture.service';
import { PacketCaptureRequestTcpdumpInterpret } from 'rest_client/pangolin/model/packetCaptureRequestTcpdumpInterpret';

export interface PacketDialogData {
  tenant: string;
  service: string;
  node: string;
  instance: string;
  port: string;
}

enum DurationRange {
  Min = 0,
  Max = 55
}

export const PACKET_CAPTURE_DEFAULTS = {
  SIZE: 750, // kb
  TIME: 10,
  PACKETS: 500
};

@Component({
  selector: 'nef-packet-capture-dialog',
  templateUrl: './packet-capture-dialog.component.html',
  styleUrls: ['./packet-capture-dialog.component.less']
})
export class PacketCaptureDialogComponent implements OnInit, OnDestroy {
  private _packetCaptureForm: FormGroup;
  private _inProgress = false;
  private _result = false;
  private _error: string;
  private _packetSubscription: Subscription;

  constructor(
    private _fb: FormBuilder,
    private _packetService: PacketCaptureService,
    @Inject(MAT_DIALOG_DATA) private _data: PacketDialogData,
  ) { }

  public ngOnInit() {
    this.initPacketCaptureForm();
    this.checkActiveSubscription();
  }

  public ngOnDestroy() {
    if (this._packetSubscription) {
      this._packetSubscription.unsubscribe();
    }
  }

  private initPacketCaptureForm() {
    this._packetCaptureForm = this._fb.group({
      format: [Format.TCPDUMP, Validators.required],
      direction: [PacketCaptureRequestDirection.IN],
      captureSize: [PACKET_CAPTURE_DEFAULTS.SIZE, Validators.required],
      packetCount: [PACKET_CAPTURE_DEFAULTS.PACKETS, Validators.required],
      duration: [
        PACKET_CAPTURE_DEFAULTS.TIME,
        [
          Validators.required,
          Validators.min(DurationRange.Min),
          Validators.max(DurationRange.Max)
        ]
      ],
      appendCommand: ''
    });
  }

  private checkActiveSubscription() {
    const trace = this._packetService.getTrace(this.packetCaptureRequest);

    if (trace) {
      this._inProgress = true;

      this._packetSubscription = trace.subscribe({
        next: () => this.handleCaptureSuccess(),
        error: error => this.handleCaptureError(error)
      });
    }
  }

  public get packetCaptureForm(): FormGroup {
    return this._packetCaptureForm;
  }

  public submitPacketCaptureForm() {
    const { invalid } = this._packetCaptureForm;

    this._error = '';
    this._result = false;

    if (invalid && !this._inProgress) {
      return;
    }

    if (this._inProgress) {
      this.cancelCapture();
    } else {
      this.startCapture();
    }
  }

  private startCapture() {
    this._inProgress = true;

    const format = this._packetCaptureForm.get('format').value;

    this._packetSubscription = this._packetService
      .trace(this._data.tenant, this.packetCaptureRequest, format)
      .subscribe(
        () => {
          this.handleCaptureSuccess();
        },
        (error: HttpErrorResponse) => {
          this.handleCaptureError(error);
        }
      );
  }

  private handleCaptureSuccess() {
    this._inProgress = false;
    this._result = true;
  }

  private handleCaptureError(err: HttpErrorResponse) {
    this._inProgress = false;

    err.error.text().then((resp: string) => {
      const errMessage = JSON.parse(resp)?.message;

      this._error = 'ERROR Capturing traffic';

      if (errMessage) {
        this._error += ` (${errMessage})`;
      }
    });
  }

  private cancelCapture() {
    this._packetSubscription.unsubscribe();
    this._packetService.cancel(this.packetCaptureRequest);

    this._inProgress = false;
  }

  private get packetCaptureRequest(): PacketCaptureRequest {
    const { service, node, instance, port } = this._data;

    const {
      format,
      direction,
      captureSize: sizeLimit,
      packetCount: packetLimit,
      duration: timeLimit,
      appendCommand: appendCmd
    }: {
      format: Format,
      direction: PacketCaptureRequestDirection,
      captureSize: number,
      packetCount: number,
      duration: number,
      appendCommand: string
    } = this._packetCaptureForm.value;

    const backend = format === Format.TSHARK
      ? PacketCaptureRequestBackend.TSHARK
      : PacketCaptureRequestBackend.TCPDUMP;

    let interpret: PacketCaptureRequestTcpdumpInterpret;

    switch (format) {
      case (Format.TCPDUMP):
        interpret = PacketCaptureRequestTcpdumpInterpret.INTERPRET;
        break;
      case (Format.PCAP):
        interpret = PacketCaptureRequestTcpdumpInterpret.NOINTERPRET;
        break;
    }

    return { appendCmd, backend, direction, instance, interpret, node, packetLimit, port, service, sizeLimit, timeLimit };
  }

  public cancelPacketCapture() {
    if (this._packetSubscription) {
      this._packetSubscription.unsubscribe();
      this._inProgress = false;
    }
  }

  public get format(): string {
    return this.packetCaptureForm.get('format').value;
  }

  public get Format(): typeof Format {
    return Format;
  }

  public get DurationRange(): typeof DurationRange {
    return DurationRange;
  }

  public get PacketCaptureRequestDirection(): typeof PacketCaptureRequestDirection {
    return PacketCaptureRequestDirection;
  }

  public get inProgress(): boolean {
    return this._inProgress;
  }

  public get result(): boolean {
    return this._result;
  }

  public get error(): string {
    return this._error;
  }
}
