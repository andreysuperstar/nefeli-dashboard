import { HttpTestingController } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Format } from '../packet-capture.service';
import { PacketDialogData, PACKET_CAPTURE_DEFAULTS, PacketCaptureDialogComponent } from './packet-capture-dialog.component';
import { MatOptionModule } from '@angular/material/core';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RequestUrlInterceptor } from './../../http-interceptors/request-url.interceptor';
import { environment } from './../../../environments/environment';
import { By } from '@angular/platform-browser';
import { PacketCaptureRequestTcpdumpInterpret } from 'rest_client/pangolin/model/packetCaptureRequestTcpdumpInterpret';
import { HttpError } from 'src/app/error-codes';

describe('PacketCaptureDialogComponent', () => {
  let component: PacketCaptureDialogComponent;
  let fixture: ComponentFixture<PacketCaptureDialogComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  const dialogSpy = {
    close: jasmine.createSpy('close')
  };

  const mockDialogData: PacketDialogData = {
    tenant: 'andrew',
    service: 'sample',
    node: 'arista',
    instance: 'arista_1',
    port: 'right'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatOptionModule,
        MatSelectModule,
        MatProgressSpinnerModule,
        NoopAnimationsModule,
        MatRadioModule
      ],
      declarations: [PacketCaptureDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockDialogData
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PacketCaptureDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    expect(el.querySelector('h1').textContent).toBe('Packet Capture');
  });

  it('should render form', () => {
    const form = el.querySelector('form');
    const labels = form.querySelectorAll('mat-label');
    expect(form).toBeDefined();
    expect(labels.length).toBe(5);
    expect(labels[0].textContent).toBe('Format');
    expect(labels[1].textContent).toBe('Capture Size (kilobytes)');
    expect(labels[2].textContent).toBe('Packet Count');
    expect(labels[3].textContent).toBe('Duration (seconds)');
    expect(labels[4].textContent).toBe('Append Command');
  });

  it('should set default values for the form fields', () => {
    const { captureSize, packetCount, duration } = component.packetCaptureForm.value;

    expect(captureSize).toBe(PACKET_CAPTURE_DEFAULTS.SIZE);
    expect(packetCount).toBe(PACKET_CAPTURE_DEFAULTS.PACKETS);
    expect(duration).toBe(PACKET_CAPTURE_DEFAULTS.TIME);
  });

  it('should change the button if request is in progress', () => {
    let buttonEl: HTMLButtonElement = el.querySelector('button');

    component.packetCaptureForm.patchValue({
      format: Format.TCPDUMP
    });

    component.packetCaptureForm.markAsDirty();
    fixture.detectChanges();
    buttonEl.click();
    fixture.detectChanges();

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockDialogData.tenant}/services/${mockDialogData.service}/packetcapture`
    });

    buttonEl = el.querySelector('button');

    expect(buttonEl.textContent.trim()).toBe('Stop Packet Capture');
    expect(component.inProgress).toBe(true);

    const mockResponse = new Blob(['']);

    packetCaptureRequest.flush(mockResponse);
    fixture.detectChanges();

    buttonEl = el.querySelector('button');
    expect(buttonEl.textContent).toBe('Start Packet Capture');
  });

  it('should display result link after successful response', () => {
    const buttonEl: HTMLButtonElement = el.querySelector('button');

    component.packetCaptureForm.patchValue({
      format: Format.TCPDUMP
    });

    component.packetCaptureForm.markAsDirty();
    fixture.detectChanges();
    buttonEl.click();

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockDialogData.tenant}/services/${mockDialogData.service}/packetcapture`
    });

    const mockResponse = new Blob(['']);

    packetCaptureRequest.flush(mockResponse);
    fixture.detectChanges();

    const result = el.querySelector('.result');

    expect(result).toBeDefined();
    expect(result.textContent.trim()).toBe('Packet Capture Received');
  });

  it('should send request with raw packet capture tcpdump', () => {
    const mockFormData = {
      format: Format.PCAP,
      appendCommand: 'command'
    };

    component.packetCaptureForm.patchValue(mockFormData);

    const { appendCmd, interpret, packetLimit } = component['packetCaptureRequest'];

    expect(appendCmd).toBe(mockFormData.appendCommand);
    expect(packetLimit).toBe(PACKET_CAPTURE_DEFAULTS.PACKETS);
    expect(interpret).toBe(PacketCaptureRequestTcpdumpInterpret.NOINTERPRET);
  });

  it('should send request with tcpdump', () => {
    const mockFormData = {
      format: Format.TCPDUMP,
      appendCommand: 'command'
    };

    component.packetCaptureForm.patchValue(mockFormData);

    const { appendCmd, interpret, packetLimit } = component['packetCaptureRequest'];

    expect(appendCmd).toBe(mockFormData.appendCommand);
    expect(packetLimit).toBe(PACKET_CAPTURE_DEFAULTS.PACKETS);
    expect(interpret).toBe(PacketCaptureRequestTcpdumpInterpret.INTERPRET);
  });

  it('should send request with tshark', () => {
    const mockFormData = {
      format: Format.TSHARK,
      appendCommand: 'command'
    };

    component.packetCaptureForm.patchValue(mockFormData);

    const { appendCmd, interpret, packetLimit } = component['packetCaptureRequest'];

    expect(appendCmd).toBe(mockFormData.appendCommand);
    expect(packetLimit).toBe(PACKET_CAPTURE_DEFAULTS.PACKETS);
    expect(interpret).toBeUndefined();
  });

  it('should cancel a capture in progress', () => {
    const form = fixture.debugElement.query(By.css('form'));
    form.triggerEventHandler('ngSubmit', {});

    component['checkActiveSubscription']();
    expect(component.inProgress).toBe(true);
    fixture.detectChanges();
    spyOn(component['_packetSubscription'], 'unsubscribe');

    const button = fixture.debugElement.query(By.css('button'));
    expect(button.nativeElement.textContent.trim()).toBe('Stop Packet Capture');
    button.triggerEventHandler('click', {});

    expect(component.inProgress).toBe(false);
    expect(component['_packetSubscription'].unsubscribe).toHaveBeenCalled();
  });

  it('should display error if capture fails', () => {
    let buttonEl: HTMLButtonElement = el.querySelector('button');
    const message = '\'|| rm *\' contains illegal character(s)';

    spyOn<any>(component, 'handleCaptureError').and.callFake((err) => {
      expect(typeof err.error.text).toBe('function');
      err.error.text().then((resp: string) => {
        expect(JSON.parse(resp)?.message).toBe(message);
      });
    });

    component.packetCaptureForm.patchValue({
      format: Format.TCPDUMP
    });

    component.packetCaptureForm.markAsDirty();
    fixture.detectChanges();
    buttonEl.click();
    fixture.detectChanges();

    const packetCaptureRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/tenants/${mockDialogData.tenant}/services/${mockDialogData.service}/packetcapture`
    });

    const mockResponse = new Blob([JSON.stringify({
      httpStatus: HttpError.BadRequest,
      message,
      errorCode: 126
    })]);

    const mockErrorResponse: Partial<HttpErrorResponse> = {
      status: HttpError.BadRequest,
      statusText: 'Bad Request'
    };

    packetCaptureRequest.flush(mockResponse, mockErrorResponse);

    expect(component['handleCaptureError']).toHaveBeenCalled();
  });
});
