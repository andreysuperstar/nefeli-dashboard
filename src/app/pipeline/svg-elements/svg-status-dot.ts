import * as SVG from 'svg.js';
import { SvgTooltip, TooltipPosition } from './svg-tooltip';

const Config = {
  diameter: 12,
  inProgress: {
    color: '#0198CF',
    stroke: {
      width: 2,
      dasharray: 28
    },
    rotate: 360
  },
  success: {
    color: '#2ED573'
  },
  error: {
    img: 'assets/icon-alert-circle.svg'
  }
};
export class SvgStatusDot {
  private _nativeElement: SVG.G;
  private _successElement: SVG.G;
  private _spinnerElement: SVG.G & SVG.Animation;
  private _errorImg: SVG.G;
  private _errorTooltip: SvgTooltip;

  public constructor(private _root: SVG.G) {
    this.init();
  }

  private init() {
    this._nativeElement = this._root.group();

    // create success dot
    this._successElement = this._nativeElement.group();
    this._successElement.add(this._root.circle(Config.diameter).attr({ fill: Config.success.color }));
    this._successElement.addClass('success');

    // create in-progress dot
    this._spinnerElement = this._nativeElement.group() as SVG.G & SVG.Animation;
    const spinner = this._root.circle(Config.diameter).attr({
      stroke: Config.inProgress.color,
      'stroke-width': Config.inProgress.stroke.width,
      'stroke-dasharray': Config.inProgress.stroke.dasharray,
      'stroke-linecap': 'round',
      fill: 'transparent'
    });
    (spinner.animate().rotate(Config.inProgress.rotate) as any).loop();
    this._spinnerElement.add(spinner);
    this._spinnerElement.addClass('in-progress');

    // create error dot
    this._errorImg = this._nativeElement.group();
    const image = this._root.image(Config.error.img, Config.diameter, Config.diameter);
    this._errorImg.add(image);
    this._errorImg.addClass('error');

    this._successElement.hide();
    this._spinnerElement.hide();
    this._errorImg.hide();

    this._nativeElement.add(this._successElement);
    this._nativeElement.add(this._spinnerElement);
    this._nativeElement.add(this._errorImg);
    this._nativeElement.addClass('status-dot');
  }

  public showCurrentStatus() {
    if (!this._nativeElement.visible()) {
      this._nativeElement.show();

      if (this._spinnerElement.visible()) {
        this._spinnerElement.play();
      }
    }
  }

  public hideCurrentStatus() {
    if (this._spinnerElement.visible()) {
      this._spinnerElement.pause();
    }

    this._nativeElement.hide();
  }

  public showError() {
    this._successElement.hide();
    this._spinnerElement.pause();
    this._spinnerElement.hide();
    this._errorImg.show();
  }

  public showSuccess() {
    this._spinnerElement.pause();
    this._spinnerElement.hide();
    this._errorImg.hide();
    this._successElement.show();
  }

  public showInProgress() {
    this._successElement.hide();
    this._errorImg.hide();
    this._spinnerElement.play();
    this._spinnerElement.show();
  }

  public get nativeElement(): SVG.G {
    return this._nativeElement;
  }

  public get diameter(): number {
    return Config.diameter;
  }

  public get errorTooltip(): SvgTooltip {
    if (!this._errorTooltip) {
      this._errorTooltip = new SvgTooltip(
        this._nativeElement,
        { position: TooltipPosition.After }
      );
    }

    return this._errorTooltip;
  }

  public set errorTooltip(tooltip: SvgTooltip) {
    this._errorTooltip = tooltip;
  }
}
