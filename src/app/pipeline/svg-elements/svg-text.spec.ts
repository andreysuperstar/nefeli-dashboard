import * as SVG from 'svg.js';
import { ellipsis, isEllipsisToken, isVariableToken, SvgText } from './svg-text';
import { Component, OnInit } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LoggerService } from '../../shared/logger.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

@Component({
  selector: 'dummy-component',
  template: '<div id="drawing"></div>',
  styles: [
    '#drawing { width: 300px; height: 300px }'
  ]

})
export class DummyComponent implements OnInit {
  private _root: SVG.G;
  private _drawing: SVG.Doc;

  public ngOnInit(): void {
    this._drawing = SVG('drawing').size(300, 300);
    this._root = this._drawing.group();
    this._root.size(300, 300);
  }

  public get root(): SVG.G {
    return this._root;
  }
}

describe('SvgText', () => {
  let fixture: ComponentFixture<DummyComponent>;
  let component: DummyComponent;
  let document: Document;
  let logger: LoggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule, HttpClientTestingModule],
      declarations: [DummyComponent]
    });
    document = TestBed.inject(DOCUMENT);
    logger = TestBed.inject(LoggerService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    const varText = new SvgText(component.root, 'Simple text');
    expect(varText).toBeTruthy();
    const textEls = varText.nativeElement.select('text');
    expect(textEls.length()).toBe(1);
    expect(isEllipsisToken({
      text: ellipsis,
      variable: false
    })).toBeTruthy();
    expect(isEllipsisToken({
      text: 'text',
      variable: false
    })).toBeFalsy();
    expect(isVariableToken({
      text: 'text',
      variable: true
    })).toBeTruthy();
    expect(isVariableToken({
      text: 'text',
      variable: false
    })).toBeFalsy();

    const fondData: SVG.FontData = { weight: 'bold' };
    varText.font(fondData);
    fixture.detectChanges();
    expect(varText['_font']).toEqual(fondData);

    varText.addClass('content');
    fixture.detectChanges();
    expect(varText.nativeElement.hasClass('content')).toBeTruthy();

    const color = 'red';
    varText.color(color);
    fixture.detectChanges();
    expect(varText['_color']).toEqual(color);

    const size = 15;
    varText.size(size);
    fixture.detectChanges();
    expect(varText['_font']).toEqual({ ...fondData, size });

    const newText = 'new text';
    varText.text(newText);
    fixture.detectChanges();
    expect((varText.nativeElement.select('text').first() as SVG.Text).text()).toEqual(newText);

    const truncateSize = 5;
    varText.truncate(truncateSize);
    fixture.detectChanges();
    expect(varText['_maxLength']).toEqual(truncateSize);
    expect((varText.nativeElement.select('text').first() as SVG.Text).text()).toEqual('…');

    const attr = { dx: 10, dy: 15 };
    spyOn(varText.nativeElement, 'attr');
    varText.attr(attr);
    fixture.detectChanges();
    expect(varText.nativeElement.attr as (attr: Object) => SVG.G).toHaveBeenCalledWith(attr);

    spyOn(varText.nativeElement, 'translate');
    varText.translate(15, 20);
    fixture.detectChanges();
    expect(varText.nativeElement.translate).toHaveBeenCalledWith(15, 20);

    spyOn(varText.nativeElement, 'rbox');
    expect(varText.rbox()).toBeUndefined();
    expect(varText.nativeElement.rbox).toHaveBeenCalled();

    spyOn(varText.nativeElement, 'bbox');
    expect(varText.bbox()).toBeUndefined();
    expect(varText.nativeElement.bbox).toHaveBeenCalled();
  });

  it('should not render an empty string', () => {
    const varText = new SvgText(component.root, '');
    const textEl = varText.nativeElement.select('text');
    expect(textEl.length()).toBe(0);
  });

  it('should remove created svg elements', () => {
    const varText = new SvgText(component.root, 'simple text');
    const textEls = varText.nativeElement.select('text');
    expect(textEls.length()).toBe(1);
    varText.remove();
    fixture.detectChanges();
    const groupEls = component.root.select('group, text');
    expect(groupEls.length()).toBe(0);
  });

  it('should draw variable', () => {
    const varText = new SvgText(component.root, 'name with {{.var1}} variable');
    fixture.detectChanges();

    const textEl: SVG.Text = varText.nativeElement.select('text').first() as SVG.Text;
    expect(textEl.text()).toEqual('name with var1 variable');
    expect(varText.nativeElement.select('rect').length()).toEqual(1);
  });

  it('should truncate text', () => {
    const varText = new SvgText(component.root, 'name with {{.var1}} variable', 145);
    varText.font({
      family: 'serif',
      size: 15
    });
    fixture.detectChanges();

    const textEl: SVG.Text = varText.nativeElement.select('text').first() as SVG.Text;
    // expect(textEl.text()).toEqual(`name with var1 ${ellipsis}`);
    expect(varText.nativeElement.select('rect').length()).toEqual(1);
  });

  it('should not truncate variable', () => {
    const varText = new SvgText(component.root, 'name with {{.var1}}', 110);
    varText.font({
      family: 'serif',
      size: 15
    });
    fixture.detectChanges();

    let textEl: SVG.Text = varText.nativeElement.select('text').first() as SVG.Text;
    // expect(textEl.text()).toEqual(`name with ${ellipsis}`);
    // expect(varText.nativeElement.select('rect').length()).toEqual(0);

    varText.text('name with var1');
    fixture.detectChanges();
    textEl = varText.nativeElement.select('text').first() as SVG.Text;
    // expect(textEl.text()).toEqual(`name with v${ellipsis}`);
  });
});
