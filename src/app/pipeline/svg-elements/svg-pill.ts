import * as SVG from 'svg.js';

const Config = {
  defaultColor: '#000000',
  fillColor: '#ffffff',
  width: 58,
  height: 24,
  roundedCorners: 15
};

export class SvgPill {
  private _nativeElement: SVG.G;
  private _labelElement: SVG.Text;

  public constructor(
    private _root: SVG.G,
    private _color = Config.defaultColor,
    private _label: string = '-'
  ) {
    this.init();
  }

  private init() {
    this._nativeElement = this._root.group();

    this._nativeElement.add(this._root.rect(Config.width, Config.height)
      .radius(Config.roundedCorners).attr({
        fill: Config.fillColor,
        stroke: this._color,
        'stroke-width': 1
      }));

    this._labelElement = this._root.text(this._label)
      .attr({
        width: Config.width,
        height: Config.height,
        x: Config.width / 2,
        'text-anchor': 'middle',
        'dominant-baseline': 'middle',
        fill: this._color,
      }).font({
        size: 10
      });

    this._nativeElement.add(this._labelElement);
  }

  public updateLabel(label: string) {
    this._labelElement.text(label);
  }

  public get nativeElement(): SVG.G {
    return this._nativeElement;
  }

  public get width(): number {
    return Config.width;
  }
}
