import * as SVG from 'svg.js';

const Config = {
  width: 120,
  height: 25,
  borderRadius: 5,
  background: '#333',
  opacity: 0.7,
  color: '#FFFFFF',
  fontSize: 10,
  offset: {
    x: 15,
    y: -10
  },
  padding: 10
};

export enum TooltipPosition {
  After,
  Before,
  Above,
  Below
}

export interface TooltipConfig {
  message?: string;
  position: TooltipPosition;
}

export class SvgTooltip {
  private _nativeElement: SVG.G;
  private _view: SVG.G;
  private _position: TooltipPosition;
  private _rect: SVG.Rect;
  private _label: SVG.Text;

  public constructor(
    private _elem: SVG.G,
    private _config?: TooltipConfig
  ) {
    this.init();
    this.initHandlers();
    this._position = _config?.position;
    this.text(_config?.message);
  }

  private init() {
    this._view = (SVG.select('.svg-viewport') as any).members[0];

    if (!this._view) {
      return;
    }

    this._nativeElement = this._view.group();
    this._nativeElement.addClass('svg-tooltip');

    this._rect = this._nativeElement.rect(
      Config.width + (Config.padding * 2),
      Config.height + (Config.padding * 2)
    );
    this._rect.attr({
      fill: Config.background,
      'fill-opacity': Config.opacity,
      rx: Config.borderRadius
    });
    this._rect.translate(Config.offset.x, Config.offset.y);

    this._nativeElement.add(this._rect);
    this.hide();
  }

  private initHandlers() {
    this._elem.on('mouseover', () => this.show());
    this._elem.on('mouseleave', () => this.hide());
  }

  public text(text: string | string[]) {
    if (this._label) {
      this._label.remove();
      this._label = undefined;
    }

    if (!text || !this._nativeElement) {
      return;
    }

    this._label = this._nativeElement.text(
      (add: SVG.Tspan) => {
        if (typeof text === 'string') {
          add.tspan(text);
        } else {
          text.forEach((t: string) => {
            add.tspan(t).newLine();
          });
        }

        // @ts-ignore
        add.font({
          fill: Config.color,
          size: Config.fontSize
        });
      }
    );

    this._label.front();
    this._label.move(
      this._rect.rbox(this._nativeElement).x + Config.padding,
      this._rect.rbox(this._nativeElement).y + Config.padding
    );
    this._nativeElement.add(this._label);
    this._rect.width(this._label.rbox().width + (Config.padding * 2));
    this._rect.height(this._label.rbox().height + (Config.padding * 2));
  }

  private show() {
    if (!this._label) {
      return;
    }

    const [cx, cy] = this.getCoordinates();
    this._nativeElement
      .x(cx)
      .y(cy);
    this._nativeElement.front();
    this._nativeElement.show();
  }

  private getCoordinates(): [number, number] {
    const rootRb: SVG.RBox = this._elem.rbox(this._view);
    const rectRb: SVG.RBox = this._rect.rbox(this._view);
    const quarter = 0.25;
    let x = rootRb.x;
    let y = rootRb.cy;

    switch (this._position) {
      case TooltipPosition.After:
        x += rootRb.w / 2;
        y -= rootRb.h * quarter;
        break;
      case TooltipPosition.Before:
        x -= rectRb.w + 2 * rootRb.w;
        y -= rootRb.h * quarter;
        break;
      case TooltipPosition.Above:
        x = rootRb.x - rectRb.w / 2;
        y -= rectRb.h;
        break;
      case TooltipPosition.Below:
        x = rootRb.x - rectRb.w / 2;
        y += rectRb.h;
        break;
      default:
        x += rootRb.w / 2;
        y -= rootRb.h / 2;
    }
    return [x, y];
  }

  private hide() {
    this._nativeElement.hide();
  }
}
