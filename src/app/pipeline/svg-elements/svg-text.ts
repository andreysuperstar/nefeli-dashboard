import * as SVG from 'svg.js';

import { TEMPLATE_VARIABLE_REGEXP } from '../../utils/codemirror';

export type TextToken = {
  text: string,
  variable: boolean,
  letterCount?: number,
  tspan?: SVG.Tspan
};

const Config = {
  variable: {
    color: '#ffe6cc',
    border: {
      color: '#d79b00',
      width: 1,
      radius: 5
    }
  }
};

export const ellipsis = '\u2026'; // '...'

export const isVariableToken = (token: TextToken): boolean => {
  return token.variable;
};

export const isEllipsisToken = (token: TextToken): boolean => {
  return token.text === ellipsis;
};

export class SvgText {
  private _root: SVG.G;
  private _font: SVG.FontData;
  private _color: string;

  public constructor(
    private _parent: SVG.G, private _text?: string, private _maxLength?: number) {
    this._root = _parent.group();
    this.render();
  }

  private render() {
    this._root.clear();
    if (!this._text) {
      return;
    }
    const tokens: TextToken[] = this.parse(this._text);
    const svgText = this.createSVGText(tokens);
    const varTokens: TextToken[] = tokens.filter(token => token.variable && token.tspan);
    varTokens.forEach(token => {
      const tspanBox = token.tspan.bbox();
      this._root
        .rect(tspanBox.width, tspanBox.height)
        .fill(Config.variable.color)
        .stroke({ color: Config.variable.border.color, width: Config.variable.border.width })
        .attr({
          rx: Config.variable.border.radius, ry: Config.variable.border.radius
        })
        .translate(tspanBox.x, tspanBox.y).after(svgText);
    });
  }

  private parse(text: string): TextToken[] {
    const result: TextToken[] = [];
    if (text) {
      let match = TEMPLATE_VARIABLE_REGEXP.exec(text);
      if (!match) {
        result.push({
          text,
          variable: false
        });
        return result;
      }
      let from = 0;
      while (match) {
        const [, variableName] = match;
        const plainText = text.slice(from, match.index);
        if (plainText) {
          result.push({
            text: plainText,
            variable: false
          });
        }
        result.push({
          text: variableName,
          variable: true
        });
        from = TEMPLATE_VARIABLE_REGEXP.lastIndex;
        match = TEMPLATE_VARIABLE_REGEXP.exec(text);
      }
      const plainTextSuffix = text.slice(from, text.length);
      if (plainTextSuffix) {
        result.push({
          text: plainTextSuffix,
          variable: false
        });
      }
    }
    return result;
  }

  private createSVGText(tokens: TextToken[]): SVG.Text {
    const svgText = this._root.text((add) => {
      tokens.forEach(token => {
        const text = !token.letterCount ? token.text : token.text.slice(0, token.letterCount);
        token.tspan = add.tspan(text);
      });
    });
    if (this._font) {
      svgText.font(this._font);
    }
    if (this._color) {
      svgText.fill(this._color);
    }
    if (svgText.length() > this._maxLength) {
      if (tokens.length === 1 && isEllipsisToken(tokens[0])) { // nothing to do if ellipsis text is larger of truncate length
        return svgText;
      }
      // clear from previous step
      svgText.remove();
      tokens.forEach(token => token.tspan = undefined);
      if (!isEllipsisToken(tokens[tokens.length - 1])) { // prepend ellipsis if there is no ellipsis at the end
        return this.createSVGText([...tokens, {
          text: ellipsis,
          variable: false
        }]);
      }
      const lastTokenIdx = tokens.length - 2;
      if (isVariableToken(tokens[lastTokenIdx])) { // do not truncate variable
        return this.createSVGText([...tokens.slice(0, lastTokenIdx), {
          text: ellipsis,
          variable: false
        }]);
      }
      const newLetterCount = (tokens[lastTokenIdx].letterCount ?? tokens[lastTokenIdx].text.length) - 1;
      if (newLetterCount < 1) { // remove token with empty text
        return this.createSVGText([...tokens.slice(0, lastTokenIdx), {
          text: ellipsis,
          variable: false
        }]);
      }
      tokens[lastTokenIdx].letterCount = newLetterCount;
      return this.createSVGText(tokens);
    }
    return svgText;
  }

  public bbox(): SVG.BBox {
    return this._root?.bbox();
  }

  public rbox(element?: SVG.Element): SVG.RBox {
    return this._root?.rbox(element);
  }

  public attr(obj: Object): SvgText {
    this._root.attr(obj);
    return this;
  }

  public translate(x: number, y: number): SvgText {
    this._root.translate(x, y);
    return this;
  }

  public text(val: string): SvgText {
    this._text = val;
    this.render();
    return this;
  }

  public font(font: SVG.FontData): SvgText {
    this._font = font;
    this.render();
    return this;
  }

  public size(size: SVG.NumberAlias): SvgText {
    this._font = {
      ...this._font,
      size
    };
    this.render();
    return this;
  }

  public color(color: string): SvgText {
    this._color = color;
    this.render();
    return this;
  }

  public truncate(length: number): SvgText {
    this._maxLength = length;
    this.render();
    return this;
  }

  public addClass(className: string): SvgText {
    this._root.addClass(className);
    return this;
  }

  public get nativeElement(): SVG.G {
    return this._root;
  }

  public remove(): SVG.Element {
    return this._root.remove();
  }
}
