import * as SVG from 'svg.js';
import { SvgTooltip, TooltipPosition } from './svg-tooltip';

const Config = {
  size: 50,
  color: '#ccc',
  border: {
    color: '#555',
    width: 2
  },
  label: {
    size: 10,
    fill: '#555'
  },
  encap: {
    size: 10,
    label: {
      size: 8
    },
    color: '#38761d',
    border: {
      color: '#38761d',
      width: 4
    },
  }
};

export enum SvgTrafficPortMode {
  DEFAULT,
  ENCAP,
  HIDDEN
}

export enum SvgTrafficPortEncapType {
  UNDEFINED,
  LOCAL,
  RAW,
  VLAN,
  VXLAN
}

export class SvgTrafficPort {
  private _nativeElement: SVG.G;
  private _encapType = SvgTrafficPortEncapType.UNDEFINED;
  private _circle: SVG.Circle;
  private _tooltip: SvgTooltip;

  public constructor(
    private _root: SVG.G,
    private _label: string,
    private _mode = SvgTrafficPortMode.DEFAULT,
    private _editMode = false,
    private _onClick?: Function
  ) {
    this.init();
  }

  private init() {
    const group = this._root.group();

    if (this._mode !== SvgTrafficPortMode.HIDDEN) {
      const circle = this.getCircle();
      const label = this.getLabel();

      group.add(circle);
      group.add(label);
      group.addClass('port');

      if (this._editMode) {
        this.initEvents(group);
      }

      this._circle = circle;
    }

    this._nativeElement = group;
  }

  private getLabel(): SVG.Text {
    const label = this._root.text((add) => add
      .plain(this._label)
      .fill(Config.label.fill)
      .size(this._mode === SvgTrafficPortMode.ENCAP ? Config.encap.label.size : Config.label.size)
    );
    const size = this._mode === SvgTrafficPortMode.ENCAP ? Config.encap.size : Config.size;

    label.attr({
      dx: (size / 2) - (label.rbox().w / 2),
      dy: (size / 2) + (label.rbox().h / 2) - 2
    }).addClass('title');

    return label;
  }

  private getCircle(): SVG.Circle {
    return this._root
      .circle(this._mode === SvgTrafficPortMode.ENCAP ? Config.encap.size : Config.size)
      .stroke({
        color: this._mode === SvgTrafficPortMode.ENCAP ? Config.encap.border.color : Config.border.color,
        width: this._mode === SvgTrafficPortMode.ENCAP ? Config.encap.border.width : Config.border.width
      })
      .fill({
        color: this._mode === SvgTrafficPortMode.ENCAP ? Config.encap.color : Config.color
      });
  }

  private initEvents(elem: SVG.G) {
    elem.on('click.port', (e) => {
      e.stopPropagation();
      this._onClick(e);
    });
  }

  public get element(): SVG.G {
    return this._nativeElement;
  }

  public get label(): string {
    return this._label;
  }

  public get encapType(): SvgTrafficPortEncapType {
    return this._encapType;
  }

  public set encapType(val: SvgTrafficPortEncapType) {
    this._encapType = val;
    this._circle?.removeClass('green');
    this._circle?.removeClass('purple');
    this._circle?.removeClass('orange');
    this._circle?.removeClass('blue');

    switch (val) {
      case SvgTrafficPortEncapType.LOCAL:
        this._circle?.addClass('green');
        break;
      case SvgTrafficPortEncapType.RAW:
        this._circle?.addClass('purple');
        break;
      case SvgTrafficPortEncapType.VLAN:
        this._circle?.addClass('orange');
        break;
      case SvgTrafficPortEncapType.VXLAN:
        this._circle?.addClass('blue');
        break;
    }
  }

  public get tooltip(): SvgTooltip {
    if (!this._tooltip) {
      this._tooltip = new SvgTooltip(
        this._nativeElement,
        { position: TooltipPosition.After }
      );
    }

    return this._tooltip;
  }

  public set tooltip(tooltip: SvgTooltip) {
    this._tooltip = tooltip;
  }
}
