import * as SVG from 'svg.js';
import { PathCoordinates } from '../pipeline.model';
import { SvgTrafficPort, SvgTrafficPortMode } from './svg-traffic-port';

const Config = {
  Interface: {
    cx: 10,
    labelToEdgeGap: 5,
    arrowYPosition: -4,
  },
  Edge: {
    markerWidth: 0.1,
    twinWidth: 25,
    labelFontSize: 10
  },
  Encap: {
    offset: 0.7
  }
};

export enum Direction {
  Uni,
  Bi
}

export class SvgEdge {
  private _nativeElement: SVG.G;
  private _markerText: SVG.Text;
  private _markerArrow: SVG.Text;
  private _path: SVG.PolyLine;
  private _twinPath: SVG.Line;
  private _encapIndicator: SvgTrafficPort;
  private _clickHandlers: Function[] = [];

  public constructor(
    private _root: SVG.G,
    private _editMode: boolean,
    private _coordinates: PathCoordinates,
    private _filter: string,
    private _showEncapIndicator: boolean,
    private _direction: Direction,
    private _onEncapClick?: Function,
    private _hasEdgeHanler = true
  ) {
    this.init();
  }

  private init() {
    const group: SVG.G = this._root.group();
    const markerText = this.getMarkerText(this._coordinates);
    const markerArrow = this.getMarkerArrow(this._coordinates);
    const path = this.getPath(this._coordinates);

    if (this._editMode) {
      // create hidden edge duplicate that is wider than visible one to make the edge easier to click
      const twinPath = this.getTwinPath(this._coordinates);
      group.add(twinPath);
      this._twinPath = twinPath;
    }

    group.add(path);
    group.add(markerArrow);
    group.add(markerText);
    group.addClass('edge-container');

    if (this._hasEdgeHanler) {
      group.addClass('edge-handler');
    }

    if (this._showEncapIndicator) {
      const FLOATING_POINT_FACTOR = 10;

      const encapIndicator = this.getEncapIndicator();
      const pathlength = (path.node as unknown as SVGGeometryElement).getTotalLength();

      const offset = this._coordinates.a[0] > this._coordinates.b[0]
        ? Config.Encap.offset
        : (FLOATING_POINT_FACTOR - Config.Encap.offset * FLOATING_POINT_FACTOR) / FLOATING_POINT_FACTOR;

      const point: SVGPoint = (path.node as unknown as SVGGeometryElement)
        .getPointAtLength(pathlength - (pathlength * offset));

      encapIndicator.element.center(point.x, point.y);
      this._encapIndicator = encapIndicator;
      group.add(encapIndicator.element);
    }

    this._path = path;
    this._markerText = markerText;
    this._markerArrow = markerArrow;
    this._nativeElement = group;

    this.initEvents();
  }

  private initEvents() {
    this._nativeElement.on('click.edge', (e) => {
      e.stopPropagation();
      this._clickHandlers.forEach((handler: Function) => handler.call(this, e));
    });
  }

  private findOffsetPoint(coordinates: PathCoordinates, offset: number): [number, number] {
    const d = this.getPathLength(coordinates);
    const t = offset / d; // desired offset
    const dx = ((1 - t) * coordinates.a[0] + t * coordinates.b[0]);
    const dy = ((1 - t) * coordinates.a[1] + t * coordinates.b[1]);

    return [dx, dy];
  }

  private getEncapIndicator(): SvgTrafficPort {
    return new SvgTrafficPort(
      this._root,
      '',
      SvgTrafficPortMode.ENCAP,
      this._editMode,
      this._onEncapClick.bind(this)
    );
  }

  private getMarkerText(coordinates: PathCoordinates): SVG.Text {
    const markerText = this._root.text(
      (add: SVG.Tspan) => add
        .text(this._filter)
        .addClass(this.labelCssClass)
        // @ts-ignore
        .font({
          size: Config.Edge.labelFontSize
        })
    );
    markerText.addClass('marker-text');
    markerText.attr('dy', -Config.Interface.labelToEdgeGap);
    this.alignText(markerText, coordinates);

    return markerText;
  }

  private getMarkerArrow(coordinates: PathCoordinates): SVG.Text {
    const markerWidth = this.getPathLength(coordinates) * Config.Edge.markerWidth;

    let markerArrow: SVG.Text;
    const arrowSign = this._coordinates.a[0] > this._coordinates.b[0] ? '❮' : '❯';
    switch (this._direction) {
      case Direction.Uni: {
        markerArrow = this._root.text(
          (add: SVG.Tspan) => add
            .tspan(arrowSign)
            .attr('dx', 0)
        )
          .addClass('uni-dir')
          .addClass(this.edgeCssClass);
        break;
      }
      case Direction.Bi: {
        markerArrow = this._root.text(
          (add: SVG.Tspan) => {
            add.tspan('❮')
              .attr('dx', -markerWidth)
              .addClass('bi-arrow-left');

            add.tspan('❯')
              .attr('dx', markerWidth)
              .addClass('bi-arrow-right');
          }
        )
          .addClass(this.edgeCssClass);
        break;
      }
    }

    markerArrow.addClass('marker-arrow');
    markerArrow.attr('dy', -Config.Interface.arrowYPosition);
    this.alignText(markerArrow, coordinates);

    return markerArrow;
  }

  private getPathLength(coordinates: PathCoordinates): number {
    return Math.sqrt(
      Math.pow(coordinates.b[0] - coordinates.a[0], 2) + Math.pow(coordinates.b[1] - coordinates.a[1], 2)
    );
  }

  private getPath(coordinates: PathCoordinates): SVG.PolyLine {
    const path = this._root.polyline([coordinates.a, coordinates.b]);
    path.addClass(this.edgeCssClass);

    return path;
  }

  private getTwinPath(coordinates: PathCoordinates): SVG.Line {
    // make a twin path to start little further from the center of the interface
    const [dx1, dy1] = this.findOffsetPoint(coordinates, Config.Interface.cx);
    const [dx2, dy2] = this.findOffsetPoint(coordinates, Config.Interface.cx);

    const twinPath = this._root
      .line([dx1, dy1, dx2, dy2])
      .stroke({
        width: Config.Edge.twinWidth
      });

    twinPath.addClass('edge-handler');
    return twinPath;
  }

  /**
   * Run text along the line
   * @param text - SVG Text element
   * @param coordinates  - coordinates of the line
   */
  private alignText(text: SVG.Text, coordinates: PathCoordinates) {
    const [x1, y1] = coordinates.a;
    const [x2, y2] = coordinates.b;

    const plotPath: string = x1 < x2 ? `M${x1},${y1} ${x2},${y2}Z` : `M${x2},${y2} ${x1},${y1}Z`;
    if (text.textPath()) {
      (text as unknown as SVG.Path).plot(plotPath);
    } else {
      text.path(plotPath);
      text.textPath().attr('startOffset', '25%');
    }
  }

  public moveEdge(coordinates: PathCoordinates, filterAb: boolean) {
    if (filterAb) {
      this._path.plot([coordinates.a, coordinates.b]);
    } else {
      this._path.plot([coordinates.b, coordinates.a]);
    }

    const [dx1, dy1] = this.findOffsetPoint(coordinates, Config.Interface.cx);
    const [dx2, dy2] = this.findOffsetPoint(coordinates, Config.Interface.cx);

    this._twinPath?.plot([dx1, dy1, dx2, dy2]);

    const markerWidth = this.getPathLength(coordinates) * Config.Edge.markerWidth;
    const [[startX, startY], [endX, endY]]
      = this._path.array().value as unknown as [[number, number], [number, number]];
    const updatedCoordinates: PathCoordinates = {
      a: [startX, startY],
      b: [endX, endY]
    };

    if (this._markerArrow.hasClass('uni-dir')) {
      const arrowSign = startX > endX ? '❮' : '❯';
      this._markerArrow.text((add: SVG.Text) => add
        .tspan(arrowSign)
        .attr('dx', 0)
      );
      this._markerArrow.attr('dy', -Config.Interface.arrowYPosition);
    }

    this.alignText(this._markerText, updatedCoordinates);
    this.alignText(this._markerArrow, updatedCoordinates);

    this._markerArrow.node.querySelector('.bi-arrow-left').setAttribute('dx', (-markerWidth / 2).toString());
    this._markerArrow.node.querySelector('.bi-arrow-right').setAttribute('dx', markerWidth.toString());

    if (this._encapIndicator) {
      const pathlength = (this._path.node as unknown as SVGGeometryElement).getTotalLength();
      const point: SVGPoint = (this._path.node as unknown as SVGGeometryElement)
        .getPointAtLength(pathlength - (pathlength * Config.Encap.offset));
      this._encapIndicator.element.center(point.x, point.y);
    }
  }

  public onClick(fn: Function) {
    this._clickHandlers.push(fn);
  }

  private get edgeCssClass(): string {
    return (this._filter !== 'ALL') ? 'edge-filter' : 'edge';
  }

  private get labelCssClass(): string {
    return (this._filter !== 'ALL') ? 'label-filter' : 'label';
  }

  public get encapIndicator(): SvgTrafficPort {
    return this._encapIndicator;
  }

  public get element(): SVG.G {
    return this._nativeElement;
  }
}
