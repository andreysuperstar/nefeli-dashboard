import * as SVG from 'svg.js';

import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { SvgText } from './svg-text';

export interface SiteGroupRect {
  x: number;
  y: number;
  width: number | string;
  height: number | string;
}

export enum SiteGroupMode {
  DEFAULT,
  NEW
}

export const Config = {
  color: '#555',
  borderRadius: 10,
  label: {
    size: 15,
    weight: 800,
    pos: {
      top: 15,
      left: 25
    }
  },
  variable: {
    color: '#ffe6cc',
    border: {
      color: '#d79b00',
      width: 1,
      radius: 5
    }
  },
  description: {
    lgFontOpts: {
      weight: '800',
      'font-size': '18pt',
      fill: '#888'
    },
    smFontOpts: {
      weight: '200',
      'font-size': '8pt',
      fill: '#888'
    }
  },
  removeIcon: {
    path: 'assets/icon-remove-pipeline-node.svg',
    width: 30
  },
};

export interface SvgSiteGroupOptions {
  editMode?: boolean;
  isTemplateInstantiateMode?: boolean;
  onDeleteClick?: Function;
}

export class SvgSiteGroup {
  private _nativeElement: SVG.G;
  private _newSiteDescription: SVG.G;
  private _worldPort: SVG.G;
  private _border: SVG.Rect;
  private _trafficPorts: SVG.G[] = [];
  private _trafficPortsGroup: SVG.G;
  private _removeBtn: SVG.Image;

  public constructor(
    private _root: SVG.G,
    private _rect: SiteGroupRect,
    private _site: SiteConfig,
    private _mode = SiteGroupMode.DEFAULT,
    private _options?: SvgSiteGroupOptions
  ) {
    this.init();
  }

  private init() {
    const group: SVG.G = this._root.group();
    group.opacity(0);

    this._border = this.getBorder(group);
    this.createLabel(group);

    group.add(this._border);

    group.translate(this._rect.x, this._rect.y);
    group.addClass('site');

    if (this._mode === SiteGroupMode.NEW) {
      this._newSiteDescription = this.getNewSiteDescription(group, this._border);
      group.add(this._newSiteDescription);
    }

    if (!this._options?.isTemplateInstantiateMode) {
      this._removeBtn = this.createRemoveButton(group, this._border);

      group.add(this._removeBtn);
    }

    group.animate().attr({ opacity: 1 });

    this._nativeElement = group;
  }

  private getNewSiteDescription(root: SVG.G, rect: SVG.Rect): SVG.G {
    const group = root.group();
    const arrowSize = 200;
    const rotate = 90;

    const image = group.image(
      'assets/drag-drop-arrow.svg',
      arrowSize,
      arrowSize);

    image
      .addClass('drag-drop-arrow')
      .center(
        rect.bbox().cx,
        rect.bbox().cy
      )
      .rotate(rotate);

    const textBox = group.text((add) => {
      ((add.tspan('Drag Network Functions') as unknown as SVG.Text)
        .font(Config.description.lgFontOpts) as unknown as SVG.Tspan).newLine();
      ((add.tspan('&') as unknown as SVG.Text)
        .font(Config.description.lgFontOpts) as unknown as SVG.Tspan).newLine();
      ((add.tspan('Drop Here') as unknown as SVG.Text)
        .font(Config.description.lgFontOpts) as unknown as SVG.Tspan).newLine();
      ((add.tspan('Direct traffic by connecting edges between network') as unknown as SVG.Text)
        .font(Config.description.smFontOpts) as unknown as SVG.Tspan).newLine();
      ((
        add.tspan(`function interfaces and to the ${this.type === SiteConfigSiteType.CLUSTER ? 'WORLD' : 'appropriate site interface'}`
        ) as unknown as SVG.Text)
        .font(Config.description.smFontOpts) as unknown as SVG.Tspan).newLine();
    });

    textBox.attr({
      'dominant-baseline': 'central',
      'text-anchor': 'middle',
      x: rect.bbox().cx,
      y: rect.bbox().cy - (textBox.bbox().height / 2)
    });

    group.add(textBox);
    group.addClass('new-site-description');

    return group;
  }

  private createLabel(group: SVG.G): SvgText {
    const result: SvgText = new SvgText(group, this.name);
    result.font({ weight: `${Config.label.weight}`, size: Config.label.size }).color(Config.color);
    result.translate(Config.label.pos.top, Config.label.pos.left).addClass('title');
    return result;
  }

  private createRemoveButton(group: SVG.G, rect: SVG.Rect): SVG.Image {
    const removeBtn: SVG.Image = group.image(Config.removeIcon.path, Config.removeIcon.width).addClass('remove-icon');
    removeBtn.on('click.removeSite', (e: Event) => {
      e.stopPropagation();
      this._options?.onDeleteClick?.(this._site?.identifier);
    });
    const borderBox: SVG.BBox = rect.bbox();
    removeBtn.translate(borderBox.width - Config.removeIcon.width, 0);
    if (!this._options?.editMode) {
      removeBtn.hide();
    }
    return removeBtn;
  }

  private getBorder(group): SVG.Rect {
    return group
      .rect(this._rect.width, this._rect.height)
      .stroke({ color: Config.color, width: 2 })
      .fill({ color: '#ccc', opacity: 0.1 })
      .attr({
        rx: Config.borderRadius, ry: Config.borderRadius
      });
  }

  public hideNewSiteDescription() {
    this._newSiteDescription?.hide();
  }

  public showNewSiteDescription() {
    this._newSiteDescription?.show();
  }

  public hideRemoveButton() {
    this._removeBtn?.hide();
  }

  public showRemoveButton() {
    this._removeBtn?.show();
  }

  public portsToFront() {
    this._worldPort?.front();
    this._trafficPortsGroup?.front();
  }

  public get nativeElement(): SVG.G {
    return this._nativeElement;
  }

  public get worldPort(): SVG.G {
    return this._worldPort;
  }

  public set worldPort(val: SVG.G) {
    this._worldPort = val;
  }

  public set trafficPortsGroup(val: SVG.G) {
    this._trafficPortsGroup = val;
  }

  public get border(): SVG.Rect {
    return this._border;
  }

  public get trafficPorts(): SVG.G[] {
    return this._trafficPorts;
  }

  public get type(): SiteConfigSiteType {
    return this._site?.siteType ?? SiteConfigSiteType.CLUSTER;
  }

  public get site(): SiteConfig {
    return this._site;
  }

  public get name(): string {
    return this._site.name ?? this._site.identifier;
  }
}
