import * as SVG from 'svg.js';
import { Component, DebugElement, OnInit } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DOCUMENT } from '@angular/common';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { LoggerService } from '../../shared/logger.service';
import { Config } from '../pipeline.component';
import { SiteGroupMode, SvgSiteGroup, Config as SVGSiteGroupConfig } from './svg-site-group';
import site from '../../../../mock/site';
import { By } from '@angular/platform-browser';
import { HttpClientTestingModule } from '@angular/common/http/testing';

@Component({
  selector: 'dummy-component',
  template: '<div id="drawing"></div>',
  styles: [
    '#drawing { width: 500px; height: 500px }'
  ]

})
class DummyComponent implements OnInit {
  private _root: SVG.G;
  private _drawing: SVG.Doc;

  public ngOnInit(): void {
    this._drawing = SVG('drawing').size(500, 500);
    this._root = this._drawing.group();
    this._root.size(500, 500);
  }

  public get root(): SVG.G {
    return this._root;
  }
}

describe('SvgSiteGroup', () => {
  let fixture: ComponentFixture<DummyComponent>;
  let component: DummyComponent;
  let document: Document;
  let logger: LoggerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [NoopAnimationsModule, HttpClientTestingModule],
      declarations: [DummyComponent]
    });
    document = TestBed.inject(DOCUMENT);
    logger = TestBed.inject(LoggerService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DummyComponent);
    component = fixture.componentInstance;
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    const svgSite = new SvgSiteGroup(component.root,
      {
        x,
        y,
        width,
        height,
      },
      site.config,
      SiteGroupMode.NEW);

    expect(svgSite).toBeTruthy();
    expect(svgSite.nativeElement.hasClass('site')).toBeTruthy();
    expect(svgSite.site).toEqual(site.config);
    expect(svgSite.type).toEqual(site.config.siteType);
    expect(svgSite.name).toEqual(site.config.name);
    expect(svgSite.border).toBeTruthy();

    const titleEl: SVG.Set = svgSite.nativeElement.select('.title');
    expect(titleEl.length()).toEqual(1);
    const siteName = site.config.name[0].toUpperCase() + site.config.name.slice(1);
    expect(titleEl.first().native().textContent).toEqual(siteName);

    const dropEl: SVG.Set = svgSite.nativeElement.select('.drag-drop-arrow');
    expect(dropEl.length()).toEqual(1);
    expect(dropEl.first().native().getAttribute('href')).toEqual('assets/drag-drop-arrow.svg');

    const newSiteDescEl: SVG.Set = svgSite.nativeElement.select('.new-site-description');
    expect(newSiteDescEl.length()).toEqual(1);
    expect(newSiteDescEl.first().native().textContent).toEqual('Drag Network Functions&Drop HereDirect traffic by connecting edges between networkfunction interfaces and to the WORLD');

    const removeIconEl: SVG.Set = svgSite.nativeElement.select('.remove-icon');
    expect(removeIconEl.length()).toEqual(1);
    let removeIconHtmlEl: HTMLElement = removeIconEl.first().native();
    expect(removeIconHtmlEl.getAttribute('href')).toEqual('assets/icon-remove-pipeline-node.svg');
    expect(removeIconHtmlEl.getAttribute('width')).toEqual(`${SVGSiteGroupConfig.removeIcon.width}`);
    expect(removeIconHtmlEl.getAttribute('height')).toEqual(`${SVGSiteGroupConfig.removeIcon.width}`);
    expect(removeIconHtmlEl.style.display).toEqual('none');
  });

  it('should display remove button', () => {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    const svgSite = new SvgSiteGroup(component.root,
      {
        x,
        y,
        width,
        height,
      },
      site.config,
      SiteGroupMode.NEW,
      {
        editMode: true
      });
    const removeIconEl: SVG.Set = svgSite.nativeElement.select('.remove-icon');
    expect(removeIconEl.length()).toEqual(1);
    let removeIconHtmlEl: HTMLElement = removeIconEl.first().native();
    expect(removeIconHtmlEl.style.display).toEqual('');
  });

  it('should hide Remove button', () => {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    const svgSite = new SvgSiteGroup(component.root,
      {
        x,
        y,
        width,
        height,
      },
      site.config,
      SiteGroupMode.NEW,
      {
        editMode: true,
        isTemplateInstantiateMode: true
      }
    );

    const removeIconEl: SVG.Set = svgSite.nativeElement.select('.remove-icon');

    expect(removeIconEl.length()).toEqual(0);
  });

  it('should propagate on delete event', () => {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    const onDeleteClick = jasmine.createSpy('close');

    const svgSite = new SvgSiteGroup(component.root,
      {
        x,
        y,
        width,
        height,
      },
      site.config,
      SiteGroupMode.NEW,
      {
        editMode: true,
        onDeleteClick
      });
    expect(svgSite).toBeTruthy();
    const removeIconDe: DebugElement = fixture.debugElement.query(By.css('.remove-icon'));
    removeIconDe.nativeElement.dispatchEvent(new Event('click'));
    removeIconDe.triggerEventHandler('click', {});
    fixture.detectChanges();
    expect(onDeleteClick).toHaveBeenCalledWith(site.config.identifier);
  });

  it('should show/hide remove button programmatically', () => {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    const svgSite = new SvgSiteGroup(component.root,
      {
        x,
        y,
        width,
        height,
      },
      site.config,
      SiteGroupMode.NEW,
      {
        editMode: true,
      });
    expect(svgSite).toBeTruthy();
    const removeIconDe: DebugElement = fixture.debugElement.query(By.css('.remove-icon'));
    expect(removeIconDe.nativeElement.style.display).toEqual('');

    svgSite.hideRemoveButton();
    fixture.detectChanges();
    expect(removeIconDe.nativeElement.style.display).toEqual('none');

    svgSite.showRemoveButton();
    fixture.detectChanges();
    expect(removeIconDe.nativeElement.style.display).toEqual('');
  });

  it('should show/hide "new site description" programmatically', () => {
    const width = '48%';
    const height = Config.SiteGroup.height;
    const x = Config.SiteGroup.padding;
    const y = Config.SiteGroup.padding;

    const svgSite = new SvgSiteGroup(component.root,
      {
        x,
        y,
        width,
        height,
      },
      site.config,
      SiteGroupMode.NEW);
    expect(svgSite).toBeTruthy();
    const newSiteDescriptionDe: DebugElement = fixture.debugElement.query(By.css('.new-site-description'));
    expect(newSiteDescriptionDe.nativeElement.style.display).toEqual('');

    svgSite.hideNewSiteDescription();
    fixture.detectChanges();
    expect(newSiteDescriptionDe.nativeElement.style.display).toEqual('none');

    svgSite.showNewSiteDescription();
    fixture.detectChanges();
    expect(newSiteDescriptionDe.nativeElement.style.display).toEqual('');
  });
});
