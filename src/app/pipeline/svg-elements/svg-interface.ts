import * as SVG from 'svg.js';
import { SvgGear } from './svg-gear';

const Config = {
  width: 20,
  height: 20,
  cornerRadius: 3,
  dotSize: 7,
  cx: 10,
  cy: 10,
  maxLabelSize: 135,
  tcpDump: {
    offset: {
      x: -2.5,
      y: 25
    }
  }
};

export interface Settings {
  handlerArea: boolean;
}

export class SvgInterface {
  private _nativeElement: SVG.G;
  private _settings: Settings;
  private _captureButton: SvgGear;
  private _name: string;

  public constructor(private _root: SVG.G, private settings?: Settings) {
    this._settings = settings || { handlerArea: false };
    this.init();
  }

  private init() {
    const group = this._root.group();
    const rect = this.getPortRect();
    const dot = this.getDot();
    group.add(rect);
    if (this._settings.handlerArea) {
      const handlerRect = this.getHandlerRect();
      group.add(handlerRect);
    }
    group.add(dot);
    group.addClass('nf-port');
    this._nativeElement = group;
  }

  private getPortRect(): SVG.Rect {
    return this._root
      .rect(Config.width, Config.height)
      .attr({ rx: Config.cornerRadius, ry: Config.cornerRadius })
      .addClass('interface-box');
  }

  private getDot(): SVG.Circle {
    return this._root
      .circle(Config.dotSize)
      .attr({
        cx: Config.cx,
        cy: Config.cy
      });
  }

  private getHandlerRect(): SVG.Rect {
    return this._root
      .rect(Config.width, Config.height + Config.height / 2)
      .attr({ opacity: '0' })
      .addClass('handlers');
  }

  public showLabel() {
    const label = (this._nativeElement as any).select('.inf-label').members[0];

    if (label) {
      label.removeClass('hidden');
    }
  }

  public hideLabel() {
    const label = (this._nativeElement as any).select('.inf-label').members[0];

    if (label) {
      label.addClass('hidden');
    }
  }

  public get element(): SVG.G {
    return this._nativeElement;
  }

  public set captureButton(gear: SvgGear) {
    this._captureButton = gear;
    this._nativeElement.add(gear.element);
  }

  public get captureButton(): SvgGear {
    return this._captureButton;
  }

  public get name(): string {
    return this._name;
  }

  public set name(val: string) {
    this._name = val;
  }
}
