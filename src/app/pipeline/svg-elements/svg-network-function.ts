import { NFInterface } from '../network-function.model';
import { PipelineNfInstance, PipelineNode } from '../pipeline.model';
import { SvgGear, Config as SvgGearConfig } from './svg-gear';
import { SvgInterface } from './svg-interface';
import { SvgPill } from './svg-pill';
import { SvgStatusDot } from './svg-status-dot';
import * as SVG from 'svg.js';
import { SvgText } from './svg-text';

const Config = {
  variable: {
    color: '#ffe6cc',
    border: {
      color: '#d79b00',
      width: 1,
      radius: 5
    }
  },
  NF: {
    width: 250,
    smHeight: 75,
    lgHeight: 115,
    siteHeaderHeight: 16,
    topBorder: 7,
    cornerRadius: 4,
    topMargin: 19,
    margin: 10,
    leftMargin: 15,
    portOffset: 20,
    titleFontSize: 12,
    maxTitleSize: 220,
    bodyFontSize: 15,
    bodyTopMargin: 32,
    instanceGap: 5,
    lopTopMargin: 68,
    lopGap: 5,
    borderDefaultColor: 'dark-grey',
    status: {
      noInstanceErrorMessage: 'No instances to launch'
    }
  },
  Interface: {
    width: 20,
    cornerRadius: 3,
    maxLabelSize: 135,
    tcpDump: {
      offset: {
        x: -2.5,
        y: 25
      }
    },
    anchorOffset: {
      top: -20,
      bottom: -50,
      middle: 14,
      left: -10,
      right: 30
    }
  },
  removeIcon: {
    path: 'assets/icon-remove-pipeline-node.svg',
    width: 20
  },
  Trace: {
    color: {
      throughput: '#5959ff',
      latency: '#ffa750',
      loss: '#63e0ae'
    }
  }
};

const xNfMiddle = Config.NF.width / 2 - Config.Interface.width / 2;
const xNfLeft = Config.NF.portOffset;
const xNfLeftMiddle = (Config.NF.width / 2) - Config.Interface.width - Config.NF.portOffset / 2;
const xNfRight = Config.NF.width - Config.Interface.width - Config.NF.portOffset;
const xNfRightMiddle = (Config.NF.width / 2) + (Config.NF.portOffset / 2);
const yNfTop = -Config.NF.lgHeight - Config.NF.siteHeaderHeight;
const yNfSmTop = -Config.NF.smHeight - Config.NF.siteHeaderHeight;
const yNfMiddle = -(Config.NF.lgHeight + Config.NF.siteHeaderHeight) / 2;
const yNfSmMiddle = -(Config.NF.smHeight + Config.NF.siteHeaderHeight) / 2;

export interface NFRectSettings {
  node: PipelineNode;
  instance?: PipelineNfInstance;
  index: number;
  animate?: boolean;
  opacity?: number;
}

enum PortIndex {
  BOTTOM_LEFT,
  BOTTOM_MIDDLE,
  BOTTOM_RIGHT,
  TOP_LEFT,
  TOP_MIDDLE,
  TOP_RIGHT,
  MIDDLE_LEFT,
  MIDDLE_RIGHT
}

enum NfPortTotal {
  ONE = 1,
  TWO,
  THREE,
  FOUR,
  FIVE
}

export interface NFPortTranslateSettings {
  dx?: number;
  dy?: number;
  anchor?: TextAnchor;
}

export enum TextAnchor {
  Start = 'start',
  Middle = 'middle',
  End = 'end'
}

// TODO (anton): update to `text-overflow: ellipsis` when it will be supported by browsers and SVG.js lib
export const TruncateElementText = (element: SVG.Text, maxLength: number) => {
  if (element.length() <= maxLength) {
    return;
  }

  const ellipsisLength = 10;
  let text: string = element.text();

  maxLength -= ellipsisLength;

  while (element.length() > maxLength) {
    text = text
      .slice(0, -1)
      .trim();

    element.text(text);
  }

  text += '…';

  element.text(text);
};

export interface SvgNetworkFunctionOptions {
  editMode?: boolean;
  isTemplateInstantiateMode?: boolean;
  isLogicalView?: boolean;
  isPerformanceTraceView?: boolean;
  getBorderColor: Function;
  onInterfaceClick?: Function;
  onInterfaceSettingsClick?: Function;
  onSettingsClick?: Function;
  onDeleteNF?: Function;
  onStatusDotClick?: Function;
}

export class SvgNetworkFunction {
  private _nativeElement: SVG.G;
  private _settingsButton: SvgGear;
  private _border: SVG.Rect;
  private _nodeName: SvgText;

  public constructor(
    private _root: SVG.G,
    private _node: PipelineNode,
    private _options?: SvgNetworkFunctionOptions) {
    this.init();
  }

  private init() {
    const group: SVG.G = this._root.group();

    const instances: SVG.G[] = [];
    let i = 0;

    if (!this._node.nf.local.instances || !this._node.nf.local.instances.length) {
      const nFRect: SVG.G = this.getNFRect(group, { node: this._node, index: 0 });
      this._settingsButton = this.createSettingsGear(
        nFRect,
        Config.NF.width - SvgGearConfig.width,
        Config.NF.smHeight - SvgGearConfig.height
      );
      if (!this._options.editMode && !this._options.isLogicalView) {
        nFRect.attr({
          opacity: 0.5
        });
      }
      group.add(nFRect);
      this.initNodeEvents();
    } else {
      for (const instance of this._node.nf.local.instances) {
        const nFRect: SVG.G = this.getNFRect(group, { node: this._node, instance, index: i++ });
        instances.push(nFRect);
        group.add(nFRect);

        if (this._options.editMode) {
          // if in edit mode, only render one instance
          break;
        }
      }
    }

    if (this._node.nf.local.interfaces) {
      instances.forEach((instance: SVG.G, instanceIndex: number) => {
        const nfPortGroup: SVG.G = this.drawNFPorts(group, this._node, instanceIndex);
        if (instanceIndex === 0) {
          group.add(nfPortGroup);
        } else {
          instance.add(nfPortGroup);
          nfPortGroup.hide();
          this.initInstanceEvents(instance, nfPortGroup);
        }
        this._node.local.defs.push(nfPortGroup);
      });

      if (!this._node.nf.local.instances || !this._node.nf.local.instances.length) {
        const nfPortGroup: SVG.G = this.drawNFPorts(group, this._node, 0);
        group.add(nfPortGroup);
        this._node.local.defs.push(nfPortGroup);
        if (!this._options.editMode && !this._options.isLogicalView) {
          nfPortGroup.attr({
            opacity: 0.5
          });
        }
      }
    }

    group.addClass('nf');
    this._nativeElement = group;
  }
  public getNFRect(root: SVG.G, settings: NFRectSettings): SVG.G {
    const group = root.group();
    let { animate = false } = settings;
    const { node, instance, index } = settings;
    const { opacity = this._options.isLogicalView && index > 0 ? 0 : 1 } = settings;
    let topBorderPattern = instance?.local?.topBorderPattern;

    if (animate && !opacity) {
      animate = false;
    }

    node.local.defs = node.local.defs || [];

    if (!topBorderPattern) {
      let machineId: string;

      if (!this._options.editMode && instance) {
        machineId = instance?.location?.placement?.machineId;
      }
      const color = instance ? this._options.getBorderColor(machineId, node.nf.local.vendor) : Config.NF.borderDefaultColor;
      topBorderPattern = this._root.pattern(Config.NF.width, Config.NF.lgHeight, (add) => {
        add.rect(Config.NF.width, Config.NF.topBorder).addClass('top-border ' + color);
        add.rect(Config.NF.width, Config.NF.lgHeight - Config.NF.topBorder)
          .move(0, Config.NF.topBorder).addClass('rect-body');
      });
      node.local.defs.push(topBorderPattern);
      if (instance) {
        instance.local.topBorderPattern = topBorderPattern;
      }
    }

    this._border = this._root.rect(Config.NF.width, Config.NF.smHeight)
      .attr({
        fill: topBorderPattern,
        rx: Config.NF.cornerRadius,
        ry: Config.NF.cornerRadius
      })
      .addClass('nf-rect');
    group.add(this._border);

    const vendor = (node.nf.local.vendor) ? node.nf.local.vendor : 'Network Function';
    const text = this._root.text((add) => {
      add.tspan(vendor);
    }).font({
      size: Config.NF.titleFontSize
    }).addClass('title');
    const titleHeight = text.bbox().height;
    const newLineMargin = titleHeight;
    text.attr({
      x: Config.NF.leftMargin,
      y: Config.NF.topMargin + newLineMargin
    });
    group.add(text);
    node.local.defs.push(text);

    const name = node.name || node.nf.local.name;
    this._nodeName = new SvgText(group, name, Config.NF.maxTitleSize);
    const LINE_SPACING = 2;
    this._nodeName.translate(
      Config.NF.leftMargin
      , Config.NF.topMargin + titleHeight + newLineMargin + LINE_SPACING
    ).font({
      size: Config.NF.bodyFontSize
    }).addClass('content');
    node.local.defs.push(this._nodeName.nativeElement);

    if (this._options.editMode && !this.options.isTemplateInstantiateMode) {
      // add "remove" icon
      const removeIcon = this._root.image(Config.removeIcon.path).addClass('remove-icon');
      removeIcon.translate(Config.NF.width - Config.removeIcon.width, Config.NF.topBorder);
      group.add(removeIcon);
      node.local.defs.push(removeIcon);
      removeIcon.on('click.removeNF', (e: Event) => {
        e.stopPropagation();
        this._options.onDeleteNF(node);
      });
    }

    group.move(0, -((Config.NF.smHeight + Config.NF.instanceGap) * index));
    if (instance) {
      instance.local.ctx = group;
    }

    // TODO(ecarino): animate on scale-up not properly working
    if (animate) {
      group.attr({ opacity: 0 });
      group.animate().attr({ opacity: 1 });
    }

    if (index === 0 && !this._options.editMode) {
      const lopGroup = this._root.group();
      const spThroughput: SvgPill = new SvgPill(this._root, Config.Trace.color.throughput);
      const spLatency: SvgPill = new SvgPill(this._root, Config.Trace.color.latency);
      const spLoss: SvgPill = new SvgPill(this._root, Config.Trace.color.loss);
      node.local.lop = {
        latency: spLatency,
        throughput: spThroughput,
        loss: spLoss
      };
      lopGroup.add(spThroughput.nativeElement);
      lopGroup.add(spLatency.nativeElement.translate(spThroughput.width + Config.NF.lopGap, 0));
      lopGroup.add(spLoss.nativeElement.translate(spThroughput.width + spLatency.width + (2 * Config.NF.lopGap), 0));
      lopGroup.translate((Config.NF.width / 2) - (lopGroup.bbox().w / 2), Config.NF.lopTopMargin);
      lopGroup.addClass('lop-stats');
      lopGroup.attr({ opacity: 0 });
      node.local.defs.push(lopGroup);

      group.add(lopGroup);
    }

    // instance status dot
    const dot: SvgStatusDot = new SvgStatusDot(this._root);
    dot.nativeElement.translate(
      Config.NF.width - dot.diameter - Config.NF.margin,
      Config.NF.smHeight - dot.diameter - Config.NF.margin);
    group.add(dot.nativeElement);

    if (instance) {
      instance.local.statusIndicator = dot;
    } else {
      node.local.statusIndicator = dot;
      dot.errorTooltip.text(Config.NF.status.noInstanceErrorMessage);
    }

    dot.nativeElement.mousedown(() => {
      this._options.onStatusDotClick(instance);
    });

    node.local.defs.push(dot.nativeElement);

    group.addClass(node.nf.local.name);
    group.addClass('nf-instance');
    group.addClass(instance ? 'scaled-instance' : 'main-instance');
    group.attr({ opacity });

    return group;
  }

  public initNodeEvents() {
    this._settingsButton.element.on('mousedown', (event: MouseEvent) => {
      this._options.onSettingsClick(event, this._node);
    });
  }

  private drawNFPorts(root: SVG.G, node: PipelineNode, instanceIndex: number): SVG.G {
    const nfPorts: NFInterface[] = node.nf.local.interfaces;
    const nfPortGroup = root.group();
    const portsCount = nfPorts.length;
    let index = 0;
    let textAnchor: TextAnchor;

    for (const nfPort of nfPorts) {
      const portName = nfPort.id;
      const interFace: SvgInterface = new SvgInterface(root, { handlerArea: !this._options.editMode });
      const inf: SVG.G = interFace.element;
      if (instanceIndex === 0) {
        nfPort.local = { ctx: inf };
      }

      const settings: NFPortTranslateSettings = this.getPortTranslateSettings(index, portsCount);
      inf.translate(settings.dx, settings.dy);
      textAnchor = settings.anchor;

      const label = this.makeInterfaceLabel(nfPortGroup, portName, textAnchor, index);
      inf.add(label);
      nfPortGroup.add(inf);

      const settingsGroup: SvgGear = this.createSettingsGear(
        nfPortGroup,
        Config.Interface.tcpDump.offset.x,
        Config.Interface.tcpDump.offset.y
      );
      settingsGroup.hide();
      interFace.captureButton = settingsGroup;
      settingsGroup.element.front();
      interFace.name = portName;

      if (this._options.editMode) {
        this.initInterfaceEvents(interFace);
      } else {
        this.initTcpSettingsEvents(interFace, node, instanceIndex);
      }
      index++;
      textAnchor = undefined;
    }

    nfPortGroup.move(0, Config.NF.smHeight - Config.NF.cornerRadius);
    nfPortGroup.back();
    return nfPortGroup;
  }

  public getPortTranslateSettings(index: number, portsCount: number): NFPortTranslateSettings {
    let settings: NFPortTranslateSettings = {};
    switch (portsCount) {
      case NfPortTotal.ONE:
        settings = this.getPortTranslation(PortIndex.BOTTOM_MIDDLE);
        break;
      case NfPortTotal.TWO:
        switch (index) {
          case 0:
            settings.dx = xNfLeftMiddle;
            settings.anchor = TextAnchor.End;
            break;
          case 1:
            settings.dx = xNfRightMiddle;
            settings.anchor = TextAnchor.Start;
            break;
        }
        break;
      case NfPortTotal.THREE:
        settings = this.getPortTranslation(index);
        break;
      case NfPortTotal.FOUR:
        switch (index) {
          // tslint:disable-next-line: no-magic-numbers
          case 3:
            settings = this.getPortTranslation(PortIndex.TOP_MIDDLE);
            break;
          default:
            settings = this.getPortTranslation(index);
        }
        break;
      case NfPortTotal.FIVE:
        switch (index) {
          // tslint:disable-next-line: no-magic-numbers
          case 3:
            settings.dx = xNfLeftMiddle;
            settings.dy = yNfTop;
            settings.anchor = TextAnchor.End;
            break;
          // tslint:disable-next-line: no-magic-numbers
          case 4:
            settings.dx = xNfRightMiddle;
            settings.dy = yNfTop;
            settings.anchor = TextAnchor.Start;
            break;
          default:
            settings = this.getPortTranslation(index);
        }
        break;
      default:
        settings = this.getPortTranslation(index);
        break;
    }
    return settings;
  }

  private getPortTranslation(portIndex: number): NFPortTranslateSettings {
    const settings: NFPortTranslateSettings = {};

    switch (portIndex) {
      case PortIndex.BOTTOM_LEFT:
        settings.dx = xNfLeft;
        settings.anchor = TextAnchor.End;
        break;
      case PortIndex.BOTTOM_MIDDLE:
        settings.dx = xNfMiddle;
        settings.anchor = TextAnchor.Middle;
        break;
      case PortIndex.BOTTOM_RIGHT:
        settings.dx = xNfRight;
        settings.anchor = TextAnchor.Start;
        break;
      case PortIndex.TOP_LEFT:
        settings.dx = xNfLeft;
        settings.dy = this._options.isPerformanceTraceView ? yNfTop : yNfSmTop;
        settings.anchor = TextAnchor.End;
        break;
      case PortIndex.TOP_MIDDLE:
        settings.dx = xNfMiddle;
        settings.dy = this._options.isPerformanceTraceView ? yNfTop : yNfSmTop;
        settings.anchor = TextAnchor.Middle;
        break;
      case PortIndex.TOP_RIGHT:
        settings.dx = xNfRight;
        settings.dy = this._options.isPerformanceTraceView ? yNfTop : yNfSmTop;
        settings.anchor = TextAnchor.Start;
        break;
      case PortIndex.MIDDLE_LEFT:
        settings.dx = -Config.Interface.width + Config.Interface.cornerRadius / 2;
        settings.dy = this._options.isPerformanceTraceView ? yNfMiddle : yNfSmMiddle;
        settings.anchor = TextAnchor.End;
        break;
      case PortIndex.MIDDLE_RIGHT:
        settings.dx = Config.NF.width - Config.Interface.cornerRadius / 2;
        settings.dy = this._options.isPerformanceTraceView ? yNfMiddle : yNfSmMiddle;
        settings.anchor = TextAnchor.Start;
    }
    return settings;
  }

  private initInstanceEvents(instance: SVG.G, nfPortGroup: SVG.G) {
    instance.on('mouseenter.instance', (e) => {
      if (!this._options.editMode && !this._options.isLogicalView) {
        nfPortGroup.show();
      }
    });
    instance.on('mouseleave.instance', (e) => {
      if (!this._options.editMode && !this._options.isLogicalView) {
        nfPortGroup.hide();
      }
    });
  }

  private initTcpSettingsEvents(
    interFace: SvgInterface,
    node: PipelineNode,
    instanceIndex: number
  ) {
    const inf: SVG.G = interFace.element;
    inf.on('mouseenter.inf', (e) => {
      if (!this._options.editMode) {
        if (this._options.isLogicalView) {
          interFace.showLabel();
        } else {
          this.onInterfaceMouseEnter(e, interFace, node);
        }
      }
    });
    inf.on('mouseleave.inf', (e) => {
      if (!this._options.editMode) {
        if (this._options.isLogicalView) {
          interFace.hideLabel();
        } else {
          this.onInterfaceMouseLeave(e, interFace, node);
        }
      }
    });

    interFace.captureButton.element.on('mousedown', (event: MouseEvent) => {
      this._options.onInterfaceSettingsClick(event, node, instanceIndex, interFace.name);
    });
  }

  private initInterfaceEvents(interFace: SvgInterface): void {
    /* TODO(eric): temporarily disable, this may not be needed and can remove
    but it may cause intermittent issues with connecting edges

    inf.on('mouseover', e => {
      if (TempEdge) {
        TempEdge.back();
      }
    });

    inf.on('mouseout', e => {
      if (TempEdge) {
        TempEdge.front();
      }
    });
    */
    const inf = interFace.element;
    inf.on('click.inf', (e) => this.onInterfaceMouseClick(e, interFace));

    if (!this._options.editMode) {
      inf.on('mouseenter.inf', (e) => this.onInterfaceMouseEnter(e, interFace));
    }

    if (!this._options.editMode) {
      inf.on('mouseleave.inf', (e) => this.onInterfaceMouseLeave(e, interFace));
    }
  }

  private onInterfaceMouseClick(e: Event, interFace: SvgInterface): void {
    e.stopPropagation();  // prevent drag-n-drop
    const inf: SVG.G = interFace.element;
    this._options.onInterfaceClick(e, inf, this._node.site);
  }

  private onInterfaceMouseEnter(
    e: Event,
    interFace: SvgInterface,
    node?: PipelineNode,
  ) {
    e.preventDefault();
    interFace.captureButton?.show();
  }

  private onInterfaceMouseLeave(
    e: Event,
    interFace: SvgInterface,
    node?: PipelineNode
  ) {
    e.preventDefault();
    interFace.captureButton?.hide();
  }

  private makeInterfaceLabel(view: SVG.G, name: string, anchor: TextAnchor, index: number): SVG.Text {
    const maxBottomSideIndex = 2;
    const leftSideIndex = 6;
    const rightSideIndex = 7;
    let dx = 0;
    let dy = (index <= maxBottomSideIndex) ? -Config.Interface.anchorOffset.bottom : Config.Interface.anchorOffset.top;

    if (index === rightSideIndex) {
      dx = Config.Interface.anchorOffset.right;
      dy = Config.Interface.anchorOffset.middle;
    } else if (index === leftSideIndex) {
      dx = Config.Interface.anchorOffset.left;
      dy = Config.Interface.anchorOffset.middle;
    } else {
      switch (anchor) {
        case TextAnchor.Middle:
          dx = Config.Interface.width / 2;
          break;
        case TextAnchor.End:
          dx = Config.Interface.width;
          break;
      }
    }

    name = name.toUpperCase();

    const label = view.text(name).font({ anchor: anchor });

    TruncateElementText(label, Config.Interface.maxLabelSize);

    label
      .font({ anchor: anchor })
      .dx(dx)
      .dy(dy)
      .addClass('inf-label').addClass('hidden');
    return label;
  }

  private createSettingsGear(view: SVG.G, offsetX: number, offsetY: number): SvgGear {
    const settingsGroup: SvgGear = new SvgGear(view);
    settingsGroup.element.translate(offsetX, offsetY);
    return settingsGroup;
  }

  public get nativeElement(): SVG.G {
    return this._nativeElement;
  }

  public get options(): SvgNetworkFunctionOptions {
    return this._options;
  }

  public get border(): SVG.Rect {
    return this._border;
  }

  public setName(newName: string) {
    this._nodeName?.text(newName);
  }
}
