import * as SVG from 'svg.js';

export const Config = {
  width: 25,
  height: 25,
  iconSize: 12,
  iconOffset: 6.5,
  roundedCorners: 2,
  iconPath: 'assets/icon-gear.svg',
  fillColor: '#ffffff'
};

export class SvgGear {
  private _nativeElement: SVG.G;

  public constructor(private _root: SVG.G) {
    this.init();
  }

  private init() {
    const group = this._root.group();
    const rect = this.getRect();
    const icon = this.getIcon();
    group.add(rect);
    group.add(icon);
    group.addClass('gear');
    this._nativeElement = group;
  }

  private getRect(): SVG.Rect {
    return this._root
      .rect(Config.width, Config.height)
      .radius(Config.roundedCorners)
      .attr({ fill: Config.fillColor });
  }

  private getIcon(): SVG.Image {
    const icon = this._root.image(Config.iconPath, Config.iconSize);
    icon.translate(Config.iconOffset, Config.iconOffset);
    return icon;
  }

  public get element(): SVG.G {
    return this._nativeElement;
  }

  public get width(): number {
    return Config.width;
  }

  public show() {
    this._nativeElement.show();
  }

  public hide() {
    this._nativeElement.hide();
  }

}
