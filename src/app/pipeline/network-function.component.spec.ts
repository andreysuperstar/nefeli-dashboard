import { NetworkFunctionService } from './network-function.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed, ComponentFixture, fakeAsync, tick, flush } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { NetworkFunctionComponent } from './network-function.component';
import { NetworkFunction } from './network-function.model';
import { NfType } from './pipeline.model';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { AlertService, AlertType } from '../shared/alert.service';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { environment } from '../../environments/environment';

describe('NetworkFunctionComponent', () => {
  let httpMockClient: HttpTestingController;
  let component: NetworkFunctionComponent;
  let fixture: ComponentFixture<NetworkFunctionComponent>;
  let el: HTMLElement;

  const mockNf: NetworkFunction = {
    name: 'mock network function',
    type: NfType.NATIVE,
    controllerType: "NF_CONTROLLER",
    components: undefined,
    local: {
      id: 'mock-nf',
      vendor: "",
      catalogId: 'mock-catalog-id',
      capability: undefined,
    }
  };
  const mockVendor = 'Arista';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatTooltipModule,
        MatDialogModule,
        MatSnackBarModule
      ],
      declarations: [
        NetworkFunctionComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        NetworkFunctionService
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NetworkFunctionComponent);
    component = fixture.componentInstance;
    component.nf = mockNf;
    component.vendor = mockVendor;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display vendor & name', () => {
    expect(component).toBeTruthy();

    expect(el.querySelector('.vendor').textContent).toBe('Arista');
    expect(el.querySelector('.description').textContent.trim()).toBe('mock network function');
  });

  it('should get proper nf color', () => {
    expect(component).toBeTruthy();
    expect(el.querySelector('.top-border.blue')).toBeDefined();
  });

  it('shoud have edit link when in edit mode', () => {
    expect(el.querySelector('.edit')).toBeNull();
    component.showEdit = true;
    fixture.detectChanges();
    expect(el.querySelector('.edit')).toBeDefined();
  })

  it('should remove network function', fakeAsync(() => {
    spyOn(component['_remove'], 'emit');
    component.showEdit = true;
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;
    const removeButton: HTMLButtonElement = el.querySelector('button.remove');
    expect(removeButton).not.toBeNull();
    removeButton.click();

    const dialog = document.querySelector('nef-confirmation-dialog');
    tick();
    const dialogText = dialog.querySelector('p');

    expect(dialogText.textContent)
      .toBe(`This will remove the network function ${mockNf.name}.`);

    const deleteButtonEl: HTMLButtonElement = dialog.querySelectorAll('button')[1];
    expect(deleteButtonEl.textContent).toBe('Delete');

    deleteButtonEl.click();
    tick();

    const req = httpMockClient.expectOne((request: HttpRequest<any>) => {
      return request.url === `${environment.restServer}/v1/nfs/mock-nf`
        && request.method === 'DELETE'
    });
    req.flush({});
    expect(component['_remove'].emit).toHaveBeenCalled();
    flush();
  }));

  it('should handle remove error if nf is in use by a service', fakeAsync(() => {
    spyOn(component['_alertService'], 'error');
    component.showEdit = true;
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;
    const removeButton: HTMLButtonElement = el.querySelector('button.remove');
    removeButton.click();
    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    dialog.close(true);
    tick();
    fixture.detectChanges();
    const req = httpMockClient.expectOne({
      url: `${environment.restServer}/v1/nfs/mock-nf`,
      method: 'DELETE'
    });
    req.flush({}, { status: 409, statusText: 'Bad Request' });
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_CLUSTER_NF_IN_USE);
    flush();
  }));
});
