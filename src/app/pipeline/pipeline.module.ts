import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';

import { TenantService } from '../tenant/tenant.service';
import { NetworkFunctionComponent } from './network-function.component';
import { NetworkFunctionMenuComponent } from './network-function-menu.component';
import { ServicePortMenuComponent } from './service-port-menu/service-port-menu.component';
import { PipelineComponent } from './pipeline.component';
import { SharedModule } from '../shared/shared.module';
import { PacketCaptureDialogComponent } from './packet-capture-dialog/packet-capture-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NetworkFunctionService } from './network-function.service';
import { NfcDialogComponent } from './nfc-dialog/nfc-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatCardModule,
    MatDialogModule,
    SharedModule,
    MatRippleModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatTabsModule,
    MatStepperModule,
    MatIconModule,
    MatTooltipModule
  ],
  providers: [
    TenantService,
    NetworkFunctionService
  ],
  declarations: [
    PipelineComponent,
    NetworkFunctionComponent,
    NetworkFunctionMenuComponent,
    ServicePortMenuComponent,
    PacketCaptureDialogComponent,
    NfcDialogComponent
  ],
  exports: [
    PipelineComponent,
    NetworkFunctionComponent,
    NetworkFunctionMenuComponent,
    ServicePortMenuComponent
  ]
})
export class PipelineModule { }
