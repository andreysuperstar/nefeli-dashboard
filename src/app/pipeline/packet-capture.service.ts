import { Injectable } from '@angular/core';
import { Observable, Subject, Subscription } from 'rxjs';
import { saveAs } from 'file-saver';

import { PacketCaptureRequest } from 'rest_client/pangolin/model/packetCaptureRequest';

import { ServicesService } from 'rest_client/pangolin/api/services.service';

export enum Format {
  PCAP,
  TCPDUMP,
  TSHARK
}

interface TraceRequest {
  subject: Subject<Blob | string>;
  request?: Observable<Blob>;
  subscription?: Subscription;
}

@Injectable({
  providedIn: 'root'
})
export class PacketCaptureService {
  private _traceRequests = new Map<string, TraceRequest>();

  constructor(private _servicesService: ServicesService) { }

  public trace(tenant: string, packetCaptureRequest: PacketCaptureRequest, format: Format): Subject<Blob | string> {
    const traceID = this.makeTraceID(packetCaptureRequest);
    let trace = this._traceRequests.get(traceID);

    if (trace?.subject && !trace?.request) {
      this.request(trace, tenant, packetCaptureRequest, format);
    } else {
      trace = {
        subject: new Subject<string>()
      };

      this.request(trace, tenant, packetCaptureRequest, format);

      this._traceRequests.set(traceID, trace);
    }

    return trace.subject;
  }

  public cancel(packetCaptureRequest: PacketCaptureRequest): void {
    const traceID = this.makeTraceID(packetCaptureRequest);
    const trace = this._traceRequests.get(traceID);

    if (trace?.request) {
      this.cancelTrace(trace);
    }
  }

  public getTrace(packetCaptureRequest: PacketCaptureRequest): Subject<Blob | string> {
    const traceID = this.makeTraceID(packetCaptureRequest);
    const trace = this._traceRequests.get(traceID);

    if (trace && trace.request) {
      return trace.subject;
    }
  }

  private request(trace: TraceRequest, tenantID: string, packetCaptureRequest: PacketCaptureRequest, format: Format) {
    const request = this._servicesService.postServicePacketCapture(tenantID, packetCaptureRequest.service, packetCaptureRequest);

    trace.subscription = request.subscribe({
      next: response => {
        this.saveToFile(response, packetCaptureRequest, format);

        trace.subject.next(response);

        this.cancelTrace(trace);
      },
      error: (e) => trace.subject.error(e)
    });

    trace.request = request;
  }

  private cancelTrace(trace: TraceRequest) {
    trace.request = undefined;
    trace.subject = undefined;

    if (trace.subscription) {
      trace.subscription.unsubscribe();
    }
  }

  private saveToFile(data: Blob | string, { backend, instance, port }: PacketCaptureRequest, format: Format) {
    let contentType = 'text/plain';
    let extension = '.txt';

    if (format === Format.PCAP) {
      extension = '.pcap';
      contentType = 'application/octet-stream';
    }

    const filename = `${backend}_${instance}_${port}${extension}`;

    const blob = new Blob([data], {
      type: contentType
    });

    saveAs(blob, filename);
  }

  private makeTraceID({ instance, node, port, service }: PacketCaptureRequest): string {
    return `${service}_${node}_${instance}_${port}`;
  }
}
