import { Component, Output, EventEmitter } from '@angular/core';
import { DragEventData } from '../../service-designer/service-designer/service-designer.component';
import { randomID } from '../network-function.model';

@Component({
  selector: 'nef-service-port-menu',
  templateUrl: './service-port-menu.component.html',
  styleUrls: ['./service-port-menu.component.less']
})
export class ServicePortMenuComponent {

  @Output() private _handleDragStart = new EventEmitter();

  constructor() { }

  public onDragStart(event: DragEvent): void {
    const eventData: DragEventData = {
      id: randomID(),
      type: 'tport',
      name: 'Traffic Port',
      event: event
    };

    this._handleDragStart.emit(eventData);
  }
}
