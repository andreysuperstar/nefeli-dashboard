import { EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { ServicePortMenuComponent } from './service-port-menu.component';

describe('ServicePortMenuComponent', () => {
  let component: ServicePortMenuComponent;
  let fixture: ComponentFixture<ServicePortMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatCardModule],
      declarations: [ServicePortMenuComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicePortMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render the menu', () => {
    fixture.detectChanges();
    const elem = fixture.debugElement.nativeElement;
    expect(elem.querySelector('.mat-card-title').textContent.toLowerCase()).toBe('service ports');
    expect(elem.querySelectorAll('.port').length).toBe(1);
  });

  it('should emit event data on drag', () => {
    spyOn(component['_handleDragStart'], 'emit').and.callThrough();
    component.onDragStart({} as DragEvent);
    expect(component['_handleDragStart'].emit).toHaveBeenCalled();
  });
});
