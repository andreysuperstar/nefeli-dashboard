import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable, forkJoin, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { NFCatalog, NetworkFunction, NFCatalogResponse, createNFCatalog } from './network-function.model';
import { TenantService } from '../tenant/tenant.service';
import { NFCConfig } from 'rest_client/pangolin/model/nFCConfig';
import { NFDescriptor } from 'rest_client/pangolin/model/nFDescriptor';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';
import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { map, tap } from 'rxjs/operators';

export interface NFConfigResponse {
  service: string;
  nf: string;
  status: string;
}

@Injectable({
  providedIn: 'root'
})
export class NetworkFunctionService {

  private _availableNFs: Observable<NFCatalog>;
  private _activatedNFs: Observable<NFCatalog>;

  public get availableNFs$(): Observable<NFCatalog> {
    return this._availableNFs;
  }

  public get activatedNFs$(): Observable<NFCatalog> {
    return this._activatedNFs;
  }

  constructor(
    private _tenantService: TenantService,
    private _restServicesService: ServicesService,
    private _restNfService: NetworkFunctionsService
  ) { }

  public getActivatedNFs(tenantId: string): Observable<NFCatalog> {
    return this._tenantService.getNfCatalog(tenantId);
  }

  /* TODO(ecarino): re-enable code block when support tenant-level NFs
  // Returns the list of all system NFs except ones that are present at Tenant
  public getAvailableNFs(tenantId: string): Observable<NFCatalog> {
    return forkJoin([
      this._tenantService.getNfCatalog(tenantId),
      this.getSystemNFs()
    ]).pipe(
      map((catalogs: NFCatalog[]) => this.getFilteredCatalog(catalogs))
    );
  }

  // exclude from system catalog nfs from tenant catalog
  private getFilteredCatalog(catalogs: NFCatalog[]): NFCatalog {
    const [tenant, system] = catalogs;
    // at least in the mocks same NF could have same vendor but different letter case
    // so it's required to normalize names to lowercase to allow comparison
    const lowercaseTenant = Object.keys(tenant).reduce((res, key) => {
      res[key.toLowerCase()] = tenant[key];
      return res;
    }, {});

    const result: NFCatalog = [];
    for (const [key, val] of Object.entries(system)) {

      const tenantVendor = lowercaseTenant[key.toLowerCase()];
      if (tenantVendor) {
        result.push(val.filter((nf: NetworkFunction) => {
          return !tenantVendor.some(o => nf.local.id === o.id);
        }));
      } else {
        result[key] = val;
      }
    }
    return result;
  }
  */

  // get all nfs for all clusters
  public getSystemNFs(): Observable<NFCatalog> {
    return this._restNfService.getNfs().pipe(
      map((catalog: RESTNFCatalog): NFCatalog => createNFCatalog(catalog))
    );
  }

  public postNf(manifest: string): Observable<NFDescriptor> {
    const contentTypeHandler = this._restNfService.configuration.selectHeaderContentType;

    // set content-type to application/x-yaml
    this._restNfService.configuration.selectHeaderContentType = (contentTypes: string[]): string => {
      if (contentTypes.length === 0) {
        return undefined;
      }

      const type = contentTypes.find((x: string) => x.includes('yaml'));
      if (type === undefined) {
        return contentTypes[0];
      }

      return type;
    };

    return this._restNfService.postNf(manifest as NFDescriptor).pipe(
      tap(() => {
        // revert back to default content-type handler
        this._restNfService.configuration.selectHeaderContentType = contentTypeHandler;
      })
    );
  }

  public putNF(nfId: string, manifest: NFDescriptor): Observable<HttpResponse<any>> {
    return this._restNfService.putNf(nfId, manifest as NFDescriptor);
  }

  public getNF(nFId: string): Observable<NFDescriptor> {
    return this._restNfService.getNf(nFId);
  }

  public postNfConfig(tenantId: string, serviceId: string, nodeId: string, config: NFCConfig): Observable<any> {
    return this._restServicesService.putServiceNFConfig(tenantId, serviceId, nodeId, config);
  }

  public getNfConfig(tenantId: string, serviceId: string, nodeId: string): Observable<NFCConfig> {
    return this._restServicesService.getServiceNFConfig(tenantId, serviceId, nodeId);
  }

  public getNfManifest(nfId: string): Observable<NFDescriptor> {
    return this._restNfService.getNf(nfId);
  }

  public deleteNf(nfId: string): Observable<HttpResponse<any>> {
    return this._restNfService.deleteNf(nfId);
  }
}
