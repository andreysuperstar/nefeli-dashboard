export interface PacketLossStat {
  name: string;
  site: string;
  stats: Array<[number, string]>;
}

export interface LatencyStat {
  name: string;
  site: string;
  stats: Array<[number, string]>;
}

export interface ThroughputStat {
  id: string;
  name: string;
  site: string;
  stats: Array<[number, string]>;
}

export interface ChartStats {
  throughputOut: number | Array<[number, string]>;
  throughputIn: number | Array<[number, string]>;
  throughputServiceOut?: Array<ThroughputStat>;
  throughputServiceIn?: Array<ThroughputStat>;
  packetLoss: number | Array<PacketLossStat>;
  latency: number | Array<LatencyStat>;
}
