import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartThroughputComponent } from './chart-throughput.component';
import { SharedModule } from '../shared/shared.module';
import { ChartLatencyComponent } from './chart-latency.component';
import { ChartPacketlossComponent } from './chart-packetloss.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [
    ChartThroughputComponent,
    ChartLatencyComponent,
    ChartPacketlossComponent
  ],
  exports: [
    ChartThroughputComponent,
    ChartLatencyComponent,
    ChartPacketlossComponent
  ]
})
export class ChartsModule { }
