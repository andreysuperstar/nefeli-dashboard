import { ChartDataSet, ChartLineComponent, UnitType, ZoomEvent } from '../shared/chart-line/chart-line.component';
import { Pipeline, SERVICE_AGGREGATE } from '../pipeline/pipeline.model';
import { Input, ViewChild, OnDestroy, OnInit, Directive, Output, EventEmitter } from '@angular/core';
import { Tenant, TenantService, TenantStats, NO_SERVICES_SELECTED } from '../tenant/tenant.service';
import { Subscription, Observable } from 'rxjs';
import { CommonChart } from './common-chart';
import { LegendItem } from '../shared/legend/legend.component';
import { ServersService } from '../cluster/servers.service';
import { ServerStats } from 'rest_client/pangolin/model/serverStats';
import { LoggerService } from '../shared/logger.service';
import { ClusterService } from '../home/cluster.service';
import { PipelineNfStats } from '../pipeline/pipeline.model';
import { PipelineService } from '../pipeline/pipeline.service';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { AlertService } from '../shared/alert.service';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';

const SecToMsecFactor = 1000;

@Directive()
// tslint:disable-next-line: directive-class-suffix
export abstract class BaseChart implements OnDestroy, OnInit {

  protected _init: boolean;
  protected _statStreamSubscription: Subscription;
  protected _statStreamTimer: number;
  protected _pipelineStatStreamTimer: number;
  protected _pipelineStatStreamSubscription: Subscription;
  protected _tenant: Tenant;
  protected _clusterId: string;
  protected _serverId: string;
  protected _rate: number = CommonChart.config.RATE;
  protected _step: number = CommonChart.config.STEP;
  protected _pollInterval: number = CommonChart.config.POLL_INTERVAL;
  protected _startTime: number;
  protected _selectedPipelines: Pipeline[];
  protected _selectedNFs: string[];
  protected _isActive = false;
  protected _dataSets = new Array<ChartDataSet>();
  protected _yAxesUnits: UnitType = UnitType.BitRate;
  protected _legendItems: LegendItem[];
  protected _showHeader = true;
  protected _sites = new Array<Site>();
  protected _initDataSet = true;
  private _subscriptions = new Subscription();
  private _label: string;
  private _pipelineStreamActive = false;
  private _updateStreamOnInit = true;
  private _zoomEvent = new EventEmitter<ZoomEvent>();

  @Input() public set showHeader(show: boolean) {
    this._showHeader = show;
  }

  @Input() public set tenant(t: Tenant) {
    this._tenant = t;

    this.updateStatsStream(true);
  }

  @Input() public set cluster(id: string) {
    this._clusterId = id;
  }

  @Input() public set server(id: string) {
    this._serverId = id;

    this.updateStatsStream(true);
  }

  @Input() public set selectedRate(rate: number) {
    if (rate) {
      this._rate = rate;
    } else {
      this._rate = CommonChart.config.RATE;
    }
    this.updateStatsStream(true);
  }

  @Input() public set startTime(time: number) {
    this._startTime = time;
    this.updateStatsStream(true);
  }

  @Input() public set selectedPipelines(val: Pipeline[]) {
    this._selectedPipelines = val;
    this.updateStatsStream(true);
  }

  @Input() public set selectedNFs(val: string[]) {
    this._selectedNFs = val;
    this.updateStatsStream(true);
  }

  @Input() public set label(val: string) {
    this._label = val;
  }

  @Input() public set active(val: boolean) {
    this._isActive = val;
    this.updateStatsStream(val);
  }

  @Input() public set yAxesUnits(type: UnitType) {
    this._yAxesUnits = type;
  }

  @Input() public set pipelineStreamActive(state: boolean) {
    this._pipelineStreamActive = state;
    this.updateStatsStream(true);
  }

  @Output() public get zoomEvent(): EventEmitter<ZoomEvent> {
    return this._zoomEvent;
  }

  @ViewChild('chart', { static: true }) protected chart: ChartLineComponent;

  constructor(
    private _tenantService: TenantService,
    private _serversService: ServersService,
    private _clusterService: ClusterService,
    private _pipelineService: PipelineService,
    private _pipelineNameService: PipelineNamePipe,
    protected _alert: AlertService,
    protected _logger: LoggerService,
  ) { }

  public ngOnInit() {
    this._init = true;

    this.chart.yAxesUnits = this._yAxesUnits;

    this._subscriptions.add(
      this._clusterService.streamSites().subscribe(({ sites: sites }: Sites) => {
        this._sites = sites;

        if (this._updateStreamOnInit) {
          this.updateStatsStream(true);
          this._updateStreamOnInit = false;
        }
      })
    );
  }

  public ngOnDestroy() {
    this.disableStatStreams();
    this._subscriptions.unsubscribe();
  }

  protected updateStatsStream(enable: boolean) {
    /* TODO(andrew): This is used to prevent `updateStatsStream` from running in inputs
       before all input values are set.

       `updateStatsStream` runs `enableStatsStream` in charts which triggers`getStats$`
       with empty `this._clusterId` and `this._serverId` values.

       Input decorators are available to use only after Angular component is initialized.

       This possibly could be refactored using `OnChanges` but was fixed this way due to
       current complexity of `updateStatsStream` usage in a large number of inputs */
    if (!this._init) {
      return;
    }

    this.disableStatStreams();
    if (enable) {
      if (this._dataSets) {
        this._dataSets.length = 0;
      }

      let pipelines: Pipeline[];

      if (this._selectedPipelines) {
        pipelines = this._selectedPipelines[0]
          ? this._selectedPipelines
          : NO_SERVICES_SELECTED;
      }

      // enable stats streams
      if (this._pipelineStreamActive) {
        this.enablePipelineStatsStream(pipelines);
      } else {
        this.enableStatsStream(pipelines);
      }
    }
  }

  private disableStatStreams() {
    // disable stats streams
    if (this._statStreamSubscription) {
      this._statStreamSubscription.unsubscribe();
      this._statStreamSubscription = undefined;
    }

    if (this._statStreamTimer) {
      clearTimeout(this._statStreamTimer);
      this._statStreamTimer = undefined;
    }

    // disable pipeline stats stream
    if (this._pipelineStatStreamSubscription) {
      this._pipelineStatStreamSubscription.unsubscribe();
      this._pipelineStatStreamSubscription = undefined;
    }

    if (this._pipelineStatStreamTimer) {
      clearTimeout(this._pipelineStatStreamTimer);
      this._pipelineStatStreamTimer = undefined;
    }
  }

  protected postPipelineStats$(pipeline: string): Observable<PipelineNfStats> {
    return this._pipelineService.streamTenantPipelineStats(this._tenant, pipeline, this._pollInterval);
  }

  protected getStats$(pipelines: string[], filter: StatFilter): Observable<TenantStats | ServerStats> {
    let obs$: Observable<TenantStats | ServerStats>;

    // for aggregate requests, API expects an empty array
    pipelines = pipelines?.filter((p) => p !== SERVICE_AGGREGATE);

    if (this._tenant && this._clusterId && this._serverId) {
      this._logger.warn('Tenant, cluster, and server specified. Not sure which stats to fetch, so will default to server stats.');
    }

    if (this._clusterId && this._serverId) {
      obs$ = this._startTime
        ? this._serversService.postServerStats(
          this._clusterId,
          this._serverId,
          [filter] as Array<StatFilter>,
          this._rate,
          this._step,
          this._startTime
        )
        : this._serversService.streamServerStats(
          this._clusterId,
          this._serverId,
          this._pollInterval,
          filter,
          this._rate,
          this._step
        );
    } else if (this._clusterId) {
      obs$ = this._startTime
        ? this._clusterService.postClusterStats(
          this._clusterId,
          [filter] as Array<StatFilter>,
          this._rate,
          this._step,
          pipelines,
          this._startTime
        )
        : this._clusterService.streamClusterStats(
          this._clusterId,
          this._pollInterval,
          filter,
          this._rate,
          this._step,
          pipelines
        );
    } else if (this._tenant) {
      pipelines = pipelines || [NO_SERVICES_SELECTED[0].identifier];

      obs$ = this._startTime
        ? this._tenantService.postTenantStats(
          this._tenant.identifier,
          [filter] as Array<StatFilter>,
          this._rate,
          this._step,
          pipelines,
          this._startTime
        )
        : this._tenantService.streamTenantStats(
          this._tenant.identifier,
          this._pollInterval,
          filter,
          this._rate,
          this._step,
          pipelines
        );
    }

    return obs$;
  }

  public get dataSets(): ChartDataSet[] {
    return this._dataSets;
  }

  public get label(): string {
    return this._label;
  }

  public get legendItems(): LegendItem[] {
    return this._legendItems;
  }

  public get showHeader(): boolean {
    return this._showHeader;
  }

  protected setLegendItems() {
    this._legendItems = new Array<LegendItem>();
    this._dataSets.forEach((dataSet: ChartDataSet) => {
      this._legendItems.push({
        label: this._pipelineNameService.transform(dataSet.label),
        color: dataSet.borderColor
      });
    });
  }

  protected updateChart(dataSets: ChartDataSet[]) {
    CommonChart.updateChart(
      dataSets,
      this._dataSets,
      [this.chart],
      this._rate,
      this._startTime * SecToMsecFactor,
      this._step,
      this._initDataSet);
    this._initDataSet = false;
  }

  protected filterSelectedNFs(stats: PipelineNfStats) {
    const filtered = {};
    for (const [key, value] of Object.entries(stats)) {
      if (this._selectedNFs.includes(key)) {
        filtered[key] = value;
      }
    }
    return filtered;
  }

  public beforeZoomHandler() {
    this.disableStatStreams();
  }

  public afterZoomHandler(event: ZoomEvent) {
    this._zoomEvent.emit(event);
  }

  protected abstract enableStatsStream(pipelines: Pipeline[]): void;

  protected abstract enablePipelineStatsStream(pipelines: Pipeline[]): void;
}
