import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { ChartPacketlossComponent } from './chart-packetloss.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TenantService, TenantStats } from '../tenant/tenant.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { environment } from '../../environments/environment';
import { ChartLineComponent, ChartDataSet } from '../shared/chart-line/chart-line.component';
import { AlertService } from '../shared/alert.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { ClusterService } from '../home/cluster.service';
import { Pipeline, PipelineNfStats } from '../pipeline/pipeline.model';
import { Site } from 'rest_client/pangolin/model/site';
import { Tenant } from 'rest_client/heimdallr/model/tenant';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { TenantStatsRequest } from 'rest_client/pangolin/model/tenantStatsRequest';

describe('ChartPacketlossComponent', () => {
  let component: ChartPacketlossComponent;
  let fixture: ComponentFixture<ChartPacketlossComponent>;
  let httpTestingController: HttpTestingController;
  let el: HTMLElement;

  const mockTenant: Tenant = {
    identifier: '1',
    name: 'Tenant A'
  };

  const mockSelectedPipelines: Pipeline[] = [
    {
      identifier: '1',
      name: 'Service A'
    }
  ];

  const mockTenantsStats: TenantStats = {
    'id': '-1',
    'name': '',
    'services': 2,
    'throughputOut': [],
    'throughputIn': [],
    'status': 'good',
    'packetLoss': [
      {
        'site': 'eric_site',
        'name': 'Unclassified',
        'stats': [
          [
            1531154025,
            '356.6'
          ]
        ]
      },
      {
        'site': 'eric_site',
        'name': 'customer0_pid4',
        'stats': [
          [
            1531154026,
            '21400.6'
          ]
        ]
      },
      {
        'site': 'eric_site',
        'name': 'ddp_pid2',
        'stats': [
          [
            1531154027,
            '0'
          ]
        ]
      }
    ],
    'latency': []
  } as TenantStats;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      declarations: [
        ChartPacketlossComponent,
        ChartLineComponent,
        ChartHeaderComponent,
        LegendComponent
      ],
      providers: [
        PipelineNamePipe,
        TenantService,
        AlertService,
        ClusterService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartPacketlossComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should start the stat stream when active', inject([TenantService], (tenantService: TenantService) => {
    spyOn(tenantService, 'streamTenantStats').and.callThrough();

    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    expect(tenantService.streamTenantStats).toHaveBeenCalled();
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[0]).toEqual(mockTenant.identifier);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[1]).toEqual(22000);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[2]).toEqual(StatFilter.PACKETLOSS);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[3]).toEqual(21600);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[4]).toEqual(22);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[5][0]).toEqual(mockSelectedPipelines[0].identifier);
  }));

  it('should create data set points for chart', fakeAsync(inject([TenantService], (tenantService: TenantService) => {
    spyOn(tenantService, 'streamTenantStats').and.callThrough();

    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    component['_subscriptions'].unsubscribe();

    component['_sites'] = [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      } as Site
    ];

    tick();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/stats`
    );
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');

    const body: TenantStatsRequest = {
      services: [mockSelectedPipelines[0].identifier],
      filter: {
        filter: [StatFilter.PACKETLOSS]
      },
      range: {
        start: undefined,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);

    req.flush(mockTenantsStats);

    expect(component.dataSets[0].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    expect(component.dataSets[0].data[0].y).toBe(356.6);
    expect((component.dataSets[0].data[0].x as Date).getTime()).toBe(1531154025000);
    expect(component.dataSets[1].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    expect(component.dataSets[1].data[0].y).toBe(21400.6);
    expect((component.dataSets[1].data[0].x as Date).getTime()).toBe(1531154026000);
    expect(component.dataSets[2].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    expect(component.dataSets[2].data[0].y).toBe(0);
    expect((component.dataSets[2].data[0].x as Date).getTime()).toBe(1531154027000);

    component.active = false;
  })));

  it('should set chart legend colors', fakeAsync(() => {
    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    component['_subscriptions'].unsubscribe();

    component['_sites'] = [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      } as Site
    ];

    tick();

    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/stats`
    );
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');

    const body: TenantStatsRequest = {
      services: [mockSelectedPipelines[0].identifier],
      filter: {
        filter: [StatFilter.PACKETLOSS]
      },
      range: {
        start: undefined,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);

    req.flush(mockTenantsStats);

    fixture.detectChanges();
    const legends = el.querySelectorAll('nef-chart-header nef-legend span');
    expect(legends.length).toBe(3);
    expect(legends[0].textContent).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    expect(legends[1].textContent).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    expect(legends[2].textContent).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    component.active = false;
  }));

  it('should start pipeline stats stream when active', () => {
    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;
    component.pipelineStreamActive = true;
    component.selectedNFs = ['user_vsrx'];

    spyOn<any>(component, 'updateChart').and.callFake((dataSets: ChartDataSet[]) => {
      expect(dataSets[0].label).toBe('user_vsrx');
      expect(dataSets[0].data[0].y).toBe(3);
    });

    const response: PipelineNfStats = { "user_vsrx": { throughput: 2040.5430388025459, loss: 3, latency: 1000 } };
    const req = httpTestingController.match(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockSelectedPipelines[0].identifier}/stats`
    );
    req[1].flush(response);

    component.active = false;
  });
});
