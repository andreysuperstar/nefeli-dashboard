import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';

import { ChartThroughputComponent } from './chart-throughput.component';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TenantService, TenantStats, STATUS } from '../tenant/tenant.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { environment } from '../../environments/environment';
import { ChartLineComponent, ChartDataSet } from '../shared/chart-line/chart-line.component';
import { AlertService } from '../shared/alert.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LegendComponent } from '../shared/legend/legend.component';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { Pipeline, PipelineNfStats } from '../pipeline/pipeline.model';
import { Site } from 'rest_client/pangolin/model/site';
import { Tenant } from 'rest_client/heimdallr/model/tenant';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { TenantStatsRequest } from 'rest_client/pangolin/model/tenantStatsRequest';

describe('ChartThroughputComponent', () => {
  let component: ChartThroughputComponent;
  let fixture: ComponentFixture<ChartThroughputComponent>;
  let httpTestingController: HttpTestingController;
  let el: HTMLElement;

  const mockTenant: Tenant = {
    identifier: '1',
    name: 'Tenant A'
  };

  const mockSelectedPipelines: Pipeline[] = [
    {
      identifier: '1',
      name: 'Service A'
    }
  ];

  const mockTenantsStats: TenantStats = {
    id: '-1',
    name: '',
    services: 2,
    throughputOut: [
      [
        1532370507.296,
        '12287'
      ]
    ],
    throughputIn: [
      [
        1532370507.297,
        '12288'
      ]
    ],
    throughputServiceOut: [
      {
        id: '4',
        name: 'ecarino',
        site: 'eric_site',
        stats: [
          [
            1548804359,
            "879.122617893576"
          ]
        ]
      }
    ],
    throughputServiceIn: [
      {
        id: '4',
        name: 'ecarino',
        site: 'eric_site',
        stats: [
          [
            1548804359,
            "879.1119391236147"
          ]
        ]
      }
    ],
    status: STATUS.GOOD,
    packetLoss: [],
    latency: [],
    coresUsed: 10,
    coresTotal: 10,
    memUsed: 0,
    memTotal: 0,
    capacity: 0
  } as TenantStats;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      declarations: [
        ChartThroughputComponent,
        ChartLineComponent,
        ChartHeaderComponent,
        LegendComponent
      ],
      providers: [
        PipelineNamePipe,
        TenantService,
        AlertService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartThroughputComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should start the stat stream when active', inject([TenantService], (tenantService: TenantService) => {
    spyOn(tenantService, 'streamTenantStats').and.callThrough();

    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    expect(tenantService.streamTenantStats).toHaveBeenCalled();
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[0]).toEqual(mockTenant.identifier);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[1]).toEqual(22000);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[2]).toEqual(StatFilter.THROUGHPUT);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[3]).toEqual(21600);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[4]).toEqual(22);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[5][0]).toEqual(mockSelectedPipelines[0].identifier);
  }));

  it('should create data set points for chart', fakeAsync(inject([TenantService], (tenantService: TenantService) => {
    spyOn(tenantService, 'streamTenantStats').and.callThrough();

    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    component['_subscriptions'].unsubscribe();

    component['_sites'] = [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      } as Site
    ];

    tick();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/stats`
    );
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');

    const body: TenantStatsRequest = {
      services: [mockSelectedPipelines[0].identifier],
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);

    req.flush(mockTenantsStats);

    expect(component.dataSets[0].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name} out`);
    expect((component.dataSets[0].data[0].x as Date).getTime()).toBe(1548804359000);
    expect(component.dataSets[0].data[0].y).toBe(879.122617893576);
    expect(component.dataSets[1].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name} in`);
    expect((component.dataSets[1].data[0].x as Date).getTime()).toBe(1548804359000);
    expect(component.dataSets[1].data[0].y).toBe(879.1119391236147);

    component.active = false;
  })));

  it('should set chart legend colors', fakeAsync(() => {
    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    component['_subscriptions'].unsubscribe();

    component['_sites'] = [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      } as Site
    ];

    tick();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/stats`
    );
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');
    req.flush(mockTenantsStats);

    const body: TenantStatsRequest = {
      services: [mockSelectedPipelines[0].identifier],
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);

    fixture.detectChanges();
    const legends = el.querySelectorAll('nef-chart-header nef-legend span');
    expect(legends.length).toBe(2);
    expect(legends[0].textContent).toBe(`Eric Site: ${mockSelectedPipelines[0].name} out`);
    expect(legends[1].textContent).toBe(`Eric Site: ${mockSelectedPipelines[0].name} in`);
    component.active = false;
  }));

  it('should start pipeline stats stream when active', () => {
    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;
    component.pipelineStreamActive = true;
    component.selectedNFs = ['user_vsrx'];

    spyOn<any>(component, 'updateChart').and.callFake((dataSets: ChartDataSet[]) => {
      expect(dataSets[0].label).toBe('user_vsrx');
      expect(dataSets[0].data[0].y).toBe(2040.5430388025459);
    });

    const response: PipelineNfStats = { "user_vsrx": { throughput: 2040.5430388025459, loss: 3, latency: 1000 } };
    const req = httpTestingController.match(
      `${environment.restServer}/v1/tenants/${mockTenant.identifier}/services/${mockSelectedPipelines[0].identifier}/stats`
    );
    req[1].flush(response);

    component.active = false;
  });

  it('should insert zero point when no stats returned', fakeAsync(() => {
    const step = 22;
    component.tenant = { identifier: '1', name: 'Tenant A' };
    component.active = true;
    component['_subscriptions'].unsubscribe();

    tick();
    let req = httpTestingController
      .expectOne(
        `${environment.restServer}/v1/tenants/${mockTenant.identifier}/stats`
      );
    req.flush(mockTenantsStats);

    let body: TenantStatsRequest = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: 21600,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    const tputOut = component.dataSets[2];
    const tputIn = component.dataSets[3];
    expect(tputOut.label).toBe('eric_site: ecarino out');
    expect(tputOut.data[0].y).toBe(879.122617893576);
    expect(tputOut.data[1]).toBeUndefined();
    expect(tputIn.label).toBe('eric_site: ecarino in');
    expect(tputIn.data[0].y).toBe(879.1119391236147);
    expect(tputIn.data[1]).toBeUndefined();

    tick(step * 1000);

    // fire a response w/ no stats data for throughput
    req = httpTestingController.expectOne(`${environment.restServer}/v1/tenants/1/stats`);
    req.flush({
      'id': '-1',
      'name': '',
      'services': 2,
      'throughputOut': [],
      'throughputIn': [],
      "throughputServiceOut": [
        {
          "id": "4",
          "name": "ecarino",
          "site": "eric_site",
          "stats": []
        }
      ],
      "throughputServiceIn": [
        {
          "id": "4",
          "name": "ecarino",
          "site": "eric_site",
          "stats": []
        }
      ],
      'status': 'good',
      'packetLoss': [],
      'latency': []
    });

    body = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: step,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    /* verify two zero'd points were inserted for each out & in
    the first one being the same time as the previous point */
    expect(tputOut.data[1].y).toBe(0);
    expect(tputOut.data[2].y).toBe(0);
    expect(tputOut.data[3]).toBeUndefined();
    expect(tputIn.data[1].y).toBe(0);
    expect(tputIn.data[2].y).toBe(0);
    expect(tputIn.data[3]).toBeUndefined();

    component.active = false;
  }));
});
