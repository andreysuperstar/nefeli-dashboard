import { ChartDataSet, ChartLineComponent } from '../shared/chart-line/chart-line.component';
import { PipelineStat } from '../home/cluster.service';
import { ChartPoint } from 'chart.js';
import { Stats } from 'rest_client/pangolin/model/stats';
import { PipelineNfStats } from '../pipeline/pipeline.model';
import { Site } from 'rest_client/pangolin/model/site';

export enum StatsType {
  Throughput = 'throughput',
  Latency = 'latency',
  Loss = 'loss'
}

export interface ChartConfig {
  POLL_INTERVAL: number;
  RATE: number;
  STEP: number;
}

const SecToMsecFactor = 1000;

const Config: ChartConfig = {
  POLL_INTERVAL: 5000,
  RATE: 21600,
  STEP: 5
};

export class CommonChart {
  public static getOptimalStep(duration: number): number {
    const divisor = 1000;
    const step = Math.round(duration / divisor);
    return (step > 0) ? step : 1;
  }

  public static getOptimalPollInterval(duration: number): number {
    const interval = CommonChart.getOptimalStep(duration) * SecToMsecFactor;
    return (Config.POLL_INTERVAL > interval) ? Config.POLL_INTERVAL : interval;
  }

  public static updateChart(
    newDataSets: ChartDataSet[],
    dataSetsSrc: ChartDataSet[],
    charts: ChartLineComponent[],
    rate: number,
    startTimeMs: number,
    step: number,
    initDataSet = false) {

    if (initDataSet) {
      dataSetsSrc.length = 0;
      newDataSets.forEach((dataSet) => dataSetsSrc.push(dataSet));
    } else {
      newDataSets.forEach((dataSet) => {
        const foundDataSrc = dataSetsSrc.find((dataSetSrc) => {
          return dataSetSrc.label === dataSet.label;
        });

        if (foundDataSrc) {
          if ((dataSet?.data?.length ?? 0) === 0
            && (foundDataSrc?.data?.length ?? 0) > 0) {
            const zeroPoint: ChartPoint = { x: new Date(), y: 0 };
            const prevPoint = foundDataSrc.data[foundDataSrc.data.length - 1];
            const diffSecs = ((zeroPoint.x as number) - (prevPoint.x as number)) / SecToMsecFactor;
            if ((diffSecs > step)) {
              /* didn't receive any data, so artificially add a
              zero'd point so chart properly represents no traffic */
              dataSet.data.push(zeroPoint);
            }
          }

          Array.prototype.push.apply(foundDataSrc.data, dataSet.data);
        } else {
          dataSetsSrc.push(dataSet);
        }
      });
    }

    charts.forEach((chart: ChartLineComponent) => {
      chart.initDataSets(dataSetsSrc, rate, startTimeMs, step);
      chart.chart?.update();
    });
  }

  public static handlePipelineStats(stats: PipelineNfStats, type: StatsType): ChartDataSet[] {
    const dataSet = new Array<ChartDataSet>();
    for (const nf of Object.keys(stats)) {
      const chartPoints = new Array<ChartPoint>();
      chartPoints.push({
        x: new Date(),
        y: +stats[nf][type]
      });
      dataSet.push({
        label: nf,
        data: chartPoints
      });
    }
    return dataSet;
  }

  public static handleThroughputStats(
    stats: Stats,
    sites: Site[],
    pipelineName?: string,
    getTotal = true
  ): ChartDataSet[] {
    const dataSet = new Array<ChartDataSet>();

    if (getTotal) {
      for (const property of ['throughputOut', 'throughputIn']) {
        const chartPoints = new Array<ChartPoint>();

        if (stats[property] instanceof Array) {
          for (const point of stats[property]) {
            chartPoints.push({
              x: new Date(point[0] * SecToMsecFactor),
              y: +point[1]
            });
          }

          dataSet.push({
            label: property,
            data: chartPoints
          });
        } else if (stats[property]) {
          dataSet.push({
            label: property,
            data: []
          });
        }
      }
    }

    for (const property of ['throughputServiceOut', 'throughputServiceIn', 'throughputServerOut', 'throughputServerIn']) {
      const pipelineStats: Array<PipelineStat> = stats[property];
      if (pipelineStats) {
        for (const pipelineStat of pipelineStats) {
          const chartPoints = new Array<ChartPoint>();
          for (const point of pipelineStat.stats) {
            chartPoints.push({
              x: new Date(point[0] * SecToMsecFactor),
              y: +point[1]
            });
          }

          // TODO(eric): optimize, sites array could be large
          // apply similar optimization to packet-loss and latency handlers as well
          const site = sites.find(s => s.config.identifier === pipelineStat.site);
          dataSet.push({
            label: `${site ? site.config.name : pipelineStat.site}: ${pipelineName || pipelineStat.name}`
              + ((property === 'throughputServiceOut' || property === 'throughputServerOut') ? ' out' : ' in'),
            data: chartPoints
          });
        }
      }
    }

    return dataSet;
  }

  public static handleLatencyStats(
    stats: Stats,
    sites: Site[],
    pipelineName: string
  ): ChartDataSet[] {
    const dataSet = new Array<ChartDataSet>();

    if (typeof stats.latency === 'object') {
      for (const service of stats.latency) {
        const chartPoints = new Array<ChartPoint>();
        const name = service.name;
        const siteId = service.site;
        const points = service.stats;
        const site = sites.find(s => s.config.identifier === siteId);
        if (points) {
          for (const point of points) {
            chartPoints.push({
              x: new Date(point[0] * SecToMsecFactor),
              y: +point[1]
            });
          }
        }

        const siteName: string = site ? site.config.name : siteId;
        const label: string = siteName
          ? `${siteName}: ${pipelineName || name}`
          : name;

        dataSet.push({
          label,
          data: chartPoints
        });
      }
    }

    return dataSet.length ? dataSet : [{
      data: []
    }];
  }

  public static handlePacketlossStats(
    stats: Stats,
    sites: Site[],
    pipelineName?: string
  ): ChartDataSet[] {
    const dataSet = new Array<ChartDataSet>();

    if (typeof stats.packetLoss === 'object') {
      for (const service of stats.packetLoss) {
        const chartPoints = new Array<ChartPoint>();
        const name = service.name;
        const siteId = service.site;
        const points = service.stats;
        const site = sites.find(s => s.config.identifier === siteId);

        if (points instanceof Array) {
          for (const point of points) {
            chartPoints.push({
              x: new Date(point[0] * SecToMsecFactor),
              y: +point[1]
            });
          }
        }

        const siteName: string = site ? site.config.name : siteId;

        const label: string = siteName
          ? `${siteName}: ${pipelineName || name}`
          : name;

        dataSet.push({
          label,
          data: chartPoints
        });
      }
    }

    return dataSet.length ? dataSet : [{
      data: []
    }];
  }

  public static get config(): ChartConfig {
    return Config;
  }
}
