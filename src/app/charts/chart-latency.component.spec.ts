import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { ChartLatencyComponent } from './chart-latency.component';
import { TenantService, TenantStats } from '../tenant/tenant.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { environment } from '../../environments/environment';
import { ChartLineComponent, ChartDataSet } from '../shared/chart-line/chart-line.component';
import { AlertService } from '../shared/alert.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { ClusterService } from '../home/cluster.service';
import { Pipeline, PipelineNfStats } from '../pipeline/pipeline.model';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Site } from 'rest_client/pangolin/model/site';
import { Tenant } from 'rest_client/heimdallr/model/tenant';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { BASE_PATH as BASE_PATH_STATS } from 'rest_client/pangolin/variables';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { TenantStatsRequest } from 'rest_client/pangolin/model/tenantStatsRequest';

describe('ChartLatencyComponent', () => {
  let component: ChartLatencyComponent;
  let fixture: ComponentFixture<ChartLatencyComponent>;
  let httpTestingController: HttpTestingController;
  let el: HTMLElement;

  const mockTenant: Tenant = {
    identifier: '1',
    name: 'Tenant A'
  };

  const mockSelectedPipelines: Pipeline[] = [
    {
      identifier: '1',
      name: 'Service A'
    }
  ];

  const mockTenantStats = {
    'id': '-1',
    'name': '',
    'services': 2,
    'throughputOut': [],
    'throughputIn': [],
    'status': 'good',
    'packetLoss': [],
    'latency': [
      {
        'site': 'eric_site',
        'name': 'ddp_pid2',
        'stats': [
          [
            1531154093,
            '12287'
          ]
        ]
      }
    ]
  } as TenantStats;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule,
        NoopAnimationsModule
      ],
      declarations: [
        ChartLatencyComponent,
        ChartLineComponent,
        ChartHeaderComponent,
        LegendComponent
      ],
      providers: [
        TenantService,
        AlertService,
        PipelineNamePipe,
        ClusterService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        {
          provide: BASE_PATH_STATS,
          useValue: '/dashboard'
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartLatencyComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should start the stat stream when active', inject([TenantService], (tenantService: TenantService) => {
    spyOn(tenantService, 'streamTenantStats').and.callThrough();

    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    expect(tenantService.streamTenantStats).toHaveBeenCalled();
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[0]).toEqual(mockTenant.identifier);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[1]).toEqual(22000);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[2]).toEqual(StatFilter.LATENCY);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[3]).toEqual(21600);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[4]).toEqual(22);
    expect((tenantService.streamTenantStats as any).calls.argsFor(0)[5][0]).toEqual(mockSelectedPipelines[0].identifier);
  }));

  it('should create data set points for chart', fakeAsync(inject([TenantService], (tenantService: TenantService) => {
    spyOn(tenantService, 'streamTenantStats').and.callThrough();

    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    component['_subscriptions'].unsubscribe();

    component['_sites'] = [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      } as Site
    ];

    tick();
    const req = httpTestingController.expectOne(
      `${environment.restServer}/dashboard/tenants/${mockTenant.identifier}/stats`
    );
    expect(req.request.method).toBe('POST');

    const body: TenantStatsRequest = {
      services: [mockSelectedPipelines[0].identifier],
      filter: {
        filter: [StatFilter.LATENCY]
      },
      range: {
        start: undefined,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);
    expect(req.request.responseType).toBe('json');

    req.flush(mockTenantStats);
    expect(component.dataSets[0].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    expect(component.dataSets[0].data[0].y).toBe(12287);
    expect((component.dataSets[0].data[0].x as Date).getTime()).toBe(1531154093000);

    component.active = false;
  })));

  it('should render legend items', () => {
    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;

    component['_sites'] = [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      } as Site
    ];

    const req = httpTestingController.expectOne(
      `${environment.restServer}/dashboard/tenants/${mockTenant.identifier}/stats`
    );
    expect(req.request.method).toBe('POST');

    const body: TenantStatsRequest = {
      services: [mockSelectedPipelines[0].identifier],
      filter: {
        filter: [StatFilter.LATENCY]
      },
      range: {
        start: undefined,
        duration: 21600,
        step: 22
      }
    };

    expect(req.request.body).toEqual(body);
    expect(req.request.responseType).toBe('json');

    req.flush(mockTenantStats);
    fixture.detectChanges();

    expect(component.dataSets[0].label).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    const legends = el.querySelectorAll('nef-chart-header nef-legend span');
    expect(legends.length).toBe(1);
    expect(legends[0].textContent).toBe(`Eric Site: ${mockSelectedPipelines[0].name}`);
    component.active = false;
  });

  it('should start pipeline stats stream when active', () => {
    component.tenant = mockTenant;
    component.selectedPipelines = mockSelectedPipelines;
    component.active = true;
    component.pipelineStreamActive = true;
    component.selectedNFs = ['user_vsrx'];

    spyOn<any>(component, 'updateChart').and.callFake((dataSets: ChartDataSet[]) => {
      expect(dataSets[0].label).toBe('user_vsrx');
      expect(dataSets[0].data[0].y).toBe(1000);
    });

    const response: PipelineNfStats = { "user_vsrx": { throughput: 2040.5430388025459, loss: 3, latency: 1000 } };
    const req = httpTestingController.match(
      `${environment.restServer}/dashboard/tenants/${mockTenant.identifier}/services/${mockSelectedPipelines[0].identifier}/stats`
    );
    req[1].flush(response);

    component.active = false;
  });
});
