import { Component } from '@angular/core';
import { TenantService, TenantStats } from '../tenant/tenant.service';
import { map } from 'rxjs/operators';
import { BaseChart } from './base-chart';
import { AlertService } from '../shared/alert.service';
import { CommonChart, StatsType } from './common-chart';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { ServersService } from '../cluster/servers.service';
import { Stats } from 'rest_client/pangolin/model/stats';
import { LoggerService } from '../shared/logger.service';
import { ClusterService } from '../home/cluster.service';
import { PipelineService } from '../pipeline/pipeline.service';
import { Pipeline, PipelineNfStats, SERVICE_AGGREGATE } from '../pipeline/pipeline.model';
import { ChartDataSet } from '../shared/chart-line/chart-line.component';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';

@Component({
  selector: 'nef-chart-throughput',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.less']
})
export class ChartThroughputComponent extends BaseChart {

  constructor(
    tenantService: TenantService,
    serversService: ServersService,
    loggerService: LoggerService,
    clusterService: ClusterService,
    pipelineService: PipelineService,
    pipelineNameService: PipelineNamePipe,
    alert: AlertService,
  ) {
    super(tenantService, serversService, clusterService, pipelineService, pipelineNameService, alert, loggerService);
  }

  protected enablePipelineStatsStream(pipelines: Pipeline[]) {
    if (!pipelines) {
      return;
    }
    if (this._isActive) {

      if (this._pipelineStatStreamSubscription) {
        this._pipelineStatStreamSubscription.unsubscribe();
      }

      this._step = undefined;
      this._pollInterval = CommonChart.getOptimalPollInterval(this._rate);

      const obs$ = this.postPipelineStats$(pipelines[0].identifier);
      this._pipelineStatStreamSubscription = obs$.pipe(
        map((stats: PipelineNfStats) => this.filterSelectedNFs(stats)),
        map((stats: PipelineNfStats) => CommonChart.handlePipelineStats(stats, StatsType.Throughput))
      )
        .subscribe(
          (dataSets: ChartDataSet[]) => {
            this.updateChart(dataSets);
            this.setLegendItems();
          },
          () => {
            setTimeout(() => {
              this.enablePipelineStatsStream(pipelines);
            }, this._pollInterval);
          });
    }
  }

  protected enableStatsStream(pipelines: Pipeline[]) {
    if (this._isActive) {
      if (this._statStreamSubscription) {
        this._statStreamSubscription.unsubscribe();
        this._statStreamSubscription = undefined;
      }

      this._step = CommonChart.getOptimalStep(this._rate);
      this._pollInterval = CommonChart.getOptimalPollInterval(this._rate);

      const pipelineIDs = pipelines?.map(({ identifier }) => identifier);

      const obs$ = this.getStats$(pipelineIDs, StatFilter.THROUGHPUT);
      const getTotal = !pipelines
        || pipelines.findIndex(({ identifier }) => identifier === SERVICE_AGGREGATE) >= 0;

      const pipelineName = pipelines && pipelines[0]?.name;

      this._statStreamSubscription = obs$.pipe(
        map((stats: TenantStats | Stats) => CommonChart.handleThroughputStats(stats, this._sites, pipelineName, getTotal))
      )
        .subscribe(
          (dataSets: ChartDataSet[]) => {
            this.updateChart(dataSets);
            this.setLegendItems();
          },
          () => {
            this._statStreamTimer = window.setTimeout(() => {
              // fixes chart artifacts appearing after going offline/online
              if (this._dataSets) {
                this._dataSets.length = 0;
              }

              this.enableStatsStream(pipelines);
            }, this._pollInterval);
          }
        );
    }
  }
}
