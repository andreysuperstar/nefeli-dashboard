import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatInputModule } from '@angular/material/input';

import { EncapComponent } from './encap.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [EncapComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatExpansionModule,
    MatRadioModule,
    MatInputModule,
    MatButtonToggleModule,
    SharedModule
  ],
  exports: [EncapComponent]
})
export class EncapModule { }
