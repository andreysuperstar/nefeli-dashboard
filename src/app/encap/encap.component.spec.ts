import { cloneDeep } from 'lodash-es';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatRadioModule } from '@angular/material/radio';
import { MatRadioButtonHarness, MatRadioGroupHarness } from '@angular/material/radio/testing';
import { MatButtonToggleHarness } from '@angular/material/button-toggle/testing';

import { Encap } from 'rest_client/pangolin/model/encap';

import { VLANType, VLANMode, VLANRange, VNIRange, UDPPortRange, EncapComponent } from './encap.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { TemplateInputComponent } from '../shared/template-input/template-input.component';
import { CodemirrorComponent } from '../shared/codemirror/codemirror.component';
import { CREATE_TEMPLATE_VARIABLE, DocChangeEvent } from '../utils/codemirror';
import { TemplateService } from '../templates/template.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('EncapComponent', () => {
  let component: EncapComponent;
  let fixture: ComponentFixture<EncapComponent>;
  let loader: HarnessLoader;
  let formBuilder: FormBuilder;
  let templateService: TemplateService;

  const mockEncap: Encap = {
    ivid: 100,
    ovid: 200,
    vxlan: {
      vni: 300,
      vtep: {
        mac: 'ec:0d:9a:2f:c7:17',
        ipAddr: '127.0.0.1',
        udpPort: 4789
      }
    }
  };

  const createDocChangeEvent = (varName: string, value: string): DocChangeEvent => {
    return {
      change: {
        text: [CREATE_TEMPLATE_VARIABLE(varName)],
        from: {}
      },
      doc: {
        getValue: () => value,
        markText: (to, from) => undefined
      }
    } as DocChangeEvent;
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        FlexLayoutModule,
        MatRadioModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonToggleModule,
        HttpClientTestingModule
      ],
      declarations: [
        EncapComponent,
        TemplateInputComponent,
        CodemirrorComponent
      ],
      providers: [FormBuilder]
    });

    formBuilder = TestBed.inject(FormBuilder);
    templateService = TestBed.inject(TemplateService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EncapComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;

    component.formGroup = formBuilder.group({});
    component.encap = mockEncap;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should properly fill default data', async () => {

    let toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();

    fixture.detectChanges();

    const debugEl: DebugElement = fixture.debugElement;
    const ividEl: HTMLInputElement = debugEl.query(By.css('input[formcontrolname=inner]')).nativeElement;
    const ovidEl: HTMLInputElement = debugEl.query(By.css('input[formcontrolname=outer]')).nativeElement;
    expect(ividEl.value).toBe('100');
    expect(ovidEl.value).toBe('200');

    toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));
    await toggle.check();
    fixture.detectChanges();

    const vniEl: HTMLInputElement = debugEl.query(By.css('input[formcontrolname=vni]')).nativeElement;
    const macEl: HTMLInputElement = debugEl.query(By.css('input[formcontrolname=mac]')).nativeElement;
    const ipEl: HTMLInputElement = debugEl.query(By.css('input[formcontrolname=ip]')).nativeElement;
    const portEl: HTMLInputElement = debugEl.query(By.css('input[formcontrolname=udpPort]')).nativeElement;
    expect(vniEl.value).toBe('300');
    expect(macEl.value).toBe('ec:0d:9a:2f:c7:17');
    expect(ipEl.value).toBe('127.0.0.1');
    expect(portEl.value).toBe('4789');
  });

  it('should show errors for invalid vlan fields', async () => {
    // open vlan panel
    const toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();
    fixture.detectChanges();

    // choose 802.1ad
    const vlanAD = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: '802.1ad'
    }));
    await vlanAD.check();

    // set invalid inner & outer vid
    const debugEl: DebugElement = fixture.debugElement;
    const ividEl: DebugElement = debugEl.query(By.css('input[formcontrolname=inner]'));
    const ovidEl: DebugElement = debugEl.query(By.css('input[formcontrolname=outer]'));
    ividEl.nativeElement.value = VLANRange.Min - 1;
    ividEl.nativeElement.dispatchEvent(new Event('input'));
    ividEl.nativeElement.dispatchEvent(new Event('blur'));
    ovidEl.nativeElement.value = VLANRange.Max + 1;
    ovidEl.nativeElement.dispatchEvent(new Event('input'));
    ovidEl.nativeElement.dispatchEvent(new Event('blur'));
    fixture.detectChanges();

    // check for errors
    const errorEl: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-form-field mat-error'));
    expect(errorEl[0].nativeElement.textContent.trim()).toBe('Outer VID maximum value is 4094');
    expect(errorEl[1].nativeElement.textContent.trim()).toBe('Inner VID minimum value is 1');
  });

  it('should show errors for invalid vxlan fields', async () => {
    // open vxlan panel
    const toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));
    await toggle.check();
    fixture.detectChanges();

    // set vxlan
    const vniInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=vni]'));
    const macInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=mac]'));
    const ipInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=ip]'));
    const portInputEl: DebugElement = fixture.debugElement.query(By.css('input[formcontrolname=udpPort]'));
    vniInputEl.nativeElement.value = VNIRange.Max + 1;
    vniInputEl.nativeElement.dispatchEvent(new Event('input'));
    vniInputEl.nativeElement.dispatchEvent(new Event('blur'));
    macInputEl.nativeElement.value = '02:42:68:65:42';
    macInputEl.nativeElement.dispatchEvent(new Event('input'));
    macInputEl.nativeElement.dispatchEvent(new Event('blur'));
    ipInputEl.nativeElement.value = '127.0.0';
    ipInputEl.nativeElement.dispatchEvent(new Event('input'));
    ipInputEl.nativeElement.dispatchEvent(new Event('blur'));
    portInputEl.nativeElement.value = UDPPortRange.Min - 1;
    portInputEl.nativeElement.dispatchEvent(new Event('input'));
    portInputEl.nativeElement.dispatchEvent(new Event('blur'));
    fixture.detectChanges();

    const errorEl: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-form-field mat-error'));
    expect(errorEl[0].nativeElement.textContent.trim()).toBe('VNI maximum value is 16777215');
    expect(errorEl[1].nativeElement.textContent.trim()).toBe('Not a valid MAC address');
    expect(errorEl[2].nativeElement.textContent.trim()).toBe('Not a valid IP address (192.168.0.1)');
    expect(errorEl[3].nativeElement.textContent.trim()).toBe('UDP port minimum value is 1024');
  });

  it('should disable None, QinQ VLAN options and VXLAN panel', async () => {
    const toggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await toggle.check();

    const vlanTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName=type]'
    }));

    const vlanTypeRadioButtons = await vlanTypeRadioGroup.getRadioButtons();

    expect(await vlanTypeRadioButtons[0].isDisabled()).toBe(
      false,
      `enabled ${VLANType.None} radio button`
    );

    expect(await vlanTypeRadioButtons[2].isDisabled()).toBe(
      false,
      `enabled ${VLANType['802.1AD']} radio button`
    );

    const vxlanPanel = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));

    expect(await vxlanPanel.isDisabled()).toBe(false, 'enabled VXLAN panel');

    // set VLAN mode to 802.1Q only
    component.vlanMode = VLANMode['802.1QOnly'];
    fixture.detectChanges();

    expect(await vlanTypeRadioButtons[0].isDisabled()).toBe(
      true,
      `disabled ${VLANType.None} radio button`
    );

    expect(await vlanTypeRadioButtons[1].isChecked()).toBe(
      true,
      `checked ${VLANType['802.1Q']} radio button`
    );

    expect(await vlanTypeRadioButtons[2].isDisabled()).toBe(
      true,
      `disabled ${VLANType['802.1AD']} radio button`
    );
  });

  it('should hide VLAN types', async () => {
    const vlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    const vxlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));

    // open VLAN panel
    await vlanToggle.check();
    fixture.detectChanges();

    // visible VLAN types radio buttons by default
    const vlanTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName=type]'
    }));

    const vlanTypeRadioButtons = await vlanTypeRadioGroup.getRadioButtons();

    expect(await vlanTypeRadioButtons[0].getLabelText()).toBe(
      'None',
      `None radio button`
    );

    expect(await vlanTypeRadioButtons[1].getLabelText()).toBe(
      VLANType['802.1Q'],
      `${VLANType['802.1Q']} radio button`
    );

    expect(await vlanTypeRadioButtons[2].getLabelText()).toBe(
      VLANType['802.1AD'],
      `${VLANType['802.1AD']} radio button`
    );

    component.vlanMode = VLANMode['802.1ADOVIDOnly'];
    fixture.detectChanges();

    // hidden VLAN types
    const vlanTypeRadioGroupDe = fixture.debugElement.query(
      By.css('[formControlName=type]')
    );

    expect(vlanTypeRadioGroupDe).toBeNull('hide VLAN type radio buttons');

    // hidden inner VID
    const innerVIDDe = fixture.debugElement.query(
      By.css('[formControlName=inner]')
    );

    expect(innerVIDDe).toBeNull('hide inner VID');
  });

  it('should get correct vlan type', () => {
    const encap = cloneDeep(mockEncap);

    component.encap = encap;

    // 802.1AD and Outer VID only VLAN mode
    component.vlanMode = VLANMode['802.1ADOVIDOnly'];

    expect(component['vlanType']).toBe(
      VLANType['802.1AD'],
      `set ${VLANType['802.1AD']} for 802.1AD and Outer VID only VLAN mode`
    );

    component.vlanMode = undefined;

    // if (ivid && ovid) return '802.1AD'
    component['_hiddenVLANIVID'] = true;
    expect(component['vlanType']).toBe(VLANType['802.1AD']);
    component['_hiddenVLANIVID'] = false;
    expect(component['vlanType']).toBe(VLANType['802.1AD']);

    // if (hiddenVLANIVID && ovid) return '802.1AD'
    component['_hiddenVLANIVID'] = true;
    encap.ivid = undefined;
    component.encap = encap;
    expect(component['vlanType']).toBe(VLANType['802.1AD']);

    // if (hideVlan8021adIvid && !ovid) return None
    component['_hiddenVLANIVID'] = true;
    encap.ivid = 100;
    encap.ovid = undefined;
    component.encap = encap;
    expect(component['vlanType']).toBe(VLANType.None);

    // if (ivid && !ovid) return '802.1Q'
    component['_hiddenVLANIVID'] = false;
    expect(component['vlanType']).toBe('802.1Q');

    // if (!ivid && !ovid) return None
    component['_hiddenVLANIVID'] = true;
    encap.ivid = undefined;
    encap.ovid = undefined;
    component.encap = encap;
    expect(component['vlanType']).toBe(VLANType.None);
  });

  it('should expand invalid Encap panels', async () => {
    const vlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    const vxlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));

    // open VLAN panelß
    await vlanToggle.check();
    fixture.detectChanges();

    // select 802.1ad VLAN
    const vlanAD = await loader.getHarness<MatRadioButtonHarness>(
      MatRadioButtonHarness.with({
        label: '802.1ad'
      })
    );

    await vlanAD.check();

    // set invalid outer VID and close VLAN panel
    const ovidDe: DebugElement = fixture.debugElement.query(
      By.css('input[formcontrolname=outer]')
    );

    ovidDe.nativeElement.value = VLANRange.Max + 1;
    ovidDe.nativeElement.dispatchEvent(new Event('input'));

    await vxlanToggle.check();
    fixture.detectChanges();

    component.openInvalidPanels();

    // expanded VLAN panel assertion
    expect(await vlanToggle.isChecked()).toBeTruthy('VLAN panel is expanded');
    expect(await vxlanToggle.isChecked()).toBeFalsy('VXLAN panel is closed');

    // open VXLAN panel
    await vxlanToggle.check();

    // set invalid UDP Port and close VXLAN panel
    const udpPortInputDe: DebugElement = fixture.debugElement.query(
      By.css('input[formcontrolname=udpPort]')
    );

    udpPortInputDe.nativeElement.value = UDPPortRange.Min - 1;
    udpPortInputDe.nativeElement.dispatchEvent(new Event('input'));

    await vlanToggle.check();

    component.openInvalidPanels();

    // if errors in both forms, expand VXLAN only
    expect(await vlanToggle.isChecked()).toBeFalsy('VLAN panel is closed');
    expect(await vxlanToggle.isChecked()).toBeTruthy('VXLAN panel is open');

    // reset invalid VLAN outer VID
    ovidDe.nativeElement.value = '';
    ovidDe.nativeElement.dispatchEvent(new Event('input'));

    // // reset Encap panels state
    await vlanToggle.check();

    component.openInvalidPanels();

    // expanded VXLAN panel assertion
    expect(await vlanToggle.isChecked()).toBeFalsy('VLAN panel is closed');
    expect(await vxlanToggle.isChecked()).toBeTruthy('VXLAN panel is expanded');
  });

  it('should render template mode', async () => {
    const el = fixture.debugElement.nativeElement;
    component.isTemplate = true;

    // open vlan panel
    const vlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await vlanToggle.check();

    // select 802.1Q
    const vlanQ = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: '802.1Q'
    }));
    await vlanQ.check();
    fixture.detectChanges();

    const [vid] = el.querySelectorAll('nef-template-input');
    expect(vid.getAttribute('placeholder')).toBe('Enter VID');

    // select 802.1ad
    const vlanAD = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: '802.1ad'
    }));
    await vlanAD.check();
    fixture.detectChanges();

    const [outer, inner] = el.querySelectorAll('nef-template-input');
    expect(outer.getAttribute('placeHolder')).toBe('Enter outer');
    expect(inner.getAttribute('placeHolder')).toBe('Enter inner');

    // open vxlan panel
    const vxlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));
    await vxlanToggle.check();
    fixture.detectChanges();

    const [vni, mac, ip, port] = el.querySelectorAll('nef-template-input');
    expect(vni.getAttribute('placeHolder')).toBe('Enter VNI');
    expect(mac.getAttribute('placeHolder')).toBe('Enter MAC');
    expect(ip.getAttribute('placeHolder')).toBe('Enter IP');
    expect(port.getAttribute('placeHolder')).toBe('Enter UDP port');
  });

  it('should add template variable', async () => {
    const addVariableSpy = spyOn(templateService, 'addTemplateVariable');
    const el = fixture.debugElement;
    component.isTemplate = true;
    // open vlan panel
    const vlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await vlanToggle.check();
    // select 802.1Q
    const vlanQ = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: '802.1Q'
    }));
    await vlanQ.check();
    fixture.detectChanges();
    const [vid] = el.queryAll(By.directive(CodemirrorComponent));
    vid.componentInstance.docChange.emit(createDocChangeEvent('template-var-1', 'variable one'));
    expect(addVariableSpy).toHaveBeenCalledWith('template-var-1');
  });

  it('should save template variables', async () => {
    const el = fixture.debugElement;
    component.isTemplate = true;

    // open vlan panel
    const vlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VLAN'
    }));
    await vlanToggle.check();

    // select 802.1Q
    const vlanQ = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: '802.1Q'
    }));
    await vlanQ.check();
    fixture.detectChanges();
    const [vid] = el.queryAll(By.directive(CodemirrorComponent));

    vid.componentInstance.docChange.emit(createDocChangeEvent('template-var-1', 'variable one'));

    // select 802.1ad
    const vlanAD = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: '802.1ad'
    }));
    await vlanAD.check();
    fixture.detectChanges();
    const [outer, inner] = el.queryAll(By.directive(CodemirrorComponent));
    outer.componentInstance.docChange.emit(createDocChangeEvent('template-var-2', 'variable two'));
    inner.componentInstance.docChange.emit(createDocChangeEvent('template-var-3', 'variable three'));

    // open vxlan panel
    const vxlanToggle = await loader.getHarness<MatButtonToggleHarness>(MatButtonToggleHarness.with({
      text: 'VXLAN'
    }));
    await vxlanToggle.check();
    fixture.detectChanges();
    const [vni, mac, ip, port] = el.queryAll(By.directive(CodemirrorComponent));
    vni.componentInstance.docChange.emit(createDocChangeEvent('template-var-4', 'variable four'));
    mac.componentInstance.docChange.emit(createDocChangeEvent('template-var-5', 'variable five'));
    ip.componentInstance.docChange.emit(createDocChangeEvent('template-var-6', 'variable six'));
    port.componentInstance.docChange.emit(createDocChangeEvent('template-var-7', 'variable seven'));

    expect(component.templateVars).toEqual(['template-var-1', 'template-var-2', 'template-var-3', 'template-var-4', 'template-var-5', 'template-var-6', 'template-var-7']);
  });
});
