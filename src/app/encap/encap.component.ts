import { Component, Input, ViewChild, ElementRef, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, ValidationErrors } from '@angular/forms';

import { Encap } from 'rest_client/pangolin/model/encap';

import { MAC_PATTERN, IP_PATTERN } from '../sites-overview/site.validation';
import { MatRadioChange } from '@angular/material/radio';
import { DocChangeEvent } from '../utils/codemirror';
import { getTemplateVariableText, isTemplateVariable, TemplateService } from '../templates/template.service';
import { Observable } from 'rxjs';

enum EncapType {
  NONE,
  VLAN,
  VXLAN
}

export enum VLANType {
  None = '',
  '802.1Q' = '802.1Q',
  '802.1AD' = '802.1ad'
}

export enum VLANMode {
  '802.1QOnly',
  '802.1ADOVIDOnly'
}

export enum VLANRange {
  Min = 1,
  Max = 4094
}

export enum VNIRange {
  Min = 1,
  Max = 16777215
}

export enum UDPPortRange {
  Min = 1024,
  Max = 49151
}

@Component({
  selector: 'nef-encap',
  templateUrl: './encap.component.html',
  styleUrls: ['./encap.component.less']
})
export class EncapComponent implements OnInit {

  private _formGroup: FormGroup;
  private _encap: Encap;
  private _vlanMode: VLANMode;
  private _disabledVLANNone: boolean;
  private _disabledVLANAD: boolean;
  private _disabledVXLAN: boolean;
  private _hiddenVLANType: boolean;
  private _hiddenVLANIVID: boolean;
  private _hiddenVXLAN: boolean;
  private _selectedType = EncapType.VLAN;
  private _isTemplate = false;

  @Input()
  public set formGroup(form: FormGroup) {
    this._formGroup = form;

    if (form) {
      this.initEncapForm();
    }
  }

  @Input()
  public set encap(encap: Encap) {
    this._encap = encap;

    if (encap && this._formGroup) {
      this.setEncap();
      this.selectedType = this.encap.vxlan ? EncapType.VXLAN : EncapType.VLAN;
    }
  }

  @Input()
  public set vlanMode(vlanMode: VLANMode) {
    this._vlanMode = vlanMode;

    switch (vlanMode) {
      case VLANMode['802.1QOnly']:
        this._formGroup
          ?.get('vlan.vid.outer')
          .reset();

        this._formGroup
          ?.get('vlan.type')
          .setValue(VLANType['802.1Q']);

        this._disabledVLANNone = true;
        this._disabledVLANAD = true;
        this._disabledVXLAN = true;

        break;
      case VLANMode['802.1ADOVIDOnly']:
        this._hiddenVLANType = true;
        this._hiddenVLANIVID = true;
        this._hiddenVXLAN = true;

        break;
      default:
        this._disabledVLANNone = false;
        this._disabledVLANAD = false;
        this._disabledVXLAN = false;

        this._hiddenVLANType = false;
        this._hiddenVLANIVID = false;
        this._hiddenVXLAN = false;
    }
  }

  @Input()
  public set isTemplate(val: boolean) {
    this._isTemplate = val;
  }

  @ViewChild('innerVlanID', { static: false }) private _innerVlanIDElem: ElementRef;
  @ViewChild('outerVlanID', { static: false }) private _outerVlanIDElem: ElementRef;

  constructor(
    private _fb: FormBuilder,
    private _templateService: TemplateService
  ) { }

  public ngOnInit() {
    this.setEncap();
  }

  private get vlanType(): VLANType {
    const ivid = this._encap?.ivid !== undefined && this._encap?.ivid !== null && this._encap?.ivid !== 0;

    return this._vlanMode === VLANMode['802.1ADOVIDOnly']
      || this._encap?.ovid && (ivid || this._hiddenVLANIVID)
      ? VLANType['802.1AD']
      : this._vlanMode === VLANMode['802.1QOnly'] || ivid && !this._hiddenVLANIVID
        ? VLANType['802.1Q']
        : VLANType.None;
  }

  private initEncapForm() {
    this._formGroup.addControl('vlan', this._fb.group({
      type: this.vlanType,
      vid: this._fb.group({
        inner: [
          this._encap?.ivid,
          [
            Validators.min(VLANRange.Min),
            Validators.max(VLANRange.Max),
            this.vlanIvidValidator.bind(this)
          ]
        ],
        outer: [
          this._encap?.ovid,
          [
            Validators.min(VLANRange.Min),
            Validators.max(VLANRange.Max),
            this.vlanOvidValidator.bind(this)
          ]
        ]
      })
    }));

    this._formGroup.addControl('vxlan', this._fb.group({
      vni: [
        this._encap?.vxlan?.vni,
        [
          Validators.min(VNIRange.Min),
          Validators.max(VNIRange.Max)
        ]
      ],
      mac: [
        this._encap?.vxlan?.vtep?.mac,
        Validators.pattern(MAC_PATTERN)
      ],
      ip: [
        this._encap?.vxlan?.vtep?.ipAddr,
        Validators.pattern(IP_PATTERN)
      ],
      udpPort: [
        this._encap?.vxlan?.vtep?.udpPort,
        [
          Validators.min(UDPPortRange.Min),
          Validators.max(UDPPortRange.Max)
        ]
      ]
    }));
  }

  private vlanIvidValidator(control: FormControl): ValidationErrors {
    const vlanType = this._formGroup.get('vlan.type')?.value;
    const vid = control.value;

    if (vlanType === VLANType['802.1AD'] && vid === undefined && !this._hiddenVLANIVID) {
      return { required: true };
    } else if (vlanType === VLANType['802.1Q'] && vid === undefined) {
      return { required: true };
    }

    return undefined;
  }

  private vlanOvidValidator(control: FormControl): ValidationErrors {
    const vlanType = this._formGroup.get('vlan.type')?.value;
    const vid = control.value;

    if (vlanType === VLANType['802.1AD'] && vid === null) {
      return { required: true };
    }

    return undefined;
  }

  public onVLANTypeChange({ value }: MatRadioChange) {
    this._formGroup
      .get('vlan.vid.inner')
      .setValue(undefined);

    this._formGroup
      .get('vlan.vid.outer')
      .reset();

    if (value === VLANType['802.1Q']) {
      setTimeout(() => this._innerVlanIDElem?.nativeElement.focus());
    } else if (value === VLANType['802.1AD']) {
      setTimeout(() => this._outerVlanIDElem?.nativeElement.focus());
    }
  }

  private setEncap() {
    this._formGroup.get('vlan.type').setValue(this.vlanType);
    this._formGroup.get('vlan.vid.inner').setValue(this._encap?.ivid);

    if (this.vlanType === VLANType['802.1AD']) {
      this._formGroup.get('vlan.vid.outer').setValue(this._encap?.ovid);
    }

    this._formGroup.get('vxlan.vni').setValue(this._encap?.vxlan?.vni);
    this._formGroup.get('vxlan.mac').setValue(this._encap?.vxlan?.vtep?.mac);
    this._formGroup.get('vxlan.ip').setValue(this._encap?.vxlan?.vtep?.ipAddr);
    this._formGroup.get('vxlan.udpPort').setValue(this._encap?.vxlan?.vtep?.udpPort);
  }

  public openInvalidPanels() {
    if (this._formGroup.get('vlan').invalid) {
      this._selectedType = EncapType.VLAN;
    }

    if (this._formGroup.get('vxlan').invalid) {
      this._selectedType = EncapType.VXLAN;
    }
  }

  /**
   * @description Fixes Firefox bug allowing to type characters in number input
   * {@link https://bugzilla.mozilla.org/show_bug.cgi?id=1398528}.
   *
   * @param event Keyboard event.
   */
  public onNumberInputKeydown({ key, metaKey, ctrlKey, preventDefault }: KeyboardEvent) {
    if (!(metaKey || ctrlKey) && key.length < 2 && isNaN(Number(key))) {
      preventDefault();
    }
  }

  private handleTemplateText(text: string) {
    if (isTemplateVariable(text)) {
      const varText = getTemplateVariableText(text);
      this._templateService.addTemplateVariable(varText);
    }
  }

  public setFormData(prop: string, event: DocChangeEvent) {
    const text = event?.change?.text[0];

    if (text) {
      this.handleTemplateText(text);
    }

    const data = event.doc.getValue();
    this._formGroup.get(prop).patchValue(data);
    this._formGroup.get(prop).markAsDirty();
  }

  public get formGroup(): FormGroup {
    return this._formGroup;
  }

  public get encap(): Encap {
    return this._encap;
  }

  public get VLANType(): typeof VLANType {
    return VLANType;
  }

  public get VLANRange(): typeof VLANRange {
    return VLANRange;
  }

  public get VNIRange(): typeof VNIRange {
    return VNIRange;
  }

  public get UDPPortRange(): typeof UDPPortRange {
    return UDPPortRange;
  }

  public get disabledVLANNone(): boolean {
    return this._disabledVLANNone;
  }

  public get disabledVLANAD(): boolean {
    return this._disabledVLANAD;
  }

  public get disabledVXLAN(): boolean {
    return this._disabledVXLAN;
  }

  public get hiddenVLANType(): boolean {
    return this._hiddenVLANType;
  }

  public get hiddenVLANIVID(): boolean {
    return this._hiddenVLANIVID;
  }

  public get hiddenVXLAN(): boolean {
    return this._hiddenVXLAN;
  }

  public get EncapType(): typeof EncapType {
    return EncapType;
  }

  public get selectedType(): EncapType {
    return this._selectedType;
  }

  public set selectedType(val: EncapType) {
    this._selectedType = val;
  }

  public get isTemplate(): boolean {
    return this._isTemplate;
  }

  public get templateVars(): string[] {
    return this._templateService.templateVariables;
  }
}
