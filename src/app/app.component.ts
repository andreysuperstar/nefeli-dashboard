import { Component, HostListener } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { LoggerService } from './shared/logger.service';

@Component({
  selector: 'nef-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  @HostListener('document:keypress', ['$event'])
  private handleKeyboardEvent({ ctrlKey, shiftKey, key }: KeyboardEvent) {
    if (ctrlKey && shiftKey) {
      switch (key) {
        case 'L':
          this._loggerService.downloadLogFile();
          break;
        case 'V':
          this._snackBar.open((window as any).nefeliVersion, 'hide');
          break;
      }
    }
  }

  constructor(
    private _snackBar: MatSnackBar,
    private _loggerService: LoggerService,
    private _matIconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer
  ) {
    this._matIconRegistry.addSvgIcon(
      'icon-done-status',
      this._domSanitizer.bypassSecurityTrustResourceUrl('//assets/icon-done-status.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icon-scheduled-status',
      this._domSanitizer.bypassSecurityTrustResourceUrl('//assets/icon-scheduled-status.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icon-pending-status',
      this._domSanitizer.bypassSecurityTrustResourceUrl('//assets/icon-pending-status.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icon-retrying-status',
      this._domSanitizer.bypassSecurityTrustResourceUrl('//assets/icon-retrying-status.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icon-link-on',
      this._domSanitizer.bypassSecurityTrustResourceUrl('//assets/icon-link-on.svg')
    );
    this._matIconRegistry.addSvgIcon(
      'icon-link-off',
      this._domSanitizer.bypassSecurityTrustResourceUrl('//assets/icon-link-off.svg')
    );
  }
}
