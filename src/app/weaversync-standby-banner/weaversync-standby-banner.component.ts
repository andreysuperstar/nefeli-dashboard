import { Component } from '@angular/core';

@Component({
  selector: 'nef-weaversync-standby-banner',
  templateUrl: './weaversync-standby-banner.component.html',
  styleUrls: ['./weaversync-standby-banner.component.less']
})
export class WeaverSyncStandbyBannerComponent { }
