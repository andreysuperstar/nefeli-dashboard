import { By } from '@angular/platform-browser';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeaverSyncStandbyBannerComponent } from './weaversync-standby-banner.component';

describe('WeaverSyncStandbyBannerComponent', () => {
  let component: WeaverSyncStandbyBannerComponent;
  let fixture: ComponentFixture<WeaverSyncStandbyBannerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [WeaverSyncStandbyBannerComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeaverSyncStandbyBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a WeaverSync message', () => {
    const paragraphDe = fixture.debugElement.query(By.css('p'));

    expect(paragraphDe).not.toBeNull('render a message element');
    expect(
      paragraphDe.nativeElement.textContent
        .trim()
        .endsWith('This Weaver is running in WeaverSync standby mode. Access is read-only.')
    ).toBeTruthy('render a message');
  });

  it('should render a link to the Status page', () => {
    const statusPageLinkDe = fixture.debugElement.query(By.css('a'));

    expect(statusPageLinkDe).not.toBeNull('render Status page link element');

    expect(statusPageLinkDe.nativeElement.textContent).toBe(
      'See More',
      'render WeaverSync system configuration link text'
    );
    expect(statusPageLinkDe.nativeElement.getAttribute('routerlink')).toBe(
      '/system-configuration',
      'render WeaverSync system configuration link reference'
    );
    expect(statusPageLinkDe.nativeElement.getAttribute('fragment')).toBe(
      'weaversync',
      'render WeaverSync system configuration link fragment'
    );
  });
});
