import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { SharedModule } from '../shared/shared.module';

import { NotificationsRoutingModule } from './notifications-routing.module';

import { NotificationSettingsComponent } from './notification-settings.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';
import { EditEmailComponent } from './edit-email/edit-email.component';
import { WebhooksComponent } from './webhooks/webhooks.component';
import { AddWebhookComponent } from './add-webhook/add-webhook.component';
import { SlackComponent } from './slack/slack.component';
import { EditSlackComponent } from './edit-slack/edit-slack.component';
import { SNMPComponent } from './snmp/snmp.component';

@NgModule({
  declarations: [
    NotificationSettingsComponent,
    EmailSettingsComponent,
    EditEmailComponent,
    WebhooksComponent,
    AddWebhookComponent,
    SlackComponent,
    EditSlackComponent,
    SNMPComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatButtonModule,
    MatListModule,
    MatInputModule,
    MatCheckboxModule,
    SharedModule,
    NotificationsRoutingModule
  ]
})
export class NotificationsModule { }
