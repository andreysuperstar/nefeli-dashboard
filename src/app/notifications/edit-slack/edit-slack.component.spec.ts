import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgZone } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { BASE_PATH } from 'rest_client/pangolin/variables';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';
import { environment } from '../../../environments/environment';

import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { NotificationService } from '../notification.service';
import { EditSlackComponent } from './edit-slack.component';
import { BackButtonComponent } from '../../shared/back-button/back-button.component';
import { AlertClassPipe } from '../../pipes/alert-class.pipe';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';

describe('EditSlackComponent', () => {
  let component: EditSlackComponent;
  let fixture: ComponentFixture<EditSlackComponent>;
  let el: HTMLElement;
  let loader: HarnessLoader;
  let httpMockClient: HttpTestingController;
  let notificationService: NotificationService;
  let ngZone: NgZone;
  const uuid = 'dc143517-757c-41f4-95a1-4e85e38ad832';
  const mockSlackData = {
    name: "My Slack Config",
    uri: "https://slack.com/api",
    channel: 'MyAlarms',
    username: 'admin',
    levels: {
      value: [
        'INFO',
        'WARNING',
        'ERROR'
      ]
    },
    classes: {
      value: [AlertClass.AUTH]
    },
    'enabled': true,
    'identifier': uuid
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCheckboxModule,
        MatSnackBarModule
      ],
      declarations: [
        EditSlackComponent,
        BackButtonComponent,
        AlertClassPipe
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        AlertService,
        NotificationService
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
    notificationService = TestBed.inject(NotificationService);
    ngZone = TestBed.inject(NgZone);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSlackComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    const title = el.querySelector('h4');

    expect(title.textContent.trim()).toBe('Connect slack');
  });

  it('should validate email field', () => {
    const username: AbstractControl = component.slackForm.get('username');

    username.setValue('');

    expect(username.errors.required).toBeTruthy();

    username.setValue('test@test');

    expect(username.errors).toBeNull();
  });

  it('should make a POST request to connect slack account on form submit', () => {
    component.slackForm.markAsDirty();

    component.slackForm.patchValue({
      name: 'slack1',
      username: 'slack',
      alerts: [true, false, false]
    });

    component.submitSlackForm();

    expect(component.slackForm.valid).toBeTruthy();

    const req = httpMockClient.expectOne(
      `${environment.restServer}/v1/alarms/receivers/slack`
    );

    expect(req.request.method).toBe('POST');
    expect(req.request.body.username).toBe('slack');
    expect(req.request.body.levels.value.length).toBe(1);
    expect(req.request.body.levels.value[0]).toBe('INFO');
  });

  it('should populate edit form with slack data', async () => {
    component['_uuid'] = uuid;
    component['initEditMode']();
    const req = httpMockClient.expectOne(
      `${environment.restServer}/v1/alarms/receivers/slack/${uuid}`
    );
    req.flush(mockSlackData);
    const nameInput: HTMLInputElement = el.querySelector('[formcontrolname=name]');
    const urlInput: HTMLInputElement = el.querySelector('[formcontrolname=url]');
    const channelInput: HTMLInputElement = el.querySelector('[formcontrolname=channel]');
    const usernameInput: HTMLInputElement = el.querySelector('[formcontrolname=username]');

    expect(nameInput.value).toBe('My Slack Config');
    expect(urlInput.value).toBe('https://slack.com/api');
    expect(channelInput.value).toBe('MyAlarms');
    expect(usernameInput.value).toBe('admin');

    const infoCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Info'
    }));
    const warningCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Warning'
    }));
    const errorCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Error'
    }));
    const criticalCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Critical'
    }));

    expect(await infoCheckbox.isChecked()).toBeTruthy();
    expect(await warningCheckbox.isChecked()).toBeTruthy();
    expect(await errorCheckbox.isChecked()).toBeTruthy();
    expect(await criticalCheckbox.isChecked()).toBeFalsy();

    const licenseClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'License'
    }));
    const performanceClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Performance'
    }));
    const serviceClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Service'
    }));
    const userClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'User'
    }));
    const siteClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Site'
    }));
    const authClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Auth'
    }));
    const nfClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'NF'
    }));
    const machineClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Machine'
    }));

    expect(await licenseClassCheckbox.isChecked()).toBeFalsy();
    expect(await performanceClassCheckbox.isChecked()).toBeFalsy();
    expect(await serviceClassCheckbox.isChecked()).toBeFalsy();
    expect(await userClassCheckbox.isChecked()).toBeFalsy();
    expect(await siteClassCheckbox.isChecked()).toBeFalsy();
    expect(await authClassCheckbox.isChecked()).toBeTruthy();
    expect(await nfClassCheckbox.isChecked()).toBeFalsy();
    expect(await machineClassCheckbox.isChecked()).toBeFalsy();

  })

  it('should render alert type checkboxes', () => {
    const checkboxes = el.querySelectorAll('[formArrayName=alerts] .mat-checkbox');

    expect(checkboxes.length).toBe(4);

    expect(checkboxes[0].textContent.trim()).toBe('Info');
    expect(checkboxes[1].textContent.trim()).toBe('Warning');
    expect(checkboxes[2].textContent.trim()).toBe('Error');
    expect(checkboxes[3].textContent.trim()).toBe('Critical');
  });

  it('should render alert classes checkboxes', () => {
    const checkboxes = el.querySelectorAll('[formArrayName=classes] .mat-checkbox');

    expect(checkboxes.length).toBe(8);

    expect(checkboxes[0].textContent.trim()).toBe('License');
    expect(checkboxes[1].textContent.trim()).toBe('Performance');
    expect(checkboxes[2].textContent.trim()).toBe('Service');
    expect(checkboxes[3].textContent.trim()).toBe('User');
    expect(checkboxes[4].textContent.trim()).toBe('Site');
    expect(checkboxes[5].textContent.trim()).toBe('Auth');
    expect(checkboxes[6].textContent.trim()).toBe('NF');
    expect(checkboxes[7].textContent.trim()).toBe('Machine');
  });

  it('should not submit if form is not valid', () => {
    spyOn<any>(notificationService, 'connectSlack').and.callThrough();

    component.slackForm.markAsDirty();

    component.slackForm.patchValue({
      email: 'test'
    });

    component.submitSlackForm();

    expect(notificationService.connectSlack).not.toHaveBeenCalled();
  });

  it('should handle successfull add email request', () => {
    spyOn(component['_alertService'], 'info').and.callThrough();

    component.slackForm.markAsDirty();

    component.slackForm.patchValue({
      name: 'slack1',
      username: 'username'
    });

    ngZone.run(() => {
      component.submitSlackForm();

      const req = httpMockClient.expectOne(
        `${environment.restServer}/v1/alarms/receivers/slack`
      );

      req.flush({
        id: 1,
        name: 'slack',
        email: 'slack@email',
        alerts: [],
        classes: []
      });

      expect(
        component['_alertService'].info
      ).toHaveBeenCalledWith(AlertType.INFO_CONNECT_SLACK_SUCCESS);
    });
  });

  it('should handle error response', () => {
    spyOn(component['_alertService'], 'error').and.callThrough();

    component.slackForm.markAsDirty();

    component.slackForm.patchValue({
      name: 'slack1',
      username: 'username'
    });

    component.submitSlackForm();

    const req = httpMockClient.expectOne(
      `${environment.restServer}/v1/alarms/receivers/slack`
    );

    const errorResponse = {
      status: 400,
      statusText: 'Bad Request'
    };

    req.flush({}, errorResponse);

    expect(
      component['_alertService'].error
    ).toHaveBeenCalledWith(AlertType.ERROR_CONNECT_SLACK);
  });
});
