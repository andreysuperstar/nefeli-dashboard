import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';

import { SlackHookConfig } from 'rest_client/pangolin/model/slackHookConfig';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { AlertClassState, NotificationService, NotificationState } from '../notification.service';

@Component({
  selector: 'nef-edit-slack',
  templateUrl: './edit-slack.component.html',
  styleUrls: ['./edit-slack.component.less']
})
export class EditSlackComponent implements OnInit, OnDestroy {

  private _uuid: string;
  private _backPath = '../';
  private _slackForm: FormGroup;
  private _slackAccountSubscription: Subscription;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _notificationService: NotificationService
  ) { }

  public ngOnInit() {
    this.initForm();

    this._uuid = this._route.snapshot.params.id;

    if (this._uuid) {
      this.initEditMode();
    }
  }

  private initEditMode() {
    this._backPath = '../../';

    this._slackAccountSubscription = this._notificationService
      .getSlackAccount(this._uuid)
      .subscribe((slackAccount: SlackHookConfig) => this.patchForm(slackAccount));
  }

  public ngOnDestroy() {
    if (this._slackAccountSubscription) {
      this._slackAccountSubscription.unsubscribe();
    }
  }

  private get alertControls(): FormArray {
    const controls: FormControl[] = this._notificationService.notificationTypes.map(
      (alert: NotificationState): FormControl => this._fb.control(alert.selected)
    );

    return this._fb.array(controls);
  }

  private get alertClassControls(): FormArray {
    const controls: FormControl[] = this._notificationService.alertClassStates.map(
      (alertClassState: AlertClassState): FormControl => this._fb.control(alertClassState.selected)
    );

    return this._fb.array(controls);
  }

  private initForm() {
    this._slackForm = this._fb.group({
      name: '',
      url: '',
      channel: '',
      username: ['', Validators.required],
      alerts: this.alertControls,
      classes: this.alertClassControls
    });
  }

  private patchForm(slackAccount: SlackHookConfig) {
    const {
      name,
      uri: url,
      channel,
      username,
      levels,
      classes
    } = slackAccount;

    this._slackForm.patchValue({
      name,
      url,
      channel,
      username,
      alerts: this._notificationService.convertAlertLevels(levels.value),
      classes: this._notificationService.convertAlertClasses(classes.value)
    });
  }

  public submitSlackForm() {
    const { invalid, pristine, value } = this._slackForm;

    if (invalid || pristine) {
      return;
    }

    const { name, url, channel, username, alerts, classes }: {
      name: string;
      url: string;
      channel: string;
      username: string;
      alerts: boolean[],
      classes: boolean[]
    } = value;

    const config: SlackHookConfig = {
      name,
      uri: url,
      channel,
      username,
      levels: {
        value: this._notificationService.convertAlertTypes(alerts)
      },
      classes: {
        value: this._notificationService.convertAlertClassStates(classes)
      }
    };
    this._uuid ? this.editSlack(config) : this.addSlack(config);
  }

  private addSlack(config: SlackHookConfig) {

    this._notificationService
      .connectSlack(config)
      .subscribe(
        (_: string) => {
          this._alertService.info(AlertType.INFO_CONNECT_SLACK_SUCCESS);

          this._router.navigate([this._backPath], {
            relativeTo: this._route
          });
        },
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_CONNECT_SLACK)
      );
  }

  private editSlack(config: SlackHookConfig) {
    this._notificationService
      .patchSlackAccount(this._uuid, config)
      .subscribe(
        (_: any[]) => {
          this._alertService.info(AlertType.INFO_EDIT_SLACK_SUCCESS);

          this._router.navigate([this._backPath], {
            relativeTo: this._route
          });
        },
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_EDIT_SLACK)
      );
  }

  public get backPath(): string {
    return this._backPath;
  }

  public get uuid(): string {
    return this._uuid;
  }

  public get slackForm(): FormGroup {
    return this._slackForm;
  }

  public get alerts(): FormArray {
    return this._slackForm.get('alerts') as FormArray;
  }

  public get classes(): FormArray {
    return this._slackForm.get('classes') as FormArray;
  }

  public get notifications(): NotificationState[] {
    return this._notificationService.notificationTypes;
  }
  public get classStates(): AlertClassState[] {
    return this._notificationService.alertClassStates;
  }
}
