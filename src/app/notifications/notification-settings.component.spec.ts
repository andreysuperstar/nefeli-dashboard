import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { NotificationSettingsComponent } from './notification-settings.component';

describe('NotificationSettingsComponent', () => {
  let component: NotificationSettingsComponent;
  let fixture: ComponentFixture<NotificationSettingsComponent>;
  let el: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NotificationSettingsComponent],
      imports: [
        NoopAnimationsModule,
        MatListModule,
        MatCardModule,
        RouterTestingModule
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationSettingsComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a header', () => {
    expect(el.querySelector('h2').textContent).toBe('Alerts Settings');
  });

  it('should render a menu', () => {
    const menu = el.querySelector('mat-nav-list');
    const items = menu.querySelectorAll('.mat-list-item');
    expect(menu).not.toBeNull;
    expect(items.length).toBe(4);
    expect(items[0].textContent).toBe('Email');
    expect(items[1].textContent).toBe('Slack');
    expect(items[2].textContent).toBe('SNMP');
  });
});
