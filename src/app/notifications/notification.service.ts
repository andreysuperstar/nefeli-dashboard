import { forkJoin, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { SlackHooks } from 'rest_client/pangolin/model/slackHooks';
import { WebHooks } from 'rest_client/pangolin/model/webHooks';
import { EmailHooks } from 'rest_client/pangolin/model/emailHooks';
import { WebHookConfig } from 'rest_client/pangolin/model/webHookConfig';
import { AlarmsService } from 'rest_client/pangolin/api/alarms.service';
import { map, pluck } from 'rxjs/operators';
import { EmailHookConfig } from 'rest_client/pangolin/model/emailHookConfig';
import { SlackHookConfig } from 'rest_client/pangolin/model/slackHookConfig';
import { AlertLevel } from 'rest_client/pangolin/model/alertLevel';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';

// TODO(eric): synchronize NotificationType with AlertLevel
export enum NotificationType {
  Unset = 'UNSET',
  Info = 'INFO',
  Warning = 'WARNING',
  Error = 'ERROR',
  Critical = 'CRITICAL'
}

export interface NotificationState {
  name: NotificationType;
  selected: boolean;
}

export interface AlertClassState {
  name: AlertClass;
  selected: boolean;
}

export type EditWebhookResponse = any;

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private _notificationTypes: NotificationState[] = [
    { name: NotificationType.Info, selected: false },
    { name: NotificationType.Warning, selected: false },
    { name: NotificationType.Error, selected: false },
    { name: NotificationType.Critical, selected: false }
  ];

  private _alertClassStates: AlertClassState[] = [
    { name: AlertClass.LICENSE, selected: false },
    { name: AlertClass.PERFORMANCE, selected: false },
    { name: AlertClass.SERVICE, selected: false },
    { name: AlertClass.USER, selected: false },
    { name: AlertClass.SITE, selected: false },
    { name: AlertClass.AUTH, selected: false },
    { name: AlertClass.NF, selected: false },
    { name: AlertClass.MACHINE, selected: false }
  ];

  constructor(
    private _restAlarmsService: AlarmsService
  ) {
  }

  public connectEmail(config: EmailHookConfig): Observable<string> {
    return this._restAlarmsService
      .postEmail(config)
      .pipe(
        pluck('identifier')
      );
  }

  public patchEmail(uuid: string, config: EmailHookConfig): Observable<any> {

    return forkJoin([
      this._restAlarmsService.patchEmailAddress(uuid, config.address),
      this._restAlarmsService.patchEmailHookLevels(uuid, config.levels),
      this._restAlarmsService.patchEmailHookName(uuid, config.name),
      this._restAlarmsService.patchEmailHookClasses(uuid, config.classes)
    ]);
  }

  public connectWebhook(config: WebHookConfig): Observable<string> {
    return this._restAlarmsService
      .postWebhooks(config)
      .pipe(
        pluck('identifier')
      );
  }

  public getNotification(): Observable<Array<EmailHookConfig>> {
    return this._restAlarmsService.getEmails().pipe(
      map((response: EmailHooks) => response.hooks.hooks)
    );
  }

  public deleteEmail(uuid: string): Observable<any> {
    return this._restAlarmsService.deleteEmail(uuid);
  }

  public getWebhooks(): Observable<Array<WebHookConfig>> {
    return this._restAlarmsService.getWebhooks().pipe(
      map((response: WebHooks) => response.hooks.hooks)
    );
  }

  public getWebhook(uuid: string): Observable<WebHookConfig> {
    return this._restAlarmsService.getWebhook(uuid);
  }

  public patchWebhook(
    uuid: string,
    config: WebHookConfig
  ): Observable<EditWebhookResponse> {
    return forkJoin([
      this._restAlarmsService.patchWebhookName(uuid, config.name),
      this._restAlarmsService.patchWebhookURI(uuid, config.uri),
      this._restAlarmsService.patchWebhookLevels(uuid, config.levels),
      this._restAlarmsService.patchWebhookClasses(uuid, config.classes)
    ]);
  }

  public deleteWebhook(uuid: string): Observable<any> {
    return this._restAlarmsService.deleteWebhook(uuid);
  }

  public getEmail(uuid: string): Observable<EmailHookConfig> {
    return this._restAlarmsService.getEmail(uuid);
  }

  public connectSlack(config: SlackHookConfig): Observable<string> {
    return this._restAlarmsService
      .postSlack(config)
      .pipe(
        pluck('identifier')
      );
  }

  public deleteSlack(uuid: string): Observable<any> {
    return this._restAlarmsService.deleteSlack(uuid);
  }

  public getSlackAccounts(): Observable<Array<SlackHookConfig>> {
    return this._restAlarmsService.getSlacks().pipe(
      map((response: SlackHooks) => response.hooks.hooks)
    );
  }

  public getSlackAccount(uuid: string): Observable<SlackHookConfig> {
    return this._restAlarmsService.getSlack(uuid);
  }

  public patchSlackAccount(
    uuid: string,
    config: SlackHookConfig
  ): Observable<any[]> {
    return forkJoin([
      this._restAlarmsService.patchSlackHookName(uuid, config.name),
      this._restAlarmsService.patchSlackHookURI(uuid, config.uri),
      this._restAlarmsService.patchSlackHookChannel(uuid, config.channel),
      this._restAlarmsService.patchSlackHookUsername(uuid, config.username),
      this._restAlarmsService.patchSlackHookLevels(uuid, config.levels),
      this._restAlarmsService.patchSlackHookClasses(uuid, config.classes)
    ]);
  }

  public convertAlertTypes(alertTypes: boolean[]): AlertLevel[] {
    return alertTypes.reduce(
      (res: AlertLevel[], selected: boolean, idx: number) => {
        if (selected) {
          res.push(this.notificationTypes[idx].name as AlertLevel);
        }
        return res;
      }, []);
  }

  public convertAlertLevels(alertLevels: AlertLevel[]): boolean[] {
    return this.notificationTypes.map((state: NotificationState): boolean => {
      const found = alertLevels.find((l: AlertLevel) => state.name === l);
      return !!found;
    });
  }

  public convertAlertClasses(alertClasses: AlertClass[]): boolean[] {
    return this._alertClassStates.map((state: AlertClassState): boolean => {
      const found = alertClasses.find((l: AlertClass) => state.name === l);
      return !!found;
    });
  }

  public convertAlertClassStates(alertClasses: boolean[]): AlertClass[] {
    return alertClasses.reduce(
      (res: AlertClass[], selected: boolean, idx: number) => {
        if (selected) {
          res.push(this.alertClassStates[idx].name as AlertClass);
        }
        return res;
      }, []);
  }

  public get notificationTypes(): NotificationState[] {
    return this._notificationTypes;
  }

  public get alertClassStates(): AlertClassState[] {
    return this._alertClassStates;
  }
}
