import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { NotificationService } from './../notification.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { environment } from '../../../environments/environment';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { WebhooksComponent } from './webhooks.component';
import { WebHooks } from 'rest_client/pangolin/model/webHooks';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { UserService } from '../../users/user.service';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatCardHarness } from '@angular/material/card/testing';
import { AlertClassPipe } from '../../pipes/alert-class.pipe';
import { AlertLevel } from 'rest_client/pangolin/model/alertLevel';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';

describe('WebhooksComponent', () => {
  let component: WebhooksComponent;
  let fixture: ComponentFixture<WebhooksComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  let notificationService: NotificationService;
  let loader: HarnessLoader;
  let userService: UserService;
  const systemAdminUser = {
    username: 'SystemAdmin',
    email: 'sysadmin@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin']
    }
  };

  const mockWebhooks: WebHooks = {
    hooks: {
      hooks: [
        {
          "name": "configuration name",
          "uri": "http://webhook1.com",
          levels: {
            value: [AlertLevel.INFO]
          },
          classes: {
            value: [AlertClass.AUTH]
          },
          enabled: true,
          identifier: "dc143517-757c-41f4-95a1-4e85e38ad833"
        },
        {
          "name": "configuration name",
          "uri": "http://webhook2.com",
          levels: {
            value: [AlertLevel.WARNING, AlertLevel.ERROR, AlertLevel.CRITICAL]
          },
          classes: {
            value: [AlertClass.PERFORMANCE, AlertClass.SERVICE]
          },
          enabled: true,
          identifier: "dc143517-757c-41f4-95a1-4e85e38ad833"
        },
        {
          "name": "configuration name",
          "uri": "http://webhook3.com",
          "levels": {
            value: []
          },
          enabled: true,
          identifier: "dc143517-757c-41f4-95a1-4e85e38ad833"
        }
      ]
    },
    metadata: {
      index: 0,
      count: 3
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        MatCardModule,
        MatSnackBarModule,
        HttpClientTestingModule
      ],
      declarations: [
        WebhooksComponent,
        AlertClassPipe
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        NotificationService,
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    notificationService = TestBed.inject(NotificationService);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WebhooksComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a header', () => {
    expect(el.querySelector('h2').textContent.trim()).toBe('Webhooks');
  });

  it('should render a list of webhooks', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks`);
    req.flush(mockWebhooks);
    fixture.detectChanges();
    const webhooks = el.querySelectorAll('.webhooks mat-card');
    expect(webhooks.length).toBe(3);
    expect(webhooks[0].querySelector('mat-card-content p:first-child').textContent.trim())
      .toBe('Receiving:  Info');
    expect(webhooks[0].querySelector('mat-card-content p:nth-child(2)').textContent.trim())
      .toBe('Classes:  Auth');
    expect(webhooks[0].querySelector('mat-card-content p:last-child').textContent.trim())
      .toBe('URL: http://webhook1.com');
    expect(webhooks[1].querySelector('mat-card-content p:first-child').textContent.trim())
      .toBe('Receiving:  Warning , Error , Critical');
    expect(webhooks[1].querySelector('mat-card-content p:nth-child(2)').textContent.trim())
      .toBe('Classes:  Performance , Service');
  });

  it('should display a message when there is no webhooks', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks`);
    req.flush({
      hooks: {
        hooks: []
      }
    } as WebHooks);
    fixture.detectChanges();
    const emptyEl = el.querySelectorAll('.empty');
    expect(emptyEl.length).toBe(1);
    expect(emptyEl[0].textContent).toBe('No webhooks');
  });

  it('should delete webhook', async () => {
    userService.setUser(systemAdminUser);
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks`);
    req.flush(mockWebhooks);
    fixture.detectChanges();

    let cards = await loader.getAllHarnesses(MatCardHarness);
    expect(cards.length).toBe(3);

    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Remove'
    }));
    await removeButton.click();
    fixture.detectChanges();
    const deleteReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks/${mockWebhooks.hooks.hooks[0].identifier}`);
    deleteReq.flush({});
    fixture.detectChanges();
    cards = await loader.getAllHarnesses(MatCardHarness);
    expect(cards.length).toBe(2);
  });
});
