import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { NotificationService } from '../notification.service';
import { WebHookConfig } from 'rest_client/pangolin/model/webHookConfig';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from 'src/app/users/user.service';

@Component({
  selector: 'nef-webhooks',
  templateUrl: './webhooks.component.html',
  styleUrls: ['./webhooks.component.less']
})
export class WebhooksComponent implements OnInit, OnDestroy {

  private _notificationSubscription: Subscription;
  private _webhooks: WebHookConfig[];

  constructor(
    private _notificationService: NotificationService,
    private _userService: UserService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this._notificationSubscription = this._notificationService
      .getWebhooks()
      .subscribe((webhooks: WebHookConfig[]) => {
        this._webhooks = webhooks;
      });
  }

  public ngOnDestroy() {
    if (this._notificationSubscription) {
      this._notificationSubscription.unsubscribe();
    }
  }

  public remove({ identifier }: WebHookConfig) {
    this._notificationService.deleteWebhook(identifier).subscribe(
      () => {
        const index = this._webhooks.findIndex((s: WebHookConfig) => s.identifier === identifier);
        this._webhooks.splice(index, 1);
        this._alertService.info(AlertType.INFO_DELETE_WEBHOOK_SUCCESS);
      },
      () => {
        this._alertService.error(AlertType.ERROR_DELETE_WEBHOOK_NOTIFICATION);
      }
    );
  }

  public get webhooks(): WebHookConfig[] {
    return this._webhooks;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
