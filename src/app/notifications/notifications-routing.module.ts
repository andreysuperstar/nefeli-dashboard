import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageDataKey } from '../toolbar/toolbar.component';

import { TenantAdminGuard } from '../auth/tenant-admin.guard';

import { NotificationSettingsComponent } from './notification-settings.component';
import { EmailSettingsComponent } from './email-settings/email-settings.component';
import { EditEmailComponent } from './edit-email/edit-email.component';
import { WebhooksComponent } from './webhooks/webhooks.component';
import { AddWebhookComponent } from './add-webhook/add-webhook.component';
import { SlackComponent } from './slack/slack.component';
import { EditSlackComponent } from './edit-slack/edit-slack.component';
import { SNMPComponent } from './snmp/snmp.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [TenantAdminGuard],
    component: NotificationSettingsComponent,
    data: {
      [PageDataKey.SettingsPage]: true
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'email'
      },
      {
        path: 'email',
        children: [
          {
            path: '',
            component: EmailSettingsComponent
          },
          {
            path: 'add',
            component: EditEmailComponent,
          },
          {
            path: 'edit/:id',
            component: EditEmailComponent
          }
        ]
      },
      {
        path: 'webhooks',
        children: [
          {
            path: '',
            component: WebhooksComponent
          },
          {
            path: 'add',
            component: AddWebhookComponent
          },
          {
            path: 'edit/:id',
            component: AddWebhookComponent
          }
        ]
      },
      {
        path: 'slack',
        children: [
          {
            path: '',
            component: SlackComponent
          },
          {
            path: 'add',
            component: EditSlackComponent
          },
          {
            path: 'edit/:id',
            component: EditSlackComponent
          }
        ]
      },
      {
        path: 'snmp',
        component: SNMPComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationsRoutingModule { }
