import { UserService } from 'src/app/users/user.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { NotificationService } from '../notification.service';
import { SlackHookConfig } from 'rest_client/pangolin/model/slackHookConfig';
import { AlertService, AlertType } from 'src/app/shared/alert.service';

@Component({
  selector: 'nef-slack',
  templateUrl: './slack.component.html',
  styleUrls: ['./slack.component.less']
})
export class SlackComponent implements OnInit, OnDestroy {

  private _notificationSubscription: Subscription;
  private _slackAccounts: SlackHookConfig[];

  constructor(
    private _notificationService: NotificationService,
    private _alertService: AlertService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this._notificationSubscription = this._notificationService
      .getSlackAccounts()
      .subscribe((slackAccounts: SlackHookConfig[]) => {
        this._slackAccounts = slackAccounts;
      });
  }

  public ngOnDestroy() {
    if (this._notificationSubscription) {
      this._notificationSubscription.unsubscribe();
    }
  }

  public get slackAccounts(): SlackHookConfig[] {
    return this._slackAccounts;
  }

  public remove(alert: SlackHookConfig) {
    this._notificationService.deleteSlack(alert.identifier).subscribe(
      () => {
        const foundIndex = this._slackAccounts.findIndex((s: SlackHookConfig) => s.identifier === alert.identifier);

        this._slackAccounts.splice(foundIndex, 1);

        this._alertService.info(AlertType.INFO_DELETE_SLACK_SUCCESS);
      },
      () => {
        this._alertService.error(AlertType.ERROR_DELETE_SLACK_NOTIFICATION);
      }
    );
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
