import { UserService } from 'src/app/users/user.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotificationService } from './../notification.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

import { cloneDeep } from 'lodash-es';

import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { environment } from '../../../environments/environment';
import { SlackComponent } from './slack.component';
import { SlackHooks } from 'rest_client/pangolin/model/slackHooks';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { User } from 'rest_client/heimdallr/model/user';
import { AlertClassPipe } from '../../pipes/alert-class.pipe';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';
import { AlertLevel } from 'rest_client/pangolin/model/alertLevel';

enum UserType {
  SystemAdmin,
  SystemUser
}

const mockUsers: User[] = [
  {
    username: 'SystemAdmin',
    email: 'sysadmin@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin']
    }
  },
  {
    username: 'SystemUser',
    email: 'sysuser@nefeli.io',
    roles: {
      scope: 'system',
      id: '-3',
      roles: ['user']
    }
  }
];

describe('SlackComponent', () => {
  let component: SlackComponent;
  let fixture: ComponentFixture<SlackComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  let notificationService: NotificationService;
  let userService: UserService;

  const mockSlackList: SlackHooks = {
    hooks: {
      hooks: [
        {
          name: "configuration name",
          uri: "https://myslack.org/api1",
          channel: "MyChannel",
          username: "admin",
          levels: {
            value: [AlertLevel.INFO]
          },
          classes: {
            value: [AlertClass.AUTH]
          },
          "enabled": true,
          "identifier": "dc143517-757c-41f4-95a1-4e85e38ad832"
        },
        {
          name: "configuration name",
          uri: "https://myslack.org/api2",
          channel: "MyChannel",
          username: "admin",
          levels: {
            value: [AlertLevel.ERROR, AlertLevel.CRITICAL]
          },
          classes: {
            value: [AlertClass.PERFORMANCE, AlertClass.SERVICE]
          },
          "enabled": true,
          "identifier": "dc143517-757c-41f4-95a1-4e85e38ad832"
        }
      ]
    },
    metadata: {
      count: 2,
      index: 0
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        MatCardModule,
        MatSnackBarModule,
        HttpClientTestingModule
      ],
      declarations: [
        SlackComponent,
        AlertClassPipe
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        NotificationService,
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    notificationService = TestBed.inject(NotificationService);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(SlackComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    const request = httpTestingController.expectOne(
      `${environment.restServer}/v1/alarms/receivers/slack`
    );

    request.flush(cloneDeep(mockSlackList));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a header', () => {
    expect(el.querySelector('h2').textContent.trim()).toBe('Connect slack');
  });

  it('should render a list of connected slack accounts', () => {
    const slackAccounts = el.querySelectorAll('.slack-accounts mat-card');
    expect(slackAccounts.length).toBe(2);
    expect(slackAccounts[1].querySelector('mat-card-content p:first-child').textContent.trim())
      .toBe('Receiving:  Error , Critical');
    expect(slackAccounts[1].querySelector('mat-card-content p:nth-child(2)').textContent.trim())
      .toBe('Classes:  Performance , Service');
    expect(slackAccounts[1].querySelector('mat-card-content p:last-of-type').textContent.trim())
      .toBe('https://myslack.org/api2');
  });

  it('should display a message when there is no slack accounts', () => {
    component.ngOnInit();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/slack`);
    req.flush({
      hooks: {
        hooks: []
      }
    } as SlackHooks);
    fixture.detectChanges();
    const emptyEl = el.querySelector('.empty');
    expect(emptyEl).not.toBeNull();
    expect(emptyEl.textContent.trim()).toBe('No Slack Accounts');
  });

  it('should delete slack config', () => {
    expect(el.querySelectorAll('mat-card').length).toBe(2);

    const removeButton = el.querySelectorAll<HTMLButtonElement>('mat-card-actions button')[0];
    removeButton.dispatchEvent(new Event('click'));

    const removeReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/slack/${mockSlackList.hooks.hooks[0].identifier}`);
    removeReq.flush({});
    fixture.detectChanges();

    expect(el.querySelectorAll('mat-card').length).toBe(1);
    expect(el.querySelector('mat-card-content p:nth-child(3)').textContent).toBe("https://myslack.org/api2");
  });

  it('should display add / edit buttons only for system admin', () => {
    const slackAccounts = el.querySelectorAll('.slack-accounts mat-card');

    let addButton: HTMLButtonElement = el.querySelector('.add');
    let buttons = slackAccounts[0].querySelectorAll('mat-card-actions [mat-button]');
    let editButton: HTMLButtonElement = buttons[0] as HTMLButtonElement;
    let removeButton: HTMLButtonElement = buttons[1] as HTMLButtonElement;

    expect(buttons.length).toBe(2);
    expect(addButton).not.toBeNull();
    expect(editButton.textContent).toBe('Edit');
    expect(removeButton.textContent).toBe('Remove');

    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    addButton = el.querySelector('.add');
    buttons = el.querySelectorAll('.mat-card-actions .mat-button');
    editButton = buttons[0] as HTMLButtonElement;
    removeButton = buttons[1] as HTMLButtonElement;

    expect(buttons.length).toBe(0);
    expect(addButton).toBeNull();
    expect(editButton).toBeUndefined();
    expect(removeButton).toBeUndefined();
  });
});
