import { forkJoin, Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '@angular/forms';
import { UserService } from 'src/app/users/user.service';
import { SNMPCommunity } from 'rest_client/pangolin/model/sNMPCommunity';
import { SNMPTarget } from 'rest_client/pangolin/model/sNMPTarget';
import { AlertService, AlertType } from '../../shared/alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { AlarmsService } from 'rest_client/pangolin/api/alarms.service';

@Component({
  selector: 'nef-snmp',
  templateUrl: './snmp.component.html',
  styleUrls: ['./snmp.component.less']
})
export class SNMPComponent implements OnInit, OnDestroy {
  private _snmpForm: FormGroup;
  private _isSystemAdmin = false;
  private _subscription = new Subscription();

  constructor(
    private _alarmsService: AlarmsService,
    private _userService: UserService,
    private _alertService: AlertService,
    private _fb: FormBuilder
  ) { }

  public ngOnInit(): void {

    this.isSystemAdmin$
      .pipe(take(1))
      .subscribe((isSystemAdmin: boolean) => {
        this._isSystemAdmin = isSystemAdmin;
        this.initSNMPForm();
      });

    const snmpSubscription = forkJoin<string, string>([
      this._alarmsService.getSNMPCommunity(),
      this._alarmsService.getSNMPTarget()
    ])
      .subscribe(([community, target]: [string, string]) => {
        this._snmpForm.patchValue({ community, target });
      });

    this._subscription.add(snmpSubscription);
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private initSNMPForm() {
    this._snmpForm = this._fb.group({
      community: ['', Validators.required],
      target: ['', Validators.required]
    });

    if (!this._isSystemAdmin) {
      this._snmpForm.disable();
    }
  }

  public submitSNMPForm() {
    const { invalid, pristine, value } = this._snmpForm;

    if (invalid || pristine) {
      return;
    }

    const community: SNMPCommunity = {
      community: value.community
    };

    const target: SNMPTarget = {
      target: value.target
    };

    forkJoin<any, any>([
      this._alarmsService.postSNMPCommunity(community),
      this._alarmsService.postSNMPTarget(target)
    ])
      .subscribe(
        (_: [any, any]) => this._alertService.info(AlertType.INFO_EDIT_SMPT_SUCCESS),
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_EDIT_SMPT)
      );
  }

  public get snmpForm(): FormGroup {
    return this._snmpForm;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
