import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from '../../users/user.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpTestingController, HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { User } from 'rest_client/heimdallr/model/user';
import { SNMPComponent } from './snmp.component';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as ALARMS_BASE_PATH } from 'rest_client/pangolin/variables';
import { environment } from '../../../environments/environment';

const mockSystemAdmin: User = {
  username: 'ecarino',
  email: 'eric@nefeli.io',
  roles: {
    scope: 'system',
    id: '-1',
    roles: ['admin', 'user']
  }
};

describe('SNMPComponent', () => {
  let component: SNMPComponent;
  let userService: UserService;
  let fixture: ComponentFixture<SNMPComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCardModule,
        MatSnackBarModule
      ],
      declarations: [SNMPComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: PANGOLIN_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: ALARMS_BASE_PATH,
          useValue: '/v1'
        },
        UserService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    userService = TestBed.inject(UserService);
    userService.setUser(mockSystemAdmin);
    fixture = TestBed.createComponent(SNMPComponent);
    component = fixture.componentInstance;
    httpMockClient = TestBed.inject(HttpTestingController);
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a header', () => {
    expect(el.querySelector('h2').textContent.trim()).toBe('SNMP');
  });

  it('should submit SNMP form', () => {
    const community: string = 'public';
    const target: string = '192.168.1.145';

    component.snmpForm.markAsDirty();

    component.snmpForm.patchValue({ community, target });

    component.submitSNMPForm();

    const communityRequest: TestRequest = httpMockClient.match(
      `${environment.restServer}/v1/alarms/snmp/community`
    )[1];

    const targetRequest: TestRequest = httpMockClient.match(
      `${environment.restServer}/v1/alarms/snmp/target`
    )[1];

    communityRequest.flush({});
    targetRequest.flush({});

    expect(communityRequest.request.method).toBe('POST');
    expect(communityRequest.request.body.community).toBe(community);

    expect(targetRequest.request.method).toBe('POST');
    expect(targetRequest.request.body.target).toBe(target);
  });

  it('should initialize configuration form on load', () => {
    const communityReq: TestRequest = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/snmp/community`);
    communityReq.flush("my-snmp-community");

    const targetReq: TestRequest = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/snmp/target`);
    targetReq.flush("my-snmp-target");

    expect(component['_snmpForm'].value.community).toBe("my-snmp-community");
    expect(component['_snmpForm'].value.target).toBe("my-snmp-target");
  });

  it('should have form controls', () => {
    const communityInputEl: DebugElement = fixture.debugElement
      .query(By.css('mat-card'))
      .query(By.css('.mat-input-element[formcontrolname=community]'));

    const targetInputEl: DebugElement = fixture.debugElement
      .query(By.css('mat-card'))
      .query(By.css('.mat-input-element[formcontrolname=target]'));

    expect(communityInputEl).not.toBeNull('render Community input');
    expect(targetInputEl).not.toBeNull('render Target input');
  });

});
