import { Subscription, Observable } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from '../notification.service';
import { EmailHookConfig } from 'rest_client/pangolin/model/emailHookConfig';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from 'src/app/users/user.service';

@Component({
  selector: 'nef-email-settings',
  templateUrl: './email-settings.component.html',
  styleUrls: ['./email-settings.component.less']
})
export class EmailSettingsComponent implements OnInit, OnDestroy {
  private _notificationSubscription: Subscription;
  private _connectedEmails: Array<EmailHookConfig>;

  constructor(
    private _notificationService: NotificationService,
    private _alertService: AlertService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this._notificationSubscription = this._notificationService
      .getNotification()
      .subscribe((emails: Array<EmailHookConfig>) => {
        this._connectedEmails = emails;
      });
  }

  public ngOnDestroy() {
    if (this._notificationSubscription) {
      this._notificationSubscription.unsubscribe();
    }
  }

  public get connectedEmails(): Array<EmailHookConfig> {
    return this._connectedEmails;
  }

  public remove(email: EmailHookConfig) {
    this._notificationService.deleteEmail(email.identifier).subscribe(
      () => {
        const foundIndex = this._connectedEmails.findIndex((e: EmailHookConfig) => e.identifier === email.identifier);

        this._connectedEmails.splice(foundIndex, 1);

        this._alertService.info(AlertType.INFO_DELETE_EMAIL_SUCCESS);
      },
      () => {
        this._alertService.error(AlertType.ERROR_DELETE_EMAIL_NOTIFICATION);
      }
    );
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
