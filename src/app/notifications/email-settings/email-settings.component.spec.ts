import { UserService } from 'src/app/users/user.service';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NotificationService } from '../notification.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { cloneDeep } from 'lodash-es';

import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { EmailSettingsComponent } from './email-settings.component';
import { environment } from '../../../environments/environment';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { EmailHooks } from 'rest_client/pangolin/model/emailHooks';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RouterTestingModule } from '@angular/router/testing';
import { User } from 'rest_client/heimdallr/model/user';
import { AlertClassPipe } from '../../pipes/alert-class.pipe';
import { AlertLevel } from 'rest_client/pangolin/model/alertLevel';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';

enum UserType {
  SystemAdmin,
  SystemUser
}

const mockUsers: User[] = [
  {
    username: 'SystemAdmin',
    email: 'sysadmin@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin']
    }
  },
  {
    username: 'SystemUser',
    email: 'sysuser@nefeli.io',
    roles: {
      scope: 'system',
      id: '-3',
      roles: ['user']
    }
  }
];

describe('EmailSettingsComponent', () => {
  let component: EmailSettingsComponent;
  let fixture: ComponentFixture<EmailSettingsComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  let notificationService: NotificationService;
  let userService: UserService;

  const mockNotifications: EmailHooks = {
    hooks: {
      hooks: [
        {
          name: "configuration name",
          address: "test@email.com",
          levels: {
            value: [AlertLevel.INFO]
          },
          classes: {
            value: [AlertClass.AUTH]
          },
          enabled: true,
          identifier: "dc143517-757c-41f4-95a1-4e85e38ad833"
        },
        {
          name: "configuration name",
          address: "test2@email.com",
          levels: {
            value: [AlertLevel.WARNING, AlertLevel.ERROR, AlertLevel.CRITICAL]
          },
          classes: {
            value: [AlertClass.PERFORMANCE, AlertClass.SERVICE]
          },
          enabled: true,
          identifier: "dc143517-757c-41f4-95a1-4e85e38ad833"
        }
      ]
    },
    metadata: {
      count: 2,
      index: 0
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        MatCardModule,
        MatSnackBarModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [
        EmailSettingsComponent,
        AlertClassPipe
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        NotificationService,
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    notificationService = TestBed.inject(NotificationService);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(EmailSettingsComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    const request = httpTestingController.expectOne(
      `${environment.restServer}/v1/alarms/receivers/email`
    );

    request.flush(cloneDeep(mockNotifications));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a header', () => {
    expect(el.querySelector('h2').textContent.trim()).toBe('Connect email');
  });

  it('should render a list of connected emails', () => {
    const emails = el.querySelectorAll('.notifications mat-card');
    expect(emails.length).toBe(2);
    expect(emails[1].querySelector('mat-card-title').textContent).toBe('configuration name');
    expect(emails[1].querySelector('mat-card-content p:first-child').textContent.trim())
      .toBe('Receiving:  Warning , Error , Critical');
    expect(emails[1].querySelector('mat-card-content p:nth-child(2)').textContent.trim())
      .toBe('Classes:  Performance , Service');
    expect(emails[1].querySelector('mat-card-content p:last-of-type').textContent.trim())
      .toBe('test2@email.com');
  });

  it('should display a message when there is no emails', () => {
    component.ngOnInit();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/email`);
    req.flush({
      hooks: {
        hooks: []
      }
    } as EmailHooks);
    fixture.detectChanges();
    const emptyEl = el.querySelectorAll('.empty');
    expect(emptyEl.length).toBe(1);
    expect(emptyEl[0].textContent).toBe('No connected emails yet');
  });

  it('should display add / edit buttons only for system admin', () => {
    const notifications = el.querySelectorAll('.notifications mat-card');
    let addButton: HTMLButtonElement = el.querySelector('.add');
    let buttons = notifications[0].querySelectorAll('mat-card-actions [mat-button]');
    let editButton: HTMLButtonElement = buttons[0] as HTMLButtonElement;
    let removeButton: HTMLButtonElement = buttons[1] as HTMLButtonElement;

    expect(buttons.length).toBe(2);
    expect(addButton).not.toBeNull();
    expect(editButton).toBeDefined();
    expect(removeButton).toBeDefined();

    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    addButton = el.querySelector('.add');
    editButton = el.querySelectorAll('mat-card mat-card-actions button')[0] as HTMLButtonElement;
    removeButton = el.querySelectorAll('mat-card mat-card-actions button')[1] as HTMLButtonElement;

    expect(addButton).toBeNull();
    expect(editButton).toBeUndefined();
    expect(removeButton).toBeUndefined();
  });

  it('should delete connected email', () => {
    expect(el.querySelectorAll('mat-card').length).toBe(2);

    const removeButton = el.querySelectorAll<HTMLButtonElement>('mat-card mat-card-actions button')[1];
    removeButton.dispatchEvent(new Event('click'));

    const removeReq = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/receivers/email/${mockNotifications.hooks.hooks[0].identifier}`);
    removeReq.flush({});
    fixture.detectChanges();

    expect(el.querySelectorAll('mat-card').length).toBe(1);
    expect(el.querySelector('mat-card-content p:nth-child(2)').textContent).toBe("Classes:  Performance , Service ");
    expect(el.querySelector('mat-card-content p:nth-child(3)').textContent).toBe("test2@email.com");
  });
});
