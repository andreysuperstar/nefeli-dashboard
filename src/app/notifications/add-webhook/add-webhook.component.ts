import { Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService, NotificationState, EditWebhookResponse, AlertClassState } from '../notification.service';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormArray,
  AbstractControl,
  ValidatorFn,
  ValidationErrors
} from '@angular/forms';
import { WebHookConfig } from 'rest_client/pangolin/model/webHookConfig';

interface WebhookFiles {
  caFilepath?: File;
  certFilepath?: File;
  keyFilepath?: File;
}

@Component({
  selector: 'nef-add-webhook',
  templateUrl: './add-webhook.component.html',
  styleUrls: ['./add-webhook.component.less']
})
export class AddWebhookComponent implements OnInit, OnDestroy {

  private _uuid: string;
  private _backPath = '../';
  private _webhookForm: FormGroup;
  private _webhookSubscription: Subscription;
  private _files: WebhookFiles = {};

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _notificationService: NotificationService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this.initWebhookForm();

    this._uuid = this._route.snapshot.params.id;

    if (this._uuid) {
      this.initEditMode();
    }
  }

  public ngOnDestroy() {
    if (this._webhookSubscription) {
      this._webhookSubscription.unsubscribe();
    }
  }

  private initEditMode() {
    this._backPath = '../../';

    this._webhookSubscription = this._notificationService
      .getWebhook(this._uuid)
      .subscribe((webHook: WebHookConfig) => this.patchForm(webHook));
  }

  private patchForm(webhook: WebHookConfig) {
    const {
      name,
      uri: url,
      levels,
      classes
    } = webhook;

    this._webhookForm.patchValue({
      configName: name,
      url,
      alertTypes: this._notificationService.convertAlertLevels(levels.value),
      alertClasses: this._notificationService.convertAlertClasses(classes.value)
    });
  }

  private initWebhookForm() {
    this._webhookForm = this._fb.group(
      {
        configName: '',
        url: [
          '',
          Validators.required
        ],
        // HTTP Config
        username: '',
        bearerToken: '',
        proxyUrl: '',
        password: '',
        confirmPassword: [
          '',
          this.confirmPasswordValidator()
        ],
        //  TLS Config
        caFilepath: '',
        certFilepath: '',
        keyFilepath: '',
        alertTypes: this.alertTypeCheckboxes(),
        alertClasses: this.alertClassCheckboxes()
      }
    );
  }

  private confirmPasswordValidator(): ValidatorFn {
    return (confirmPassword: AbstractControl): ValidationErrors => {
      if (!this._webhookForm) {
        return;
      }
      const confirm: string = confirmPassword.value;
      const password: string = this._webhookForm.get('password').value;
      return confirm !== password
        ? {
          confirmPassword: {
            value: confirm
          }
        }
        : undefined;
    };
  }

  private alertTypeCheckboxes(): FormArray {
    const arr = this._notificationService.notificationTypes.map(alert => {
      return this._fb.control(alert.selected);
    });
    return this._fb.array(arr);
  }

  private alertClassCheckboxes(): FormArray {
    const arr = this._notificationService.alertClassStates.map(alertClass => {
      return this._fb.control(alertClass.selected);
    });
    return this._fb.array(arr);
  }

  public get alertFormTypes(): FormArray {
    return this._webhookForm.get('alertTypes') as FormArray;
  }

  public get alertClasses(): FormArray {
    return this._webhookForm.get('alertClasses') as FormArray;
  }

  public get notificationTypes(): NotificationState[] {
    return this._notificationService.notificationTypes;
  }

  public get alertClassStates(): AlertClassState[] {
    return this._notificationService.alertClassStates;
  }

  public submitWebhookForm() {
    const { invalid, pristine, value: { configName, url } } = this._webhookForm;
    if (invalid || pristine) {
      return;
    }

    const config: WebHookConfig = {
      name: configName,
      uri: url,
      levels: {
        value: this._notificationService.convertAlertTypes(this._webhookForm.value.alertTypes)
      },
      classes: {
        value: this._notificationService.convertAlertClassStates(this._webhookForm.value.alertClasses)
      }
    };

    if (this._uuid) {
      this.editWebhook(config);
    } else {
      this.addWebhook(config);
    }
  }

  private editWebhook(config: WebHookConfig) {
    this._webhookSubscription = this._notificationService
      .patchWebhook(this._uuid, config)
      .subscribe(
        (_: EditWebhookResponse) => {
          this._alertService.info(AlertType.INFO_EDIT_WEBHOOK_SUCCESS);
          this._router.navigate([this._backPath], {
            relativeTo: this._route
          });
        },
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_EDIT_WEBHOOK)
      );

  }

  private addWebhook(config: WebHookConfig) {
    this._webhookSubscription = this._notificationService
      .connectWebhook(config)
      .subscribe(
        () => {
          this._alertService.info(AlertType.INFO_CONNECT_WEBHOOK_SUCCESS);
          this._router.navigate(['../'], { relativeTo: this._route });
        },
        () => this._alertService.error(AlertType.ERROR_CONNECT_WEBHOOK)
      );
  }

  public get webhookForm(): FormGroup {
    return this._webhookForm;
  }

  public setFilePath(event: Event, controlName: string) {
    const target = event.target as HTMLInputElement;
    if (target?.files) {
      const file: File = target?.files[0];
      this._webhookForm.get(controlName).setValue(file?.name);
      this._files[controlName] = file;
    }
  }

  public get backPath(): string {
    return this._backPath;
  }

  public get uuid(): string {
    return this._uuid;
  }
}
