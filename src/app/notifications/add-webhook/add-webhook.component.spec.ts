import { WebHookConfig } from 'rest_client/pangolin/model/webHookConfig';
import { AlertService, AlertType } from '../../shared/alert.service';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { NgZone } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpTestingController, HttpClientTestingModule, RequestMatch } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { environment } from '../../../environments/environment';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NotificationService } from '../notification.service';
import { AddWebhookComponent } from './add-webhook.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BackButtonComponent } from '../../shared/back-button/back-button.component';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { AlertLevel } from 'rest_client/pangolin/model/alertLevel';
import { AlertClass } from 'rest_client/pangolin/model/alertClass';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { AlertClassPipe } from '../../pipes/alert-class.pipe';

describe('AddWebhookComponent', () => {
  let component: AddWebhookComponent;
  let fixture: ComponentFixture<AddWebhookComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;
  let notificationService: NotificationService;
  let ngZone: NgZone;
  let loader: HarnessLoader;
  const uuid = 'dc143517-757c-41f4-95a1-4e85e38ad832';
  const mockWebhookData: WebHookConfig = {
    name: 'test_webhook',
    uri: 'http://localhost',
    levels: {
      value: [AlertLevel.INFO, AlertLevel.ERROR]
    },
    classes: {
      value: [AlertClass.AUTH]
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCheckboxModule,
        MatSnackBarModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        AddWebhookComponent,
        BackButtonComponent,
        AlertClassPipe
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        NotificationService,
        AlertService
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
    notificationService = TestBed.inject(NotificationService);
    ngZone = TestBed.inject(NgZone);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddWebhookComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    const title = el.querySelector('header h4');
    expect(title.textContent.trim()).toBe('Connect webhook');
  });

  it('should validate url field', () => {
    const url: AbstractControl = component.webhookForm.get('url');
    url.setValue('');
    expect(url.errors.required).toBeTruthy();
    url.setValue('http://localhost/');
    expect(url.errors).toBeUndefined;
  });

  it('should make a POST request to connect email on form submit', () => {
    component.webhookForm.markAsDirty();
    component.webhookForm.patchValue({
      configName: 'Test',
      url: 'http://localhost'
    });
    component.submitWebhookForm();
    expect(component.webhookForm.valid).toBeTruthy();
    const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body instanceof FormData).toBeTruthy;
  });

  it('should submit only valid form', () => {
    spyOn<any>(notificationService, 'connectWebhook').and.callThrough();
    component.webhookForm.markAsDirty();
    component.submitWebhookForm();
    expect(notificationService.connectWebhook).not.toHaveBeenCalled();
  });

  it('should handle successfull add webhook request', () => {
    spyOn(component['_alertService'], 'info').and.callThrough()
    component.webhookForm.markAsDirty();
    component.webhookForm.patchValue({ url: 'http://localhost' });
    ngZone.run(() => {
      component.submitWebhookForm();
      const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks`);
      req.flush({
        id: 1,
        url: 'valid@email'
      });
      expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_CONNECT_WEBHOOK_SUCCESS);
    })
  });

  it('should handle error response', () => {
    spyOn(component['_alertService'], 'error').and.callThrough();
    component.webhookForm.markAsDirty();
    component.webhookForm.patchValue({ url: 'http://localhost' });
    component.submitWebhookForm();
    const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/webhooks`);
    const errorResponse = { status: 400, statusText: 'Bad Request' };
    req.flush({}, errorResponse);
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_CONNECT_WEBHOOK);
  });

  it('should convert alert types', () => {
    const checkboxes = <NodeListOf<HTMLInputElement>> el.querySelectorAll('[formArrayName=alertTypes] [type=checkbox]');
    const infoCheckbox: HTMLInputElement = checkboxes[0];
    const criticalChekbox: HTMLInputElement = checkboxes[3];
    infoCheckbox.click();
    criticalChekbox.click();
    fixture.detectChanges();

    const alertTypes = component['_notificationService'].convertAlertTypes(component.webhookForm.value.alertTypes);
    expect(alertTypes.length).toBe(2);
    expect(alertTypes[0]).toBe('INFO');
    expect(alertTypes[1]).toBe('CRITICAL');
  });

  it('should convert alert classes', () => {
    const checkboxes = <NodeListOf<HTMLInputElement>> el.querySelectorAll('[formArrayName=alertClasses] [type=checkbox]');
    const performanceCheckbox: HTMLInputElement = checkboxes[1];
    const authCheckbox: HTMLInputElement = checkboxes[5];
    performanceCheckbox.click();
    authCheckbox.click();
    fixture.detectChanges();

    const alertClases = component['_notificationService'].convertAlertClassStates(component.webhookForm.value.alertClasses);
    expect(alertClases.length).toBe(2);
    expect(alertClases[0]).toBe(AlertClass.PERFORMANCE);
    expect(alertClases[1]).toBe(AlertClass.AUTH);
  });

  it('should set file path', () => {
    const file: File = new File([], 'test-file.txt', {
      type: 'application/octet-stream',
    });

    const fileList: FileList = {
      length: 1,
      item: (index: number): File | null => file,
      0: file,
    };

    const mockFileEvent = {
      target: {
        files: fileList
      } as unknown
    } as Event;

    component.setFilePath(mockFileEvent, 'certFilepath');
    const certFilepathControl: AbstractControl = component.webhookForm.get('certFilepath');
    expect(certFilepathControl.value).toBe('test-file.txt');
    expect(component['_files'].certFilepath).toBe(file);
  });

  it('should fill in webhook edit form values', async () => {
    component['_uuid'] = uuid;
    component['initEditMode']();
    const req = httpMockClient.expectOne({
      url: `${environment.restServer}/v1/alarms/receivers/webhooks/${uuid}`,
      method: 'GET'
    });
    req.flush(mockWebhookData);

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Configuration name'
    }));
    expect(await nameInput.getValue()).toBe(mockWebhookData.name);

    const urlInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'http://'
    }));
    expect(await urlInput.getValue()).toBe(mockWebhookData.uri);

    const infoCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Info'
    }));
    const warningCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Warning'
    }));
    const errorCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Error'
    }));
    const criticalCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Critical'
    }));

    expect(await infoCheckbox.isChecked()).toBeTruthy();
    expect(await warningCheckbox.isChecked()).toBeFalsy();
    expect(await errorCheckbox.isChecked()).toBeTruthy();
    expect(await criticalCheckbox.isChecked()).toBeFalsy();

    const licenseClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'License'
    }));
    const performanceClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Performance'
    }));
    const serviceClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Service'
    }));
    const userClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'User'
    }));
    const siteClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Site'
    }));
    const authClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Auth'
    }));
    const nfClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'NF'
    }));
    const machineClassCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Machine'
    }));

    expect(await licenseClassCheckbox.isChecked()).toBeFalsy();
    expect(await performanceClassCheckbox.isChecked()).toBeFalsy();
    expect(await serviceClassCheckbox.isChecked()).toBeFalsy();
    expect(await userClassCheckbox.isChecked()).toBeFalsy();
    expect(await siteClassCheckbox.isChecked()).toBeFalsy();
    expect(await authClassCheckbox.isChecked()).toBeTruthy();
    expect(await nfClassCheckbox.isChecked()).toBeFalsy();
    expect(await machineClassCheckbox.isChecked()).toBeFalsy();
  });
});
