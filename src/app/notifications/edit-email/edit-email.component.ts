import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AlertService, AlertType } from '../../shared/alert.service';
import { AlertClassState, NotificationService, NotificationState } from '../notification.service';
import { EmailHookConfig } from 'rest_client/pangolin/model/emailHookConfig';

@Component({
  selector: 'nef-edit-email',
  templateUrl: './edit-email.component.html',
  styleUrls: ['./edit-email.component.less']
})
export class EditEmailComponent implements OnInit, OnDestroy {

  private _uuid: string;
  private _backPath = '../';
  private _emailForm: FormGroup;
  private _emailAddSubscription: Subscription;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _notificationService: NotificationService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this._uuid = this._route.snapshot.params.id;

    this.initEmailForm();

    if (this._uuid) {
      this._backPath = '../../';

      this.getEmailConfiguration(this._uuid);
    }
  }

  public ngOnDestroy() {
    if (this._emailAddSubscription) {
      this._emailAddSubscription.unsubscribe();
    }
  }

  private getEmailConfiguration(uuid: string) {
    this._notificationService.getEmail(uuid).subscribe((hook: EmailHookConfig) => {
      this._emailForm.get('name').setValue(hook.name);
      this._emailForm.get('email').setValue(hook.address);
      this._emailForm.get('alertTypes').setValue(this._notificationService.convertAlertLevels(hook.levels?.value || []));
      this._emailForm.get('alertClasses').setValue(this._notificationService.convertAlertClasses(hook.classes?.value || []));
    });
  }

  private initEmailForm() {
    this._emailForm = this._fb.group(
      {
        name: '',
        email: [
          '',
          [
            Validators.required,
            Validators.email
          ]
        ],
        alertTypes: this.alertTypeCheckboxes(),
        alertClasses: this.alertClassCheckboxes()
      }
    );
  }

  public get uuid(): string {
    return this._uuid;
  }

  private alertTypeCheckboxes(): FormArray {
    const arr = this._notificationService.notificationTypes.map(alert => {
      return this._fb.control(alert.selected);
    });
    return this._fb.array(arr);
  }

  private alertClassCheckboxes(): FormArray {
    const arr = this._notificationService.alertClassStates.map(alertClass => {
      return this._fb.control(alertClass.selected);
    });
    return this._fb.array(arr);
  }

  public get backPath(): string {
    return this._backPath;
  }

  public get alertFormTypes(): FormArray {
    return this._emailForm.get('alertTypes') as FormArray;
  }

  public get alertClassesFormArray(): FormArray {
    return this._emailForm.get('alertClasses') as FormArray;
  }

  public get notificationTypes(): NotificationState[] {
    return this._notificationService.notificationTypes;
  }

  public get alertClassStates(): AlertClassState[] {
    return this._notificationService.alertClassStates;
  }

  public submitEmailForm() {
    const { invalid, pristine, value } = this._emailForm;

    if (invalid || pristine) {
      return;
    }

    const config: EmailHookConfig = {
      name: value.name,
      address: value.email,
      levels: {
        value: this._notificationService.convertAlertTypes(value.alertTypes)
      },
      classes: {
        value: this._notificationService.convertAlertClassStates(value.alertClasses)
      }
    };

    const obs = this._uuid ?
      this._notificationService.patchEmail(this._uuid, config) :
      this._notificationService.connectEmail(config);

    this._emailAddSubscription = obs.subscribe(
      () => {
        if (this._uuid) {
          this._alertService.info(AlertType.INFO_EDIT_EMAIL_SUCCESS);
        } else {
          this._alertService.info(AlertType.INFO_CONNECT_EMAIL_SUCCESS);
        }

        this._router.navigate([this._backPath], {
          relativeTo: this._route
        });
      },
      () => {
        if (this._uuid) {
          this._alertService.error(AlertType.ERROR_EDIT_EMAIL);
        } else {
          this._alertService.error(AlertType.ERROR_CONNECT_EMAIL);
        }
      }
    );
  }

  public get emailForm(): FormGroup {
    return this._emailForm;
  }
}
