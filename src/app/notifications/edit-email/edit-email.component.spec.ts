import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgZone } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { environment } from '../../../environments/environment';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { EditEmailComponent } from './edit-email.component';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { NotificationService } from '../notification.service';
import { BackButtonComponent } from '../../shared/back-button/back-button.component';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { EmailHookConfig } from 'rest_client/pangolin/model/emailHookConfig';
import { AlertClassPipe } from '../../pipes/alert-class.pipe';

describe('EditEmailComponent', () => {
  let component: EditEmailComponent;
  let fixture: ComponentFixture<EditEmailComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;
  let notificationService: NotificationService;
  let ngZone: NgZone;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCheckboxModule,
        MatSnackBarModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      declarations: [
        EditEmailComponent,
        BackButtonComponent,
        AlertClassPipe
      ],
      providers: [
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        NotificationService,
        AlertService
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
    notificationService = TestBed.inject(NotificationService);
    ngZone = TestBed.inject(NgZone);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditEmailComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    const title = el.querySelector('header h4');
    expect(title.textContent).toBe('Connect new email address');
  });

  it('should validate email field', () => {
    const email: AbstractControl = component.emailForm.get('email');
    email.setValue('test');
    expect(email.errors.required).toBeUndefined;
    expect(email.errors.email).toBeTruthy();

    email.setValue('test@test');
    expect(email.errors).toBeUndefined;
  });

  it('should make a POST request to connect email on form submit', () => {
    component.emailForm.markAsDirty();
    component.emailForm.patchValue({
      name: 'test',
      email: 'test@test',
      alertTypes: [true, false, false],
      alertClasses: [false, false, false, false, false, false, false, false]
    });
    component.submitEmailForm();
    expect(component.emailForm.valid).toBeTruthy();
    expect(component.emailForm.pristine).toBeFalsy();
    const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.address).toBe('test@test');
    expect(req.request.body.levels.value.length).toBe(1);
    expect(req.request.body.levels.value[0]).toBe('INFO');
  });

  it('should process alertTypes from the formData', () => {
    spyOn<any>(notificationService, 'connectEmail').and.callThrough();
    component.emailForm.markAsDirty();
    component.emailForm.patchValue({
      name: 'test',
      email: 'test@test',
      alertTypes: [true, false, true]
    });
    component.submitEmailForm();
    expect(notificationService.connectEmail).toHaveBeenCalled();
    expect((notificationService.connectEmail as any).calls.argsFor(0)[0].address).toEqual('test@test');
    expect((notificationService.connectEmail as any).calls.argsFor(0)[0].levels.value.length).toBe(2);
    expect((notificationService.connectEmail as any).calls.argsFor(0)[0].levels.value[0]).toBe('INFO');
    expect((notificationService.connectEmail as any).calls.argsFor(0)[0].levels.value[1]).toBe('ERROR');

    const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email`);
    expect(req.request.body.levels.value.length).toBe(2);
    expect(req.request.body.levels.value[0]).toBe('INFO');
    expect(req.request.body.levels.value[1]).toBe('ERROR');
  });

  it('should render alert type checkboxes', () => {
    const checkboxes = el.querySelectorAll('[formArrayName=alertTypes] .mat-checkbox');
    expect(checkboxes.length).toBe(4);
    expect(checkboxes[0].textContent.trim()).toBe('Info');
    expect(checkboxes[1].textContent.trim()).toBe('Warning');
    expect(checkboxes[2].textContent.trim()).toBe('Error');
    expect(checkboxes[3].textContent.trim()).toBe('Critical');
  });

  it('should render alert classes checkboxes', () => {
    const checkboxes = el.querySelectorAll('[formArrayName=alertClasses] .mat-checkbox');
    expect(checkboxes.length).toBe(notificationService.alertClassStates.length);
    expect(checkboxes[0].textContent.trim()).toBe('License');
    expect(checkboxes[1].textContent.trim()).toBe('Performance');
    expect(checkboxes[2].textContent.trim()).toBe('Service');
    expect(checkboxes[3].textContent.trim()).toBe('User');
    expect(checkboxes[4].textContent.trim()).toBe('Site');
    expect(checkboxes[5].textContent.trim()).toBe('Auth');
    expect(checkboxes[6].textContent.trim()).toBe('NF');
    expect(checkboxes[7].textContent.trim()).toBe('Machine');
  });

  it('should not submit if form is not valid', () => {
    spyOn<any>(notificationService, 'connectEmail').and.callThrough();
    component.emailForm.markAsDirty();
    component.emailForm.patchValue({ email: 'test' });
    component.submitEmailForm();
    expect(notificationService.connectEmail).not.toHaveBeenCalled();
  });

  it('should handle successfull add email request', () => {
    spyOn(component['_alertService'], 'info').and.callThrough();
    spyOn(component['_router'], 'navigate').and.callThrough();
    component.emailForm.markAsDirty();
    component.emailForm.patchValue({ email: 'valid@email' });
    ngZone.run(() => {
      component.submitEmailForm();
      const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email`);
      req.flush({
        id: 1,
        email: 'valid@email',
        name: '',
        alertTypes: [],
        alertClasses: []
      });
      expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_CONNECT_EMAIL_SUCCESS);
      expect(component['_router'].navigate).toHaveBeenCalled();
    });
  });

  it('should handle error response', () => {
    spyOn(component['_alertService'], 'error').and.callThrough();
    component.emailForm.markAsDirty();
    component.emailForm.patchValue({ email: 'valid@email' });
    component.submitEmailForm();
    const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email`);
    const errorResponse = { status: 400, statusText: 'Bad Request' };
    req.flush({}, errorResponse);
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_CONNECT_EMAIL);
  });

  it('should edit email configuration', () => {
    component['_route'].snapshot.params.id = '3';
    component.ngOnInit();

    const hook: EmailHookConfig = {
      "address": "eric@nefeli.io",
      "levels": {
        "value": [
          "ERROR",
          "CRITICAL"
        ]
      },
      "classes": {
        "value": [
          "ALERT_CLASS_AUTH"
        ]
      },
      "name": "E-Mail Notify",
      "enabled": true,
      "identifier": "my-uuid"
    };

    const req = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email/3`);
    req.flush(hook);

    expect(component['_emailForm'].get('name').value).toBe("E-Mail Notify");
    expect(component['_emailForm'].get('email').value).toBe("eric@nefeli.io");
    expect(component['_emailForm'].get('alertTypes').value[0]).toBe(false);
    expect(component['_emailForm'].get('alertTypes').value[1]).toBe(false);
    expect(component['_emailForm'].get('alertTypes').value[2]).toBe(true);
    expect(component['_emailForm'].get('alertTypes').value[3]).toBe(true);

    expect(component['_emailForm'].get('alertClasses').value[0]).toBe(false);
    expect(component['_emailForm'].get('alertClasses').value[1]).toBe(false);
    expect(component['_emailForm'].get('alertClasses').value[2]).toBe(false);
    expect(component['_emailForm'].get('alertClasses').value[3]).toBe(false);
    expect(component['_emailForm'].get('alertClasses').value[4]).toBe(false);
    expect(component['_emailForm'].get('alertClasses').value[5]).toBe(true);
    expect(component['_emailForm'].get('alertClasses').value[6]).toBe(false);
    expect(component['_emailForm'].get('alertClasses').value[7]).toBe(false);

    component['_emailForm'].markAsDirty();
    component.submitEmailForm();

    const reqEmail = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email/3/email`);
    const reqLevels = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email/3/levels`);
    const reqName = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email/3/name`);
    const reqClasses = httpMockClient.expectOne(`${environment.restServer}/v1/alarms/receivers/email/3/classes`);

    expect(reqEmail.request.body).toBe("eric@nefeli.io");
    expect(reqName.request.body).toBe("E-Mail Notify");
    expect(reqLevels.request.body["value"][0]).toBe("ERROR");
    expect(reqLevels.request.body["value"][1]).toBe("CRITICAL");
    expect(reqClasses.request.body["value"][0]).toBe("ALERT_CLASS_AUTH");
  });
});
