export enum HttpSuccess {
  OK = 200
}

export enum HttpError {
  Canceled = 0,
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  Conflict = 409,
  UnprocessableEntity = 422
}

export enum PipelineError {
  DuplicateName = 100
}

export enum HttpErrorCode {
  InUse = 802,
  DuplicateConfiguration = 805
}

export enum NFConfigErrorCode {
  InvalidConfigCredentials = 104
}
