import { Doc, Editor, EditorChange, EditorConfiguration, Pass, Pos, Position } from 'codemirror';
import { Renderer2 } from '@angular/core';

export const TEMPLATE_VARIABLE_PREFIX = '{{.';
export const TEMPLATE_VARIABLE_SUFFIX = '}}';
export const TEMPLATE_VARIABLE_MIN_LENGTH = TEMPLATE_VARIABLE_PREFIX.length + TEMPLATE_VARIABLE_SUFFIX.length + 1;

const TEMPLATE_VARIABLE_NAME_PARTIAL = '[\\w.@$]+';

export const TEMPLATE_VARIABLE_NAME_PATTERN = new RegExp(`^${TEMPLATE_VARIABLE_NAME_PARTIAL}$`);
export const TEMPLATE_VARIABLE_REGEXP = new RegExp(`{{\.(${TEMPLATE_VARIABLE_NAME_PARTIAL})}}`, 'g');

export const CREATE_TEMPLATE_VARIABLE = (name: string) => `{{.${name}}}`;

export type CodeMirrorInst = any;

export type DocChangeEvent = {
  doc: Doc,
  change: EditorChange
};

type Hints = {
  from: Position;
  to: Position;
  list: Array<Hint | string>;
};

/**
 * Interface used by showHint.js Codemirror add-on
 * When completions aren't simple strings, they should be objects with the following properties:
 */
type Hint = {
  text: string;
  className?: string | undefined;
  displayText?: string | undefined;
  from?: Position | undefined;
  /** Called if a completion is picked. If provided *you* are responsible for applying the completion */
  hint?: ((cm: Editor, data: Hints, cur: Hint) => void) | undefined;
  render?: ((element: HTMLLIElement, data: Hints, cur: Hint) => void) | undefined;
  to?: Position | undefined;
};

export type OptionName = keyof EditorConfiguration;
export type OptionValue = EditorConfiguration[OptionName];

// in order to allow for universal rendering, we import Codemirror runtime with `require` to prevent node errors
declare var CodeMirror: CodeMirrorInst;

export function getCodeMirrorGlobal(): any {
  if (CodeMirror !== 'undefined') {
    return CodeMirror;
  }
  CodeMirror = require('codemirror');
  return CodeMirror;
}

export const shouldShowVariablesHint = (cm: Editor): boolean => {
  const cur = cm.getCursor();
  const line = cm.getLine(cur.line);
  const pos = cur.ch;
  return line.charAt(pos - 1) === '{' && !cm.state.completionActive;
};

export function showVariablesHint(cm: CodeMirrorInst, variables: string[]): typeof Pass {
  if (shouldShowVariablesHint(cm)) {
    cm.showHint({
      closeCharacters: /}/,
      hint: () => {
        return new Promise((accept) => {
          setTimeout(() => {
            if (!cm.state.completionActive) {
              return accept(undefined);
            }
            const hints: Hints = getVariableHints(cm as Editor, variables);
            return accept(hints?.list.length ? hints : undefined);
          }, 100);
        });
      }
    });
  }
  // should remain pressed char
  return getCodeMirrorGlobal().Pass;
}

/**
 * create list of variable hints
 *
 * @param cm CodeMirror editor
 * @param variables list of available variables
 */
export function getVariableHints(cm: Editor, variables: string[]): Hints {
  const cur: Position = cm.getCursor();
  const line: string = cm.getLine(cur.line);
  let start: number = cur.ch;
  let end: number = cur.ch;
  while (start && !/\./.test(line.charAt(start - 1))) {
    --start;
  }
  while (end < line.length && !/}/.test(line.charAt(end))) {
    ++end;
  }
  const word: string = line.slice(start, end).toLowerCase();
  const result: Hints = {
    list: [],
    from: Pos(cur.line, start - 2),
    to: Pos(cur.line, end + 1)
  };
  // include variables containing typed word only
  variables.forEach(v => {
    if (!word || v.toLowerCase().indexOf(word) !== -1) {
      result.list.push({
        text: CREATE_TEMPLATE_VARIABLE(v),
        displayText: v
      });
    }
  });
  return result;
}

/**
 * handler of codemirror's doc change event which analyzing the changed text and mark all
 * occurs of variables with <span class="variable">
 *
 * @param e doc change event
 * @param renderer render service
 */
export function markVariables(e: DocChangeEvent, renderer: Renderer2): void {
  e.change.text.forEach((text, indx) => {
    let match = TEMPLATE_VARIABLE_REGEXP.exec(text);

    while (match) { // if changed text contains variable
      const [, variableName] = match;
      const line = e.change.from.line + indx;
      const fromPos = indx === 0 ? e.change.from.ch + match.index : match.index;
      const toPos = indx === 0 ? e.change.from.ch + TEMPLATE_VARIABLE_REGEXP.lastIndex : TEMPLATE_VARIABLE_REGEXP.lastIndex;
      // calculate the start position of "{."
      const from: Position = {
        line,
        ch: fromPos
      };
      // calculate the end position of "}"
      const to: Position = {
        line,
        ch: toPos
      };
      // mark the text with <span class="variable">...</span>
      const span = renderer.createElement('span');
      span.innerText = variableName;
      span.className = 'variable';
      e.doc.markText(from, to, {
        atomic: true,
        replacedWith: span
      });
      match = TEMPLATE_VARIABLE_REGEXP.exec(text);
    }
  });
}
