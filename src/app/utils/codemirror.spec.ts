import { CodemirrorComponent } from '../shared/codemirror/codemirror.component';
import {
  CREATE_TEMPLATE_VARIABLE,
  DocChangeEvent,
  getCodeMirrorGlobal,
  getVariableHints, markVariables,
  shouldShowVariablesHint,
  showVariablesHint
} from './codemirror';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Pos } from 'codemirror';

const variables = ['var1', 'var2', 'var3'];

describe('utils - codemirror', () => {
  let component: CodemirrorComponent;
  let fixture: ComponentFixture<CodemirrorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CodemirrorComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CodemirrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should allow to show variables hint', () => {
    component.codeMirror.setValue('{.');
    component.codeMirror.setCursor({ line: 0, ch: 1 });
    fixture.detectChanges();

    expect(shouldShowVariablesHint(component.codeMirror)).toBe(true);
  });

  it('should show variables hint', async () => {
    const cursor = {
      line: 0,
      ch: 1
    };
    const cm = {
      state: {
        completionActive: false
      },
      getCursor: () => cursor,
      getLine: (line: number) => '{.',
      showHint: jasmine.createSpy('showHint')
    };

    const result = showVariablesHint(cm, variables);
    expect(result).toBe(getCodeMirrorGlobal().Pass);
    expect(cm.showHint).toHaveBeenCalledTimes(1);
    const args = cm.showHint.calls.argsFor(0)[0];
    expect(args.closeCharacters).toEqual(/}/);
    expect(args.hint).not.toBeNull();
    expect(typeof args.hint).toBe("function");
    const hints = await args.hint();
    expect(hints).not.toBeNull();
  });

  it('should create a list of variables hints', () => {
    component.codeMirror.setValue('{.');
    component.codeMirror.setCursor({ line: 0, ch: 2 });
    fixture.detectChanges();

    let variableHints = getVariableHints(component.codeMirror, variables);
    expect(variableHints.list.length).toBe(3);
    expect(variableHints.from).toEqual(Pos(0, 0));
    expect(variableHints.to).toEqual(Pos(0, 3));
    expect(variableHints.list[0]).toEqual({
      text: CREATE_TEMPLATE_VARIABLE(variables[0]),
      displayText: variables[0]
    });
    expect(variableHints.list[1]).toEqual({
      text: CREATE_TEMPLATE_VARIABLE(variables[1]),
      displayText: variables[1]
    });
    expect(variableHints.list[2]).toEqual({
      text: CREATE_TEMPLATE_VARIABLE(variables[2]),
      displayText: variables[2]
    });
  });

  it('should mark all variables', () => {
    const spanEl = {
      innerText: '',
      className: ''
    } as HTMLSpanElement;

    const renderer = jasmine.createSpyObj('Renderer', {
      createElement: spanEl
    });

    const doc = component.codeMirror.getDoc();
    const markText = spyOn(doc, 'markText');

    const e: DocChangeEvent = {
      doc,
      change: {
        from: Pos(0, 0),
        to: Pos(0, 0),
        text: [`insert ${CREATE_TEMPLATE_VARIABLE('var1')} for test`]
      }
    };

    markVariables(e, renderer);
    expect(renderer.createElement).toHaveBeenCalledTimes(1);
    expect(renderer.createElement.calls.argsFor(0)[0]).toEqual('span');
    expect(spanEl.innerText).toEqual('var1');
    expect(spanEl.className).toEqual('variable');

    expect(markText)
      .toHaveBeenCalledWith(
        {
          line: 0,
          ch: 7
        },
        {
          line: 0,
          ch: 16
        },
        {
          atomic: true,
          replacedWith: spanEl
        }
      );
  });

  it('should mark variable with a hyphen', () => {
    const spanEl = {
      innerText: '',
      className: ''
    } as HTMLElement;

    const renderer = jasmine.createSpyObj('Renderer', {
      createElement: spanEl
    });

    const doc = component.codeMirror.getDoc();

    const e: DocChangeEvent = {
      doc,
      change: {
        from: Pos(0, 0),
        to: Pos(0, 0),
        text: [`insert ${CREATE_TEMPLATE_VARIABLE('hello-world')} for test`]
      }
    };

    markVariables(e, renderer);
    expect(renderer.createElement).not.toHaveBeenCalled();
  });
});
