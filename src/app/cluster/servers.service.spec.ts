import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ServersService, Server, Socket, ServerNF, SocketsResponse, ServerState } from './servers.service';
import { environment } from '../../environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';
import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { RESTServerStatus } from 'rest_client/pangolin/model/rESTServerStatus';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { CommonChart } from '../charts/common-chart';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { RESTServerList } from 'rest_client/pangolin/model/rESTServerList';
import { RESTServer } from 'rest_client/pangolin/model/rESTServer';
import { ServerStatsRequest } from 'rest_client/pangolin/model/serverStatsRequest';

describe('ServersService', () => {
  let service: ServersService;
  let httpMockClient: HttpTestingController;

  const siteID = '-1';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ServersService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        NetworkFunctionsService,
        SitesService
      ]
    });

    service = TestBed.inject(ServersService);
    httpMockClient = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMockClient.verify();
  });

  it('should be created', inject([ServersService], (service: ServersService) => {
    expect(service).toBeTruthy();
  }));

  it('should return empty list if no servers', () => {
    const dummyResponse: Server[] = [];

    service.getServers('-1')
      .subscribe((result: Server[]) => expect(result.length).toBe(0));

    const req = httpMockClient.expectOne(`${environment.restServer}/v1/sites/-1/servers`)

    req.flush(dummyResponse);
  });

  it('should return servers property', () => {
    const dummyServers: RESTServerList = {
      servers: [
        { identifier: '-1', name: 'Server1' },
        { identifier: '-2', name: 'Server2' },
        { identifier: '-3', name: 'Server3' }
      ]
    };

    service.getServers('-1').subscribe(result => {
      expect(result.length).toBe(3);
    });

    const req = httpMockClient.expectOne(`${environment.restServer}/v1/sites/-1/servers`);

    req.flush(dummyServers);

    expect(req.request.method).toBe('GET');
  });

  it('should stream servers for different sites', () => {
    const mockServers1: RESTServerList = {
      servers: [
        { identifier: '-1', name: 'Server1' },
        { identifier: '-2', name: 'Server2' },
      ]
    };

    const mockServers2: RESTServerList = {
      servers: [
        { identifier: '-3', name: 'Server3' },
        { identifier: '-4', name: 'Server4' },
      ]
    };

    service.streamServers('-1').subscribe(result => {
      expect(result.length).toBe(2);
      expect(result[0].identifier).toBe('-1');
      expect(result[1].identifier).toBe('-2');
    });

    const req1 = httpMockClient.expectOne(`${environment.restServer}/v1/sites/-1/servers`);
    req1.flush(mockServers1);

    service.streamServers('-2').subscribe(result => {
      expect(result.length).toBe(2);
      expect(result[0].identifier).toBe('-3');
      expect(result[1].identifier).toBe('-4');
    });
    const req2 = httpMockClient.expectOne(`${environment.restServer}/v1/sites/-2/servers`);
    req2.flush(mockServers2);
  });

  it('should return server by id', () => {
    const dummyServer: RESTServer = {
      identifier: 'server-id',
      name: 'server-name',
      nfs: {},
      sockets: [],
      status: RESTServerStatus.GOOD,
      version: '1.0.0'
    };

    const clusterId = '-1';
    const serverId = '-3';
    service.getServer(clusterId, serverId)
      .subscribe((result: RESTServer) => expect(result.identifier).toBe('server-id'));

    const req = httpMockClient.expectOne(`${environment.restServer}/v1/sites/-1/servers/-3`)
    req.flush(dummyServer);
  })

  it('should return sockets by server id', () => {
    const dummySockets: SocketsResponse = {
      sockets: [
        {
          id: 0,
          memory: '201392652288',
          hugepages: [{ size: '1073741824', count: 20 }],
          cores: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
          cpuOversubscriptionFactor: "2"
        },
        {
          id: 1,
          memory: '201392652288',
          hugepages: [{ size: '1073741824', count: 20 }],
          cores: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
          cpuOversubscriptionFactor: "2"
        }
      ]
    };

    const serverId = 'fluffy';

    service.getServerSockets('-1', serverId)
      .subscribe((sockets: Socket[]) => {
        expect(sockets[1].id).toBe(1);
        expect(sockets[0].memory).toEqual('201392652288');
        expect(sockets[0].hugepages[0].size).toEqual('1073741824');
        expect(sockets[1].hugepages[0].count).toEqual(20);
        expect(sockets[1].cores.length).toBe(14);
      });

    const req = httpMockClient.expectOne(`${environment.restServer}/v1/sites/-1/servers/${serverId}/sockets`);

    req.flush(dummySockets);

  });

  it('should return network functions by server id', () => {
    const mockNFCatalog: RESTNFCatalog = {
      descriptors: [
        {
          identifier: '21fb9258-b759-469d-bb9c-5db0f3668ec6',
          name: 'arista_router'
        },
        {
          identifier: '5fa5ff07-37d1-4582-a13e-3dc87c6f07f9',
          name: 'pan_firewall'
        }
      ]
    };

    const dummyNFs: { nfInstances: ServerNF[] } = {
      nfInstances: [{
        metadata: {
          logicalNf: 'arista',
          name: 'arista_0',
          service: '27fc3a26-f398-4e5a-b8be-ccfdd51d5ea2',
          draining: false,
          machine: 'fluffy',
          nfType: mockNFCatalog.descriptors[0].identifier,
          tenant: 'Tenant 1'
        },
        resources: {
          coresBySocket: {
            0: {
              cores: [2, 3, 4, 5, 6, 7]
            }
          },
          hugepagesBySocket: {
            0: {
              hugepages: { 1073741824: 4 }
            }
          },
          cpuOversubscriptionFactor: "3"
        }
      }, {
        metadata: {
          logicalNf: 'filter',
          service: '2afc1634-8714-466b-ba84-7bcb62687460',
          name: 'filter_1',
          draining: false,
          machine: 'fluffy',
          nfType: mockNFCatalog.descriptors[1].identifier,
          tenant: 'Tenant 2'
        },
        resources: {
          coresBySocket: {
            0: {
              cores: [13]
            }
          },
          hugepagesBySocket: {
            0: {
              hugepages: {}
            }
          },
          cpuOversubscriptionFactor: "1"
        }
      }]
    };

    const serverId = 'fluffy';
    service.getServerNFs(siteID, serverId)
      .subscribe((serverNFs: ServerNF[]) => {
        expect(serverNFs.length).toBe(2);
        expect(serverNFs[0].resources.coresBySocket[0].cores.length).toBe(6);
        expect(serverNFs[1].resources.coresBySocket[0].cores[0]).toEqual(13);
        expect(Object.keys(serverNFs[1].resources.hugepagesBySocket[0].hugepages).length).toBe(0);
        expect(serverNFs[1].metadata.machine).toBe(serverId);
      });

    const req = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverId}/nfs`
    );

    req.flush(dummyNFs);

    const serviceRequest = httpMockClient.expectOne(
      `${environment.restServer}/v1/nfs`
    );

    serviceRequest.flush(mockNFCatalog);

    const siteServicesRequest = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/services`
    );

    const mockServices = {
      services: [
        {
          identifier: dummyNFs.nfInstances[0].metadata.service,
          name: 'arista'
        },
        {
          identifier: dummyNFs.nfInstances[1].metadata.service,
          name: 'vsrx'
        }
      ]
    }

    siteServicesRequest.flush(mockServices);
  });

  it('should properly calculate current server state', () => {
    const mockServer: Server = {
      identifier: 'id',
      name: 'name',
      sockets: [{
        id: 0,
        memory: '',
        hugepages: [{
          size: '1000',
          count: 1
        }],
        cores: [0, 1, 2, 3]
      }, {
        id: 1,
        memory: '',
        hugepages: [{
          size: '2000',
          count: 1
        }, {
          size: '3500',
          count: 2
        }],
        cores: [4, 5, 6, 7]
      }],
      nfs: {
        nfInstances: [{
          metadata: {
            logicalNf: 'arista',
            name: 'arista_0',
            service: 'service1',
            draining: false,
            machine: 'fluffy',
            nfType: '',
            tenant: 'Tenant 1'
          },
          resources: {
            coresBySocket: {
              0: {
                cores: [2, 3]
              }
            },
            hugepagesBySocket: {
              0: {
                hugepages: { "100": 4 }
              }
            }
          }
        }, {
          metadata: {
            logicalNf: 'filter',
            service: 'service2',
            name: 'filter_1',
            draining: false,
            machine: 'fluffy',
            tenant: 'Tenant 2',
            nfType: ''
          },
          resources: {
            coresBySocket: {
              0: {
                cores: [6, 7]
              }
            },
            hugepagesBySocket: {
              0: {
                hugepages: { "200": 4 }
              }
            }
          }
        }]
      },
      control: {
        numaResources: {
          0: {
            cores: [0],
            hugepages: { "200": 1 }
          }
        }
      },
      bess: {
        numaResources: {
          0: {
            cores: [0],
            hugepages: { "200": 2 }
          }
        }
      }
    };

    const state: ServerState = service.getServerState(mockServer);
    expect(state.cores.indices).toEqual([0, 1, 2, 3, 4, 5, 6, 7]);
    expect(Array.from(state.cores.active)).toEqual([2, 3, 6, 7, 0]);
    expect(state.cores.active.size).toEqual(5);
    expect(state.memory.total).toBe(10000);
    expect(state.memory.used).toBe(1800);
  });

  it('should poll from server throughput stats', fakeAsync(() => {
    const rate = 86400;
    const step = CommonChart.getOptimalStep(rate);
    const pollInterval = CommonChart.getOptimalPollInterval(rate);

    const subscription = service.streamServerStats(
      'my_site',
      'my_server',
      pollInterval,
      StatFilter.THROUGHPUT,
      rate,
      step
    ).subscribe();

    let req = httpMockClient.expectOne(`${environment.restServer}/v1/sites/my_site/servers/my_server/stats`);
    req.flush({});
    let body: ServerStatsRequest = {
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: rate,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    tick(pollInterval);

    req = httpMockClient.expectOne(`${environment.restServer}/v1/sites/my_site/servers/my_server/stats`);
    req.flush({});
    body = {
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: 86,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    subscription.unsubscribe();
  }));
});
