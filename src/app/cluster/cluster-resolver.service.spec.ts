import { Component } from '@angular/core';
import { TestBed, inject } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router, Routes, RouterStateSnapshot, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { of } from 'rxjs';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { ClusterResolverService } from './cluster-resolver.service';

import { AlertService } from '../shared/alert.service';
import { ClusterService, Cluster } from '../home/cluster.service';

@Component({
  template: ''
})
class DummyComponent { }

const routes: Routes = [
  {
    path: 'sites/-1',
    component: DummyComponent
  },
  {
    path: 'sites/-2',
    component: DummyComponent
  }
];

const spyRouter = {
  // to spy on the url that has been routed
  navigate: jasmine.createSpy('navigate')
};

const mockSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);

const mockRoute: ActivatedRoute = {
  snapshot: undefined,
  url: undefined,
  params: of({
    id: 123
  }),
  queryParams: undefined,
  fragment: undefined,
  data: undefined,
  outlet: undefined,
  component: undefined,
  routeConfig: undefined,
  root: undefined,
  parent: {
    snapshot: undefined,
    url: undefined,
    params: of({
      id: -1
    }),
    queryParams: undefined,
    fragment: undefined,
    data: undefined,
    outlet: undefined,
    component: undefined,
    routeConfig: undefined,
    root: undefined,
    parent: undefined,
    firstChild: undefined,
    children: undefined,
    pathFromRoot: undefined,
    paramMap: of({
      keys: undefined,
      has: undefined,
      get: (_: string): string | null => '-1',
      getAll: undefined
    }),
    queryParamMap: undefined,
    toString: undefined
  },
  firstChild: undefined,
  children: undefined,
  pathFromRoot: undefined,
  paramMap: of({
    keys: undefined,
    has: undefined,
    get: (_: string): string | null => '-1',
    getAll: undefined
  }),
  queryParamMap: undefined,
  toString: undefined
};

describe('ClusterResolverService', () => {
  let clusterService: ClusterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        MatSnackBarModule
      ],
      declarations: [DummyComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: Router,
          useValue: spyRouter
        },
        {
          provide: RouterStateSnapshot,
          useValue: mockSnapshot
        },
        {
          provide: ActivatedRoute,
          useValue: mockRoute,
        },
        ClusterResolverService,
        AlertService,
        ClusterService
      ]
    });
  });

  beforeEach(inject([ClusterService], (service: ClusterService) => {
    clusterService = service;
  }));

  it('should be created', inject([ClusterResolverService], (service: ClusterResolverService) => {
    expect(service).toBeTruthy();
  }));

  it('should resolve to empty array if no clusters', inject([ClusterResolverService], (service: ClusterResolverService, route: ActivatedRoute) => {
    const mockClusters: Cluster[] = [];

    spyOn(clusterService, 'getClusters').and.returnValue(of(mockClusters));

    clusterService.getClusters();

    service
      .resolve(new ActivatedRouteSnapshot(), mockSnapshot)
      .subscribe(response => {
        expect(Array.isArray(response)).toBe(true);
        expect(response.length).toBe(0);
      })
  })
  );

  it('should resolve to clusters', inject([ClusterResolverService], (service: ClusterResolverService, route: ActivatedRoute) => {
    const mockClusters: Cluster[] = [
      {
        id: '-1',
        name: 'Cluster1'
      },
      {
        id: '-2',
        name: 'Cluster2'
      }
    ];

    spyOn(clusterService, 'getClusters').and.returnValue(of(mockClusters));

    clusterService.getClusters();

    const activatedRoute = new ActivatedRouteSnapshot();

    spyOn(activatedRoute.paramMap, 'get').and.returnValue(mockClusters[0].id);

    service
      .resolve(activatedRoute, mockSnapshot)
      .subscribe(response => {
        expect(response[0].id).toBe('-1');
        expect(response.length).toBe(2);
      });
  }));

  it('should resolve to clusters and navigate if no cluster selected', inject([ClusterResolverService], (service: ClusterResolverService, route: ActivatedRoute) => {
    const mockClusters: Cluster[] = [
      {
        id: '-1',
        name: 'Cluster1'
      },
      {
        id: '-2',
        name: 'Cluster2'
      }
    ];

    spyOn(clusterService, 'getClusters').and.returnValue(of(mockClusters));

    clusterService.getClusters();

    const activatedRoute = new ActivatedRouteSnapshot();

    spyOn(activatedRoute.paramMap, 'get').and.returnValue(undefined);

    service
      .resolve(activatedRoute, mockSnapshot)
      .subscribe((clusters) => {
        expect(clusters[0].id).toBe('-1');
        expect(clusters[0].name).toBe('Cluster1');
        expect(clusters[1].id).toBe('-2');
        expect(clusters[1].name).toBe('Cluster2');
      });
  }));
});
