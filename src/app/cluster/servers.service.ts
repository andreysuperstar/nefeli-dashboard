import { Injectable } from '@angular/core';

import { Observable, concat, timer, forkJoin, of, defer } from 'rxjs';
import { pluck, map, shareReplay, skip, repeat, mapTo, mergeMap } from 'rxjs/operators';

import { cloneDeep, orderBy } from 'lodash-es';

import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';
import { ServersService as RESTServersService } from 'rest_client/pangolin/api/servers.service';
import { RESTServer } from 'rest_client/pangolin/model/rESTServer';
import { ServerResourceAllocation } from 'rest_client/pangolin/model/serverResourceAllocation';
import { RESTNFInstance } from 'rest_client/pangolin/model/rESTNFInstance';
import { ServerStats } from 'rest_client/pangolin/model/serverStats';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { StatsService } from 'rest_client/pangolin/api/stats.service';
import { AddMachine } from 'rest_client/pangolin/model/addMachine';
import { RESTServerList } from 'rest_client/pangolin/model/rESTServerList';
import { RESTServerSocket } from 'rest_client/pangolin/model/rESTServerSocket';

const Config = {
  SERVER_POLL_INTERVAL: 10000
};

export interface PostServerResponse {
  status: string;
}

export interface Server extends RESTServer {
  sockets?: Socket[];
}

interface DataPathNic {
  pciAddress: string;
  socket: number;
  mac: string;
  througputGbps: number;
  driver: string;
}

interface ControlPathBondedNic {
  name: string;
  interfaces: Array<string>;
}

export enum BondedNicModes {
  ActiveBackup = 'active-backup',
  XOR = 'xor',
  TXLB = 'txlb',
  LACP = 'lacp'
}

export enum HashFunction {
  L2 = 'l2',
  L23 = 'l23',
  L34 = 'l34'
}

interface DataPathBondedNic {
  socket: number;
  driver: string;
  throughputGbps: number;
  mode: BondedNicModes;
  hash: HashFunction;
  interfaces: [{
    pciAddress: string;
    mac: string;
  }];
}

export interface ServerConfiguration {
  hostname: string;
  ip: string;
  dataPathNics?: Array<DataPathNic>;
  controlPathBondedNics?: Array<ControlPathBondedNic>;
  dataPathBondedNics?: Array<DataPathBondedNic>;
}

export interface SocketsResponse {
  sockets: Socket[];
}

export interface Socket extends RESTServerSocket {
  activeCores?: number[];
  nfs?: ServerNF[];
  cpuOversubscriptionFactor?: string;
  system?: {
    cores: number[];
    memory: number;
    hugepages?: HugePage[];
  };
}

export interface HugePage {
  size: string;
  count: number;
}

// tslint:disable-next-line: no-empty-interface
export interface ServerNF extends RESTNFInstance { }

export interface ServerState {
  cores: {
    indices?: number[];
    active?: Set<number>;
  };
  memory: {
    used: number;
    total: number;
  };
}

export const VALID_SERVER_NAME_FORMAT = /^[a-zA-Z0-9-]{1,63}$/;

@Injectable({
  providedIn: 'root'
})
export class ServersService {

  constructor(
    private _networkFunctionsService: NetworkFunctionsService,
    private _sitesService: SitesService,
    private _restServersService: RESTServersService,
    private _restStatsService: StatsService
  ) { }

  public getServers(clusterId: string): Observable<Server[]> {
    return this._restServersService.getClusterServers(clusterId).pipe(
      map(({ servers }: RESTServerList): Array<Server> => {
        if (!servers) {
          return [];
        }

        // sort servers by id
        servers.sort((serverA: Server, serverB: Server): number => {
          const asc = -1;
          const dsc = 1;
          return (serverA.identifier < serverB.identifier) ? asc : dsc;
        });
        return servers;
      }),
      map((servers: Array<Server>) => {
        // sort sockets in each server by id
        servers.map((server: Server) => {
          if (server.sockets) {
            server.sockets.sort((socketA: Socket, socketB: Socket) => {
              const asc = -1;
              const dsc = 1;
              return (socketA.id < socketB.id) ? asc : dsc;
            });
          }
        });

        return servers;
      }));
  }

  public streamServers(clusterId: string): Observable<Server[]> {
    return concat(
      this.getServers(clusterId),
      timer(Config.SERVER_POLL_INTERVAL).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      shareReplay({
        refCount: true
      })
    );
  }

  public getServer(clusterId: string, serverId: string): Observable<RESTServer> {
    return this._restServersService.getServer(clusterId, serverId);
  }

  public getServerResources(siteId: string, serverId: string): Observable<ServerResourceAllocation> {
    return this._restServersService.getServerResource(siteId, serverId);
  }

  public postServer(siteId: string, machine: AddMachine): Observable<string> {
    return this._restServersService.addServer(siteId, machine);
  }

  public deleteServer(siteId: string, serverid: string): Observable<RESTServer> {
    return this._restServersService.deleteServer(siteId, serverid);
  }

  public getServerNFs(clusterId: string, serverId: string): Observable<ServerNF[]> {
    const serverNFs$ = this._restServersService
      .getServerNfs(clusterId, serverId)
      .pipe(
        pluck('nfInstances')
      );

    const serverNFsWithNFAndServiceNames$ = serverNFs$.pipe(
      mergeMap(serverNFs => forkJoin([
        this._networkFunctionsService.getNfs(),
        this._sitesService.getClusterServices(clusterId)
      ])
        .pipe(
          map(([{
            descriptors: nfs
          }, { services }]) => serverNFs.map(serverNF => {
            const nfID = serverNF.metadata.nfType;

            const service = services.find(
              ({ identifier }) => identifier === serverNF.metadata.service
            );

            const nf = nfs.find(
              ({ identifier }) => identifier === nfID
            );
            serverNF = cloneDeep(serverNF);

            serverNF.metadata.name = nf.name;
            serverNF.metadata.service = service.name;

            return serverNF;
          }))
        )
      )
    );

    const orderedServerNFs$ = serverNFsWithNFAndServiceNames$.pipe(
      map(serverNFs => orderBy(serverNFs, ['metadata.name', 'metadata.service']))
    );

    return orderedServerNFs$;
  }

  public getServerSockets(clusterId: string, serverId: string): Observable<Socket[]> {
    return this._restServersService.getServerSockets(clusterId, serverId)
      .pipe(
        map(({ sockets }: SocketsResponse) => {
          if (!sockets) {
            return [];
          }

          sockets.sort((socketA: Socket, socketB: Socket) => {
            const asc = -1;
            const dsc = 1;
            return (socketA.id < socketB.id) ? asc : dsc;
          });

          return sockets;
        })
      );
  }

  public postServerStats(
    clusterId: string,
    serverId: string,
    filter?: Array<StatFilter>,
    duration?: number,
    step?: number,
    start?: number
  ): Observable<ServerStats> {
    return this._restStatsService.postServerStats(
      clusterId,
      serverId,
      {
        filter: {
          filter: filter,
        },
        range: {
          start: start,
          duration: duration,
          step: step
        }
      }
    );
  }

  public streamServerStats(
    clusterId: string,
    serverId: string,
    delay: number,
    filter?: StatFilter,
    duration?: number,
    step?: number
  ): Observable<ServerStats> {
    const msInSec = 1000;
    let lastFetchTime: number;

    return concat(
      defer(() => {
        if (!duration) {
          return of(duration);
        }

        const now: number = new Date().getTime();
        const newDuration = lastFetchTime ?
          Math.round((now - lastFetchTime) / msInSec) :
          duration;
        lastFetchTime = now;

        return of(newDuration);
      }).pipe(
        mergeMap((_duration: number) =>
          this.postServerStats(clusterId, serverId, [filter] as Array<StatFilter>, _duration, step)
        )
      ),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      shareReplay({
        refCount: true
      })
    );
  }

  private getTotalMemoryFromHugepages(hugepages): number {
    let memory = 0;

    for (const size of Object.keys(hugepages)) {
      const count = hugepages[size];
      memory += (+size * count);
    }

    return memory;
  }

  public getServerState(server: Server): ServerState {
    const state: ServerState = {
      cores: {
        indices: [],
        active: new Set()
      },
      memory: {
        total: 0,
        used: 0
      }
    };

    // nf cores
    if (server.nfs && server.nfs.nfInstances) {
      server.nfs.nfInstances.forEach((nf: ServerNF) => {
        if (nf) {
          if (nf.resources.coresBySocket) {
            const coresBySockets = Object.values(nf.resources.coresBySocket);

            for (const coresBySocket of coresBySockets) {
              coresBySocket.cores.forEach((core: number) => {
                state.cores.active.add(core);
              });
            }
          }

          if (nf.resources.hugepagesBySocket) {
            const hugepagesBySockets = Object.values(nf.resources.hugepagesBySocket);
            for (const hugepagesBySocket of hugepagesBySockets) {
              state.memory.used += this.getTotalMemoryFromHugepages(hugepagesBySocket.hugepages);
            }
          }
        }
      });
    }

    // control plane cores
    if (server.control && server.control.numaResources) {
      for (const resource of Object.values(server.control.numaResources)) {
        resource.cores.forEach((core: number) => {
          state.cores.active.add(core);
        });

        state.memory.used += this.getTotalMemoryFromHugepages(resource.hugepages);
      }
    }

    // data plane cores
    if (server.bess && server.bess.numaResources) {
      for (const resource of Object.values(server.bess.numaResources)) {
        resource.cores.forEach((core: number) => {
          state.cores.active.add(core);
        });

        state.memory.used += this.getTotalMemoryFromHugepages(resource.hugepages);
      }
    }

    if (server.sockets) {
      server.sockets.forEach(socket => {
        state.cores.indices = state.cores.indices.concat(socket.cores);
        state.memory.total += socket.hugepages.reduce((usedMemory: number, hugepage: HugePage): number => {
          return usedMemory + (hugepage.count * +hugepage.size);
        }, 0);
      });
    }

    return state;
  }

}
