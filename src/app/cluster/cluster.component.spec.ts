import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatMenuModule } from '@angular/material/menu';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DecimalPipe, DOCUMENT, formatDate } from '@angular/common';
import { environment } from 'src/environments/environment';

import { TableSiteRow } from './sites.data-source';
import { SiteError } from 'rest_client/pangolin/model/siteError';

import softwareProfiles from 'mock/software_profiles';
import { HttpError } from '../error-codes';

import { TableComponent } from '../shared/table/table.component';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { ClusterComponent } from './cluster.component';
import { UserService } from '../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';
import { ClusterService } from 'src/app/home/cluster.service';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { AddSiteDialogComponent } from './add-site-dialog/add-site-dialog.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatOptionModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { SearchComponent } from '../shared/search/search.component';
import { MatChipsModule } from '@angular/material/chips';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { ZTPPackageDialogComponent } from './ztp-package-dialog/ztp-package-dialog.component';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { SoftwareProfiles } from 'rest_client/pangolin/model/softwareProfiles';
import { MatIconTestingModule } from '@angular/material/icon/testing';

enum UserType {
  SystemAdmin,
  TenantAdmin,
  SystemUser,
  TenantUser
}

const mockUsers: User[] = [
  {
    username: 'SystemAdmin',
    email: 'admin@nefeli.io',
    roles: {
      scope: 'system',
      id: 'Nefeli Networks',
      roles: ['admin', 'user']
    }
  },
  {
    username: 'TenantAdmin',
    email: 'admin@nefeli.io',
    roles: {
      scope: 'tenant',
      id: 'Nefeli Networks',
      roles: ['admin', 'user']
    }
  },
  {
    username: 'SystemUser',
    email: 'user@nefeli.io',
    roles: {
      scope: 'system',
      id: 'Nefeli Networks',
      roles: ['user']
    }
  },
  {
    username: 'TenantUser',
    email: 'user@nefeli.io',
    roles: {
      scope: 'tenant',
      id: 'Nefeli Networks',
      roles: ['user']
    }
  }
];

const mockSite1: Site = {
  config: {
    dataplaneVlanTag: 0,
    name: 'Eric\'s Site',
    identifier: 'eric_site',
    siteStatus: 'ACTIVE',
    siteType: SiteConfigSiteType.CLUSTER,
    publicIp: '127.0.0.1',
    privateIp: '127.0.0.1',
    gatewayMac: '00:00:00:00:00:01',
    inboundVxlanPort: 13333,
    mgmtUrl: '',
    vniRangeEnd: 1,
    vniRangeStart: 0,
    softwareProfileSyncStatus: 'SOFTWARE_PROFILE_SYNC_SCHEDULED',
    softwareProfileId: '1',
    serverVersions: {
      server1: '0.0.1',
      server2: '0.0.2',
      server3: '0.0.3'
    }
  },
  status: {
    lastHeardFromAt: new Date()
  }
};

const mockSite2: Site = {
  config: {
    dataplaneVlanTag: 0,
    name: 'Test Site',
    identifier: 'test_site',
    siteStatus: 'ACTIVE',
    siteType: SiteConfigSiteType.UCPE,
    publicIp: '127.0.0.1',
    privateIp: '127.0.0.1',
    gatewayMac: '00:00:00:00:00:01',
    inboundVxlanPort: 13333,
    mgmtUrl: '',
    vniRangeEnd: 1,
    vniRangeStart: 0,
    softwareProfileSyncStatus: 'SOFTWARE_PROFILE_SYNC_DONE',
    softwareProfileId: '2',
    serverVersions: {
      server1: '0.0.4',
      server2: '0.0.5',
      server3: '0.0.6'
    }
  },
  status: {
    lastHeardFromAt: new Date(2021, 11, 21)
  }
};

const mockSitesResponse: Sites = {
  sites: [
    mockSite1,
    mockSite2
  ],
  metadata: {
    total: 2
  }
};

const mockSelectedSiteRows: TableSiteRow[] = [
  {
    id: mockSite1.config.identifier,
    name: mockSite1.config.name
  },
  {
    id: mockSite2.config.identifier,
    name: mockSite2.config.name
  }
];

const mockSoftwareProfiles: SoftwareProfiles = softwareProfiles;

describe('ClusterComponent', () => {
  let component: ClusterComponent;
  let fixture: ComponentFixture<ClusterComponent>;
  let document: Document;
  let el: HTMLElement;
  let userService: UserService;
  let alertService: AlertService;
  let httpTestingController: HttpTestingController;
  let loader: HarnessLoader;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTabsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatMenuModule,
        MatDialogModule,
        MatSnackBarModule,
        MatTableModule,
        MatCheckboxModule,
        MatTooltipModule,
        NgbDatepickerModule,
        MatPaginatorModule,
        MatDividerModule,
        NoopAnimationsModule,
        MatFormFieldModule,
        MatOptionModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatInputModule,
        MatExpansionModule,
        MatChipsModule,
        MatProgressSpinnerModule,
        MatListModule,
        MatIconModule,
        MatIconTestingModule
      ],
      declarations: [
        ClusterComponent,
        TableComponent,
        ConfirmationDialogComponent,
        AvatarComponent,
        AddSiteDialogComponent,
        SearchComponent,
        ZTPPackageDialogComponent
      ],
      providers: [
        UserService,
        ClusterService,
        RequestUrlInterceptor,
        DecimalPipe,
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        SoftwareProfilesService,
        AlertService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
    alertService = TestBed.inject(AlertService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(ClusterComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    httpTestingController
      .expectOne(`${environment.restServer}/v1/sites?status=true&index=0`)
      .flush(mockSitesResponse);

    const softwareProfilesRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/software_profiles`
    });

    softwareProfilesRequest.flush(mockSoftwareProfiles);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render site menu button', () => {
    const siteMenuButtonDe = fixture.debugElement.query(By.css('header button.three-dots-button'));

    expect(siteMenuButtonDe).toBeDefined('render site menu button');
  });

  it('should render Assign software profile button disabled when cluster sites selected only', () => {
    const siteMenuButtonDe = fixture.debugElement.query(By.css('header button.three-dots-button'));
    const tableDe = fixture.debugElement.query(By.css('nef-table'));

    tableDe.triggerEventHandler('rowsSelect', mockSelectedSiteRows.slice(0, 1));

    siteMenuButtonDe.nativeElement.click();

    const assignSoftwareProfileButtonEl = document.querySelector<HTMLButtonElement>('.mat-menu-panel button.mat-menu-item');

    expect(assignSoftwareProfileButtonEl.disabled).toBeTruthy('render disabled button');
  });

  it('should render site table', () => {
    const table = fixture.debugElement.query(By.css('nef-table'));
    expect(table).toBeTruthy('table is rendered');
  });

  it('should set sites', () => {
    const sites = component.dataSource.sites;
    expect(sites.length).toBe(2);
  });

  it('should get sync', () => {
    let date = new Date();
    let sync = component.dataSource['getSync'](date);

    expect(sync).toBe('< 1m ago', 'valid sync relative time for date less than a minute');

    let minutes = date.getMinutes();

    minutes -= 1;

    date.setMinutes(minutes);

    sync = component.dataSource['getSync'](date);

    expect(sync).toBe('1m ago', 'valid sync relative time for date more than a minute');

    date = new Date();
    minutes = date.getMinutes();

    minutes -= 59;

    date.setMinutes(minutes);

    sync = component.dataSource['getSync'](date);

    expect(sync).toBe('59m ago', 'valid sync relative time for date less than an hour');

    date = new Date();

    let hours = date.getHours();

    hours -= 1;

    date.setHours(hours);

    sync = component.dataSource['getSync'](date);

    expect(sync).toBe('1h 0m ago', 'valid sync relative time for date more than an hour');

    date = new Date();
    hours = date.getHours();
    minutes = date.getMinutes();

    hours -= 23;
    minutes -= 59;

    date.setHours(hours);
    date.setMinutes(minutes);

    sync = component.dataSource['getSync'](date);

    expect(sync).toBe('23h 59m ago', 'valid sync relative time for date less than a day');

    date = new Date();

    hours -= 24;

    date.setHours(hours);

    sync = component.dataSource['getSync'](date);

    const mockDate = formatDate(date, 'MMM d, y, h:mm a', 'en-US');

    expect(sync).toBe(mockDate, 'valid sync date for date more by a day');
  });

  it('should set dataSource', () => {
    component.dataSource.fetch();
    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush(mockSitesResponse);
    fixture.detectChanges();

    const { name, status, type, sync, version, scheduledVersion } = component.dataSource.rows[0];
    expect(name).toBe(mockSitesResponse.sites[0].config.name, 'valid site name');
    expect(status).toBe(mockSitesResponse.sites[0].config.siteStatus, 'valid site status');
    expect(type).toBe(mockSitesResponse.sites[0].config.siteType, 'valid site type');
    expect(sync).toBe('< 1m ago', 'valid site sync');
    expect(version).toEqual(['0.0.1', '0.0.2', '0.0.3'], 'valid site versions');
    expect(scheduledVersion.label).toEqual(mockSoftwareProfiles.softwareProfiles[0].distroVersion)
    expect(scheduledVersion.icon).toEqual('icon-scheduled-status')
    expect(scheduledVersion.tooltip).toContain('Site to be updated after Sat Feb 02 2030')
    expect(scheduledVersion.classes).toEqual(['']);
  });

  it('should show message and add button for System Admin', () => {
    fixture = TestBed.createComponent(ClusterComponent);
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();

    const emptySitesReponse: Sites = {
      sites: []
    }
    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush(emptySitesReponse);
    fixture.detectChanges();

    const noSitesEl = fixture.debugElement.query(By.css('.add-site')).nativeElement;
    const header = noSitesEl.querySelector('h1');
    const addButton = noSitesEl.querySelector('button');

    expect(noSitesEl).not.toBeNull();
    expect(header.textContent).toBe('No Sites');
    expect(addButton).not.toBeNull;
    expect(addButton.textContent).toBe('Add new Site');
  });

  it('should hide add button for non System Admins', () => {
    fixture = TestBed.createComponent(ClusterComponent);
    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();

    const emptySitesReponse: Sites = {
      sites: []
    }
    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush(emptySitesReponse);
    fixture.detectChanges();

    let noSitesEl = fixture.debugElement.query(By.css('.add-site')).nativeElement;
    let header = noSitesEl.querySelector('h1');
    let addButton = noSitesEl.querySelector('button');

    expect(noSitesEl).not.toBeNull();
    expect(header.textContent).toBe('No Sites');
    expect(addButton).toBeNull;

    userService.setUser(mockUsers[UserType.TenantAdmin]);
    fixture.detectChanges();
    addButton = noSitesEl.querySelector('button');
    expect(addButton).toBeNull;

    userService.setUser(mockUsers[UserType.TenantUser]);
    fixture.detectChanges();
    addButton = noSitesEl.querySelector('button');
    expect(addButton).toBeNull;
    fixture.detectChanges();
  });

  it('should display add site dialog', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();
    let siteDialog: HTMLElement = document.querySelector('nef-add-site-dialog');
    expect(siteDialog).toBeNull();
    const addSiteButtonEl = el.querySelector<HTMLButtonElement>('header button.add');
    addSiteButtonEl.click();
    fixture.detectChanges();
    siteDialog = document.querySelector('nef-add-site-dialog');
    expect(siteDialog).not.toBeNull();
  });

  it('should set selected sites', async () => {
    const tableDe = fixture.debugElement.query(By.css('nef-table'));

    spyOn(component, 'onRowsSelect').and.callThrough();

    tableDe.triggerEventHandler('rowsSelect', mockSelectedSiteRows);

    expect(component.onRowsSelect).toHaveBeenCalledWith(mockSelectedSiteRows);

    const selectedSites = mockSelectedSiteRows.map(({ id }) => id);

    expect(component['_selectedSites']).toEqual(selectedSites, 'set selected sites');
  });

  it('should assign software profiles for selected sites', () => {
    const tableDe = fixture.debugElement.query(By.css('nef-table'));

    tableDe.triggerEventHandler('rowsSelect', mockSelectedSiteRows);

    const mockSoftwareProfileID = mockSoftwareProfiles.softwareProfiles[0].identifier;

    // assign software profile site request error
    component.onSoftwareProfileAssign(mockSoftwareProfileID);

    spyOn(alertService, 'error');

    const mockSiteError: SiteError = {
      httpStatus: HttpError.NotFound,
      message: 'Not found.',
      errorCode: 1200
    };

    mockSelectedSiteRows
      .filter(({ id }) => {
        const site = mockSitesResponse.sites.find(site => site.config.identifier === id);

        return site.config.siteType === SiteConfigSiteType.UCPE;
      })
      .forEach(({ id }) => {
        const siteRequest = httpTestingController.expectOne({
          method: 'PUT',
          url: `${environment.restServer}/v1/sites/${id}`
        });

        if (!siteRequest.cancelled) {
          const responseOptions: {
            headers?: HttpHeaders | {
              [name: string]: string | string[];
            };
            status?: number;
            statusText?: string;
          } = {
            status: HttpError.NotFound,
            statusText: 'Not Found'
          };

          siteRequest.flush(mockSiteError, responseOptions);
        }
      });

    expect(alertService.error).toHaveBeenCalledWith(AlertType.ERROR_ASSIGN_SITES_SOFTWARE_PROFILE, mockSiteError.message);

    // assign software profile to sites
    component.onSoftwareProfileAssign(mockSoftwareProfileID);

    spyOn(alertService, 'info');

    const selectedClusterSites: string[] = [];

    mockSelectedSiteRows
      .filter(({ id }) => {
        const {
          config: { name, siteType }
        } = mockSitesResponse.sites.find(site => site.config.identifier === id);

        if (siteType === SiteConfigSiteType.CLUSTER) {
          selectedClusterSites.push(name);
        }

        return siteType === SiteConfigSiteType.UCPE;
      })
      .forEach(({ id, name }) => {
        const siteRequest = httpTestingController.expectOne({
          method: 'PUT',
          url: `${environment.restServer}/v1/sites/${id}`
        });

        expect(siteRequest.request.body.softwareProfileId).toBe(mockSoftwareProfileID, `assign software profile to ${name} site`);

        siteRequest.flush({});
      });

    expect(alertService.info).toHaveBeenCalledWith(`${AlertType.INFO_ASSIGN_SITES_SOFTWARE_PROFILE_SUCCESS} ${selectedClusterSites.join(', ')}.`);

    // unassign software profiles from sites
    tableDe.triggerEventHandler('rowsSelect', mockSelectedSiteRows);

    component.onSoftwareProfileAssign();

    mockSelectedSiteRows
      .filter(({ id }) => {
        const site = mockSitesResponse.sites.find(site => site.config.identifier === id);

        return site.config.siteType === SiteConfigSiteType.UCPE;
      })
      .forEach(({ id, name }) => {
        const siteRequest = httpTestingController.expectOne({
          method: 'PUT',
          url: `${environment.restServer}/v1/sites/${id}`
        });

        expect(siteRequest.request.body.softwareProfileId).toBeUndefined(`unassign software profile from ${name} site`);

        siteRequest.flush({});
      });

    expect(alertService.info).toHaveBeenCalledWith(`${AlertType.INFO_ASSIGN_SITES_SOFTWARE_PROFILE_SUCCESS} ${selectedClusterSites.join(', ')}.`);
  });

  it('should remove selected sites', () => {
    const tableDe = fixture.debugElement.query(By.css('nef-table'));
    let rowDes = tableDe.queryAll(By.css('mat-row'));

    expect(rowDes.length).toBe(mockSitesResponse.sites.length, `render ${mockSitesResponse.sites.length} sites rows`);

    tableDe.triggerEventHandler('rowsSelect', mockSelectedSiteRows);

    // remove sites request error
    component.onRemoveSites();

    spyOn(alertService, 'error');

    const mockSiteError: SiteError = {
      httpStatus: HttpError.NotFound,
      message: 'Not found.',
      errorCode: 1200
    };

    mockSelectedSiteRows.forEach(({ id }) => {
      const siteRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/sites/${id}`
      });

      if (!siteRequest.cancelled) {
        const responseOptions: {
          headers?: HttpHeaders | {
            [name: string]: string | string[];
          };
          status?: number;
          statusText?: string;
        } = {
          status: HttpError.NotFound,
          statusText: 'Not Found'
        };

        siteRequest.flush(mockSiteError, responseOptions);
      }
    });

    expect(alertService.error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_SITES, mockSiteError.message);

    // remove sites
    component.onRemoveSites();

    spyOn(alertService, 'info');

    mockSelectedSiteRows.forEach(({ id }) => {
      const siteRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/sites/${id}`
      });

      siteRequest.flush({});
    });

    const siteRequests = httpTestingController.match({
      method: 'GET',
      url: `${environment.restServer}/v1/sites?status=true&index=0`
    });

    const mockSites = mockSitesResponse.sites.filter(site => !mockSelectedSiteRows.some(row => row.id === site.config.identifier));

    const mockRemovedSitesResponse = {
      sites: mockSites
    };

    siteRequests[1].flush(mockRemovedSitesResponse);
    fixture.detectChanges();

    expect(alertService.info).toHaveBeenCalledWith(AlertType.INFO_REMOVE_SITES_SUCCESS);

    rowDes = fixture.debugElement.queryAll(By.css('mat-row'));

    expect(rowDes.length).toBe(mockRemovedSitesResponse.sites.length, `render ${mockRemovedSitesResponse.sites.length} site rows without removed site`);
  });

  it('should go to site when click on row', () => {
    spyOn(component['_router'], 'navigate');

    const row = el.querySelectorAll('mat-row')[0];
    row.dispatchEvent(new Event('click'));

    expect(component['_router'].navigate).toHaveBeenCalledWith(["sites", "eric_site"]);
  });

  it('should render Package buttons', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);

    fixture = TestBed.createComponent(ClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const siteRequests = httpTestingController.match({
      method: 'GET',
      url: `${environment.restServer}/v1/sites?status=true&index=0`
    });

    siteRequests.forEach(request => {
      request.flush(Object.freeze(mockSitesResponse));
    });

    fixture.detectChanges();

    const rowDes = fixture.debugElement
      .query(By.css('nef-table'))
      .queryAll(By.css('mat-row'));

    rowDes.forEach(rowDe => {
      const packageButtonDe = rowDe.query(By.css('button.package'));

      expect(packageButtonDe.nativeElement.textContent).toBe('Package', 'render Package button');
    });
  });

  it('should generate a package', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);

    fixture = TestBed.createComponent(ClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush(mockSitesResponse);
    fixture.detectChanges();

    const rowDes: DebugElement[] = fixture.debugElement
      .query(By.css('nef-table'))
      .queryAll(By.css('mat-row'));

    const packageButtonEl: HTMLButtonElement = rowDes[1].query(
      By.css('button.package')
    ).nativeElement;

    expect(packageButtonEl).not.toBeNull('render Package button');
    expect(packageButtonEl.textContent).toBe('Package', 'render Package button text');

    spyOn<any>(component, 'generateZTPPackage').and.callThrough();

    packageButtonEl.click();
    expect(component['generateZTPPackage']).toHaveBeenCalled();

    const dialog: MatDialogRef<ZTPPackageDialogComponent> = component['_dialog'].openDialogs[0];

    expect(dialog).toBeDefined('render ZTP package dialog');
    expect(dialog['_containerInstance']['_config'].disableClose).toBeTruthy();
  });

  it('should remove site', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(ClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush(mockSitesResponse);
    fixture.detectChanges();

    // click remove site action
    const elem = fixture.debugElement.nativeElement;
    const removeButton: HTMLButtonElement = elem.querySelector('mat-row button.remove');
    removeButton.click();

    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    expect(dialog).toBeDefined();

    // confirm action in the confirmation dialog
    dialog.afterOpened().subscribe(() => {
      const confirmDialog: ConfirmationDialogComponent = dialog.componentInstance;
      expect(confirmDialog.action).toBe('Remove Site');
      const confirmDialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
      const descriptionEls: NodeListOf<HTMLParagraphElement> = confirmDialogEl.querySelectorAll('p');
      expect(descriptionEls[0].textContent).toBe(`This will remove the site Eric's Site.`);

      const confirmButton: HTMLButtonElement = confirmDialogEl.querySelectorAll('button')[1];
      confirmButton.click();
      fixture.detectChanges();
    });

    // check that delete method and get sites has called
    dialog.afterClosed().subscribe(() => {
      spyOn(component['_alertService'], 'info');
      const request = httpTestingController.expectOne(
        `${environment.restServer}/v1/sites/eric_site`
      );
      expect(request.request.method).toBe('DELETE');
      request.flush({});
      fixture.detectChanges();

      const requests = httpTestingController.expectOne({
        url: `${environment.restServer}/v1/sites?status=true&index=0`,
        method: 'GET'
      });
      expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_REMOVE_SITE_SUCCESS);
    });
  });

  it('should show error alert if remove site failed', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(ClusterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush(mockSitesResponse);
    fixture.detectChanges();
    const removeButton: HTMLButtonElement = fixture.debugElement.nativeElement.querySelector('mat-row button.remove');
    removeButton.click();
    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    dialog.afterOpened().subscribe(() => {
      const confirmDialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
      const confirmButton: HTMLButtonElement = confirmDialogEl.querySelectorAll('button')[1];
      confirmButton.click();
      fixture.detectChanges();
    });
    dialog.afterClosed().subscribe(() => {
      spyOn(component['_alertService'], 'error');
      const request = httpTestingController.expectOne(`${environment.restServer}/v1/sites/eric_site`);
      expect(request.request.method).toBe('DELETE');
      request.flush({}, {
        status: 400,
        statusText: 'Bad Request'
      });
      fixture.detectChanges();
      expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_SITE, undefined);
    });
  });

  it('should perform search and update rows', () => {
    const el = fixture.debugElement.nativeElement;

    component.onSearchModified(['foo', 'bar']);
    httpTestingController.expectOne(`${environment.restServer}/v1/sites?status=true&search=foo&search=bar&index=0`).flush({
      sites: [mockSite1, mockSite2]
    });
    fixture.detectChanges();
    expect(el.querySelectorAll('mat-row').length).toBe(2);

    component.onSearchModified(['foo']);
    httpTestingController.expectOne(`${environment.restServer}/v1/sites?status=true&search=foo&index=0`).flush({
      sites: [mockSite1]
    });
    fixture.detectChanges();
    expect(el.querySelectorAll('mat-row').length).toBe(1);

    component.onSearchModified([]);

    const requests = httpTestingController.match(`${environment.restServer}/v1/sites?status=true&index=0`);
    requests[1].flush({
      sites: []
    });
    fixture.detectChanges();
    expect(el.querySelectorAll('mat-row').length).toBe(0);
  });

  it('should get site from the datasource', () => {
    const site = component.dataSource.getSiteById('test_site');
    expect(site).not.toBeNull();
    expect(site.config.name).toBe('Test Site');
  });

  it('should get site length from the datasource', () => {
    expect(component.dataSource.length).toBe(2);
    expect(component.dataSource.total).toBe(2);
  });

  it('should set total for datasource', () => {
    expect(component.dataSource.total).toBe(2);
    component.dataSource.total = 1
    expect(component.dataSource.total).toBe(1);
  });

  it('should display filtering button and panel', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture.detectChanges();

    const filterButtonEl: HTMLButtonElement = fixture.debugElement.query(By.css('.add-hide-button')).nativeElement;
    expect(filterButtonEl).not.toBeNull();
    filterButtonEl.click();
    fixture.detectChanges();

    expect(fixture.debugElement.query(By.css('[formControlName=siteType]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[formControlName=serverVersions]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[formControlName=scheduledVersion]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[formControlName=siteStatus]'))).not.toBeNull();
    expect(fixture.debugElement.query(By.css('[formControlName=softwareProfileSyncStatus]'))).not.toBeNull();
  });

  it('should filter by scheduled versions', () => {
    const buttonEl: HTMLButtonElement = fixture.debugElement.query(By.css('.add-hide-button')).nativeElement;
    buttonEl.click();
    fixture.detectChanges();
    const scheduledVersionsSelect: HTMLInputElement = el.querySelector(
      'mat-select[formcontrolname=scheduledVersion]'
    );

    scheduledVersionsSelect
      .querySelector('.mat-select-trigger')
      .dispatchEvent(new Event('click'));
    fixture.detectChanges();

    const optionEls: NodeListOf<HTMLUnknownElement> = document.querySelectorAll('mat-option');
    expect(optionEls.length).toBe(4);
    expect(optionEls[0].textContent).toBe('-- None --');
    expect(optionEls[1].textContent).toBe(mockSoftwareProfiles.softwareProfiles[0].distroVersion);
    expect(optionEls[2].textContent).toBe(mockSoftwareProfiles.softwareProfiles[1].distroVersion);
    expect(optionEls[3].textContent).toBe(mockSoftwareProfiles.softwareProfiles[2].distroVersion);

    optionEls[1].dispatchEvent(new Event('click'));
    fixture.detectChanges();
    component.onSearchModified([]);

    httpTestingController
      .expectOne(`${environment.restServer}/v1/sites?status=true&filter=eq(scheduledVersion,${mockSoftwareProfiles.softwareProfiles[0].distroVersion})&index=0`)
      .flush({
        sites: []
      });
    fixture.detectChanges();

    expect(el.querySelectorAll('mat-row').length).toBe(0);
  });

  it('should filter by server versions', () => {
    const buttonEl: HTMLButtonElement = fixture.debugElement.query(By.css('.add-hide-button')).nativeElement;
    buttonEl.click();
    fixture.detectChanges();

    const serverVersionsSelect: HTMLInputElement = el.querySelector(
      'mat-select[formcontrolname=serverVersions]'
    );
    serverVersionsSelect
      .querySelector('.mat-select-trigger')
      .dispatchEvent(new Event('click'));
    fixture.detectChanges();

    const optionEls: NodeListOf<HTMLUnknownElement> = document.querySelectorAll('mat-option');
    expect(optionEls.length).toBe(7);
    expect(optionEls[0].textContent).toBe('-- None --');
    expect(optionEls[1].textContent).toBe('0.0.1');
    expect(optionEls[2].textContent).toBe('0.0.2');
    expect(optionEls[6].textContent).toBe('0.0.6');

    optionEls[1].dispatchEvent(new Event('click'));
    fixture.detectChanges();
    component.onSearchModified([]);

    httpTestingController
      .expectOne(`${environment.restServer}/v1/sites?status=true&filter=eq(serverVersions,0.0.1)&index=0`)
  });

  it('should remove row from datasource', () => {
    const siteId: string = 'test_site';
    component.dataSource.remove(siteId);
    expect(component.dataSource.total).toBe(1);
    expect(component.dataSource.rows.length).toBe(1);
    expect(component.dataSource.rows[0].id).not.toBe('test_site');
  });
});
