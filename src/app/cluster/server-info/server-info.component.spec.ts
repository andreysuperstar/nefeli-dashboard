import { cloneDeep } from 'lodash-es';
import { RouterTestingModule } from '@angular/router/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { TestBed, ComponentFixture, fakeAsync, tick, flush } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { of } from 'rxjs';

import { environment } from '../../../environments/environment';

import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';

import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';

import { ServersService, Socket, ServerNF } from '../servers.service';

import { ServerInfoComponent, ViewType } from './server-info.component';
import { SocketComponent } from '../socket/socket.component';
import { MemoryMeterComponent } from '../memory-meter/memory-meter.component';
import { ToggleComponent } from '../../shared/toggle/toggle.component';
import { LegendComponent } from '../../shared/legend/legend.component';
import { TableComponent } from '../../shared/table/table.component';
import { CellGridComponent } from '../../shared/cell-grid/cell-grid.component';
import { AvatarComponent } from '../../shared/avatar/avatar.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { AlertService } from '../../shared/alert.service';
import { DateRangeComponent } from '../../shared/date-range/date-range.component';
import { ChartThroughputComponent } from '../../charts/chart-throughput.component';
import { ChartPacketlossComponent } from '../../charts/chart-packetloss.component';
import { ChartLatencyComponent } from '../../charts/chart-latency.component';
import { DurationPipe } from '../../pipes/duration.pipe';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartHeaderComponent } from '../../shared/chart-header/chart-header.component';
import { ChartLineComponent } from '../../shared/chart-line/chart-line.component';
import { PipelineNamePipe } from '../../pipes/pipeline-name.pipe';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { ByteConverterPipe } from '../../pipes/byte-converter.pipe';
import { DecimalPipe } from '@angular/common';
import { ServerResourceAllocation } from 'rest_client/pangolin/model/serverResourceAllocation';
import { UserService } from '../../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { MatTooltipModule } from '@angular/material/tooltip';

describe('ServerInfoComponent', () => {
  let httpMockClient: HttpTestingController;
  let component: ServerInfoComponent;
  let fixture: ComponentFixture<ServerInfoComponent>;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  let serverInfoDe: DebugElement;
  let serverInfoEl: HTMLElement;
  let userService: UserService;

  const siteID = '-1';
  const serverID = 'fluffy';

  const mockSystemAdmin: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  const mockSystemUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['user']
    }
  }

  const mockNFCatalog: RESTNFCatalog = {
    descriptors: [
      {
        identifier: '21fb9258-b759-469d-bb9c-5db0f3668ec6',
        name: 'arista_router'
      },
      {
        identifier: '5fa5ff07-37d1-4582-a13e-3dc87c6f07f9',
        name: 'pan_firewall'
      },
      {
        identifier: '3dc87c6f07f9-37d1-4582-a13e-5fa5ff07',
        name: 'load_balancer_f5'
      }
    ]
  };

  const mockServices = {
    services: [
      {
        identifier: '2afc1634-8714-466b-ba84-7bcb62687460',
        name: 'arista'
      },
      {
        identifier: '27fc3a26-f398-4e5a-b8be-ccfdd51d5ea2',
        name: 'panpan'
      },
      {
        identifier: '5fa5ff07-37d1-4582-a13e-3dc87c6f07f9',
        name: 'vsrx'
      }
    ]
  };

  const mockServerNFs: ServerNF[] = [
    {
      metadata: {
        logicalNf: 'arista',
        name: 'arista_0',
        tenant: 'Tenant 1',
        service: mockServices.services[0].identifier,
        draining: false,
        machine: 'fluffy',
        nfType: mockNFCatalog.descriptors[0].identifier
      },
      resources: {
        coresBySocket: {
          0: {
            cores: [2, 3, 4]
          }
        },
        hugepagesBySocket: {
          0: {
            hugepages: {
              1073741824: 4
            }
          }
        },
        cpuOversubscriptionFactor: '3'
      }
    },
    {
      metadata: {
        logicalNf: 'arista1',
        name: 'arista_0',
        tenant: 'Tenant 1',
        service: mockServices.services[1].identifier,
        draining: false,
        machine: 'fluffy',
        nfType: mockNFCatalog.descriptors[1].identifier
      },
      resources: {
        coresBySocket: {
          0: {
            cores: [5, 6]
          }
        },
        hugepagesBySocket: {
          0: {
            hugepages: {
              1073741824: 4
            }
          }
        },
        cpuOversubscriptionFactor: '3'
      }
    },
    {
      metadata: {
        logicalNf: 'arista2',
        name: 'arista_0',
        tenant: 'Tenant 1',
        service: mockServices.services[2].identifier,
        draining: false,
        machine: 'fluffy',
        nfType: mockNFCatalog.descriptors[2].identifier
      },
      resources: {
        coresBySocket: {
          0: {
            cores: [7, 8]
          }
        },
        hugepagesBySocket: {
          0: {
            hugepages: {
              1073741824: 4
            }
          }
        },
        cpuOversubscriptionFactor: '3'
      }
    }
  ];

  const mockSockets: Socket[] = [
    {
      id: 0,
      memory: '201392652288',
      hugepages: [
        { size: '1073741824', count: 20 }
      ],
      cores: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
      cpuOversubscriptionFactor: '5'
    },
    {
      id: 1,
      memory: '201392652288',
      hugepages: [
        { size: '1073741824', count: 20 }
      ],
      cores: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
      cpuOversubscriptionFactor: '2'
    }
  ];

  const fetchServerNFs = () => {
    const request = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverID}/nfs`
    );

    request.flush({
      nfInstances: mockServerNFs
    });
  }

  const fetchSockets = () => {
    const request = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverID}/sockets`
    );

    request.flush({
      sockets: mockSockets
    });
  }

  const fetchServerResources = () => {
    const request = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverID}/resources`
    );

    request.flush({
      control: {
        numaResources: {
          key1: {
            cores: [
              0
            ],
            hugepages: {
              '1073741824': 4
            }
          }
        }
      },
      bess: {
        numaResources: {
          bess_key1: {
            cores: [
              1
            ],
            hugepages: {
              '1073741824': 4
            }
          }
        }
      }
    } as ServerResourceAllocation);
  }

  function fetchNFsAndSiteServices() {
    const serviceRequest = httpMockClient.expectOne(
      `${environment.restServer}/v1/nfs`
    );

    serviceRequest.flush(mockNFCatalog);

    const siteServicesRequest = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/services`
    );

    siteServicesRequest.flush(mockServices);
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        OverlayModule,
        MatSnackBarModule,
        MatCardModule,
        MatTableModule,
        MatCheckboxModule,
        MatDialogModule,
        NgbDatepickerModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        ServerInfoComponent,
        SocketComponent,
        MemoryMeterComponent,
        ToggleComponent,
        LegendComponent,
        TableComponent,
        CellGridComponent,
        AvatarComponent,
        DateRangeComponent,
        ChartThroughputComponent,
        ChartPacketlossComponent,
        ChartLatencyComponent,
        DurationPipe,
        ChartHeaderComponent,
        ChartLineComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: Router,
          useValue: mockRouter
        },
        {
          provide: ActivatedRoute,
          useValue: {
            parent: {
              params: of({
                id: siteID
              }),
              paramMap: of({
                get: () => siteID
              })
            },
            params: of({
              id: serverID
            })
          }
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        ServersService,
        AlertService,
        PipelineNamePipe,
        ByteConverterPipe,
        DecimalPipe,
        UserService
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServerInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    serverInfoDe = fixture.debugElement;
    serverInfoEl = serverInfoDe.nativeElement;

    const type: ViewType = ViewType.Socket;
    component.onChangeView(type);
    fetchServerNFs();
    fetchSockets();
    fetchServerResources();
    fetchNFsAndSiteServices();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render toggle element', () => {
    expect(serverInfoEl.querySelectorAll('nef-toggle')).toBeTruthy('toggle is rendered');
    expect(serverInfoEl.querySelector('nef-toggle').getAttribute('rightlabel')).toBe('list view');
    expect(serverInfoEl.querySelector('nef-toggle').getAttribute('leftlabel')).toBe('socket view');
  });

  it('should update view type after toggle', () => {
    const type: ViewType = ViewType.Socket;
    component.onChangeView(type);
    expect(component.activeViewType).toBeTruthy(type);
    expect(mockRouter.navigate).toHaveBeenCalledWith(['../', type], { relativeTo: component['_route'] });
  });

  it('should render socket elements', () => {
    expect(serverInfoEl.querySelectorAll('nef-socket').length).toBe(2, 'has 2 socket elements');
  });

  it('should render nf list', () => {
    const type: ViewType = ViewType.List;
    component.onChangeView(type);
    fixture.detectChanges();
    expect(serverInfoEl.querySelectorAll('nef-table').length).toBe(1);
    expect(serverInfoEl.querySelectorAll('mat-row').length).toBe(3);
  });

  it('should render legend', () => {
    const legendEl: HTMLUnknownElement = serverInfoEl.querySelector('nef-legend');
    const legendItemEls: NodeListOf<HTMLSpanElement> = legendEl.querySelectorAll('span');

    expect(legendEl).toBeTruthy('legend element is rendered');
    expect(legendEl.classList).toContain('core-legend', 'legend element class is \'core-legend\'');

    expect(legendItemEls.length).toBe(2, 'legend has 2 item elements');
    expect(legendItemEls[0].textContent).toBe('System');
    expect(legendItemEls[0].classList).toContain('dark-grey');
    expect(legendItemEls[1].textContent).toBe('Available', 'last legend item label is \'Available\'');
    expect(legendItemEls[1].classList).toContain('grey', 'last legend item contains \'grey\' class');
  });

  it('should render cores proper color', () => {
    const socket = serverInfoEl.querySelector('nef-socket');
    const cells = socket.querySelectorAll('.mat-card:first-child .cell');
    expect(cells.length).toBe(14);

    expect(cells[0].classList.contains('dark-grey')).toBeTruthy();
    expect(cells[1].classList.contains('dark-grey')).toBeTruthy();
    expect(cells[2].classList.contains('blue')).toBeTruthy();
    expect(cells[3].classList.contains('blue')).toBeTruthy();
    expect(cells[4].classList.contains('blue')).toBeTruthy();
    expect(cells[5].classList.contains('green')).toBeTruthy();
    expect(cells[6].classList.contains('green')).toBeTruthy();
    expect(cells[7].classList.contains('orange')).toBeTruthy();
    expect(cells[8].classList.contains('orange')).toBeTruthy();

    component['getServer']();

    const request = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverID}/nfs`
    );

    const mockNFs = cloneDeep(mockServerNFs);

    mockNFs[0] = mockServerNFs[2];
    mockNFs[2] = mockServerNFs[0];

    request.flush({
      nfInstances: mockNFs
    });

    fetchNFsAndSiteServices();
    fetchSockets();
    fixture.detectChanges();

    expect(cells[0].classList.contains('dark-grey')).toBeTruthy();
    expect(cells[1].classList.contains('dark-grey')).toBeTruthy();
    expect(cells[2].classList.contains('blue')).toBeTruthy();
    expect(cells[3].classList.contains('blue')).toBeTruthy();
    expect(cells[4].classList.contains('blue')).toBeTruthy();
    expect(cells[5].classList.contains('green')).toBeTruthy();
    expect(cells[6].classList.contains('green')).toBeTruthy();
    expect(cells[7].classList.contains('orange')).toBeTruthy();
    expect(cells[8].classList.contains('orange')).toBeTruthy();
  });

  it('should render performance graphs', () => {
    const perfGraphEl: HTMLDivElement = fixture.debugElement.nativeElement.querySelector('section.performance');
    expect(perfGraphEl.querySelector('nef-chart-throughput')).not.toBeNull();
    expect(perfGraphEl.querySelector('nef-chart-latency')).not.toBeNull();
    expect(perfGraphEl.querySelector('nef-chart-packetloss')).not.toBeNull();
  });

  it('should attempt to remove server', fakeAsync(() => {
    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();

    const request = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverID}`
    );

    request.flush({
      identifier: '508839ea',
      name: 'fritz',
      status: 'GOOD'
    });

    const el = fixture.debugElement.nativeElement;
    const removeButton = el.querySelector('.remove-button');
    removeButton.click();

    const dialog = document.querySelector('nef-confirmation-dialog');
    tick();
    const dialogText = dialog.querySelectorAll('p');
    expect(dialogText[0].textContent).toBe('This will immediately remove the server fritz.');
    expect(dialogText[1].textContent).toBe('Are you sure you want to continue?');

    const deleteButtonEl: HTMLButtonElement = dialog.querySelectorAll('button')[1];

    expect(deleteButtonEl.textContent).toBe('Remove Server');

    deleteButtonEl.click();
    tick();

    const req = httpMockClient.expectOne((request: HttpRequest<any>) => {
      return request.url === `${environment.restServer}/v1/sites/-1/servers/fluffy`
        && request.method === 'DELETE'
    });
    req.flush({});

    expect(mockRouter.navigate).toHaveBeenCalledWith(['/sites', '-1'],);

    flush();
  }));

  it('should not update sockets with no changes', fakeAsync(() => {
    const socketsSpy = spyOnProperty(component, 'sockets', 'set').and.callThrough();
    component['getServer']();
    fetchServerNFs();
    fetchSockets();
    tick();

    expect(socketsSpy).not.toHaveBeenCalled();
    fixture.destroy();
  }));

  it('should update sockets only if the changed', fakeAsync(() => {
    const socketsSpy = spyOnProperty(component, 'sockets', 'set').and.callThrough();
    component['getServer']();
    fetchServerNFs();
    fetchSockets();
    fetchServerResources();
    fetchNFsAndSiteServices();
    tick();

    component['getServer']();
    fetchServerNFs();
    fetchNFsAndSiteServices();

    const request = httpMockClient.expectOne(
      `${environment.restServer}/v1/sites/${siteID}/servers/${serverID}/sockets`
    );

    // fetch a single socket
    request.flush({
      sockets: [mockSockets[1]]
    });

    fetchServerResources();
    tick();

    expect(socketsSpy).toHaveBeenCalled();
    fixture.destroy();
  }));

  it('should properly render system resources', () => {
    expect(component.sockets[0].system.cores[0]).toBe(0);
    expect(component.sockets[0].system.cores[1]).toBe(1);
    expect(component.sockets[0].system.memory).toBe(8589934592);
  });

  it('should render remove server button only for system admin', fakeAsync(() => {
    userService.setUser(mockSystemUser);
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;
    const removeButton = el.querySelector('.remove-button');
    expect(removeButton).toBeNull();
  }));
});
