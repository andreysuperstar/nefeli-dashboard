import { orderBy, isEqual } from 'lodash-es';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Subscription, combineLatest, forkJoin, of, Observable } from 'rxjs';
import { tap, map, catchError, filter, switchMap } from 'rxjs/operators';

import { LegendItem } from '../../shared/legend/legend.component';
import { Column } from '../../shared/table/table.component';
import { Range } from '../../shared/date-range/date-range.component';
import { UnitType, ZoomEvent } from '../../shared/chart-line/chart-line.component';

import { AlertService, AlertType } from '../../shared/alert.service';
import { Socket, ServerNF, ServersService } from '../servers.service';
import { LocalStorageService, LocalStorageKey } from '../../shared/local-storage.service';

import { ConfirmationDialogComponent, ConfirmationDialogData } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpError } from 'src/app/error-codes';
import { ServerResourceAllocation } from 'rest_client/pangolin/model/serverResourceAllocation';
import { RESTServer } from 'rest_client/pangolin/model/rESTServer';
import { UserService } from '../../users/user.service';

export enum ViewType {
  List = 'list',
  Socket = 'socket'
}

const NF_LIST_COLUMNS: Column[] = [
  {
    label: 'NF Instance',
    name: 'instance'
  },
  {
    name: 'service'
  },
  {
    label: 'NF Cores',
    name: 'cores'
  },
  {
    name: 'socket'
  }
];

const Config = {
  POLL_INTERVAL: 5000
};

const msInSec = 1000;

interface NFRow {
  instance: string;
  service: string;
  cores: number;
  socket: string;
}

const DIALOG_CONFIG: MatDialogConfig<ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-server-info',
  templateUrl: './server-info.component.html',
  styleUrls: ['./server-info.component.less']
})
export class ServerInfoComponent implements OnInit, OnDestroy {
  private _activeViewType: ViewType = ViewType.Socket;
  private _clusterId: string;
  private _server: RESTServer;
  private _serverId: string;
  private _sockets: Socket[];
  private _serverNFs: ServerNF[];
  private _dataSource: NFRow[];
  private _chartRange: Range;
  private _chartStartTime: number;
  private _chartDuration: number;
  private _subscription = new Subscription();
  private _dialogSubscription: Subscription;
  private _socketSubscription: Subscription;
  private _socketTimer: number;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _alert: AlertService,
    private _serversService: ServersService,
    private _store: LocalStorageService,
    private _dialog: MatDialog,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    const subscription = combineLatest([
      this._route.params,
      this._route.parent.params
    ])
      .subscribe(([params, parentParams]) => {
        this._activeViewType = params.view;
        this._clusterId = parentParams.id;
        this._serverId = params.id;
        this._sockets = undefined;
        this.getServer();
      });

    this._subscription.add(subscription);

    this.getChartRange();
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
    this.stopSocketStream();
    this._sockets = undefined;
  }

  private getServer() {
    this._serversService.getServer(this._clusterId, this._serverId)
      .subscribe((response: RESTServer) => {
        this._server = response;
      });

    if (this._socketSubscription) {
      this._socketSubscription.unsubscribe();
    }

    this.startSocketStream();
  }

  private startSocketStream() {
    this.stopSocketStream();

    this._socketSubscription = forkJoin([
      // TODO(ecarino): refactor, all of this data is available via /server API, these reqs not needed
      this._serversService.getServerNFs(this._clusterId, this._serverId),
      this._serversService.getServerSockets(this._clusterId, this._serverId),
      this._serversService.getServerResources(this._clusterId, this._serverId)
    ]).pipe(
      tap(this.setServerNFs.bind(this)),
      tap(this.setDataSource.bind(this)),
      map(this.setSocketsNFs.bind(this)),
      tap((_: Socket[]) =>
        this._alert.dismiss(AlertType.ERROR_SITE_CONNECTION_RETRY)
      ),
      catchError((error: Error) => {
        if ((error as HttpErrorResponse).status !== HttpError.Canceled) {
          this._alert.error(AlertType.ERROR_SITE_CONNECTION_RETRY);
        }

        this._socketTimer = window.setTimeout(() => {
          this.startSocketStream();
        }, Config.POLL_INTERVAL);

        return of([]);
      })
    ).subscribe((sockets: Socket[]) => {
      sockets = orderBy(sockets, 'id');
      //  Re-assign sockets only when they changed to prevent flickering on hover
      if (!this._sockets || !isEqual(sockets, this.sockets)) {
        this.sockets = sockets;
      }

      this._socketTimer = window.setTimeout(() => {
        this.startSocketStream();
      }, Config.POLL_INTERVAL);
    });
  }

  private stopSocketStream() {
    if (this._socketSubscription) {
      this._socketSubscription.unsubscribe();
    }

    if (this._socketTimer) {
      clearTimeout(this._socketTimer);
      this._socketTimer = undefined;
    }
  }

  private setServerNFs([serverNFs, sockets, _]: [ServerNF[], Socket[], ServerResourceAllocation]) {
    this._serverNFs = serverNFs;
  }

  private setDataSource() {
    this._dataSource = this._serverNFs.map((nF: ServerNF): NFRow => {
      let cores: number[] = [];
      let socket = '';
      let count = 0;

      if (nF.resources.coresBySocket) {
        const sockets = Object.keys(nF.resources.coresBySocket);
        for (const sock of sockets) {
          cores = cores.concat(nF.resources.coresBySocket[sock].cores);
          socket += (count > 0) ? `,${sock}` : `${sock}`;
          count++;
        }
      }

      return {
        instance: nF.metadata.name,
        service: nF.metadata.service,
        cores: cores.length,
        socket
      } as NFRow;
    });
  }

  private setSocketsNFs([_, sockets, resources]: [ServerNF[], Socket[], ServerResourceAllocation]): Socket[] {
    return sockets.map((socket: Socket): Socket => {
      const nfs: ServerNF[] = this.getSocketNFs(socket);
      const systemCores = this.getSocketSystemCores(socket, resources);
      const systemMemory = this.getSocketSystemMemory(socket, resources);

      return {
        ...socket,
        nfs,
        system: {
          cores: systemCores,
          memory: systemMemory
        }
      } as Socket;
    });
  }

  private getSocketNFs({ id }: Socket): ServerNF[] {
    return this._serverNFs.filter((nF: ServerNF): boolean => {
      let rt = false;

      if (nF.resources.coresBySocket) {
        const sockets = Object.keys(nF.resources.coresBySocket);
        for (const socket of sockets) {
          if (+socket === id) {
            rt = true;
          }
        }
      }

      return rt;
    });
  }

  private getSocketSystemMemory(socket: Socket, resources: ServerResourceAllocation): number {
    let memory = 0;

    if (resources.control && resources.control.numaResources) {
      for (const resource of Object.values(resources.control.numaResources)) {
        if (resource.cores.every((a) => socket.cores.includes(a))) {
          const hugePageSize: number = Number(Object.keys(resource.hugepages)[0]);
          if (hugePageSize) {
            const { [hugePageSize]: hugePageCount } = resource.hugepages;
            memory += Number(hugePageSize) * hugePageCount;
          }
        }
      }
    }

    if (resources.bess && resources.bess.numaResources) {
      for (const resource of Object.values(resources.bess.numaResources)) {
        if (resource.cores.every((a) => socket.cores.includes(a))) {
          const hugePageSize: number = Number(Object.keys(resource.hugepages)[0]);
          if (hugePageSize) {
            const { [hugePageSize]: hugePageCount } = resource.hugepages;
            memory += Number(hugePageSize) * hugePageCount;
          }
        }
      }
    }

    return memory;
  }

  private getSocketSystemCores(socket: Socket, resources: ServerResourceAllocation): number[] {
    let systemCores = new Array<number>();

    if (resources.control && resources.control.numaResources) {
      for (const resource of Object.values(resources.control.numaResources)) {
        systemCores = systemCores.concat(resource.cores);
      }
    }

    if (resources.bess && resources.bess.numaResources) {
      for (const resource of Object.values(resources.bess.numaResources)) {
        systemCores = systemCores.concat(resource.cores);
      }
    }

    return socket.cores.filter(x => systemCores.includes(x));
  }

  public onChangeView(viewType: ViewType) {
    this._activeViewType = viewType;
    this._router.navigate(['../', viewType], { relativeTo: this._route });
  }

  private getChartRange() {
    const data: string | null = this._store.read(LocalStorageKey.TENANT_CHART_DATE_RANGE);

    if (data) {
      this._chartRange = JSON.parse(data);
    }
  }

  public onZoomEvent(event: ZoomEvent) {
    const range: Range = {
      start: event.start.unix(),
      duration: event.end.diff(event.start) / msInSec,
      period: false
    };
    this._chartRange = range;
    this.onDateSelect(range);
  }

  public onDateSelect({ start, duration, period }: Range) {
    this._chartStartTime = (period) ? undefined : start;
    this._chartDuration = duration;

    this._store.write(
      LocalStorageKey.TENANT_CHART_DATE_RANGE,
      JSON.stringify({ start, duration, period })
    );
  }

  public onDeleteServer() {
    const data: ConfirmationDialogData = {
      description: `This will immediately remove the server <strong>${this._server?.name || this._serverId}</strong>.`,
      action: 'Remove Server'
    };

    const dialogRef = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        switchMap((_: boolean) => this._serversService.deleteServer(
          this._clusterId,
          this._serverId
        ))
      )
      .subscribe(() => {
        // go back to the overview page
        this._router.navigate(['/sites', this._clusterId]);
      });
  }

  public set sockets(sockets: Socket[]) {
    this._sockets = sockets;
  }

  public get activeViewType(): ViewType {
    return this._activeViewType;
  }

  public get sockets(): Socket[] {
    return this._sockets;
  }

  public get ViewType() {
    return ViewType;
  }

  public get nFLegendItems(): LegendItem[] {
    return [
      {
        label: 'System',
        color: 'dark-grey'
      },
      {
        label: 'Available',
        color: 'grey'
      }
    ];
  }

  public get columns(): Column[] {
    return NF_LIST_COLUMNS;
  }

  public get dataSource(): NFRow[] {
    return this._dataSource;
  }

  public get UnitType(): typeof UnitType {
    return UnitType;
  }

  public get clusterId(): string {
    return this._clusterId;
  }

  public get serverId(): string {
    // TODO(ecarino): workaround to use server name instead of id, until NEF-7001 is resolved
    return this._server?.identifier;
  }

  public get chartRange(): Range {
    return this._chartRange;
  }

  public get chartDuration(): number {
    return this._chartDuration;
  }

  public get chartStartTime(): number {
    return this._chartStartTime;
  }

  public get server(): RESTServer {
    return this._server;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
