import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClusterResolverService } from './cluster-resolver.service';

import { ClusterComponent } from './cluster.component';
import { ClusterOverviewComponent } from './cluster-overview/cluster-overview.component';
import { ServerInfoComponent } from './server-info/server-info.component';
import { SitesOverviewComponent } from '../sites-overview/sites-overview.component';

const routes: Routes = [
  {
    path: '',
    component: ClusterComponent,
    children: [
      {
        path: 'add',
        component: ClusterComponent,
        data: {
          isAddSite: true
        }
      }
    ]
  },
  {
    path: ':id',
    component: ClusterOverviewComponent,
    resolve: {
      clusters: ClusterResolverService
    },
    children: [
      {
        path: '',
        component: SitesOverviewComponent,
        resolve: {
          clusters: ClusterResolverService
        }
      },
      {
        path: 'servers/:id',
        redirectTo: 'servers/:id/socket',
        pathMatch: 'full'
      },
      {
        path: 'servers/:id/:view',
        component: ServerInfoComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  providers: [ClusterResolverService],
  exports: [
    RouterModule
  ]
})
export class ClusterRoutingModule { }
