import { Component, ViewChild, OnInit, OnDestroy, Inject } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators, ValidatorFn, AbstractControl, ValidationErrors, FormControl, FormArray } from '@angular/forms';
import { MatDialogRef, MatDialog, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatExpansionPanel } from '@angular/material/expansion';

import { Subscription, Observable, of, Subject } from 'rxjs';
import { map, catchError, take } from 'rxjs/operators';

import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { SiteConfigWAN } from 'rest_client/pangolin/model/siteConfigWAN';
import { StaticWANConfig } from 'rest_client/pangolin/model/staticWANConfig';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';
import { HardwareProfileExpDataplaneBondWithID } from 'rest_client/pangolin/model/hardwareProfileExpDataplaneBondWithID';
import { DHCPWANConfig } from 'rest_client/pangolin/model/dHCPWANConfig';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { ClusterService } from 'src/app/home/cluster.service';
import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';

import { URL_PATTERN, MAC_PATTERN, CIDR_PATTERN, vniRangeValidator } from '../../sites-overview/site.validation';
import { HardwareService } from 'rest_client/pangolin/api/hardware.service';
import { HardwareProfiles } from 'rest_client/pangolin/model/hardwareProfiles';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { HardwareProfilesDialogComponent } from '../../hardware-profiles/hardware-profiles-dialog/hardware-profiles-dialog.component';
import { HardwareProfileDialogData } from '../../hardware-profiles/hardware-profiles-dialog/hardware-profiles-dialog.component';
import { MatRadioChange } from '@angular/material/radio';
import { SoftwareProfileManagerDialogComponent, SoftwareProfileManagerDialogData } from 'src/app/software-profiles/software-profile-manager-dialog/software-profile-manager-dialog.component';

const DIALOG_CONFIG: MatDialogConfig<HardwareProfileDialogData | SoftwareProfileManagerDialogData> = {
  width: '750px',
  autoFocus: false,
  restoreFocus: false
};

const DefaultVXLANUDPPort = 4789;

export enum UDPPortRange {
  Min = 1024,
  Max = 49151
}

export enum WANIPType {
  Static = 'Static',
  DHCP = 'DHCP'
}

export interface SiteDialogData {
  site: SiteConfig;
}

@Component({
  selector: 'nef-add-site-dialog',
  templateUrl: './add-site-dialog.component.html',
  styleUrls: ['./add-site-dialog.component.less']
})
export class AddSiteDialogComponent implements OnInit, OnDestroy {

  private _site: SiteConfig;
  private _siteForm: FormGroup;
  private _siteTypes: SiteConfigSiteType[] = Object.values(SiteConfigSiteType);
  private _siteSubscription: Subscription;
  private _hardwareProfiles: HardwareProfile[];
  private _softwareProfiles: readonly SoftwareProfile[];
  private _bonds: HardwareProfileExpDataplaneBondWithID[];
  private _hardwareProfiles$ = new Subject<HardwareProfile[]>();
  private _dialogSubscription: Subscription;

  @ViewChild('wanIPPanel', {
    static: true
  }) private _wanIPPanel: MatExpansionPanel;

  @ViewChild('dataPlanePanel', {
    static: true
  }) private _dataPlanePanel: MatExpansionPanel;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<AddSiteDialogComponent>,
    private _dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private _data: SiteDialogData,
    private _alertService: AlertService,
    private _siteService: ClusterService,
    private _hardwareService: HardwareService,
    private _softwareProfilesService: SoftwareProfilesService
  ) { }

  public ngOnInit() {
    if (this._data) {
      this._site = this._data.site;
    }

    this.initSiteForm();
    this.getHardwareProfiles();
    this.getSoftwareProfiles();

    if (this._site) {
      this.setWANFields();
    }
  }

  public ngOnDestroy() {
    if (this._siteSubscription) {
      this._siteSubscription.unsubscribe();
    }
  }

  private getHardwareProfiles() {
    this._hardwareService.getHardwareProfiles().subscribe((profiles: HardwareProfiles) => {
      this._hardwareProfiles = profiles.hardwareProfiles;
      this._hardwareProfiles$.next(profiles.hardwareProfiles);
      const siteProfileId = this._site?.hardwareProfileId;
      if (siteProfileId) {
        this._siteForm.get('profiles.hardware').setValue(siteProfileId);
        this.onHardwareProfileIDSelectionChange(siteProfileId);
      }
    });
  }

  private getSoftwareProfiles() {
    this._softwareProfilesService
      .getSoftwareProfiles()
      .subscribe(({ softwareProfiles }) => {
        this._softwareProfiles = Object.freeze(softwareProfiles);

        this._siteForm
          .get('profiles.software')
          .enable();
      });
  }

  private initSiteForm() {
    const siteType: SiteConfigSiteType = this._site?.siteType ?? SiteConfigSiteType.SITETYPENULL;
    this._siteForm = this._fb.group({
      name: [
        this._site?.name ?? '',
        Validators.required
      ],
      type: siteType,
      url: [
        this._site?.mgmtUrl ?? '',
        [
          Validators.required,
          Validators.pattern(URL_PATTERN)
        ]
      ],
      vlanTag: [
        this._site?.mgmtVlanTag,
        Validators.required
      ],
      profiles: this._fb.group(
        {
          hardware: [
            this._site?.hardwareProfileId ?? undefined,
            this.hardwareProfileValidator.bind(this)
          ],
          software: {
            value: this._site?.softwareProfileId ?? undefined,
            disabled: true
          }
        }
      ),
      provisioningId: [
        this._site?.provisioningIdentifier ?? '',
        Validators.required
      ],
      dataPlaneConnectivity: this._fb.group({
        vlan: this._site?.dataplaneVlanTag,
        ip: [
          this._site?.publicIp ?? '',
          Validators.required
        ],
        mac: [
          this._site?.gatewayMac ?? '',
          [
            Validators.required,
            Validators.pattern(MAC_PATTERN)
          ]
        ],
        vxlanPort: [
          this._site?.inboundVxlanPort ?? DefaultVXLANUDPPort,
          [
            Validators.min(UDPPortRange.Min),
            Validators.max(UDPPortRange.Max)
          ]
        ],
        vniRange: this._fb.group(
          {
            start: [
              this._site?.vniRangeStart ?? 0,
              [
                Validators.required,
                Validators.min(0)
              ]
            ],
            end: [
              this._site?.vniRangeEnd,
              [
                Validators.required,
                Validators.min(0),
              ]
            ]
          },
          {
            validators: vniRangeValidator
          }
        ),
      }),
      wanIP: this._fb.group({
        bond: undefined,
        type: [undefined, Validators.required],
        static: this._fb.group({
          ip: [undefined, [
            Validators.pattern(CIDR_PATTERN),
            this.wanIpValidator.bind(this)
          ]],
          defaultGateway: [undefined, this.wanIpValidator.bind(this)],
          dns: this._fb.array([''], this.wanIpDnsValidator.bind(this))
        }),
        dhcp: this._fb.group({
          dns: this._fb.array([''])
        })
      })
    });

    if (this._site?.siteType === SiteConfigSiteType.UCPE && this._site?.hardwareProfileId) {
      this._siteForm.get('wanIP').enable();
    } else {
      this._siteForm.get('wanIP').disable();
    }
  }

  private wanIpDnsValidator(control: FormArray): ValidationErrors {
    // filter out blank entries
    const addresses = control.value?.filter((dns: string) => dns && dns.trim().length > 0);
    const type = this._siteForm?.get('wanIP.type')?.value;
    return (type === WANIPType.Static && addresses.length === 0) ? { required: true } : undefined;
  }

  private wanIpValidator(control: FormControl): ValidationErrors {
    const type = this._siteForm?.get('wanIP.type')?.value;
    return (type === WANIPType.Static && !control.value) ? { required: true } : undefined;
  }

  private hardwareProfileValidator(control: FormControl): ValidationErrors {
    if (this._siteForm?.get('type')?.value === SiteConfigSiteType.UCPE && !control.value) {

      return {
        required: true
      };
    }

    return undefined;
  }

  private setWANFields() {
    const wanIPControl: AbstractControl = this._siteForm.get('wanIP');
    const wan = this._site.wans?.[0] ?? {};
    const { bondId, dhcp } = wan;

    this._hardwareProfiles$
      .pipe(
        take(1)
      )
      .subscribe((hwProfiles: HardwareProfile[]) => {
        wanIPControl.get('bond').setValue(bondId);

        if (wan['static']) {
          wanIPControl.get('type').setValue(WANIPType.Static);
          const staticControl: AbstractControl = wanIPControl.get('static');
          const { dnsAddrs, gatewayAddr, wanCidr } = wan['static'];
          staticControl.get('ip').setValue(wanCidr);
          staticControl.get('defaultGateway').setValue(gatewayAddr);
          this.staticDNS.clear();
          dnsAddrs.forEach((dns: string) => this.staticDNS.push(new FormControl(dns)));
        } else if (dhcp) {
          wanIPControl.get('type').setValue(WANIPType.DHCP);
          const dynamicControl: AbstractControl = wanIPControl.get('dhcp');
          const { dnsAddrs } = dhcp;
          this.dhcpDNS.clear();
          dnsAddrs.forEach((dns: string) => this.dhcpDNS.push(new FormControl(dns)));
        }
      });
  }

  public onSiteTypeSelectionChange(selectedSiteType: SiteConfigSiteType) {
    this._siteForm.updateValueAndValidity();

    if (selectedSiteType !== SiteConfigSiteType.UCPE) {
      this._siteForm.get('wanIP').disable();
      this._wanIPPanel.close();
    } else {
      this._siteForm.get('wanIP').enable();
      this._siteForm.get('wanIP').updateValueAndValidity();
    }
  }

  public onWanTypeChange(ev: MatRadioChange) {
    switch (ev.value) {
      case WANIPType.Static:
        this._siteForm.get('wanIP.static').enable();
        this._siteForm.get('wanIP.dhcp').disable();
        break;
      case WANIPType.DHCP:
        this._siteForm.get('wanIP.static').disable();
        this._siteForm.get('wanIP.dhcp').enable();
        break;
    }
  }

  public onHardwareProfileIDSelectionChange(selectedHardwareProfileID: string) {
    const { bonds }: HardwareProfile = this._hardwareProfiles?.find(
      ({ identifier }: HardwareProfile): boolean => identifier === selectedHardwareProfileID
    );

    this._bonds = bonds;

    if (selectedHardwareProfileID && this._siteForm.get('wanIP').disabled) {
      this._siteForm.get('wanIP').enable();
    }

    this._siteForm.get('wanIP.bond')?.patchValue(
      bonds.filter(b => b?.bond?.purpose === 'WAN')?.[0]?.identifier
    );
    this._siteForm.get('wanIP.bond')?.updateValueAndValidity?.({
      onlySelf: true
    });
  }

  /**
   * @description Fixes Firefox bug allowing to type characters in number input
   * {@link https://bugzilla.mozilla.org/show_bug.cgi?id=1398528}.
   *
   * @param event Keyboard event.
   */
  public onNumberInputKeydown({
    key,
    metaKey,
    ctrlKey,
    preventDefault,
  }: KeyboardEvent) {
    if (!(metaKey || ctrlKey) && key.length < 2 && isNaN(Number(key))) {
      preventDefault();
    }
  }

  public onAddStaticDNS() {
    this.staticDNS.push(new FormControl());
  }

  public onRemoveStaticDNS(index: number) {
    this.staticDNS.removeAt(index);
  }

  public onAddDHCPDNS() {
    this.dhcpDNS.push(new FormControl());
  }

  public onRemoveDHCPDNS(index: number) {
    this.dhcpDNS.removeAt(index);
  }

  public submitSiteForm() {
    const { invalid } = this._siteForm;

    if (invalid) {
      if (this._siteForm.get('dataPlaneConnectivity').invalid) {
        this._dataPlanePanel.open();
      }

      if (this._siteForm.get('wanIP').invalid) {
        this._wanIPPanel.open();
      }

      return;
    }

    this._site ? this.editSite() : this.createSite();
  }

  public onHardwareProfilesEditClick(event: Event) {
    const data: HardwareProfileDialogData = {
      hardwareProfiles: this._hardwareProfiles
    };

    const dialogRef = this._dialog.open(
      HardwareProfilesDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .subscribe(() => {
        this.getHardwareProfiles();
      });
  }

  public onSoftwareProfilesManagerOpen() {
    const data: SoftwareProfileManagerDialogData = {
      profiles: this._softwareProfiles
    };

    const dialogRef = this._dialog.open<
      SoftwareProfileManagerDialogComponent,
      SoftwareProfileManagerDialogData,
      readonly SoftwareProfile[]
    >(SoftwareProfileManagerDialogComponent, { ...DIALOG_CONFIG, data });

    this._dialogSubscription?.unsubscribe();

    this._dialogSubscription = dialogRef
      .afterClosed()
      .subscribe(profiles => {
        this._softwareProfiles = Object.freeze(profiles);
      });
  }

  private createSite() {
    const site: SiteConfig = this.parseSiteForm();

    if (this._siteSubscription) {
      this._siteSubscription.unsubscribe();
    }

    this._siteSubscription = this._siteService
      .postSite(site)
      .subscribe(
        (response: SiteConfig) => {
          this._alertService.info(AlertType.INFO_CREATE_SITE_SUCCESS);

          this._dialogRef.close(response);
        },
        (resp: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_CREATE_SITE, resp.error.message)
      );
  }

  private editSite() {
    const site: SiteConfig = this.parseSiteForm();
    if (this._siteSubscription) {
      this._siteSubscription.unsubscribe();
    }

    this._siteSubscription = this._siteService
      .putSite(this._site.identifier, site)
      .subscribe(
        (response: any) => {
          this._alertService.info(AlertType.INFO_EDIT_SITE_SUCCESS);
          this._dialogRef.close(site);
        },
        (resp: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_UPDATE_SITE, resp.error.message)
      );
  }

  private parseSiteForm(): SiteConfig {
    const {
      name,
      type,
      url,
      vlanTag,
      profiles: {
        hardware: hardwareProfile,
        software: softwareProfile
      },
      provisioningId,
      dataPlaneConnectivity: {
        vlan,
        ip,
        mac,
        vxlanPort,
        vniRange: { start, end }
      }
    }: {
      name: string;
      type: SiteConfigSiteType;
      url: string;
      vlanTag: string;
      profiles: {
        hardware: string;
        software: string;
      },
      provisioningId: string;
      dataPlaneConnectivity: {
        vlan: number;
        ip: string;
        mac: string;
        vxlanPort: number;
        vniRange: {
          start: number;
          end: number;
        }
      };
    } = this._siteForm.value;

    let bond: string;
    let wanIPType: string;
    let wanCIDR: string;
    let defaultGateway: string;
    let staticDNS: string[];
    let dhcpDNS: string[];

    if (this._siteForm.get('wanIP').enabled) {
      bond = this._siteForm.get('wanIP.bond').value;
      wanIPType = this._siteForm.get('wanIP.type').value;

      if (this._siteForm.get('wanIP.static').enabled) {
        wanCIDR = this._siteForm.get('wanIP.static.ip').value;
        defaultGateway = this._siteForm.get('wanIP.static.defaultGateway').value;
        staticDNS = this._siteForm.get('wanIP.static.dns').value;
      }

      if (this._siteForm.get('wanIP.dhcp').enabled) {
        dhcpDNS = this._siteForm.get('wanIP.dhcp.dns').value;
      }
    }

    const site: SiteConfig = {
      dataplaneVlanTag: Number(vlan),
      name: name,
      publicIp: ip,
      privateIp: this._site?.privateIp ?? ip,
      gatewayMac: mac,
      inboundVxlanPort: vxlanPort,
      hardwareProfileId: hardwareProfile,
      softwareProfileId: softwareProfile,
      provisioningIdentifier: provisioningId,
      mgmtUrl: url,
      mgmtVlanTag: Number(vlanTag),
      siteStatus: (type === SiteConfigSiteType.CLUSTER) ? SiteConfigSiteStatus.ACTIVE : SiteConfigSiteStatus.STAGED,
      siteType: type,
      vniRangeStart: Number(start),
      vniRangeEnd: Number(end),
    };

    if (type === SiteConfigSiteType.UCPE) {
      const wans: SiteConfigWAN[] = [];

      const wan: SiteConfigWAN = {
        bondId: bond
      };

      switch (wanIPType) {
        case WANIPType.Static:
          wan['static'] = {
            dnsAddrs: staticDNS.filter((dns: string) => dns && dns.trim().length > 0),
            gatewayAddr: defaultGateway,
            wanCidr: wanCIDR
          } as StaticWANConfig;
          break;
        case WANIPType.DHCP:
          wan.dhcp = {
            dnsAddrs: dhcpDNS.filter((dns: string) => dns && dns.trim().length > 0)
          } as DHCPWANConfig;
      }

      wans.push(wan);

      site.wans = wans;
    }

    return site;
  }

  public get siteForm(): FormGroup {
    return this._siteForm;
  }

  public get siteTypes(): SiteConfigSiteType[] {
    return this._siteTypes;
  }

  public get SiteConfigSiteType(): typeof SiteConfigSiteType {
    return SiteConfigSiteType;
  }

  public get hardwareProfiles(): HardwareProfile[] {
    return this._hardwareProfiles;
  }

  public get softwareProfiles(): readonly SoftwareProfile[] {
    return this._softwareProfiles;
  }

  public get bonds(): HardwareProfileExpDataplaneBondWithID[] {
    return this._bonds;
  }

  public get WANIPType(): typeof WANIPType {
    return WANIPType;
  }

  public get staticDNS(): FormArray {
    return this._siteForm.get('wanIP.static.dns') as FormArray;
  }

  public get dhcpDNS(): FormArray {
    return this._siteForm.get('wanIP.dhcp.dns') as FormArray;
  }

  public get site(): SiteConfig {
    return this._site;
  }

  public get wanBonds(): HardwareProfileExpDataplaneBondWithID[] {
    return this._bonds?.filter((bond: HardwareProfileExpDataplaneBondWithID) => bond.bond.purpose === SubnetType.WAN);
  }

  public get UDPPortRange(): typeof UDPPortRange {
    return UDPPortRange;
  }
}
