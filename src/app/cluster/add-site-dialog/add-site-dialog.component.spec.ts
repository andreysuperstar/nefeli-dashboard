import { HardwareProfilesDialogComponent } from '../../hardware-profiles/hardware-profiles-dialog/hardware-profiles-dialog.component';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { OverlayContainer } from '@angular/cdk/overlay';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatRadioGroupHarness } from '@angular/material/radio/testing';
import { ReactiveFormsModule, FormGroup, AbstractControl } from '@angular/forms';
import { MatDialogRef, MatDialogModule, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule, MatRadioChange } from '@angular/material/radio';
import { MatExpansionPanel, MatExpansionModule } from '@angular/material/expansion';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';

import { cloneDeep } from 'lodash-es';

import softwareProfiles from '../../../../mock/software_profiles';
import { environment } from 'src/environments/environment';

import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { HardwareProfile } from 'rest_client/pangolin/model/hardwareProfile';
import { HardwareProfiles } from 'rest_client/pangolin/model/hardwareProfiles';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { SubnetType } from 'rest_client/pangolin/model/subnetType';
import { BASE_PATH } from 'rest_client/pangolin/variables';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { ClusterService } from 'src/app/home/cluster.service';
import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';

import { WANIPType, AddSiteDialogComponent, SiteDialogData } from './add-site-dialog.component';
import { SoftwareProfileManagerDialogComponent } from 'src/app/software-profiles/software-profile-manager-dialog/software-profile-manager-dialog.component';
import { SoftwareProfilesComponent } from 'src/app/software-profiles/software-profiles.component';
import { TableComponent } from 'src/app/shared/table/table.component';

describe('AddSiteDialogComponent', () => {
  let component: AddSiteDialogComponent;
  let fixture: ComponentFixture<AddSiteDialogComponent>;
  let debugEl: DebugElement;
  let httpTestingController: HttpTestingController;
  let overlayContainer: OverlayContainer;
  let loader: HarnessLoader;
  let documentRootLoader: HarnessLoader;

  const dialogSpy = {
    close: jasmine.createSpy('close')
  };

  const mockSiteFormData: Partial<{
    id: string;
    name: string;
    type: SiteConfigSiteType;
    url: string;
    vlanTag: number;
    profiles: {
      hardware: string;
      software: string;
    },
    provisioningId: string;
    dataPlaneConnectivity: {
      vlan: string;
      ip: string;
      mac: string;
      vxlanPort: number;
      vniRange: {
        start: string;
        end: string;
      };
    };
    wanIP: Partial<{
      bond: string;
      type: WANIPType
      static: Partial<{
        ip: string;
        defaultGateway: string;
        dns: string[];
      }>;
      dhcp: Partial<{
        dns: string[];
      }>;
    }>;
  }> = {
    id: 'andrew_site',
    name: 'Andrew Site',
    type: SiteConfigSiteType.UCPE,
    profiles: {
      hardware: 'profile_1',
      software: 'profile_1',
    },
    provisioningId: 'some-unique-id',
    url: 'https://nefeli.io',
    vlanTag: 3,
    dataPlaneConnectivity: {
      vlan: '4',
      ip: '112.92.1.214',
      mac: '00-E0-4C-00-07-BE',
      vxlanPort: 13333,
      vniRange: {
        start: '91',
        end: '3110'
      }
    }
  };

  const mockSite: SiteConfig = {
    dataplaneVlanTag: Number(mockSiteFormData.dataPlaneConnectivity.vlan),
    name: mockSiteFormData.name,
    identifier: mockSiteFormData.id,
    publicIp: mockSiteFormData.dataPlaneConnectivity.ip,
    privateIp: mockSiteFormData.dataPlaneConnectivity.ip,
    gatewayMac: mockSiteFormData.dataPlaneConnectivity.mac,
    inboundVxlanPort: mockSiteFormData.dataPlaneConnectivity.vxlanPort,
    mgmtUrl: mockSiteFormData.url,
    mgmtVlanTag: mockSiteFormData.vlanTag,
    siteStatus: SiteConfigSiteStatus.STAGED,
    siteType: mockSiteFormData.type,
    vniRangeStart: Number(mockSiteFormData.dataPlaneConnectivity.vniRange.start),
    vniRangeEnd: Number(mockSiteFormData.dataPlaneConnectivity.vniRange.end),
    hardwareProfileId: mockSiteFormData.profiles.hardware,
    softwareProfileId: mockSiteFormData.profiles.software,
    provisioningIdentifier: 'my-unique-id',
    wans: [
      {
        bondId: 'bond_1',
      }
    ]
  };

  mockSite.wans[0]['static'] = {
    dnsAddrs: ['1.1.1.1', '3.3.3.3'],
    gatewayAddr: '255.255.255.255',
    wanCidr: '112.23.78.1/32'
  };

  const mockSiteDialogData: SiteDialogData = {
    site: mockSite
  };

  const hardwareProfile: HardwareProfile = {
    bonds: [
      {
        identifier: 'lan_bond',
        bond: {}
      },
      {
        identifier: 'wan_bond',
        bond: {
          purpose: SubnetType.WAN,
          devices: ['device1', 'device2', 'device3']
        }
      },
    ],
    description: '',
    name: 'Profile 1',
    identifier: 'profile_1'
  };

  const mockHWProfilesResponse: HardwareProfiles = {
    hardwareProfiles: [
      hardwareProfile
    ]
  }

  const mockSoftwareProfiles = Object.freeze(softwareProfiles);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatExpansionModule,
        MatRadioModule,
        MatTableModule,
        MatTooltipModule
      ],
      declarations: [
        AddSiteDialogComponent,
        SoftwareProfilesComponent,
        SoftwareProfileManagerDialogComponent,
        TableComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: undefined
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        AlertService,
        ClusterService,
        SoftwareProfilesService,
        DecimalPipe
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    overlayContainer = TestBed.inject(OverlayContainer);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSiteDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
    component = fixture.componentInstance;
    debugEl = fixture.debugElement;
    fixture.detectChanges();

    const softwareProfilesRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/software_profiles`
    });

    softwareProfilesRequest.flush(mockSoftwareProfiles);
    fixture.detectChanges();

    dialogSpy.close.calls.reset();
  });

  afterEach(async () => {
    const dialogs = await documentRootLoader.getAllHarnesses(MatDialogHarness);

    await Promise.all(
      dialogs.map(dialog => dialog.close())
    );

    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(debugEl.query(
      By.css('[mat-dialog-title]')
    ).nativeElement.textContent).toBe('Add New Site', 'valid heading');
  });

  it('should have site form', () => {
    expect(debugEl.query(By.css('#site-form')).nativeElement).toBeDefined('has site form');
  });

  it('should validate MAC address field', () => {
    const macAddressFormField: AbstractControl = component['_siteForm'].get('dataPlaneConnectivity.mac');
    const {
      dataPlaneConnectivity: { mac }
    } = mockSiteFormData;

    macAddressFormField.setValue(mac);

    expect(macAddressFormField.hasError('pattern'))
      .toBeFalsy('has no \'pattern\' validation error for MAC address field');

    macAddressFormField.setValue(mac.replace(/[a-z]/i, 'g'));

    expect(macAddressFormField.hasError('pattern'))
      .toBeTruthy('has a \'pattern\' validation error for MAC address field');
  });

  it('should close dialog with form data only when form is valid', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`);
    req.flush(mockHWProfilesResponse);
    const form: FormGroup = component['_siteForm'];
    const submitButtonEl: HTMLButtonElement = debugEl.query(
      By.css('button[type=submit]')
    ).nativeElement;

    spyOn<any>(component, 'createSite').and.callThrough();

    const {
      name,
      dataPlaneConnectivity: { ip }
    } = mockSiteFormData;

    form.patchValue({ name, ip });
    form.markAsDirty();

    submitButtonEl.click();

    expect(component['createSite']).not.toHaveBeenCalled();

    form.patchValue(mockSiteFormData);
    fixture.detectChanges();
    submitButtonEl.click();

    expect(component['createSite']).toHaveBeenCalledWith();
  });

  it('should make a POST request on form submit for CLUSTER site', () => {
    component['_siteForm'].patchValue(mockSiteFormData);
    component['createSite']();

    const request: TestRequest = httpTestingController.expectOne((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites` && req.method === 'POST';
    });

    expect(request.request.method).toBe('POST');
    expect(request.request.body.dataplaneVlanTag)
      .toBe(Number(mockSiteFormData.dataPlaneConnectivity.vlan));
    expect(request.request.body.name).toBe(mockSiteFormData.name);
    expect(request.request.body.publicIp).toBe(mockSiteFormData.dataPlaneConnectivity.ip);
    expect(request.request.body.privateIp).toBe(mockSiteFormData.dataPlaneConnectivity.ip);
    expect(request.request.body.gatewayMac).toBe(mockSiteFormData.dataPlaneConnectivity.mac);
    expect(request.request.body.inboundVxlanPort).toBe(mockSiteFormData.dataPlaneConnectivity.vxlanPort);
    expect(request.request.body.mgmtUrl).toBe(mockSiteFormData.url);
    expect(request.request.body.mgmtVlanTag).toBe(mockSiteFormData.vlanTag);
    expect(request.request.body.siteStatus).toBe(SiteConfigSiteStatus.STAGED);
    expect(request.request.body.siteType).toBe(mockSiteFormData.type);
    expect(request.request.body.vniRangeStart)
      .toBe(Number(mockSiteFormData.dataPlaneConnectivity.vniRange.start));
    expect(request.request.body.vniRangeEnd)
      .toBe(Number(mockSiteFormData.dataPlaneConnectivity.vniRange.end));
    expect(request.request.body.hardwareProfileId).toBe('profile_1');
    expect(request.request.body.softwareProfileId).toBe(mockSiteDialogData.site.softwareProfileId);

    request.flush(mockSite);
  });

  it('should make a POST request on form submit for UCPE site', () => {
    const mockFormData = cloneDeep(mockSiteFormData);
    mockFormData.type = SiteConfigSiteType.UCPE;
    mockFormData.profiles.hardware = 'profile-id';

    mockFormData.wanIP = {
      bond: '1',
      type: WANIPType.Static,
      static: {
        ip: '112.23.78.1/32',
        defaultGateway: '255.255.255.255',
        dns: ['1.1.1.1', '3.3.3.3']
      }
    };

    // create a UCPE site with a Static WAN IP
    component.siteForm.get('wanIP').enable();
    component.onAddStaticDNS();
    component['_siteForm'].patchValue(mockFormData);
    component['createSite']();

    let request: TestRequest = httpTestingController.expectOne({
      url: `${environment.restServer}/v1/sites`,
      method: 'POST'
    });

    expect(request.request.body.hardwareProfileId).toBe('profile-id');
    expect(request.request.body.siteType).toBe(SiteConfigSiteType.UCPE);
    expect(request.request.body.wans[0].bondId).toBe(mockFormData.wanIP.bond);
    expect(request.request.body.wans[0].static.dnsAddrs[0]).toBe(mockFormData.wanIP.static.dns[0]);
    expect(request.request.body.wans[0].static.dnsAddrs[1]).toBe(mockFormData.wanIP.static.dns[1]);
    expect(request.request.body.wans[0].static.gatewayAddr).toBe(mockFormData.wanIP.static.defaultGateway);
    expect(request.request.body.wans[0].static.wanCidr).toBe(mockFormData.wanIP.static.ip);

    request.flush(mockSite);

    mockFormData.wanIP = {
      bond: '2',
      type: WANIPType.DHCP,
      dhcp: {
        dns: ['2.2.2.2', '4.4.4.4']
      }
    };

    // create a UCPE site with a DHCP WAN IP
    component.onAddDHCPDNS();
    component['_siteForm'].patchValue(mockFormData);
    component['createSite']();

    request = httpTestingController.expectOne({
      url: `${environment.restServer}/v1/sites`,
      method: 'POST'
    });

    expect(request.request.body.hardwareProfileId).toBe('profile-id');
    expect(request.request.body.siteType).toBe(SiteConfigSiteType.UCPE);
    expect(request.request.body.wans[0].bondId).toBe(mockFormData.wanIP.bond);
    expect(request.request.body.wans[0].dhcp.dnsAddrs[0]).toBe(mockFormData.wanIP.dhcp.dns[0]);
    expect(request.request.body.wans[0].dhcp.dnsAddrs[1]).toBe(mockFormData.wanIP.dhcp.dns[1]);

    request.flush(mockSite);
  });

  it('should close dialog only on successful POST response', () => {
    component['_siteForm'].patchValue(mockSiteFormData);
    component['createSite']();

    const request: TestRequest = httpTestingController.expectOne((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites` && req.method === 'POST';
    });

    request.flush(mockSite);

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should not close dialog when getting error http response', () => {
    component['_siteForm'].patchValue(mockSiteFormData);
    component['createSite']();

    const request: TestRequest = httpTestingController.expectOne((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites` && req.method === 'POST';
    });

    const mockErrorResponseOptions: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };

    request.flush({}, mockErrorResponseOptions);

    expect(dialogSpy.close).not.toHaveBeenCalled();
  });

  it('should request for hardware profiles on init', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`);
  });

  it('should show/hide hardware profiles depending on type', () => {
    const el = fixture.debugElement.nativeElement;

    // by default, hardware profile is hidden
    expect(el.querySelector('mat-select[formControlName="hardware"]')).toBeNull();

    // select type=UCPE, hardware profile is visible
    component['_siteForm'].controls.type.patchValue(SiteConfigSiteType.UCPE);
    expect(el.querySelector('mat-select[formControlName="hardware"]')).toBeDefined();

    // select type=CLUSTER, hardware profile is hidden
    component['_siteForm'].controls.type.patchValue(SiteConfigSiteType.CLUSTER);
    expect(el.querySelector('mat-select[formControlName="hardware"]')).toBeNull();
  });

  it('should validate hardware profile', () => {
    const hwprofileControl = component['_siteForm'].get('profiles.hardware');

    // by default, hardware profile control is valid
    expect(hwprofileControl.invalid).toBe(false);

    // select type=UCPE, hardware profile control is invalid when not set
    component['_siteForm'].controls.type.patchValue(SiteConfigSiteType.UCPE);
    hwprofileControl.updateValueAndValidity();
    expect(hwprofileControl.invalid).toBe(true);

    // select type=CLUSTER, hardware profile control is hidden and valid
    component['_siteForm'].controls.type.patchValue(SiteConfigSiteType.CLUSTER);
    hwprofileControl.updateValueAndValidity();
    expect(hwprofileControl.invalid).toBe(false);

    // select type=UCPE, hardware profile control is valid when set
    component['_siteForm'].controls.type.patchValue(SiteConfigSiteType.UCPE);
    hwprofileControl.updateValueAndValidity();
    expect(hwprofileControl.invalid).toBe(true);
    hwprofileControl.patchValue('profile_1');
    hwprofileControl.updateValueAndValidity();
    expect(hwprofileControl.invalid).toBe(false);
  });

  it('should enable/disable WAN IP panel depending on hardware profile selected', () => {
    const expansionPanels: DebugElement[] = fixture.debugElement.queryAll(
      By.directive(MatExpansionPanel)
    );

    const wanIPPanel: MatExpansionPanel = expansionPanels[1].componentInstance;

    expect(wanIPPanel.disabled).toBeTruthy('WAN IP panel is disabled by default');

    component['_siteForm'].patchValue({
      type: SiteConfigSiteType.UCPE,
      profiles: {
        hardware: '1'
      }
    });

    fixture.detectChanges();

    expect(wanIPPanel.disabled).toBeFalsy(
      'WAN IP panel is enabled for UCPE site with hardware profile selected'
    );

    component['_siteForm']
      .get('type')
      .patchValue(SiteConfigSiteType.CLUSTER);

    fixture.detectChanges();

    expect(wanIPPanel.disabled).toBeTruthy(
      'WAN IP panel is disabled for Cluster site'
    );
  });

  it('should update WAN IP interfaces when hardware profile modified', () => {
    const mockHardwareProfiles: HardwareProfile[] = [
      {
        identifier: '1',
        name: 'Profile 1',
        bonds: [
          {
            identifier: '1',
            bond: {
              devices: ['device 1', 'device 2', 'device 3']
            }
          }
        ]
      },
      {
        identifier: '2',
        name: 'Profile 2',
        bonds: [
          {
            identifier: '2',
            bond: {
              devices: ['device 4', 'device 5', 'device 6']
            }
          }
        ]
      }
    ];

    component['_hardwareProfiles'] = mockHardwareProfiles;
    component.onHardwareProfileIDSelectionChange(mockHardwareProfiles[0].identifier);

    expect(component['_bonds'][0].identifier).toBe(
      mockHardwareProfiles[0].bonds[0].identifier,
      'set interfaces'
    );

    expect(component['_bonds'][0].bond.devices[0]).toBe(
      mockHardwareProfiles[0].bonds[0].bond.devices[0],
      'set interface devices'
    );

    component.onHardwareProfileIDSelectionChange(mockHardwareProfiles[1].identifier);

    expect(component['_bonds'][0].identifier).toBe(
      mockHardwareProfiles[1].bonds[0].identifier,
      'update interfaces'
    );

    expect(component['_bonds'][0].bond.devices[0]).toBe(
      mockHardwareProfiles[1].bonds[0].bond.devices[0],
      'update interface devices'
    );
  });

  it('should update profiles after hardware dialog closed', async () => {
    spyOn<any>(component, 'getHardwareProfiles').and.callThrough();
    const el = fixture.debugElement.nativeElement;
    const typeSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=type]'
    }));
    await typeSelect.clickOptions({
      text: 'UCPE'
    });
    fixture.detectChanges();

    const hardwareProfileButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Hardware Profile Manager'
    }));
    await hardwareProfileButton.click();

    const dialog: MatDialogRef<HardwareProfilesDialogComponent> = component['_dialog'].openDialogs[0];

    expect(component['_dialog'].openDialogs.length).toBe(1);
    dialog.afterOpened().subscribe(async () => {
      const closeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
        text: 'Close'
      }));
      await closeButton.click();
    });

    dialog.afterClosed().subscribe(() => {
      expect(component['getHardwareProfiles']).toHaveBeenCalled();
    });
  });

  it('should update software profiles from Software profiles manager dialog', async () => {
    const typeSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName=type]'
    }));

    await typeSelect.clickOptions({
      text: SiteConfigSiteType.UCPE
    });

    const softwareProfileManagerButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Software Profile Manager'
    }));

    await softwareProfileManagerButton.click();

    await documentRootLoader.getHarness(MatDialogHarness);

    const mockSoftwareProfile: SoftwareProfile = {
      identifier: '4',
      name: 'Nefeli Profile',
      description: 'Nefeli\'s profile',
      distroVersion: '4',
      notBefore: new Date(2091, 1, 2, 12, 54)
    };

    const mockProfiles = [...mockSoftwareProfiles.softwareProfiles, mockSoftwareProfile];

    component['_dialog'].openDialogs[0].close(mockProfiles);

    const softwareProfileSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="software"]'
    }));

    await softwareProfileSelect.open();

    const softwareProfileOptions = await softwareProfileSelect.getOptions();

    const softwareProfileOptionTexts = await Promise.all(softwareProfileOptions.map(option => option.getText()));

    const mockSoftwareProfileNames = mockProfiles.map(({ name }) => name);

    expect(softwareProfileOptionTexts).toEqual(mockSoftwareProfileNames, 'render updated Software profile select options');
  });

  it('should not submit site form when profile manager buttons clicked', async () => {
    spyOn<any>(component, 'submitSiteForm').and.callThrough();
    const typeSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=type]'
    }));
    await typeSelect.clickOptions({
      text: 'UCPE'
    });
    fixture.detectChanges();

    const hardwareProfileButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Hardware Profile Manager'
    }));
    await hardwareProfileButton.click();
    expect(component.submitSiteForm).not.toHaveBeenCalled();

    const softwareProfileManagerButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Software Profile Manager'
    }));

    await softwareProfileManagerButton.click();

    expect(component.submitSiteForm).not.toHaveBeenCalled();
  });

  it('should display correct dialog title for edit and add site', () => {
    expect(
      fixture.debugElement.query(By.css('h1')).nativeElement.textContent
    ).toBe('Add New Site');

    component['_data'] = mockSiteDialogData;
    component.ngOnInit();
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('h1')).nativeElement.textContent
    ).toBe('Edit Site');
  });

  it('should fill in site fields when edit site', async () => {
    component['_data'] = mockSiteDialogData;
    fixture.detectChanges();
    component.ngOnInit();
    const hwReqs = httpTestingController.match(`${environment.restServer}/v1/hardware/profiles`);
    hwReqs[0].flush(mockHWProfilesResponse);
    hwReqs[1].flush(mockHWProfilesResponse);
    fixture.detectChanges();

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=name]'
    }));
    expect(await nameInput.getValue()).toBe('Andrew Site');

    const typeSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=type]'
    }));
    expect(await typeSelect.getValueText()).toBe('UCPE');

    const urlInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=url]'
    }));
    expect(await urlInput.getValue()).toBe('https://nefeli.io');

    const provisioningIdInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=provisioningId]'
    }));
    expect(await provisioningIdInput.getValue()).toBe('my-unique-id');

    const hardwareSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=hardware]'
    }));
    expect(await hardwareSelect.getValueText()).toBe('Profile 1');

    const expansionPanels: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-expansion-panel'));
    const connectivityPanel: MatExpansionPanel = expansionPanels[0].componentInstance;
    connectivityPanel.open();
    fixture.detectChanges();

    const vlanInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=vlan]'
    }));
    expect(await vlanInput.getValue()).toBe('4');

    const ipInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=ip]'
    }));
    expect(await ipInput.getValue()).toBe('112.92.1.214');

    const macInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=mac]'
    }));
    expect(await macInput.getValue()).toBe('00-E0-4C-00-07-BE');

    const portInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=vxlanPort]'
    }));
    expect(await portInput.getValue()).toBe('13333');

    const startInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=start]'
    }));
    expect(await startInput.getValue()).toBe('91');

    const endInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=end]'
    }));
    expect(await endInput.getValue()).toBe('3110');

    const wanPanel: MatExpansionPanel = expansionPanels[1].componentInstance;
    wanPanel.open();
    fixture.detectChanges();

    const bondSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: '[formControlName=bond]'
    }));
    expect(await bondSelect.getValueText()).toBe('device1 ; device2 ; device3');

    const typeRadio = await loader.getHarness<MatRadioGroupHarness>(MatRadioGroupHarness.with({
      selector: '[formControlName=type]'
    }));
    expect(await typeRadio.getCheckedValue()).toBe('Static');

    const wanIPInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formgroupname=static] [formcontrolname=ip]'
    }));
    expect(await wanIPInput.getValue()).toBe('112.23.78.1/32');

    const gatewayInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formgroupname=static] [formcontrolname=defaultGateway]'
    }));
    expect(await gatewayInput.getValue()).toBe('255.255.255.255');

    const dnsInputs = await loader.getAllHarnesses<MatInputHarness>(MatInputHarness.with({
      selector: '[formgroupname=static] [formarrayname=dns] input'
    }));
    expect(dnsInputs.length).toBe(2);
    expect(await dnsInputs[0].getValue()).toBe('1.1.1.1');
    expect(await dnsInputs[1].getValue()).toBe('3.3.3.3');
  });

  it('should send update site request', async () => {
    spyOn(component['_alertService'], 'info');
    spyOn(component['_alertService'], 'error');
    component['_data'] = mockSiteDialogData;
    fixture.detectChanges();
    component.ngOnInit();
    const hwReqs = httpTestingController.match(`${environment.restServer}/v1/hardware/profiles`);
    hwReqs[0].flush(mockHWProfilesResponse);
    hwReqs[1].flush(mockHWProfilesResponse);
    fixture.detectChanges();

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button[type=submit]'
    }));
    expect(await submitButton.isDisabled()).toBe(true);

    const nameInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formcontrolname=name]'
    }));
    await nameInput.setValue('New Site Name')
    fixture.detectChanges();
    expect(await submitButton.isDisabled()).toBe(false);
    component.siteForm.markAsDirty();
    await submitButton.click();
    const req = httpTestingController.expectOne({
      url: `${environment.restServer}/v1/sites/andrew_site`,
      method: 'PUT'
    });
    expect(req.request.body.name).toBe('New Site Name');
    expect(Object.keys(req.request.body.wans[0])).toContain('static');

    req.flush({});
    expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_EDIT_SITE_SUCCESS);
    component.siteForm.markAsDirty();
    await submitButton.click();
    const failReq = httpTestingController.expectOne({
      url: `${environment.restServer}/v1/sites/andrew_site`,
      method: 'PUT'
    });
    failReq.flush({ message: 'this ain\'t right' }, {
      status: 422,
      statusText: 'may not change type of an existing site',
    });
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_UPDATE_SITE, 'this ain\'t right');
  });

  it('should validate form data', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`).flush(mockHWProfilesResponse);

    // default form validity
    expect(component.siteForm.valid).toBeFalsy();
    expect(component.siteForm.get('dataPlaneConnectivity').valid).toBeFalsy();
    expect(component.siteForm.get('wanIP').disabled).toBeTruthy();

    // create a 'cluster' site
    component.siteForm.get('name').setValue('site name');
    component.siteForm.get('type').setValue(SiteConfigSiteType.CLUSTER);
    component.siteForm.get('url').setValue('http://192.168.0.1');
    component.siteForm.get('vlanTag').setValue(mockSiteFormData.vlanTag);
    component.siteForm.get('provisioningId').setValue('my-unique-id');
    component.siteForm.updateValueAndValidity();

    // form validity shouldn't have changed
    expect(component.siteForm.valid).toBeFalsy();
    expect(component.siteForm.get('dataPlaneConnectivity').valid).toBeFalsy();
    expect(component.siteForm.get('wanIP').disabled).toBeTruthy();

    // set dataplane connectivity data
    component.siteForm.get('dataPlaneConnectivity.vlan').setValue('vlan-id');
    component.siteForm.get('dataPlaneConnectivity.ip').setValue('192.168.0.1');
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('00-E0-4C-00-07-BE');
    component.siteForm.get('dataPlaneConnectivity.vxlanPort').setValue('13333');
    component.siteForm.get('dataPlaneConnectivity.vniRange.start').setValue('1');
    component.siteForm.get('dataPlaneConnectivity.vniRange.end').setValue('100');
    component.siteForm.updateValueAndValidity();

    // form should now be valid for a 'cluster'
    expect(component.siteForm.valid).toBeTruthy();
    expect(component.siteForm.get('dataPlaneConnectivity').valid).toBeTruthy();
    expect(component.siteForm.get('wanIP').disabled).toBeTruthy();

    // change site to 'ucpe', and form should now be invalid
    component.siteForm.get('type').setValue(SiteConfigSiteType.UCPE);
    component.onSiteTypeSelectionChange(SiteConfigSiteType.UCPE);
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeFalsy();
    expect(component.siteForm.get('dataPlaneConnectivity').valid).toBeTruthy();
    expect(component.siteForm.get('wanIP').enabled).toBeTruthy();
    expect(component.siteForm.get('wanIP').valid).toBeFalsy();

    // set wan ip configuration to static
    component.siteForm.get('wanIP.bond').setValue('5');
    component.siteForm.get('wanIP.type').setValue(WANIPType.Static);
    component.onWanTypeChange({ value: WANIPType.Static } as MatRadioChange);
    component.siteForm.get('wanIP.static.ip').setValue('127.0.0.1/22');
    component.siteForm.get('wanIP.static.defaultGateway').setValue('127.0.0.1');
    component.siteForm.get('wanIP.static.dns').setValue(['127.0.0.1']);
    component.siteForm.updateValueAndValidity();

    // form should now be valid for a ucpe w/ a static wan configuration
    expect(component.siteForm.valid).toBeTruthy();
    expect(component.siteForm.get('dataPlaneConnectivity').valid).toBeTruthy();
    expect(component.siteForm.get('wanIP').valid).toBeTruthy();

    // set wan ip configuration to dhcp
    component.siteForm.get('wanIP.type').setValue(WANIPType.DHCP);
    component.onWanTypeChange({ value: WANIPType.DHCP } as MatRadioChange);
    component.siteForm.get('wanIP.dhcp.dns').setValue(['127.0.0.1']);
    component.siteForm.updateValueAndValidity();

    // form should be valid for a ucpe w/ a dhcp wan configuration
    expect(component.siteForm.valid).toBeTruthy();
    expect(component.siteForm.get('dataPlaneConnectivity').valid).toBeTruthy();
    expect(component.siteForm.get('wanIP').valid).toBeTruthy();

    // validate mac address formats
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('00-E0-4C-00-07-BE');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('00000ABB28FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('00 00 0A BB 28 FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('0000 0ABB 28FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('00:00:0A:BB:28:FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('0000:0ABB:28FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('00.00.0A.BB.28.FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
    component.siteForm.get('dataPlaneConnectivity.mac').setValue('0000.0ABB.28FC');
    component.siteForm.updateValueAndValidity();
    expect(component.siteForm.valid).toBeTruthy();
  });

  it('should open panels if have any invalid fields on submit', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/hardware/profiles`);
    req.flush(mockHWProfilesResponse);
    const form: FormGroup = component['_siteForm'];
    const submitButtonEl: HTMLButtonElement = debugEl.query(
      By.css('button[type=submit]')
    ).nativeElement;
    const invalidSiteFormData = cloneDeep(mockSiteFormData);

    // make dataPlaneConnectivity panel invalid
    invalidSiteFormData.dataPlaneConnectivity.mac = 'bad-mac';

    // make wanIP panel invalid
    invalidSiteFormData.wanIP = {
      type: WANIPType.Static,
      static: {
        ip: 'bad-ip'
      }
    };
    form.patchValue(invalidSiteFormData);
    component.onSiteTypeSelectionChange(SiteConfigSiteType.UCPE);
    form.markAsDirty();
    fixture.detectChanges();

    // verify panels are closed by default
    expect(component['_dataPlanePanel'].expanded).toBe(false);
    expect(component['_wanIPPanel'].expanded).toBe(false);

    // submit form
    submitButtonEl.click();
    fixture.detectChanges();

    // verify panels are open
    expect(component['_dataPlanePanel'].expanded).toBe(true);
    expect(component['_wanIPPanel'].expanded).toBe(true);
  });
});
