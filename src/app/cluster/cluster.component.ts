/**
 * Cluster Component.
 * Router resolver provides clusters list. Rendering clusters tabs.
 */
import { cloneDeep } from 'lodash-es';
import { Subscription, Observable, of, interval, forkJoin, BehaviorSubject } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { filter, switchMap, switchMapTo, take, skipWhile, startWith } from 'rxjs/operators';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { AddSiteDialogComponent, SiteDialogData } from './add-site-dialog/add-site-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { ClusterService, SiteResponse, SiteErrorCodes } from '../home/cluster.service';
import { UserService } from '../users/user.service';
import { Column, ColumnType, BadgeColor, Row, TableActions, TableActionEvent } from '../shared/table/table.component';
import { SiteRow } from '../shared/table/row-types';
import { SitesDataSource, TableSiteRow } from './sites.data-source';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { ZTPPackageDialogData, ZTPPackageDialogComponent } from './ztp-package-dialog/ztp-package-dialog.component';
import { AlertService, AlertType } from '../shared/alert.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { SoftwareProfileSyncStatus } from 'rest_client/pangolin/model/softwareProfileSyncStatus';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';

interface FilterFields {
  siteType: string;
  siteStatus: string;
}

export interface TabLink {
  id: string;
  label: string;
}

const POLL_INTERVAL = 5000;

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

const COLUMNS: Column[] = [
  {
    name: 'select',
    type: ColumnType.SELECT
  },
  {
    name: 'name',
    type: ColumnType.ICON
  },
  {
    name: 'type'
  },
  {
    name: 'version',
    type: ColumnType.LIST
  },
  {
    name: 'scheduledVersion',
    label: 'Scheduled Version',
    type: ColumnType.LINK
  },
  {
    name: 'status',
    label: 'State',
    type: ColumnType.BADGE,
    classes: [
      [SiteConfigSiteStatus.ACTIVE, BadgeColor.green],
      [SiteConfigSiteStatus.STAGED, BadgeColor.blue],
      [SiteConfigSiteStatus.PROVISIONING, BadgeColor.blue],
      [SiteConfigSiteStatus.INACTIVE, BadgeColor.grey]
    ]
  },
  {
    name: 'sync',
    label: 'Last Sync',
    type: ColumnType.TOOLTIP,
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS
  }
];

@Component({
  selector: 'nef-cluster',
  templateUrl: './cluster.component.html',
  styleUrls: ['./cluster.component.less']
})
export class ClusterComponent implements OnInit, OnDestroy {
  private _softwareProfiles: SoftwareProfile[];
  private _dataSource: SitesDataSource;
  private _dialogSubscription: Subscription;
  private _columns: Column[] = COLUMNS;
  private _searchEnabled = false;
  private _selectedSites: string[];
  private _sitePollingSubscription: Subscription;
  private _sitesSoftwareProfileSubscription: Subscription;
  private _removeSitesSubscription: Subscription;
  private _showFilters = false;
  private _filtersForm: FormGroup;
  private _scheduledVersions: string[];
  private _subscription = new Subscription();
  private _softwareProfilesSubject = new BehaviorSubject<SoftwareProfile[]>([]);

  constructor(
    private _router: Router,
    private _softwareProfileService: SoftwareProfilesService,
    private _userService: UserService,
    private _siteService: ClusterService,
    private _activatedRoute: ActivatedRoute,
    private _dialog: MatDialog,
    private _alertService: AlertService,
    private _fb: FormBuilder,
  ) { }

  public ngOnInit() {
    this._dataSource = new SitesDataSource(this._siteService, this.softwareProfiles$);

    const softwareProfilesSubscription = this._softwareProfileService
      .getSoftwareProfiles()
      .subscribe(({ softwareProfiles }) => {
        this._softwareProfiles = softwareProfiles;
        this._scheduledVersions = softwareProfiles.map(
          ({ distroVersion }: SoftwareProfile) => distroVersion
        );
        this._softwareProfilesSubject.next(softwareProfiles);
        this._dataSource.fetch();
      });
    this._subscription.add(softwareProfilesSubscription);

    this.pollSites();
    if (this._activatedRoute.snapshot.data.isAddSite) {
      this.onAddSite();
    }

    this.isSystemAdmin$
      .pipe(take(1))
      .subscribe((isAdmin: boolean) => {
        if (!isAdmin) {
          this._columns = this._columns.filter((column: Column) => column.name !== 'actions');
        }
      });

    this._filtersForm = this._fb.group({
      siteType: '',
      siteStatus: '',
      serverVersions: '',
      scheduledVersion: '',
      softwareProfileSyncStatus: ''
    });

    const subscription = this._filtersForm.valueChanges
      .subscribe((filters: FilterFields) => {
        if (this._filtersForm.dirty) {
          const siteFilter = [];

          Object.keys(filters).map((field: string) => {
            if (filters[field]) {
              siteFilter.push(`eq(${field},${filters[field]})`);
            }
          });
          this._dataSource.filter = siteFilter;
        }
      });

    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    this._subscription?.unsubscribe();
    this._sitePollingSubscription?.unsubscribe();
    this._sitesSoftwareProfileSubscription?.unsubscribe();
    this._subscription.unsubscribe();
  }

  private pollSites() {
    this._sitePollingSubscription?.unsubscribe();

    this._sitePollingSubscription = interval(POLL_INTERVAL)
      .pipe(
        switchMapTo(this._dataSource.fetching$.pipe(
          take(1)
        )),
        filter(fetching => !fetching),
        startWith(undefined as string)
      )
      .subscribe(() => this._dataSource.fetch());
  }

  public onSoftwareProfileAssign(id?: string) {
    this._sitesSoftwareProfileSubscription?.unsubscribe();

    const selectedClusterSites = [];

    const ucpeSiteRequests = this._selectedSites
      .filter(siteID => {
        const {
          config: { name, siteType }
        } = this._dataSource.getSiteById(siteID);

        if (siteType === SiteConfigSiteType.CLUSTER) {
          selectedClusterSites.push(name);
        }

        return siteType === SiteConfigSiteType.UCPE;
      })
      .map(siteID => {
        const site = this._dataSource.getSiteById(siteID);

        site.config.softwareProfileId = id;

        return this._siteService.putSite(siteID, site.config);
      });

    this._sitesSoftwareProfileSubscription = forkJoin(ucpeSiteRequests)
      .subscribe({
        next: () => {
          this._selectedSites = undefined;

          this._dataSource.fetch();

          const alert = selectedClusterSites.length
            ? `${AlertType.INFO_ASSIGN_SITES_SOFTWARE_PROFILE_SUCCESS} ${selectedClusterSites.join(', ')}.`
            : AlertType.INFO_ASSIGN_UCPE_SITES_ONLY_SOFTWARE_PROFILE_SUCCESS;

          this._alertService.info(alert);
        },
        error: ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_ASSIGN_SITES_SOFTWARE_PROFILE, message);
        }
      });
  }

  public onAddSite() {
    const dialogRef = this._dialog.open(AddSiteDialogComponent, DIALOG_CONFIG);

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((_: SiteResponse) => {
        // TODO (anton): make an API request for count after JIRA NEF-4505 resolved.
        this._dataSource.total += 1;
        this._dataSource.fetch();
      });
  }

  public onRowsSelect(rows: TableSiteRow[]) {
    this._selectedSites = rows.map(({ id }) => id);
  }

  public onRowClick(row: Row) {
    row = row as SiteRow;
    this._router.navigate(['sites', row.id]);
  }

  public onFilterButtonClick() {
    this._showFilters = !this._showFilters;
    this._dataSource.filter = [];
    this.onSearchModified([]);
    this._filtersForm.reset();
  }

  public get dataSourceTotal(): number {
    return this._dataSource.total;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get columns(): Column[] {
    return this._columns;
  }

  public get dataSource(): SitesDataSource {
    return this._dataSource;
  }

  public get dataSourceLoading$(): Observable<boolean> {
    return this._dataSource.loading$;
  }

  public get searchRequestInProgress$(): Observable<boolean> {
    return this._searchEnabled ? this._dataSource.fetching$ : of(this._searchEnabled);
  }

  public get selectedSites(): string[] {
    return this._selectedSites;
  }

  public get hasClusterSitesSelectedOnly(): boolean {
    return this._selectedSites.every(siteID => {
      const site = this._dataSource.getSiteById(siteID);

      return site.config.siteType === SiteConfigSiteType.CLUSTER;
    });
  }

  public get softwareProfiles(): SoftwareProfile[] {
    return this._softwareProfiles;
  }

  public get softwareProfiles$(): Observable<SoftwareProfile[]> {
    return this._softwareProfilesSubject.asObservable();
  }

  private generateZTPPackage({ id, name }: SiteRow) {
    const data: ZTPPackageDialogData = { id, name };

    this._dialog.open(ZTPPackageDialogComponent, {
      ...DIALOG_CONFIG,
      width: 'auto',
      disableClose: true,
      data
    });
  }

  public onActionClick(event: TableActionEvent) {
    switch (event.type) {
      case TableActions.Package:
        this.generateZTPPackage(event.data);
        break;
      case TableActions.Edit:
        this.editSite(event.data);
        break;
      case TableActions.Remove:
        this.removeSite(event.data);
        break;
    }
  }

  public onSearchModified(chips: string[]) {
    this._dataSource.searchFilter = chips;
    this._searchEnabled = true;

    this.pollSites();

    if (chips?.length <= 0) {
      this._dataSource.fetching$.pipe(skipWhile(fetching => fetching), take(1))
        .subscribe(() => {
          this._searchEnabled = false;
        });
    }
  }

  private editSite({ id, name }: SiteRow) {
    const site = this._dataSource.getSiteById(id);

    const data: SiteDialogData = {
      site: cloneDeep(site.config)
    };

    const dialog: MatDialogRef<AddSiteDialogComponent, SiteConfig> = this._dialog.open(
      AddSiteDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    // TODO (anton): optimize to update table without a fetch from the server
    this._dialogSubscription = dialog
      .afterClosed()
      .subscribe((editedSite: SiteConfig) => {
        if (editedSite) {
          this._dataSource.fetch();
        }
      });
  }

  private removeSite({ id, name }: SiteRow) {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: `This will remove the site <strong>${name}</strong>.`,
      action: 'Remove Site'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef.afterClosed().pipe(
      filter(Boolean),
      switchMap((_: boolean) => this._siteService.deleteSite(id))
    ).subscribe(
      () => {
        // TODO (anton): optimize to update table without a fetch from the server
        this._dataSource.fetch();
        this._alertService.info(AlertType.INFO_REMOVE_SITE_SUCCESS);
      },
      ({
        error: { errorCode, message }
      }: HttpErrorResponse) => {
        switch (errorCode) {
          case SiteErrorCodes.NotFound:
          case SiteErrorCodes.Unspecified:
            this._alertService.error(AlertType.ERROR_REMOVE_SITE);
            break;
          case SiteErrorCodes.Miscellaneous:
          case SiteErrorCodes.HasRunningServices:
          default:
            this._alertService.error(AlertType.ERROR_REMOVE_SITE, message);
        }
      }
    );
  }

  public onRemoveSites() {
    this._removeSitesSubscription?.unsubscribe();

    const siteRequests = this._selectedSites.map(id => this._siteService.deleteSite(id));

    this._removeSitesSubscription = forkJoin(siteRequests)
      .subscribe({
        next: () => {
          this._selectedSites = undefined;

          this._dataSource.fetch();

          this._alertService.info(AlertType.INFO_REMOVE_SITES_SUCCESS);
        },
        error: ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_REMOVE_SITES, message);
        }
      });
  }

  public get searchEnabled(): boolean {
    return this._searchEnabled;
  }

  public get filtersForm(): FormGroup {
    return this._filtersForm;
  }

  public get siteTypes(): string[] {
    return Object
      .keys(SiteConfigSiteType)
      .filter((key: string) => SiteConfigSiteType[key] !== SiteConfigSiteType.SITETYPENULL);
  }

  public get siteStatuses(): string[] {
    return Object
      .keys(SiteConfigSiteStatus)
      .filter((key: string) => SiteConfigSiteStatus[key] !== SiteConfigSiteStatus.SITESTATUSNULL);
  }

  public get provisioningStatuses(): string[] {
    return Object
      .keys(SoftwareProfileSyncStatus)
      .filter((key: string) => SoftwareProfileSyncStatus[key] !== SoftwareProfileSyncStatus.NOOP);
  }

  public get siteVersions(): string[] {
    return this.dataSource.siteVersions;
  }

  public get scheduledVersions(): string[] {
    return this._scheduledVersions;
  }

  public get showFilters(): boolean {
    return this._showFilters;
  }

  public set showFilters(value: boolean) {
    this._showFilters = value;
  }
}
