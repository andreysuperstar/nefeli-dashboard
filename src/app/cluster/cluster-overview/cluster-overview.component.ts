import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Data, Params } from '@angular/router';
import { Observable, Subscription, EMPTY, combineLatest } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import { orderBy } from 'lodash-es';

import { Cluster, ClusterService } from '../../home/cluster.service';
import { Item } from '../../shared/status-list/status-list.component';

import { AlertService, AlertType } from '../../shared/alert.service';
import { ServersService, Server } from '../servers.service';
import { AddServerDialogComponent } from './add-server-dialog/add-server-dialog.component';
import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { Site } from 'rest_client/pangolin/model/site';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpError } from 'src/app/error-codes';

export interface AddServerDialogData {
  site: SiteConfig;
}
const DEFAULT_PROGRESS = 0;
const DIALOG_CONFIG: MatDialogConfig<AddServerDialogData> = {
  width: '900px'
};

const Config = {
  PollInterval: 5000
};
@Component({
  templateUrl: './cluster-overview.component.html',
  styleUrls: ['./cluster-overview.component.less']
})
export class ClusterOverviewComponent implements OnInit, OnDestroy {

  private _clusterId: string;
  private _cluster: Cluster;
  private _site: Site;
  private _serverItems: Item[];
  private _serverItemsSubscription: Subscription;
  private _serverItemsTimer: number;
  private _subscription = new Subscription();
  private _addServerDialogSubscription: Subscription;

  constructor(
    private _serversService: ServersService,
    private _clusterService: ClusterService,
    private _route: ActivatedRoute,
    private _alert: AlertService,
    private _dialog: MatDialog
  ) { }

  public ngOnInit() {
    const subscription = combineLatest([
      this._route.data,
      this._route.params
    ]).pipe(
      mergeMap<[Data, Params], Observable<[Data, Params, Site]>>(([data, params]) => {
        return this._clusterService
          .getSite(params.id)
          .pipe(
            map(site => [data, params, site])
          );
      })
    ).subscribe(([data, params, site]) => {
      this._site = site;
      this._clusterId = params.id;
      this._cluster = data.clusters.find(cluster => cluster.id === this._clusterId);

      if (site.config.siteStatus === SiteConfigSiteStatus.ACTIVE) {
        this.startServersStream(params.id);
      } else {
        this.clearStreams();
      }
    });
    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
    this.clearStreams();

    if (this._addServerDialogSubscription) {
      this._addServerDialogSubscription.unsubscribe();
    }
  }

  private clearStreams() {
    this._serverItems = [];
    this.stopServersStream();
  }

  private stopServersStream() {
    if (this._serverItemsSubscription) {
      this._serverItemsSubscription.unsubscribe();
    }

    if (this._serverItemsTimer) {
      clearTimeout(this._serverItemsTimer);
      this._serverItemsTimer = undefined;
    }
  }

  private startServersStream(clusterId: string): void {
    this.stopServersStream();

    this._serverItemsSubscription = this._serversService.streamServers(clusterId).pipe(
      map((servers: Server[]): Item[] => {
        this._alert.dismiss(AlertType.ERROR_SITE_CONNECTION_RETRY);

        const serverItems = servers.map((s: Server): Item => ({
          id: s.identifier,
          label: s.name,
          status: s.status,
          progress: this.getProgress(s),
          link: ['servers', s.identifier]
        }) as Item);

        return orderBy(serverItems, 'label');
      }),
      catchError((error: Error) => {
        if ((error as HttpErrorResponse).status !== HttpError.Canceled) {
          this._alert.error(AlertType.ERROR_SITE_CONNECTION_RETRY);
        }

        this._serverItemsTimer = window.setTimeout(() => this.startServersStream(clusterId), Config.PollInterval);
        return EMPTY;
      })
    ).subscribe((serverItems: Item[]) => {
      this._serverItems = serverItems;
    });
  }

  private getProgress(item: Server): number {
    // TODO(anton): add support for the actual progress received from server
    return DEFAULT_PROGRESS;
  }

  public get serverItems(): Item[] {
    return this._serverItems;
  }

  public get clusterId(): string {
    return this._clusterId;
  }

  public get cluster(): Cluster {
    return this._cluster;
  }

  public onAddServerClick(): void {
    const data: AddServerDialogData = {
      site: this._site.config
    };

    const dialogRef = this._dialog.open(AddServerDialogComponent, { ...DIALOG_CONFIG, data });

    if (this._addServerDialogSubscription) {
      this._addServerDialogSubscription.unsubscribe();
    }

    this._addServerDialogSubscription = dialogRef
      .afterClosed()
      .subscribe((server: Server) => {
        this.startServersStream(this._clusterId);
      });
  }

  public get site(): Site {
    return this._site;
  }

  public get SiteConfigSiteStatus(): any {
    return SiteConfigSiteStatus;
  }
}
