import { of } from 'rxjs';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { ClusterOverviewComponent } from './cluster-overview.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ClusterService } from '../../home/cluster.service';
import { ServersService } from '../servers.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { Cluster } from './../../home/cluster.service';
import { environment } from '../../../environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { StatusListComponent, Status } from 'src/app/shared/status-list/status-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AlertService } from 'src/app/shared/alert.service';
import { Site } from 'rest_client/pangolin/model/site';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { By } from '@angular/platform-browser';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { AddServerDialogComponent } from './add-server-dialog/add-server-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { DebugElement } from '@angular/core';
import { RESTServerList } from 'rest_client/pangolin/model/rESTServerList';

const CLUSTERS: Cluster[] = [
  {
    id: '-1',
    name: 'Cluster A'
  },
  {
    id: '-2',
    name: 'Cluster B'
  }
];
const CURRENT_CLUSTER_ID = '-1';

let mockActivatedRoute = {
  data: of({ clusters: CLUSTERS }),
  params: of({ id: CURRENT_CLUSTER_ID }),
  snapshot: {
    url: [
      {
        path: '/sites/-1'
      }
    ]
  }
};

describe('ClusterOverviewComponent', () => {
  let component: ClusterOverviewComponent;
  let fixture: ComponentFixture<ClusterOverviewComponent>;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ClusterOverviewComponent,
        AvatarComponent,
        StatusListComponent,
        AddServerDialogComponent
      ],
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        RouterTestingModule,
        MatCardModule,
        MatDividerModule,
        MatProgressBarModule,
        MatListModule,
        MatSnackBarModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatDialogModule,
        MatExpansionModule,
        MatIconModule,
        MatProgressSpinnerModule
      ],
      providers: [
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute
        },
        AlertService,
        ClusterService,
        ServersService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClusterOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const fetchServers = (errorResponse = false) => {
    const request = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/-1/servers`);
    const flushData: RESTServerList = {
      servers: [
        {
          identifier: 'sylvester',
          name: 'sylvester',
          status: Status.GOOD
        }, {
          identifier: 'felix',
          name: 'felix',
          status: Status.WARNING
        }, {
          identifier: 'fluffy',
          name: 'fluffy',
          status: Status.CRITICAL
        }
      ]
    };

    if (errorResponse) {
      request.flush({}, {
        status: 400,
        statusText: 'mock error'
      })
    } else {
      request.flush(flushData);
    }
  };

  const fetchSite = (status = SiteConfigSiteStatus.ACTIVE) => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1`);

    const mockSiteResponse: Site = {
      config: {
        dataplaneVlanTag: 0,
        name: 'Eric\'s Site',
        identifier: 'eric_site',
        publicIp: '127.0.0.1',
        privateIp: '127.0.0.1',
        gatewayMac: '00:00:00:00:00:01',
        inboundVxlanPort: 13333,
        mgmtUrl: '',
        siteStatus: status,
        siteType: SiteConfigSiteType.UCPE,
        vniRangeEnd: 1,
        vniRangeStart: 0
      }
    };

    request.flush(mockSiteResponse);
  };

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a site heading', () => {
    fetchSite();
    fixture.detectChanges();

    const siteHeadingDe: DebugElement = fixture.debugElement.query(By.css('h1'));

    expect(siteHeadingDe).toBeDefined('render a site heading');

    expect(
      (siteHeadingDe.nativeElement.textContent.trim() as string).endsWith('Eric\'s Site')
    ).toBeTruthy('render a site name');

    const avatarEl: DebugElement = siteHeadingDe.query(By.css('nef-avatar'));

    expect(avatarEl).toBeDefined('render a site avatar');
    expect(avatarEl.nativeElement.textContent.trim()).toBe(
      'E',
      'render a site avatar capital letter'
    );
  });

  it('should display servers list', () => {
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelectorAll('.status-list').length).toBe(1);
  });

  it('should display servers for the current cluster', () => {
    fetchSite();
    fetchServers();
    fixture.detectChanges();
    const serverList = fixture.debugElement.nativeElement.querySelector('.status-list');
    const servers = serverList.querySelectorAll('[mat-list-item]');
    expect(serverList.querySelector('mat-card-header').textContent).toBe('Servers');

    // items should be listed in alphabetical order
    expect(servers[0].querySelector('.list-item-text').textContent).toBe('felix');
    expect(servers[1].querySelector('.list-item-text').textContent).toBe('fluffy');
    expect(servers[2].querySelector('.list-item-text').textContent).toBe('sylvester');
  });

  it('should display correct status for server', () => {
    fetchSite();
    fetchServers();
    fixture.detectChanges();

    const statusIndicators = fixture.debugElement.nativeElement.querySelector('.status-list').querySelectorAll('.status-indicator');

    // felix
    expect(statusIndicators[0].classList.contains('warning')).toBe(true);

    // fluffy
    expect(statusIndicators[1].classList.contains('critical')).toBe(true);

    // sylvester
    expect(statusIndicators[2].classList.contains('good')).toBe(true);
  });

  it('should not fetch servers if site is not active', () => {
    fetchSite(SiteConfigSiteStatus.STAGED);
    httpTestingController.verify();
  });

  it('should prompt to add server', () => {
    fetchSite();
    fixture.detectChanges();

    const serverList = fixture.debugElement.query(By.css('nef-status-list[title="Servers"]'));
    serverList.triggerEventHandler('handleAddClick', {});
    fixture.detectChanges();

    const addServerDialog = document.querySelector('mat-dialog-content');

    expect(addServerDialog).toBeDefined();
  });

  it('should retry if requests for servers fail', fakeAsync(() => {
    spyOn(component['_alert'], 'error');
    fetchSite();

    // trigger errors when fetching for servers
    fetchServers(true);

    // verify error alert is displayed
    expect(component['_alert'].error).toHaveBeenCalled();

    // servers poller should resume
    tick(5000);
    httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/servers`);

    // cancel pending tasks
    component['clearStreams']();
  }));
});
