import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RequestUrlInterceptor } from '../../../http-interceptors/request-url.interceptor';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { environment } from '../../../../environments/environment';
import { ServersService } from '../../servers.service';
import { AddServerDialogComponent } from './add-server-dialog.component';
import { AlertService } from 'src/app/shared/alert.service';
import { AddServerDialogData } from '../cluster-overview.component';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { RESTServer } from 'rest_client/pangolin/model/rESTServer'

const dialogSpy = {
  close: jasmine.createSpy('close')
};

const mockMatDialogData: AddServerDialogData = {
  site: {
    identifier: '-1'
  }
};

describe('AddServerDialogComponent', () => {
  let component: AddServerDialogComponent;
  let fixture: ComponentFixture<AddServerDialogComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatExpansionModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatInputModule,
        MatCardModule,
        MatSelectModule,
        MatButtonModule,
        MatIconModule,
        MatProgressSpinnerModule
      ],
      declarations: [AddServerDialogComponent],
      providers: [
        AlertService,
        ServersService,
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockMatDialogData
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddServerDialogComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    dialogSpy.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(el.querySelector('[mat-dialog-title]').textContent).toBe('Add Server');
  });

  it('should display add server form', () => {
    expect(el.querySelector('#server-form')).not.toBeNull();
  });

  it('should have form controls', () => {
    const nameInput = el.querySelector('.mat-input-element[formcontrolname=serverName]');
    expect(nameInput).not.toBeNull();
    const ipInput = el.querySelector('.mat-input-element[formcontrolname=ip]');
    expect(ipInput).not.toBeNull();
    const usernameInput = el.querySelector('.mat-input-element[formcontrolname=username]');
    expect(usernameInput).not.toBeNull();
  });

  it('should add data path bonded NICs', () => {
    const addButton: HTMLButtonElement = el.querySelector('button[title="Add Data Path Bonded NIC"]');
    addButton.click();
    fixture.detectChanges();
    const nicEl = el.querySelector('.data-path-bonded-nic');
    expect(nicEl).not.toBeNull();

    const socketInput = nicEl.querySelector('.mat-input-element[formcontrolname=socket]');
    const driverInput = nicEl.querySelector('.mat-input-element[formcontrolname=driver]');
    const throughpuInput = nicEl.querySelector('.mat-input-element[formcontrolname=throughputGbps]');
    const modeSelect = nicEl.querySelector('.mat-select[formcontrolname=mode]');
    const hashSelect = nicEl.querySelector('.mat-select[formcontrolname=hash]');
    const pciInput = nicEl.querySelector('.mat-input-element[formcontrolname=pciAddress]');
    const macInput = nicEl.querySelector('.mat-input-element[formcontrolname=mac]');
    const subnetInput = nicEl.querySelector('mat-select[formcontrolname=subnet]');
    expect(socketInput).not.toBeNull();
    expect(driverInput).not.toBeNull();
    expect(throughpuInput).not.toBeNull();
    expect(modeSelect).not.toBeNull();
    expect(hashSelect).not.toBeNull();
    expect(pciInput).not.toBeNull();
    expect(macInput).not.toBeNull();
    expect(subnetInput).not.toBeNull();
  });

  it('should add and remove interfaces to data path bonded NICs', () => {
    const addButton: HTMLButtonElement = el.querySelector('button[title="Add Data Path Bonded NIC"]');
    addButton.click();
    fixture.detectChanges();
    const nicEl = el.querySelector('.data-path-bonded-nic');

    const pciInput: HTMLInputElement = nicEl.querySelector('.mat-input-element[formcontrolname=pciAddress]');
    pciInput.value = '0';
    const keyupEvent: Event = new KeyboardEvent('keyup');
    pciInput.dispatchEvent(keyupEvent);
    fixture.detectChanges();
    const pciInputs = nicEl.querySelectorAll('.mat-input-element[formcontrolname=pciAddress]');
    expect(pciInputs.length).toBe(2);

    let macInputs = <NodeListOf<HTMLInputElement>> nicEl.querySelectorAll('.mat-input-element[formcontrolname=mac]')
    macInputs[1].value = '1';
    macInputs[1].dispatchEvent(keyupEvent);
    fixture.detectChanges();
    macInputs = nicEl.querySelectorAll('.mat-input-element[formcontrolname=mac]');
    expect(macInputs.length).toBe(3);

    const removeInterfaceButton: HTMLButtonElement = nicEl.querySelector('button[title="Remove Interface"]');
    removeInterfaceButton.click();
    fixture.detectChanges();
    macInputs = nicEl.querySelectorAll('.mat-input-element[formcontrolname=mac]');
    expect(macInputs.length).toBe(2);
  });

  it('should delete data path bonded NIC', () => {
    const addButton: HTMLButtonElement = el.querySelector('button[title="Add Data Path Bonded NIC"]');
    addButton.click();
    fixture.detectChanges();

    const nicEl = el.querySelector('.data-path-bonded-nic');
    const removeButton: HTMLButtonElement = nicEl.querySelector('button[title="Remove NIC"]');
    removeButton.click();
    fixture.detectChanges();
    const nicEls = el.querySelectorAll('.data-path-bonded-nic');
    expect(nicEls.length).toBe(0);
  });

  it('should submit form', () => {
    spyOn(component, 'submitServerForm').and.callThrough();
    const submitButtonEl: HTMLButtonElement = el.querySelector('button[type=submit]');
    component.serverForm.markAsDirty();
    submitButtonEl.click();
    fixture.detectChanges();
    expect(component['submitServerForm']).toHaveBeenCalled();
  });

  it('should create server on submit form', () => {
    const serverNameInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=serverName]');
    const ipInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=ip]');
    const usernameInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=username]');
    const submitButtonEl: HTMLButtonElement = el.querySelector('button[type=submit]');
    fixture.detectChanges();

    serverNameInput.value = 'test-host';
    ipInput.value = '127.0.0.1';
    usernameInput.value = 'ecarino';

    serverNameInput.dispatchEvent(new Event('input'));
    ipInput.dispatchEvent(new Event('input'));
    usernameInput.dispatchEvent(new Event('input'));
    const keyupEvent: Event = new KeyboardEvent('keyup');

    component.serverForm.markAsDirty();
    submitButtonEl.click();
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/sites/${mockMatDialogData.site.identifier}/servers`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.ip).toBe('127.0.0.1');
    expect(req.request.body.username).toBe('ecarino');
    expect(req.request.body.name).toBe('test-host');
    req.flush({} as RESTServer)
  });
});
