
import { Subscription } from 'rxjs';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, FormArray, ValidatorFn } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddServerDialogData } from '../cluster-overview.component';
import { AlertService, AlertType } from '../../../shared/alert.service';
import { HashFunction, BondedNicModes, ServersService, VALID_SERVER_NAME_FORMAT } from '../../servers.service';
import { AddMachine } from 'rest_client/pangolin/model/addMachine';
import { DatapathBondedNIC } from 'rest_client/pangolin/model/datapathBondedNIC';
import { NIC } from 'rest_client/pangolin/model/nIC';
import { DPDKInfo } from 'rest_client/pangolin/model/dPDKInfo';
import { DPDKInfoSubnet } from 'rest_client/pangolin/model/dPDKInfoSubnet';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';

interface FormDatapathNic {
  driver: string;
  mac: string;
  pciAddress: string;
  socket: number;
  througputGbps: string;
  subnet: DPDKInfoSubnet;
}

interface FormControlPathBondedNic {
  interfaces: Array<string>;
  name: string;
}

interface FormInterface {
  pciAddress: string;
  mac: string;
}

interface FormDatapathBondedNic {
  driver: string;
  hash: string;
  interfaces: Array<FormInterface>;
  mode: string;
  socket: number;
  throughputGbps: string;
  subnet: DPDKInfoSubnet;
}

@Component({
  selector: 'nef-add-server-dialog',
  templateUrl: './add-server-dialog.component.html',
  styleUrls: ['./add-server-dialog.component.less']
})
export class AddServerDialogComponent implements OnInit {

  private _serverForm: FormGroup;
  private _serverSubscription: Subscription;
  private _isProcessing = false;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<AddServerDialogComponent>,
    private _serverService: ServersService,
    private _alertService: AlertService,
    @Inject(MAT_DIALOG_DATA) private _data: AddServerDialogData,
  ) { }

  public ngOnInit() {
    this.initServerForm();
  }

  private initServerForm() {
    this._serverForm = this._fb.group(
      {
        serverName: ['', Validators.required],
        ip: ['', Validators.required],
        username: ['', Validators.required],
        dataPathBondedNics: this._fb.array([])
      }
    );
  }

  public get serverForm(): FormGroup {
    return this._serverForm;
  }

  public get dataPathBondedNICControls(): AbstractControl[] {
    return (this._serverForm.get('dataPathBondedNics') as FormArray).controls;
  }

  public addDataPathBondedNIC() {
    const control: FormArray = this.serverForm.controls.dataPathBondedNics as FormArray;

    control.push(
      this._fb.group({
        socket: [''],
        driver: [''],
        throughputGbps: [''],
        mode: [''],
        hash: [''],
        subnet: [''],
        interfaces: this._fb.array([
          this._fb.group({
            pciAddress: [''],
            mac: ['']
          })
        ])
      })
    );
  }

  public submitServerForm() {
    const {
      invalid,
      pristine,
      value: {
        serverName,
        ip,
        username,
        dataPathBondedNics
      }
    } = this.serverForm;

    if (invalid || pristine) {
      return;
    }

    if (this._serverSubscription) {
      this._serverSubscription.unsubscribe();
    }

    const datapathBondedNICs: DatapathBondedNIC[] = dataPathBondedNics.map(
      (dataPathBondedNic: FormDatapathBondedNic): DatapathBondedNIC => {
        return {
          dpdk: {
            bandwidth: dataPathBondedNic.throughputGbps,
            driver: dataPathBondedNic.driver,
            socket: dataPathBondedNic.socket.toString(),
            subnet: dataPathBondedNic.subnet
          },
          mode: dataPathBondedNic.mode,
          nics: dataPathBondedNic.interfaces
            .filter((inf: FormInterface) => inf.mac && inf.pciAddress)
            .map((inf: FormInterface): NIC => {
              return {
                mac: inf.mac,
                pci: inf.pciAddress
              };
            }),
          policy: dataPathBondedNic.hash
        };
      });

    const machine: AddMachine = {
      datapathBondedNICs,
      name: serverName,
      ip,
      username
    };

    this._isProcessing = true;
    this._serverSubscription = this._serverService
      .postServer(this._data.site.identifier, machine)
      .subscribe(
        (response: string) => {
          this._isProcessing = false;
          this._alertService.info(AlertType.INFO_CREATE_SERVER_SUCCESS);
          this._dialogRef.close(response);
        },
        () => {
          this._isProcessing = false;
          this._alertService.error(AlertType.ERROR_CREATE_SERVER);
        }
      );
  }

  public deleteDataPathBondedNIC(index: number) {
    const control: FormArray = this.serverForm.controls.dataPathBondedNics as FormArray;
    control.removeAt(index);
  }

  public addDataBondedInterface(
    pciEvent: Event,
    macEvent: Event,
    control: FormArray,
    index: number
  ) {
    let pci = '';
    let mac = '';

    if (pciEvent) {
      const inputEl: HTMLInputElement = pciEvent.target as HTMLInputElement;

      pci = inputEl.value;
    }

    if (macEvent) {
      const inputEl: HTMLInputElement = macEvent.target as HTMLInputElement;

      mac = inputEl.value;
    }

    if ((pci !== '' || mac !== '') && control.length - index === 1) {
      control.push(
        this._fb.group({
          pciAddress: [''],
          mac: ['']
        })
      );
    }
  }

  public deleteDataPathBondedInterface(index: number, control: FormArray) {
    control.removeAt(index);
  }

  public get hashFunction(): typeof HashFunction {
    return HashFunction;
  }

  public get bondedNicModes(): typeof BondedNicModes {
    return BondedNicModes;
  }

  public get dpdkSubnets(): Array<string> {
    const expUcpeIndex = 3;

    const subnets = Object.values(DPDKInfoSubnet).filter((subnet: string, index: number) => {
      if (this._data.site) {
        if (this._data.site.siteType === SiteConfigSiteType.CLUSTER && index < expUcpeIndex) {
          return subnet;
        } else if (this._data.site.siteType === SiteConfigSiteType.UCPE && index >= expUcpeIndex) {
          return subnet;
        }
      }
    });

    return subnets;
  }

  public get isProcessing(): boolean {
    return this._isProcessing;
  }
}
