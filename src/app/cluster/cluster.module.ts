import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';

import { SharedModule } from './../shared/shared.module';
import { HardwareProfilesModule } from '../hardware-profiles/hardware-profiles.module';

import { ClusterRoutingModule } from './cluster-routing.module';

import { ClusterComponent } from './cluster.component';
import { ClusterOverviewComponent } from './cluster-overview/cluster-overview.component';
import { ServerInfoComponent } from './server-info/server-info.component';
import { SocketComponent } from './socket/socket.component';
import { MemoryMeterComponent } from './memory-meter/memory-meter.component';
import { ChartsModule } from '../charts/charts.module';
import { TenantService } from '../tenant/tenant.service';
import { AddServerDialogComponent } from './cluster-overview/add-server-dialog/add-server-dialog.component';
import { ZTPPackageDialogComponent } from './ztp-package-dialog/ztp-package-dialog.component';
import { AddSiteDialogComponent } from './add-site-dialog/add-site-dialog.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { SoftwareProfilesModule } from '../software-profiles/software-profiles.module';

@NgModule({
  imports: [
    CommonModule,
    ClipboardModule,
    FlexLayoutModule,
    MatMenuModule,
    MatTabsModule,
    MatCardModule,
    HardwareProfilesModule,
    ClusterRoutingModule,
    SharedModule,
    ChartsModule,
    MatDialogModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatExpansionModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    OverlayModule,
    SharedModule,
    SoftwareProfilesModule
  ],
  declarations: [
    ClusterComponent,
    ClusterOverviewComponent,
    ServerInfoComponent,
    SocketComponent,
    MemoryMeterComponent,
    AddServerDialogComponent,
    AddSiteDialogComponent,
    ZTPPackageDialogComponent,
  ],
  providers: [
    TenantService
  ]
})
export class ClusterModule { }
