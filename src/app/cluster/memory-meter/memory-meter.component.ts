import { Component, Input, OnInit } from '@angular/core';

import { NFMemory } from '../socket/socket.component';

export const CELLS = 22;

export interface NFMemoryCell {
  nFIndex: number;
  classes: NFMemoryCellClasses;
}

export interface NFMemoryCellClasses {
  [key: string]: boolean;
}

@Component({
  selector: 'nef-memory-meter',
  templateUrl: './memory-meter.component.html',
  styleUrls: ['./memory-meter.component.less']
})
export class MemoryMeterComponent implements OnInit {

  @Input('totalMemory') public set totalMemory(val: number) {
    this._totalMemory = val;
  }

  @Input('nFsMemory') public set nFsMemory(val: NFMemory[]) {
    this._nFsMemory = val;
  }

  private _totalMemory: number;
  private _nFsMemory: NFMemory[];
  private _cells: Partial<NFMemoryCell>[];

  constructor() { }

  public ngOnInit() {
    this.setCells();
  }

  private setCells(): void {
    const nFsCells: NFMemoryCell[] = this.nFsCells;

    this._cells = Array.from(Array(CELLS), (_, cellIndex: number): Partial<NFMemoryCell> =>
      nFsCells[cellIndex] as NFMemoryCell || {}
    );
  }

  private get nFsCells(): NFMemoryCell[] {
    return this._nFsMemory
      .reduce((nFsCells: NFMemoryCell[], nFMemory: NFMemory, nFIndex: number) => {
        const { memory, invalid } = nFMemory;

        if (memory) {
          const size: number = this.calculateNFCells(memory);
          const classes: NFMemoryCellClasses = this.getNFCellClasses(nFIndex, undefined, invalid);

          const nFCell: NFMemoryCell = { nFIndex, classes };

          const nFCells: NFMemoryCell[] = Array(size).fill(nFCell);

          return [...nFsCells, ...nFCells];
        }

        return nFsCells;
      }, []);
  }

  private calculateNFCells(nFMemory: number): number {
    const rate: number = nFMemory / this._totalMemory;

    const cellsFloat: number = CELLS * rate;
    const cells: number = Math.round(cellsFloat);

    return cells ? cells : 1;
  }

  public get cells(): Partial<NFMemoryCell>[] {
    return this._cells;
  }

  private getNFCellClasses(nFIndex: number, activeNFIndex?: number, isInvalid = false): NFMemoryCellClasses {
    let isActive = false;
    let isMuted = false;

    if (activeNFIndex !== undefined) {
      const hasIndex = nFIndex === activeNFIndex;

      isActive = hasIndex;
      isMuted = !hasIndex;
    }

    const cellClasses = {
      active: isActive,
      muted: isMuted,
      'cell-invalid': isInvalid
    };

    if (nFIndex !== undefined) {
      const color = this._nFsMemory[nFIndex].color;

      cellClasses[color] = true;
    }

    return cellClasses;
  }

  public updateCellsClasses(activeNFIndex?: number): void {
    this._cells = this._cells
      .map((cell: Partial<NFMemoryCell>): Partial<NFMemoryCell> => {
        const { nFIndex } = cell;
        const invalid = cell.classes && cell.classes['cell-invalid'];

        const classes: NFMemoryCellClasses = this.getNFCellClasses(nFIndex, activeNFIndex, invalid);

        return { ...cell, classes };
      });
  }

}
