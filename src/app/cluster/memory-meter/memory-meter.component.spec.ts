import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NFMemory } from '../socket/socket.component';

import { CELLS, MemoryMeterComponent, NFMemoryCell } from './memory-meter.component';

describe('MemoryMeterComponent', () => {
  let component: MemoryMeterComponent;
  let fixture: ComponentFixture<MemoryMeterComponent>;

  let memoryMeterDe: DebugElement;
  let memoryMeterEl: HTMLElement;
  let cellEls: NodeListOf<HTMLDivElement>;

  const mockTotalMemory = 201392652288;

  const mockNFsMemory: NFMemory[] = [
    {
      memory: 4294967296,
      color: 'purple'
    },
    {
      memory: 121392652288,
      color: 'red',
      invalid: true
    },
    {
      memory: 0,
      color: 'green'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MemoryMeterComponent]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoryMeterComponent);
    component = fixture.componentInstance;

    memoryMeterDe = fixture.debugElement;
    memoryMeterEl = memoryMeterDe.nativeElement;

    component.totalMemory = mockTotalMemory;
    component.nFsMemory = mockNFsMemory;

    fixture.detectChanges();

    cellEls = memoryMeterEl.querySelectorAll('.cell');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set #_totalMemory and #_nFsMemory', () => {
    expect(component['_totalMemory']).toEqual(mockTotalMemory, 'valid total memory');
    expect(component['_nFsMemory']).toBe(mockNFsMemory, 'valid NFs memory');

    expect(component['_nFsMemory'][0].memory).toEqual(mockNFsMemory[0].memory, 'valid NF memory');
    expect(component['_nFsMemory'][1].memory).toEqual(mockNFsMemory[1].memory, 'valid empty NF memory');
    expect(component['_nFsMemory'][2].color).toBe(mockNFsMemory[2].color, 'valid NF color');
  });

  it('#calculateNFCells should calculate NF cells quantity', () => {
    const calculateNFCells: Function = component['calculateNFCells'].bind(component);

    expect(calculateNFCells(mockNFsMemory[0].memory)).toEqual(1, 'valid NF cell quantity');
    expect(calculateNFCells(mockNFsMemory[1].memory)).toEqual(13, 'valid NF cells quantity');
  });

  it('#nFsCells should return valid overall NF cells quantity', () => {
    expect(component['nFsCells'].length).toBe(14, 'valid NF cells length');
  });

  it('should return valid #cells', () => {
    const cells: Partial<NFMemoryCell>[] = component.cells;

    expect(cells.length).toEqual(CELLS, 'valid cells length');

    expect(cells[0].nFIndex).toEqual(0, 'valid cell NF index for 1st NF');
    expect(cells[0].classes.hasOwnProperty(mockNFsMemory[0].color)).toBeTruthy('valid cell color for 1st NF');

    expect(cells[1].nFIndex).toEqual(1, 'valid cell NF index for 2nd NF start');
    expect(cells[1].classes.hasOwnProperty(mockNFsMemory[1].color)).toBeTruthy('valid cell color for 2st NF');
    expect(cells[13].nFIndex).toEqual(1, 'valid cell NF index for 2nd NF end');

    expect(Object.keys(cells[14]).length).toBe(0, 'valid empty cell');
    expect(Object.keys(cells[CELLS - 1]).length).toBe(0, 'valid last empty cell');
  });

  it('cell elements should have proper classes', () => {
    expect(cellEls[0].classList).toContain('cell', 'cell element contains \'cell\' class');
    expect(cellEls[CELLS - 1].classList.toString()).toBe('cell', 'last cell element contains only \'cell\' class');

    expect(cellEls[0].classList).toContain(mockNFsMemory[0].color, `1st NF cell element contains \'${mockNFsMemory[0].color}\' class`);
    expect(cellEls[1].classList).toContain(mockNFsMemory[1].color, `2nd NF cell element contains \'${mockNFsMemory[1].color}\' class`);
  });

  it('cell elements should have proper classes when there\'s active NF or not', () => {
    const updateCellsClasses: Function = component.updateCellsClasses.bind(component);

    updateCellsClasses(0);

    fixture.detectChanges();

    cellEls = memoryMeterEl.querySelectorAll('.cell');

    expect(cellEls[0].classList).toContain('active', '1st NF cell element contains \'active\' class');
    expect(cellEls[1].classList).not.toContain('active', '2nd NF cell element does not contain \'active\' class');
    expect(cellEls[17].classList).not.toContain('active', 'empty cell element does not contain \'active\' class');

    expect(cellEls[1].classList).toContain('muted', '2nd NF cell element contain \'active\' class');
    expect(cellEls[17].classList).toContain('muted', 'empty cell element does not contain \'active\' class');

    updateCellsClasses();

    fixture.detectChanges();

    cellEls = memoryMeterEl.querySelectorAll('.cell');

    expect(cellEls[0].classList).not.toContain('active', '1st NF cell element does not contain \'active\' class');

    expect(cellEls[1].classList).not.toContain('muted', '2nd NF cell element does not contain \'muted\' class');
    expect(cellEls[17].classList).not.toContain('muted', 'empty cell element does not contain \'muted\' class');
  });

  it('should properly render invalid memory cells', () => {
    expect(cellEls[0].classList).not.toContain('cell-invalid');
    expect(cellEls[1].classList).toContain('cell-invalid');
    expect(cellEls[2].classList).toContain('cell-invalid');
    expect(cellEls[3].classList).toContain('cell-invalid');
    expect(cellEls[4].classList).toContain('cell-invalid');
    expect(cellEls[5].classList).toContain('cell-invalid');
    expect(cellEls[6].classList).toContain('cell-invalid');
    expect(cellEls[7].classList).toContain('cell-invalid');
    expect(cellEls[8].classList).toContain('cell-invalid');
    expect(cellEls[9].classList).toContain('cell-invalid');
    expect(cellEls[10].classList).toContain('cell-invalid');
    expect(cellEls[11].classList).toContain('cell-invalid');
    expect(cellEls[12].classList).toContain('cell-invalid');
    expect(cellEls[13].classList).toContain('cell-invalid');
    expect(cellEls[14].classList).not.toContain('cell-invalid');
  });
});
