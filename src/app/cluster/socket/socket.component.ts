import { Component, Input, OnInit } from '@angular/core';

import { Socket, ServerNF, HugePage } from '../servers.service';
import { CellGroup, CellPopupField } from '../../shared/cell-grid/cell-grid.component';

import { ByteConverterPipe, ConverterUnit } from '../../pipes/byte-converter.pipe';

import { GetNFColor, GetNFNodeName } from '../../pipeline/network-function.model';
import { PipelineNamePipe } from '../../pipes/pipeline-name.pipe';

export interface NFMemory {
  memory: number;
  color: string;
  invalid?: boolean;
}

@Component({
  selector: 'nef-socket',
  templateUrl: './socket.component.html',
  styleUrls: ['./socket.component.less']
})
export class SocketComponent implements OnInit {

  private _socket: Socket;
  private _titleIndex: number;
  private _coresRate: number;
  private _nFsMemory: NFMemory[];
  private _memoryRate: number;
  private _popupFields: CellPopupField[];

  @Input('socket') public set socket(val: Socket) {
    this._socket = val;
  }

  constructor(
    private _converter: ByteConverterPipe,
    private _pipelineNameService: PipelineNamePipe) { }

  public ngOnInit() {
    this._titleIndex = this._socket.id + 1;

    this.setCoresRate();
    this.setNFsMemory();
    this.setMemoryRate();
  }

  public get socket(): Socket {
    return this._socket;
  }

  public get cores(): number[] {
    return this._socket.cores;
  }

  /**
   * Order of adding cores should be aligned with order of memory.
   * Hightlight interaction between cores and memory component relies on it.
   */
  public get activeCores(): CellGroup[] {
    let activeCores: CellGroup[] = [];

    const nfCores: CellGroup[] = this._socket.nfs.reduce<CellGroup[]>(
      (cores: CellGroup[], nF: ServerNF, index: number): CellGroup[] => {
        const nFMemory: number = this.getNfMemory(nF);

        const {
          metadata: {
            nfType,
            name: serverNFName,
            logicalNf,
            tenant,
            service,
            draining = false
          },
          resources: { coresBySocket, cpuOversubscriptionFactor }
        } = nF;

        const name = GetNFNodeName(logicalNf, nfType);
        const [memory, unit]: [number, string] = this._converter.convert(
          nFMemory,
          ConverterUnit.BYTES,
          0
        );

        const socketCores: number[] = coresBySocket[this._socket.id].cores;

        return [
          ...cores,
          {
            indices: socketCores,
            color: GetNFColor(name, service, index),
            invalid: draining,
            popupBadge: {
              text: draining ? 'DRAINING' : undefined
            },
            popupFields: [
              {
                header: 'Instance Name',
                description: name
              },
              {
                header: 'Network Function',
                description: serverNFName
              },
              {
                header: 'Tenant',
                description: tenant
              },
              {
                header: 'Service',
                description: this._pipelineNameService.transform(service)
              },
              {
                header: 'Cores Used',
                description: socketCores.length
              },
              {
                header: 'Over-Subscription Ratio',
                description: !cpuOversubscriptionFactor
                  || cpuOversubscriptionFactor === '0'
                  ? '1:1'
                  : `${cpuOversubscriptionFactor}:1`
              },
              {
                header: 'Memory Used',
                description: `${memory} ${unit}`
              }
            ]
          }
        ] as CellGroup[];

      },
      []
    );

    // populate NF cores
    activeCores = activeCores.concat(nfCores);

    if (this._socket.system) {
      const systemCores: CellGroup = {
        indices: this._socket.system.cores,
        color: 'dark-grey'
      };

      // populate system cores
      activeCores.push(systemCores);
    }

    return activeCores;
  }

  private get usedCores(): number {
    return this.activeCores
      .reduce((usedCores: number, nFCores: CellGroup): number => {
        return usedCores + nFCores.indices.length;
      }, 0);
  }

  private calculateRate(total: number, used: number): number {
    const available: number = total - used;

    const rate: number = available / total;

    return rate;
  }

  private setCoresRate(): void {
    const cores = this.cores.length;

    this._coresRate = this.calculateRate(cores, this.usedCores);
  }

  /**
   * Order of adding memory should be aligned with order of activeCores.
   * Hightlight interaction between cores and memory component relies on it.
   */
  private setNFsMemory() {
    // memory used by NFs
    this._nFsMemory = this.socket.nfs
      .map((nF: ServerNF, index: number): NFMemory => {
        const memory = this.getNfMemory(nF);
        const name = GetNFNodeName(nF.metadata.logicalNf, nF.metadata.nfType);
        const color: string = GetNFColor(name, nF.metadata.service, index);
        const invalid = nF.metadata.draining;

        return { memory, color, invalid } as NFMemory;
      });

    if (this._socket.system) {
      // populate system memory
      this._nFsMemory.push({
        memory: this._socket.system.memory,
        color: 'dark-grey'
      });
    }
  }

  private getNfMemory(nF: ServerNF): number {
    let memory = 0;

    const hugepages = nF.resources.hugepagesBySocket[this._socket.id].hugepages;
    const hugePageSize: number = Number(Object.keys(hugepages)[0]);

    if (hugePageSize) {
      const { [hugePageSize]: hugePageCount } = hugepages;

      memory = Number(hugePageSize) * hugePageCount;
    }

    return memory;
  }

  public get titleIndex(): number {
    return this._titleIndex;
  }

  public get coresRate(): number {
    return this._coresRate;
  }

  public get nFsMemory(): NFMemory[] {
    return this._nFsMemory;
  }

  public get totalMemory(): number {
    return this._socket.hugepages.reduce((usedMemory: number, hugepage: HugePage): number => {
      return usedMemory + (hugepage.count * +hugepage.size);
    }, 0);
  }

  private get usedMemory(): number {
    return this._nFsMemory.reduce((usedMemory: number, nFMemory: NFMemory): number => {
      return usedMemory + nFMemory.memory;
    }, 0);
  }

  private setMemoryRate(): void {
    this._memoryRate = this.calculateRate(this.totalMemory, this.usedMemory);
  }

  public get memoryRate(): number {
    return this._memoryRate;
  }

  public get popupFields(): Array<CellPopupField> {
    return this._popupFields;
  }
}
