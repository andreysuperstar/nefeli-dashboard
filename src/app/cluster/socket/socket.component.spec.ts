import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';

import { cloneDeep } from 'lodash-es';

import { Socket, ServerNF } from '../servers.service';

import { SocketComponent } from './socket.component';
import { MemoryMeterComponent } from '../memory-meter/memory-meter.component';
import { CellGridComponent } from 'src/app/shared/cell-grid/cell-grid.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { ByteConverterPipe } from 'src/app/pipes/byte-converter.pipe';
import { PipelineNamePipe } from 'src/app/pipes/pipeline-name.pipe';
import { CellPopupField } from '../../shared/cell-grid/cell-grid.component';

describe('SocketComponent', () => {
  let component: SocketComponent;
  let fixture: ComponentFixture<SocketComponent>;

  let socketDe: DebugElement;
  let socketEl: HTMLElement;

  const mockServerNFs: ServerNF[] = [
    {
      metadata: {
        logicalNf: 'arista',
        name: 'arista_0',
        tenant: 'Tenant 1',
        service: 'Service 1',
        draining: false,
        machine: 'fluffy',
        nfType: ''
      },
      resources: {
        hugepagesBySocket: {
          0: {
            hugepages: {
              1073741824: 4
            }
          }
        },
        coresBySocket: {
          0: {
            cores: [2, 3, 4, 5, 6, 7]
          }
        },
        cpuOversubscriptionFactor: "0"
      }
    }, {
      metadata: {
        logicalNf: 'filter',
        name: 'filter_1',
        tenant: 'Tenant 2',
        service: 'Service 3',
        draining: false,
        machine: 'fluffy',
        nfType: ''
      },
      resources: {
        hugepagesBySocket: {
          0: {
            hugepages: {}
          }
        },
        coresBySocket: {
          0: {
            cores: [13]
          }
        },
        cpuOversubscriptionFactor: "4"
      }
    }
  ];

  const mockSocket: Socket = {
    id: 0,
    memory: '201392652288',
    hugepages: [
      { size: '1073741824', count: 20 }
    ],
    cores: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13],
    nfs: mockServerNFs,
    cpuOversubscriptionFactor: "2"
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        OverlayModule,
        MatCardModule
      ],
      declarations: [
        SocketComponent,
        MemoryMeterComponent,
        CellGridComponent
      ],
      providers: [
        ByteConverterPipe,
        PipelineNamePipe
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SocketComponent);
    component = fixture.componentInstance;

    socketDe = fixture.debugElement;
    socketEl = socketDe.nativeElement;

    component.socket = cloneDeep(mockSocket);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render correct title', () => {
    expect(component.titleIndex).toBe(1, 'valid incremented id');
    expect(socketEl.querySelector('h3').textContent).toBe('Socket 1', 'valid title');
  });

  it('#cores should return correct quantity', () => {
    expect(component.cores.length).toEqual(mockSocket.cores.length, 'valid cores quantity');
  });

  it('#activeCores should return correct cores', () => {
    expect(component.activeCores[0].indices).toEqual(mockServerNFs[0].resources.coresBySocket[0].cores, 'valid 1st NF active cores');
    expect(component.activeCores[1].indices).toEqual(mockServerNFs[1].resources.coresBySocket[0].cores, 'valid 2nd NF active cores');
  });

  it('#usedCores should return correct cores', () => {
    expect(component['usedCores']).toEqual(7, 'valid used cores quantity');
  });

  it('#calculateRate should return correct rate value', () => {
    const calculateRate: Function = component['calculateRate'];

    const total: number = 200;
    const used: number = 50;

    const rate: number = calculateRate(total, used);

    expect(rate).toEqual(0.75, 'valid calculated rate');
  });

  it('#setCoresRate should set #coresRate to correct value', () => {
    expect(component.coresRate).toEqual(0.5, 'valid cores rate');
  });

  it('#coresRate should display correct value in template', () => {
    const coresCardEl: HTMLElement = socketEl.querySelector('mat-card');
    const availableCores: string = coresCardEl.querySelector('span').textContent;

    expect(availableCores).toEqual('50% Available', 'valid template cores rate');
  });

  it('#setMemoryRate should set #memoryRate to correct value', () => {
    expect(component.memoryRate).toEqual(0.8, 'valid memory rate');
  });

  it('#memoryRate should display correct value in template', () => {
    const memoryCard = socketEl.querySelectorAll('mat-card')[1];
    const availableMemory = memoryCard.querySelector('span').textContent;

    expect(availableMemory).toEqual('80% Available', 'valid template memory rate');
  });

  it('#setNFsMemory should set #NFsMemory to correct value', () => {
    expect(component.nFsMemory.length).toBe(2, 'valid NFs memory quantity');
    expect(component.nFsMemory[0].memory).toEqual(1073741824 * 4, 'valid NF memory');
    expect(component.nFsMemory[1].memory).toEqual(0, 'valid NF memory');
  });

  it('should align active cores and memory data order', () => {
    component['_socket'].system = {
      cores: [0, 1],
      memory: 1073741824
    };

    component.ngOnInit();

    expect(component.activeCores[0].color).not.toBe('dark-grey', 'valid NF core order');
    expect(component.activeCores[2].color).toBe('dark-grey', 'valid system core order');

    expect(component['_nFsMemory'][0].color).not.toBe('dark-grey', 'valid NF memory order');
    expect(component['_nFsMemory'][2].color).toBe('dark-grey', 'valid system memory order');
  });

  it('should display correct over-subscription factor', () => {
    const popupField: CellPopupField[] = component.activeCores[1].popupFields;
    expect(popupField.length).toBe(7);
    expect(popupField[5].description).toBe('4:1');
    expect(component.activeCores[0].popupFields[5].description).toBe('1:1');
  });

  it('should render system cores and memory', () => {
    component['_socket'].system = {
      cores: [0, 1],
      memory: 1073741824
    };

    fixture.detectChanges();

    const cells = socketEl.querySelectorAll('.cell');
    expect(cells[0].classList).toContain('dark-grey');
    expect(cells[1].classList).toContain('dark-grey');
    expect(cells[2].classList).not.toContain('dark-grey');
  });

});
