import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { inject, ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TestRequest, HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Clipboard, ClipboardModule } from '@angular/cdk/clipboard';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { ChecksumAlgorithm } from 'rest_client/pangolin/model/checksumAlgorithm';
import { SitePackage } from 'rest_client/pangolin/model/sitePackage';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { AlertService } from 'src/app/shared/alert.service';

import { ZTPPackageDialogData, ZTPPackageDialogComponent } from './ztp-package-dialog.component';
import * as fileSaver from 'file-saver';
import { HttpError } from 'src/app/error-codes';
import { MatFormFieldModule } from '@angular/material/form-field';

describe('ZTPPackageDialogComponent', () => {
  let component: ZTPPackageDialogComponent;
  let fixture: ComponentFixture<ZTPPackageDialogComponent>;
  let loader: HarnessLoader;
  let httpTestingController: HttpTestingController;

  const mockDialogRef = jasmine.createSpyObj<MatDialogRef<ZTPPackageDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  const mockDialogData: ZTPPackageDialogData = {
    id: 'andrew_site',
    name: 'ztp_package_name'
  };

  const mockPackage: SitePackage = {
    checksum: 'f4e7f3ad7d3b79e92ec0dfa095f9a70a',
    checksumAlg: ChecksumAlgorithm.MD5,
    encryptedPackage: 'package content',
    passphrase: 'theta'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ClipboardModule,
        FlexLayoutModule,
        MatDialogModule,
        MatProgressSpinnerModule,
        MatSnackBarModule,
        MatFormFieldModule
      ],
      declarations: [ZTPPackageDialogComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: MatDialogRef,
          useValue: mockDialogRef
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockDialogData
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        Clipboard,
        SitesService,
        AlertService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZTPPackageDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a message with a spinner', () => {
    const matDialogContent: DebugElement = fixture.debugElement.query(
      By.css('mat-dialog-content')
    );

    const spinnerDe: DebugElement = matDialogContent.query(By.css('mat-spinner'));

    expect(spinnerDe).not.toBeNull('render a spinner');

    expect(matDialogContent.nativeElement.textContent).toContain(
      'Generating package…',
      'render a message'
    );
  });

  it('should close a dialog on Cancel button click and cancel request', async () => {
    const cancelButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Cancel'
    }));

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site/package`
    );

    await cancelButton.click();

    expect(mockDialogRef.close).toHaveBeenCalled();

    fixture.destroy();

    expect(request.cancelled).toBeTruthy('request is cancelled');
  });

  it('should generate a package', async () => {
    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site/package`
    );

    request.flush(mockPackage);
    fixture.detectChanges();

    expect(component['_package']).toBe(mockPackage, 'set site package');

    const dialogHeaderDe: DebugElement = fixture.debugElement.query(
      By.css('[mat-dialog-title]')
    );

    const matDialogContent: DebugElement = fixture.debugElement.query(
      By.css('mat-dialog-content')
    );

    const passphraseHeadingEl: HTMLHeadingElement = matDialogContent.query(
      By.css('h2')
    ).nativeElement;

    const passphraseEl: HTMLElement = matDialogContent.query(By.css('p')).nativeElement;

    const checksumHeadingEl: HTMLHeadingElement = matDialogContent.queryAll(
      By.css('h2')
    )[1].nativeElement;

    const checksumEl: HTMLElement = matDialogContent.queryAll(
      By.css('p')
    )[1].nativeElement;

    expect(dialogHeaderDe).toBeDefined('render package title');

    expect(passphraseHeadingEl.textContent).toBe('Passphrase', 'render a passphrase heading');
    expect(
      passphraseEl.textContent
        .trim()
        .startsWith(mockPackage.passphrase)
    ).toBeTruthy('render a passphrase');

    await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Copy'
    }));

    expect(checksumHeadingEl.textContent).toBe(
      `${mockPackage.checksumAlg} Checksum`,
      'render a passphrase heading'
    );

    expect(
      checksumEl.textContent
        .trim()
        .startsWith(mockPackage.checksum)
    ).toBeTruthy('render a checksum');

    await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Close'
    }));

    await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Download Package'
    }));
  });

  it('should download a package file', async () => {
    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site/package`
    );

    request.flush(mockPackage);
    fixture.detectChanges();

    const downloadButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Download Package'
    }));

    spyOn(fileSaver, 'saveAs').and.callFake(((pkg: Blob) => {
      expect(pkg.size).toBe(10);
      expect(pkg.type).toBe('application/octet-stream');
    }) as typeof fileSaver.saveAs);

    await downloadButton.click();

    expect(fileSaver.saveAs).toHaveBeenCalled();
  });

  it('should copy a package passphrase', inject([Clipboard], async (clipboard: Clipboard) => {
    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site/package`
    );

    request.flush(mockPackage);
    fixture.detectChanges();

    const copyButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Copy'
    }));

    spyOn(component, 'onCopyPassphrase').and.callThrough();
    spyOn(clipboard, 'copy').and.callThrough();

    await copyButton.click();

    expect(component.onCopyPassphrase).toHaveBeenCalled();
    expect(clipboard.copy).toHaveBeenCalledWith(mockPackage.passphrase);
  }));

  it('should copy a package checksum', inject([Clipboard], async (clipboard: Clipboard) => {
    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site/package`
    );

    request.flush(mockPackage);
    fixture.detectChanges();

    const copyButtons = await loader.getAllHarnesses<MatButtonHarness>(MatButtonHarness.with({
      text: 'Copy'
    }));

    spyOn(component, 'onCopyChecksum').and.callThrough();
    spyOn(clipboard, 'copy').and.callThrough();

    await copyButtons[1].click();

    expect(component.onCopyChecksum).toHaveBeenCalled();
    expect(clipboard.copy).toHaveBeenCalledWith(mockPackage.checksum);
  }));

  it('should display generate package failure', async () => {
    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites/andrew_site/package`
    );

    request.flush({}, {
      status: HttpError.BadRequest,
      statusText: 'Bad Request'
    });
    fixture.detectChanges();

    expect(fixture.debugElement.nativeElement.querySelector('mat-dialog-content').textContent)
      .toBe('Failed to create site package, please try again.')
  });
});
