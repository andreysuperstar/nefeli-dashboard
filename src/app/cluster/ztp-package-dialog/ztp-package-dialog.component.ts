import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Clipboard } from '@angular/cdk/clipboard';

import { Subscription } from 'rxjs';

import * as moment from 'moment';
import { saveAs } from 'file-saver';

import { SitePackage } from 'rest_client/pangolin/model/sitePackage';
import { SitePackageError } from 'rest_client/pangolin/model/sitePackageError';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { AlertService } from 'src/app/shared/alert.service';

export interface ZTPPackageDialogData {
  id: string;
  name: string;
}

@Component({
  selector: 'nef-ztp-package-dialog',
  templateUrl: './ztp-package-dialog.component.html',
  styleUrls: ['./ztp-package-dialog.component.less']
})
export class ZTPPackageDialogComponent implements OnInit, OnDestroy {
  private _package: SitePackage;
  private _packageSubscription: Subscription;
  private _hasPackageError = false;

  constructor(
    private _clipboard: Clipboard,
    @Inject(MAT_DIALOG_DATA) private _data: ZTPPackageDialogData,
    private _siteService: SitesService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this.generatePackage();
  }

  public ngOnDestroy() {
    if (!this._packageSubscription.closed) {
      this._packageSubscription.unsubscribe();
    }
  }

  private generatePackage() {
    this._packageSubscription = this._siteService
      .postSitePackage(this._data.id)
      .subscribe(
        (sitePackage: SitePackage) => {
          this._hasPackageError = false;
          this._package = sitePackage;
        },
        ({ message }: SitePackageError) => {
          this._hasPackageError = true;
        }
      );
  }

  public onDownloadPackage() {
    // convert base64 to binary blob
    const decodedData = window.atob(this._package.encryptedPackage);
    const uInt8Array = new Uint8Array(decodedData.length);
    for (let i = 0; i < decodedData.length; ++i) {
      uInt8Array[i] = decodedData.charCodeAt(i);
    }

    const blob: Blob = new Blob([uInt8Array], {
      type: 'application/octet-stream'
    });

    const date: string = moment()
      .format('YYYY-MM-DD_HH-mm-ss');

    const filename = `${this._data.name || this._data.id}_${date}.nefeli.pkg`;

    saveAs(blob, filename);
  }

  public onCopyPassphrase() {
    this._clipboard.copy(this._package.passphrase);
  }

  public onCopyChecksum() {
    this._clipboard.copy(this._package.checksum);
  }

  public get package(): SitePackage {
    return this._package;
  }

  public get hasPackageError(): boolean {
    return this._hasPackageError;
  }
}
