import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { ClusterService, Cluster } from '../home/cluster.service';
import { AlertService } from '../shared/alert.service';

@Injectable()
export class ClusterResolverService implements Resolve<Cluster[]> {

  constructor(private _router: Router, private _clusterService: ClusterService, private _alert: AlertService) { }

  public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Cluster[]> {
    return this._clusterService
      .getClusters()
      .pipe(
        catchError(() => {
          return [];
        }),
        map((clusters: Cluster[]) => {
          if (clusters.length) {
            return clusters;
          } else if (!clusters.length) {
            return [];
          }
        })
      );
  }
}
