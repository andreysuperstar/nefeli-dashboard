import { map, catchError, withLatestFrom } from 'rxjs/operators';
import { BehaviorSubject, Observable, Subscription, of } from 'rxjs';
import { union, flatten, uniq } from 'lodash-es';
import { formatDate } from '@angular/common';
import { CollectionViewer } from '@angular/cdk/collections';
import { SortDirection } from '@angular/material/sort';
import { SiteRow } from '../shared/table/row-types';
import { Link, TableDataSource } from '../shared/table/table.component';
import { ClusterService } from '../home/cluster.service';
import { TableActions } from '../shared/table/table.component';
import { SiteConfigSiteType } from 'rest_client/pangolin/model/siteConfigSiteType';
import { SiteConfigSiteStatus } from 'rest_client/pangolin/model/siteConfigSiteStatus';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { SoftwareProfileSyncStatus } from 'rest_client/pangolin/model/softwareProfileSyncStatus';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { BadgeColor } from '../shared/table/table.component';

const DEFAULT_PAGE_SIZE = 50;

export interface TableSiteRow extends SiteRow {
  type?: SiteConfigSiteType;
  version?: string[];
  scheduledVersion?: Link;
  status?: SiteConfigSiteStatus;
  sync?: string;
}

export class SitesDataSource implements TableDataSource<TableSiteRow> {

  private _siteRows$ = new BehaviorSubject<TableSiteRow[]>([]);
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _fetching$ = new BehaviorSubject<boolean>(false);
  private _sitesSubscription: Subscription;
  private _total: number;
  private _sites: Site[];
  private _rows: TableSiteRow[];
  private _index = 0;
  private _pageSize = DEFAULT_PAGE_SIZE;
  private _searchFilter: string[];
  private _filter: string[];
  private _siteVersions: string[];
  private _softwareProfiles: SoftwareProfile[];

  constructor(
    private _siteService: ClusterService,
    private _softwareProfiles$: Observable<SoftwareProfile[]>
  ) { }

  public connect(collectionViewer: CollectionViewer): Observable<TableSiteRow[]> {
    return this._siteRows$.asObservable();
  }

  public disconnect(collectionViewer: CollectionViewer): void {
    if (this._sitesSubscription) {
      this._sitesSubscription.unsubscribe();
    }
    this._siteRows$.complete();
    this._loading$.complete();
    this._fetching$.complete();
  }

  public fetch(index?: number, pageSize?: number, sortColumn?: string, sortDirection?: SortDirection) {
    this._index = index ?? this._index;
    this._pageSize = pageSize ?? this._pageSize;

    if (this._sitesSubscription) {
      this._sitesSubscription.unsubscribe();
    }

    // sort column and direction should be defined together
    sortColumn = sortDirection ? sortColumn : undefined;

    this._fetching$.next(true);

    this._sitesSubscription = this._siteService.getSites(
      true,
      this._searchFilter,
      this._filter,
      sortColumn,
      sortDirection,
      this._index
      // todo (anton): add this._pageSize argument when we support pagination
    )
      .pipe(
        withLatestFrom(this._softwareProfiles$),
        map(([{ sites: sites, metadata }, softwareProfiles]: [Sites, SoftwareProfile[]]): TableSiteRow[] => {
          this._sites = sites;
          this._softwareProfiles = softwareProfiles;
          this._total = metadata?.total;
          this._siteVersions = this.getSiteVersions();
          return sites.map(site => this.getSiteRow(site));
        }),
        catchError(() => {
          this._loading$.next(false);
          this._fetching$.next(false);
          return of([]);
        })
      )
      .subscribe((siteRows: TableSiteRow[]) => {
        this._rows = siteRows;
        this._siteRows$.next(siteRows);
        this._loading$.next(false);
        this._fetching$.next(false);
      });
  }

  private getSiteVersions(): string[] {
    return union(
      flatten(
        this._sites.map(site => Object.values(site.config.serverVersions ?? {}))
      )
    );
  }

  private getSync(date: Date): string {
    if (!date) {
      return;
    }

    const now = Date.now();

    const time = new Date(date)
      .getTime();

    const difference = now - time;

    const SECOND = 1000;
    const SECONDS = 60;
    const MINUTE = SECONDS * SECOND;
    const MINUTES = 60;
    const HOUR = MINUTES * MINUTE;
    const HOURS = 24;
    const DAY = HOURS * HOUR;

    if (difference >= DAY) {
      return formatDate(date, 'MMM d, y, h:mm a', 'en-US');
    } else if (difference >= HOUR) {
      let hours = difference / HOUR;
      let minutes = difference % HOUR / MINUTE;

      hours = Math.floor(hours);
      minutes = Math.floor(minutes);

      return `${hours}h ${minutes}m ago`;
    } else if (difference >= MINUTE) {
      let minutes = difference / MINUTE;

      minutes = Math.floor(minutes);

      return `${minutes}m ago`;
    } else {
      return '< 1m ago';
    }
  }

  private getSiteRow({
    config: {
      identifier,
      name,
      siteType,
      siteStatus,
      serverVersions,
      softwareProfileSyncStatus,
      softwareProfileId
    },
    status: { lastHeardFromAt } = {}
  }: Site): TableSiteRow {
    const siteRow: TableSiteRow = {
      id: identifier,
      name: name,
      type: siteType,
      status: siteStatus,
      sync: this.getSync(lastHeardFromAt),
      version: uniq(Object.values(serverVersions ?? {})),
      scheduledVersion: this.getScheduledVersion(
        softwareProfileId,
        softwareProfileSyncStatus,
        identifier
      ),
      actions: [
        {
          action: TableActions.Package,
          text: 'Package',
          icon: 'change'
        },
        {
          action: TableActions.Edit,
          icon: 'edit',
          text: 'Edit'
        },
        {
          action: TableActions.Remove,
          icon: 'trash-blue',
          text: 'Remove'
        }
      ]
    };

    return siteRow;
  }

  private getScheduledVersion(
    softwareProfileId: string,
    softwareProfileSyncStatus: SoftwareProfileSyncStatus,
    siteId: string,
    notBefore?: string
  ): Link {
    let icon: string;
    let tooltip: string;
    let iconColor = '';

    const softwareProfile: SoftwareProfile = this._softwareProfiles?.find(
      (p: SoftwareProfile) => p.identifier === softwareProfileId
    );

    switch (softwareProfileSyncStatus) {
      case SoftwareProfileSyncStatus.DONE:
        icon = 'icon-done-status';
        tooltip = 'Site update complete';
        iconColor = BadgeColor.green;
        break;
      case SoftwareProfileSyncStatus.SCHEDULED:
        icon = 'icon-scheduled-status';
        tooltip = `Site to be updated after ${softwareProfile?.notBefore}`;
        break;
      case SoftwareProfileSyncStatus.PENDING:
        icon = 'icon-pending-status';
        tooltip = 'Site update pending';
        iconColor = BadgeColor.blue;
        break;
      case SoftwareProfileSyncStatus.RETRYING:
        icon = 'icon-retrying-status';
        tooltip = 'Retrying to update site';
        iconColor = BadgeColor.yellow;
        break;
    }

    return {
      label: softwareProfile?.distroVersion,
      icon,
      tooltip,
      classes: [iconColor]
    };

  }

  public remove(id: string) {
    const index: number = this._sites.findIndex(site => site.config.identifier === id);

    this._sites.splice(index, 1);
    this._rows.splice(index, 1);
    this._siteRows$.next(this._rows);
    this._total -= 1;
  }

  public getSiteById(id: string): Site {
    return this._sites.find(site => site.config.identifier === id);
  }

  public set total(count: number) {
    this._total = count;
  }

  public get total(): number {
    return this._total;
  }

  public get loading$(): Observable<boolean> {
    return this._loading$.asObservable();
  }

  public get fetching$(): Observable<boolean> {
    return this._fetching$.asObservable();
  }

  public get length(): number {
    return this._siteRows$.value.length;
  }

  public get sites(): Site[] {
    return this._sites;
  }

  public get rows(): TableSiteRow[] {
    return this._rows;
  }

  public get siteVersions(): string[] {
    return this._siteVersions;
  }

  public set searchFilter(search: string[]) {
    this._searchFilter = search;
  }

  public set filter(filter: string[]) {
    this._filter = filter;
  }
}
