import { Component } from '@angular/core';
import { UserService } from '../users/user.service';
import { Observable } from 'rxjs';
import { User } from 'rest_client/heimdallr/model/user';
import { map } from 'rxjs/operators';

@Component({
  selector: 'nef-saas-home',
  templateUrl: './saas-home.component.html',
  styleUrls: ['./saas-home.component.less']
})
export class SaasHomeComponent {

  constructor(private _userService: UserService) { }

  public get username$(): Observable<string> {
    return this._userService.user$.pipe(
      map((user: User): string => {
        return user?.firstName || user?.username;
      })
    );
  }
}
