import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaasHomeComponent } from './saas-home.component';
import { SaasAuthenticationGuard } from '../auth/saas.authentication.guard';

const routes: Routes = [
  {
    path: '',
    component: SaasHomeComponent,
    canActivate: [SaasAuthenticationGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaasHomeRoutingModule { }
