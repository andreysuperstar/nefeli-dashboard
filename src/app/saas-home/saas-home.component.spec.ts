import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaasHomeComponent } from './saas-home.component';
import { By } from '@angular/platform-browser';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatChipsModule } from '@angular/material/chips';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { AlarmListComponent } from '../shared/alarm-list/alarm-list.component';
import { User } from 'rest_client/heimdallr/model/user';
import { UserService } from '../users/user.service';

describe('SaasHomeComponent', () => {
  const mockUsers: User[] = [
    {
      username: 'ecarino',
      email: 'eric@nefeli.io',
      firstName: 'Eric',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'SystemAdmin',
      email: 'sysadmin@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    }];

  let component: SaasHomeComponent;
  let fixture: ComponentFixture<SaasHomeComponent>;
  let userService: UserService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        OverlayModule,
        MatCardModule,
        MatSnackBarModule,
        MatBadgeModule,
        MatDividerModule,
        MatListModule,
        NoopAnimationsModule,
        MatTooltipModule,
        MatChipsModule
      ],
      providers: [UserService],
      declarations: [
        SaasHomeComponent,
        ToolbarComponent,
        AvatarComponent,
        AlarmListComponent
      ]
    })
      .compileComponents();
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SaasHomeComponent]
    })
      .compileComponents();
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaasHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render toolbar', () => {
    const toolbarComponent = (fixture.debugElement.query(By.directive(ToolbarComponent))).componentInstance;
    expect(toolbarComponent).toBeTruthy();
  });

  it('should render FirstName of user', () => {
    userService.setUser(mockUsers[0]);
    fixture.detectChanges();
    component.username$.subscribe((username: string) => expect(username).toBe(mockUsers[0].firstName));
  });

  it('should render username of user', () => {
    userService.setUser(mockUsers[1]);
    fixture.detectChanges();
    component.username$.subscribe((username: string) => expect(username).toBe(mockUsers[1].username));
  });
});
