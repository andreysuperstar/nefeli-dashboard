import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { User } from 'rest_client/heimdallr/model/user';

import { UserService } from '../users/user.service';

@Injectable({
  providedIn: 'root'
})
export class TenantUserGuard implements CanActivate {

  constructor(private _router: Router, private _userService: UserService) { }

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this._userService.user$.pipe(
      map((user: User): [string, boolean] => {
        if (!user || !user.roles || !user.roles.roles) {
          return [undefined, false];
        }

        const { roles: { scope, id, roles } } = user;

        const isTenantUser = scope === 'tenant' && !roles.includes('admin');

        return [id, isTenantUser];
      }),
      tap(([id, isTenantUser]: [string, boolean]) => {
        if (isTenantUser) {
          this._router.navigate(['/tenants', id]);
        }
      }),
      map(([_, isTenantUser]: [string, boolean]): boolean => !isTenantUser)
    );
  }

}
