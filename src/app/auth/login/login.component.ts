import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, AbstractControl, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';

import { MIN_PASSWORD_LENGTH, getPasswordErrorMessage, ERROR_MESSAGES } from '../auth-validation';
import { User } from 'rest_client/heimdallr/model/user';

import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'nef-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit, OnDestroy {

  private _loginForm: FormGroup;
  private _loading = false;
  private _errorMessage: string;
  private _subscription = new Subscription();
  private _loginSubscription: Subscription;

  constructor(private _fb: FormBuilder, private _router: Router, private _authenticationService: AuthenticationService) { }

  public ngOnInit() {
    this.initLoginForm();

    const subscription = this._loginForm.valueChanges.subscribe((_: any) => {
      this._errorMessage = undefined;
    });

    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    if (this._loginSubscription) {
      this._loginSubscription.unsubscribe();
    }

    this._subscription.unsubscribe();
  }

  private initLoginForm() {
    this._loginForm = this._fb.group({
      username: ['', Validators.required],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(MIN_PASSWORD_LENGTH),
          // TODO(eric): uncomment & update when password requirements are defined
          // Validators.pattern(PASSWORD_PATTERN)
        ]
      ]
    });
  }

  public submitLoginForm() {
    const { value: { username, password }, invalid } = this._loginForm;

    if (invalid) {
      return;
    }

    this._loading = true;

    if (this._loginSubscription) {
      this._loginSubscription.unsubscribe();
    }

    this._loginSubscription = this._authenticationService
      .login(username, password)
      .subscribe({
        next: (_: User) => {
          this._loading = false;

          this._router.navigate(['/']);
        },
        error: ({ error }: HttpErrorResponse) => {
          this._loading = false;

          this._errorMessage = error;
        }
      });
  }

  public get loginForm(): FormGroup {
    return this._loginForm;
  }

  public get usernameErrorMessage(): string {
    return ERROR_MESSAGES.USERNAME.REQUIRED;
  }

  public get passwordErrorMessage(): string {
    const password: AbstractControl = this._loginForm.get('password');

    const message: string = getPasswordErrorMessage(password);

    return message;
  }

  public get errorMessage(): string {
    return this._errorMessage;
  }

  public get loading(): boolean {
    return this._loading;
  }

}
