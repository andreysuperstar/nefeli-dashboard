import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { HttpError } from '../../error-codes';

import { environment } from '../../../environments/environment';

import { LoginResp } from 'rest_client/heimdallr/model/loginResp';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';

import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';

import { AlertService } from 'src/app/shared/alert.service';
import { AuthenticationService } from '../authentication.service';

import { LoginComponent } from './login.component';
import { of } from 'rxjs';
import { User } from 'rest_client/pangolin/model/user';
import { UserService } from 'src/app/users/user.service';

describe('LoginComponent', () => {
  let userService: UserService;
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let httpTestingController: HttpTestingController;

  let loginDe: DebugElement;
  let loginEl: HTMLElement;

  let authenticationService: AuthenticationService;
  let username: AbstractControl;
  let password: AbstractControl;
  const emailMock = 'test@nefeli.io';
  const passwordMock = 'test12_@';
  const mockUser: User = {
    username: 'ecarino',
    email: emailMock,
    roles: null
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatSnackBarModule
      ],
      declarations: [LoginComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        AlertService,
        AuthenticationService
      ]
    });

    userService = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
    authenticationService = TestBed.inject(AuthenticationService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;

    loginDe = fixture.debugElement;
    loginEl = loginDe.nativeElement;

    fixture.detectChanges();

    username = component['_loginForm'].get('username');
    password = component['_loginForm'].get('password');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create login form', () => {
    expect(loginEl.querySelectorAll('#login-form').length).toBe(1);
    expect(loginEl.querySelectorAll('mat-form-field').length).toBe(2);
  });

  it('should contain autofocus', () => {
    expect(loginEl.querySelectorAll('[autofocus]').length).toBe(1, 'render only one control with autofocus');
    expect(loginEl.querySelector('[autofocus]').getAttribute('formcontrolname')).toBe('username');
  });

  it('should validate username and password required fields', () => {
    expect(username.errors.required).toBeTruthy();
    expect(password.errors.required).toBeTruthy();
    username.setValue(emailMock);
    password.setValue(passwordMock);
    expect(username.errors).toBeNull();
    expect(password.errors).toBeNull();
  });

  it('should disable form submit when the form is not valid', () => {
    const loginButtonEl: HTMLButtonElement = loginEl.querySelector('button[type=submit]');

    spyOn<any>(authenticationService, 'login').and.callThrough();

    loginButtonEl.click();

    expect(authenticationService.login).not.toHaveBeenCalled();

    username.setValue(emailMock);
    password.setValue(passwordMock);
    component.loginForm.markAsDirty();
    fixture.detectChanges();

    loginButtonEl.click();

    expect(authenticationService.login).toHaveBeenCalledWith(emailMock, passwordMock);
  });

  it('should send login request on submit', () => {
    const loginButtonEl: HTMLButtonElement = loginEl.querySelector('button[type=submit]');

    spyOn(authenticationService, 'login').and.callThrough();
    spyOn(userService, 'getUser').and.returnValue(of(mockUser));

    username.setValue(emailMock);
    password.setValue(passwordMock);
    component.loginForm.markAsDirty();
    fixture.detectChanges();

    loginButtonEl.click();

    expect(authenticationService.login).toHaveBeenCalledWith(emailMock, passwordMock);

    let request = httpTestingController.expectOne(`${environment.restServer}/auth/login`);

    const mockLoginResponse: LoginResp = {
      token: 'ac3fc9d0'
    };

    request.flush(mockLoginResponse);

    expect(component.loading).toBe(false, 'remove loading spinner');

    loginButtonEl.click();

    request = httpTestingController.expectOne(`${environment.restServer}/auth/login`);

    const mockError = 'Error message';

    const mockErrorResponse: Partial<HttpErrorResponse> = {
      status: HttpError.BadRequest,
      statusText: 'Bad Request'
    };

    request.flush(mockError, mockErrorResponse);

    expect(component.loading).toBe(false, 'remove loading spinner');
    expect(component.errorMessage).toBe(mockError, 'set error message');
  });
});
