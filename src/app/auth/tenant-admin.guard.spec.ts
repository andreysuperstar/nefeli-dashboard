import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { Roles } from 'rest_client/heimdallr/model/roles';
import { User } from 'rest_client/heimdallr/model/user';

import { TenantAdminGuard } from "./tenant-admin.guard";

import { UserService } from '../users/user.service';

describe('TenantAdminGuard', () => {
  let tenantAdminGuard: TenantAdminGuard;
  let userService: UserService;

  const spyRouter: jasmine.SpyObj<Router> = jasmine.createSpyObj<Router>(
    'Router',
    ['navigate']
  );

  const mockUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: null
  };

  const mockTenantAdminRole: Roles = {
    scope: 'tenant',
    id: '-1',
    roles: ['admin', 'user']
  };

  const mockSiteAdminRole: Roles = {
    scope: 'system',
    id: '-1',
    roles: ['admin', 'user']
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule
      ],
      providers: [
        {
          provide: Router,
          useValue: spyRouter
        },
        TenantAdminGuard,
        UserService
      ]
    });

    tenantAdminGuard = TestBed.inject(TenantAdminGuard);
    userService = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(tenantAdminGuard).toBeTruthy();
  });

  it('should redirect tenant admin to Tenants page', () => {
    mockUser.roles = mockTenantAdminRole;
    userService.setUser(mockUser);

    tenantAdminGuard
      .canActivate(undefined, undefined)
      .subscribe((activate: boolean) => {
        expect(spyRouter.navigate).toHaveBeenCalledWith(['/tenants', '-1']);

        expect(activate).toBe(false);
      });
  });

  it('should allow access to users other than tenant admin', () => {
    mockUser.roles = mockSiteAdminRole;
    userService.setUser(mockUser);

    tenantAdminGuard
      .canActivate(undefined, undefined)
      .subscribe((activate: boolean) => {
        expect(activate).toBe(true);
      });
  });
});
