import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { AuthenticationService } from './authentication.service';
import { UserService, Jwt } from '../users/user.service';

import { OverlayModule } from '@angular/cdk/overlay';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { LoggerService } from '../shared/logger.service';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';
import { MatChipsModule } from '@angular/material/chips';
import { User } from 'rest_client/heimdallr/model/user';
import { BASE_PATH } from 'rest_client/heimdallr/variables';
import { HttpError } from '../error-codes';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AlertService, AlertType } from '../shared/alert.service';

const spyRouter = { navigate: jasmine.createSpy('navigate') };

const mockUser: User = {
  username: 'Andrew',
  email: 'andrew@nefeli.io',
  roles: {
    scope: 'system',
    id: '-1',
    roles: ['admin', 'user']
  }
};

describe('AuthenticationService', () => {
  let httpMock: HttpTestingController;

  let service: AuthenticationService;
  let userService: UserService;
  let localStorageService: LocalStorageService;
  let loggerService: LoggerService;
  let alertService: AlertService;

  const nonLdapToken: Jwt = {
    aud: 'nefeli-heimdallr',
    exp: Math.round(new Date().getTime() / 1000) + 3600,
    iat: Math.round(new Date().getTime() / 1000),
    iss: 'weaver.tovino.com-heimdallr',
    jti: '86172465054303046557578602619063514986',
    ldap: false,
    nbf: Math.round(new Date().getTime() / 1000),
    roles: {
      scope: 'system',
      id: '',
      roles: [
        'admin'
      ]
    },
    sub: 'signing-key-ldap-admins'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatSnackBarModule,
        OverlayModule,
        MatBadgeModule,
        MatDividerModule,
        MatListModule,
        MatTooltipModule,
        MatChipsModule
      ],
      providers: [
        AuthenticationService,
        UserService,
        LocalStorageService,
        LoggerService,
        AlertService,
        {
          provide: Router,
          useValue: spyRouter
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/auth'
        }
      ]
    });

    httpMock = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AuthenticationService);
    localStorageService = TestBed.inject(LocalStorageService);
    loggerService = TestBed.inject(LoggerService);
    userService = TestBed.inject(UserService);
    alertService = TestBed.inject(AlertService);

    spyOn<any>(userService, 'getDecodedToken').and.returnValue(nonLdapToken);
  });

  afterEach(() => {
    httpMock.verify();
  })

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should initialize unauthorized state', () => {
    expect(service['_isLoggedIn$'].value).toBeFalsy('valid isLoggedIn$ value');

    service.isLoggedIn$.subscribe((isLoggedIn: boolean) => {
      expect(isLoggedIn).toBeFalsy('valid isLogged value');
    });
  });

  it('should simulate the login process', () => {
    service.login('Andrew', 'ericpass').subscribe((response: User) => {
      expect(response).toBe(mockUser);
    });

    userService.user$.subscribe((user: User) => {
      if (user) {
        expect(user.email).toBe('andrew@nefeli.io');
        expect(user.username).toBe('Andrew');
        expect(user.roles.id).toBe('-1');
        expect(user.roles.scope).toBe('system');
        expect(user.roles.roles).toBeDefined();
      }
    });

    let isLoggedInCount = 0;
    service.isLoggedIn$.subscribe((isLoggedIn: boolean) => {
      if (isLoggedInCount++ === 1) {
        expect(isLoggedIn).toBe(true);
      }
    });

    const loginReq = httpMock.expectOne(`${environment.restServer}/auth/login`);
    expect(loginReq.request.method).toBe('POST');
    expect(loginReq.request.body).toBeNull();
    expect(loginReq.request.headers.get('Authorization')).toBe('Basic QW5kcmV3OmVyaWNwYXNz');
    loginReq.flush({
      token: 'my-fake-token'
    });

    const userReq = httpMock.expectOne(`${environment.restServer}/auth/users/Andrew`);
    expect(userReq.request.method).toBe('GET');
    expect(userReq.request.headers.get('Authorization')).toBe('Bearer my-fake-token');
    userReq.flush(mockUser);

    expect(localStorageService.read(LocalStorageKey.ACCESS_TOKEN)).toBe('my-fake-token');
    expect(localStorageService.read(LocalStorageKey.USER_NAME)).toBe('Andrew');
  });

  it('should simulate the check authentication process', () => {
    localStorageService.write(LocalStorageKey.USER_NAME, 'ecarino');
    localStorageService.write(LocalStorageKey.ACCESS_TOKEN, 'my-fake-token');

    userService.user$.subscribe((user: User) => {
      if (user) {
        expect(user.email).toBe('andrew@nefeli.io');
        expect(user.username).toBe('Andrew');
        expect(user.roles.id).toBe('-1');
        expect(user.roles.scope).toBe('system');
        expect(user.roles.roles).toBeDefined();
      }
    });

    let isLoggedInCount = 0;
    service.isLoggedIn$.subscribe((isLoggedIn: boolean) => {
      if (isLoggedInCount++ === 1) {
        expect(isLoggedIn).toBe(true);
      }
    });

    service.authenticate().subscribe((response: User) => {
      expect(response.username).toBe('Andrew');
      expect(response.email).toBe('andrew@nefeli.io');
      expect(response.roles).toEqual({
        scope: 'system',
        id: '-1',
        roles: ['admin', 'user']
      });
    });

    const userReq = httpMock.expectOne(`${environment.restServer}/auth/users/ecarino`);
    expect(userReq.request.method).toBe('GET');
    expect(userReq.request.headers.get('Authorization')).toBe('Bearer my-fake-token');
    userReq.flush(mockUser);
  });

  it('should simulate the logout process', () => {
    localStorageService.write(LocalStorageKey.USER_NAME, 'ecarino');
    localStorageService.write(LocalStorageKey.ACCESS_TOKEN, 'my-fake-token');

    expect(localStorageService.read(LocalStorageKey.ACCESS_TOKEN)).toBe('my-fake-token');
    expect(localStorageService.read(LocalStorageKey.USER_NAME)).toBe('ecarino');

    spyOn(loggerService, 'clearLogs');

    service.logout();

    const logoutReq = httpMock.expectOne(`${environment.restServer}/auth/logout`);
    expect(logoutReq.request.method).toBe('POST');
    expect(logoutReq.request.body).toBeNull();
    expect(logoutReq.request.headers.get('Authorization')).toBe('Bearer my-fake-token');
    logoutReq.flush({
      token: 'my-fake-token'
    });

    expect(localStorageService.read(LocalStorageKey.ACCESS_TOKEN)).toBeNull();
    expect(localStorageService.read(LocalStorageKey.USER_NAME)).toBeNull();

    expect(loggerService.clearLogs).toHaveBeenCalled();
    expect(spyRouter.navigate).toHaveBeenCalledWith(['/auth/login']);

  });

  it('should display error if logout req fails', () => {
    spyOn(alertService, 'error')
    service.logout();

    httpMock.expectOne(`${environment.restServer}/auth/logout`).flush({},
      {
        status: HttpError.BadRequest,
        statusText: 'Bad Request',
      }
    );

    expect(alertService.error).toHaveBeenCalledWith(AlertType.ERROR_LOGOUT)
  });

  it('should simulate the saas logout process', () => {
    localStorageService.write(LocalStorageKey.USER_NAME, 'ecarino');
    localStorageService.write(LocalStorageKey.ACCESS_TOKEN, 'my-fake-token');
    localStorageService.write(LocalStorageKey.IS_SAAS, 'true');

    expect(localStorageService.read(LocalStorageKey.ACCESS_TOKEN)).toBe('my-fake-token');
    expect(localStorageService.read(LocalStorageKey.USER_NAME)).toBe('ecarino');

    spyOn(loggerService, 'clearLogs');

    service.logout();

    const logoutReq = httpMock.expectOne(`${environment.restServer}/auth/logout`);
    expect(logoutReq.request.method).toBe('POST');
    expect(logoutReq.request.body).toBeNull();
    expect(logoutReq.request.headers.get('Authorization')).toBe('Bearer my-fake-token');
    logoutReq.flush({
      token: 'my-fake-token'
    });

    expect(localStorageService.read(LocalStorageKey.ACCESS_TOKEN)).toBeNull();
    expect(localStorageService.read(LocalStorageKey.USER_NAME)).toBeNull();
    expect(localStorageService.read(LocalStorageKey.IS_SAAS)).toBeNull();

    expect(loggerService.clearLogs).toHaveBeenCalled();
    expect(spyRouter.navigate).toHaveBeenCalledWith(['/saas']);
  });

  it('should send "register" request', () => {
    const username = 'user';
    const password = 'fake-pwd';
    const invite_token = 'fake-token';
    const token = 'resp-token';

    service.register(username, password, invite_token).subscribe(resp => expect(resp).toEqual({ token }));
    const req = httpMock.expectOne(`${environment.restServer}/auth/register`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      invite_token,
      username,
      password
    });
    req.flush({ token });
  });

  it('should send "request reset password" request', () => {
    const username = 'user';
    const expectResp = 'OK';

    service.requestResetPassword(username).subscribe(resp => expect(resp).toEqual(expectResp));
    const req = httpMock.expectOne(`${environment.restServer}/auth/request-password-reset`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      username
    });
    req.flush(expectResp);
  });

  it('should send "reset password" request', () => {
    const otp_token = 'fake-token';
    const new_password = 'new-pwd';
    const expectResp = 'OK';

    service.resetPassword(otp_token, new_password).subscribe(resp => expect(resp).toEqual(expectResp));
    const req = httpMock.expectOne(`${environment.restServer}/auth/password-reset`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      otp_token,
      new_password
    });
    req.flush(expectResp);
  });

  it('should send "change password" request', () => {
    const username = 'user';
    const current_password = 'pwd';
    const new_password = 'new-pwd';
    const expectResp = 'OK';

    service.editPassword(username, current_password, new_password).subscribe(resp => expect(resp).toEqual(expectResp));
    const req = httpMock.expectOne(`${environment.restServer}/auth/change-password`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body).toEqual({
      username,
      current_password,
      new_password
    });
    req.flush(expectResp);
  });
});
