import { TestBed } from "@angular/core/testing";
import { AuthenticationGuard } from './authentication.guard';
import { Router, Route } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthenticationService } from './authentication.service';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';
import { UserService } from '../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { environment } from '../../environments/environment';
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { take } from "rxjs/operators";

describe('AuthenticationGuard', () => {
  let service: AuthenticationGuard;
  let httpMock: HttpTestingController;
  let authenticationService: AuthenticationService;
  let localStorageService: LocalStorageService;
  const spyRouter = { navigate: jasmine.createSpy('navigate') };
  const mockToken = 'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJuZWZlbGktaGVpbWRhbGxyIiwiY3JlYXRlZF9hdCI6IjE2MzIzMTYzNzgiLCJleHAiOjE2MzI5MzgzODYsImlhdCI6MTYzMjMzMzU4NiwiaXNzIjoiYXp1cmUtd2VhdmVyLWhlaW1kYWxsciIsImp0aSI6IjIzMjkzMjMwNjA3NjYwNjExNDMxODY4NDQxOTk0MjYzNTk4MTg1NSIsIm5iZiI6MTYzMjMzMzU4NiwibmVmZWxpX3VuYW1lIjoibW9udHkiLCJyb2xlcyI6eyJzY29wZSI6InN5c3RlbSIsImlkIjoiIiwicm9sZXMiOlsidXNlciIsImFkbWluIl19LCJzdWIiOiJtb250eSJ9.dmF1bHQ6djE6TUVRQ0lHdGhlRytTcnVkc1ZiYnFEQzhoL013SitYbGtLaGpIQzRYbjdMaXhlK1BuQWlCMkJKQ1F4U2ZobC9DeHVUUnY0dmkzMzVheitLVitsM0xUVmVuUXd6cHlDUT09';

  const mockUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  const testAuthenticationError = (errorCode: number, errorText: string) => {
    const mockRoute: Route = {
      path: ''
    }

    localStorageService.write(LocalStorageKey.USER_NAME, 'ecarino');
    localStorageService.write(LocalStorageKey.ACCESS_TOKEN, mockToken);
    authenticationService['_isLoggedIn$'].next(false);

    service
      .canLoad(mockRoute)
      .pipe(
        take(1)
      )
      .subscribe((activate: boolean) => {
        httpMock.expectOne(`${environment.restServer}/auth/logout`).flush({});
        expect(spyRouter.navigate).toHaveBeenCalledWith(['/auth/login']);
        expect(activate).toBe(true);
      });

    const req = httpMock.expectOne(`${environment.restServer}/auth/users/ecarino`);
    req.flush({}, {
      status: errorCode,
      statusText: errorText,
    });

    localStorageService.delete(LocalStorageKey.USER_NAME);
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatSnackBarModule,
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        AuthenticationGuard,
        AuthenticationService,
        {
          provide: Router,
          useValue: spyRouter
        },
        UserService,
        LocalStorageService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        }
      ],
      declarations: []
    });

    service = TestBed.inject(AuthenticationGuard);
    httpMock = TestBed.inject(HttpTestingController);
    authenticationService = TestBed.inject(AuthenticationService);
    localStorageService = TestBed.inject(LocalStorageService);
    localStorageService.delete(LocalStorageKey.ACCESS_TOKEN);
  });

  afterEach(() => {
    localStorageService.delete(LocalStorageKey.ACCESS_TOKEN);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should route to home page if user is already logged in and on login page', () => {
    const mockRoute: Route = {
      path: '/auth/login'
    }

    authenticationService['_isLoggedIn$'].next(true);
    localStorageService.write(LocalStorageKey.ACCESS_TOKEN, mockToken);

    service
      .canLoad(mockRoute)
      .subscribe((activate: boolean) => {
        expect(activate).toBe(false);
        expect(spyRouter.navigate).toHaveBeenCalledWith(['/']);
      });
  });

  it('should allow user access after successful authentication', () => {
    const mockRoute: Route = {
      path: ''
    }

    localStorageService.write(LocalStorageKey.USER_NAME, 'ecarino');
    localStorageService.write(LocalStorageKey.ACCESS_TOKEN, mockToken);
    authenticationService['_isLoggedIn$'].next(false);

    service
      .canLoad(mockRoute)
      .subscribe((activate: boolean) => {
        expect(activate).toBe(true);
      });

    const req = httpMock.expectOne(`${environment.restServer}/auth/users/ecarino`);
    req.flush(mockUser);

    localStorageService.delete(LocalStorageKey.USER_NAME);
  });

  it('should redirect user to login page if 401 authentication failure', () => {
    testAuthenticationError(401, 'No Authentication');
  });

  it('should redirect user to login page if 400 authentication failure', () => {
    testAuthenticationError(400, 'Bad Request');
  });

  it('should redirect user to login page if 403 authentication failure', () => {
    testAuthenticationError(403, 'Forbidden');
  });
});
