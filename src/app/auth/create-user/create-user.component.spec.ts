import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ERROR_MESSAGES } from '../auth-validation';

import { AlertService } from 'src/app/shared/alert.service';
import { AuthenticationService } from '../authentication.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { environment } from '../../../environments/environment';

import { CreateUserComponent } from './create-user.component';

describe('CreateUserComponent', () => {
  let component: CreateUserComponent;
  let fixture: ComponentFixture<CreateUserComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule,
        MatSelectModule
      ],
      declarations: [CreateUserComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        AlertService,
        AuthenticationService
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUserComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain autofocus', () => {
    expect(el.querySelectorAll('[autofocus]').length).toBe(1, 'render only one control with autofocus');
    expect(el.querySelector('[autofocus]').getAttribute('formcontrolname')).toBe('username');
  });

  it('should return valid \'email\' error message', () => {
    const email: AbstractControl = component['_userForm'].get('email');

    expect(email.errors.required).toBeTruthy('valid \'required\' error for empty email control');
    expect(component.emailErrorMessage).toBe(ERROR_MESSAGES.EMAIL.REQUIRED, 'valid \'required\' email error message on blank control');

    email.setValue('john.doe');

    expect(email.errors.email).toBeTruthy('valid \'email\' error for empty email control');
    expect(component.emailErrorMessage).toBe(ERROR_MESSAGES.EMAIL.EMAIL, 'valid \'email\' error message on invalid email control value');

    email.setValue('john.doe@nefeli.io');

    expect(component.emailErrorMessage).toBe('', 'empty error message on valid email control value');

    email.setValue('john.doe@');

    expect(email.errors.email).toBeTruthy('valid \'email\' error for empty email control');
    expect(component.emailErrorMessage).toBe(ERROR_MESSAGES.EMAIL.EMAIL, 'valid \'email\' error message on invalid email control value with \'@\' symbol');
  });

  it('should issue create user request on form submit', () => {
    const username: AbstractControl = component['_userForm'].get('username');
    const email: AbstractControl = component['_userForm'].get('email');
    const roles: AbstractControl = component['_userForm'].get('roles');
    const scope: AbstractControl = component['_userForm'].get('scope');
    const scopeId: AbstractControl = component['_userForm'].get('scopeId');

    username.setValue('ecarino');
    email.setValue('ecarino@nefeli.io');
    scope.setValue('tenant');
    scopeId.setValue('-1');
    roles.setValue(['user']);

    component.submitUserForm();

    const req = httpMockClient.expectOne(`${environment.restServer}/auth/users`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.username).toBe('ecarino');
    expect(req.request.body.email).toBe('ecarino@nefeli.io');
    expect(req.request.body.roles.scope).toBe('tenant');
    expect(req.request.body.roles.id).toBe('-1');
    expect(req.request.body.roles.roles[0]).toBe('user');
  })
});
