import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';

import { Subscription } from 'rxjs';

import { ERROR_MESSAGES, getEmailErrorMessage } from '../auth-validation';
import { Scope } from '../authentication.service';
import { AlertService, AlertType } from '../../shared/alert.service';
import { UserService } from 'src/app/users/user.service';
import { PostUserResp } from 'rest_client/heimdallr/model/postUserResp';

@Component({
  selector: 'nef-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.less']
})
export class CreateUserComponent implements OnInit, OnDestroy {

  private _userForm: FormGroup;
  private _scopes: Array<string>;
  private _createUserSubscription: Subscription;

  constructor(
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _userService: UserService) { }

  public ngOnInit() {
    this._scopes = Object.values(Scope);

    this.initUserForm();
  }

  public ngOnDestroy() {
    if (this._createUserSubscription) {
      this._createUserSubscription.unsubscribe();
    }
  }

  private initUserForm() {
    this._userForm = this._fb.group({
      username: ['', Validators.required],
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      scope: ['', Validators.required],
      scopeId: ['', Validators.required],
      roles: ['', Validators.required]
    });
  }

  public submitUserForm() {
    const { value: { username, email, scope, scopeId, roles }, invalid } = this._userForm;

    if (invalid) {
      return;
    }

    if (this._createUserSubscription) {
      this._createUserSubscription.unsubscribe();
    }

    this._createUserSubscription = this._userService
      .addUser({
        username: username,
        email: email,
        roles: {
          scope: scope,
          id: scopeId,
          roles: roles
        }
      })
      .subscribe(
        (_: PostUserResp) => this._alertService.info(AlertType.INFO_CREATE_USER_SUCCESS),
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_CREATE_USER_FAILED)
      );
  }

  public get userForm(): FormGroup {
    return this._userForm;
  }

  public get ERROR_MESSAGES(): typeof ERROR_MESSAGES {
    return ERROR_MESSAGES;
  }

  public get emailErrorMessage(): string {
    const email: AbstractControl = this._userForm.get('email');

    const message: string = getEmailErrorMessage(email);

    return message;
  }

  public get scopes(): Array<string> {
    return this._scopes;
  }
}
