import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AlertType, AlertService } from 'src/app/shared/alert.service';
import { AuthenticationService } from '../authentication.service';

import { PasswordRecoveryComponent } from './password-recovery.component';
import { HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { environment } from '../../../environments/environment';

describe('PasswordRecoveryComponent', () => {
  let component: PasswordRecoveryComponent;
  let fixture: ComponentFixture<PasswordRecoveryComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      declarations: [PasswordRecoveryComponent],
      providers: [
        AlertService,
        AuthenticationService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        }
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordRecoveryComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
    httpMockClient = TestBed.inject(HttpTestingController);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain autofocus', () => {
    expect(el.querySelectorAll('[autofocus]').length).toBe(1, 'render only one control with autofocus');
    expect(el.querySelector('[autofocus]').getAttribute('formcontrolname')).toBe('username');
  });

  it('should return valid \'username\' error message', () => {
    const username: AbstractControl = component['_passwordRecoveryForm'].get('username');
    expect(username.errors.required).toBe(true);
    username.setValue('ecarino');
    expect(username.errors).toBeNull();
  });

  it('should issue request to reset password', () => {
    const username: AbstractControl = component['_passwordRecoveryForm'].get('username');
    username.setValue('ecarino');

    component.submitPasswordRecoveryForm();
    const req = httpMockClient.expectOne(`${environment.restServer}/auth/request-password-reset`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.username).toBe('ecarino');

    req.flush('password-reset');

    let alertSpanEl = document.querySelector<HTMLSpanElement>('simple-snack-bar span');

    expect(alertSpanEl.textContent).toBe(
      AlertType.INFO_REQUEST_RESET_PASSWORD,
      'render a request reset password success alert'
    );
  })

  it('should issue error request to reset password', () => {
    const username: AbstractControl = component['_passwordRecoveryForm'].get('username');
    username.setValue('ecarino');

    component.submitPasswordRecoveryForm();
    const req = httpMockClient.expectOne(`${environment.restServer}/auth/request-password-reset`);

    const mockErrorResponse: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };

    req.flush({}, mockErrorResponse);

    let alertSpanEl = document.querySelector<HTMLSpanElement>('simple-snack-bar span');

    expect(alertSpanEl.textContent).toBe(
      AlertType.ERROR_REQUEST_RESET_PASSWORD,
      'render a request reset password error alert'
    );
  })
});
