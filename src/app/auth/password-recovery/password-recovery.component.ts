import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Subscription } from 'rxjs';

import { ERROR_MESSAGES } from '../auth-validation';

import { AlertService, AlertType } from '../../shared/alert.service';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'nef-password-recovery',
  templateUrl: './password-recovery.component.html',
  styleUrls: ['./password-recovery.component.less']
})
export class PasswordRecoveryComponent implements OnInit, OnDestroy {

  private _passwordRecoveryForm: FormGroup;
  private _resetPasswordSubscription: Subscription;

  constructor(private _fb: FormBuilder, private _alertService: AlertService, private _authService: AuthenticationService) { }

  public ngOnInit() {
    this.initPasswordRecoveryForm();
  }

  public ngOnDestroy() {
    if (this._resetPasswordSubscription) {
      this._resetPasswordSubscription.unsubscribe();
    }
  }

  private initPasswordRecoveryForm() {
    this._passwordRecoveryForm = this._fb.group({
      username: ['', Validators.required]
    });
  }

  public submitPasswordRecoveryForm() {
    const { value: { username }, invalid } = this._passwordRecoveryForm;

    if (invalid) {
      return;
    }

    if (this._resetPasswordSubscription) {
      this._resetPasswordSubscription.unsubscribe();
    }

    this._resetPasswordSubscription = this._authService
      .requestResetPassword(username)
      .subscribe({
        next: () => {
          this._alertService.info(AlertType.INFO_REQUEST_RESET_PASSWORD);
        },
        error: () => {
          this._alertService.error(AlertType.ERROR_REQUEST_RESET_PASSWORD);
        }
      });
  }

  public get passwordRecoveryForm(): FormGroup {
    return this._passwordRecoveryForm;
  }

  public get usernameErrorMessage(): string {
    return ERROR_MESSAGES.USERNAME.REQUIRED;
  }
}
