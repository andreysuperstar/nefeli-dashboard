import { Injectable } from '@angular/core';

import { Observable, BehaviorSubject, throwError, of } from 'rxjs';
import { tap, mergeMap, catchError } from 'rxjs/operators';

import { UserService } from '../users/user.service';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';
import { LoggerService } from '../shared/logger.service';
import { HttpError } from '../error-codes';
import { Router } from '@angular/router';
import { User } from 'rest_client/heimdallr/model/user';
import { LoginResp } from 'rest_client/heimdallr/model/loginResp';
import { RegisterResp } from 'rest_client/heimdallr/model/registerResp';
import { RegisterReqBody } from 'rest_client/heimdallr/model/registerReqBody';
import { RequestPasswordResetReqBody } from 'rest_client/heimdallr/model/requestPasswordResetReqBody';
import { ResetPasswordReqBody } from 'rest_client/heimdallr/model/resetPasswordReqBody';
import { UsersService } from 'rest_client/heimdallr/api/users.service';
import { AlertService, AlertType } from '../shared/alert.service';
import { ChangePasswordReqBody } from 'rest_client/heimdallr/model/changePasswordReqBody';
import { environment } from 'src/environments/environment';

export enum Scope {
  System = 'system',
  Tenant = 'tenant'
}

const MOCK_TOKEN = 'eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJuZWZlbGktaGVpbWRhbGxyIiwiY3JlYXRlZF9hdCI6IjE2NDAxODI0NDkiLCJleHAiOjE2NDA4NDg0MDAsImlhdCI6MTY0MDI0MzYwMCwiaXNzIjoiYXp1cmUtd2VhdmVyLWhlaW1kYWxsciIsImp0aSI6IjE5OTQ0OTgwMDk1OTA2MDAwMDAwODc3NTEwNjcyNDA2NDUxNDQ2IiwibmJmIjoxNjQwMjQzNjAwLCJuZWZlbGlfdW5hbWUiOiJtb250eSIsInJvbGVzIjp7InNjb3BlIjoic3lzdGVtIiwiaWQiOiIiLCJyb2xlcyI6WyJ1c2VyIiwiYWRtaW4iXX0sInN1YiI6Im1vbnR5In0=.dmF1bHQ6djE6TUVZQ0lRRE8wbjRzOHp6SmdGdTN1NGNQSkRVYmRPYVQ0U3R2T3BRRkt3RjVsdDlMS3dJaEFJMXpFeTN6RGxoRnN3eWxxVGExcG5XTlVPZmw5SFFiNVVGSEFoTGo4dXVw';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _isLoggedIn$ = new BehaviorSubject<boolean>(false);

  constructor(
    private _userService: UserService,
    private _localStorageService: LocalStorageService,
    private _loggerService: LoggerService,
    private _router: Router,
    private _restUsersService: UsersService,
    private _alertService: AlertService
  ) { }

  public authenticate(): Observable<User> {
    const userName = this._localStorageService.read(LocalStorageKey.USER_NAME);
    const accessToken = this._localStorageService.read(LocalStorageKey.ACCESS_TOKEN);

    if (!userName) {
      return throwError({
        status: HttpError.Unauthorized
      });
    }

    return this._userService.getUser(
      userName, accessToken).pipe(
        tap((user: User) => {
          this._userService.setUser(user);

          this._isLoggedIn$.next(true);
        })
      );
  }

  public login(username: string, password: string): Observable<User> {
    this._restUsersService.configuration.username = username;
    this._restUsersService.configuration.password = password;

    return this._restUsersService.login()
      .pipe(
        mergeMap((response: LoginResp) => {
          if (response.token) {
            this._localStorageService.write(LocalStorageKey.ACCESS_TOKEN, response.token);
          } else if (!environment.production) {
            // write a fake token when in dev mode
            this._localStorageService.write(LocalStorageKey.ACCESS_TOKEN, MOCK_TOKEN);
          }

          return this._userService.getUser(username, response.token);
        }),
        tap((user: User) => {
          this._userService.setUser(user);
          this._localStorageService.write(LocalStorageKey.USER_NAME, user.username);
          this._isLoggedIn$.next(true);
        })
      );
  }

  public logout() {
    this._restUsersService.logout()
      .pipe(catchError(() => of(true)))
      .subscribe((hasError: boolean) => {
        if (hasError === true) {
          this._alertService.error(AlertType.ERROR_LOGOUT);
        } else {
          const isSaas: boolean = this._localStorageService.read(LocalStorageKey.IS_SAAS) === 'true';
          const loginRoute: string = isSaas ? '/saas' : '/auth/login';
          this.cleanSession();
          this._userService.setUser(undefined);
          this._router.navigate([loginRoute]);
        }
      });
  }

  public cleanSession() {
    this._localStorageService.cleanUserSession();
    this._loggerService.clearLogs();
    this._isLoggedIn$.next(false);
  }

  public register(username: string, password: string, token: string): Observable<RegisterResp> {
    const requestBody: RegisterReqBody = {
      invite_token: token,
      username: username,
      password: password
    };

    return this._restUsersService.register(requestBody);
  }

  public requestResetPassword(username: string): Observable<string> {
    const requestBody: RequestPasswordResetReqBody = { username };
    return this._restUsersService.requestPasswordReset(requestBody);
  }

  public resetPassword(token: string, password: string): Observable<string> {
    const requestBody: ResetPasswordReqBody = {
      otp_token: token,
      new_password: password
    };

    return this._restUsersService.passwordReset(requestBody);
  }

  public editPassword(username: string, currentPassword: string, newPassword: string): Observable<string> {
    const requestBody: ChangePasswordReqBody = {
      username,
      current_password: currentPassword,
      new_password: newPassword
    };

    return this._restUsersService.changePassword(requestBody);
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this._isLoggedIn$.asObservable();
  }
}
