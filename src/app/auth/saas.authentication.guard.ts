import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route,
  Router,
  RouterStateSnapshot,
  UrlSegment
} from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, switchMapTo, take, tap } from 'rxjs/operators';
import { User } from 'rest_client/heimdallr/model/user';
import { HttpErrorResponse } from '@angular/common/http';
import { HttpError, HttpSuccess } from '../error-codes';

const loginPath = '/saas';
const defaultRoutePath = '/saas/home';

@Injectable({
  providedIn: 'root'
})
export class SaasAuthenticationGuard implements CanActivate, CanLoad {

  constructor(
    protected _router: Router,
    protected _authenticationService: AuthenticationService,
    protected _localStorageService: LocalStorageService
  ) { }

  public canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> {
    const segmentsPath = segments.reduce((path: string, currentSegment: UrlSegment) => {
      return `${path}/${currentSegment.path}`;
    }, '');
    const url = route.path ? `${segmentsPath}/${route.path}` : segmentsPath;
    return this.permit(url);
  }

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.permit(state.url);
  }

  protected permit(url: string): Observable<boolean> {
    let isLoginPage: boolean = url === loginPath;

    if (this._localStorageService.read(LocalStorageKey.ACCESS_TOKEN) === null && !isLoginPage) {
      this._router.navigate([loginPath]);
      return of(false);
    }

    return this._authenticationService.isLoggedIn$.pipe(
      take(1),
      tap((isLoggedIn: boolean) => {
        if (isLoggedIn && isLoginPage) {
          this._router.navigate([defaultRoutePath]);
        }
      }),
      switchMap((isLoggedIn: boolean): Observable<any | User> => isLoggedIn
        ? of(true)
        : this._authenticationService
          .authenticate()
          .pipe(
            tap(
              (_: User) => { },
              (error: HttpErrorResponse) => {
                if ((error.status === HttpError.Unauthorized
                  || error.status === HttpError.BadRequest
                  || error.status === HttpError.Forbidden
                  || (error.status === HttpSuccess.OK && error.name === 'HttpErrorResponse'))
                  && !isLoginPage) {
                  this._authenticationService.logout();

                  isLoginPage = true;
                }
              }
            ),
            catchError(() => of(false))
          )
      ),
      switchMapTo(this._authenticationService.isLoggedIn$),
      map((isLoggedIn: boolean): boolean => isLoginPage
        ? !isLoggedIn
        : isLoggedIn
      )
    );
  }
}
