import { TenantUserGuard } from "./tenant-user.guard";
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserService } from '../users/user.service';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { Roles } from 'rest_client/heimdallr/model/roles';
import { User } from 'rest_client/heimdallr/model/user';

describe('TenantUserGuard', () => {
  let service: TenantUserGuard;
  let userService: UserService;
  const spyRouter = { navigate: jasmine.createSpy('navigate') };

  const mockTenantRole: Roles = {
    scope: 'tenant',
    id: '-1',
    roles: ['user']
  };

  const mockClusterRole: Roles = {
    scope: 'system',
    id: '-1',
    roles: ['admin', 'user']
  };

  const mockUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: null
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule
      ],
      providers: [
        TenantUserGuard,
        UserService,
        {
          provide: Router,
          useValue: spyRouter
        }
      ]
    });

    service = TestBed.inject(TenantUserGuard);
    userService = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have tenant user directed to tenants page', () => {
    mockUser.roles = mockTenantRole;
    userService.setUser(mockUser);

    service.canActivate(new ActivatedRouteSnapshot(), undefined).subscribe(() => {
      expect(spyRouter.navigate).toHaveBeenCalledWith(['/tenants', '-1']);
    });
  });

  it('should not have non-tenant user directed to tenants page', () => {
    mockUser.roles = mockClusterRole;
    userService.setUser(mockUser);

    service.canActivate(new ActivatedRouteSnapshot(), undefined).subscribe((activate: boolean) => {
      expect(activate).toBe(true);
    });
  });
});
