import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageDataKey } from '../toolbar/toolbar.component';

import { AuthenticationGuard } from './authentication.guard';

import { AuthComponent } from './auth.component';
import { LoginComponent } from './login/login.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { SetupPasswordComponent, PasswordSetupMode } from './setup-password/setup-password.component';
import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
        canLoad: [AuthenticationGuard],
        data: {
          [PageDataKey.LoginPage]: true
        }
      },
      {
        path: 'create-user',
        component: CreateUserComponent
      },
      {
        path: 'register',
        component: SetupPasswordComponent
      },
      {
        path: 'password-recovery',
        component: PasswordRecoveryComponent
      },
      {
        path: 'password-reset',
        component: SetupPasswordComponent,
        data: {
          mode: PasswordSetupMode.Reset
        }
      },
      {
        path: 'password-edit',
        component: SetupPasswordComponent,
        canLoad: [AuthenticationGuard],
        data: {
          mode: PasswordSetupMode.Edit
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AuthRoutingModule { }
