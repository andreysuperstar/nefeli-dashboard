import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, CanLoad, UrlSegment, Route } from '@angular/router';

import { Observable, of } from 'rxjs';
import { take, tap, switchMap, switchMapTo, map, catchError } from 'rxjs/operators';

import { User } from 'rest_client/heimdallr/model/user';
import { PageDataKey } from '../toolbar/toolbar.component';
import { HttpSuccess, HttpError } from '../error-codes';

import { AuthenticationService } from './authentication.service';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanLoad {

  constructor(
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private _localStorageService: LocalStorageService
  ) { }

  public canLoad(route: Route): Observable<boolean> {
    const loginPath = '/auth/login';
    let isLoginPage: boolean = route.path === loginPath;

    /* Fix for NEF-7459: Only load child modules if access token exists */
    if (this._localStorageService.read(LocalStorageKey.ACCESS_TOKEN) === null && environment.production) {
      this._router.navigate([loginPath]);
      return of(false);
    }

    return this._authenticationService.isLoggedIn$.pipe(
      take(1),
      tap((isLoggedIn: boolean) => {
        if (isLoggedIn && isLoginPage) {
          this._router.navigate(['/']);
        }
      }),
      switchMap((isLoggedIn: boolean): Observable<any | User> => isLoggedIn
        ? of(true)
        : this._authenticationService
          .authenticate()
          .pipe(
            tap(
              (_: User) => { },
              (error: HttpErrorResponse) => {
                if ((error.status === HttpError.Unauthorized
                  || error.status === HttpError.BadRequest
                  || error.status === HttpError.Forbidden
                  || (error.status === HttpSuccess.OK && error.name === 'HttpErrorResponse'))
                  && !isLoginPage) {
                  this._authenticationService.logout();

                  isLoginPage = true;
                }
              }
            ),
            catchError(() => of(false))
          )
      ),
      switchMapTo(this._authenticationService.isLoggedIn$),
      map((isLoggedIn: boolean): boolean => isLoginPage
        ? !isLoggedIn
        : isLoggedIn
      )
    );
  }
}
