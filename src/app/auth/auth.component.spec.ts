import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationService } from './authentication.service';

import { AuthComponent } from './auth.component';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { MatBadgeModule } from '@angular/material/badge';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AlertService } from '../shared/alert.service';
import { AlarmListComponent } from '../shared/alarm-list/alarm-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { MatChipsModule } from '@angular/material/chips';

describe('AuthComponent', () => {
  let component: AuthComponent;
  let fixture: ComponentFixture<AuthComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        OverlayModule,
        MatCardModule,
        MatSnackBarModule,
        MatBadgeModule,
        MatDividerModule,
        MatListModule,
        NoopAnimationsModule,
        MatTooltipModule,
        MatChipsModule
      ],
      providers: [
        AuthenticationService,
        AlertService,
        RequestUrlInterceptor
      ],
      declarations: [
        AuthComponent,
        ToolbarComponent,
        AvatarComponent,
        AlarmListComponent
      ]
    });
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
