import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable, combineLatest } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { User } from 'rest_client/heimdallr/model/user';

import { UserService } from '../users/user.service';

@Injectable({
  providedIn: 'root'
})
export class TenantAdminGuard implements CanActivate {

  constructor(private _router: Router, private _userService: UserService) { }

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return combineLatest([this._userService.user$, this._userService.isTenantAdmin$])
      .pipe(
        map(([user, isTenantAdmin]: [User, boolean]): [string, boolean] => {
          return user?.roles?.id
            ? [user.roles.id, isTenantAdmin]
            : [undefined, false];
        }),
        tap(([id, isTenantAdmin]: [string, boolean]) => {
          if (isTenantAdmin) {
            this._router.navigate(['/tenants', id]);
          }
        }),
        map(([_, isTenantAdmin]: [string, boolean]): boolean => !isTenantAdmin)
      );
  }

}
