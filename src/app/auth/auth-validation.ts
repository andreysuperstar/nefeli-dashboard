import { AbstractControl } from '@angular/forms';

// TODO(eric): for now, set min to 1 until we define password rules
export const MIN_PASSWORD_LENGTH = 1;
export const PASSWORD_PATTERN: RegExp = /^[\w\d_.-@$!%*#?&]*$/;

export const ERROR_MESSAGES = {
  USERNAME: {
    REQUIRED: 'Username is <strong>required</strong>'
  },
  EMAIL: {
    REQUIRED: 'Email is <strong>required</strong>',
    EMAIL: 'Not a <strong>valid</strong> email'
  },
  PASSWORD: {
    REQUIRED: 'Password is <strong>required</strong>',
    MIN_LENGTH: `Min <strong>${MIN_PASSWORD_LENGTH}</strong> characters`,
    PATTERN: 'Letters, numbers and special characters are <strong>allowed</strong>'
  },
  ROLES: {
    REQUIRED: 'Roles is <strong>required</strong>'
  }
};

export const getEmailErrorMessage = (email: AbstractControl): string => {
  let message = '';

  if (email.hasError('required')) {
    message = ERROR_MESSAGES.EMAIL.REQUIRED;
  }

  if (email.hasError('email')) {
    message = ERROR_MESSAGES.EMAIL.EMAIL;
  }

  return message;
};

export const getPasswordErrorMessage = (password: AbstractControl): string => {
  let message = '';

  if (password.hasError('required')) {
    message = ERROR_MESSAGES.PASSWORD.REQUIRED;
  }

  if (password.hasError('minlength')) {
    message = ERROR_MESSAGES.PASSWORD.MIN_LENGTH;
  }

  if (password.hasError('pattern')) {
    message = ERROR_MESSAGES.PASSWORD.PATTERN;
  }

  return message;
};
