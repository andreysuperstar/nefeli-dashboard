import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ValidatorFn, AbstractControl, ValidationErrors } from '@angular/forms';

import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { MIN_PASSWORD_LENGTH, PASSWORD_PATTERN, ERROR_MESSAGES, getEmailErrorMessage, getPasswordErrorMessage } from '../auth-validation';

import { AlertService, AlertType } from '../../shared/alert.service';
import { AuthenticationService } from '../authentication.service';
import { RegisterResp } from 'rest_client/heimdallr/model/registerResp';
import { UserService } from 'src/app/users/user.service';
import { User } from 'rest_client/heimdallr/model/user';

export enum PasswordSetupMode {
  Register,
  Edit,
  Reset
}

@Component({
  selector: 'nef-setup-password',
  templateUrl: './setup-password.component.html',
  styleUrls: ['./setup-password.component.less']
})
export class SetupPasswordComponent implements OnInit, OnDestroy {

  private _passwordForm: FormGroup;
  private _registerSubscription: Subscription;
  private _mode: PasswordSetupMode;
  private _user: User;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _authService: AuthenticationService,
    private _userService: UserService,
    private _activatedRoute: ActivatedRoute
  ) { }

  public ngOnInit() {
    this._mode = this._activatedRoute.snapshot?.data?.mode ?? PasswordSetupMode.Register;
    this._authService.cleanSession();

    this._userService.user$
      .pipe(take(1))
      .subscribe((user: User) => {
        this._user = user;
        this.initPasswordForm();
      });
  }

  public ngOnDestroy() {
    if (this._registerSubscription) {
      this._registerSubscription.unsubscribe();
    }
  }

  private initPasswordForm() {
    const params = (new URL(this._activatedRoute.snapshot.data?.href))?.searchParams;
    const defaultUsername = params?.get('user');

    let username: string | [string, ValidationErrors];

    switch (this._mode) {
      case PasswordSetupMode.Reset:
        username = defaultUsername;
        break;
      case PasswordSetupMode.Edit:
        username = this._user?.username;
        break;
      default:
        username = [defaultUsername, Validators.required];
    }

    this._passwordForm = this._fb.group({
      username,
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(MIN_PASSWORD_LENGTH),
          Validators.pattern(PASSWORD_PATTERN)
        ]
      ],
      confirmPassword: [
        '',
        [
          Validators.required,
          this.confirmPasswordValidator()
        ]
      ]
    });
  }

  private confirmPasswordValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors => {
      if (!this._passwordForm || this._mode === PasswordSetupMode.Edit) {
        return;
      }

      const password: string = this._passwordForm.get('password').value;

      return control.value !== password
        ? {
          confirmPassword: {
            value: control.value
          }
        }
        : undefined;
    };
  }

  public submitPasswordForm() {
    const { value: { username, password, confirmPassword }, invalid } = this._passwordForm;

    if (invalid) {
      return;
    }

    /* The URL is in the format:
    https://<address>/dashboard/?token=session_token#/auth/register
    For some reason, Angular doesn't like that the query parameters comes
    before the fragment? Therefore, can't use the router to access the a
    query parameter. Instead, use document.location.href
    */
    const params = (new URL(this._activatedRoute.snapshot.data?.href))?.searchParams;
    const token = params?.get('token');

    if (this._registerSubscription) {
      this._registerSubscription.unsubscribe();
    }

    switch (this._mode) {
      case PasswordSetupMode.Reset:
        this._registerSubscription = this._authService
          .resetPassword(token, password)
          .subscribe(
            (_: string) => {
              this._alertService.info(AlertType.INFO_RESET_PASSWORD);

              this._router.navigate(['../login']);
            },
            (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_RESET_PASSWORD)
          );
        break;
      case PasswordSetupMode.Edit:
        this._registerSubscription = this._authService
          .editPassword(this._user.username, password, confirmPassword)
          .subscribe(
            (_: string) => {
              this._alertService.info(AlertType.INFO_EDIT_PASSWORD);
            },
            (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_EDIT_PASSWORD)
          );
        break;
      default:
        this._registerSubscription = this._authService
          .register(username, password, token)
          .subscribe(
            (_: RegisterResp) => {
              this._alertService.info(AlertType.INFO_CREATE_USER_SUCCESS);

              this._router.navigate(['../login']);
            },
            (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_CREATE_USER_FAILED)
          );
    }
  }

  public get passwordForm(): FormGroup {
    return this._passwordForm;
  }

  public get usernameErrorMessage(): string {
    return ERROR_MESSAGES.USERNAME.REQUIRED;
  }

  public get emailErrorMessage(): string {
    const email: AbstractControl = this._passwordForm.get('email');

    const message: string = getEmailErrorMessage(email);

    return message;
  }

  public get passwordErrorMessage(): string {
    const password: AbstractControl = this._passwordForm.get('password');

    const message: string = getPasswordErrorMessage(password);

    return message;
  }

  public get PasswordSetupMode(): typeof PasswordSetupMode {
    return PasswordSetupMode;
  }

  public get mode(): PasswordSetupMode {
    return this._mode;
  }
}
