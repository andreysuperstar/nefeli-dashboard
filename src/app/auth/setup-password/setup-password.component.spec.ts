import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';

import { ERROR_MESSAGES } from '../auth-validation';

import { AlertService } from 'src/app/shared/alert.service';
import { AuthenticationService } from '../authentication.service';

import { SetupPasswordComponent, PasswordSetupMode } from './setup-password.component';
import { ActivatedRoute } from '@angular/router';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH as BASE_PATH_STATS } from 'rest_client/pangolin/variables';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { environment } from '../../../environments/environment';
import { UserService } from 'src/app/users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing'

describe('SetupPasswordComponent', () => {
  let component: SetupPasswordComponent;
  let fixture: ComponentFixture<SetupPasswordComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;
  let userService: UserService;
  let loader: HarnessLoader;

  const mockUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule
      ],
      declarations: [SetupPasswordComponent],
      providers: [
        AlertService,
        AuthenticationService,
        UserService,
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              data: {
                href: 'https://10.88.0.81/dashboard?token=tokendata&user=ecarino#/auth/password-reset'
              },
              queryParams: {
                user: 'ecarino'
              }
            }
          }
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_STATS,
          useValue: '/dashboard'
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        }
      ]
    });

    httpMockClient = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupPasswordComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain autofocus', () => {
    expect(el.querySelectorAll('[autofocus]').length).toBe(1, 'render only one control with autofocus');
    expect(el.querySelector('[autofocus]').getAttribute('formcontrolname')).toBe('username');
  });

  it('should return valid \'confirm password\' error message', () => {
    const password: AbstractControl = component['_passwordForm'].get('password');
    const confirmPassword: AbstractControl = component['_passwordForm'].get('confirmPassword');

    password.setValue('12345678');
    confirmPassword.setValue('12345678');

    expect(confirmPassword.errors).toBeNull();

    confirmPassword.setValue('123');

    expect(confirmPassword.hasError('confirmPassword')).toBeTruthy();
    expect(confirmPassword.getError('confirmPassword').value).toBe('123');
  });

  it('should return valid \'password\' error message', () => {
    const password: AbstractControl = component['_passwordForm'].get('password');

    expect(password.errors.required).toBeTruthy('valid \'required\' error for empty password control');
    expect(component.passwordErrorMessage).toBe(ERROR_MESSAGES.PASSWORD.REQUIRED, 'valid \'required\' password error message on blank control');

    // TODO(eric): for now, we disabled minimum length check. renable test below once we renable the validator
    // password.setValue('12345');
    // expect(password.errors.minlength).toBeTruthy('valid \'minlength\' error for too short password control');
    // expect(component.passwordErrorMessage).toBe(ERROR_MESSAGES.PASSWORD.MIN_LENGTH, 'valid \'minlength\' error message on too short password control value');

    password.setValue('12345678');

    expect(password.valid).toBeTruthy('no errors for valid password length');
    expect(component.passwordErrorMessage).toBe('', 'empty error message on valid password control value');

    password.setValue('1234^5~6789');

    expect(password.errors.pattern).toBeTruthy('valid \'pattern\' error for empty password control');
    expect(component.passwordErrorMessage).toBe(ERROR_MESSAGES.PASSWORD.PATTERN, 'valid \'patter\' error message on invalid password symbols control value');
  });

  it('should issue register request on form submit', () => {
    const username: AbstractControl = component['_passwordForm'].get('username');
    const confirmPassword: AbstractControl = component['_passwordForm'].get('confirmPassword');
    const password: AbstractControl = component['_passwordForm'].get('password');
    const el = fixture.debugElement.nativeElement;

    expect(el.querySelector('mat-card-title').textContent.trim()).toBe('Setup password');
    expect(el.querySelector('button').textContent.trim()).toBe('Setup password');

    username.setValue('ecarino');
    password.setValue('secret');
    confirmPassword.setValue('secret');

    spyOn(component['_router'], 'navigate').and.callFake((url: string[]) => {
      expect(url[0]).toBe('../login');

      return Promise.resolve(true);
    });

    component.submitPasswordForm();
    const req = httpMockClient.expectOne(`${environment.restServer}/auth/register`);
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');
    expect(req.request.body.username).toBe('ecarino');
    expect(req.request.body.password).toBe('secret');
    expect(req.request.body.invite_token).toBe('tokendata');
    req.flush({});
  });

  it('should render reset password form', () => {
    component['_mode'] = PasswordSetupMode.Reset;
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;

    const formElems = el.querySelectorAll('mat-form-field');
    expect(formElems.length).toBe(3);

    const inputElems = el.querySelectorAll('input[type="password"]');
    expect(inputElems.length).toBe(2);

    expect(el.querySelector('mat-card-title').textContent.trim()).toBe('Reset password');
    expect(el.querySelector('button').textContent.trim()).toBe('Reset password');
  });

  it('should issue reset password on submit', () => {
    component['_mode'] = PasswordSetupMode.Reset;
    component['initPasswordForm']();
    fixture.detectChanges();

    const confirmPassword: AbstractControl = component['_passwordForm'].get('confirmPassword');
    const password: AbstractControl = component['_passwordForm'].get('password');
    password.setValue('secret');
    confirmPassword.setValue('secret');

    spyOn(component['_router'], 'navigate').and.callFake((url: string[]) => {
      expect(url[0]).toBe('../login');

      return Promise.resolve(true);
    });

    component.submitPasswordForm();
    const req = httpMockClient.expectOne(`${environment.restServer}/auth/password-reset`);
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');
    expect(req.request.body.new_password).toBe('secret');
    expect(req.request.body.otp_token).toBe('tokendata');
    req.flush({});
  });

  it('should pre-populate the username from query param', () => {
    const username: AbstractControl = component['_passwordForm'].get('username');
    expect(username.value).toBe('ecarino');
  });

  it('should change user password', async () => {
    userService.setUser(mockUser);
    component.ngOnInit();
    component['_mode'] = PasswordSetupMode.Edit;
    fixture.detectChanges();

    expect(el.querySelector('mat-card-title').textContent.trim()).toBe('Edit your password');
    expect(el.querySelector('button').textContent.trim()).toBe('Change password');

    fixture.detectChanges();

    const currentPasswordInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter current password'
    }));
    const newPasswordInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      placeholder: 'Enter new password'
    }));
    await currentPasswordInput.setValue('currentPassword');
    await newPasswordInput.setValue('newPassword');

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Change password'
    }));
    await submitButton.click();

    const req = httpMockClient.expectOne(`${environment.restServer}/auth/change-password`);
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');
    expect(req.request.body.new_password).toBe('newPassword');
    expect(req.request.body.current_password).toBe('currentPassword');
    expect(req.request.body.username).toBe('ecarino');
    req.flush({});
  })
});
