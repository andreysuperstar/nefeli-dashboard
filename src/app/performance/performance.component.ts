import { Component, ViewChild, Input, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { ChartDataSet, ChartLineComponent, UnitType, ZoomEvent } from '../shared/chart-line/chart-line.component';

import { Subscription, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Pipeline } from '../pipeline/pipeline.model';
import { LegendItem } from '../shared/legend/legend.component';

import { Cluster, ClusterService } from '../home/cluster.service';
import { Stats } from 'rest_client/pangolin/model/stats';
import { LoggerService } from '../shared/logger.service';
import { CommonChart } from '../charts/common-chart';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { NO_SERVICES_SELECTED } from '../tenant/tenant.service';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';

export enum PERFORMANCE_CHART {
  THROUGHPUT,
  LATENCY,
  PACKET_LOSS
}

const SecToMsecFactor = 1000;

@Component({
  selector: 'nef-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.less']
})

export class PerformanceComponent implements OnDestroy, OnInit {

  private _rate: number = CommonChart.config.RATE;
  private _step: number = CommonChart.config.STEP;
  private _pollInterval: number = CommonChart.config.POLL_INTERVAL;
  private _startTime: number;
  private _throughputDataSets = new Array<ChartDataSet>();
  private _latencyDataSets = new Array<ChartDataSet>();
  private _packetLossDataSets = new Array<ChartDataSet>();
  private _throughputSubscription: Subscription;
  private _latencySubscription: Subscription;
  private _packetLossSubscription: Subscription;
  private _active = false;
  private _activeTab: PERFORMANCE_CHART = PERFORMANCE_CHART.THROUGHPUT;
  private _selectedPipelines: Pipeline[];
  private _throughputLegendItems: LegendItem[];
  private _sites = new Array<Site>();
  private _subscriptions = new Subscription();
  private _zoomEvent = new EventEmitter<ZoomEvent>();

  @Input() public cluster: Cluster;

  @Input() public set active(val: boolean) {
    this._active = val;
    this.updateStatsStream(val);
  }

  @Input() public set selectedPipelines(val: Pipeline[]) {
    this._selectedPipelines = val;
    this.updateStatsStream(true);
  }

  @Input() public set startTime(time: number) {
    this._startTime = time;
    this.updateStatsStream(true);
  }

  @Input() public set rate(rate: number) {
    this._rate = CommonChart.config.RATE;
    if (rate) {
      this._rate = rate;
    }

    this.updateStatsStream(true);
  }

  @ViewChild('throughputTabChart') public throughputTabChart: ChartLineComponent;
  @ViewChild('latencyTabChart') public latencyTabChart: ChartLineComponent;
  @ViewChild('packetLossTabChart') public packetLossTabChart: ChartLineComponent;

  @Output() public chartChange: EventEmitter<PERFORMANCE_CHART> = new EventEmitter();

  @Output() public get zoomEvent(): EventEmitter<ZoomEvent> {
    return this._zoomEvent;
  }

  constructor(
    private _clusterService: ClusterService,
    private _log: LoggerService
  ) { }

  public ngOnInit() {
    this._subscriptions.add(
      this._clusterService.streamSites().subscribe(({ sites: sites }: Sites) => this._sites = sites)
    );
  }

  public ngOnDestroy() {
    this.disableStatsStream();
    this._subscriptions.unsubscribe();
  }

  private postClusterStatsObservable(type: StatFilter, pipelines?: string[], startTime?: number): Observable<any> {
    this._step = CommonChart.getOptimalStep(this._rate);
    this._pollInterval = CommonChart.getOptimalPollInterval(this._rate);
    const obs$ = (this._startTime) ?
      this._clusterService.postClusterStats(
        this.cluster.id,
        [type] as Array<StatFilter>,
        this._rate,
        this._step,
        pipelines,
        this._startTime)
      : this._clusterService.streamClusterStats(
        this.cluster.id,
        this._pollInterval,
        type,
        this._rate,
        this._step,
        pipelines);

    return obs$;
  }

  private enableThroughputStream(pipelines: Pipeline[]) {
    if (this.cluster && this._active) {
      if (this._throughputSubscription) {
        this._throughputSubscription.unsubscribe();
      }
      this._throughputSubscription = this.postClusterStatsObservable(
        StatFilter.THROUGHPUT,
        undefined,
        this._startTime
      ).pipe(
        map((stats: Stats) => CommonChart.handleThroughputStats(stats, this._sites)),
      ).subscribe(
        (dataSets: ChartDataSet[]) => this.updateTabChart(dataSets, StatFilter.THROUGHPUT),
        (error: Error) => {
          this._log.error(error);
          setTimeout(() => {
            this.enableThroughputStream(pipelines);
          }, this._pollInterval);
        }
      );
    }
  }

  private updateTabChart(dataSets: ChartDataSet[], type: StatFilter) {
    const chartArguments = {
      dataSetsSrc: undefined,
      charts: []
    };

    switch (type) {
      case StatFilter.THROUGHPUT:
        chartArguments.dataSetsSrc = this._throughputDataSets;
        chartArguments.charts = [this.throughputTabChart];
        break;
      case StatFilter.LATENCY:
        chartArguments.dataSetsSrc = this._latencyDataSets;
        chartArguments.charts = [this.latencyTabChart];
        break;
      case StatFilter.PACKETLOSS:
        chartArguments.dataSetsSrc = this._packetLossDataSets;
        chartArguments.charts = [this.packetLossTabChart];
        break;
    }

    if (chartArguments.charts[0]) {
      CommonChart.updateChart(
        dataSets,
        chartArguments.dataSetsSrc,
        chartArguments.charts,
        this._rate,
        this._startTime * SecToMsecFactor,
        this._step
      );
    }
  }

  private enableLatencyStream(pipelines: Pipeline[]) {
    if (this.cluster && this._active) {
      if (this._latencySubscription) {
        this._latencySubscription.unsubscribe();
      }

      const pipelineIDs = pipelines && pipelines.length
        ? pipelines.map(({ identifier }) => identifier)
        : [NO_SERVICES_SELECTED[0].identifier];

      this._latencySubscription = this.postClusterStatsObservable(
        StatFilter.LATENCY,
        pipelineIDs,
        this._startTime
      ).pipe(
        map((stats: Stats) => CommonChart.handleLatencyStats(stats, this._sites, pipelines[0].name)),
      ).subscribe(
        (dataSets: ChartDataSet[]) => this.updateTabChart(dataSets, StatFilter.LATENCY),
        (error: Error) => {
          this._log.error(error);
          setTimeout(() => {
            this.enableLatencyStream(pipelines);
          }, this._pollInterval);
        }
      );
    }
  }

  private enablePacketlossStream(pipelines: Pipeline[]) {
    if (this.cluster && this._active) {
      if (this._packetLossSubscription) {
        this._packetLossSubscription.unsubscribe();
      }

      const pipelineIDs = pipelines && pipelines.length
        ? pipelines.map(({ identifier }) => identifier)
        : [NO_SERVICES_SELECTED[0].identifier];

      this._packetLossSubscription = this.postClusterStatsObservable(
        StatFilter.PACKETLOSS,
        pipelineIDs,
        this._startTime
      ).pipe(
        map((stats: Stats) => CommonChart.handlePacketlossStats(stats, this._sites, pipelines[0].name))
      ).subscribe(
        (dataSets: ChartDataSet[]) => this.updateTabChart(dataSets, StatFilter.PACKETLOSS),
        (error: Error) => {
          this._log.error(error);
          setTimeout(() => {
            this.enablePacketlossStream(pipelines);
          }, this._pollInterval);
        }
      );
    }
  }

  private updateStatsStream(enable: boolean) {
    this.disableStatsStream();

    if (enable && this.active) {
      if (this._throughputDataSets) {
        this._throughputDataSets.length = 0;
      }

      if (this._latencyDataSets) {
        this._latencyDataSets.length = 0;
      }

      if (this._packetLossDataSets) {
        this._packetLossDataSets.length = 0;
      }

      let pipelines: Pipeline[];

      if (this.selectedPipelines) {
        pipelines = this.selectedPipelines;
      }

      // enable stats streams
      this.enableThroughputStream(pipelines);
      this.enableLatencyStream(pipelines);
      this.enablePacketlossStream(pipelines);
    }
  }

  private disableStatsStream() {
    if (this._throughputSubscription) {
      this._throughputSubscription.unsubscribe();
      this._throughputSubscription = undefined;
    }

    if (this._latencySubscription) {
      this._latencySubscription.unsubscribe();
      this._latencySubscription = undefined;
    }

    if (this._packetLossSubscription) {
      this._packetLossSubscription.unsubscribe();
      this._packetLossSubscription = undefined;
    }
  }

  public onZoomEvent(event: ZoomEvent) {
    this._zoomEvent.emit(event);
  }

  public set selectedMenuIndex(index: number) {
    this._activeTab = index;
    this.chartChange.emit(index);
  }

  public get selectedMenuIndex(): number {
    return this._activeTab;
  }

  public get throughputDataSets(): ChartDataSet[] {
    return this._throughputDataSets;
  }

  public get latencyDataSets(): ChartDataSet[] {
    return this._latencyDataSets;
  }

  public get packetLossDataSets(): ChartDataSet[] {
    return this._packetLossDataSets;
  }

  public get throughputStats(): Object[] {
    // TODO: we don't support filtering throughput by service yet, so return empty array for now
    return [];
  }

  public get selectedPipelines(): Pipeline[] {
    return this._selectedPipelines;
  }

  public get UnitType(): typeof UnitType {
    return UnitType;
  }

  public get throughputLegendItems(): LegendItem[] {
    return this._throughputLegendItems;
  }

  public get startTime(): number {
    return this._startTime;
  }

  public get rate(): number {
    return this._rate;
  }

  public get clusterId(): string {
    if (this.cluster) {
      return this.cluster.id;
    }
  }

  public get active(): boolean {
    return this._active;
  }
}
