import { ComponentFixture, TestBed, fakeAsync, tick, discardPeriodicTasks } from '@angular/core/testing';

import { PerformanceComponent } from './performance.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ClusterService, Cluster } from '../home/cluster.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../../environments/environment';
import { HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { ChartLineComponent } from '../shared/chart-line/chart-line.component';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { AlertService } from '../shared/alert.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { LoggerService } from '../shared/logger.service';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { ChartThroughputComponent } from '../charts/chart-throughput.component';
import { ChartPacketlossComponent } from '../charts/chart-packetloss.component';
import { ChartLatencyComponent } from '../charts/chart-latency.component';
import { Stats } from 'rest_client/pangolin/model/stats';
import { BASE_PATH as STATS_BASE_PATH } from 'rest_client/pangolin/variables';
import { NO_SERVICES_SELECTED } from '../tenant/tenant.service';
import { Pipeline } from '../pipeline/pipeline.model';
import { ClusterStatsRequest } from 'rest_client/pangolin/model/clusterStatsRequest';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';

describe('PerformanceComponent', () => {
  let component: PerformanceComponent;
  let fixture: ComponentFixture<PerformanceComponent>;
  let httpTestingController: HttpTestingController;
  const mockCluster: Cluster = {
    id: '-1',
    name: 'Cluster A'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatTabsModule,
        HttpClientTestingModule,
        BrowserAnimationsModule,
        MatSnackBarModule
      ],
      declarations: [
        PerformanceComponent,
        ChartLineComponent,
        ChartHeaderComponent,
        LegendComponent,
        ChartThroughputComponent,
        ChartLatencyComponent,
        ChartPacketlossComponent,
      ],
      providers: [
        PipelineNamePipe,
        ClusterService,
        AlertService,
        LoggerService,
        { provide: HTTP_INTERCEPTORS, useClass: RequestUrlInterceptor, multi: true },
        {
          provide: STATS_BASE_PATH,
          useValue: '/v1'
        },
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.cluster = mockCluster;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable packet loss stream', fakeAsync(() => {
    component.active = true;
    const duration = 21600;
    const step = Math.round(duration / 1000);

    tick();
    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.PACKETLOSS
    })[0];

    const body: ClusterStatsRequest = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.PACKETLOSS]
      },
      range: {
        start: undefined,
        duration,
        step
      }
    };
    expect(req.request.body).toEqual(body);
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');

    component.active = false;
  }));

  it('should enable throughput stream', fakeAsync(() => {
    component.active = true;
    const duration = 21600;
    const step = Math.round(duration / 1000);

    tick();
    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.THROUGHPUT
    })[0];

    const body: ClusterStatsRequest = {
      services: undefined,
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration,
        step
      }
    };
    expect(req.request.body).toEqual(body);
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');

    component.active = false;
  }));

  it('should change duration value in the request', fakeAsync(() => {
    const duration = 60;
    const step = 1;
    component.rate = duration;
    component.active = true;
    tick();

    const rate = component.rate;

    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.LATENCY
    })[0];

    const body: ClusterStatsRequest = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.LATENCY]
      },
      range: {
        start: undefined,
        duration: rate,
        step
      }
    };
    expect(req.request.body).toEqual(body);
    expect(req.request.method).toBe('POST');

    discardPeriodicTasks();
    component.active = false;
  }));

  it('should enable latency stream', fakeAsync(() => {
    component.active = true;
    const duration = 21600;
    const step = Math.round(duration / 1000);

    tick();
    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.LATENCY
    })[0];

    const body: ClusterStatsRequest = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.LATENCY]
      },
      range: {
        start: undefined,
        duration,
        step
      }
    };
    expect(req.request.body).toEqual(body);
    expect(req.request.method).toBe('POST');
    expect(req.request.responseType).toBe('json');

    component.active = false;
  }));

  it('should update throughput chart and legends', fakeAsync(() => {
    component.active = true;
    const duration = 21600;
    const step = Math.round(duration / 1000);
    const time = Math.round(new Date().getTime() / 1000);

    tick();
    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.THROUGHPUT
    })[0];

    const body: ClusterStatsRequest = {
      services: undefined,
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    req.flush({
      services: 0,
      coresUsed: 0,
      coresTotal: 0,
      memUsed: 0,
      memTotal: 0,
      throughputOut: [
        [
          time - 1,
          '6364.0252404462035'
        ],
        [
          time,
          '6362.454143548052'
        ]
      ],
      throughputIn: [
        [
          time - 1,
          '6464.0252404462035'
        ],
        [
          time,
          '6462.454143548052'
        ]
      ],
      throughputServiceIn: [],
      throughputServiceOut: [],
      capacity: null,
      packetLoss: [],
      latency: []
    });

    expect(component.throughputDataSets[0].label).toBe('throughputOut');
    expect(Math.round((component.throughputDataSets[0].data[0].x as Date).getTime() / 1000)).toBe(time - duration);
    expect(component.throughputDataSets[0].data[0].y).toBe(0);
    expect(Math.floor((component.throughputDataSets[0].data[1].x as Date).getTime() / 1000)).toBe(time - 2);
    expect(component.throughputDataSets[0].data[1].y).toBe(0);
    expect((component.throughputDataSets[0].data[2].x as Date).getTime() / 1000).toBe(time - 1);
    expect(component.throughputDataSets[0].data[2].y).toBe(6364.0252404462035);
    expect((component.throughputDataSets[0].data[3].x as Date).getTime() / 1000).toBe(time);
    expect(component.throughputDataSets[0].data[3].y).toBe(6362.454143548052);

    expect(component.throughputDataSets[1].label).toBe('throughputIn');
    expect(Math.round((component.throughputDataSets[0].data[0].x as Date).getTime() / 1000)).toBe(time - duration);
    expect(component.throughputDataSets[0].data[0].y).toBe(0);
    expect(Math.floor((component.throughputDataSets[0].data[1].x as Date).getTime() / 1000)).toBe(time - 2);
    expect(component.throughputDataSets[0].data[1].y).toBe(0);
    expect((component.throughputDataSets[1].data[2].x as Date).getTime() / 1000).toBe(time - 1);
    expect(component.throughputDataSets[1].data[2].y).toBe(6464.0252404462035);
    expect((component.throughputDataSets[1].data[3].x as Date).getTime() / 1000).toBe(time);
    expect(component.throughputDataSets[1].data[3].y).toBe(6462.454143548052);

    component.active = false;
  }));

  it('should update latency chart', fakeAsync(() => {
    component.selectedPipelines = NO_SERVICES_SELECTED;
    component.active = true;
    const duration = 21600;
    const step = Math.round(duration / 1000);
    const time = Math.round(new Date().getTime() / 1000);

    tick();
    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.LATENCY
    })[0];

    const body: ClusterStatsRequest = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.LATENCY]
      },
      range: {
        start: undefined,
        duration,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    req.flush({
      id: '0',
      services: 0,
      coresUsed: 0,
      coresTotal: 0,
      memUsed: 0,
      memTotal: 0,
      throughput: 0,
      throughputServiceOut: [],
      throughputServiceIn: [],
      capacity: null,
      packetLoss: [],
      throughputOut: [],
      throughputIn: [],
      latency: [
        {
          'site': 'eric_site',
          'name': 'sample',
          'stats': [
            [
              time - 1,
              '1'
            ],
            [
              time,
              '2'
            ]
          ]
        }
      ],
      status: 'good'
    } as Stats);

    expect(component.latencyDataSets[0].label).toBe(`eric_site: ${NO_SERVICES_SELECTED[0].name}`);
    expect(Math.round((component.latencyDataSets[0].data[0].x as Date).getTime() / 1000)).toBe(time - duration);
    expect(component.latencyDataSets[0].data[0].y).toBe(0);
    expect(Math.floor((component.latencyDataSets[0].data[1].x as Date).getTime() / 1000)).toBe(time - 2);
    expect(component.latencyDataSets[0].data[1].y).toBe(0);
    expect((component.latencyDataSets[0].data[2].x as Date).getTime() / 1000).toBe(time - 1);
    expect(component.latencyDataSets[0].data[2].y).toBe(1);
    expect((component.latencyDataSets[0].data[3].x as Date).getTime() / 1000).toBe(time);
    expect(component.latencyDataSets[0].data[3].y).toBe(2);

    component.active = false;
  }));

  it('should update packet loss chart', fakeAsync(() => {
    component.selectedPipelines = NO_SERVICES_SELECTED;
    component.active = true;
    const duration = 21600;
    const step = Math.round(duration / 1000);
    const time = Math.round(new Date().getTime() / 1000);

    tick();
    const req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats` && req.body.filter.filter[0] === StatFilter.PACKETLOSS
    })[0];

    const body: ClusterStatsRequest = {
      services: ['-none-'],
      filter: {
        filter: [StatFilter.PACKETLOSS]
      },
      range: {
        start: undefined,
        duration,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    req.flush({
      id: '0',
      services: 0,
      coresUsed: 0,
      coresTotal: 0,
      memUsed: 0,
      memTotal: 0,
      throughput: 0,
      throughputServiceOut: [],
      throughputServiceIn: [],
      capacity: 0,
      packetLoss: [
        {
          'name': 'Unclassified',
          'site': 'eric_site',
          'stats': [
            [time - 1, '514897224'],
            [time, '514923717']
          ]
        },
        {
          'name': 'sample',
          'site': 'eric_site',
          'stats': [
            [time - 1, '0'],
            [time, '0']
          ]
        }
      ],
      latency: [],
      throughputOut: [],
      throughputIn: [],
      status: 'good'
    } as Stats);

    expect(component.packetLossDataSets[0].label).toBe(`eric_site: ${NO_SERVICES_SELECTED[0].name}`);
    expect(Math.round((component.packetLossDataSets[0].data[0].x as Date).getTime() / 1000)).toBe(time - duration);
    expect(component.packetLossDataSets[0].data[0].y).toBe(0);
    expect(Math.floor((component.packetLossDataSets[0].data[1].x as Date).getTime() / 1000)).toBe(time - 2);
    expect(component.packetLossDataSets[0].data[1].y).toBe(0);
    expect((component.packetLossDataSets[0].data[2].x as Date).getTime() / 1000).toBe(time - 1);
    expect(component.packetLossDataSets[0].data[2].y).toBe(514897224);
    expect((component.packetLossDataSets[0].data[3].x as Date).getTime() / 1000).toBe(time);
    expect(component.packetLossDataSets[0].data[3].y).toBe(514923717);

    expect(Math.round((component.packetLossDataSets[0].data[0].x as Date).getTime() / 1000)).toBe(time - duration);
    expect(component.packetLossDataSets[0].data[0].y).toBe(0);
    expect(Math.floor((component.packetLossDataSets[0].data[1].x as Date).getTime() / 1000)).toBe(time - 2);
    expect(component.packetLossDataSets[0].data[1].y).toBe(0);

    component.active = false;
  }));

  it('should request for loss & latency filtered by pipeline and start time', fakeAsync(() => {
    component.active = true;

    const mockPipelines: Pipeline[] = [
      {
        identifier: '-1',
        name: 'Pipeline 1'
      },
      {
        identifier: '-2',
        name: 'Pipeline 2'
      }
    ];

    component.selectedPipelines = mockPipelines;

    const now = new Date().getTime();
    component.startTime = now;

    let req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats`
        && req.body.filter.filter[0] === StatFilter.PACKETLOSS
        && req.body.range.start === now
    })[0];
    let body: ClusterStatsRequest = {
      services: [mockPipelines[0].identifier, mockPipelines[1].identifier],
      filter: {
        filter: [StatFilter.PACKETLOSS]
      },
      range: {
        start: now,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);

    req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/-1/stats`
        && req.body.filter.filter[0] === StatFilter.LATENCY
        && req.body.range.start === now
    })[0];
    body = {
      services: [mockPipelines[0].identifier, mockPipelines[1].identifier],
      filter: {
        filter: [StatFilter.LATENCY]
      },
      range: {
        start: now,
        duration: 21600,
        step: 22
      }
    };
    expect(req.request.body).toEqual(body);

    component.active = false;
  }));
});
