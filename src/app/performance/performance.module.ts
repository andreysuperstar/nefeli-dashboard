
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { SharedModule } from '../shared/shared.module';
import { PerformanceComponent } from './performance.component';
import { ChartsModule } from '../charts/charts.module';

@NgModule({
  imports: [
    CommonModule,
    MatTabsModule,
    SharedModule,
    ChartsModule
  ],
  declarations: [
    PerformanceComponent
  ],
  exports: [
    PerformanceComponent
  ]
})
export class PerformanceModule { }
