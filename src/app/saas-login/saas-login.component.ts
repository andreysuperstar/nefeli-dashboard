import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { AuthenticationService } from '../auth/authentication.service';
import { ERROR_MESSAGES, getPasswordErrorMessage, MIN_PASSWORD_LENGTH } from '../auth/auth-validation';
import { User } from 'rest_client/heimdallr/model/user';
import { HttpErrorResponse } from '@angular/common/http';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';

@Component({
  selector: 'nef-saas-login',
  templateUrl: './saas-login.component.html',
  styleUrls: ['./saas-login.component.less']
})
export class SaasLoginComponent implements OnInit, OnDestroy {
  private _loginForm: FormGroup;
  private _loading = false;
  private _errorMessage: string;
  private _subscription = new Subscription();
  private _loginSubscription: Subscription;

  constructor(
    private _fb: FormBuilder, private _router: Router,
    private _authenticationService: AuthenticationService,
    private _localStorageService: LocalStorageService,
    private _zone: NgZone) { }

  public ngOnInit() {
    this.initLoginForm();

    const subscription = this._loginForm.valueChanges.subscribe((_: any) => {
      this._errorMessage = undefined;
    });

    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    this._loginSubscription?.unsubscribe();
    this._subscription.unsubscribe();
  }

  private initLoginForm() {
    this._loginForm = this._fb.group({
      username: ['', Validators.required],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(MIN_PASSWORD_LENGTH)
        ]
      ]
    });
  }

  public submitLoginForm() {
    const { value: { username, password }, invalid } = this._loginForm;

    if (invalid) {
      return;
    }

    this._loading = true;

    if (this._loginSubscription) {
      this._loginSubscription.unsubscribe();
    }

    this._loginSubscription = this._authenticationService
      .login(username, password)
      .subscribe({
        next: (_: User) => {
          this._loading = false;
          this._localStorageService.write(LocalStorageKey.IS_SAAS, true);
          this._zone.run(() => this._router.navigate(['/saas/home']));
        },
        error: ({ error }: HttpErrorResponse) => {
          this._loading = false;

          this._errorMessage = error;
        }
      });
  }

  public get loginForm(): FormGroup {
    return this._loginForm;
  }

  public get usernameErrorMessage(): string {
    return ERROR_MESSAGES.USERNAME.REQUIRED;
  }

  public get passwordErrorMessage(): string {
    const password: AbstractControl = this._loginForm.get('password');

    return getPasswordErrorMessage(password);
  }

  public get errorMessage(): string {
    return this._errorMessage;
  }

  public get loading(): boolean {
    return this._loading;
  }

}
