import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SaasAuthenticationGuard } from '../auth/saas.authentication.guard';
import { PageDataKey } from '../toolbar/toolbar.component';
import { SaasLoginComponent } from './saas-login.component';

const routes: Routes = [
  {
    path: '',
    component: SaasLoginComponent,
    canActivate: [SaasAuthenticationGuard],
    data: {
      [PageDataKey.LoginPage]: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SaasLoginRoutingModule { }
