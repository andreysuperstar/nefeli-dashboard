import { Md5 } from 'ts-md5/dist/md5';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router, RouterEvent } from '@angular/router';
import { ConnectedPosition } from '@angular/cdk/overlay';

import { Observable, of, Subscription } from 'rxjs';
import { catchError, filter, pluck, startWith } from 'rxjs/operators';
import { User } from 'rest_client/heimdallr/model/user';
import { AuthenticationService } from '../auth/authentication.service';
import { Alarm, AlarmsCount, AlarmService, Severity } from '../shared/alarm.service';
import { LoggerService } from '../shared/logger.service';
import { UserService } from '../users/user.service';
import { SidebarService } from '../shared/sidebar.service';
import { AlarmDataSource } from '../shared/alarm.data-source';
import { AVATAR_MODE } from '../shared/avatar/avatar.component';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';

export const Config = {
  menuHoverTimeout: 500
};

export enum PageDataKey {
  LoginPage = 'isLogin',
  NetworkFunctionsPage = 'isNFs',
  ConnectivityPage = 'isConnectivity',
  SettingsPage = 'isSettings',
  ServicesPage = 'isServices'
}

@Component({
  selector: 'nef-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.less']
})
export class ToolbarComponent implements OnInit, OnDestroy {
  private _isLoginPage: boolean;
  private _isNFsPage: boolean;
  private _isConnectivityPage: boolean;
  private _isSettingsPage: boolean;
  private _isServicesPage: boolean;
  private _user$: Observable<User> = this._userService.user$;
  private _isNFsDropdownOpen = false;
  private _isConnectivityDropdownOpen = false;
  private _isServicesDropdownOpen = false;
  private _isSaas = false;

  private _menuDropdownConnectedPositions: ConnectedPosition[] = [
    {
      originX: 'start',
      originY: 'bottom',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: 7
    }
  ];

  private _isSettingsDropdownOpen = false;
  private _isUserDropdownOpen = false;

  private _secondaryMenuDropdownConnectedPositions: ConnectedPosition[] = [
    {
      originX: 'end',
      originY: 'bottom',
      overlayX: 'end',
      overlayY: 'top',
      offsetY: 7
    }
  ];

  private _alarms: Alarm[];
  private _isAlarmDropdownOpen = false;

  private _alarmDropdownConnectedPositions: ConnectedPosition[] = [
    {
      originX: 'center',
      originY: 'bottom',
      overlayX: 'center',
      overlayY: 'top',
      offsetY: -3
    }
  ];

  private _subscription = new Subscription();
  private _alarmsSubscription: Subscription;
  private _alarmsDataSourceSubscription: Subscription;
  private _unreadAlarmsCount = 0;
  private _alarmsDataSource: AlarmDataSource;
  private _emailHash: string;
  private _gravatarUrl: string;
  private _nfsDropdownHover: boolean;
  private _connectivityDropdownHover: boolean;
  private _servicesDropdownHover: boolean;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _authenticationService: AuthenticationService,
    private _userService: UserService,
    private _alarmService: AlarmService,
    private _sidebarService: SidebarService,
    private _log: LoggerService,
    private _localStorageService: LocalStorageService
  ) { }

  public ngOnInit() {
    const isLoginSubscription = this._router.events
      .pipe(
        filter((event: RouterEvent): boolean => event instanceof NavigationEnd),
        startWith(undefined as string)
      )
      .subscribe((_: RouterEvent) => {
        const isRoutePageDataProperty = (
          dataKey: PageDataKey,
          { data, children } = this._route.snapshot
        ): boolean => data.hasOwnProperty(dataKey)
            ? data[dataKey] === true
            : children.some(childRoute => isRoutePageDataProperty(dataKey, childRoute));

        this._isLoginPage = isRoutePageDataProperty(PageDataKey.LoginPage);
        this._isConnectivityPage = isRoutePageDataProperty(PageDataKey.ConnectivityPage);
        this._isNFsPage = isRoutePageDataProperty(PageDataKey.NetworkFunctionsPage);
        this._isSettingsPage = isRoutePageDataProperty(PageDataKey.SettingsPage);
        this._isServicesPage = isRoutePageDataProperty(PageDataKey.ServicesPage);
      });

    this._subscription.add(isLoginSubscription);
    this.listenForLoginEvents();
  }

  private listenForLoginEvents() {
    this._subscription.add(
      this._authenticationService.isLoggedIn$.subscribe((isLoggedIn: boolean) => {
        if (isLoggedIn) {
          this.startAlarmsStream();
          this.fetchGravatar();
          this._isSaas = this._localStorageService.read(LocalStorageKey.IS_SAAS) === 'true';
        } else {
          this.stopAlarmsStream();
          this._isSaas = false;
        }
      })
    );
  }

  private startAlarmsStream() {
    if (this._isLoginPage) {
      return;
    }

    if (this._alarmsSubscription) {
      this._alarmsSubscription.unsubscribe();
    }

    this._alarmsSubscription = this._alarmService.streamAlarmsCount$.subscribe(
      (count: AlarmsCount) => this._unreadAlarmsCount = count.total);

    this._subscription.add(this._alarmsSubscription);
  }

  private fetchGravatar() {
    if (this._isLoginPage) {
      return;
    }
    const emailSubscription = this.email$.subscribe(
      (email: string) => {
        try {
          this._emailHash = Md5.hashStr(email.trim().toLowerCase()) as string;
          this._userService.getGravatarUrl(this._emailHash).subscribe(
            (gravatarUrl: string) => this._gravatarUrl = gravatarUrl
          );
        } catch (e) {
          this._log.error(`FAILED to get gravatar by email: ${e}`);
        }
      }
    );

    this._subscription.add(emailSubscription);
  }

  private stopAlarmsStream() {
    if (this._alarmsSubscription) {
      this._alarmsSubscription.unsubscribe();
    }
  }

  public closeDropdowns() {
    this._isNFsDropdownOpen = false;
    this._isConnectivityDropdownOpen = false;
    this._isServicesDropdownOpen = false;
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  public closeConnectivityDropdown() {
    this._isConnectivityDropdownOpen = false;
  }

  public toggleSettingsDropdown() {
    this._isSettingsDropdownOpen = !this._isSettingsDropdownOpen;
  }

  public closeSettingsDropdown() {
    this._isSettingsDropdownOpen = false;
  }

  public toggleUserDropdown() {
    this._isUserDropdownOpen = !this._isUserDropdownOpen;
  }

  public closeUserDropdown() {
    this._isUserDropdownOpen = false;
  }

  public closeServicesDropdown() {
    this._isServicesDropdownOpen = false;
  }

  public logout() {
    this.closeUserDropdown();
    this._authenticationService.logout();
  }

  public openNFsDropdown() {
    this._isNFsDropdownOpen = true;
  }

  public closeNFsDropdown() {
    this._isNFsDropdownOpen = false;
  }

  public toggleAlarmDropdown() {
    this._isAlarmDropdownOpen
      ? this.closeAlarmDropdown()
      : this.openAlarmDropdown();
  }

  public openAlarmDropdown() {
    this._isAlarmDropdownOpen = true;

    this._alarmsDataSource = new AlarmDataSource(this._alarmService);
    this._alarmsDataSource.severity = [Severity.Critical, Severity.Error, Severity.Warn];
    this._alarmsDataSource.getAll = false;
    this._alarmsDataSourceSubscription = this._alarmsDataSource.newAlarmStream$.subscribe((alarms: Alarm[]) => {
      if (alarms && alarms.length && alarms[0]) {
        this.username$.subscribe((username: string) =>
          this._alarmService.markAsRead(username, alarms).subscribe());
      }
    });

    /* TODO(eric): find more elegant solution
    If the alarm dropdown is open, and new alarms are received,
    it's a bit tricky on how to handle this situation. For now, just
    stop the alarms stream and resume when this dropdown is closed. */
    this.stopAlarmsStream();
  }

  public closeAlarmDropdown() {
    this._isAlarmDropdownOpen = false;

    if (this._alarmsDataSourceSubscription) {
      this._alarmsDataSourceSubscription.unsubscribe();
    }

    this._alarmsDataSource = undefined;

    this.startAlarmsStream();
  }

  public onNFsMouseLeave() {
    this._nfsDropdownHover = false;
    window.setTimeout(() => {
      if (this._nfsDropdownHover === false) {
        this._isNFsDropdownOpen = false;
      }
    }, Config.menuHoverTimeout);
  }

  public onConnectivityMouseLeave() {
    this._connectivityDropdownHover = false;
    window.setTimeout(() => {
      if (this._connectivityDropdownHover === false) {
        this._isConnectivityDropdownOpen = false;
      }
    }, Config.menuHoverTimeout);
  }

  public onServiceMouseLeave() {
    this._servicesDropdownHover = false;
    window.setTimeout(() => {
      if (this._servicesDropdownHover === false) {
        this._isServicesDropdownOpen = false;
      }
    }, Config.menuHoverTimeout);
  }

  public set connectivityDropdownHover(value: boolean) {
    this._connectivityDropdownHover = value;
  }

  public set nfsDropdownHover(value: boolean) {
    this._nfsDropdownHover = value;
  }

  public set servicesDropdownHover(value: boolean) {
    this._servicesDropdownHover = value;
  }

  public set isConnectivityDropdownOpen(value: boolean) {
    this._isConnectivityDropdownOpen = value;
  }

  public set isNFsDropdownOpen(value: boolean) {
    this._isNFsDropdownOpen = value;
  }

  public set isServicesDropdownOpen(value: boolean) {
    this._isServicesDropdownOpen = value;
  }

  public get isLoggedIn$(): Observable<boolean> {
    return this._authenticationService.isLoggedIn$;
  }

  public get isTenantUser$(): Observable<boolean> {
    return this._userService.isTenantUser$;
  }

  public get isTenant$(): Observable<boolean> {
    return this._userService.isTenant$;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get isLoginPage(): boolean {
    return this._isLoginPage;
  }

  public get isNFsPage(): boolean {
    return this._isNFsPage;
  }

  public get isConnectivityPage(): boolean {
    return this._isConnectivityPage;
  }

  public get isSettingsPage(): boolean {
    return this._isSettingsPage;
  }

  public get isServicesPage(): boolean {
    return this._isServicesPage;
  }

  public get username$(): Observable<string> {
    return this._user$.pipe(
      pluck('username'),
      catchError(() => {
        // This is necessary for e2e test. Firefox fails when a button does not contain text.
        return of('...');
      })
    );
  }

  public get user$(): Observable<User> {
    return this._user$;
  }

  public get email$(): Observable<string> {
    return this._user$.pipe(
      pluck('email')
    );
  }

  public get isNFsDropdownOpen(): boolean {
    return this._isNFsDropdownOpen;
  }

  public get isConnectivityDropdownOpen(): boolean {
    return this._isConnectivityDropdownOpen;
  }

  public get isServicesDropdownOpen(): boolean {
    return this._isServicesDropdownOpen;
  }

  public get menuDropdownConnectedPositions(): ConnectedPosition[] {
    return this._menuDropdownConnectedPositions;
  }

  public get isSettingsDropdownOpen(): boolean {
    return this._isSettingsDropdownOpen;
  }

  public get isUserDropdownOpen(): boolean {
    return this._isUserDropdownOpen;
  }

  public get secondaryMenuDropdownConnectedPositions(): ConnectedPosition[] {
    return this._secondaryMenuDropdownConnectedPositions;
  }

  public get alarms(): Alarm[] {
    return this._alarms;
  }

  public get isAlarmDropdownOpen(): boolean {
    return this._isAlarmDropdownOpen;
  }

  public get alarmDropdownConnectedPositions(): ConnectedPosition[] {
    return this._alarmDropdownConnectedPositions;
  }

  public get alarmsCount(): number {
    return this._alarms ? this._alarms.length : 0;
  }

  public get alarmsDataSource(): AlarmDataSource {
    return this._alarmsDataSource;
  }

  public get unreadAlarmsCount(): number {
    return this._unreadAlarmsCount;
  }

  public get badgeAlarmsCount(): string {
    const maxAlarmCount = 99;
    let count = this._unreadAlarmsCount.toString();

    if (this._unreadAlarmsCount > maxAlarmCount) {
      count = `${maxAlarmCount}+`;
    }
    return count;
  }

  public get gravatarUrl(): string {
    return this._gravatarUrl;
  }

  public showAllNotifications() {
    this._sidebarService.open();
    this.closeAlarmDropdown();
  }

  public get AVATAR_MODE(): typeof AVATAR_MODE {
    return AVATAR_MODE;
  }

  public get isSaas(): boolean {
    return this._isSaas;
  }

  public get router(): Router {
    return this._router;
  }
}
