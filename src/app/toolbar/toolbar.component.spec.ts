import { By } from '@angular/platform-browser';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DOCUMENT, Location } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { OverlayModule } from '@angular/cdk/overlay';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { AlertService } from '../shared/alert.service';
import { AuthenticationService } from '../auth/authentication.service';
import { UserService } from '../users/user.service';
import { AlarmsCount, AlarmService } from '../shared/alarm.service';

import { ToolbarComponent, Config } from './toolbar.component';
import { DetailedMenuComponent } from '../shared/detailed-menu/detailed-menu.component';
import { DetailedMenuItemComponent } from '../shared/detailed-menu-item/detailed-menu-item.component';
import { AvatarComponent } from '../shared/avatar/avatar.component';
import { AlarmListComponent } from '../shared/alarm-list/alarm-list.component';
import { LoginComponent } from '../auth/login/login.component';
import { NetworkFunctionsComponent } from '../network-functions/network-functions.component';
import { TunnelsComponent } from '../tunnels/tunnels.component';
import { UsersComponent } from '../users/users.component';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;
  let httpTestingController: HttpTestingController;

  let location: Location;
  let document: Document;
  let toolbarDe: DebugElement;
  let toolbarEl: HTMLElement;
  let authenticationService: AuthenticationService;
  let userService: UserService;
  let localStorageService: LocalStorageService;

  const mockAdminUser: any = {
    firstName: 'Andrew',
    lastName: 'Smith',
    username: 'Andrew',
    email: 'andrew@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  const mockTenantUser: any = {
    username: 'Anton',
    email: 'anton@nefeli.io',
    roles: {
      scope: 'tenant',
      id: '-1',
      roles: ['user']
    }
  };

  const mockTenantAdmin: any = {
    username: 'TenantAdmin',
    email: 'tenantadmin@nefeli.io',
    roles: {
      scope: 'tenant',
      id: '-1',
      roles: ['admin']
    }
  };

  const mockSystemUser: any = {
    username: 'TenantAdmin',
    email: 'tenantadmin@nefeli.io',
    roles: JSON.stringify({
      scope: 'system',
      id: '-1',
      roles: ['user']
    })
  };

  const gravatarUrl = `https://www.gravatar.com/avatar`;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          {
            path: 'auth/login',
            component: LoginComponent
          },
          {
            path: 'nfs',
            component: NetworkFunctionsComponent
          },
          {
            path: 'tunnels',
            component: TunnelsComponent
          },
          {
            path: 'users',
            component: UsersComponent
          }
        ]),
        OverlayModule,
        MatSnackBarModule,
        MatCardModule,
        MatBadgeModule,
        MatDividerModule,
        MatListModule,
        ReactiveFormsModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatTooltipModule,
        MatChipsModule
      ],
      providers: [
        AlertService,
        AlarmService,
        AuthenticationService,
        UserService,
        RequestUrlInterceptor,
        {
          // base path for auto-generated rest_client
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        LocalStorageService
      ],
      declarations: [
        ToolbarComponent,
        DetailedMenuComponent,
        DetailedMenuItemComponent,
        AvatarComponent,
        AlarmListComponent,
        LoginComponent,
        DetailedMenuComponent,
        DetailedMenuItemComponent
      ]
    });

    location = TestBed.inject(Location);
    document = TestBed.inject(DOCUMENT);
    authenticationService = TestBed.inject(AuthenticationService);
    userService = TestBed.inject(UserService);
    httpTestingController = TestBed.inject(HttpTestingController);
    localStorageService = TestBed.inject(LocalStorageService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;

    toolbarDe = fixture.debugElement;
    toolbarEl = toolbarDe.nativeElement;

    authenticationService['_isLoggedIn$'].next(true);
    userService.setUser(mockAdminUser);

    fixture.detectChanges();
  });

  afterEach(() => {
    localStorageService.write(LocalStorageKey.IS_SAAS, false);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display the logo', () => {
    expect(toolbarEl.querySelectorAll('.logo img').length).toBe(1);
  });

  it('should hide menu links', () => {
    // system admin should see menu links
    expect(toolbarEl.querySelectorAll('.menu a').length).toBe(2);
    // NFs and Connectivity buttons
    expect(toolbarEl.querySelectorAll('.menu button').length).toBe(3);
    // settings and user buttons
    expect(toolbarEl.querySelectorAll('.secondary-menu button').length).toBe(2);

    userService.setUser(mockTenantAdmin);
    fixture.detectChanges();

    // tenant admin should only see settings and user buttons
    expect(toolbarEl.querySelectorAll('.menu a').length).toBe(0);
    expect(toolbarEl.querySelectorAll('.menu button').length).toBe(0);
    expect(toolbarEl.querySelectorAll('.secondary-menu button').length).toBe(2);

    userService.setUser(mockSystemUser);
    fixture.detectChanges();

    // system user should see menu links
    expect(toolbarEl.querySelectorAll('.menu a').length).toBe(2);
    // NFs and Connectivity buttons
    expect(toolbarEl.querySelectorAll('.menu button').length).toBe(3);
    // settings and user buttons
    expect(toolbarEl.querySelectorAll('.secondary-menu button').length).toBe(2);

    userService.setUser(mockTenantUser);
    fixture.detectChanges();

    // tenant user should only see user button
    expect(toolbarEl.querySelectorAll('.menu a').length).toBe(0);
    expect(toolbarEl.querySelectorAll('.menu button').length).toBe(0);
    expect(toolbarEl.querySelectorAll('.secondary-menu button').length).toBe(1);
  });

  it('should display alarm button', () => {
    expect(toolbarEl.querySelector('.alarms button')).not.toBeNull();
  });

  it('should not display alarm button when not logged in', () => {
    authenticationService['_isLoggedIn$'].next(false);

    fixture.detectChanges();

    expect(toolbarEl.querySelector('.alarms button')).toBeNull();
  });

  it('should display the login button when not logged in', () => {
    authenticationService['_isLoggedIn$'].next(false);

    fixture.detectChanges();

    expect(toolbarEl.querySelector('.login').textContent).toBe('Log In', 'valid login link text');
  });

  it('should hide NFs, Connectivity and Settings buttons for tenant user', () => {
    let [nfsButtonDe, connectivityButtonDe] = fixture.debugElement.queryAll(By.css('.menu button'));
    let settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));

    expect(nfsButtonDe).toBeDefined('render NFs button for admin user');
    expect(connectivityButtonDe).toBeDefined('render Connectivity button for admin user');
    expect(settingsButtonDe).toBeDefined('render Settings button for admin user');

    userService.setUser(mockTenantUser);
    fixture.detectChanges();

    [nfsButtonDe, connectivityButtonDe] = fixture.debugElement.queryAll(By.css('.menu button'));
    settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));

    expect(nfsButtonDe).toBeUndefined('hide NFs button for tenant user');
    expect(connectivityButtonDe).toBeUndefined('hide Connectivity button for tenant user');
    expect(settingsButtonDe).toBeNull('hide Settings button for tenant user');
  });

  it('should display the user avatar when logged in', () => {
    expect(toolbarEl.querySelector('nef-avatar').getAttribute('url')).toBeDefined('defined url attribute on nef-avatar');
  });

  it('should fetch gavatar', fakeAsync(() => {
    const req = httpTestingController.expectOne(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);
    expect(req).toBeTruthy();
  }));

  it('should display first letter of username with no gravatar', fakeAsync(() => {
    const mockNonameUser: any = {
      username: 'Test',
      email: 'testgravatar@nefeli.io',
      roles: JSON.stringify({
        scope: 'tenant',
        id: '-1',
        roles: ['user']
      })
    }
    userService.setUser(mockNonameUser);
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);
    req.flush({}, {
      status: 401,
      statusText: 'Not found'
    });
    expect(toolbarEl.querySelector('nef-avatar').getAttribute('url')).toBeDefined('defined url attribute on nef-avatar');
    expect(toolbarEl.querySelector('.cap-letter').textContent.trim()).toBe('T');
  }));

  it('should display first letter of first name', fakeAsync(() => {
    const mockFirstNameUser: any = {
      username: 'Test',
      firstName: 'Firstname',
      email: 'testgravatar@nefeli.io',
      roles: JSON.stringify({
        scope: 'tenant',
        id: '-1',
        roles: ['user']
      })
    }
    userService.setUser(mockFirstNameUser);
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);
    req.flush({}, {
      status: 401,
      statusText: 'Not found'
    });
    expect(toolbarEl.querySelector('nef-avatar').getAttribute('url')).toBeDefined('defined url attribute on nef-avatar');
    expect(toolbarEl.querySelector('.cap-letter').textContent.trim()).toBe('F');
  }));

  it('should display first letter of last name', fakeAsync(() => {
    const mockLastNameUser: any = {
      username: 'Test',
      lastName: 'Lastname',
      email: 'testgravatar@nefeli.io',
      roles: JSON.stringify({
        scope: 'tenant',
        id: '-1',
        roles: ['user']
      })
    }
    userService.setUser(mockLastNameUser);
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);
    req.flush({}, {
      status: 401,
      statusText: 'Not found'
    });
    expect(toolbarEl.querySelector('nef-avatar').getAttribute('url')).toBeDefined('defined url attribute on nef-avatar');
    expect(toolbarEl.querySelector('.cap-letter').textContent.trim()).toBe('L');
  }));

  it('should display first letters of first name and last name', fakeAsync(() => {
    userService.setUser(mockTenantUser);
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);
    req.flush({}, {
      status: 401,
      statusText: 'Not found'
    });
    expect(toolbarEl.querySelector('nef-avatar').getAttribute('url')).toBeDefined('defined url attribute on nef-avatar');
    expect(toolbarEl.querySelector('.cap-letter').textContent.trim()).toBe('A');
  }));

  it('should display gravatar', fakeAsync(() => {
    userService.setUser(mockTenantUser);
    fixture.detectChanges();

    const req = httpTestingController.expectOne(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);
    req.flush({});
    fixture.detectChanges();
    const avatarEl: HTMLElement = toolbarEl.querySelector('nef-avatar');
    const avatarImg: HTMLImageElement = avatarEl.querySelector('img');
    expect(avatarImg.getAttribute('src')).toBe(`${gravatarUrl}/be56583461a289ddee368a48f41cf0a2?d=404`);

  }));

  it('should display and hide NFs dropdown', fakeAsync(() => {
    spyOn(component['_router'], 'navigate');
    const nfsButtonDe = fixture.debugElement.query(By.css('.menu button'));
    let nfsDropdownEl = document.querySelector<HTMLUnknownElement>('nfs-dropdown');

    expect(nfsDropdownEl).toBeNull('hide NFs dropdown initially');

    nfsButtonDe.triggerEventHandler('mouseenter', {});
    fixture.detectChanges();

    nfsDropdownEl = document.querySelector('nef-detailed-menu');

    expect(nfsDropdownEl).toBeDefined('render NFs dropdown');

    const menuItemEls = nfsDropdownEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');

    expect(menuItemEls.length).toBe(2);

    expect(menuItemEls[0].querySelector('h1').textContent).toBe('NF Catalog', 'render NF Catalog title');
    expect(menuItemEls[0].querySelector('p').textContent).toBe('Manage network functions, add new NF and delete old Network functions', 'render NF Catalog description');

    expect(menuItemEls[1].querySelector('h1').textContent).toBe('Licensing', 'render Licensing title');
    expect(menuItemEls[1].querySelector('p').textContent).toBe('Manage licenses and license pools', 'render Licensing description');

    nfsButtonDe.triggerEventHandler('click', {});
    expect(component['_router'].navigate).toHaveBeenCalledWith(['/nfs']);

    nfsButtonDe.triggerEventHandler('mouseleave', {});
    tick(Config.menuHoverTimeout);
    fixture.detectChanges();

    nfsDropdownEl = document.querySelector('nef-detailed-menu');

    expect(nfsDropdownEl).toBeNull('hide NFs dropdown');
  }));

  it('should navigate to Network Functions', async () => {
    const nfsButtonDe = fixture.debugElement.query(By.css('.menu button'));

    nfsButtonDe.triggerEventHandler('mouseenter', {});
    fixture.detectChanges();

    const nfsMenuEl = document.querySelector<HTMLUnknownElement>('.nfs-dropdown nef-detailed-menu');
    const nfsMenuItemEls = nfsMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');
    const nfCatalogMenuItemAnchorEl = nfsMenuItemEls[0].querySelector('a');

    nfCatalogMenuItemAnchorEl.click();

    await fixture.whenStable();
    expect(location.path()).toEqual('/nfs', 'navigate to Network Functions page');
  });

  it('should display and hide Connectivity dropdown', fakeAsync(() => {
    spyOn(component['_router'], 'navigate');
    const connectivityButtonDe = fixture.debugElement.queryAll(By.css('.menu button'))[1];
    let connectivityDropdownEl = document.querySelector<HTMLUnknownElement>('.connectivity-dropdown');

    expect(connectivityDropdownEl).toBeNull('hide connectivity dropdown initially');

    connectivityButtonDe.triggerEventHandler('mouseenter', {});
    fixture.detectChanges();

    connectivityDropdownEl = document.querySelector('.connectivity-dropdown');

    expect(connectivityDropdownEl).toBeDefined('render connectivity dropdown');

    const connectivityMenuEl = connectivityDropdownEl.querySelector<HTMLUnknownElement>('nef-detailed-menu');
    const connectivityMenuItemEls = connectivityMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');

    expect(connectivityMenuItemEls.length).toEqual(2, 'render 2 menu items');

    connectivityButtonDe.triggerEventHandler('click', {});
    expect(component['_router'].navigate).toHaveBeenCalledWith(['/tunnels']);

    connectivityButtonDe.triggerEventHandler('mouseleave', {});
    tick(Config.menuHoverTimeout);
    fixture.detectChanges();

    connectivityDropdownEl = document.querySelector('.connectivity-dropdown');

    expect(connectivityDropdownEl).toBeNull('hide connectivity dropdown');
  }));

  it('should navigate to Tunnels', async () => {
    const connectivityButtonDe = fixture.debugElement.queryAll(By.css('.menu button'))[1];

    connectivityButtonDe.triggerEventHandler('mouseenter', {});
    fixture.detectChanges();

    const connectivityMenuEl = document.querySelector<HTMLUnknownElement>('.connectivity-dropdown nef-detailed-menu');
    const connectivityMenuItemEls = connectivityMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');
    const tunnelsMenuItemAnchorEl = connectivityMenuItemEls[0].querySelector('a');

    tunnelsMenuItemAnchorEl.click();

    await fixture.whenStable();
    expect(location.path()).toEqual('/tunnels', 'navigate to Tunnels page');
  });

  it('should display and hide Settings dropdown', () => {
    const settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));
    let settingsDropdownEl = document.querySelector<HTMLUnknownElement>('.settings-dropdown');

    expect(settingsDropdownEl).toBeNull('hide settings dropdown initially');

    settingsButtonDe.triggerEventHandler('click', {});
    fixture.detectChanges();

    settingsDropdownEl = document.querySelector('.settings-dropdown');

    expect(settingsDropdownEl).toBeTruthy('render settings dropdown');

    const settingsMenuEl = settingsDropdownEl.querySelector<HTMLUnknownElement>('nef-detailed-menu');
    const settingsMenuItemEls = settingsMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');

    expect(settingsMenuItemEls.length).toEqual(7, 'render 7 menu items for system admin');

    userService.setUser(mockSystemUser);
    fixture.detectChanges();

    expect(settingsMenuItemEls.length).toEqual(7, 'render 7 menu items for system user');

    settingsButtonDe.triggerEventHandler('click', {});
    fixture.detectChanges();

    settingsDropdownEl = document.querySelector('.settings-dropdown');

    expect(settingsDropdownEl).toBeNull('hide settings dropdown');
  });

  it('should hide Settings dropdown items for tenant admin', () => {
    userService.setUser(mockTenantAdmin);
    fixture.detectChanges();

    const settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));
    settingsButtonDe.triggerEventHandler('click', {});
    fixture.detectChanges();

    let settingsDropdownEl = document.querySelector<HTMLUnknownElement>('.settings-dropdown');
    const settingsMenuEl = settingsDropdownEl.querySelector<HTMLUnknownElement>('nef-detailed-menu');
    const settingsMenuItemEls = settingsMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');

    expect(settingsMenuItemEls.length).toEqual(1, 'render 1 menu item for tenant admin');
    expect(settingsMenuItemEls[0].querySelector('h1').textContent).toBe('Users');
  })

  it('should navigate to Users', async () => {
    const settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));

    settingsButtonDe.triggerEventHandler('click', {});
    fixture.detectChanges();

    const settingsMenuEl = document.querySelector<HTMLUnknownElement>('.settings-dropdown nef-detailed-menu');
    const settingsMenuItemEls = settingsMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');
    const usersMenuItemAnchorEl = settingsMenuItemEls[0].querySelector('a');

    usersMenuItemAnchorEl.click();

    await fixture.whenStable();
    expect(location.path()).toEqual('/users', 'navigate to Users page');
  });

  it('should display and hide user dropdown on username click', () => {
    const avatarEl: HTMLElement = toolbarEl.querySelector('nef-avatar');
    let userDropdownEl: HTMLUnknownElement = document.querySelector('.user-dropdown');

    expect(userDropdownEl).toBeNull('user dropdown is not displayed initially');

    avatarEl.click();
    fixture.detectChanges();
    userDropdownEl = document.querySelector('.user-dropdown');

    expect(userDropdownEl).toBeTruthy('user dropdown is displayed after user avatar is clicked');

    avatarEl.click();
    fixture.detectChanges();
    userDropdownEl = document.querySelector('.user-dropdown');

    expect(userDropdownEl).toBeNull('user dropdown is hidden after subsequent avatar click');
  });

  it('should render User dropdown items', () => {
    const avatarEl: HTMLElement = toolbarEl.querySelector('nef-avatar');
    let userDropdownEl: HTMLUnknownElement = document.querySelector('.user-dropdown');

    avatarEl.click();
    fixture.detectChanges();

    userDropdownEl = document.querySelector('.user-dropdown');

    expect(userDropdownEl.querySelector('mat-card-header > nef-avatar')).toBeDefined();
    expect(userDropdownEl.querySelector('mat-card-header mat-card-title').textContent).toBe('Andrew Smith');
    expect(userDropdownEl.querySelector('mat-card-header mat-card-subtitle').textContent).toBe('andrew@nefeli.io');
    expect(userDropdownEl.querySelector('mat-card-actions > button').textContent).toBe('Logout');
  });

  it('should append backdrop to the DOM and hide user dropdown on backdrop click', () => {
    const avatarEl: HTMLElement = toolbarEl.querySelector('nef-avatar');
    let backdrop: HTMLUnknownElement;

    avatarEl.click();
    fixture.detectChanges();
    backdrop = document.querySelector('.cdk-overlay-backdrop');

    expect(backdrop).toBeTruthy('backdrop exists in DOM');
  });

  it('should call logout and send request on \'Logout\' click', () => {
    const avatarEl: HTMLElement = toolbarEl.querySelector('nef-avatar');
    let userDropdownEl: HTMLUnknownElement;
    let logoutButtonEl: HTMLButtonElement;

    spyOn<any>(component, 'logout').and.callThrough();
    spyOn<any>(authenticationService, 'logout').and.callThrough();

    avatarEl.click();
    fixture.detectChanges();

    userDropdownEl = document.querySelector('.user-dropdown');
    logoutButtonEl = userDropdownEl.querySelector('button');

    logoutButtonEl.click();

    expect(component.logout).toHaveBeenCalled();
    expect(authenticationService.logout).toHaveBeenCalled();
  });

  it('should hide user dropdown on \'Logout\' click', () => {
    const avatarEl: HTMLElement = toolbarEl.querySelector('nef-avatar');
    let userDropdownEl: HTMLUnknownElement;
    let logoutButtonEl: HTMLButtonElement;

    avatarEl.click();
    fixture.detectChanges();

    userDropdownEl = document.querySelector('.user-dropdown');
    logoutButtonEl = userDropdownEl.querySelector('button');

    logoutButtonEl.click();
    fixture.detectChanges();
    userDropdownEl = document.querySelector('.user-dropdown');

    expect(userDropdownEl).toBeNull('user dropdown is hidden after \'Logout\' button click');
  });

  it('should display badge with the number of unread alarms in the bell', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 34
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 22
    } as AlarmsCount);

    fixture.detectChanges();
    const badgeButton: HTMLElement = toolbarEl.querySelector('.alarms button.mat-badge');
    const badge = badgeButton.querySelector('.mat-badge-content');

    expect(badgeButton.classList.contains('mat-badge-hidden')).toBe(false);
    expect(badge.textContent).toBe('56');
  });

  it('should display badge with less than 99 unread alarms', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 49
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 50
    } as AlarmsCount);

    fixture.detectChanges();
    const badge = toolbarEl.querySelector('.alarms button .mat-badge-content');
    const badgeButton: HTMLElement = toolbarEl.querySelector('.alarms button.mat-badge');

    expect(badgeButton.classList.contains('mat-badge-hidden')).toBe(false);
    expect(badge.textContent).toBe('99');
  });

  it('should display badge 99+ unread alarms', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 50
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 50
    } as AlarmsCount);

    fixture.detectChanges();
    const badgeButton: HTMLElement = toolbarEl.querySelector('.alarms button.mat-badge');
    const badge = badgeButton.querySelector('.mat-badge-content');

    expect(badgeButton.classList.contains('mat-badge-hidden')).toBe(false);
    expect(badge.textContent).toBe('99+');
  });

  it('should not display badge when there is no alarms', () => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 0
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 0
    } as AlarmsCount);
    fixture.detectChanges();

    const badge: HTMLElement = toolbarEl.querySelector('.alarms button.mat-badge');
    expect(badge.classList.contains('mat-badge-hidden')).toBe(true);
  });

  it('should open alarms overlay when clicked on the bell', () => {
    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');
    alarmButtonEl.click();

    fixture.detectChanges();

    const alarmDropdown: HTMLElement = document.querySelector('.cdk-overlay-container .alarm-dropdown');
    expect(alarmDropdown).not.toBeNull();
  });

  it('should display a message when no alarms in the overlay', () => {
    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 0
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 0
    } as AlarmsCount);

    alarmButtonEl.click();

    fixture.detectChanges();

    const noAlarmsEl: HTMLParagraphElement = document.querySelector('.alarm-dropdown .no-alarms');

    expect(noAlarmsEl).not.toBeNull();
    expect(noAlarmsEl.textContent.trim()).toBe('No new notifications');
  });

  it('should display unread alarms in the overlay', () => {
    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    req.flush({
      count: 1
    } as AlarmsCount);

    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);
    req2.flush({
      count: 2
    } as AlarmsCount);

    spyOn(component, 'openAlarmDropdown');
    alarmButtonEl.click();
    fixture.detectChanges();
    expect(component.openAlarmDropdown).toHaveBeenCalled();
  });

  it('should have a button \'See All Notifications\'', () => {
    spyOn(component['_sidebarService'], 'open').and.callThrough();
    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');

    alarmButtonEl.click();
    fixture.detectChanges();

    const allNotificationsButtonEl: HTMLButtonElement = document.querySelector(
      '.alarm-dropdown mat-card-actions button'
    );

    expect(allNotificationsButtonEl).not.toBeNull();
    expect(
      allNotificationsButtonEl.textContent.trim()
    ).toBe('See All Notifications');

    allNotificationsButtonEl.click();

    expect(component['_sidebarService'].open).toHaveBeenCalled();
  });

  it('should have a link to \"Settings\"', () => {
    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');
    alarmButtonEl.click();
    fixture.detectChanges();

    const settingsLink = document.querySelector('.alarm-dropdown header a');
    expect(settingsLink).not.toBeNull();
    expect(settingsLink.textContent.trim()).toBe('Settings');
  });

  it('should display Settings link only for System Admin', () => {
    userService.setUser(mockAdminUser);
    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');
    alarmButtonEl.click();
    fixture.detectChanges();

    let settingsLink = document.querySelector('.alarm-dropdown header a');
    expect(settingsLink).not.toBeNull();
    expect(settingsLink.textContent.trim()).toBe('Settings');

    userService.setUser(mockTenantAdmin);
    fixture.detectChanges();
    settingsLink = document.querySelector('.alarm-dropdown header a');
    expect(settingsLink).toBeNull();
  });

  it('should start stream for unread alarms', () => {
    spyOn(component['_alarmService'].streamAlarmsCount$, 'subscribe').and.callThrough();
    component.ngOnInit();
    expect(component['_alarmService'].streamAlarmsCount$.subscribe).toHaveBeenCalled();
    expect(component['_alarmsSubscription'].closed).toBe(false);
  });

  it('should pause stream for alerts while alarm dropdown is open', () => {
    // verify alarm stream is active
    expect(component['_alarmsSubscription'].closed).toBe(false);

    const alarmButtonEl: HTMLButtonElement = toolbarEl.querySelector('.alarms button');
    alarmButtonEl.click();
    fixture.detectChanges();

    expect(component['_alarmsSubscription'].closed).toBe(true);

    alarmButtonEl.click();
    fixture.detectChanges();

    expect(component['_alarmsSubscription'].closed).toBe(false);
  });

  it('should display Services dropdown', fakeAsync(() => {
    spyOn(component['_router'], 'navigate');
    const servicesButtonDe = fixture.debugElement.queryAll(By.css('.menu button'))[2];
    let servicesDropdownEl = document.querySelector<HTMLUnknownElement>('.services-dropdown');

    expect(servicesDropdownEl).toBeNull('services dropdown initially hidden');

    servicesButtonDe.triggerEventHandler('mouseenter', {});
    fixture.detectChanges();

    servicesDropdownEl = document.querySelector('.services-dropdown');

    expect(servicesDropdownEl).toBeDefined('render services dropdown');

    const servicesMenuEl = servicesDropdownEl.querySelector<HTMLUnknownElement>('nef-detailed-menu');
    const servicesMenuItemEls = servicesMenuEl.querySelectorAll<HTMLUnknownElement>('nef-detailed-menu-item');

    expect(servicesMenuItemEls.length).toEqual(2, 'render 2 menu items');
    expect(servicesMenuItemEls[0].querySelector('h1').textContent).toBe('Active Services & Drafts');
    expect(servicesMenuItemEls[1].querySelector('h1').textContent).toBe('Templates');

    servicesButtonDe.triggerEventHandler('click', {});
    expect(component['_router'].navigate).toHaveBeenCalledWith(['/tenants']);

    servicesButtonDe.triggerEventHandler('mouseleave', {});
    tick(Config.menuHoverTimeout);
    fixture.detectChanges();

    servicesDropdownEl = document.querySelector('.services-dropdown');
    expect(servicesDropdownEl).toBeNull('hide dropdown');
  }));

  it('should hide Overview,Connectivity and Settings buttons for saas user', () => {
    let [nfsButtonDe, connectivityButtonDe] = fixture.debugElement.queryAll(By.css('.menu button'));
    let settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));
    let systemOverviewLinkDe = fixture.debugElement.query(By.css('[routerLink="/overview"]'));

    expect(nfsButtonDe).toBeDefined('render NFs button for admin user');
    expect(connectivityButtonDe).toBeDefined('render Connectivity button for admin user');
    expect(settingsButtonDe).toBeDefined('render Settings button for admin user');
    expect(systemOverviewLinkDe).toBeDefined('render System Overview link for admin user');

    localStorageService.write(LocalStorageKey.IS_SAAS, true);
    authenticationService['_isLoggedIn$'].next(true);
    fixture.detectChanges();

    [nfsButtonDe, connectivityButtonDe] = fixture.debugElement.queryAll(By.css('.menu button'));
    settingsButtonDe = fixture.debugElement.query(By.css('.settings button'));
    systemOverviewLinkDe = fixture.debugElement.query(By.css('[routerLink="/overview"]'));

    expect(nfsButtonDe).toBeDefined('render NFs button for for saas user');
    expect(connectivityButtonDe).toBeUndefined('hide Connectivity button for saas user');
    expect(settingsButtonDe).toBeNull('hide Settings button for saas user');
    expect(systemOverviewLinkDe).toBeNull('hide System Overview link for saas user');
  });
});
