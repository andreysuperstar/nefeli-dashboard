import { HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { TunnelsService } from 'rest_client/pangolin/api/tunnels.service';

export interface TunnelErrorResponse extends HttpErrorResponse {
  tunnel: string;
}

@Injectable({
  providedIn: 'root'
})
export class TunnelService {

  constructor(
    private _restTunnelsService: TunnelsService
  ) { }

  public deleteTunnel(identifier: string): Observable<any> {
    return this._restTunnelsService.deleteTunnel(identifier)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          const error: TunnelErrorResponse = { ...err, tunnel: identifier };
          return of(error);
        })
      );
  }
}
