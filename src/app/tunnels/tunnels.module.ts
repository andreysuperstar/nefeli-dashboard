import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';

import { SharedModule } from '../shared/shared.module';
import { EncapModule } from '../encap/encap.module';

import { TunnelsRoutingModule } from './tunnels-routing.module';

import { TunnelsComponent } from './tunnels.component';
import { TunnelDialogComponent } from './tunnel-dialog/tunnel-dialog.component';

@NgModule({
  declarations: [
    TunnelsComponent,
    TunnelDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatDialogModule,
    MatCardModule,
    MatButtonToggleModule,
    MatInputModule,
    MatCheckboxModule,
    MatIconModule,
    MatSelectModule,
    MatRadioModule,
    SharedModule,
    EncapModule,
    TunnelsRoutingModule
  ]
})
export class TunnelsModule { }
