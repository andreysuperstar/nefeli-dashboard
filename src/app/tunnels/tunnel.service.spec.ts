import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { TunnelService } from './tunnel.service';

describe('TunnelService', () => {
  let service: TunnelService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(TunnelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
