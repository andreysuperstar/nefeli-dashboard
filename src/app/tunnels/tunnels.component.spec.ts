import { UserService } from 'src/app/users/user.service';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, flush, waitForAsync } from '@angular/core/testing';
import { DOCUMENT, DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TestRequest, HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatTableHarness } from '@angular/material/table/testing';
import { MatIconTestingModule } from '@angular/material/icon/testing';
import { MatIconModule } from '@angular/material/icon';
import { cloneDeep } from 'lodash-es';

import { environment } from 'src/environments/environment';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { Tunnel } from 'rest_client/pangolin/model/tunnel';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { Sites } from 'rest_client/pangolin/model/sites';
import { Site } from 'rest_client/pangolin/model/site';
import { TunnelsService } from 'rest_client/pangolin/api/tunnels.service';
import { BASE_PATH as BASE_PATH_PANGOLIN } from 'rest_client/pangolin/variables';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { TenantsResponse, Tenant, TenantService } from 'src/app/tenant/tenant.service';
import { ClusterService } from 'src/app/home/cluster.service';

import { COLUMNS, TunnelsComponent } from './tunnels.component';
import { TunnelDialogComponent } from './tunnel-dialog/tunnel-dialog.component';
import { BackButtonComponent } from 'src/app/shared/back-button/back-button.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { HttpError } from 'src/app/error-codes';
import { EncapComponent } from '../encap/encap.component';
import { User } from 'rest_client/heimdallr/model/user';
import { Tunnels } from 'rest_client/pangolin/model/tunnels';
import { RemoteTunnel } from 'rest_client/pangolin/model/remoteTunnel';
import { ActivatedRoute } from '@angular/router';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';

describe('TunnelsComponent', () => {
  let component: TunnelsComponent;
  let fixture: ComponentFixture<TunnelsComponent>;
  let httpTestingController: HttpTestingController;
  let document: Document;
  let tableDe: DebugElement;
  let userService: UserService;
  let loader: HarnessLoader;

  const mockTenants: Tenant[] = [
    {
      identifier: '1',
      name: 'Stumptown Coffee'
    },
    {
      identifier: '2',
      name: 'World Market'
    },
    {
      identifier: '3',
      name: 'Bed, Bath & Beyond'
    },
    {
      identifier: '4',
      name: 'Costco'
    }
  ];

  const mockEricSite: Site = {
    config: {
      identifier: 'eric_site',
      name: 'Eric Site'
    }
  };

  const mockAndrewSite: Site = {
    config: {
      identifier: 'andrew_site',
      name: 'Andrew Site'
    }
  };

  const mockAntonSite: Site = {
    config: {
      identifier: 'anton_site',
      name: 'Anton Site'
    }
  };

  const mockSiteResponse: Sites = {
    sites: [
      mockEricSite,
      mockAndrewSite,
      mockAntonSite
    ]
  };

  enum UserType {
    SystemAdmin,
    SystemUser
  }

  const mockUsers: User[] = [
    {
      username: 'SystemAdmin',
      email: 'sysadmin@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    },
    {
      username: 'SystemUser',
      email: 'sysuser@nefeli.io',
      roles: {
        scope: 'system',
        id: '-3',
        roles: ['user']
      }
    }
  ];

  const mockTunnels: Tunnel[] = [
    {
      config: {
        identifier: '1',
        name: 'Left Tunnel',
        siteId: mockEricSite.config.identifier,
        tenantId: mockTenants[0].identifier,
        remoteTunnel: {
          encap: {
            ivid: 1,
            ovid: 2035
          }
        },
        attachmentId: '2'
      }
    },
    {
      config: {
        identifier: '2',
        name: 'Right Tunnel',
        siteId: mockAndrewSite.config.identifier,
        tenantId: mockTenants[2].identifier,
        remoteTunnel: {
          physicalPort: '1'
        },
        attachmentId: '3'
      }
    },
    {
      config: {
        identifier: '123',
        name: 'Local Tunnel',
        siteId: mockAndrewSite.config.identifier,
        tenantId: mockTenants[2].identifier,
        localTunnel: {
          peerTunnelId: '321'
        },
        attachmentId: '3'
      }
    },
    {
      config: {
        identifier: '321',
        name: 'Peer Tunnel',
        siteId: mockAndrewSite.config.identifier,
        tenantId: mockTenants[2].identifier,
        localTunnel: {
          peerTunnelId: '123'
        },
        attachmentId: '3'
      }
    },
    {
      config: {
        identifier: '456',
        name: 'Unpaired Local Tunnel',
        siteId: mockAndrewSite.config.identifier,
        tenantId: mockTenants[2].identifier,
        localTunnel: {},
        attachmentId: '3'
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TunnelsComponent,
        BackButtonComponent,
        TableComponent,
        AvatarComponent,
        ConfirmationDialogComponent,
        TunnelDialogComponent,
        EncapComponent
      ],
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatButtonModule,
        MatSnackBarModule,
        MatTableModule,
        MatCheckboxModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatExpansionModule,
        MatRadioModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule,
        MatButtonToggleModule,
        MatCardModule,
        MatIconModule,
        MatIconTestingModule
      ],
      providers: [
        DecimalPipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH_PANGOLIN,
          useValue: '/v1'
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        AlertService,
        TunnelsService,
        TenantService,
        ClusterService,
        UserService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(TunnelsComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const tunnelsRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tunnels`
    );

    const tenantsRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/auth/tenants`
    );

    const sitesRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites`
    );

    expect(tunnelsRequest.request.method).toBe('GET');
    expect(tunnelsRequest.request.responseType).toBe('json');
    expect(tenantsRequest.request.method).toBe('GET');
    expect(tenantsRequest.request.responseType).toBe('json');
    expect(sitesRequest.request.method).toBe('GET');

    const tunnelMockResponse: Tunnels = {
      tunnels: cloneDeep(mockTunnels)
    };

    tunnelsRequest.flush(tunnelMockResponse);

    const mockTenantsResp: TenantsResponse = {
      tenants: mockTenants,
      pagination: {
        index: 0,
        size: mockTenants.length
      }
    };

    tenantsRequest.flush(mockTenantsResp);

    sitesRequest.flush(mockSiteResponse);

    fixture.detectChanges();
    tableDe = fixture.debugElement.query(By.css('nef-table'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page heading', () => {
    expect(
      fixture.debugElement.query(By.css('header h2')).nativeElement.textContent
    ).toBe('Tunnels', 'valid \'Tunnels\' heading');
  });

  it('should render \'Add New Tunnel\' button', () => {
    expect(
      fixture.debugElement.query(By.css('header button.add span')).nativeElement.textContent
    ).toBe('Add New Tunnel', 'valid \'Add New Tunnel\' button text');
  });

  it('should not render add button for system user', () => {
    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    expect(
      fixture.debugElement.query(By.css('header button.add span'))
    ).toBeNull();
  });

  it('should render tunnels table', () => {
    expect(tableDe).not.toBeNull('table is rendered');

    const headerCellDes: DebugElement[] = tableDe.queryAll(By.css('mat-header-cell'));

    expect(headerCellDes.length).toBe(6, 'table header cells are rendered');

    expect(headerCellDes[1].nativeElement.textContent).toContain(
      COLUMNS[1].label,
      `valid ${COLUMNS[1].name} column label`
    );
    expect(headerCellDes[2].nativeElement.textContent).toContain(
      COLUMNS[2].name,
      `valid ${COLUMNS[2].name} column label`
    );
    expect(headerCellDes[3].nativeElement.textContent).toContain(
      COLUMNS[3].name,
      `valid ${COLUMNS[3].name} column label`
    );
    expect(headerCellDes[4].nativeElement.textContent).toContain(
      COLUMNS[4].name,
      `valid ${COLUMNS[4].name} column label`
    );
    expect(headerCellDes[5].nativeElement.textContent).toContain(
      COLUMNS[5].name,
      `valid ${COLUMNS[5].name} column label`
    );
  });

  it('should set tunnels and data source', () => {
    const tunnels = component['_tunnels'];
    const dataSource = component['_dataSource'];

    expect(tunnels.length).toEqual(mockTunnels.length);

    expect(tunnels[0].name).toBe(mockTunnels[0].config.name, `valid ${mockTunnels[0].config.name} tunnel name`);
    expect(tunnels[0].tenantId).toBe(mockTunnels[0].config.tenantId, `valid ${mockTunnels[0].config.name} tunnel tenant`);
    expect(tunnels[1].siteId).toBe(mockTunnels[1].config.siteId, `valid ${mockTunnels[1].config.name} tunnel site`);

    expect(dataSource.length).toEqual(mockTunnels.length);

    expect(dataSource[1].tunnel).toBe(mockTunnels[1].config.name, `valid data source ${mockTunnels[1].config.name} tunnel name`);
    expect(dataSource[1].tenant).toBe(mockTenants[2].name, `valid data source ${mockTunnels[1].config.name} tunnel tenant`);
    expect(dataSource[1].tenantID).toBe(mockTunnels[1].config.tenantId, `valid data source ${mockTunnels[1].config.name} tunnel tenant ID`);
    expect(dataSource[0].site).toBe(mockEricSite.config.name, `valid data source ${mockTunnels[1].config.name} tunnel site`);
  });

  it('should render tunnels table rows', () => {
    const rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    expect(rowDes.length).toBe(mockTunnels.length, `render ${mockTunnels.length} rows`);

    const firstRowCellDes: DebugElement[] = rowDes[0].queryAll(By.css('mat-cell'));

    expect(firstRowCellDes[1].nativeElement.textContent).toBe(
      mockTunnels[0].config.name,
      `render ${mockTunnels[1].config.name} tunnel name`
    );
    expect(firstRowCellDes[2].nativeElement.textContent).toBe(
      mockTenants[0].name,
      `render ${mockTunnels[0].config.name} tunnel tenant`
    );
    expect(firstRowCellDes[3].nativeElement.textContent).toBe(
      mockEricSite.config.name,
      `render ${mockTunnels[0].config.name} tunnel site`
    );
    expect(firstRowCellDes[4].nativeElement.textContent.trim()).toBe('remote');

    const secondRowCellDes: DebugElement[] = rowDes[1].queryAll(By.css('mat-cell'));

    expect(secondRowCellDes[1].nativeElement.textContent).toBe(
      mockTunnels[1].config.name,
      `render ${mockTunnels[1].config.name} tunnel name`
    );
    expect(secondRowCellDes[2].nativeElement.textContent).toBe(
      mockTenants[2].name,
      `render ${mockTunnels[1].config.name} tunnel tenant`
    );
    expect(secondRowCellDes[3].nativeElement.textContent).toBe(
      mockAndrewSite.config.name,
      `render ${mockTunnels[1].config.name} tunnel site`
    );
    expect(secondRowCellDes[4].nativeElement.textContent.trim()).toBe('remote');
  });

  it('should add a new tunnel', fakeAsync(() => {
    // save initial number of rows
    const rowsLength: number = tableDe.queryAll(By.css('mat-row')).length;

    const addNewTunnelButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('header button.add')
    ).nativeElement;

    spyOn<any>(component, 'onAddTunnel').and.callThrough();

    addNewTunnelButtonEl.click();
    expect(component.onAddTunnel).toHaveBeenCalled();
    tick();

    const tunnelDialogEl: HTMLUnknownElement = document.querySelector('nef-tunnel-dialog');

    expect(tunnelDialogEl).not.toBeNull('render Tunnel dialog');

    const nameInputEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'input[formcontrolname="name"]'
    );

    const mockName = 'New Tunnel';

    nameInputEl.value = mockName;
    nameInputEl.dispatchEvent(new Event('input'));

    const tenantSelectEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'mat-select[formcontrolname="tenant"]'
    );

    tenantSelectEl
      .querySelector('.mat-select-trigger')
      .dispatchEvent(new Event('click'));
    fixture.detectChanges();

    let optionEls: NodeListOf<HTMLUnknownElement> = document.querySelectorAll('mat-option');

    // select tenant
    optionEls[2].dispatchEvent(new Event('click'));
    fixture.detectChanges();

    const siteSelectEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'mat-select[formcontrolname="site"]'
    );

    siteSelectEl
      .querySelector('.mat-select-trigger')
      .dispatchEvent(new Event('click'));
    fixture.detectChanges();

    optionEls = document
      .querySelectorAll('.mat-select-panel')[1]
      .querySelectorAll('mat-option');

    // select site
    optionEls[1].dispatchEvent(new Event('click'));
    fixture.detectChanges();

    const physicalPortInputEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'input[formcontrolname="physicalPort"]'
    );

    const mockPhysicalPort = '10'

    physicalPortInputEl.value = mockPhysicalPort;
    physicalPortInputEl.dispatchEvent(new Event('input'));
    physicalPortInputEl.dispatchEvent(new KeyboardEvent('keyup'));

    const submitButtonEl: HTMLButtonElement = document.querySelector('button[form="tunnel-form"]');

    submitButtonEl.click();
    tick();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tunnels`
    );

    const mockTunnel: TunnelConfig = {
      name: mockName,
      tenantId: mockTenants[2].identifier,
      siteId: mockAndrewSite.config.identifier,
      remoteTunnel: <RemoteTunnel> {
        physicalPort: mockPhysicalPort
      }
    };

    request.flush(mockTunnel);
    fixture.detectChanges();
    tick();

    fixture.detectChanges();

    const rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    expect(rowDes.length).toBe(rowsLength + 1, 'render a new tunnel row');

    const newTunnelRowCells: DebugElement[] = rowDes[rowsLength].queryAll(
      By.css('mat-cell')
    );

    expect(
      newTunnelRowCells[1].nativeElement.textContent
    ).toBe(mockName, 'render a new tunnel name');
    expect(
      newTunnelRowCells[2].nativeElement.textContent
    ).toBe(mockTenants[2].name, 'render a new tunnel tenant');
    expect(
      newTunnelRowCells[3].nativeElement.textContent
    ).toBe(mockAndrewSite.config.name, 'render a new tunnel site');

    flush();
  }));

  it('should edit a remote tunnel', fakeAsync(() => {
    const rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    const editTunnelButtonEl: HTMLButtonElement = rowDes[1].query(
      By.css('button.edit')
    ).nativeElement;

    spyOn<any>(component, 'editTunnel').and.callThrough();

    editTunnelButtonEl.click();
    expect(component['editTunnel']).toHaveBeenCalled();
    tick();

    const tunnelDialogEl: HTMLUnknownElement = document.querySelector('nef-tunnel-dialog');

    expect(tunnelDialogEl).not.toBeNull('render Tunnel dialog');

    let physicalPortInputEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'input[formcontrolname="physicalPort"]'
    );

    const mockPhysicalPort = '37'

    physicalPortInputEl.value = mockPhysicalPort;
    physicalPortInputEl.dispatchEvent(new Event('input'));
    physicalPortInputEl.dispatchEvent(new KeyboardEvent('keyup'));

    const submitButtonEl: HTMLButtonElement = document.querySelector('button[form="tunnel-form"]');

    submitButtonEl.click();
    tick();

    fixture.detectChanges();
    tick();

    editTunnelButtonEl.click();
    tick();

    physicalPortInputEl = tunnelDialogEl.querySelector(
      'input[formcontrolname="physicalPort"]'
    );

    expect(physicalPortInputEl.value).toBe(mockPhysicalPort, 'render updated physical port');

    flush();
  }));

  it('should have current data selected when editing a local tunnel', fakeAsync(() => {
    const rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    const editTunnelButtonEl: HTMLButtonElement = rowDes[4].query(
      By.css('button.edit')
    ).nativeElement;

    spyOn<any>(component, 'editTunnel').and.callThrough();

    editTunnelButtonEl.click();
    expect(component['editTunnel']).toHaveBeenCalled();
    tick();

    const tunnelDialogEl: HTMLUnknownElement = document.querySelector('nef-tunnel-dialog');

    expect(tunnelDialogEl).not.toBeNull('render Tunnel dialog');

    const nameInputEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'input[formcontrolname="name"]'
    );

    const tenantSelectEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'mat-select[formcontrolname="tenant"]'
    );

    const siteSelectEl: HTMLInputElement = tunnelDialogEl.querySelector(
      'mat-select[formcontrolname="site"]'
    );

    expect(nameInputEl.value).toBe('Unpaired Local Tunnel');
    expect(tenantSelectEl.innerText.trim()).toBe('Bed, Bath & Beyond');
    expect(siteSelectEl.innerText.trim()).toBe('Andrew Site');

    flush();
  }));

  it('should remove a tunnel', fakeAsync(() => {
    let rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    // save tunnel row name to confirm it's removed later
    const tunnelName: string = rowDes[0].query(
      By.css('.mat-column-tunnel')
    ).nativeElement.textContent;

    const removeButtonEl: HTMLButtonElement = rowDes[0].query(
      By.css('button.remove')
    ).nativeElement;

    expect(rowDes.length).toBe(
      mockTunnels.length,
      `render ${mockTunnels.length} tunnel rows`
    );

    expect(removeButtonEl).not.toBeNull('render Remove button');
    expect(removeButtonEl.textContent).toBe('Remove', 'render Remove button text');

    spyOn<any>(component, 'removeTunnel').and.callThrough();

    removeButtonEl.click();
    expect(component['removeTunnel']).toHaveBeenCalled();
    tick();

    const dialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
    const descriptionEls: NodeListOf<HTMLParagraphElement> = dialogEl.querySelectorAll('p');
    const removeTunnelButtonEl: HTMLButtonElement = dialogEl.querySelectorAll('button')[1];

    expect(descriptionEls[0].textContent).toBe(
      `This will remove the tunnel ${mockTunnels[0].config.name}.`,
      'render dialog description'
    );

    expect(removeTunnelButtonEl.textContent).toBe(
      'Remove Tunnel',
      'render Remove Tunnel button text'
    );

    removeTunnelButtonEl.click();
    tick();

    const request: TestRequest = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/tunnels/${String(mockTunnels[0].config.identifier)}`
    });

    request.flush({});
    fixture.detectChanges();
    tick();

    fixture.detectChanges();
    rowDes = tableDe.queryAll(By.css('mat-row'));

    expect(rowDes.length).toEqual(
      mockTunnels.length - 1,
      'render tunnel rows without removed tunnel'
    );

    const shiftedTunnelRowName: string = rowDes[0].query(
      By.css('.mat-column-tunnel')
    ).nativeElement.textContent;

    expect(shiftedTunnelRowName).not.toBe(tunnelName, 'render shifted tunnel row name');

    flush();
  }));

  it('should render \'Remove\' button and delete multiple selected tunnels', async () => {
    const checkboxes = await loader.getAllHarnesses<MatCheckboxHarness>(MatCheckboxHarness);
    await checkboxes[1].check();
    await checkboxes[2].check();
    fixture.detectChanges();

    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Remove'
    }));
    await removeButton.click();
    fixture.detectChanges();

    const dialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
    const confirmRemoveButton: HTMLButtonElement = dialogEl.querySelectorAll('button')[1];
    confirmRemoveButton.click();

    fixture.whenStable().then(() => {
      const req1: TestRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/tunnels/${String(mockTunnels[0].config.identifier)}`
      });
      const req2: TestRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/tunnels/${String(mockTunnels[1].config.identifier)}`
      });

      req1.flush({});
      req2.flush({});
      fixture.detectChanges();
      const rows = tableDe.queryAll(By.css('mat-row'));
      expect(rows.length).toEqual(mockTunnels.length - 2);
    });
  });

  it('should display correct alert after partial deletion', async () => {
    spyOn(component['_alertService'], 'error').and.callThrough();
    const checkboxes = await loader.getAllHarnesses<MatCheckboxHarness>(MatCheckboxHarness);
    await checkboxes[1].check();
    await checkboxes[2].check();
    fixture.detectChanges();
    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({ text: 'Remove' }));
    await removeButton.click();
    fixture.detectChanges();
    const dialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
    const confirmRemoveButton: HTMLButtonElement = dialogEl.querySelectorAll('button')[1];
    confirmRemoveButton.click();

    fixture.whenStable().then(() => {
      const req1: TestRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/tunnels/${String(mockTunnels[0].config.identifier)}`
      });
      const req2: TestRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/tunnels/${String(mockTunnels[1].config.identifier)}`
      });

      req1.flush({}, { status: 404, statusText: 'Not Found' });
      req2.flush({});
      fixture.detectChanges();
      const rows = tableDe.queryAll(By.css('mat-row'));
      expect(rows.length).toEqual(mockTunnels.length - 1);
      expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_TUNNELS_PARTIAL_RETRY);
    });
  });

  it('should show conflict error on removing a tunnel', fakeAsync(() => {
    let rowDes: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    const removeButtonEl: HTMLButtonElement = rowDes[0].query(
      By.css('button.remove')
    ).nativeElement;
    removeButtonEl.click();
    tick();

    const dialogEl: HTMLUnknownElement = document.querySelector('nef-confirmation-dialog');
    const removeTunnelButtonEl: HTMLButtonElement = dialogEl.querySelectorAll('button')[1];
    removeTunnelButtonEl.click();
    tick();

    const request: TestRequest = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/tunnels/${String(mockTunnels[0].config.identifier)}`
    });

    request.flush({}, {
      status: HttpError.Conflict,
      statusText: ''
    });
    fixture.detectChanges();
    tick();

    expect(document.querySelector('simple-snack-bar span').textContent)
      .toBe('Could not remove tunnel, currently in use.');

    flush();
  }));

  it('should properly list an unpaired local tunnel', () => {
    const rows: DebugElement[] = tableDe.queryAll(By.css('mat-row'));
    const fifthRowCells: DebugElement[] = rows[4].queryAll(By.css('mat-cell'));
    expect(fifthRowCells[1].nativeElement.textContent).toBe("Unpaired Local Tunnel");
    expect(fifthRowCells[4].nativeElement.textContent.trim()).toBe("local");
    expect(fifthRowCells[4].nativeElement.querySelector('mat-icon').getAttribute('data-mat-icon-name')).toBe("icon-link-off");

    // verify type is not a link
    expect(fifthRowCells[4].nativeElement.querySelector('a')).toBeNull();
  });

  it('should properly list a paired local tunnel', () => {
    const rows: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    // check the paired local tunnel
    const thirdRowCells: DebugElement[] = rows[2].queryAll(By.css('mat-cell'));
    expect(thirdRowCells[1].nativeElement.textContent).toBe("Local Tunnel");
    expect(thirdRowCells[4].nativeElement.textContent.trim()).toBe("local");
    expect(thirdRowCells[4].nativeElement.querySelector('a')).toBeDefined();
    expect(thirdRowCells[4].nativeElement.querySelector('mat-icon').getAttribute('data-mat-icon-name')).toBe("icon-link-on");

    // check the local tunnel's peer
    const fourthRowCells: DebugElement[] = rows[3].queryAll(By.css('mat-cell'));
    expect(fourthRowCells[1].nativeElement.textContent).toBe("Peer Tunnel");
    expect(fourthRowCells[4].nativeElement.textContent.trim()).toBe("local");
    expect(fourthRowCells[4].nativeElement.querySelector('a')).toBeDefined();
    expect(fourthRowCells[4].nativeElement.querySelector('mat-icon').getAttribute('data-mat-icon-name')).toBe("icon-link-on");
  });

  it('should properly jump to local peer tunnel', () => {
    const rows: DebugElement[] = tableDe.queryAll(By.css('mat-row'));

    const thirdRowCells: DebugElement[] = rows[2].queryAll(By.css('mat-cell'));
    const localLink: HTMLAnchorElement = thirdRowCells[4].nativeElement.querySelector('a');
    expect(localLink.getAttribute('mattooltipposition')).toBe("above");
    expect(localLink.getAttribute('href')).toBe("/tunnels;scrollTo=321");

    const fourthRowCells: DebugElement[] = rows[3].queryAll(By.css('mat-cell'));
    const peerLink: HTMLAnchorElement = fourthRowCells[4].nativeElement.querySelector('a');
    expect(peerLink.getAttribute('mattooltipposition')).toBe("above");
    expect(peerLink.getAttribute('href')).toBe("/tunnels;scrollTo=123");

    // verify router handler is properly setup
    spyOn(component['_table'], 'scrollToRow');
    TestBed.get(ActivatedRoute).params.next({ scrollTo: '321' });
    expect(component['_table'].scrollToRow).toHaveBeenCalledWith(3);
  });

  it('should properly update peer tunnel when deleting local paired tunnel', fakeAsync(() => {
    // verify local & peer tunnel reference each other
    expect(component['_tunnels'][2].localTunnel.peerTunnelId).toBe(mockTunnels[3].config.identifier);
    expect(component['_tunnels'][3].localTunnel.peerTunnelId).toBe(mockTunnels[2].config.identifier);

    // verify peer tunnel is at index 3
    expect(component['_tunnels'][3].identifier).toBe("321");
    expect(component['_tunnels'][3].name).toBe("Peer Tunnel");

    // delete local tunnel at index 2
    component['removeTunnel']({
      identifier: mockTunnels[2].config.identifier,
      tunnel: mockTunnels[2].config.name,
      tenant: mockTunnels[2].config.tenantId,
      tenantID: mockTunnels[2].config.tenantId,
      site: mockAndrewSite.config.name,
      siteID: mockTunnels[2].config.siteId,
      type: undefined,
      actions: undefined
    });

    // confirm delete confirmation dialog
    component['_dialog'].openDialogs[0].close(true);
    tick();
    fixture.detectChanges();
    tick();

    httpTestingController.expectOne(`${environment.restServer}/v1/tunnels/123`).flush({});
    fixture.detectChanges();

    // verify peer tunnel is now at index 2
    expect(component['_tunnels'][2].identifier).toBe("321");
    expect(component['_tunnels'][2].name).toBe("Peer Tunnel");

    // verify peer tunnel does not reference another local tunnel
    expect(component['_tunnels'][2].localTunnel.peerTunnelId).toBeFalsy();

    flush();
  }));

  it('should properly display newly created local & peer tunnel', waitForAsync(async () => {
    const table = await loader.getHarness<MatTableHarness>(MatTableHarness);
    const rows = await table.getRows();
    expect(rows.length).toBe(mockTunnels.length);

    const addButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Add New Tunnel'
    }));
    await addButton.click();
    fixture.detectChanges();

    const newTunnels: TunnelConfig[] = [{
      identifier: '123',
      name: 'Test Local Tunnel',
      siteId: mockEricSite.config.identifier,
      tenantId: mockTenants[0].identifier,
      localTunnel: {
        peerTunnelId: '321'
      }
    }, {
      identifier: '321',
      name: 'Test Peer Tunnel',
      siteId: mockEricSite.config.identifier,
      tenantId: mockTenants[0].identifier,
      localTunnel: {
        peerTunnelId: '123'
      }
    }];

    component['_dialog'].openDialogs[0].close(newTunnels);
    fixture.detectChanges();

    fixture.whenStable().then(async () => {
      fixture.detectChanges();
      const updatedRows = await table.getRows();
      expect(updatedRows.length).toBe(mockTunnels.length + newTunnels.length);

      const cells1 = await updatedRows[5].getCellTextByColumnName();
      const cells2 = await updatedRows[6].getCellTextByColumnName();
      expect(cells1.tunnel).toBe('Test Local Tunnel');
      expect(cells1.type).toBe('local');
      expect(cells2.tunnel).toBe('Test Peer Tunnel');
      expect(cells2.type).toBe('local');
    });
  }));
});
