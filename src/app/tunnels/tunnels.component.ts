import { UserService } from 'src/app/users/user.service';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialogConfig, MatDialog, MatDialogRef } from '@angular/material/dialog';

import { Subscription, Observable, forkJoin } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';

import { cloneDeep } from 'lodash-es';

import { Column, ColumnType, TableActionButton, TableActionEvent, TableActions, Link, TableComponent } from 'src/app/shared/table/table.component';

import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { TunnelsService as RESTTunnelsService } from 'rest_client/pangolin/api/tunnels.service';
import { Tunnel } from 'rest_client/pangolin/model/tunnel';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { AlertType, AlertService } from 'src/app/shared/alert.service';
import { Tenant, TenantService } from 'src/app/tenant/tenant.service';
import { ClusterService } from 'src/app/home/cluster.service';

import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { TunnelDialogData, TunnelDialogComponent } from './tunnel-dialog/tunnel-dialog.component';
import { HttpError } from 'src/app/error-codes';
import { TunnelService, TunnelErrorResponse } from './tunnel.service';
import { ActivatedRoute } from '@angular/router';
import { Tunnels } from 'rest_client/pangolin/model/tunnels';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { Location } from '@angular/common';

export interface TunnelRow {
  identifier: string;
  tunnel: string;
  tenant: string;
  tenantID: string;
  site: string;
  siteID: string;
  type: Link;
  actions: Array<TableActionButton>;
}

export const COLUMNS: Column[] = [
  {
    name: 'select',
    type: ColumnType.SELECT
  },
  {
    name: 'tunnel',
    label: 'name',
  },
  {
    name: 'tenant'
  },
  {
    name: 'site'
  },
  {
    name: 'type',
    type: ColumnType.LINK
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS
  }
];

const DIALOG_CONFIG: MatDialogConfig<TunnelDialogData | ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-tunnels',
  templateUrl: './tunnels.component.html',
  styleUrls: ['./tunnels.component.less']
})
export class TunnelsComponent implements OnInit, OnDestroy {

  private _selectedTunnels: TunnelRow[];
  private _tunnels: TunnelConfig[];
  private _tenants: Tenant[];
  private _sites: Site[];
  private _dataSource: TunnelRow[] = [];
  private _dialogSubscription: Subscription;

  constructor(
    private _dialog: MatDialog,
    private _alertService: AlertService,
    private _restTunnelsService: RESTTunnelsService,
    private _tenantService: TenantService,
    private _siteService: ClusterService,
    private _userService: UserService,
    private _tunnelService: TunnelService,
    private _route: ActivatedRoute,
    private _location: Location
  ) { }

  @ViewChild('table', { static: false }) private _table: TableComponent;

  public ngOnInit() {
    this.getTunnelsAndTenantsAndSites();
    this.routeHandler();
  }

  public ngOnDestroy() {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }
  }

  private routeHandler() {
    this._route.params.subscribe((routeParams) => {
      if (routeParams?.scrollTo && this._table) {
        const tunnelId = routeParams.scrollTo;
        const renderedData = this._table.dataSource.connect().value;
        const index = renderedData.findIndex((row: TunnelRow) => row.identifier === tunnelId);
        this._table.scrollToRow(index);

        // clear the 'scrollTo' param to avoid buggy behavior
        this._location.replaceState('/tunnels');
      }
    });
  }

  private getTunnelsAndTenantsAndSites() {
    forkJoin([
      this._restTunnelsService.getTunnels(),
      this._tenantService.getTenants(),
      this._siteService.getSites()
    ])
      .subscribe(([tunnelList, { tenants }, { sites: sites }]: [Tunnels, Tenants, Sites]) => {
        this._tunnels = tunnelList?.tunnels?.map((tunnel: Tunnel) => tunnel.config);
        this._tenants = tenants;
        this._sites = sites;

        this.setDataSource();
      });
  }

  private setDataSource() {
    const actions: Array<TableActionButton> = [
      {
        action: TableActions.Edit,
        text: 'Edit',
        icon: 'edit'
      },
      {
        action: TableActions.Remove,
        text: 'Remove',
        icon: 'trash-blue'
      }
    ];

    this._dataSource = this._tunnels?.map(
      ({ identifier, name, siteId, tenantId: tenantID, remoteTunnel, localTunnel }: TunnelConfig): TunnelRow => {
        const tunnelTenant: Tenant = this._tenants.find(
          (tenant: Tenant): boolean => tenant.identifier === tenantID
        );

        const typeDescription = localTunnel ? 'local' : 'remote';
        const peerRoute = localTunnel?.peerTunnelId ? ['/tunnels', { scrollTo: localTunnel.peerTunnelId }] : undefined;
        let typeIcon;
        let peerTooltip;

        if (localTunnel) {
          if (localTunnel.peerTunnelId) {
            const peerTunnel = this._tunnels.find((tunnel: TunnelConfig) => tunnel.identifier === localTunnel.peerTunnelId);
            typeIcon = 'icon-link-on';
            peerTooltip = `(peer) ${peerTunnel?.name}`;
          } else {
            typeIcon = 'icon-link-off';
          }
        }

        const site = this._sites?.find(s => s.config.identifier === siteId);

        return {
          identifier,
          tunnel: name,
          tenant: tunnelTenant.name,
          tenantID,
          site: site?.config?.name,
          siteID: siteId,
          type: {
            label: typeDescription,
            route: peerRoute,
            tooltip: peerTooltip,
            icon: typeIcon
          },
          actions
        } as TunnelRow;
      }
    );
  }

  public onAddTunnel() {
    const data: TunnelDialogData = {
      tenants: this._tenants,
      sites: this._sites
    };

    const dialogRef: MatDialogRef<TunnelDialogComponent, TunnelConfig> = this._dialog.open(
      TunnelDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((addedTunnels: TunnelConfig[]) => {
        addedTunnels.forEach((addedTunnel: TunnelConfig) => {
          if (addedTunnel.localTunnel?.peerTunnelId) {
            const peerTunnel = this._tunnels.find((tunnel: TunnelConfig) =>
              tunnel.identifier === addedTunnel.localTunnel.peerTunnelId);

            if (peerTunnel?.localTunnel) {
              peerTunnel.localTunnel.peerTunnelId = addedTunnel.identifier;
            }
          }
        });

        this._tunnels = this._tunnels.concat(cloneDeep(addedTunnels));

        this.setDataSource();
      });
  }

  private editTunnel({ identifier, tunnel: name }: TunnelRow) {
    const editableTunnel: TunnelConfig = this._tunnels.find(
      (tunnel: TunnelConfig): boolean => tunnel.name === name
    );

    const peerTunnel: TunnelConfig = (editableTunnel?.localTunnel?.peerTunnelId) ? this._tunnels.find(
      (tunnel: TunnelConfig): boolean => tunnel?.localTunnel?.peerTunnelId === identifier
    ) : undefined;

    const data: TunnelDialogData = {
      tunnel: cloneDeep(editableTunnel),
      tenants: this._tenants,
      sites: this._sites,
      peerTunnel
    };

    const dialogRef: MatDialogRef<TunnelDialogComponent, TunnelConfig> = this._dialog.open(
      TunnelDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((editedTunnels: TunnelConfig[]) => {
        if (editedTunnels) {
          this.getTunnelsAndTenantsAndSites();
        }
      });
  }

  private removeTunnel({ identifier, tunnel: name }: TunnelRow) {
    const data: ConfirmationDialogData = {
      description: `This will remove the tunnel <strong>${name}</strong>.`,
      action: 'Remove Tunnel'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        mergeMap((_: boolean) => this._restTunnelsService.deleteTunnel(identifier))
      )
      .subscribe(
        (_: any) => {
          const index: number = this._tunnels.findIndex(
            (tunnel: TunnelConfig): boolean => tunnel.identifier === identifier
          );
          const deletedTunnel: TunnelConfig = this._tunnels[index];

          if (deletedTunnel.localTunnel?.peerTunnelId) {
            const peerTunnel: TunnelConfig = this._tunnels.find(
              (tunnel: TunnelConfig): boolean => tunnel.identifier === deletedTunnel.localTunnel.peerTunnelId
            );
            peerTunnel.localTunnel.peerTunnelId = undefined;
          }

          this._tunnels.splice(index, 1);

          this.setDataSource();

          this._alertService.info(AlertType.INFO_REMOVE_TUNNEL_SUCCESS);
        },
        ({ status, error }: HttpErrorResponse) => {
          if (status === HttpError.Conflict) {
            this._alertService.error(error.message ?
              AlertType.ERROR_REMOVE_TUNNEL :
              AlertType.ERROR_REMOVE_ACTIVE_TUNNEL,
              error.message);
          } else {
            this._alertService.error(AlertType.ERROR_REMOVE_TUNNEL_RETRY);
          }
        }
      );
  }

  public onRemoveTunnels() {
    const data: ConfirmationDialogData = {
      description: `This will remove <strong>all</strong> selected tunnels.`,
      action: 'Remove Tunnels'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }
    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        mergeMap((_: boolean) => forkJoin(
          this._selectedTunnels.map(
            ({ identifier, tunnel: name }: TunnelRow): Observable<any> =>
              this._tunnelService.deleteTunnel(identifier)
          )
        ))
      )
      .subscribe(
        (responses: (any)[]) => {
          const failedTunnels: string[] = responses ?
            responses.filter((res): boolean => res && 'error' in res)
              .map((res: TunnelErrorResponse) => res.tunnel)
            : [];
          const numDeleted = this._selectedTunnels.length - failedTunnels.length;

          this._selectedTunnels.forEach((tunnel: TunnelRow) => {
            const index: number = this._tunnels.findIndex(
              (t: TunnelConfig): boolean => t.identifier === tunnel.identifier
            );
            if (!failedTunnels.includes(tunnel.identifier)) {
              this._tunnels.splice(index, 1);
            }
          });

          this.setDataSource();

          this._selectedTunnels = undefined;

          if (failedTunnels.length) {
            this._alertService.error(numDeleted
              ? AlertType.ERROR_REMOVE_TUNNELS_PARTIAL_RETRY
              : AlertType.ERROR_REMOVE_TUNNELS_RETRY);
          } else {
            this._alertService.info(numDeleted > 1
              ? AlertType.INFO_REMOVE_TUNNELS_SUCCESS
              : AlertType.INFO_REMOVE_TUNNEL_SUCCESS);
          }
        },
        (_: HttpErrorResponse) =>
          this._alertService.error(AlertType.ERROR_REMOVE_TUNNELS_RETRY)
      );
  }

  public onActionClick(event: TableActionEvent<TunnelRow>) {
    switch (event.type) {
      case TableActions.Edit:
        this.editTunnel(event.data);
        break;
      case TableActions.Remove:
        this.removeTunnel(event.data);
        break;
    }
  }

  public onSelectTunnel(rows: TunnelRow[]) {
    this._selectedTunnels = rows;
  }

  public get columns(): Column[] {
    return COLUMNS;
  }

  public get dataSource(): TunnelRow[] {
    return this._dataSource;
  }

  public get tenants(): Tenant[] {
    return this._tenants;
  }

  public get sites(): Site[] {
    return this._sites;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get selectedTunnels(): TunnelRow[] {
    return this._selectedTunnels;
  }
}
