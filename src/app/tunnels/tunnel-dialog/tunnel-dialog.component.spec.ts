import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpErrorResponse } from '@angular/common/http';
import { HttpTestingController, HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormGroup } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatRadioModule } from '@angular/material/radio';

import { environment } from 'src/environments/environment';

import { Tenant } from 'src/app/tenant/tenant.service';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { Attachment } from 'rest_client/pangolin/model/attachment';
import { Attachments } from 'rest_client/pangolin/model/attachments';
import { Encap } from 'rest_client/pangolin/model/encap';
import { VxlanVtep } from 'rest_client/pangolin/model/vxlanVtep';
import { Vxlan } from 'rest_client/pangolin/model/vxlan';
import { Site } from 'rest_client/pangolin/model/site';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { TunnelsService } from 'rest_client/pangolin/api/tunnels.service';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { AlertService, AlertType } from 'src/app/shared/alert.service';

import { TunnelType, PeerOption, TunnelDialogComponent, TunnelDialogData } from './tunnel-dialog.component';
import { VLANType, EncapComponent } from '../../encap/encap.component';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { DebugElement, setTestabilityGetter } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatRadioButtonHarness } from '@angular/material/radio/testing';
import { MatSelectHarness } from '@angular/material/select/testing';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { HttpErrorCode } from '../../error-codes';
import { MatButtonHarness } from '@angular/material/button/testing';

describe('TunnelDialogComponent', () => {
  let component: TunnelDialogComponent;
  let fixture: ComponentFixture<TunnelDialogComponent>;
  let httpTestingController: HttpTestingController;
  let tunnelService: TunnelsService;
  let loader: HarnessLoader;

  const dialogSpy = jasmine.createSpyObj<MatDialogRef<TunnelDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  const mockTenants: Tenant[] = [
    {
      identifier: '1',
      name: 'Tenant 1'
    },
    {
      identifier: '2',
      name: 'Tenant 2'
    },
    {
      identifier: '3',
      name: 'Tenant 3'
    },
    {
      identifier: '4',
      name: 'Nefeli Networks'
    }
  ];

  const mockSites: Site[] = [
    {
      config: {
        identifier: 'eric_site',
        name: 'Eric\'s Site'
      }
    },
    {
      config: {
        identifier: 'andrew_site',
        name: 'Andrew\'s Site'
      }
    },
    {
      config: {
        identifier: 'anton_site',
        name: 'Anton\'s Site'
      }
    },
    {
      config: {
        identifier: 'inactive_site',
        name: 'Inactive Site'
      }
    }
  ];

  const mockRawTunnelFormData = {
    name: 'Right Tunnel',
    description: 'Right tunnel',
    site: mockSites[1].config.identifier,
    tenant: mockTenants[0].identifier,
    physicalPort: '1',
    attachment: '2'
  };

  const mockVLANTunnelFormData = {
    name: 'Right Tunnel',
    description: 'Right tunnel',
    site: mockSites[1].config.identifier,
    tenant: mockTenants[0].identifier,
    attachment: '3',
    encap: {
      vlan: {
        type: VLANType['802.1Q'],
        vid: {
          inner: 1
        }
      }
    }
  };

  const mockVXLANTunnelFormData = {
    name: 'Right Tunnel',
    description: 'Right tunnel',
    site: mockSites[1].config.identifier,
    tenant: mockTenants[0].identifier,
    encap: {
      vxlan: {
        vni: 1,
        mac: '00-E0-4C-00-07-BE',
        ip: '112.92.1.214',
        udpPort: 1024
      }
    }
  };

  const mockRawTunnel: TunnelConfig = {
    identifier: '123',
    name: mockRawTunnelFormData.name,
    description: mockRawTunnelFormData.description,
    siteId: mockRawTunnelFormData.site,
    tenantId: mockRawTunnelFormData.tenant,
    remoteTunnel: {
      physicalPort: mockRawTunnelFormData.physicalPort
    },
    attachmentId: mockRawTunnelFormData.attachment
  };

  const mockVLANTunnel: TunnelConfig = {
    name: mockVLANTunnelFormData.name,
    description: mockVLANTunnelFormData.description,
    siteId: mockVLANTunnelFormData.site,
    tenantId: mockVLANTunnelFormData.tenant,
    remoteTunnel: {
      encap: <Encap> {
        ivid: mockVLANTunnelFormData.encap.vlan.vid.inner
      }
    },
    attachmentId: mockVLANTunnelFormData.attachment
  };

  const mockVXLANTunnel: TunnelConfig = {
    name: mockVXLANTunnelFormData.name,
    description: mockVXLANTunnelFormData.description,
    siteId: mockVXLANTunnelFormData.site,
    tenantId: mockVXLANTunnelFormData.tenant,
    remoteTunnel: {
      encap: <Encap> {
        vxlan: <Vxlan> {
          vni: mockVXLANTunnelFormData.encap.vxlan.vni,
          vtep: <VxlanVtep> {
            ipAddr: mockVXLANTunnelFormData.encap.vxlan.ip,
            mac: mockVXLANTunnelFormData.encap.vxlan.mac,
            udpPort: mockVXLANTunnelFormData.encap.vxlan.udpPort
          }
        }
      }
    }
  };

  const mockLocalUnpairedTunnel: TunnelConfig = {
    identifier: '123',
    name: 'Local Unpaired',
    siteId: 'my_site',
    tenantId: 'my_tenant',
    localTunnel: {}
  };

  const mockAttachments: Attachment[] = [
    {
      identifier: '1',
      name: 'attachment1',
      siteId: mockSites[1].config.identifier,
      description: 'First Attachment'
    },
    {
      identifier: '2',
      name: 'attachment2',
      siteId: mockSites[1].config.identifier,
      description: 'Second Attachment'
    },
    {
      identifier: '3',
      name: 'attachment3',
      siteId: mockSites[1].config.identifier,
      description: 'Third Attachment'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TunnelDialogComponent,
        EncapComponent
      ],
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatDialogModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatExpansionModule,
        MatCheckboxModule,
        MatIconModule,
        MatTooltipModule,
        MatRadioModule,
        MatButtonToggleModule,
        MatCardModule
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: {
            tenants: mockTenants,
            sites: mockSites
          }
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        TunnelsService,
        AttachmentsService,
        AlertService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    tunnelService = TestBed.inject(TunnelsService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TunnelDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogSpy.close.calls.reset();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render title', () => {
    let headingEl: HTMLHeadingElement = fixture.debugElement.query(
      By.css('[mat-dialog-title]')
    ).nativeElement;

    expect(headingEl.textContent).toBe('Add New Tunnel', 'render new tunnel title');

    component['_data'].tunnel = mockRawTunnelFormData;
    fixture.detectChanges();

    headingEl = fixture.debugElement.query(By.css('[mat-dialog-title]')).nativeElement;

    expect(headingEl.textContent).toBe('Edit Tunnel', 'render edit tunnel title');
  });

  it('should render tunnel form', () => {
    expect(fixture.debugElement.query(
      By.css('#tunnel-form')
    ).nativeElement).not.toBeNull('render tunnel form');
  });

  it('should get site attachments', () => {
    spyOn<any>(component, 'getSiteAttachments').and.callThrough();

    component['_data'].tunnel = mockRawTunnel;
    component.ngOnInit();

    // Edit Tunnel dialog behavior
    expect(component['getSiteAttachments']).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/attachments?filter=eq(site_id,${encodeURIComponent(String(mockRawTunnelFormData.site))})`
    });

    request.flush(<Attachments> {
      attachments: mockAttachments
    });

    expect(
      component['_attachments'][0].name
    ).toBe(mockAttachments[0].name, 'a valid attachment name');
    expect(
      component['_attachments'][1].siteId
    ).toBe(mockAttachments[1].siteId, 'a valid attachment site');
    expect(
      component['_attachments'][2].name
    ).toBe(mockAttachments[2].name, 'a valid attachment name');
  });

  it('should update site attachments', () => {
    component['_tunnelForm']
      .get('site')
      .patchValue(mockRawTunnelFormData.site);

    const request: TestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/attachments?filter=eq(site_id,${encodeURIComponent(String(mockRawTunnelFormData.site))})`
    });

    request.flush(<Attachments> {
      attachments: mockAttachments
    });

    expect(
      component['_attachments'][0].name
    ).toBe(mockAttachments[0].name, 'a valid attachment name');
    expect(
      component['_attachments'][1].siteId
    ).toBe(mockAttachments[1].siteId, 'a valid attachment site');
    expect(
      component['_attachments'][2].name
    ).toBe(mockAttachments[2].name, 'a valid attachment name');
  });

  it('should validate unique tunnel name', () => {
    component['_selectedType'] = TunnelType.LOCAL;
    component['_tunnelForm']
      .get('associatePeerTunnel')
      .setValue(true);

    expect(
      component['_tunnelForm'].get('peerTunnel').hasError('uniqueTunnelName')
    ).toBeFalsy('validate empty name');

    component['_tunnelForm'].patchValue({
      name: 'Nefeli',
      peerOption: PeerOption.NEW,
      peerTunnel: 'nefeli'
    });

    expect(
      component['_tunnelForm'].get('peerTunnel').hasError('uniqueTunnelName')
    ).toBeTruthy('invalidate duplicate name');

    component['_tunnelForm'].patchValue({
      peerTunnel: 'Nefeli 2'
    });

    expect(
      component['_tunnelForm'].get('peerTunnel').hasError('uniqueTunnelName')
    ).toBeFalsy('validate unique name');
  });

  it('should not submit invalid form', () => {
    const form: FormGroup = component['_tunnelForm'];
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    spyOn(tunnelService, 'postTunnel').and.callThrough();

    const { name, physicalPort } = mockRawTunnelFormData;

    form.patchValue({ name, physicalPort });
    form.markAsDirty();
    submitButtonEl.click();

    expect(tunnelService.postTunnel).not.toHaveBeenCalled();
    expect(dialogSpy.close).not.toHaveBeenCalled();
  });

  it('should fill form controls values for Edit Tunnel dialog', () => {
    component['_data'].tunnel = mockVXLANTunnel;
    component['initTunnelForm']();

    fixture.detectChanges();

    expect(
      component['_tunnelForm'].get('name').value
    ).toBe(mockVXLANTunnel.name, 'valid Name control value');
    expect(
      component['_tunnelForm'].get('description').value
    ).toBe(mockVXLANTunnel.description, 'valid description control value');
    expect(
      component['_tunnelForm'].get('tenant').value
    ).toBe(mockVXLANTunnel.tenantId, 'valid Tenant control value');
    expect(
      component['_tunnelForm'].get('site').value
    ).toBe(mockVXLANTunnel.siteId, 'valid Site control value');
    expect(
      component['_tunnelForm'].get('attachment').value
    ).toBe(mockVXLANTunnel.attachmentId, 'valid Attachment control value');

    expect(
      component['_tunnelForm'].get('encap.vxlan.vni').value
    ).toBe(mockVXLANTunnel.remoteTunnel.encap.vxlan.vni, 'valid VNI control value');
    expect(
      component['_tunnelForm'].get('encap.vxlan.mac').value
    ).toBe(mockVXLANTunnel.remoteTunnel.encap.vxlan.vtep.mac, 'valid MAC control value');
    expect(
      component['_tunnelForm'].get('encap.vxlan.ip').value
    ).toBe(mockVXLANTunnel.remoteTunnel.encap.vxlan.vtep.ipAddr, 'valid IP control value');
    expect(
      component['_tunnelForm'].get('encap.vxlan.udpPort').value
    ).toBe(mockVXLANTunnel.remoteTunnel.encap.vxlan.vtep.udpPort, 'valid UPD port control value');
  });

  it('should make a request submitting a valid raw tunnel form', async () => {
    const form: FormGroup = component['_tunnelForm'];
    await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Complete'
    }));

    spyOn(tunnelService, 'postTunnel').and.callThrough();

    form.patchValue(mockRawTunnelFormData);
    form.markAsDirty();
    component.submitTunnelForm();

    expect(tunnelService.postTunnel).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tunnels`
    );

    expect(request.request.method).toBe('POST');

    expect(request.request.body.name).toBe(mockRawTunnelFormData.name);
    expect(request.request.body.description).toBe(mockRawTunnelFormData.description);
    expect(request.request.body.siteId).toBe(mockRawTunnelFormData.site);
    expect(request.request.body.tenantId).toBe(mockRawTunnelFormData.tenant);
    expect(
      request.request.body.remoteTunnel.physicalPort
    ).toBe(mockRawTunnelFormData.physicalPort);
    expect(request.request.body.attachmentId).toBe(mockRawTunnelFormData.attachment);

    request.flush(mockRawTunnel);

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should make a request submitting a valid VLAN tunnel form', () => {
    const form: FormGroup = component['_tunnelForm'];
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    spyOn(tunnelService, 'postTunnel').and.callThrough();

    form.patchValue(mockVLANTunnelFormData);
    form.markAsDirty();
    fixture.detectChanges();

    // FIXME(andrew): form someway is losing dirty state in tests
    form.markAsDirty();
    fixture.detectChanges();

    submitButtonEl.click();

    expect(tunnelService.postTunnel).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tunnels`
    );

    expect(request.request.method).toBe('POST');

    expect(request.request.body.name).toBe(mockVLANTunnelFormData.name);
    expect(request.request.body.description).toBe(mockVLANTunnelFormData.description);
    expect(request.request.body.siteId).toBe(mockVLANTunnelFormData.site);
    expect(request.request.body.tenantId).toBe(mockVLANTunnelFormData.tenant);
    expect(request.request.body.attachmentId).toBe(mockVLANTunnelFormData.attachment);

    expect(request.request.body.remoteTunnel.encap.ivid).toBe(Number(mockVLANTunnelFormData.encap.vlan.vid.inner));
    expect(request.request.body.remoteTunnel.encap.ovid).toBeUndefined();

    request.flush(mockVLANTunnel);

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should make a request submitting a valid VXLAN tunnel form', () => {
    const form: FormGroup = component['_tunnelForm'];
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    spyOn(tunnelService, 'postTunnel').and.callThrough();

    form.patchValue(mockVXLANTunnelFormData);
    form.markAsDirty();
    fixture.detectChanges();
    submitButtonEl.click();

    expect(tunnelService.postTunnel).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tunnels`
    );

    expect(request.request.method).toBe('POST');

    expect(request.request.body.name).toBe(mockVXLANTunnelFormData.name);
    expect(request.request.body.description).toBe(mockVXLANTunnelFormData.description);
    expect(request.request.body.siteId).toBe(mockVXLANTunnelFormData.site);
    expect(request.request.body.tenantId).toBe(mockVXLANTunnelFormData.tenant);
    expect(request.request.body.attachmentId).toBeUndefined();

    expect(request.request.body.remoteTunnel.encap.vxlan.vni).toBe(mockVXLANTunnelFormData.encap.vxlan.vni);
    expect(
      request.request.body.remoteTunnel.encap.vxlan.vtep.mac
    ).toBe(mockVXLANTunnelFormData.encap.vxlan.mac);
    expect(
      request.request.body.remoteTunnel.encap.vxlan.vtep.ipAddr
    ).toBe(mockVXLANTunnelFormData.encap.vxlan.ip);
    expect(
      request.request.body.remoteTunnel.encap.vxlan.vtep.udpPort
    ).toBe(mockVXLANTunnelFormData.encap.vxlan.udpPort);

    request.flush(mockVXLANTunnel);

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should not close dialog when getting an error request response', () => {
    const form: FormGroup = component['_tunnelForm'];
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    spyOn(tunnelService, 'postTunnel').and.callThrough();

    form.patchValue(mockVLANTunnelFormData);
    form.markAsDirty();
    fixture.detectChanges();

    // FIXME(andrew): form someway is losing dirty state in tests
    form.markAsDirty();
    fixture.detectChanges();

    submitButtonEl.click();

    expect(tunnelService.postTunnel).toHaveBeenCalled();

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/tunnels`
    );

    expect(request.request.method).toBe('POST');

    const mockErrorResponseOptions: Partial<HttpErrorResponse> = {
      status: 400,
      statusText: 'Bad Request'
    };

    request.flush({}, mockErrorResponseOptions);

    expect(dialogSpy.close).not.toHaveBeenCalled();
  });

  it('should confirm force submit if geting conflict request error', waitForAsync(() => {
    const form: FormGroup = component['_tunnelForm'];
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    form.patchValue(mockVLANTunnelFormData);
    form.markAsDirty();
    fixture.detectChanges();

    // FIXME(andrew): form someway is losing dirty state in tests
    form.markAsDirty();
    fixture.detectChanges();

    submitButtonEl.click();

    const request: TestRequest = httpTestingController.expectOne({
      url: `${environment.restServer}/v1/tunnels`,
      method: 'POST'
    });

    request.flush(
      {
        errorCode: HttpErrorCode.InUse
      },
      {
        status: 409,
        statusText: 'Conflict Request'
      });
    spyOn(component, 'submitTunnelForm').and.callThrough();

    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    const confirmDialog: ConfirmationDialogComponent = dialog.componentInstance;
    expect(confirmDialog.action).toBe('Proceed anyways');
    dialog.close(true);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      expect(component.submitTunnelForm).toHaveBeenCalledWith(true);
    });
  }));

  it('should not display confirm dialog when got duplicate config response', waitForAsync(() => {
    spyOn(component['_alertService'], 'error');
    const form: FormGroup = component['_tunnelForm'];
    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;

    form.patchValue(mockVLANTunnelFormData);
    form.markAsDirty();
    fixture.detectChanges();

    // FIXME(andrew): form someway is losing dirty state in tests
    form.markAsDirty();
    fixture.detectChanges();

    submitButtonEl.click();

    const request: TestRequest = httpTestingController.expectOne({
      url: `${environment.restServer}/v1/tunnels`,
      method: 'POST'
    });

    request.flush(
      {
        errorCode: 807,
        message: 'Duplicate configuration'
      },
      {
        status: 409,
        statusText: ''
      });
    expect(component['_alertService'].error).toHaveBeenCalledWith(
      AlertType.ERROR_CREATE_TUNNEL,
      'Duplicate configuration'
    );
  }));

  it('should render local tunnel form', async () => {
    const el = fixture.debugElement.nativeElement;
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    // verify default form
    expect(el.querySelectorAll('mat-form-field').length).toBe(4);

    const inputName = el.querySelector('input[formControlName="name"]');
    const selectTenant = el.querySelector('mat-select[formControlName="tenant"]');
    const selectSite = el.querySelector('mat-select[formControlName="site"]');
    expect(inputName).toBeDefined();
    expect(selectTenant).toBeDefined();
    expect(selectSite).toBeDefined();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    expect(await associatePeerTunnelCheckbox.isChecked()).toBe(false);

    await associatePeerTunnelCheckbox.check();

    expect(await associatePeerTunnelCheckbox.isChecked()).toBe(true);

    const radioNew = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: 'New'
    }));
    const radioExisting = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: 'Existing'
    }));

    expect(await radioNew.isChecked()).toBe(true);

    // verify creating a new peer tunnel form
    expect(el.querySelectorAll('mat-form-field').length).toBe(7);
    let selectPeerTenant = el.querySelector('mat-select[formControlName="peerTenant"]');
    const inputPeerTunnel = el.querySelector('input[formControlName="peerTunnel"]');
    const peerDescriptionDe = fixture.debugElement.query(
      By.css('input[formControlName="peerDescription"]')
    );

    expect(selectPeerTenant).toBeDefined();
    expect(inputPeerTunnel).toBeDefined();
    expect(peerDescriptionDe).toBeDefined();

    // verify assigning to existing tunnel form
    await radioExisting.check();
    fixture.detectChanges();

    expect(el.querySelectorAll('mat-form-field').length).toBe(6);
    selectPeerTenant = el.querySelector('mat-select[formControlName="peerTenant"]');
    const selectPeerTunnel = el.querySelector('mat-select[formControlName="peerTunnel"]');
    expect(selectPeerTenant).toBeDefined();
    expect(selectPeerTunnel).toBeDefined();
  });

  it('should make a request creating an unpaired local tunnel', () => {
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    component.tunnelForm.get('name').patchValue('Unpaired Local Tunnel');
    component.tunnelForm.get('tenant').patchValue('1234567');
    component.tunnelForm.get('site').patchValue('eric_site');
    component.tunnelForm.markAsDirty();
    fixture.detectChanges();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;
    submitButtonEl.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels`).request;

    expect(req.body.name).toBe('Unpaired Local Tunnel');
    expect(req.body.siteId).toBe('eric_site');
    expect(req.body.tenantId).toBe('1234567');
    expect(req.body.localTunnel).toBeDefined();
    expect(req.method).toBe('POST');
  });

  it('should make a request creating a local tunnel paired to existing tunnel', async () => {
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    const radioExisting = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: 'Existing'
    }));
    await radioExisting.check();
    fixture.detectChanges();

    component.tunnelForm.get('name').patchValue('Unpaired Local Tunnel');
    component.tunnelForm.get('tenant').patchValue('1234567');
    component.tunnelForm.get('site').patchValue('eric_site');
    component.tunnelForm.get('peerTenant').patchValue('7654321');
    component.tunnelForm.get('peerTunnel').patchValue('98765');
    component.tunnelForm.markAsDirty();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;
    submitButtonEl.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels`).request;

    expect(req.body.name).toBe('Unpaired Local Tunnel');
    expect(req.body.siteId).toBe('eric_site');
    expect(req.body.tenantId).toBe('1234567');
    expect(req.body.localTunnel.peerTunnelId).toBe('98765');
    expect(req.method).toBe('POST');
  });

  it('should make a request creating a local tunnel paired to a new tunnel', async () => {
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    component.tunnelForm.get('name').patchValue('Paired Local Tunnel');
    component.tunnelForm.get('tenant').patchValue('1234567');
    component.tunnelForm.get('site').patchValue('eric_site');
    component.tunnelForm.get('peerTenant').patchValue('7654321');
    component.tunnelForm.get('peerTunnel').patchValue('Peer Local Tunnel');
    component.tunnelForm.get('peerDescription').patchValue('Peer tunnel');
    component.tunnelForm.markAsDirty();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;
    submitButtonEl.click();

    dialogSpy.close.and.callFake((tunnels: TunnelConfig[]) => {
      /* TODO(andrew): `tunnels[0]` and `tunnels[1]` are sometimes `undefined` when
         _callFake_ is called twice */
      if (tunnels[0] && tunnels[1]) {
        expect(tunnels[0].identifier).toBe(tunnels[1].localTunnel.peerTunnelId);
        expect(tunnels[1].identifier).toBe(tunnels[0].localTunnel.peerTunnelId);
      }
    });

    const tunnel1: TunnelConfig = {
      tenantId: '1234567',
      siteId: 'eric_site',
      name: 'Paired Local Tunnel',
      attachmentId: '',
      localTunnel: {
        peerTunnelId: ''
      },
      identifier: '3798afc1-dfb4-4e08-9b32-c13aefb6baa7'
    };
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels`);
    expect(req.request.body.name).toBe(tunnel1.name);
    expect(req.request.body.siteId).toBe(tunnel1.siteId);
    expect(req.request.body.tenantId).toBe(tunnel1.tenantId);
    expect(req.request.method).toBe('POST');
    req.flush(tunnel1);

    const tunnel2: TunnelConfig = {
      tenantId: '7654321',
      siteId: 'eric_site',
      name: 'Peer Local Tunnel',
      description: 'Peer tunnel',
      attachmentId: '',
      localTunnel: {
        peerTunnelId: '3798afc1-dfb4-4e08-9b32-c13aefb6baa7'
      },
      identifier: '4044815c-4266-4b3a-b498-ad1424b01d01'
    };
    const peerReq = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels`);
    expect(peerReq.request.body.name).toBe(tunnel2.name);
    expect(peerReq.request.body.description).toBe(tunnel2.description);
    expect(peerReq.request.body.siteId).toBe(tunnel2.siteId);
    expect(peerReq.request.body.tenantId).toBe(tunnel2.tenantId);
    expect(peerReq.request.method).toBe('POST');
    peerReq.flush(tunnel2);

    expect(dialogSpy.close).toHaveBeenCalled();
  });

  it('should request for local tunnels when selecting tenant', async () => {
    const el = fixture.debugElement.nativeElement;
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    const radioExisting = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: 'Existing'
    }));
    await radioExisting.check();
    fixture.detectChanges();

    const tenantSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: 'mat-select[formControlName="peerTenant"]'
    }));
    await tenantSelect.clickOptions({
      text: 'Tenant 2'
    });
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/tunnels`);

    await tenantSelect.clickOptions({
      text: 'Nefeli Networks'
    });
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/4/tunnels`);
  });

  it('should display error if tenant has no tunnels', async () => {
    spyOn(component['_alertService'], 'error');
    const el = fixture.debugElement.nativeElement;
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    const radioExisting = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: 'Existing'
    }));
    await radioExisting.check();
    fixture.detectChanges();

    const tenantSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: 'mat-select[formControlName="peerTenant"]'
    }));
    await tenantSelect.clickOptions({
      text: 'Tenant 2'
    });
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/tunnels`).flush({});
    fixture.detectChanges()
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_TENANT_NO_TUNNELS);
  });

  it('should display error if failed to get tunnels', async () => {
    spyOn(component['_alertService'], 'error');
    const el = fixture.debugElement.nativeElement;
    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    const radioExisting = await loader.getHarness<MatRadioButtonHarness>(MatRadioButtonHarness.with({
      label: 'Existing'
    }));
    await radioExisting.check();
    fixture.detectChanges();

    const tenantSelect = await loader.getHarness<MatSelectHarness>(MatSelectHarness.with({
      selector: 'mat-select[formControlName="peerTenant"]'
    }));
    await tenantSelect.clickOptions({
      text: 'Tenant 2'
    });
    httpTestingController.expectOne(`${environment.restServer}/v1/tenants/2/tunnels`).flush({}, {
      status: 400,
      statusText: 'Bad Request'
    });
    fixture.detectChanges()
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_RETRIEVE_TUNNELS);
  });

  it('should edit an existing unpaired local tunnel and pair to new tunnel', async () => {
    const dialogData: TunnelDialogData = {
      tunnel: mockLocalUnpairedTunnel,
      tenants: mockTenants,
      sites: mockSites
    };

    component['_data'] = dialogData;
    component.ngOnInit();
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    component.tunnelForm.get('peerTenant').patchValue('7654321');
    component.tunnelForm.get('peerTunnel').patchValue('Peer Local Tunnel');
    component.tunnelForm.get('peerDescription').patchValue('Peer tunnel');
    component.tunnelForm.markAsDirty();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;
    submitButtonEl.click();

    httpTestingController.expectOne(`${environment.restServer}/v1/tunnels/${mockLocalUnpairedTunnel.identifier}`).flush({});

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels`).request;
    expect(req.method).toBe('POST');
    expect(req.body.localTunnel.peerTunnelId).toBe(mockLocalUnpairedTunnel.identifier);
    expect(req.body.name).toBe('Peer Local Tunnel');
    expect(req.body.description).toBe('Peer tunnel');
    expect(req.body.siteId).toBe(mockLocalUnpairedTunnel.siteId);
    expect(req.body.tenantId).toBe('7654321');
  });

  it('should display proper error if no encap specified', () => {
    spyOn(component['_alertService'], 'error');

    component.tunnelForm.get('name').setValue('My Tunnel');
    component.tunnelForm.get('tenant').setValue('My Tenant');
    component.tunnelForm.get('site').setValue('My Site');
    component.tunnelForm.markAsDirty();

    component.submitTunnelForm();

    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_CREATE_TUNNEL_NO_ENCAP);
  });

  it('should convert remote tunnel to local tunnel', () => {
    component['_data'].tunnel = mockRawTunnel;
    component.ngOnInit();

    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;
    component.tunnelForm.markAsDirty();
    fixture.detectChanges();
    submitButtonEl.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels/${mockRawTunnel.identifier}`).request;
    expect(req.method).toBe('PUT');
    expect(req.body.identifier).toBe('123');
    expect(req.body.localTunnel).toBeDefined();
    expect(req.body.remoteTunnel).toBeUndefined();
    expect(req.body.name).toBe('Right Tunnel');
    expect(req.body.siteId).toBe('andrew_site');
    expect(req.body.tenantId).toBe('1');
  });

  it('should convert remote tunnel to local tunnel and create a new peer', async () => {
    component['_data'].tunnel = mockRawTunnel;
    component.ngOnInit();

    const localButton: DebugElement = fixture.debugElement.query(By.css('mat-button-toggle-group'));
    localButton.triggerEventHandler('change', { value: component.TunnelType.LOCAL });
    fixture.detectChanges();

    const associatePeerTunnelCheckbox = await loader.getHarness(MatCheckboxHarness.with({
      label: 'Associate Peer Tunnel'
    }));

    await associatePeerTunnelCheckbox.check();

    component.tunnelForm.get('peerTenant').patchValue('7654321');
    component.tunnelForm.get('peerTunnel').patchValue('Peer Local Tunnel');
    component.tunnelForm.get('peerDescription').patchValue('Peer tunnel');
    component.tunnelForm.markAsDirty();

    const submitButtonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('button[type=submit]')
    ).nativeElement;
    submitButtonEl.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels/${mockRawTunnel.identifier}`);
    expect(req.request.method).toBe('PUT');
    expect(req.request.body.identifier).toBe('123');
    expect(req.request.body.localTunnel).toBeDefined();
    expect(req.request.body.remoteTunnel).toBeUndefined();
    expect(req.request.body.name).toBe('Right Tunnel');
    expect(req.request.body.siteId).toBe('andrew_site');
    expect(req.request.body.tenantId).toBe('1');
    req.flush({});

    const peerReq = httpTestingController.expectOne(`${environment.restServer}/v1/tunnels`).request;
    expect(peerReq.method).toBe('POST');
    expect(peerReq.body.localTunnel.peerTunnelId).toBe(mockRawTunnel.identifier);
    expect(peerReq.body.name).toBe('Peer Local Tunnel');
    expect(peerReq.body.description).toBe('Peer tunnel');
    expect(peerReq.body.siteId).toBe('andrew_site');
    expect(peerReq.body.tenantId).toBe('7654321');
  });
});
