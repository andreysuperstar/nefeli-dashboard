import { Component, Inject, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup, FormBuilder, Validators, ValidationErrors, AbstractControl, AbstractControlOptions } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatCheckboxChange } from '@angular/material/checkbox';

import { Observable, Subscription } from 'rxjs';
import { tap, filter, switchMap, map, concatMap } from 'rxjs/operators';

import { Tenant } from 'src/app/tenant/tenant.service';
import { HttpError, HttpErrorCode } from '../../error-codes';
import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';

import { Site } from 'rest_client/pangolin/model/site';
import { Attachment } from 'rest_client/pangolin/model/attachment';
import { Attachments } from 'rest_client/pangolin/model/attachments';
import { Encap } from 'rest_client/pangolin/model/encap';
import { VxlanVtep } from 'rest_client/pangolin/model/vxlanVtep';
import { Vxlan } from 'rest_client/pangolin/model/vxlan';
import { TunnelsService } from 'rest_client/pangolin/api/tunnels.service';
import { AttachmentsService } from 'rest_client/pangolin/api/attachments.service';
import { Tunnel } from 'rest_client/pangolin/model/tunnel';
import { RemoteTunnel } from 'rest_client/pangolin/model/remoteTunnel';
import { Tunnels } from 'rest_client/pangolin/model/tunnels';
import { TunnelConfig } from 'rest_client/pangolin/model/tunnelConfig';
import { LocalTunnel } from 'rest_client/pangolin/model/localTunnel';
import { AlertType, AlertService } from 'src/app/shared/alert.service';

import { VLANType, VLANMode, EncapComponent } from '../../encap/encap.component';

export interface TunnelDialogData {
  tunnel?: TunnelConfig;
  peerTunnel?: TunnelConfig;
  tenants: Tenant[];
  sites: Site[];
}

export enum TunnelType {
  REMOTE = 'remote',
  LOCAL = 'local'
}

export enum PeerOption {
  NONE = 'none',
  NEW = 'new',
  EXISTING = 'existing'
}

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

@Component({
  selector: 'nef-tunnel-dialog',
  templateUrl: './tunnel-dialog.component.html',
  styleUrls: ['./tunnel-dialog.component.less']
})
export class TunnelDialogComponent implements OnInit, OnDestroy {

  private _tunnelForm: FormGroup;
  private _attachments: Attachment[];
  private _tunnelSubscription: Subscription;
  private _subscription = new Subscription();
  private _selectedType: TunnelType = TunnelType.REMOTE;
  private _selectedTenantTunnels: Tunnel[];
  private _dialogSubscription: Subscription;

  @ViewChild('encap') private _encapComponent: EncapComponent;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<TunnelDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: TunnelDialogData,
    private _tunnelService: TunnelsService,
    private _attachmentService: AttachmentsService,
    private _alertService: AlertService,
    private _dialog: MatDialog
  ) { }

  public ngOnInit() {
    this.initTunnelForm();

    if (this._data.tunnel) {
      this.getSiteAttachments();
    } else {
      this.updateSiteAttachments();
    }
  }

  public ngOnDestroy() {
    if (this._tunnelSubscription) {
      this._tunnelSubscription.unsubscribe();
    }

    this._subscription.unsubscribe();
  }

  private initTunnelForm() {
    const disabled: boolean = Boolean(this._data.tunnel);

    this._selectedType = this._data.tunnel?.localTunnel ? TunnelType.LOCAL : TunnelType.REMOTE;

    if (this._data.tunnel?.localTunnel?.peerTunnelId) {
      this.onSelectedTenantChange(this._data.peerTunnel.tenantId);
    }

    this._tunnelForm = this._fb.group(
      {
        name: [
          {
            value: this._data?.tunnel?.name,
            disabled
          },
          Validators.required
        ],
        description: this._data?.tunnel?.description,
        tenant: [
          {
            value: this._data?.tunnel?.tenantId,
            disabled
          },
          Validators.required
        ],
        site: [
          {
            value: this._data?.tunnel?.siteId,
            disabled
          },
          Validators.required
        ],
        physicalPort: this._data?.tunnel?.remoteTunnel?.physicalPort,
        attachment: {
          value: this._data?.tunnel?.attachmentId,
          // should be disabled until attachments arrive
          disabled: !this._attachments
        },
        encap: this._fb.group({}),
        associatePeerTunnel: this._data.tunnel?.localTunnel?.peerTunnelId ? true : undefined,
        peerTenant: [
          {
            value: this._data?.peerTunnel?.tenantId,
            disabled: false
          },
          this.peerValidator.bind(this)
        ],
        peerTunnel: [
          {
            value: this._data?.tunnel?.localTunnel?.peerTunnelId,
            disabled: false
          },
          this.peerValidator.bind(this)
        ],
        peerDescription: undefined,
        peerOption: !this._data.tunnel || this._data.tunnel.remoteTunnel
          ? PeerOption.NONE
          : this._data.tunnel.localTunnel?.peerTunnelId
            ? PeerOption.EXISTING
            : PeerOption.NONE
      },
      {
        validators: this.uniqueTunnelNameValidator.bind(this)
      } as AbstractControlOptions
    );
  }

  private peerValidator(control: AbstractControl): ValidationErrors {
    return this._selectedType === TunnelType.LOCAL
      && this._tunnelForm?.get('associatePeerTunnel').value
      && !control.value
      ? {
        required: true
      }
      : undefined;
  }

  private uniqueTunnelNameValidator(form: FormGroup) {
    const tunnelName: string = form?.get('name').value?.trim?.();
    const peerTunnelControl = form?.get('peerTunnel');
    const peerTunnelName: string = peerTunnelControl.value?.trim?.();

    if (this._selectedType === TunnelType.LOCAL
      && form?.get('associatePeerTunnel').value
      && form?.get('peerOption').value === PeerOption.NEW
      && peerTunnelName?.length && tunnelName?.length
      && peerTunnelName.toLocaleLowerCase() === tunnelName.toLocaleLowerCase()) {
      peerTunnelControl.setErrors({
        ...peerTunnelControl.errors,
        uniqueTunnelName: true
      });
    } else {
      peerTunnelControl.updateValueAndValidity({
        onlySelf: true
      });
    }
  }

  /**
   * used to update site attachment options for Add New Tunnel dialog only
   */
  private updateSiteAttachments() {
    const subscription: Subscription = this._tunnelForm
      .get('site')
      .valueChanges
      .pipe(
        tap((_: string) => {
          // disable attachment field when site is changing
          this._tunnelForm
            .get('attachment')
            .disable();
        }),
        // pass just if an actual site option is selected
        filter(Boolean),
        switchMap<string, Observable<Attachments>>((site) => {
          // get site attachment options
          return this._attachmentService.getAttachments([`eq(site_id,${site})`]);
        })
      )
      .subscribe(({ attachments }: Attachments) => {
        this._attachments = attachments as Attachment[];

        const hasAttachment = attachments.some(
          ({ name }) => this._tunnelForm.get('attachment').value === name
        );

        if (!hasAttachment) {
          this._tunnelForm
            .get('attachment')
            .reset();
        }

        this._tunnelForm
          .get('attachment')
          .enable();
      });

    this._subscription.add(subscription);
  }

  /**
   * used to get site attachment options for Edit Tunnel dialog only
   */
  private getSiteAttachments() {
    const site: string = this._data.tunnel.siteId;

    const subscription: Subscription = this._attachmentService
      .getAttachments([`eq(site_id,${site})`])
      .subscribe(({ attachments }: Attachments) => {
        this._attachments = attachments as Attachment[];

        this._tunnelForm
          .get('attachment')
          .enable();
      });

    this._subscription.add(subscription);
  }

  private resetPeerTenantAndTunnel() {
    this._tunnelForm
      .get('peerTenant')
      .reset();

    this._tunnelForm
      .get('peerTunnel')
      .reset();
  }

  private displayConfirmation() {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: `This tunnel is currently in use. These changes may interrupt service(s).`,
      action: 'Proceed anyways'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe(() => {
        this.submitTunnelForm(true);
      });
  }

  public onAssociatePeerTunnelChange({ checked }: MatCheckboxChange) {
    if (!checked) {
      this.resetPeerTenantAndTunnel();

      this._tunnelForm
        .get('peerOption')
        .patchValue(PeerOption.NONE);
    } else {
      if (this._tunnelForm.get('peerOption').value === PeerOption.NONE) {
        this._tunnelForm.get('peerOption').patchValue(PeerOption.NEW);
      }
    }
  }

  public onPeerTunnelTypeChange() {
    this.resetPeerTenantAndTunnel();
  }

  public submitLocalTunnel(force?: boolean) {
    const { description, peerTenant, peerTunnel, peerDescription } = this._tunnelForm.value;
    const name = this._tunnelForm?.get('name').value;
    const tenant = this._tunnelForm?.get('tenant').value;
    const site = this._tunnelForm?.get('site').value;
    const localTunnel: LocalTunnel =
      (this._tunnelForm?.get('peerOption').value === PeerOption.EXISTING) ? { peerTunnelId: peerTunnel } : {};

    const tunnel: TunnelConfig = {
      identifier: this._data.tunnel?.identifier,
      name,
      description,
      siteId: site,
      tenantId: tenant,
      localTunnel
    };

    let obs$: Observable<TunnelConfig>;

    if (this._tunnelForm?.get('peerOption').value === PeerOption.NEW) {
      const localPeerTunnel: TunnelConfig = {
        name: peerTunnel,
        description: peerDescription,
        tenantId: peerTenant,
        siteId: site,
        localTunnel: {
          peerTunnelId: this._data?.tunnel?.identifier
        }
      };

      if (this._data?.tunnel) {
        obs$ = this._tunnelService
          .putTunnel(this._data.tunnel.identifier, tunnel, force).pipe(
            concatMap((res: TunnelConfig) => {
              return this._tunnelService.postTunnel(localPeerTunnel);
            })
          );
      } else {
        obs$ = this._tunnelService
          .postTunnel(tunnel).pipe(
            concatMap((res: TunnelConfig) => {
              localPeerTunnel.localTunnel.peerTunnelId = res.identifier;
              return this._tunnelService.postTunnel(localPeerTunnel);
            })
          );
      }
    } else {
      obs$ = this._data.tunnel
        ? this._tunnelService.putTunnel(this._data.tunnel.identifier, tunnel, force)
        : this._tunnelService.postTunnel(tunnel);
    }

    if (this._tunnelSubscription) {
      this._tunnelSubscription.unsubscribe();
    }

    this._tunnelSubscription = obs$.subscribe(
      (response: TunnelConfig) => {
        this._data.tunnel
          ? this._alertService.info(AlertType.INFO_UPDATE_TUNNEL_SUCCESS)
          : this._alertService.info(AlertType.INFO_CREATE_TUNNEL_SUCCESS);

        const tunnels = [response];

        if (this._tunnelForm?.get('peerOption').value === PeerOption.NEW) {
          tunnel.identifier = response.localTunnel?.peerTunnelId;
          tunnel.localTunnel = { peerTunnelId: response.identifier };
          tunnels.push(tunnel);
        }

        this._dialogRef.close(tunnels);
      },
      ({ status, message }: HttpErrorResponse) => {
        if (status === HttpError.Conflict) {
          this.displayConfirmation();
        } else {
          this._data.tunnel
            ? this._alertService.error(AlertType.ERROR_UPDATE_TUNNEL, message)
            : this._alertService.error(AlertType.ERROR_CREATE_TUNNEL, message);
        }
      }
    );
  }

  public submitRemoteTunnel(force?: boolean) {
    /* assign name, tenant and site values from form controls separately
    as disabled form fields aren't fallen into form value property */
    const name: string = this._tunnelForm?.get('name').value;
    const tenant: string = this._tunnelForm?.get('tenant').value;
    const site: string = this._tunnelForm?.get('site').value;
    const attachment: string = this._tunnelForm?.get('attachment').value;

    const {
      description,
      physicalPort,
      encap: {
        vlan: {
          type: vlanType,
          vid
        },
        vxlan: { vni, mac, ip, udpPort }
      }
    }: {
      name: string;
      description: string;
      site: string;
      tenant: string;
      physicalPort: string;
      attachment: string;
      encap: {
        vlan: {
          type: string;
          vid: {
            inner: number;
            outer: number;
          }
        },
        vxlan: {
          vni: number;
          mac: string;
          ip: string;
          udpPort: number;
        }
      }
    } = this._tunnelForm.value;

    let ivid: number;
    let ovid: number;
    let vxlan: Vxlan;

    switch (vlanType) {
      case VLANType.None:
        vxlan = {
          vni,
          vtep: {
            ipAddr: ip,
            mac,
            udpPort
          } as VxlanVtep
        };
        break;
      case VLANType['802.1Q']:
        ivid = vid.inner;
        break;
      case VLANType['802.1AD']:
        ivid = vid.inner;
        ovid = vid.outer;
        break;
    }

    if (vlanType === VLANType.None
      && !physicalPort
      && !(vxlan?.vni && vxlan?.vtep?.ipAddr && vxlan?.vtep?.mac)) {
      this._alertService.error(AlertType.ERROR_CREATE_TUNNEL_NO_ENCAP);
      return;
    }

    // if fill physicalPort, leave VLAN type unselected and empty MAC or IP
    const encap: Encap = physicalPort && vlanType === VLANType.None && !mac && !ip
      ? undefined
      : { ivid, ovid, vxlan } as Encap;

    const tunnel: TunnelConfig = {
      identifier: this._data.tunnel?.identifier,
      name,
      description,
      siteId: site,
      tenantId: tenant,
      remoteTunnel: { encap, physicalPort } as RemoteTunnel,
      attachmentId: attachment
    };

    const tunnel$: Observable<TunnelConfig> = this._data.tunnel
      ? this._tunnelService.putTunnel(this._data.tunnel.identifier, tunnel, force)
      : this._tunnelService.postTunnel(tunnel);

    if (this._tunnelSubscription) {
      this._tunnelSubscription.unsubscribe();
    }

    this._tunnelSubscription = tunnel$.subscribe(
      (response: TunnelConfig) => {
        this._data.tunnel
          ? this._alertService.info(AlertType.INFO_UPDATE_TUNNEL_SUCCESS)
          : this._alertService.info(AlertType.INFO_CREATE_TUNNEL_SUCCESS);

        this._dialogRef.close([response]);
      },
      ({
        error: { errorCode, message },
        status
      }: HttpErrorResponse) => {
        if (status === HttpError.Conflict && errorCode === HttpErrorCode.InUse) {
          this.displayConfirmation();
        } else {
          this._data.tunnel
            ? this._alertService.error(AlertType.ERROR_UPDATE_TUNNEL, message)
            : this._alertService.error(AlertType.ERROR_CREATE_TUNNEL, message);
        }
      }
    );
  }

  public submitTunnelForm(force?: boolean) {
    const invalid = this._tunnelForm.invalid;
    let pristine = this._tunnelForm.pristine;

    if (this._data?.tunnel?.remoteTunnel && this._selectedType === TunnelType.LOCAL
      || this._data?.tunnel?.localTunnel && this._selectedType === TunnelType.REMOTE) {
      // modifying existing tunnel to a new type
      pristine = false;
    }

    if (invalid || pristine) {
      if (this._tunnelForm?.get('encap').invalid) {
        // workaround, to make sure form validation is performed on vlan inner vid
        if (this._tunnelForm?.get('encap.vlan.type').value === VLANType['802.1AD']) {
          this._tunnelForm?.get('encap.vlan.vid.inner').markAsTouched();
        }

        // open VLAN Configuration panels if VLAN groups have errors
        this._encapComponent.openInvalidPanels();
      }

      return;
    }

    if (this._selectedType === TunnelType.LOCAL) {
      this.submitLocalTunnel(force);
    } else if (this._selectedType === TunnelType.REMOTE) {
      this.submitRemoteTunnel(force);
    }
  }

  public onSelectedTenantChange(tenantId: string) {
    this._selectedTenantTunnels = [];
    this._tunnelService.getTenantTunnels(tenantId).pipe(
      map((tunnels: Tunnels) => tunnels?.tunnels?.filter(
        ({
          config: { identifier, localTunnel }
        }: Tunnel) => identifier !== this._data.tunnel?.identifier && localTunnel
          && (!localTunnel?.peerTunnelId
            || this._data?.tunnel?.localTunnel?.peerTunnelId === identifier)
      )
      )
    ).subscribe({
      next: (tunnels: Tunnel[]) => {
        if ((this._tunnelForm?.get('peerOption').value === PeerOption.EXISTING) && (!tunnels || tunnels.length === 0)) {
          this._alertService.error(AlertType.ERROR_TENANT_NO_TUNNELS);
        }

        this._selectedTenantTunnels = tunnels;
      },
      error: (err: Error) => {
        if (this._tunnelForm?.get('peerOption').value === PeerOption.EXISTING) {
          this._alertService.error(AlertType.ERROR_RETRIEVE_TUNNELS);
        }
      }
    });
  }

  public get tunnelForm(): FormGroup {
    return this._tunnelForm;
  }

  public get encapForm(): FormGroup {
    return this._tunnelForm?.get('encap') as FormGroup;
  }

  public get attachments(): Attachment[] {
    return this._attachments;
  }

  public get tunnel(): TunnelConfig {
    return this._data.tunnel;
  }

  public get tenants(): Tenant[] {
    return this._data.tenants;
  }

  public get sites(): Site[] {
    return this._data.sites;
  }

  public get tunnelEncap(): Encap {
    return this._data?.tunnel?.remoteTunnel?.encap;
  }

  public get selectedType(): TunnelType {
    return this._selectedType;
  }

  public set selectedType(type: TunnelType) {
    this._selectedType = type;
  }

  public get TunnelType(): typeof TunnelType {
    return TunnelType;
  }

  public get vlanMode(): VLANMode {
    if (this._tunnelForm.get('attachment').value) {
      return VLANMode['802.1QOnly'];
    }
  }

  public get PeerOption(): typeof PeerOption {
    return PeerOption;
  }

  public get selectedTenantTunnels(): Tunnel[] {
    return this._selectedTenantTunnels;
  }
}
