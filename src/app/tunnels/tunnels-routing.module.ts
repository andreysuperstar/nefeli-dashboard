import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TunnelsComponent } from './tunnels.component';

const routes: Routes = [
  {
    path: '',
    component: TunnelsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TunnelsRoutingModule { }
