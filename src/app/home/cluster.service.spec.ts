import { isEqual } from 'lodash-es';
import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from '../../environments/environment';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as STATS_BASE_PATH } from 'rest_client/pangolin/variables';
import { ClusterService, Cluster } from './cluster.service';
import { AlertService } from 'src/app/shared/alert.service';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { CommonChart } from '../charts/common-chart';
import { ClusterStatsRequest } from 'rest_client/pangolin/model/clusterStatsRequest';

describe('ClusterService', () => {
  let httpTestingController: HttpTestingController;
  let service: ClusterService;

  const mockEricSite: Site = {
    config: {
      identifier: 'eric_site',
      name: 'Eric Site'
    }
  };

  const mockAndrewSite: Site = {
    config: {
      identifier: 'andrew_site',
      name: 'Andrew Site'
    }
  };

  const mockAntonSite: Site = {
    config: {
      identifier: 'anton_site',
      name: 'Anton Site'
    }
  };

  const mockSiteResponse: Sites = {
    sites: [
      mockEricSite,
      mockAndrewSite,
      mockAntonSite
    ]
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatSnackBarModule
      ],
      providers: [
        {
          provide: PANGOLIN_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: STATS_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        SitesService,
        ClusterService,
        AlertService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(ClusterService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get site IDs', () => {
    service
      .getSiteIDs()
      .subscribe((siteIDs: string[]) => {
        expect(isEqual(siteIDs, ['eric_site', 'andrew_site', 'anton_site'])).toBeTruthy();
      });

    const request: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites`
    );

    expect(request.request.method).toBe('GET');

    request.flush(mockSiteResponse);
  });

  it('should get sites', () => {
    service
      .getSites()
      .subscribe(({ sites: sites }: Sites) => {
        expect(sites.length).toBe(mockSiteResponse.sites.length);

        expect(sites[0].config.identifier).toBe(mockEricSite.config.identifier);
        expect(sites[0].config.name).toBe(mockEricSite.config.name);

        expect(sites[1].config.identifier).toBe(mockAndrewSite.config.identifier);
        expect(sites[1].config.name).toBe(mockAndrewSite.config.name);

        expect(sites[2].config.identifier).toBe(mockAntonSite.config.identifier);
        expect(sites[2].config.name).toBe(mockAntonSite.config.name);
      });

    const sitesRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites`
    );

    sitesRequest.flush(mockSiteResponse);
  });

  it('should get clusters in asc order', () => {
    service.getClusters().subscribe((clusters: Cluster[]) => {
      expect(clusters.length).toBe(3);
      expect(clusters[0].id).toBe('andrew_site');
      expect(clusters[0].name).toBe('Andrew Site');
      expect(clusters[1].name).toBe('Anton Site');
      expect(clusters[2].name).toBe('Eric Site');
    });

    const sitesRequest: TestRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/sites`
    );

    sitesRequest.flush(mockSiteResponse);
  });

  it('should poll from cluster throughput stats', fakeAsync(() => {
    const rate = 900;
    const step = CommonChart.getOptimalStep(rate);
    const pollInterval = CommonChart.getOptimalPollInterval(rate);

    const subscription = service.streamClusterStats(
      'my_site',
      pollInterval,
      StatFilter.THROUGHPUT,
      rate,
      step,
      ['my_service1', 'my_service2']
    ).subscribe();

    let req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/my_site/stats`
        && req.body.filter.filter[0] === StatFilter.THROUGHPUT
    })[0];
    req.flush({});
    let body: ClusterStatsRequest = {
      services: ['my_service1', 'my_service2'],
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: rate,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    tick(pollInterval);

    req = httpTestingController.match((req: HttpRequest<any>) => {
      return req.url === `${environment.restServer}/v1/sites/my_site/stats`
        && req.body.filter.filter[0] === StatFilter.THROUGHPUT
    })[0];
    req.flush({});

    body = {
      services: ['my_service1', 'my_service2'],
      filter: {
        filter: [StatFilter.THROUGHPUT]
      },
      range: {
        start: undefined,
        duration: 5,
        step
      }
    };
    expect(req.request.body).toEqual(body);

    subscription.unsubscribe();
  }));
});
