import { AlarmDataSource } from '../shared/alarm.data-source';
import { Subscription, Observable } from 'rxjs';
import { pluck, map, take } from 'rxjs/operators';
import { Component, ViewChild, OnDestroy, HostBinding, OnInit, Injector } from '@angular/core';
import { WeaverSync } from 'rest_client/pangolin/model/weaverSync';
import { WeaverSyncStatusState } from 'rest_client/pangolin/model/weaverSyncStatusState';
import { SidebarData, SidebarService } from '../shared/sidebar.service';
import { HighAvailabilityService } from 'rest_client/pangolin/api/highAvailability.service';
import { AlarmService } from '../shared/alarm.service';
import { MatSidenav } from '@angular/material/sidenav';
import { UserService } from '../users/user.service';
import { MatCheckbox } from '@angular/material/checkbox';
import { LocalStorageKey, LocalStorageService } from '../shared/local-storage.service';
import { ComponentType } from '@angular/cdk/portal';

@Component({
  selector: 'nef-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit, OnDestroy {

  @HostBinding('class.weaversync-standby-banner')
  private _weaverSyncStandbyBanner: boolean;
  @HostBinding('class.alarm-banner') private _alarmBannerClass: boolean;
  @ViewChild('sidenav', { static: true }) private _sidenav: MatSidenav;
  @ViewChild('viewTenantCheckbox') private _viewTenantCheckbox: MatCheckbox;

  private _subscription = new Subscription();
  private _alarmsDataSource: AlarmDataSource;
  private _injector: Injector;

  constructor(
    private _highAvailabilityService: HighAvailabilityService,
    private _sidebarService: SidebarService,
    private _alarmService: AlarmService,
    private _userService: UserService,
    private _localStorageService: LocalStorageService,
    private _inj: Injector
  ) { }

  public ngOnInit() {
    this.initWeaverSyncBanner();
    this.initAlarmBanner();
    this.initViewTenantNotifications();
  }

  public initWeaverSyncBanner() {
    const weaverSyncSubscription = this._highAvailabilityService
      .getWeaverSync()
      .pipe(
        pluck<WeaverSync, WeaverSyncStatusState>('status', 'state'),
        map((weaverSyncStatusState) => {
          return weaverSyncStatusState === WeaverSyncStatusState.STANDBY;
        })
      )
      .subscribe(visible => {
        this._weaverSyncStandbyBanner = visible;
      });

    this._subscription.add(weaverSyncSubscription);
  }

  public initAlarmBanner() {
    const alarmBannerSubscription = this._alarmService.banner$.subscribe(visible => {
      this._alarmBannerClass = visible;
    });

    this._subscription.add(alarmBannerSubscription);
  }

  public get isSidebarOpen(): boolean {
    return this._sidebarService.opened;
  }

  private initViewTenantNotifications() {
    this.isSystem$.pipe(
      take(1)
    ).subscribe((isSystem: boolean) => {
      if (isSystem) {
        // by default, for system users, enable viewing all notifications
        if (this._localStorageService.read(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS) === null) {
          this._localStorageService.write(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS, true);
        }
      }
    });
  }

  public onSidebarOpen() {
    if (!this.sidebarContent) {
      /* If no sidebar content provided, then sidebar is used for listing alarms
      TODO(ecarino): refactor so alarms is not a "sidebar special case" */
      this._alarmsDataSource = new AlarmDataSource(this._alarmService);

      if (this._localStorageService.read(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS) === 'true') {
        this._alarmsDataSource.verbose = true;
        this._viewTenantCheckbox.checked = true;
      }
    }

    this._injector = this.createInjector(this._sidebarService.data);
  }

  public onSidebarClose() {
    this._sidebarService.close();

    if (!this.sidebarContent) {
      this._alarmsDataSource = undefined;

      if (this._viewTenantCheckbox) {
        this._viewTenantCheckbox.checked = false;
      }
    }
  }

  public closeSidebar() {
    this._sidebarService.close();
    this._sidenav.close();
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  public onCheckViewTenantNotifications(checked: boolean) {
    this._localStorageService.write(LocalStorageKey.VIEW_TENANT_NOTIFICATIONS, checked);

    if (this._alarmsDataSource?.verbose !== checked) {
      this._alarmsDataSource = undefined;
      window.setTimeout(() => {
        this._alarmsDataSource = new AlarmDataSource(this._alarmService);
        this._alarmsDataSource.verbose = checked;
      });
    } else {
      this._alarmsDataSource.verbose = checked;
    }
  }

  public createInjector(data: unknown) {
    const injector = Injector.create(
      {
        providers: [
          { provide: SidebarData, useValue: data }
        ],
        parent: this._inj
      });

    return injector;
  }

  public get alarmsDataSource(): AlarmDataSource {
    return this._alarmsDataSource;
  }

  public get weaverSyncStandbyBanner(): boolean {
    return this._weaverSyncStandbyBanner;
  }

  public get alarmBanner$(): Observable<boolean> {
    return this._alarmService.banner$;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get isSystem$(): Observable<boolean> {
    return this._userService.isSystem$;
  }

  public get sidebarContent(): ComponentType<unknown> {
    return this._sidebarService.content;
  }

  public sidebarData(): SidebarData {
    return this._sidebarService.data;
  }

  public get sidebarTitle(): string {
    return this._sidebarService.title;
  }

  public get injector(): Injector {
    return this._injector;
  }
}
