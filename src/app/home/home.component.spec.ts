import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, fakeAsync, tick, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { HomeModule } from './home.module';

import { AuthenticationService } from '../auth/authentication.service';

import { HomeComponent } from './home.component';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { By } from '@angular/platform-browser';
import { ToolbarComponent } from '../toolbar/toolbar.component';
import { AlarmBannerComponent } from '../alarm-banner/alarm-banner.component';
import { WeaverSync } from 'rest_client/pangolin/model/weaverSync';
import { WeaverSyncStatusState } from 'rest_client/pangolin/model/weaverSyncStatusState';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { AlarmsService } from 'rest_client/pangolin/api/alarms.service';
import { HighAvailabilityService } from 'rest_client/pangolin/api/highAvailability.service';
import { UserService } from '../users/user.service';
import { environment } from 'src/environments/environment';
import { BASE_PATH as ALARMS_BASE_PATH } from 'rest_client/pangolin/variables';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { AlarmsResponse, AlarmsCount } from '../shared/alarm.service';
import { SidebarService } from '../shared/sidebar.service';
import { User } from 'rest_client/heimdallr/model/user';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatSidenavHarness } from '@angular/material/sidenav/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment, UnitTestElement } from '@angular/cdk/testing/testbed';
import { Component } from '@angular/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { HomeRoutingModule } from './home-routing.module';

@Component({
  selector: 'simple-test-component',
  template: 'Simple Test Component Example'
})
export class SimpleComponent { }

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let httpTestingController: HttpTestingController;
  let authenticationService: AuthenticationService;
  let userService: UserService;
  let sidebarService: SidebarService;
  let loader: HarnessLoader;

  const mockAlert: AlarmsResponse = {
    alerts: [
      {
        timestamp: "1547571234",
        severity: "critical",
        name: "Machine is out of disk space",
        metadata: {},
        identity: {},
        uuid: "dc143517-757c-41f4-95a1-4e85e38ad833",
        _class: "pipeline",
        reason: ""
      }
    ],
    metadata: {
      count: 0,
      startTime: "2019-05-08T18:40:08.831Z",
      endTime: "2019-05-08T18:40:08.831Z"
    }
  }

  const mockUsers: User[] = [
    {
      username: 'SystemAdmin',
      email: 'admin@nefeli.io',
      roles: {
        scope: 'system',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    },
    {
      username: 'TenantAdmin',
      email: 'admin@nefeli.io',
      roles: {
        scope: 'tenant',
        id: 'Nefeli Networks',
        roles: ['admin', 'user']
      }
    }
  ];

  enum UserType {
    SystemAdmin,
    TenantAdmin,
    SystemUser,
    TenantUser
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatSidenavModule,
        ScrollingModule,
        MatCardModule,
        MatCheckboxModule,
        HomeRoutingModule
      ],
      providers: [
        AuthenticationService,
        RequestUrlInterceptor,
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: ALARMS_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        HighAvailabilityService,
        AlarmsService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    authenticationService = TestBed.inject(AuthenticationService);
    sidebarService = TestBed.inject(SidebarService);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render WeaverSync standby banner', () => {
    const weaverSyncRequest = httpTestingController.expectOne(
      `${environment.restServer}/v1/ha/weaversync`
    );

    const mockWeaverSync: WeaverSync = {
      status: {
        state: WeaverSyncStatusState.STANDBY
      }
    };

    weaverSyncRequest.flush(mockWeaverSync);
    fixture.detectChanges();

    expect(component.weaverSyncStandbyBanner).toBe(true, 'set WeaverSync standby banner');

    const weaverSyncStandbyBanner = fixture.debugElement.query(By.css(
      'nef-weaversync-standby-banner'
    ));

    expect(weaverSyncStandbyBanner).not.toBeNull('render WeaverSync standby banner');
  });

  it('should listen to display alarm banner', () => {
    component.alarmBanner$.subscribe((show: boolean) => {
      expect(show).toBe(false);
    });

    component['_alarmService'].setBanner(false);
  });

  it('should properly clean up alarm poller', fakeAsync(() => {
    const toolbarComponent = (fixture.debugElement.query(By.directive(ToolbarComponent))).componentInstance;
    userService.setUser({
      username: 'Andrew',
      email: 'andrew@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin', 'user']
      }
    } as User);

    // simulate user logged in
    authenticationService['_isLoggedIn$'].next(true);
    tick();

    // check alarm poller has started
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=critical`);
    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/alarms/count?severity=error,warning`);

    // flush a critical alarm to display alarm banner
    req.flush({
      "count": 1
    } as AlarmsCount);
    req2.flush({
      "count": 0
    } as AlarmsCount);

    fixture.detectChanges();

    // destroy alarm banner
    const alarmComponent = (fixture.debugElement.query(By.directive(AlarmBannerComponent))).componentInstance;
    alarmComponent.ngOnDestroy();

    // simulate user logged out
    authenticationService['_isLoggedIn$'].next(false);

    // if the alarm poller was still active, this test will fail due to an active periodic timer
  }));

  it('should open/close sidebar', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    // sidenav closed by default
    const sideNav = fixture.debugElement.query(By.css('mat-sidenav'));
    expect(sideNav.componentInstance.opened).toBe(false);

    // open sidenav
    sidebarService.open();
    fixture.detectChanges();
    expect(sideNav.componentInstance.opened).toBe(true);

    // close sidenav by clicking on 'settings' button
    const settingsButton: HTMLAnchorElement = fixture.debugElement.nativeElement.querySelector('mat-sidenav a');
    settingsButton.click();
    fixture.detectChanges();
    expect(sideNav.componentInstance.opened).toBe(false);
  });

  it('should show settings link for System Admin', () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);

    const sideNav = fixture.debugElement.query(By.css('mat-sidenav'));
    // open sidenav
    sidebarService.open();
    fixture.detectChanges();

    const settingsButton: HTMLAnchorElement = fixture.debugElement.nativeElement.querySelector('mat-sidenav a');
    expect(settingsButton).not.toBeNull();
  });

  it('should not show settings link for non System Admin', () => {
    userService.setUser(mockUsers[UserType.TenantAdmin]);

    const sideNav = fixture.debugElement.query(By.css('mat-sidenav'));
    // open sidenav
    sidebarService.open();
    fixture.detectChanges();

    const settingsButton: HTMLAnchorElement = fixture.debugElement.nativeElement.querySelector('mat-sidenav a');
    expect(settingsButton).toBeNull();
  });

  it('should request for alarms on open sidebar', waitForAsync(async () => {
    // tenant admin should not see checkbox
    userService.setUser(mockUsers[UserType.TenantAdmin]);
    sidebarService.open();
    fixture.detectChanges();
    expect(fixture.debugElement.nativeElement.querySelectorAll('mat-checkbox').length).toBe(0);
    sidebarService.close();

    // system admin should see checkbox
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    sidebarService.open();
    fixture.detectChanges();
    const checkBox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    expect(await checkBox.getLabelText()).toBe('View tenant notifications');
    expect(await checkBox.isChecked()).toBe(false);

    // when checked, retrieve alarms with 'verbose = true'
    await checkBox.check();
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      httpTestingController.match((req: HttpRequest<any>) => {
        return (req.url === 'http://localhost:3030/v1/alarms')
          && (req.params.get('verbose') == 'true');
      });
    });
  }));

  it('should preserve view tenant notifications setting', waitForAsync(async () => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    sidebarService.open();
    fixture.detectChanges();
    let checkBox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    expect(await checkBox.getLabelText()).toBe('View tenant notifications');
    expect(await checkBox.isChecked()).toBe(false);
    await checkBox.check();

    // close & re-open side bar
    sidebarService.close();
    sidebarService.open();

    // verify checkbox is still checked
    checkBox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness);
    expect(await checkBox.isChecked()).toBe(true);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      httpTestingController.match((req: HttpRequest<any>) => {
        return (req.url === 'http://localhost:3030/v1/alarms')
          && (req.params.get('verbose') == 'true');
      });
    });
  }));

  it('should display custom content in sidebar', async () => {
    const title = 'My Mock Title';
    sidebarService.open(title, SimpleComponent);
    fixture.detectChanges();

    const sideNav = await loader.getAllHarnesses<MatSidenavHarness>(MatSidenavHarness);
    const host = await sideNav[0].host() as UnitTestElement;

    expect(host.element.querySelector('h4').textContent).toBe(title);
    expect(host.element.querySelector('simple-test-component').textContent).toBe('Simple Test Component Example');
  });
});
