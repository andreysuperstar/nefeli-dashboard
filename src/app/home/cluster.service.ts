import { Injectable } from '@angular/core';

import { Observable, timer, of, concat, defer } from 'rxjs';
import { exhaustMap, mergeMap, map, pluck, shareReplay, skip, repeat, mapTo } from 'rxjs/operators';

import { orderBy } from 'lodash-es';

import { SiteConfig } from 'rest_client/pangolin/model/siteConfig';
import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { StatFilter } from 'rest_client/pangolin/model/statFilter';
import { LoggingConfig } from 'rest_client/pangolin/model/loggingConfig';
import { SitesService } from 'rest_client/pangolin/api/sites.service';
import { LogsService } from 'rest_client/pangolin/api/logs.service';
import { Stats } from 'rest_client/pangolin/model/stats';
import { StatsService as RestDashboardStatsService } from 'rest_client/pangolin/api/stats.service';
import { SortDirection } from '@angular/material/sort';

export interface SiteResponse {
  result: {
    common: {
      sshUsername?: string;
      sshKeyPath?: string;
      sshAddress?: string;
      url: string;
      vniRangeStart: number;
      vniRangeEnd: number;
    };
    ip: string;
    mac: string;
  };
  status?: string;
}

export interface SiteError {
  httpStatus: number;
  message: string;
  errorCode: number;
}

export enum SiteErrorCodes {
  NotFound = 1200,
  Unspecified,
  Miscellaneous,
  HasRunningServices
}

export interface Cluster {
  id: string;
  name: string;
}

export interface PipelineStat {
  id: string;
  name: string;
  site: string;
  stats: Array<any>;
}

export enum LogLevel {
  Debug,
  Info,
  Warning,
  Error,
  Critical
}

export interface ElasticConfig {
  host: string;
  username: string;
  password: string;
}

export interface SitesConfiguration {
  isLogging: boolean;
  logLevel: LogLevel;
  elastic?: ElasticConfig;
}

enum SortableColumn {
  name = 'name'
}

const Config = {
  POLL_INTERVAL: 5000
};

@Injectable({
  providedIn: 'root'
})
export class ClusterService {

  private _streamSitesObservable: Observable<Sites>;

  constructor(
    private _sitesService: SitesService,
    private _logsService: LogsService,
    private _restDashboardStatsService: RestDashboardStatsService
  ) {
    this._streamSitesObservable = timer(0, Config.POLL_INTERVAL)
      .pipe(
        exhaustMap((): Observable<Sites> => this.getSites()),
        shareReplay<Sites>({
          refCount: true
        })
      );
  }

  public getSiteIDs(): Observable<string[]> {
    return this._sitesService
      .getSites()
      .pipe(
        map(({ sites }) => sites.map(site => site.config.identifier))
      );
  }

  public getSite(id: string): Observable<Site> {
    return this._sitesService.getSite(id);
  }

  public getSites(
    status?: boolean,
    search?: string[],
    filter?: string[],
    sortColumn?: string,
    sortDirection?: SortDirection,
    index?: number,
    pageSize?: number
  ): Observable<Sites> {
    const sortField: SortableColumn = SortableColumn[sortColumn];

    const sort = sortField
      ? [`${sortDirection}(${sortField})`]
      : undefined;

    return this._sitesService.getSites(status, search, filter, sort, index, pageSize);
  }

  public postSite(entry: SiteConfig): Observable<SiteConfig> {
    return this._sitesService.postSite(entry);
  }

  public putSite(id: string, entry: SiteConfig): Observable<any> {
    return this._sitesService.putSite(id, entry);
  }

  public deleteSite(id: string): Observable<any> {
    return this._sitesService.deleteSite(id);
  }

  // TODO(eric): remove getClusters(), utilize getSites() instead
  public getClusters(): Observable<Cluster[]> {
    return this.getSites().pipe(
      map(({ sites: sites }: Sites): Cluster[] => {
        if (!sites) {
          return [];
        } else {
          return sites.map<Cluster>(({
            config: {
              identifier,
              name
            }
          }) => ({
            id: identifier,
            name
          }));
        }
      }),
      map((clusters: Cluster[]): Cluster[] => orderBy(clusters, 'name', 'asc'))
    );
  }

  public streamSites(): Observable<Sites> {
    return this._streamSitesObservable;
  }

  public postClusterStats(
    id: string,
    filter?: Array<StatFilter>,
    duration?: number,
    step?: number,
    services?: string[],
    start?: number
  ): Observable<Stats> {
    return this._restDashboardStatsService.postClusterStats(
      id,
      {
        services,
        filter: {
          filter: filter,
        },
        range: {
          start: start,
          duration: duration,
          step: step
        }
      }
    );
  }

  public streamClusterStats(
    id: string,
    delay: number,
    filter?: StatFilter,
    duration?: number,
    step?: number,
    services?: string[]
  ): Observable<Stats> {
    const msInSec = 1000;
    let lastFetchTime: number;

    return concat(
      defer(() => {
        if (!duration) {
          return of(duration);
        }

        const now: number = new Date().getTime();
        const newDuration = lastFetchTime ?
          Math.round((now - lastFetchTime) / msInSec) :
          duration;
        lastFetchTime = now;

        return of(newDuration);
      }).pipe(
        mergeMap((_duration: number) =>
          this.postClusterStats(id, [filter] as Array<StatFilter>, _duration, step, services)
        )
      ),
      timer(delay).pipe(
        mapTo(undefined),
        skip(1)
      )
    ).pipe(
      repeat(),
      shareReplay({
        refCount: true
      })
    );
  }

  public getLoggingConfig(): Observable<LoggingConfig> {
    return this._logsService.getLogging();
  }

  public putLoggingConfig(config: LoggingConfig): Observable<any> {
    return this._logsService.putLogging('', config);
  }
}
