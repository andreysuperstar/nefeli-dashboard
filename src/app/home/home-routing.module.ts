import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageDataKey } from '../toolbar/toolbar.component';

import { TenantAdminGuard } from '../auth/tenant-admin.guard';
import { TenantUserGuard } from '../auth/tenant-user.guard';

import { HomeComponent } from './home.component';
import { OverviewComponent } from '../overview/overview.component';

export const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        redirectTo: 'overview',
        pathMatch: 'full'
      },
      {
        path: 'overview',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        component: OverviewComponent
      },
      {
        path: 'tenants',
        loadChildren: () => import('../tenant/tenant.module').then(m => m.TenantModule),
        data: {
          [PageDataKey.ServicesPage]: true
        }
      },
      {
        path: 'templates',
        loadChildren: () => import('../templates/templates.module').then(m => m.TemplatesModule),
        data: {
          [PageDataKey.ServicesPage]: true
        }
      },
      {
        path: 'sites',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../cluster/cluster.module').then(m => m.ClusterModule)
      },
      {
        path: 'nfs',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../network-functions/network-functions.module').then(m => m.NetworkFunctionsModule),
        data: {
          [PageDataKey.NetworkFunctionsPage]: true
        }
      },
      {
        path: 'licenses',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../licenses/licenses.module').then(m => m.LicensesModule),
        data: {
          [PageDataKey.NetworkFunctionsPage]: true
        }
      },
      {
        path: 'tunnels',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../tunnels/tunnels.module').then(m => m.TunnelsModule),
        data: {
          [PageDataKey.ConnectivityPage]: true
        }
      },
      {
        path: 'attachments',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../attachments/attachments.module').then(m => m.AttachmentsModule),
        data: {
          [PageDataKey.ConnectivityPage]: true
        }
      },
      {
        path: 'users',
        canActivate: [TenantUserGuard],
        loadChildren: () => import('../users/users.module').then(m => m.UsersModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      },
      {
        path: 'tenants-list',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../tenants/tenants.module').then(m => m.TenantsModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      },
      {
        path: 'file-manager',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../file-manager/file-manager.module').then(m => m.FileManagerModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      },
      {
        path: 'alerts',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../notifications/notifications.module').then(m => m.NotificationsModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      },
      {
        path: 'logging',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../logging/logging.module').then(m => m.LoggingModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      },
      {
        path: 'provisioning',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../provisioning/provisioning.module').then(m => m.ProvisioningModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      },
      {
        path: 'system-configuration',
        canActivate: [
          TenantAdminGuard,
          TenantUserGuard
        ],
        loadChildren: () => import('../system-configuration/system-configuration.module').then(m => m.SystemConfigurationModule),
        data: {
          [PageDataKey.SettingsPage]: true
        }
      }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
