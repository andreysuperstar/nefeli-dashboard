import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatSidenavModule } from '@angular/material/sidenav';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { HomeRoutingModule } from './home-routing.module';
import { OverviewModule } from '../overview/overview.module';
import { SharedModule } from '../shared/shared.module';
import { HomeComponent } from './home.component';
import { AlarmBannerComponent } from '../alarm-banner/alarm-banner.component';
import { WeaverSyncStandbyBannerComponent } from '../weaversync-standby-banner/weaversync-standby-banner.component';
import { SitesOverviewModule } from '../sites-overview/sites-overview.module';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    FlexLayoutModule,
    HomeRoutingModule,
    OverviewModule,
    SharedModule,
    MatSidenavModule,
    ScrollingModule,
    MatCardModule,
    SitesOverviewModule,
    MatCheckboxModule,
    MatDividerModule
  ],
  declarations: [
    HomeComponent,
    AlarmBannerComponent,
    WeaverSyncStandbyBannerComponent
  ]
})
export class HomeModule { }
