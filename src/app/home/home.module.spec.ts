import { HomeRoutingModule } from './home-routing.module';
import { HomeModule } from './home.module';

describe('HomeModule', () => {
  let homeModule: HomeModule;
  let homeRoutingModule: HomeRoutingModule;

  beforeEach(() => {
    homeModule = new HomeModule();
    homeRoutingModule = new HomeRoutingModule();
  });

  it('should create an instance', () => {
    expect(homeModule).toBeTruthy();
  });

  it('should create instance of routing module', () => {
    expect(homeRoutingModule).toBeTruthy();
  });
});
