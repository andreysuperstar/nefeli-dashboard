import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { SummaryModule } from '../summary/summary.module';
import { PerformanceModule } from '../performance/performance.module';
import { SharedModule } from '../shared/shared.module';

import { SitesOverviewComponent } from './sites-overview.component';
import { SiteUsageComponent } from './site-usage/site-usage.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatTabsModule,
    MatListModule,
    MatMenuModule,
    MatCardModule,
    MatButtonModule,
    RouterModule,
    SummaryModule,
    PerformanceModule,
    SharedModule
  ],
  declarations: [
    SitesOverviewComponent,
    SiteUsageComponent
  ],
  exports: [
    SiteUsageComponent
  ]
})
export class SitesOverviewModule { }
