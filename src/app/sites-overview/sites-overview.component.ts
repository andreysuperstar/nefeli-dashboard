import { HttpError } from '../error-codes';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSelectionListChange, MatListOption } from '@angular/material/list';

import { Observable, EMPTY, combineLatest, Subscription } from 'rxjs';
import { catchError, mergeMap, map } from 'rxjs/operators';

import { SERVICE_TOTAL, Pipeline } from '../pipeline/pipeline.model';
import { Range } from '../shared/date-range/date-range.component';

import { ClusterService, Cluster } from '../home/cluster.service';
import { LoggerService } from '../shared/logger.service';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';

import { PERFORMANCE_CHART } from '../performance/performance.component';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';
import { PipelineService, ClusterPipelinesResponse, ClusterPipeline } from '../pipeline/pipeline.service';
import { UserService } from '../users/user.service';
import { Site } from 'rest_client/pangolin/model/site';
import { ZoomEvent } from '../shared/chart-line/chart-line.component';

const SecToMsecFactor = 1000;

@Component({
  templateUrl: './sites-overview.component.html',
  styleUrls: ['./sites-overview.component.less']
})
export class SitesOverviewComponent implements OnInit, OnDestroy {
  private _cluster: Cluster;
  private _chartRange: Range;
  private _enableFilterMenu = false;
  private _pipelines: Pipeline[] = [];
  private _selectedPipelines: Pipeline[] = [SERVICE_TOTAL];
  private _chartStartTime: number;
  private _chartDuration: number;
  private _subscription = new Subscription();

  constructor(
    private _clusterService: ClusterService,
    private _pipelineService: PipelineService,
    private _log: LoggerService,
    private _store: LocalStorageService,
    private _router: Router,
    private _route: ActivatedRoute,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    const subscription = combineLatest([
      this._route.parent.data,
      this._route.parent.params
    ]).pipe(
      mergeMap<[Data, Params], Observable<[Data, Params, Site]>>(([data, params]) => {
        return this._clusterService
          .getSite(params.id)
          .pipe(
            map(site => [data, params, site])
          );
      }),
      catchError((error: HttpErrorResponse) => {
        switch (error.status) {
          case HttpError.Unauthorized:
          case HttpError.Forbidden:
            this._router.navigate(['/auth/login']);
            break;
        }

        return EMPTY;
      })
    ).subscribe(([data, params, site]) => {
      this._cluster = {
        id: site.config.identifier,
        name: site.config.name
      } as Cluster;
      this.updatePipelines(this._cluster);
    });

    this.setChartRange();
    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private setChartRange(): void {
    const data: string | null = this._store.read(LocalStorageKey.PERFORMANCE_CHART_DATE_RANGE);

    if (data) {
      try {
        this._chartRange = JSON.parse(data);
      } catch (e) { }
    }
  }

  public onDateSelect({ start, duration, period }: Range): void {
    this._chartStartTime = period ? undefined : start;
    this._chartDuration = duration;

    this._store.write(
      LocalStorageKey.PERFORMANCE_CHART_DATE_RANGE,
      JSON.stringify({ start, duration, period })
    );
  }

  private updatePipelines(cluster: Cluster) {
    this._pipelineService
      .getClusterPipelines(cluster.id)
      .subscribe(
        (serviceResponse: ClusterPipelinesResponse) => {
          if (serviceResponse.services) {
            const services: Pipeline[] = serviceResponse.services.map(
              ({ expService, weaverService }: ClusterPipeline): Pipeline => ({
                identifier: expService,
                name: weaverService,
                status: 'success'
              }) as Pipeline
            );

            this._pipelines = [SERVICE_TOTAL, ...services];
            this._selectedPipelines = [SERVICE_TOTAL];
          }
        },
        (error: Error) => {
          this._log.error(error);
        }
      );
  }

  public selectionChange(ev: MatSelectionListChange) {
    this._selectedPipelines = ev.source.selectedOptions.selected.map(
      (option: MatListOption): Pipeline => option.value
    );
  }

  public performanceChartChange(index: PERFORMANCE_CHART) {
    this._enableFilterMenu = (index !== PERFORMANCE_CHART.THROUGHPUT);
  }

  public onZoomEvent(event: ZoomEvent) {
    const range: Range = {
      start: event.start.unix(),
      duration: event.end.diff(event.start) / SecToMsecFactor,
      period: false
    };
    this._chartRange = range;
    this.onDateSelect(range);
  }

  public get chartRange(): Range {
    return this._chartRange;
  }

  public get enableFilterMenu(): boolean {
    return this._enableFilterMenu;
  }

  public get pipelines(): Pipeline[] {
    return this._pipelines;
  }

  public get selectedPipelines(): Pipeline[] {
    return this._selectedPipelines;
  }

  public get chartStartTime(): number {
    return this._chartStartTime;
  }

  public get chartDuration(): number {
    return this._chartDuration;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get cluster(): Cluster {
    return this._cluster;
  }
}
