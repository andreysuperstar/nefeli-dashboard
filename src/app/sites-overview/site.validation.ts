import { ValidatorFn, FormGroup, AbstractControl, ValidationErrors } from '@angular/forms';

const HOST = '([a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*)(:\\d{1,5})?$';

export const HOST_PATTERN: RegExp = new RegExp('^' + HOST);

export const URL_PATTERN: RegExp = new RegExp('^(http:\\/\\/www\\.|https:\\/\\/www\\.|http:\\/\\/|https:\\/\\/)' + HOST);

export const IP_PATTERN: RegExp =
  /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/;

export const MAC_PATTERN: RegExp = /^([0-9A-F]{2}[.:-\s]){5}([0-9A-F]{2})$|^([0-9A-F]{12})$|^([0-9A-F]{4}[.:\s]){2}([0-9A-F]{4})$/i;

export const HTTPS_PATTERN: RegExp = /^https:\/\//;

export const CIDR_PATTERN: RegExp = /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/;

export const PCI_PATTERN: RegExp = /^(([0-9A-Fa-f]{4}:)?[0-9A-Fa-f]{2}:[0-9A-Fa-f]{2}\.[0-9A-Fa-f])?$/;

export const vniRangeValidator: ValidatorFn = (group: FormGroup): ValidationErrors => {
  const controls: AbstractControl[] = Object.values(group.controls);
  const start: number = Number(group.get('start').value);
  const end: number = Number(group.get('end').value);

  const hasInvalidControl: boolean = controls.some((control: AbstractControl): boolean =>
    control.invalid && !control.hasError('vniRange')
  );

  const isGroupValid: boolean = hasInvalidControl || start < end;

  const errors: ValidationErrors = {
    vniRange: true
  };

  controls.forEach((control: AbstractControl) => isGroupValid
    ? control.updateValueAndValidity({ onlySelf: true })
    : control.setErrors(errors)
  );

  return isGroupValid ? undefined : errors;
};
