import * as moment from 'moment';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { SitesOverviewComponent } from './sites-overview.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute } from '@angular/router';
import { OverlayModule } from '@angular/cdk/overlay';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';
import { HttpError } from '../error-codes';
import { By } from '@angular/platform-browser';

import { of } from 'rxjs';
import { ClusterService } from 'src/app/home/cluster.service';

import { Router } from '@angular/router';
import { Site } from 'rest_client/pangolin/model/site';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { ClusterPipelinesResponse } from '../pipeline/pipeline.service';
import { User } from 'rest_client/heimdallr/model/user';
import { PERFORMANCE_CHART } from '../performance/performance.component';
import { Range } from '../shared/date-range/date-range.component';
import { LocalStorageService, LocalStorageKey } from '../shared/local-storage.service';
import { SummaryComponent } from '../summary/summary.component';
import { ByteConverterPipe } from '../pipes/byte-converter.pipe';
import { PipelineNamePipe } from '../pipes/pipeline-name.pipe';
import { DurationPipe } from '../pipes/duration.pipe';
import { ZoomEvent } from '../shared/chart-line/chart-line.component';
import { NetworkFunctionComponent } from '../pipeline/network-function.component';
import { SummaryItemComponent } from '../summary/summary-item.component';
import { PerformanceComponent } from '../performance/performance.component';
import { ChartPacketlossComponent } from '../charts/chart-packetloss.component';
import { ChartLatencyComponent } from '../charts/chart-latency.component';
import { ChartHeaderComponent } from '../shared/chart-header/chart-header.component';
import { LegendComponent } from '../shared/legend/legend.component';
import { ChartLineComponent } from '../shared/chart-line/chart-line.component';
import { ChartThroughputComponent } from '../charts/chart-throughput.component';
import { MenuComponent } from '../shared/menu/menu.component';
import { DateRangeComponent } from '../shared/date-range/date-range.component';
import { SiteUsageComponent } from './site-usage/site-usage.component';

const CURRENT_SITE_ID = '-1';

let mockActivatedRoute = {
  parent: {
    params: of({ id: CURRENT_SITE_ID }),
    data: of({})
  },
  snapshot: {
    url: [
      {
        path: '/sites/-1'
      }
    ]
  }
};

const mockSite: Site = {
  config: {
    identifier: '-1',
    name: 'Mock_site',
    mgmtUrl: 'https://192.168.0.144:7300',
    siteType: 'CLUSTER',
    siteStatus: 'ACTIVE',
    dataplaneVlanTag: 1,
    vniRangeStart: 7191,
    vniRangeEnd: 8191,
    publicIp: '10.50.0.1',
    privateIp: '10.50.0.1',
    gatewayMac: '0a:00:00:00:00:01',
    inboundVxlanPort: 13333
  }
};

describe('SitesOverviewComponent', () => {
  let component: SitesOverviewComponent;
  let fixture: ComponentFixture<SitesOverviewComponent>;
  let httpTestingController: HttpTestingController;

  let router = {
    navigate: jasmine.createSpy('navigate'),
    routerState: {}
  };
  let el: HTMLElement;

  const mockUsers: User[] = [
    {
      username: 'Eric',
      email: 'eric@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['admin']
      }
    },
    {
      username: 'Eric',
      email: 'eric@nefeli.io',
      roles: {
        scope: 'system',
        id: '-1',
        roles: ['user']
      }
    },
  ]

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        NgbDatepickerModule,
        MatSnackBarModule,
        MatCardModule,
        OverlayModule,
        MatTabsModule,
        MatSelectModule,
        MatListModule
      ],
      declarations: [
        SitesOverviewComponent,
        SummaryComponent,
        SummaryItemComponent,
        NetworkFunctionComponent,
        PerformanceComponent,
        ByteConverterPipe,
        PipelineNamePipe,
        DurationPipe,
        ChartPacketlossComponent,
        ChartLatencyComponent,
        ChartHeaderComponent,
        LegendComponent,
        ChartLineComponent,
        ChartThroughputComponent,
        MenuComponent,
        DateRangeComponent,
        SiteUsageComponent
      ],
      providers: [
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: Router,
          useValue: router
        },
        {
          provide: ActivatedRoute,
          useValue: mockActivatedRoute
        },
        ClusterService,
        ByteConverterPipe,
        PipelineNamePipe,
        DurationPipe
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SitesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable filter menu when throughput chart selected', () => {
    // filter menu disabled by default
    expect(component.enableFilterMenu).toBe(false);
    component.performanceChartChange(PERFORMANCE_CHART.LATENCY);
    expect(component.enableFilterMenu).toBe(true);
    component.performanceChartChange(PERFORMANCE_CHART.PACKET_LOSS);
    expect(component.enableFilterMenu).toBe(true);
    component.performanceChartChange(PERFORMANCE_CHART.THROUGHPUT);
    expect(component.enableFilterMenu).toBe(false);
  });

  it('should get site data', fakeAsync(() => {
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1`);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush(mockSite);
  }));

  it('should get site pipelines', fakeAsync(() => {
    const req1 = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1`);
    req1.flush(mockSite);
    tick();
    const req2 = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/services`);
    req2.flush({
      services: [{
        weaverService: 'MockPipeline',
        expService: '_39212182969902458441195932660343882095079MockPipeline',
        weaverTenant: '212182969902458441195932660343882095079',
        expTenant: '174766142345748448428529545112578385371'
      }]
    } as ClusterPipelinesResponse);

    httpTestingController.verify();

    expect(component.pipelines.length).toBe(2);
    expect(component.pipelines[0].name).toBe('Aggregate');
    expect(component.pipelines[1].name).toBe('MockPipeline');
  }));

  it('should go to login on 401 error', () => {
    fixture.detectChanges();
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1`);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    req.flush('', {
      status: HttpError.Unauthorized,
      statusText: 'Unauthorized'
    });

    expect(router.navigate).toHaveBeenCalledWith(['/auth/login']);
  });

  it('should update the date range', () => {
    fixture.detectChanges();
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1`);
    req.flush(mockSite);
    fixture.detectChanges();

    const dateRange = fixture.debugElement.query(By.css('nef-date-range'));
    const testRange: Range = { "start": 1581005463, "duration": 21600, "period": false };
    dateRange.triggerEventHandler('dateSelect', testRange);

    expect(component.chartStartTime).toBe(testRange.start);
    expect(component.chartDuration).toBe(testRange.duration);

    const localStorageService: LocalStorageService = TestBed.inject(LocalStorageService);
    const storedRange = localStorageService.read(LocalStorageKey.PERFORMANCE_CHART_DATE_RANGE);
    expect(storedRange).toBe(JSON.stringify(testRange));
  });

  it('should handle zoom event', () => {
    fixture.detectChanges();

    spyOn(component, 'onDateSelect').and.callThrough();
    const startTime = moment();
    const endTime = moment();
    const event: ZoomEvent = {
      start: startTime,
      end: endTime
    };
    component.onZoomEvent(event);
    expect(component['_chartRange'].start).toBe(startTime.unix());
    expect(component.onDateSelect).toHaveBeenCalled();
  });
});
