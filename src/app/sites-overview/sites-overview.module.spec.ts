import { SitesOverviewModule } from './sites-overview.module';

describe('SitesOverviewModule', () => {
  let sitesOverviewModule: SitesOverviewModule;

  beforeEach(() => {
    sitesOverviewModule = new SitesOverviewModule();
  });

  it('should create an instance', () => {
    expect(sitesOverviewModule).toBeTruthy();
  });
});
