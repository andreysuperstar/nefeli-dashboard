import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { environment } from '../../../environments/environment';
import { BASE_PATH } from 'rest_client/pangolin/variables';

import { Cluster } from '../../home/cluster.service';

import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';

import { ServersService, ServerNF, Socket } from '../../cluster/servers.service';

import { SiteUsageComponent } from './site-usage.component';
import { LegendComponent } from '../../shared/legend/legend.component';
import { CellGridComponent } from '../../shared/cell-grid/cell-grid.component';
import { OverlayModule } from '@angular/cdk/overlay';
import { AlertService } from '../../shared/alert.service';
import { RESTServerList } from 'rest_client/pangolin/model/rESTServerList';

describe('ClusterUsageComponent', () => {
  let component: SiteUsageComponent;
  let fixture: ComponentFixture<SiteUsageComponent>;
  let httpTestingController: HttpTestingController;

  let clusterUsageDe: DebugElement;
  let clusterUsageEl: HTMLElement;

  const mockCluster: Cluster = {
    id: '-1',
    name: 'cluster1'
  }

  const nfs: ServerNF[] = [{
    metadata: {
      name: 'filter_0',
      logicalNf: 'filter',
      service: 'sample_1',
      nfType: '',
      tenant: 'Tenant 1',
      draining: false,
      machine: 'sylvester',
    },
    resources: {
      coresBySocket: {
        0: {
          cores: [1, 2, 3]
        }
      },
      hugepagesBySocket: {
        0: {
          hugepages: {
            1073741824: 4
          }
        }
      },
      cpuOversubscriptionFactor: "0"
    }
  }, {
    metadata: {
      name: 'nat_0',
      logicalNf: 'nat',
      service: 'sample_2',
      nfType: '',
      tenant: 'Tenant 1',
      draining: false,
      machine: 'sylvester'
    },
    resources: {
      coresBySocket: {
        0: {
          cores: [5, 6]
        }
      },
      hugepagesBySocket: {
        0: {
          hugepages: {
            1073741824: 4
          }
        }
      },
      cpuOversubscriptionFactor: "1"
    }
  }];

  const fetchServers = () => {
    const request = httpTestingController.expectOne(`${environment.restServer}/v1/sites/-1/servers`);
    const flushData: RESTServerList = {
      servers: [
        {
          identifier: "sylvester",
          name: "sylvester",
          sockets: [
            {
              id: 0,
              cores: [
                0,
                1,
                2,
                3,
                4,
                5,
                6,
                7
              ],
              memory: '0',
              hugepages: [],
              cpuOversubscriptionFactor: "2"
            },
            {
              id: 1,
              cores: [
                8,
                9,
                10,
                11,
                12,
                13,
                14,
                15
              ],
              memory: '0',
              hugepages: [],
              cpuOversubscriptionFactor: "1"
            }
          ] as Socket[],
          nfs: {
            nfInstances: nfs
          },
          control: {
            numaResources: {
              0: {
                cores: [
                  8, 9
                ],
                hugepages: {
                  "1073741824": 1
                }
              }
            }
          },
          bess: {
            numaResources: {
              0: {
                cores: [
                  10, 11
                ],
                hugepages: {}
              }
            }
          }
        }
      ]
    };
    request.flush(flushData);
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MatCardModule,
        OverlayModule,
        MatSnackBarModule
      ],
      declarations: [
        SiteUsageComponent,
        LegendComponent,
        CellGridComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        AlertService,
        ServersService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiteUsageComponent);
    component = fixture.componentInstance;

    clusterUsageDe = fixture.debugElement;
    clusterUsageEl = clusterUsageDe.nativeElement;

    component.cluster = mockCluster;

    fixture.detectChanges();

    fetchServers();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render legend', () => {
    const legendEl: HTMLUnknownElement = clusterUsageEl.querySelector('nef-legend');
    const legendItemEls: NodeListOf<HTMLSpanElement> = legendEl.querySelectorAll('span');

    expect(legendEl).toBeTruthy();
    expect(legendEl.classList).toContain('core-legend', 'legend element class is \'core-legend\'');

    expect(legendItemEls[0].classList).toContain('grey', '\'Available\' legend item contains \'grey\' class');
    expect(legendItemEls[1].classList).toContain('blue', '\'Active\' legend item contains \'blue\' class');

    expect(legendItemEls[0].textContent).toBe('44% Available', 'valid \'Available\' cores rate percentage and label');
    expect(legendItemEls[1].textContent).toBe('56% Active', 'valid \'Active\' cores rate percentage and label');
  });

  it('should render servers, sockets, socket cores', () => {
    expect(clusterUsageEl.querySelectorAll('mat-card').length).toBe(1, '1 rendered server');

    const cellGridEls: NodeListOf<HTMLUnknownElement> = clusterUsageEl.querySelectorAll('nef-cell-grid');

    expect(cellGridEls.length).toBe(2, '2 rendered sockets');

    expect(cellGridEls[0].querySelectorAll('.cell').length).toBe(8, '8 renderer cores in 1st socket');
    expect(cellGridEls[1].querySelectorAll('.cell').length).toBe(8, '8 renderer cores in 2nd socket');
  });

  it('should render active cores', () => {
    const cells = clusterUsageEl.querySelectorAll('.container .cell');

    expect(cells[0].classList).not.toContain('blue');
    expect(cells[1].classList).toContain('blue');
    expect(cells[2].classList).toContain('blue');
    expect(cells[3].classList).toContain('blue');
    expect(cells[4].classList).not.toContain('blue');
    expect(cells[5].classList).toContain('blue');
    expect(cells[6].classList).toContain('blue');
    expect(cells[7].classList).not.toContain('blue');
    expect(cells[8].classList).toContain('blue');
    expect(cells[9].classList).toContain('blue');
    expect(cells[10].classList).toContain('blue');
    expect(cells[11].classList).toContain('blue');
    expect(cells[12].classList).not.toContain('blue');
    expect(cells[13].classList).not.toContain('blue');
    expect(cells[14].classList).not.toContain('blue');
    expect(cells[15].classList).not.toContain('blue');
  });
});
