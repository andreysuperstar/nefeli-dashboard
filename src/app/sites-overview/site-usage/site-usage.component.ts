import { Component, OnInit, Input, OnDestroy } from '@angular/core';

import { Subscription, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { LegendItem } from 'src/app/shared/legend/legend.component';
import { Cluster } from '../../home/cluster.service';

import { Server, Socket, ServerState, ServersService } from '../../cluster/servers.service';

import { CellGroup } from '../../shared/cell-grid/cell-grid.component';

const Config = {
  cellColors: 'soft-blue',
  POLL_INTERVAL: 2000
};

@Component({
  selector: 'nef-site-usage',
  templateUrl: './site-usage.component.html',
  styleUrls: ['./site-usage.component.less']
})
export class SiteUsageComponent implements OnInit, OnDestroy {

  private _totalCores = 0;
  private _activeCores = 0;
  private _coreLegendItems: LegendItem[];
  private _cluster: Cluster;
  private _servers: Server[];
  private _subscription = new Subscription();

  @Input() public set cluster(cluster: Cluster) {
    this._cluster = cluster;
  }

  constructor(
    private _serversService: ServersService
  ) { }

  public ngOnInit() {
    this.getServers();
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private getServers() {
    const subscription = this._serversService.streamServers(this._clusterId).pipe(
      catchError(() => {
        setTimeout(() => {
          this.getServers();
        }, Config.POLL_INTERVAL);
        return of([]);
      })
    ).subscribe((servers: Server[]) => {
      this._servers = servers;
      this.setActiveCores();
    });
    this._subscription.add(subscription);
  }

  private setActiveCores() {
    this._activeCores = 0;
    this._totalCores = 0;

    this._servers.forEach((server: Server) => {
      const {
        cores: {
          indices: serverCores,
          active: serverActiveCores
        }
      }: ServerState = this._serversService.getServerState(server);

      this._activeCores += serverActiveCores.size;
      this._totalCores += serverCores.length;

      if (server.sockets) {
        server.sockets.forEach(socket => {
          socket.activeCores = [];

          serverActiveCores.forEach((core: number) => {
            if (socket.cores.includes(core)) {
              socket.activeCores.push(core);
            }
          });
        });
      }
    });

    this.setCoreLegendItems();
  }

  private setCoreLegendItems() {
    const activeCoresRate: number = this._totalCores ? Math.round(this._activeCores * 100 / this._totalCores) : 0;

    const availableCoresRate: number = this._totalCores ? 100 - activeCoresRate : 0;

    this._coreLegendItems = [
      {
        label: `${availableCoresRate}% Available`,
        color: 'grey'
      },
      {
        label: `${activeCoresRate}% Active`,
        color: 'blue'
      }
    ];
  }

  public getActiveCores(socket: Socket): CellGroup[] {
    return [{ indices: socket.activeCores.map(c => socket.cores.indexOf(c)), color: Config.cellColors }];
  }

  public get coreLegendItems(): LegendItem[] {
    return this._coreLegendItems;
  }

  public get cluster(): Cluster {
    return this._cluster;
  }

  private get _clusterId(): string {
    return this._cluster ? this._cluster.id : undefined;
  }

  public get servers(): Server[] {
    return this._servers;
  }

}
