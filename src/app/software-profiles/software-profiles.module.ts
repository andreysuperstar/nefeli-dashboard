import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';

import { SharedModule } from '../shared/shared.module';

import { SoftwareProfilesComponent } from './software-profiles.component';
import { SoftwareProfileFormComponent } from './software-profile-form/software-profile-form.component';
import { SoftwareProfileDialogComponent } from './software-profile-dialog/software-profile-dialog.component';
import { SoftwareProfileManagerDialogComponent } from './software-profile-manager-dialog/software-profile-manager-dialog.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

@NgModule({
  declarations: [
    SoftwareProfilesComponent,
    SoftwareProfileFormComponent,
    SoftwareProfileDialogComponent,
    SoftwareProfileManagerDialogComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatSelectModule,
    MatRadioModule,
    MatButtonModule,
    SharedModule,
    MatProgressSpinnerModule
  ],
  exports: [
    SoftwareProfilesComponent,
    SoftwareProfileManagerDialogComponent
  ]
})
export class SoftwareProfilesModule { }
