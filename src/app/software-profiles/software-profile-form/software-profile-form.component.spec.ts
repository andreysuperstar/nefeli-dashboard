import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HTTP_INTERCEPTORS, HttpHeaders } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormControl, ReactiveFormsModule, ValidationErrors } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDatepickerInputHarness } from '@angular/material/datepicker/testing';
import { MatSelectModule } from '@angular/material/select';
import { MatSelectHarness } from '@angular/material/select/testing';
import { MatRadioModule } from '@angular/material/radio';
import { MatRadioGroupHarness } from '@angular/material/radio/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { Observable, of, throwError } from 'rxjs';

import { MnemeFileInfos } from 'rest_client/mneme/model/mnemeFileInfos';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { SoftwareProfileError } from 'rest_client/pangolin/model/softwareProfileError';

import { BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as MNEME_BASE_PATH } from 'rest_client/mneme/variables';
import files from '../../../../mock/files';
import { environment } from 'src/environments/environment';
import { HttpError } from 'src/app/error-codes';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { FilesService } from 'rest_client/mneme/api/files.service';

import {
  DistributionType,
  distributionValidator,
  HOURS,
  MINUTES,
  SoftwareProfileFormComponent,
  TimePeriod,
  timeValidator
} from './software-profile-form.component';

describe('SoftwareProfileFormComponent', () => {
  let component: SoftwareProfileFormComponent;
  let fixture: ComponentFixture<SoftwareProfileFormComponent>;
  let loader: HarnessLoader;
  let httpTestingController: HttpTestingController;
  let alertService: AlertService;
  let fileService: FilesService;

  const mockFiles: MnemeFileInfos = { files: files.files.filter(file => file.isDistroDeb) };

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          HttpClientTestingModule,
          ReactiveFormsModule,
          FlexLayoutModule,
          MatSnackBarModule,
          MatInputModule,
          MatNativeDateModule,
          MatDatepickerModule,
          MatSelectModule,
          MatRadioModule,
          MatProgressSpinnerModule
        ],
        declarations: [SoftwareProfileFormComponent],
        providers: [
          {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestUrlInterceptor,
            multi: true
          },
          {
            provide: BASE_PATH,
            useValue: '/v1'
          },
          {
            provide: MNEME_BASE_PATH,
            useValue: '/v1'
          },
          SoftwareProfilesService,
          AlertService,
          FilesService
        ]
      })
      .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
    alertService = TestBed.inject(AlertService);
    fileService = TestBed.inject(FilesService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareProfileFormComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const filesRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/files?filter=eq(isDistroDeb,true)`
    });

    filesRequest.flush(mockFiles);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a form', async () => {
    loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="name"]'
    }));

    loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="description"]'
    }));

    loader.getHarness(MatDatepickerInputHarness.with({
      selector: '[formControlName="date"]'
    }));

    const hoursSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="hours"]'
    }));

    await hoursSelect.open();

    const hoursOptions = await hoursSelect.getOptions();

    const hoursOptionTexts = await Promise.all(hoursOptions.map(option => option.getText()));

    const HOURS_TEXTS = Array.from({
      length: HOURS
    }, (_, index) => (index + 1)
      .toString()
      .padStart(2, '0')
    );

    expect(hoursOptionTexts).toEqual(['HH', ...HOURS_TEXTS], 'render hours select options');

    await hoursSelect.close();

    const minutesSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="minutes"]'
    }));

    await minutesSelect.open();

    const minutesOptions = await minutesSelect.getOptions();

    const minutesOptionTexts = await Promise.all(minutesOptions.map(option => option.getText()));

    const MINUTES_TEXTS = Array.from({
      length: MINUTES
    }, (_, index) => index
      .toString()
      .padStart(2, '0'));

    expect(minutesOptionTexts).toEqual(['MM', ...MINUTES_TEXTS], 'render minutes select options');

    await minutesSelect.close();

    const periodSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="period"]'
    }));

    await periodSelect.open();

    const periodOptions = await periodSelect.getOptions();

    const periodOptionTexts = await Promise.all(periodOptions.map(option => option.getText()));

    const PERIOD_TEXTS = Object
      .keys(TimePeriod)
      .slice(2);

    expect(periodOptionTexts).toEqual(PERIOD_TEXTS, 'render period select options');

    await periodSelect.close();

    const distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));

    const distributionTypeRadioButtons = await distributionTypeRadioGroup.getRadioButtons();

    const [autoTypeRadioButtonLabelText, newTypeRadioButtonLabelText, existingRadioTypeButtonLabelText] = await Promise.all(
      distributionTypeRadioButtons.map(typeButton => typeButton.getLabelText())
    );

    expect(autoTypeRadioButtonLabelText).toBe('Auto', 'render Auto distribution type radio button');
    expect(newTypeRadioButtonLabelText).toBe('New', 'render New distribution type radio button');
    expect(existingRadioTypeButtonLabelText).toBe('Existing', 'render Existing distribution type radio button');

    await distributionTypeRadioGroup.checkRadioButton({
      label: 'New'
    });

    const fileInputDe = fixture.debugElement.query(By.css('input[type="file"]'));

    expect(fileInputDe).toBeDefined('render distribution file input');

    await distributionTypeRadioGroup.checkRadioButton({
      label: 'Existing'
    });

    const versionSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="version"]'
    }));

    await versionSelect.open();

    const versionOptions = await versionSelect.getOptions();

    const versionOptionTexts = await Promise.all(
      versionOptions.map(versionOption => versionOption.getText())
    );

    const fileNames = mockFiles.files?.map(({ name }) => name);

    expect(versionOptionTexts).toEqual(['Choose a distribution', ...fileNames], 'render version options');

    await versionSelect.close();
  });

  it('should upload a distribution', async () => {
    let distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));
    await distributionTypeRadioGroup.checkRadioButton({
      label: 'New'
    });
    let fileInputDe = fixture.debugElement.query(By.css('input[type="file"]'));
    expect(fileInputDe).toBeDefined('render distribution file input');

    const distributionVersion = '1.5.1';
    const fileName = `distro-nefeli_${distributionVersion}_all.deb`;
    const file = new File(['distribution'], fileName, {
      type: 'application/vnd.debian.binary-package',
    });
    const fileList: FileList = {
      length: 1,
      item: (_: number): File | null => file,
      0: file
    };
    const event = {
      target: {
        files: fileList
      } as unknown
    } as Event;

    const uploadFileSpy = spyOn(fileService, 'uploadFile')
      .and.returnValue(
        of({
          name: fileName,
          isDistroDeb: true
        }) as Observable<any>
      );

    await component.onDistributionUpload(event);
    fixture.detectChanges();

    expect(uploadFileSpy).toHaveBeenCalledTimes(1);
    expect(uploadFileSpy).toHaveBeenCalledWith(fileName, undefined, file, true);

    try {
      distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness);
    } catch {
      distributionTypeRadioGroup = undefined;
    }
    expect(distributionTypeRadioGroup).toBeUndefined('hide distribution type radio group');

    fileInputDe = fixture.debugElement.query(By.css('input[type="file"]'));
    expect(fileInputDe).toBeNull('hide distribution file input');

    const distributionFileDe = fixture.debugElement.query(By.css('fieldset[formGroupName="distribution"] span.distribution-file'));
    expect(distributionFileDe).toBeDefined('render distribution file');
    expect(distributionFileDe.nativeElement.textContent).toBe(fileName, 'render distribution file name');

    const versionControl = component.form.get('distribution.version') as FormControl;
    expect(versionControl.value).toBe(distributionVersion, 'set a form version control value');
  });

  it('should handle error on upload a distribution', async () => {
    let distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));

    await distributionTypeRadioGroup.checkRadioButton({
      label: 'New'
    });

    let fileInputDe = fixture.debugElement.query(By.css('input[type="file"]'));

    expect(fileInputDe).toBeDefined('render distribution file input');

    const distributionVersion = '1.5.1';
    const fileName = `distro-nefeli_${distributionVersion}_all.deb`;

    const file = new File(['distribution'], fileName, {
      type: 'application/vnd.debian.binary-package',
    });

    const fileList: FileList = {
      length: 1,
      item: (_: number): File | null => file,
      0: file
    };

    const event = {
      target: {
        files: fileList
      } as unknown
    } as Event;

    const error = 'not supported';
    const alertErrorSpy = spyOn(alertService, 'error');
    const uploadFileSpy = spyOn(fileService, 'uploadFile').and.returnValue(throwError({ error: { message: error } }));
    await component.onDistributionUpload(event);
    fixture.detectChanges();

    expect(uploadFileSpy).toHaveBeenCalledTimes(1);
    expect(uploadFileSpy).toHaveBeenCalledWith(fileName, undefined, file, true);
    expect(alertErrorSpy).toHaveBeenCalledTimes(1);
    expect(alertErrorSpy).toHaveBeenCalledWith(AlertType.ERROR_UPLOAD_FILE, error);

    distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness);
    expect(distributionTypeRadioGroup).toBeTruthy();

    fileInputDe = fixture.debugElement.query(By.css('input[type="file"]'));
    expect(fileInputDe).toBeNull();

    const distributionFileDe = fixture.debugElement.query(By.css('fieldset[formGroupName="distribution"] span.distribution-file'));
    expect(distributionFileDe).toBeNull();

    const versionControl = component.form.get('distribution.version') as FormControl;
    expect(versionControl.value).toBeFalsy();
  });

  it('should handle a submit Software profile form error', async () => {
    const nameInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="name"]'
    }));

    await nameInput.setValue('Andrew');
    const distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));
    await distributionTypeRadioGroup.checkRadioButton({
      label: 'Existing'
    });

    const versionSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="version"]'
    }));

    await versionSelect.clickOptions({
      text: mockFiles.files[0].name
    });

    spyOn(alertService, 'error');
    spyOn(component.addProfile, 'emit').and.callThrough();

    component.onFormSubmit();

    const softwareProfileRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/software_profiles`
    });

    const mockErrorMessage = 'Name is in use.';

    const mockSoftwareProfileError: SoftwareProfileError = {
      httpStatus: HttpError.Conflict,
      message: mockErrorMessage,
      errorCode: 2504
    };

    const mockResponseOptions: {
      headers?: HttpHeaders | {
        [name: string]: string | string[];
      };
      status?: number;
      statusText?: string;
    } = {
      status: HttpError.Conflict,
      statusText: 'Conflict'
    };

    softwareProfileRequest.flush(mockSoftwareProfileError, mockResponseOptions);

    expect(alertService.error).toHaveBeenCalledWith(AlertType.ERROR_CREATE_SOFTWARE_PROFILE, mockErrorMessage);

    expect(component.addProfile.emit).not.toHaveBeenCalled();
  });

  it('should make time field required when date field is filled', async () => {
    const nameInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="name"]'
    }));

    await nameInput.setValue('Andrew');
    const distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));
    await distributionTypeRadioGroup.checkRadioButton({
      label: 'Existing'
    });

    const versionSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="version"]'
    }));

    await versionSelect.clickOptions({
      text: mockFiles.files[0].name
    });

    const datepickerInput = await loader.getHarness(MatDatepickerInputHarness.with({
      selector: '[formControlName="date"]'
    }));

    const mockDate = new Date();
    const mockLocaleDate = mockDate.toLocaleDateString();

    await datepickerInput.setValue(mockLocaleDate);
    component.onDateChange();
    component.onFormSubmit();

    httpTestingController.expectNone({
      method: 'POST',
      url: `${environment.restServer}/v1/software_profiles`
    });
    expect(component.form.get('date.time.hours').valid).toBeFalsy();
    expect(component.form.get('date.time.hours').hasError('required')).toBeTruthy();
    expect(component.form.get('date.time.minutes').valid).toBeFalsy();
    expect(component.form.get('date.time.minutes').hasError('required')).toBeTruthy();
    expect(component.form.get('date.time.period').valid).toBeTruthy();
    expect(component.form.get('date.time.period').hasError('required')).toBeFalsy();
  });

  it('should check distributionValidator', async () => {
    const distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));
    await distributionTypeRadioGroup.checkRadioButton({
      label: 'New'
    });
    let errors: ValidationErrors | null = distributionValidator(DistributionType.New)(component.form.get('distribution.file'));
    expect(errors).toBeTruthy();
    expect(errors.required).toBeTruthy();
    errors = distributionValidator(DistributionType.Existing)(component.form.get('distribution.version'));
    expect(errors).toBeFalsy();

    await distributionTypeRadioGroup.checkRadioButton({
      label: 'Existing'
    });
    errors = distributionValidator(DistributionType.Existing)(component.form.get('distribution.version'));
    expect(errors).toBeTruthy();
    expect(errors.required).toBeTruthy();
    errors = distributionValidator(DistributionType.New)(component.form.get('distribution.file'));
    expect(errors).toBeFalsy();

    const versionSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="version"]'
    }));
    await versionSelect.clickOptions({
      text: mockFiles.files[0].name
    });
    errors = distributionValidator(DistributionType.Existing)(component.form.get('distribution.version'));
    expect(errors).toBeFalsy();
  });

  it('should check timeValidator', async () => {
    const nameInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="name"]'
    }));

    let errors: ValidationErrors | null = timeValidator(component.form.get('date.time.hours'));
    expect(errors).toBeFalsy();
    errors = timeValidator(component.form.get('date.time.minutes'));
    expect(errors).toBeFalsy();
    errors = timeValidator(component.form.get('date.time.period'));
    expect(errors).toBeFalsy();

    const datepickerInput = await loader.getHarness(MatDatepickerInputHarness.with({
      selector: '[formControlName="date"]'
    }));
    const mockDate = new Date();
    const mockLocaleDate = mockDate.toLocaleDateString();
    await datepickerInput.setValue(mockLocaleDate);
    component.onDateChange();

    errors = timeValidator(component.form.get('date.time.hours'));
    expect(errors).toBeTruthy();
    expect(errors.required).toBeTruthy();
    errors = timeValidator(component.form.get('date.time.minutes'));
    expect(errors).toBeTruthy();
    expect(errors.required).toBeTruthy();
    errors = timeValidator(component.form.get('date.time.period'));
    expect(errors).toBeFalsy();
  });

  it('should submit a Software profile form', async () => {
    const nameInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="name"]'
    }));

    const mockName = 'Andrew';

    await nameInput.setValue(mockName);

    const descriptionInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="description"]'
    }));

    const mockDescription = 'Andrew profile';

    await descriptionInput.setValue(mockDescription);

    const datepickerInput = await loader.getHarness(MatDatepickerInputHarness.with({
      selector: '[formControlName="date"]'
    }));

    const mockDate = new Date();
    const mockLocaleDate = mockDate.toLocaleDateString();

    await datepickerInput.setValue(mockLocaleDate);

    const hoursSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="hours"]'
    }));

    let mockHours = mockDate.getHours();

    const mockPeriodText = mockHours < HOURS ? TimePeriod[0] : TimePeriod[1];

    mockHours = mockHours % HOURS || HOURS;

    const mockHoursText = mockHours
      .toString()
      .padStart(2, '0');

    await hoursSelect.clickOptions({
      text: mockHoursText
    });

    const minutesSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="minutes"]'
    }));

    const mockMinutes = mockDate.getMinutes();

    const mockMinutesText = mockMinutes
      .toString()
      .padStart(2, '0');

    await minutesSelect.clickOptions({
      text: mockMinutesText
    });

    const periodSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="period"]'
    }));

    await periodSelect.clickOptions({
      text: mockPeriodText
    });

    const distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));

    await distributionTypeRadioGroup.checkRadioButton({
      label: 'Existing'
    });

    const versionSelect = await loader.getHarness(MatSelectHarness.with({
      selector: '[formControlName="version"]'
    }));

    await versionSelect.clickOptions({
      text: mockFiles.files[0].name
    });

    spyOn(alertService, 'info');
    spyOn(component.addProfile, 'emit').and.callThrough();

    component.onFormSubmit();

    const softwareProfileRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/software_profiles`
    });

    mockDate.setSeconds(0);

    const mockVersion = mockFiles.files[0].name.split('_')[1];

    expect(softwareProfileRequest.request.body.name).toEqual(mockName, 'valid request body software profile name');
    expect(softwareProfileRequest.request.body.description).toEqual(mockDescription, 'valid request body software profile description');
    expect(softwareProfileRequest.request.body.notBefore.toLocaleString()).toEqual(mockDate.toLocaleString(), 'valid request body software profile date');
    expect(softwareProfileRequest.request.body.distroVersion).toEqual(mockVersion, 'valid request body software profile distribution version');

    const mockSoftwareProfile: SoftwareProfile = {
      identifier: '1',
      name: mockName,
      description: mockDescription,
      notBefore: mockDate,
      distroVersion: mockVersion
    };

    softwareProfileRequest.flush(mockSoftwareProfile);

    expect(alertService.info).toHaveBeenCalledWith(AlertType.INFO_CREATE_SOFTWARE_PROFILE);

    expect(component.addProfile.emit).toHaveBeenCalledWith(mockSoftwareProfile);

    expect(component.form.pristine).toBeTruthy('Software profile form has been reset');
  });

  it('should submit a Software profile form with auto distribution type', async () => {
    const nameInput = await loader.getHarness(MatInputHarness.with({
      selector: '[formControlName="name"]'
    }));
    const mockName = 'Andrew';
    await nameInput.setValue(mockName);

    const distributionTypeRadioGroup = await loader.getHarness(MatRadioGroupHarness.with({
      selector: '[formControlName="type"]'
    }));
    await distributionTypeRadioGroup.checkRadioButton({
      label: 'Auto'
    });

    component.onFormSubmit();
    const softwareProfileRequest = httpTestingController.expectOne({
      method: 'POST',
      url: `${environment.restServer}/v1/software_profiles?from_weaver=true`
    });
  });
});
