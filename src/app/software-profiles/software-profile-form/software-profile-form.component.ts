import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

import { of, Subscription } from 'rxjs';
import { catchError, pluck, tap } from 'rxjs/operators';

import { MnemeFileInfo } from 'rest_client/mneme/model/mnemeFileInfo';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { FilesService } from 'rest_client/mneme/api/files.service';

export enum DistributionType {
  Auto,
  New,
  Existing
}

export enum TimePeriod {
  AM,
  PM
}

export const HOURS = 12;
export const MINUTES = 60;

export function distributionValidator(type: DistributionType): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (control?.parent?.get('type')?.value === type) {
      return Validators.required(control);
    }
    // tslint:disable-next-line:no-null-keyword
    return null;
  };
}

export const timeValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  if (control?.parent?.parent?.get('date')?.value) {
    return Validators.required(control);
  }
  // tslint:disable-next-line:no-null-keyword
  return null;
};

@Component({
  selector: 'nef-software-profile-form',
  templateUrl: './software-profile-form.component.html',
  styleUrls: ['./software-profile-form.component.less']
})
export class SoftwareProfileFormComponent implements OnInit, OnDestroy {
  private _softwareProfile: Readonly<SoftwareProfile>;
  private _form: FormGroup;
  private _distributionFileName: string;
  private _files: MnemeFileInfo[];
  private _addProfile = new EventEmitter<SoftwareProfile>();
  private _profileSubscription: Subscription;
  private _subscription: Subscription;
  private _uploadInProgress = false;

  @Input()
  public set softwareProfile(profile: Readonly<SoftwareProfile>) {
    this._softwareProfile = profile;
  }

  @Output()
  public get addProfile(): EventEmitter<SoftwareProfile> {
    return this._addProfile;
  }

  constructor(
    private _fb: FormBuilder,
    private _softwareProfilesService: SoftwareProfilesService,
    private _alertService: AlertService,
    private _fileService: FilesService
  ) {
  }

  public ngOnInit() {
    this.initProfileForm();

    this._subscription = this._fileService
      .getFiles(undefined, undefined, ['eq(isDistroDeb,true)'])
      .pipe(
        pluck('files')
      )
      .subscribe(files => {
        this._files = files;
        this._form
          .get('distribution.version')
          .enable();
      });
  }

  public ngOnDestroy() {
    this._subscription?.unsubscribe();
  }

  private initProfileForm() {
    const type = DistributionType.Auto;

    let date = this._softwareProfile?.notBefore;

    if (date && typeof date === 'string') {
      date = new Date(this._softwareProfile?.notBefore);
    }

    this._form = this._fb.group({
      name: [this._softwareProfile?.name, Validators.required],
      description: this._softwareProfile?.description,
      date: this._fb.group({
        date,
        time: this._fb.group({
          hours: [
            date ? date.getHours() % HOURS : undefined,
            timeValidator
          ],
          minutes: [
            date?.getMinutes(),
            timeValidator
          ],
          period: [
            date
              ? (date.getHours() < HOURS ? TimePeriod.AM : TimePeriod.PM)
              : TimePeriod.AM,
            timeValidator
          ]
        })
      }),
      distribution: this._fb.group({
        type: [type, Validators.required],
        file: [undefined, distributionValidator(DistributionType.New)],
        version: [{
          value: this._softwareProfile?.distroVersion,
          disabled: false
        }, distributionValidator(DistributionType.Existing)]
      })
    });
  }

  public onDistributionTypeChange() {
    this._form
      .get('distribution.version')
      .reset();
    this._uploadInProgress = false;
    this._form.get('distribution.file')?.updateValueAndValidity();
    this._form.get('distribution.version')?.updateValueAndValidity();
  }

  public onDateChange() {
    this._form.get('date.time.hours')?.updateValueAndValidity();
    this._form.get('date.time.minutes')?.updateValueAndValidity();
  }

  private async uploadFiles(event: Event): Promise<string> {
    const target = event.target as HTMLInputElement;

    const fileList = target.files;
    const file = fileList?.item(0);

    if (!file) {
      return Promise.reject();
    }

    const fileName = file.name;
    return this._fileService
      .uploadFile(fileName, undefined, file, true)
      .pipe(
        pluck('name'),
        catchError(({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_UPLOAD_FILE, message);
          return of(undefined);
        }),
      ).toPromise();
  }

  public async onDistributionUpload(event: Event): Promise<void> {
    this._uploadInProgress = true;
    try {
      this._distributionFileName = await this.uploadFiles(event);
      this._form
        .get('distribution.type')
        .reset();
      if (this._distributionFileName) {
        const distributionVersion = this._distributionFileName.split('_')[1];
        this._form
          .get('distribution.version')
          .setValue(distributionVersion);
      } else {
        this._form
          .get('distribution.version')
          .setValue(undefined);
      }
    } catch {
    } finally {
      this._uploadInProgress = false;
    }
  }

  public onFormSubmit() {
    const { invalid, pristine } = this._form;

    this._profileSubscription?.unsubscribe();

    if (invalid || pristine) {
      return;
    }

    const {
      name,
      description,
      date: {
        date,
        time,
        time: {
          minutes,
          period
        }
      },
      distribution: {
        type,
        version
      }
    }: {
      name: string,
      description: string,
      date: {
        date: Date,
        time: {
          hours: number,
          minutes: number,
          period: TimePeriod
        }
      },
      distribution: {
        type: DistributionType,
        version: string
      }
    } = this._form.value;

    let { hours } = time;

    if (date) {
      if (hours !== undefined) {
        if (period === TimePeriod.PM) {
          hours += HOURS;
        }

        date.setHours(hours);
      }

      if (minutes !== undefined) {
        date.setMinutes(minutes);
      }
    }

    const softwareProfile: SoftwareProfile = {
      identifier: this._softwareProfile?.identifier,
      name,
      description,
      notBefore: date,
      distroVersion: version
    };

    const fromWeaver = type === DistributionType.Auto ? true : undefined;

    const profile$ = this._softwareProfile
      ? this._softwareProfilesService.putSoftwareProfile(this._softwareProfile.identifier, softwareProfile, fromWeaver)
      : this._softwareProfilesService.postSoftwareProfile(softwareProfile, fromWeaver);

    this._profileSubscription = profile$.subscribe({
      next: profile => {
        const alert = this._softwareProfile
          ? AlertType.INFO_EDIT_SOFTWARE_PROFILE
          : AlertType.INFO_CREATE_SOFTWARE_PROFILE;

        this._alertService.info(alert);

        this._addProfile.emit(profile ?? softwareProfile);

        this._form.reset();
      },
      error: ({
        error: { message }
      }: HttpErrorResponse) => {
        const alert = this._softwareProfile
          ? AlertType.ERROR_EDIT_SOFTWARE_PROFILE
          : AlertType.ERROR_CREATE_SOFTWARE_PROFILE;

        this._alertService.error(alert, message);
      }
    });
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get minDate(): Date {
    return new Date();
  }

  public get HOURS(): number[] {
    return Array.from({
      length: HOURS
    }, (_, index) => index + 1);
  }

  public get MINUTES(): number[] {
    return Array.from({
      length: MINUTES
    }, (_, index) => index);
  }

  public get TimePeriod(): typeof TimePeriod {
    return TimePeriod;
  }

  public get DistributionType(): typeof DistributionType {
    return DistributionType;
  }

  public get distributionFileName(): string {
    return this._distributionFileName;
  }

  public get files(): MnemeFileInfo[] {
    return this._files;
  }

  public get uploadInProgress(): boolean {
    return this._uploadInProgress;
  }
}
