import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DecimalPipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatCardModule } from '@angular/material/card';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';

import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

import softwareProfiles from '../../../../mock/software_profiles';

import { SoftwareProfileManagerDialogComponent, SoftwareProfileManagerDialogData } from './software-profile-manager-dialog.component';
import { SoftwareProfileFormComponent } from '../software-profile-form/software-profile-form.component';
import { TableComponent } from 'src/app/shared/table/table.component';
import { SoftwareProfilesComponent } from '../software-profiles.component';

describe('SoftwareProfileManagerDialogComponent', () => {
  let component: SoftwareProfileManagerDialogComponent;
  let fixture: ComponentFixture<SoftwareProfileManagerDialogComponent>;
  let loader: HarnessLoader;

  const mockDialogRef = jasmine.createSpyObj<MatDialogRef<SoftwareProfileManagerDialogComponent, readonly SoftwareProfile[]>>(
    'MatDialogRef',
    ['close']
  );

  const mockSoftwareProfiles = Object.freeze(softwareProfiles.softwareProfiles);

  const mockSoftwareProfile: SoftwareProfile = {
    identifier: '4',
    name: 'Nefeli Profile',
    description: 'Nefeli\'s profile',
    distroVersion: '4',
    notBefore: new Date(2091, 1, 2, 12, 54)
  };

  const mockDialogData: SoftwareProfileManagerDialogData = {
    profiles: mockSoftwareProfiles
  };

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          HttpClientTestingModule,
          ReactiveFormsModule,
          FlexLayoutModule,
          MatDialogModule,
          MatButtonModule,
          MatTableModule,
          MatTooltipModule,
          MatSnackBarModule,
          MatCardModule,
          MatInputModule,
          MatNativeDateModule,
          MatDatepickerModule,
          MatSelectModule,
          MatRadioModule
        ],
        declarations: [
          SoftwareProfileManagerDialogComponent,
          SoftwareProfileFormComponent,
          TableComponent,
          SoftwareProfilesComponent,
          TableComponent
        ],
        providers: [
          DecimalPipe,
          {
            provide: MAT_DIALOG_DATA,
            useValue: mockDialogData
          },
          {
            provide: MatDialogRef,
            useValue: mockDialogRef
          }
        ]
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareProfileManagerDialogComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a title', () => {
    const headingDe = fixture.debugElement.query(By.css('[mat-dialog-title]'));

    expect(headingDe.nativeElement.textContent).toBe('Software Profile Manager', 'render a title');
  });

  it('should render a software profile form', async () => {
    const addNewProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Profile'
    }));

    await addNewProfileButton.click();

    await loader.getHarness(MatCardHarness);

    const softwareProfileFormDe = fixture.debugElement.query(By.css('mat-dialog-content mat-card nef-software-profile-form'));

    expect(softwareProfileFormDe).toBeDefined('render a Software Profile Form component');

    const hideButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Hide'
    }));

    await hideButton.click();

    let profileFormCard: MatCardHarness;

    try {
      profileFormCard = await loader.getHarness(MatCardHarness);
    } catch { }

    expect(profileFormCard).toBeUndefined('hide profile form card');
  });

  it('should render a Software Profiles component', () => {
    const softwareProfilesDe = fixture.debugElement.query(By.css('mat-dialog-content nef-software-profiles'));

    expect(softwareProfilesDe).toBeDefined('render a Software Profiles component');
  });

  it('should add a software profile', async () => {
    const addNewProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Profile'
    }));

    await addNewProfileButton.click();

    const softwareProfileFormDe = fixture.debugElement.query(By.css('mat-dialog-content mat-card nef-software-profile-form'));
    const softwareProfilesDe = fixture.debugElement.query(By.css('mat-dialog-content nef-software-profiles'));

    const softwareProfilesComponent = softwareProfilesDe.componentInstance as SoftwareProfilesComponent;

    spyOn(softwareProfilesComponent, 'addProfile');

    softwareProfileFormDe.triggerEventHandler('addProfile', mockSoftwareProfile);

    expect(softwareProfilesComponent.addProfile).toHaveBeenCalledWith(mockSoftwareProfile);
  });

  it('should pass software profiles from dialog', async () => {
    const addNewProfileButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Add New Profile'
    }));

    await addNewProfileButton.click();

    const softwareProfileFormDe = fixture.debugElement.query(By.css('mat-dialog-content mat-card nef-software-profile-form'));

    softwareProfileFormDe.triggerEventHandler('addProfile', mockSoftwareProfile);

    const closeButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Close'
    }));

    await closeButton.click();

    expect(mockDialogRef.close).toHaveBeenCalledWith([...mockSoftwareProfiles, mockSoftwareProfile]);
  });
});
