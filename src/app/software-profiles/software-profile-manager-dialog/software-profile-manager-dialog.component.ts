import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

export interface SoftwareProfileManagerDialogData {
  readonly profiles: readonly SoftwareProfile[];
}

@Component({
  selector: 'nef-software-profile-manager-dialog',
  templateUrl: './software-profile-manager-dialog.component.html',
  styleUrls: ['./software-profile-manager-dialog.component.less']
})
export class SoftwareProfileManagerDialogComponent {
  private _showNewProfile: boolean;

  constructor(@Inject(MAT_DIALOG_DATA) private _data: SoftwareProfileManagerDialogData) { }

  public toggleNewProfile() {
    this._showNewProfile = !this._showNewProfile;
  }

  public get showNewProfile(): boolean {
    return this._showNewProfile;
  }

  public get softwareProfiles(): readonly SoftwareProfile[] {
    return this._data?.profiles;
  }
}
