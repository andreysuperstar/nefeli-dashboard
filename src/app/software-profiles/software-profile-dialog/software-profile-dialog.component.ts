import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

export interface SoftwareProfileDialogData {
  readonly profile: Readonly<SoftwareProfile>;
}

@Component({
  selector: 'nef-software-profile-dialog',
  templateUrl: './software-profile-dialog.component.html',
  styleUrls: ['./software-profile-dialog.component.less']
})
export class SoftwareProfileDialogComponent {
  constructor(
    private _dialogRef: MatDialogRef<SoftwareProfileDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: SoftwareProfileDialogData
  ) { }

  public onAddProfile(profile: SoftwareProfile) {
    this._dialogRef.close(profile);
  }

  public get isEditMode(): boolean {
    return Boolean(this._data);
  }

  public get profile(): Readonly<SoftwareProfile> {
    return this._data?.profile;
  }
}
