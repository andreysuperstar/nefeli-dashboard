import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';

import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

import { SoftwareProfileFormComponent } from '../software-profile-form/software-profile-form.component';
import { SoftwareProfileDialogComponent } from './software-profile-dialog.component';

describe('SoftwareProfileDialogComponent', () => {
  let component: SoftwareProfileDialogComponent;
  let fixture: ComponentFixture<SoftwareProfileDialogComponent>;
  let loader: HarnessLoader;

  const dialogSpy = jasmine.createSpyObj<MatDialogRef<SoftwareProfileDialogComponent>>(
    'MatDialogRef',
    ['close']
  );

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          HttpClientTestingModule,
          ReactiveFormsModule,
          FlexLayoutModule,
          MatDialogModule,
          MatSnackBarModule,
          MatInputModule,
          MatNativeDateModule,
          MatDatepickerModule,
          MatSelectModule,
          MatRadioModule,
          MatButtonModule
        ],
        declarations: [
          SoftwareProfileFormComponent,
          SoftwareProfileDialogComponent
        ],
        providers: [
          {
            provide: MatDialogRef,
            useValue: dialogSpy
          },
          {
            provide: MAT_DIALOG_DATA,
            useValue: undefined
          }
        ]
      })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SoftwareProfileDialogComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render a title', () => {
    const headingDe = fixture.debugElement.query(By.css('[mat-dialog-title]'));

    expect(headingDe.nativeElement.textContent).toBe('Add Software Profile', 'render a title');
  });

  it('should render a Software Profile Form component and dialog buttons', async () => {
    const softwareProfileFormDe = fixture.debugElement.query(By.css('mat-dialog-content nef-software-profile-form'));

    expect(softwareProfileFormDe).toBeDefined('render a Software Profile Form component');

    loader.getHarness(MatButtonHarness.with({
      text: 'Cancel'
    }));

    const saveButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Save'
    }));

    const isSaveButtonDisabled = await saveButton.isDisabled();

    expect(isSaveButtonDisabled).toBeTruthy('render Save button disabled');
  });

  it('should close a dialog with a software profile', async () => {
    const softwareProfileFormDe = fixture.debugElement.query(By.css('mat-dialog-content nef-software-profile-form'));

    const mockSoftwareProfile: SoftwareProfile = {
      identifier: '4',
      name: 'Nefeli Profile',
      description: 'Nefeli\'s profile',
      distroVersion: '4',
      notBefore: new Date(2091, 1, 2, 12, 54)
    };

    softwareProfileFormDe.triggerEventHandler('addProfile', mockSoftwareProfile);

    expect(dialogSpy.close).toHaveBeenCalledWith(mockSoftwareProfile);
  });
});
