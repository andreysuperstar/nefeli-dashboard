import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { formatDate } from '@angular/common';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { BehaviorSubject, iif, Observable, Subscription } from 'rxjs';
import { filter, map, mergeMapTo, switchMap, tap } from 'rxjs/operators';

import { SoftwareProfiles } from 'rest_client/pangolin/model/softwareProfiles';
import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';

import { Column, ColumnType, TableActionButton, TableActionEvent, TableActions, TableComponent } from '../shared/table/table.component';
import { SoftwareProfileDialogComponent, SoftwareProfileDialogData } from './software-profile-dialog/software-profile-dialog.component';

import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';
import { ConfirmationDialogComponent, ConfirmationDialogData } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { AlertService, AlertType } from '../shared/alert.service';

interface SoftwareProfileRow {
  identifier: string;
  name: string;
  description: string;
  version: string;
  date: string;
  actions: TableActionButton[];
}

export const COLUMNS: Column[] = [
  {
    name: 'name'
  },
  {
    name: 'description'
  },
  {
    name: 'version',
    label: 'Version'
  },
  {
    name: 'date',
    label: 'Version Applied After',
    type: ColumnType.TOOLTIP
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS
  }
];

const ACTIONS: TableActionButton[] = [
  {
    text: 'Edit',
    action: TableActions.Edit,
    icon: 'edit'
  },
  {
    text: 'Remove',
    action: TableActions.Remove,
    icon: 'trash-blue'
  }
];

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

@Component({
  selector: 'nef-software-profiles',
  templateUrl: './software-profiles.component.html',
  styleUrls: ['./software-profiles.component.less']
})
export class SoftwareProfilesComponent implements OnInit, OnDestroy {
  private _softwareProfiles$ = new BehaviorSubject<readonly SoftwareProfile[]>(undefined);
  private _dataSource$: Observable<SoftwareProfileRow[]>;
  private _dialogSubscription: Subscription;
  private _localDataSource = false;

  @Input() public set profiles(profiles: readonly SoftwareProfile[]) {
    this.softwareProfiles = profiles;
  }

  @ViewChild('softwareProfilesTable', {
    static: true
  }) private _softwareProfilesTable: TableComponent;

  constructor(
    private _dialog: MatDialog,
    private _softwareProfilesService: SoftwareProfilesService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this._localDataSource = Boolean(this._softwareProfiles$.value);
    const softwareProfiles$ = this._localDataSource
      ? this._softwareProfiles$
      : this._softwareProfilesService
        .getSoftwareProfiles(undefined, ['asc(name)'])
        .pipe(
          tap(({ softwareProfiles }) => {
            this.softwareProfiles = softwareProfiles;
          }),
          mergeMapTo<SoftwareProfiles, BehaviorSubject<readonly SoftwareProfile[]>>(this._softwareProfiles$)
        );

    this._dataSource$ = softwareProfiles$.pipe(
      filter(Boolean),
      map(this.mapDataSource),
      tap(() => {
        if (this._localDataSource) {
          this.sortTable();
        }
      })
    );
  }

  public ngOnDestroy() {
    this._dialogSubscription?.unsubscribe();
  }

  private mapDataSource(profiles: SoftwareProfile[]): SoftwareProfileRow[] {
    return profiles?.map(({
      identifier,
      name,
      description,
      distroVersion: version,
      notBefore: date
    }) => ({
      identifier,
      name,
      description,
      version,
      date: formatDate(date, 'medium', 'en-US'),
      actions: ACTIONS
    }));
  }

  private sortTable() {
    this._softwareProfilesTable.setSortDetails({
      id: 'name',
      start: 'asc',
      disableClear: false
    });
  }

  public addProfile(profile: SoftwareProfile) {

    if (this._localDataSource) {
      this.softwareProfiles = [
        ...this.softwareProfiles,
        Object.freeze(profile)
      ];
    } else {
      this._softwareProfilesService
        .getSoftwareProfiles(undefined, this.sort)
        .subscribe(({ softwareProfiles }) => {
          this.softwareProfiles = softwareProfiles;
        });
    }
  }

  public editProfile(identifier: string) {
    this._dialogSubscription?.unsubscribe();

    const softwareProfile = this.softwareProfiles.find(
      profile => profile.identifier === identifier
    );

    const data: SoftwareProfileDialogData = {
      profile: softwareProfile
    };

    const dialogRef = this._dialog.open<SoftwareProfileDialogComponent, SoftwareProfileDialogData, SoftwareProfile>(
      SoftwareProfileDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter<SoftwareProfile>(Boolean)
      )
      .subscribe(editedProfile => {
        if (this._localDataSource) {
          const index = this.softwareProfiles.findIndex(
            profile => profile.identifier === editedProfile.identifier
          );

          this.softwareProfiles = [
            ...this.softwareProfiles.slice(0, index),
            editedProfile,
            ...this.softwareProfiles.slice(index + 1)
          ];
        } else {
          this._softwareProfilesService
            .getSoftwareProfiles(undefined, this.sort)
            .subscribe(({ softwareProfiles }) => {
              this.softwareProfiles = softwareProfiles;
            });
        }
      });
  }

  private deleteProfile(identifier: string, name: string) {
    this._dialogSubscription?.unsubscribe();

    const data: ConfirmationDialogData = {
      description: `This will remove the software profile <strong>${name}</strong>.`,
      action: 'Remove Software Profile'
    };

    const dialogRef = this._dialog.open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        switchMap(() => this._softwareProfilesService.deleteSoftwareProfile(identifier))
      )
      .subscribe({
        next: () => {
          const index = this.softwareProfiles.findIndex(
            profile => profile.identifier === identifier
          );
          if (this._localDataSource) {
            this.softwareProfiles = [
              ...this.softwareProfiles.slice(0, index),
              ...this.softwareProfiles.slice(index + 1)
            ];
          } else {
            this._softwareProfilesService
              .getSoftwareProfiles(undefined, this.sort)
              .subscribe(({ softwareProfiles }) => {
                this.softwareProfiles = softwareProfiles;
              });
          }
          this._alertService.info(AlertType.INFO_REMOVE_SOFTWARE_PROFILE);
        },
        error: () => {
          this._alertService.error(AlertType.ERROR_REMOVE_SOFTWARE_PROFILE);
        }
      });
  }

  public onAction({
    type,
    data: { identifier, name }
  }: TableActionEvent<SoftwareProfileRow>) {
    switch (type) {
      case TableActions.Edit:
        this.editProfile(identifier);
        break;
      case TableActions.Remove:
        this.deleteProfile(identifier, name);
        break;
    }
  }

  public get softwareProfiles(): readonly SoftwareProfile[] {
    const profiles = this._softwareProfiles$.getValue();

    return Object.freeze(profiles);
  }

  public set softwareProfiles(profiles: readonly SoftwareProfile[]) {
    profiles = Object.freeze(profiles);

    this._softwareProfiles$.next(profiles);
  }

  public get COLUMNS(): Column[] {
    return COLUMNS;
  }

  public get dataSource$(): Observable<SoftwareProfileRow[]> {
    return this._dataSource$;
  }

  public get sort(): string[] {
    const sort = this._softwareProfilesTable.sort;
    const column = sort?.active;
    if (!column) {
      return ['asc(name)'];
    }
    return [`${sort.direction}(${column})`];
  }
}
