import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { DecimalPipe, formatDate } from '@angular/common';
import { HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OverlayContainer } from '@angular/cdk/overlay';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatTableModule } from '@angular/material/table';
import { MatTableHarness } from '@angular/material/table/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonHarness } from '@angular/material/button/testing';

import { of } from 'rxjs';

import { SoftwareProfile } from 'rest_client/pangolin/model/softwareProfile';
import { SoftwareProfileError } from 'rest_client/pangolin/model/softwareProfileError';

import { HttpError } from '../error-codes';

import { BASE_PATH } from 'rest_client/pangolin/variables';

import softwareProfiles from '../../../mock/software_profiles';
import { environment } from 'src/environments/environment';
import files from '../../../mock/files';

import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';

import { SoftwareProfilesService } from 'rest_client/pangolin/api/softwareProfiles.service';
import { AlertService, AlertType } from '../shared/alert.service';
import { FilesService } from 'rest_client/mneme/api/files.service';

import { COLUMNS, SoftwareProfilesComponent } from './software-profiles.component';
import { TableComponent } from '../shared/table/table.component';
import { SoftwareProfileFormComponent } from './software-profile-form/software-profile-form.component';
import { SoftwareProfileDialogComponent } from './software-profile-dialog/software-profile-dialog.component';

describe('SoftwareProfilesComponent', () => {
  let component: SoftwareProfilesComponent;
  let fixture: ComponentFixture<SoftwareProfilesComponent>;
  let httpTestingController: HttpTestingController;
  let overlayContainer: OverlayContainer;
  let documentRootLoader: HarnessLoader;
  let loader: HarnessLoader;
  let fileService: FilesService;
  let softwareProfilesService: SoftwareProfilesService;

  const mockSoftwareProfiles = softwareProfiles;

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        imports: [
          NoopAnimationsModule,
          HttpClientTestingModule,
          ReactiveFormsModule,
          FlexLayoutModule,
          MatTableModule,
          MatTooltipModule,
          MatSnackBarModule,
          MatDialogModule,
          MatInputModule,
          MatNativeDateModule,
          MatDatepickerModule,
          MatSelectModule,
          MatRadioModule,
          MatButtonModule
        ],
        declarations: [
          SoftwareProfilesComponent,
          TableComponent,
          SoftwareProfileFormComponent,
          SoftwareProfileDialogComponent
        ],
        providers: [
          DecimalPipe,
          {
            provide: HTTP_INTERCEPTORS,
            useClass: RequestUrlInterceptor,
            multi: true
          },
          {
            provide: BASE_PATH,
            useValue: '/v1'
          },
          SoftwareProfilesService,
          AlertService,
          FilesService
        ]
      })
      .compileComponents();

    httpTestingController = TestBed.inject(HttpTestingController);
    overlayContainer = TestBed.inject(OverlayContainer);
    fileService = TestBed.inject(FilesService);
    softwareProfilesService = TestBed.inject(SoftwareProfilesService);
  });

  afterEach(async () => {

    const dialogs = await documentRootLoader.getAllHarnesses(MatDialogHarness);
    await Promise.all(
      dialogs.map(dialog => dialog.close())
    );

    overlayContainer.ngOnDestroy();

  });

  describe('with local storage', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(SoftwareProfilesComponent);
      documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
      loader = TestbedHarnessEnvironment.loader(fixture);
      component = fixture.componentInstance;
      component.softwareProfiles = mockSoftwareProfiles.softwareProfiles;

      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should render software profiles table columns', async () => {
      const table = await loader.getHarness(MatTableHarness);

      const headerRows = await table.getHeaderRows();
      const headerCells = await headerRows[0].getCellTextByIndex();

      expect(headerCells).toEqual(COLUMNS.map(({ name, label }) => label ?? name), 'render table columns');
    });

    it('should render software profiles table', async () => {
      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();

      expect(rows.length).toBe(mockSoftwareProfiles.softwareProfiles.length, `render ${mockSoftwareProfiles.softwareProfiles.length} rows`);

      const cellTexts = await Promise.all(
        rows.map(row => row.getCellTextByColumnName())
      );

      cellTexts.forEach(({ name, description, version, date }, index) => {
        expect(name).toBe(mockSoftwareProfiles.softwareProfiles[index].name, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile name`);
        expect(description).toBe(mockSoftwareProfiles.softwareProfiles[index].description, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile description`);
        expect(version).toBe(mockSoftwareProfiles.softwareProfiles[index].distroVersion, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile version`);

        const formattedDate = formatDate(mockSoftwareProfiles.softwareProfiles[index].notBefore, 'medium', 'en-US');

        expect(date).toBe(formattedDate, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile date`);
      });
    });

    it('should add a software profile', async () => {
      const mockSoftwareProfile: SoftwareProfile = {
        identifier: '4',
        name: 'Nefeli Profile',
        description: 'Nefeli\'s profile',
        notBefore: new Date(2050, 2, 14, 12, 30),
        distroVersion: '1.5.1'
      };

      component.addProfile(mockSoftwareProfile);

      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();

      expect(rows.length).toBe(mockSoftwareProfiles.softwareProfiles.length + 1, `render ${mockSoftwareProfiles.softwareProfiles.length + 1} added rows`);

      const cellTexts = await Promise.all(
        rows.map(row => row.getCellTextByColumnName())
      );

      const addedRow = cellTexts.find(({ name }) => name === mockSoftwareProfile.name);

      expect(addedRow.name).toEqual(mockSoftwareProfile.name, 'render an added software profile row name');
      expect(addedRow.description).toEqual(mockSoftwareProfile.description, 'render an added software profile row description');

      const formattedDate = formatDate(mockSoftwareProfile.notBefore, 'medium', 'en-US');

      expect(addedRow.date).toEqual(formattedDate, 'render an added software profile row date');
      expect(addedRow.version).toEqual(mockSoftwareProfile.distroVersion, 'render an added software profile row version');
    });

    it('should edit a software profile', async () => {
      const rowDes = fixture.debugElement.queryAll(By.css('nef-table mat-row'));

      const index = 1;

      const editButtonDe = rowDes[index].query(By.css('button.action'));

      editButtonDe.nativeElement.click();

      await documentRootLoader.getHarness(MatDialogHarness);

      const mockSoftwareProfile: SoftwareProfile = {
        ...mockSoftwareProfiles.softwareProfiles[index],
        name: 'Nefeli Profile',
        description: 'Nefeli\'s profile',
        notBefore: new Date(2045, 11, 11),
        distroVersion: '4'
      };

      component['_dialog'].openDialogs[0].close(mockSoftwareProfile);
      fixture.detectChanges();

      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();
      const editedRow = await rows[index].getCellTextByColumnName();

      expect(editedRow.name).toEqual(mockSoftwareProfile.name, 'render an edited software profile row name');
      expect(editedRow.description).toEqual(mockSoftwareProfile.description, 'render an edited software profile row description');

      const formattedDate = formatDate(mockSoftwareProfile.notBefore, 'medium', 'en-US');

      expect(editedRow.date).toEqual(formattedDate, 'render an edited software profile row date');
      expect(editedRow.version).toEqual(mockSoftwareProfile.distroVersion, 'render an edited software profile row version');
    });

    it('should handle a remove software profile error', async () => {
      const rowDes = fixture.debugElement.queryAll(By.css('nef-table mat-row'));

      const index = 1;

      const [, removeButtonDe] = rowDes[index].queryAll(By.css('button.action'));

      removeButtonDe.nativeElement.click();

      await documentRootLoader.getHarness(MatDialogHarness);

      const removeSoftwareProfileButton = await documentRootLoader.getHarness(MatButtonHarness.with({
        ancestor: 'nef-confirmation-dialog',
        text: 'Remove Software Profile'
      }));

      spyOn(component['_alertService'], 'error');

      await removeSoftwareProfileButton.click();

      const deleteSoftwareProfileRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/software_profiles/${mockSoftwareProfiles.softwareProfiles[index].identifier}`
      });

      const mockSoftwareProfileError: SoftwareProfileError = {
        httpStatus: HttpError.Conflict,
        message: 'The profile is being used by a site.',
        errorCode: 2505
      };

      const mockResponseOptions: {
        headers?: HttpHeaders | {
          [name: string]: string | string[];
        };
        status?: number;
        statusText?: string;
      } = {
        status: HttpError.Conflict,
        statusText: 'Conflict'
      };

      deleteSoftwareProfileRequest.flush(mockSoftwareProfileError, mockResponseOptions);

      expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_SOFTWARE_PROFILE);
    });

    it('should remove a software profile', async () => {
      fixture.detectChanges();
      const rowDes = fixture.debugElement.queryAll(By.css('nef-table mat-row'));

      const index = 1;

      const [, removeButtonDe] = rowDes[index].queryAll(By.css('button.action'));

      removeButtonDe.nativeElement.click();

      const removeButton = await documentRootLoader.getHarness(MatButtonHarness.with({
        ancestor: 'nef-confirmation-dialog',
        text: 'Remove Software Profile'
      }));

      spyOn(component['_alertService'], 'info');

      await removeButton.click();

      const deleteSoftwareProfileRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/software_profiles/${mockSoftwareProfiles.softwareProfiles[index].identifier}`
      });

      deleteSoftwareProfileRequest.flush({});

      expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_REMOVE_SOFTWARE_PROFILE);

      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();

      expect(rows.length).toBe(mockSoftwareProfiles.softwareProfiles.length - 1, `render ${mockSoftwareProfiles.softwareProfiles.length - 1} remaining rows`);

      const cellTexts = await Promise.all(
        rows.map(row => row.getCellTextByColumnName())
      );

      const hasRemovedRow = cellTexts.some(({ identifier }) => identifier === mockSoftwareProfiles.softwareProfiles[index].identifier);

      expect(hasRemovedRow).toBeFalsy(`render software profiles rows without a removed software profile`);
    });

  });

  describe('with remote storage', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(SoftwareProfilesComponent);
      documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
      loader = TestbedHarnessEnvironment.loader(fixture);
      component = fixture.componentInstance;
      fixture.detectChanges();

      const softwareProfilesRequest = httpTestingController.expectOne({
        method: 'GET',
        url: `${environment.restServer}/v1/software_profiles?sort=asc(name)`
      });

      softwareProfilesRequest.flush(mockSoftwareProfiles);
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should render software profiles table columns', async () => {
      const table = await loader.getHarness(MatTableHarness);

      const headerRows = await table.getHeaderRows();
      const headerCells = await headerRows[0].getCellTextByIndex();

      expect(headerCells).toEqual(COLUMNS.map(({ name, label }) => label ?? name), 'render table columns');
    });

    it('should render software profiles table', async () => {
      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();

      expect(rows.length).toBe(mockSoftwareProfiles.softwareProfiles.length, `render ${mockSoftwareProfiles.softwareProfiles.length} rows`);

      const cellTexts = await Promise.all(
        rows.map(row => row.getCellTextByColumnName())
      );

      cellTexts.forEach(({ name, description, version, date }, index) => {
        expect(name).toBe(mockSoftwareProfiles.softwareProfiles[index].name, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile name`);
        expect(description).toBe(mockSoftwareProfiles.softwareProfiles[index].description, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile description`);
        expect(version).toBe(mockSoftwareProfiles.softwareProfiles[index].distroVersion, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile version`);

        const formattedDate = formatDate(mockSoftwareProfiles.softwareProfiles[index].notBefore, 'medium', 'en-US');

        expect(date).toBe(formattedDate, `render ${mockSoftwareProfiles.softwareProfiles[index].name} software profile date`);
      });
    });

    it('should add a software profile', async () => {
      const mockSoftwareProfile: SoftwareProfile = {
        identifier: '4',
        name: 'Nefeli Profile',
        description: 'Nefeli\'s profile',
        notBefore: new Date(2050, 2, 14, 12, 30),
        distroVersion: '1.5.1'
      };

      component.addProfile(mockSoftwareProfile);

      const softwareProfilesRequest = httpTestingController.expectOne({
        method: 'GET',
        url: `${environment.restServer}/v1/software_profiles?sort=asc(name)`
      });
      softwareProfilesRequest.flush({ softwareProfiles: [...mockSoftwareProfiles.softwareProfiles, mockSoftwareProfile] });

      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();

      expect(rows.length).toBe(mockSoftwareProfiles.softwareProfiles.length + 1, `render ${mockSoftwareProfiles.softwareProfiles.length + 1} added rows`);

      const cellTexts = await Promise.all(
        rows.map(row => row.getCellTextByColumnName())
      );

      const addedRow = cellTexts.find(({ name }) => name === mockSoftwareProfile.name);

      expect(addedRow.name).toEqual(mockSoftwareProfile.name, 'render an added software profile row name');
      expect(addedRow.description).toEqual(mockSoftwareProfile.description, 'render an added software profile row description');

      const formattedDate = formatDate(mockSoftwareProfile.notBefore, 'medium', 'en-US');

      expect(addedRow.date).toEqual(formattedDate, 'render an added software profile row date');
      expect(addedRow.version).toEqual(mockSoftwareProfile.distroVersion, 'render an added software profile row version');
    });

    it('should edit a software profile', async () => {
      const index = 1;
      const mockSoftwareProfile: SoftwareProfile = {
        ...mockSoftwareProfiles.softwareProfiles[index],
        name: 'Nefeli Profile',
        description: 'Nefeli\'s profile',
        notBefore: new Date(2045, 11, 11),
        distroVersion: '4'
      };

      const getFilesSpy = spyOn(fileService, 'getFiles')
        .and.returnValue(of<any>(files));

      const getSoftwareProfilesSpy = spyOn(softwareProfilesService, 'getSoftwareProfiles')
        .and.returnValue(
          of<any>({
            softwareProfiles: [...mockSoftwareProfiles.softwareProfiles.slice(0, index), mockSoftwareProfile, ...mockSoftwareProfiles.softwareProfiles.slice(index + 1)]
          })
        );

      const rowDes = fixture.debugElement.queryAll(By.css('nef-table mat-row'));

      const editButtonDe = rowDes[index].query(By.css('button.action'));

      editButtonDe.nativeElement.click();

      await documentRootLoader.getHarness(MatDialogHarness);
      component['_dialog'].openDialogs[0].close(mockSoftwareProfile);
      fixture.detectChanges();

      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();
      const editedRow = await rows[index].getCellTextByColumnName();

      expect(editedRow.name).toEqual(mockSoftwareProfile.name, 'render an edited software profile row name');
      expect(editedRow.description).toEqual(mockSoftwareProfile.description, 'render an edited software profile row description');

      const formattedDate = formatDate(mockSoftwareProfile.notBefore, 'medium', 'en-US');

      expect(editedRow.date).toEqual(formattedDate, 'render an edited software profile row date');
      expect(editedRow.version).toEqual(mockSoftwareProfile.distroVersion, 'render an edited software profile row version');

      expect(getFilesSpy).toHaveBeenCalledWith(undefined, undefined, ['eq(isDistroDeb,true)']);
      expect(getSoftwareProfilesSpy).toHaveBeenCalledWith(undefined, ['asc(name)']);
    });

    it('should handle a remove software profile error', async () => {
      const rowDes = fixture.debugElement.queryAll(By.css('nef-table mat-row'));

      const index = 1;

      const [, removeButtonDe] = rowDes[index].queryAll(By.css('button.action'));

      removeButtonDe.nativeElement.click();

      await documentRootLoader.getHarness(MatDialogHarness);

      const removeSoftwareProfileButton = await documentRootLoader.getHarness(MatButtonHarness.with({
        ancestor: 'nef-confirmation-dialog',
        text: 'Remove Software Profile'
      }));

      spyOn(component['_alertService'], 'error');

      await removeSoftwareProfileButton.click();

      const deleteSoftwareProfileRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/software_profiles/${mockSoftwareProfiles.softwareProfiles[index].identifier}`
      });

      const mockSoftwareProfileError: SoftwareProfileError = {
        httpStatus: HttpError.Conflict,
        message: 'The profile is being used by a site.',
        errorCode: 2505
      };

      const mockResponseOptions: {
        headers?: HttpHeaders | {
          [name: string]: string | string[];
        };
        status?: number;
        statusText?: string;
      } = {
        status: HttpError.Conflict,
        statusText: 'Conflict'
      };

      deleteSoftwareProfileRequest.flush(mockSoftwareProfileError, mockResponseOptions);

      expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_REMOVE_SOFTWARE_PROFILE);
    });

    it('should remove a software profile', async () => {
      fixture.detectChanges();
      const rowDes = fixture.debugElement.queryAll(By.css('nef-table mat-row'));

      const index = 1;

      const [, removeButtonDe] = rowDes[index].queryAll(By.css('button.action'));

      removeButtonDe.nativeElement.click();

      const removeButton = await documentRootLoader.getHarness(MatButtonHarness.with({
        ancestor: 'nef-confirmation-dialog',
        text: 'Remove Software Profile'
      }));

      spyOn(component['_alertService'], 'info');

      await removeButton.click();

      const deleteSoftwareProfileRequest = httpTestingController.expectOne({
        method: 'DELETE',
        url: `${environment.restServer}/v1/software_profiles/${mockSoftwareProfiles.softwareProfiles[index].identifier}`
      });

      deleteSoftwareProfileRequest.flush({});

      const softwareProfilesRequest = httpTestingController.expectOne({
        method: 'GET',
        url: `${environment.restServer}/v1/software_profiles?sort=asc(name)`
      });
      softwareProfilesRequest.flush({ softwareProfiles: [...mockSoftwareProfiles.softwareProfiles.slice(0, index), ...mockSoftwareProfiles.softwareProfiles.slice(index + 1)] });

      expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_REMOVE_SOFTWARE_PROFILE);

      const table = await loader.getHarness(MatTableHarness);
      const rows = await table.getRows();

      expect(rows.length).toBe(mockSoftwareProfiles.softwareProfiles.length - 1, `render ${mockSoftwareProfiles.softwareProfiles.length - 1} remaining rows`);

      const cellTexts = await Promise.all(
        rows.map(row => row.getCellTextByColumnName())
      );

      const hasRemovedRow = cellTexts.some(({ identifier }) => identifier === mockSoftwareProfiles.softwareProfiles[index].identifier);

      expect(hasRemovedRow).toBeFalsy(`render software profiles rows without a removed software profile`);
    });

  });
});
