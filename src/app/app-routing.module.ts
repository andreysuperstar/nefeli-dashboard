import { NgModule } from '@angular/core';
import { RouterModule, Routes, ExtraOptions } from '@angular/router';

import { AuthenticationGuard } from './auth//authentication.guard';
import { SaasAuthenticationGuard } from './auth/saas.authentication.guard';

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule),
    data: document.location
  },
  {
    path: 'saas',
    loadChildren: () => import('./saas-login/saas-login.module').then(m => m.SaasLoginModule),
    data: document.location
  },
  {
    path: 'saas/home',
    loadChildren: () => import('./saas-home/saas-home.module').then(m => m.SaasHomeModule),
    canLoad: [SaasAuthenticationGuard],
    data: document.location
  },
  {
    path: '',
    canLoad: [AuthenticationGuard],
    children: [
      {
        path: '',
        canLoad: [AuthenticationGuard],
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      }
    ]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

const config: ExtraOptions = {
  useHash: true,
  paramsInheritanceStrategy: 'always',
  relativeLinkResolution: 'legacy'
};

@NgModule({
  imports: [
    RouterModule.forRoot(routes, config)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
