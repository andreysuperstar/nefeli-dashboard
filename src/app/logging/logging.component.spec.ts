import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatSelectHarness } from '@angular/material/select/testing';

import { Sites } from 'rest_client/pangolin/model/sites';
import { RESTServerList } from 'rest_client/pangolin/model/rESTServerList';
import { User } from 'rest_client/heimdallr/model/user';

import { ServersService } from 'rest_client/pangolin/api/servers.service';

import { LoggingComponent } from './logging.component';
import { environment } from '../../environments/environment';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { BackButtonComponent } from '../shared/back-button/back-button.component';
import { UserService } from '../users/user.service';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { LoggingConfigStatus } from 'rest_client/pangolin/model/loggingConfigStatus';
import { AlertService } from '../shared/alert.service';
import { LogsService } from 'rest_client/pangolin/api/logs.service';
import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as ALARMS_BASE_PATH } from 'rest_client/pangolin/variables';

const mockGetLogging = {
  endpoint: {
    host: 'my-host',
    password: 'my-password',
    user: 'my-user'
  },
  status: "ENABLED"
}

const mockSystemAdmin: User = {
  username: 'ecarino',
  email: 'eric@nefeli.io',
  roles: {
    scope: 'system',
    id: '-1',
    roles: ['admin', 'user']
  }
};

const mockSystemUser: User = {
  username: 'andrew',
  email: 'andrew@nefeli.io',
  roles: {
    scope: 'system',
    id: '-1',
    roles: ['user']
  }
};

describe('LoggingComponent', () => {
  let component: LoggingComponent;
  let fixture: ComponentFixture<LoggingComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;
  let userService: UserService;
  let loader: HarnessLoader;

  const mockSites: Sites = {
    sites: [
      {
        config: {
          identifier: 'eric_site',
          name: 'Eric Site'
        }
      },
      {
        config: {
          identifier: 'andrew_site',
          name: 'Andrew Site'
        }
      },
      {
        config: {
          identifier: 'anton_site',
          name: 'Anton Site'
        }
      }
    ]
  };

  const mockServers: RESTServerList = {
    servers: [
      {
        identifier: '50ff708a-45fa-47ed-a579-d86fa6e117c7',
        name: 'fluffy'
      },
      {
        identifier: '64946861-02ab-4106-a938-d5ea9be1e1d9',
        name: 'fritz'
      },
      {
        identifier: 'cf29f798-0295-4136-ba6f-993df4bb51d9',
        name: 'felicia'
      }
    ]
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,
        MatSnackBarModule,
        MatCardModule,
        MatDialogModule
      ],
      declarations: [
        LoggingComponent,
        BackButtonComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: PANGOLIN_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: ALARMS_BASE_PATH,
          useValue: '/v1'
        },
        ServersService,
        AlertService,
        UserService,
        LogsService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    userService = TestBed.inject(UserService);
    userService.setUser(mockSystemAdmin);
    fixture = TestBed.createComponent(LoggingComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    el = fixture.debugElement.nativeElement;
    httpMockClient = TestBed.inject(HttpTestingController);
    fixture.detectChanges();

    const req = httpMockClient.expectOne(
      {
        method: 'GET',
        url: `${environment.restServer}/v1/logs/centralized_logging`
      }
    );

    req.flush(mockGetLogging);

    const sitesRequest = httpMockClient.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/sites`
    });

    sitesRequest.flush(mockSites);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', () => {
    const title = el.querySelector('header h2');
    expect(title.textContent.trim()).toBe('Logging');
  });

  it('should have form controls', () => {
    const hostInput = el.querySelector('.mat-input-element[formcontrolname=host]');
    const userInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=user]');
    const passwordInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=password]');

    expect(hostInput).not.toBeNull();
    expect(hostInput).not.toBeNull();
    expect(userInput).not.toBeNull();
    expect(passwordInput).not.toBeNull();
  });

  it('should submit config form', async () => {
    const mockConfigFormData = {
      logging: true,
      host: 'https://sylvester.lazy-dog.org'
    };

    component.loggingForm.patchValue(mockConfigFormData);
    component.loggingForm.markAsDirty();

    const saveButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      text: 'Save'
    }));
    await saveButton.click();

    const loggingReq = httpMockClient.expectOne(
      {
        method: 'PUT',
        url: `${environment.restServer}/v1/logs/centralized_logging`
      }
    );

    expect(loggingReq.request.method).toBe('PUT');
    expect(loggingReq.request.body.status).toBe(LoggingConfigStatus.ENABLED);
    expect(loggingReq.request.body.endpoint.host).toBe(mockConfigFormData.host);
    expect(loggingReq.request.body.endpoint.password).toBe('my-password');
    expect(loggingReq.request.body.endpoint.user).toBe('my-user');
  });

  it('should initialize configuration form on load', () => {
    expect(component['_loggingForm'].value.host).toBe("my-host");
    expect(component['_loggingForm'].value.logging).toBe(true);
    expect(component['_loggingForm'].value.password).toBe("my-password");
    expect(component['_loggingForm'].value.user).toBe("my-user");
  });

  it('should not disable user and password', () => {
    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();
    const userInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=user]');
    const passwordInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=password]');
    expect(userInput).not.toBeNull();
    expect(passwordInput).not.toBeNull();
    expect(userInput.disabled).toBeFalsy();
    expect(passwordInput.disabled).toBeFalsy();
  });

  it('should render Download Logs card for system admin only', async () => {
    await loader.getHarness(MatCardHarness.with({
      title: 'Download Logs'
    }));

    userService.setUser(mockSystemUser);
    fixture.detectChanges();

    let downloadLogsCard: MatCardHarness;

    try {
      downloadLogsCard = await loader.getHarness(MatCardHarness.with({
        title: 'Download Logs'
      }));
    } catch { }

    expect(downloadLogsCard).toBeUndefined('hide Download Logs card for system user');
  })

  it('should render Download Logs form', async () => {
    // Initially render Site and Server selects disabled
    fixture = TestBed.createComponent(LoggingComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    fixture.detectChanges();

    const [siteSelect, serverSelect] = await loader.getAllHarnesses(MatSelectHarness.with({
      ancestor: 'form#download-logs-form'
    }));

    let [siteSelectValue, isSiteSelectDisabled, serverSelectValue, isServerSelectDisabled] = await Promise.all([
      siteSelect.getValueText(),
      siteSelect.isDisabled(),
      serverSelect.getValueText(),
      serverSelect.isDisabled()
    ]);

    expect(siteSelectValue).toBe('All Sites', 'valid Site select value');
    expect(isSiteSelectDisabled).toBeTruthy('render Site select disabled');
    expect(serverSelectValue).toBe('All Servers', 'valid Server select value');
    expect(isServerSelectDisabled).toBeTruthy('render Server select disabled');

    // Fetch sites then render Sites select enabled
    const sitesRequest = httpMockClient.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/sites`
    });

    sitesRequest.flush(mockSites);
    fixture.detectChanges();

    expect(await siteSelect.isDisabled()).toBeFalsy('render Site select enabled');

    await siteSelect.open();

    const siteOptions = await siteSelect.getOptions();

    expect(siteOptions.length).toBe(mockSites.sites?.length + 1, `render ${mockSites.sites?.length} site options`);

    const [defaultSiteOptionText, ...siteOptionTexts] = await Promise.all(
      siteOptions.map(siteOption => siteOption.getText())
    );

    expect(defaultSiteOptionText).toBe('All Sites');
    expect(siteOptionTexts[0]).toBe(mockSites.sites[0].config.name, `render ${mockSites.sites[0].config.name} site option name`);
    expect(siteOptionTexts[1]).toBe(mockSites.sites[1].config.name, `render ${mockSites.sites[1].config.name} site option name`);
    expect(siteOptionTexts[2]).toBe(mockSites.sites[2].config.name, `render ${mockSites.sites[2].config.name} site option name`);

    // Select a site then fetch servers
    await siteOptions[2].click();
    await siteSelect.close();

    const serversRequest = httpMockClient.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/sites/${mockSites.sites[1].config.identifier}/servers`
    });

    serversRequest.flush(mockServers);
    fixture.detectChanges();

    // Render Servers select enabled
    [serverSelectValue, isServerSelectDisabled] = await Promise.all([
      serverSelect.getValueText(),
      serverSelect.isDisabled()
    ]);

    expect(isServerSelectDisabled).toBeFalsy('render Server select enabled');

    await serverSelect.open();

    const serverOptions = await serverSelect.getOptions();

    expect(serverOptions.length).toBe(mockServers.servers?.length + 1, `render ${mockServers.servers?.length} server options`);

    const [defaultServerOptionText, ...serverOptionTexts] = await Promise.all(
      serverOptions.map(serverOption => serverOption.getText())
    );

    expect(defaultServerOptionText).toBe('All Servers');
    expect(serverOptionTexts[0]).toBe(mockServers.servers[0]?.name, `render ${mockServers.servers[0]?.name} server option name`);
    expect(serverOptionTexts[1]).toBe(mockServers.servers[1]?.name, `render ${mockServers.servers[1]?.name} server option name`);
    expect(serverOptionTexts[2]).toBe(mockServers.servers[2]?.name, `render ${mockServers.servers[2]?.name} server option name`);

    await serverSelect.close();

    // Render filter checkboxes
    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness.with({
      ancestor: 'form#download-logs-form'
    }));

    const checkboxesLabelTexts = await Promise.all(
      checkboxes.map(checkbox => checkbox.getLabelText())
    );

    const checkboxesValues = await Promise.all(
      checkboxes.map(checkbox => checkbox.isChecked())
    );

    expect(checkboxesLabelTexts[0]).toBe('Include configuration files', 'render Include configuration files checkbox label');
    expect(checkboxesValues[0]).toBeTruthy('valid Include configuration files checkbox value');

    expect(checkboxesLabelTexts[1]).toBe('Include runtime configuration files', 'render Include runtime configuration files checkbox label');
    expect(checkboxesValues[1]).toBeTruthy('valid Include runtime configuration files value');

    expect(checkboxesLabelTexts[2]).toBe('Include data files', 'render Include data files checkbox label');
    expect(checkboxesValues[2]).toBeTruthy('valid Include data files value');

    expect(checkboxesLabelTexts[3]).toBe('Include log files', 'render Include log files checkbox label');
    expect(checkboxesValues[3]).toBeTruthy('valid Include log files value');

    expect(checkboxesLabelTexts[4]).toBe('Include temp files', 'render Include temp files checkbox label');
    expect(checkboxesValues[4]).toBeTruthy('valid Include temp files value');

    expect(checkboxesLabelTexts[5]).toBe('Include etc files', 'render Include etc files checkbox label');
    expect(checkboxesValues[5]).toBeTruthy('valid Include etc files value');

    expect(checkboxesLabelTexts[6]).toBe('Include library files', 'render Include library files checkbox label');
    expect(checkboxesValues[6]).toBeTruthy('valid Include library files value');

    expect(checkboxesLabelTexts[7]).toBe('Include BESS graph', 'render Include BESS graph checkbox label');
    expect(checkboxesValues[7]).toBeTruthy('valid Include BESS graph value');

    expect(checkboxesLabelTexts[8]).toBe('Include journalctl files', 'render Include journalctl files checkbox label');
    expect(checkboxesValues[8]).toBeTruthy('valid Include journalctl files value');

    expect(checkboxesLabelTexts[9]).toBe('Include linux logs', 'render Include linux logs checkbox label');
    expect(checkboxesValues[9]).toBeTruthy('valid Include linux logs value');

    await loader.getHarness(MatButtonHarness.with({
      text: 'Download Logs'
    }));
  });

  it('should submit Download Logs form', async () => {
    const [siteSelect, serverSelect] = await loader.getAllHarnesses(MatSelectHarness.with({
      ancestor: 'form#download-logs-form',
    }));

    // Select a site then fetch servers
    await siteSelect.open();

    await siteSelect.clickOptions({
      text: mockSites.sites[1].config.name
    });

    await siteSelect.close();

    const serversRequest = httpMockClient.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/sites/${mockSites.sites[1].config.identifier}/servers`
    });

    serversRequest.flush(mockServers);
    fixture.detectChanges();

    // Select a server
    await serverSelect.open();

    await serverSelect.clickOptions({
      text: mockServers.servers[2].name
    });

    await serverSelect.close();

    // Customize logs filters
    const checkboxes = await loader.getAllHarnesses(MatCheckboxHarness.with({
      ancestor: 'form#download-logs-form'
    }));

    const [, runtimeConfigurationFilesCheckbox, , , tempFilesCheckbox, etcFilesCheckbox] = checkboxes;

    await Promise.all(
      [runtimeConfigurationFilesCheckbox, tempFilesCheckbox, etcFilesCheckbox].map(checkbox => checkbox.uncheck())
    )

    const checkboxesValues = await Promise.all(
      checkboxes.map(checkbox => checkbox.isChecked())
    );

    // Download logs
    const downloadLogsButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Download Logs'
    }));

    await downloadLogsButton.click();

    httpMockClient.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/logs/tapestry_logs?site-id=${mockSites.sites[1].config.identifier}&server-id=${mockServers.servers[2].identifier}&skiptemp=${!checkboxesValues[4]}&skipetc=${!checkboxesValues[5]}&skipconfrun=${!checkboxesValues[1]}`
    });
  });

  it('should hide logging form when disabled', async () => {
    const disableCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      label: 'Enable'
    }));
    await disableCheckbox.uncheck();
    fixture.detectChanges();

    const userInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=user]');
    const passwordInput: HTMLInputElement = el.querySelector('.mat-input-element[formcontrolname=password]');
    expect(userInput).toBeNull();
    expect(passwordInput).toBeNull();

    const saveButton: HTMLButtonElement = el.querySelector('button[type=submit]');
    expect(saveButton).not.toBeNull();
  })
});
