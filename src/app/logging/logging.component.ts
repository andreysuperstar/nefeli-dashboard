import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatSelectChange } from '@angular/material/select';

import { Site } from 'rest_client/pangolin/model/site';
import { Sites } from 'rest_client/pangolin/model/sites';
import { RESTServer } from 'rest_client/pangolin/model/rESTServer';

import { ServersService } from 'rest_client/pangolin/api/servers.service';

import { UserService } from 'src/app/users/user.service';
import { ClusterService } from '../home/cluster.service';
import { LoggingConfig } from 'rest_client/pangolin/model/loggingConfig';
import { LoggingConfigStatus } from 'rest_client/pangolin/model/loggingConfigStatus';
import { LogsService } from 'rest_client/pangolin/api/logs.service';
import { AlertService, AlertType } from '../shared/alert.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'nef-logging',
  templateUrl: './logging.component.html',
  styleUrls: ['./logging.component.less']
})
export class LoggingComponent implements OnInit, OnDestroy {

  private _loggingForm: FormGroup;
  private _isSystemAdmin = false;
  private _downloadLogsForm: FormGroup;
  private _sites: Site[];
  private _servers: RESTServer[];
  private _loggingSubscription: Subscription;
  private _siteServersSubscription: Subscription;
  private _logsSubscription: Subscription;
  private _subscription = new Subscription();

  constructor(
    private _userService: UserService,
    private _alertService: AlertService,
    private _clusterService: ClusterService,
    private _logsService: LogsService,
    private _fb: FormBuilder,
    private _serversService: ServersService
  ) { }

  public ngOnInit() {
    this.isSystemAdmin$
      .pipe(take(1))
      .subscribe((isSystemAdmin: boolean) => {
        this._isSystemAdmin = isSystemAdmin;

        this.initLoggingForm();
      });

    this.initDownloadLogsForm();

    const subscription = this._clusterService
      .getSites()
      .subscribe(({ sites: sites }: Sites) => {
        this._sites = sites;

        this._downloadLogsForm
          .get('site')
          .enable();
      });

    this._subscription.add(subscription);
  }

  public ngOnDestroy() {
    this._loggingSubscription?.unsubscribe();
    this._siteServersSubscription?.unsubscribe();
    this._logsSubscription?.unsubscribe();

    this._subscription.unsubscribe();
  }

  private initLoggingForm() {
    this._loggingForm = this._fb.group({
      logging: '',
      host: ['', Validators.required],
      user: '',
      password: '',
    });

    const subscription = this._clusterService.getLoggingConfig().subscribe((config: LoggingConfig) => {

      if (config.endpoint?.host) {
        this._loggingForm.get('host').setValue(config.endpoint.host);
        this._loggingForm.get('user').setValue(config.endpoint.user);
        this._loggingForm.get('password').setValue(config.endpoint.password);
      }

      this._loggingForm.get('logging').setValue((config.status === LoggingConfigStatus.ENABLED) ? true : false);
    });
    this._subscription.add(subscription);

    if (!this._isSystemAdmin) {
      this._loggingForm.disable();
    }
  }

  private initDownloadLogsForm() {
    this._downloadLogsForm = this._fb.group({
      site: {
        value: undefined,
        disabled: true
      },
      server: {
        value: undefined,
        disabled: true
      },
      filters: this._fb.group({
        configurationFiles: true,
        runtimeConfigurationFiles: true,
        dataFiles: true,
        logFiles: true,
        tempFiles: true,
        etcFiles: true,
        libraryFiles: true,
        bessGraph: true,
        journalctlFiles: true,
        linuxLogs: true
      })
    });
  }

  public onSiteSelectionChange({
    value: siteID
  }: MatSelectChange) {
    this._siteServersSubscription?.unsubscribe();

    const serverControl = this._downloadLogsForm.get('server') as FormControl;

    if (!siteID) {
      serverControl.reset();
      serverControl.disable();

      return;
    }

    this._siteServersSubscription = this._serversService
      .getClusterServers(siteID)
      .subscribe(({ servers }) => {
        this._servers = servers;

        serverControl.enable();
      });
  }

  public submitLoggingForm() {
    const { invalid, pristine } = this._loggingForm;

    if (invalid || pristine) {
      return;
    }

    const { host, password, user, logging } = this._loggingForm.value;

    const config: LoggingConfig = {
      endpoint: {
        host,
        password,
        user
      },
      status: logging ? LoggingConfigStatus.ENABLED : LoggingConfigStatus.DISABLED
    };

    this._loggingSubscription?.unsubscribe();

    this._loggingSubscription = this._clusterService
      .putLoggingConfig(config)
      .subscribe({
        next: () => this._alertService.info(AlertType.INFO_UPDATE_SYSTEM_CONFIGURATION),
        error: () => this._alertService.error(AlertType.ERROR_UPDATE_SYSTEM_CONFIGURATION)
      });
  }

  public submitDownloadLogsForm() {
    const { invalid, pristine } = this._downloadLogsForm;

    this._logsSubscription?.unsubscribe();

    if (invalid || pristine) {
      return;
    }

    const {
      site: siteID,
      server: serverID,
      filters
    }: {
      site: string;
      server: string;
      filters: {
        configurationFiles: boolean;
        runtimeConfigurationFiles: boolean;
        dataFiles: boolean;
        logFiles: boolean;
        tempFiles: boolean;
        etcFiles: boolean;
        libraryFiles: boolean;
        bessGraph: boolean;
        journalctlFiles: boolean;
        linuxLogs: boolean;
      }
    } = this._downloadLogsForm.value;

    const [
      skipConfigurationFiles,
      skipRuntimeConfigurationFiles,
      skipDataFiles,
      skipLogFiles,
      skipTempFiles,
      skipEtcFiles,
      skipLibraryFiles,
      skipBESSGraph,
      skipJournalctlFiles,
      skipLinuxLogs
    ] = Object
      .values(filters)
      .map(isChecked => isChecked ? undefined : true);

    this._logsSubscription = this._logsService
      .getLogs(
        siteID,
        serverID,
        skipConfigurationFiles,
        skipDataFiles,
        skipLogFiles,
        skipTempFiles,
        skipEtcFiles,
        skipRuntimeConfigurationFiles,
        skipLibraryFiles,
        undefined,
        skipBESSGraph,
        skipJournalctlFiles,
        skipLinuxLogs
      )
      .subscribe({
        error: () => {
          this._alertService.error(AlertType.ERROR_GET_LOGS);
        }
      });
  }

  public get loggingForm(): FormGroup {
    return this._loggingForm;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get isEnabled(): boolean {
    return this._loggingForm.get('logging').value;
  }

  public get downloadLogsForm(): FormGroup {
    return this._downloadLogsForm;
  }

  public get sites(): Site[] {
    return this._sites;
  }

  public get servers(): RESTServer[] {
    return this._servers;
  }
}
