import { UserService } from 'src/app/users/user.service';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { FileManagerComponent } from './file-manager.component';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatDividerModule } from '@angular/material/divider';
import { MatPaginatorModule } from '@angular/material/paginator';

import { ReactiveFormsModule } from '@angular/forms';
import { TableComponent } from 'src/app/shared/table/table.component';
import { AvatarComponent } from 'src/app/shared/avatar/avatar.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FileService } from './file.service';
import { AlertService } from 'src/app/shared/alert.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { DOCUMENT, DecimalPipe } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { cloneDeep } from 'lodash-es';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH } from 'rest_client/mneme/variables';
import { environment } from 'src/environments/environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { User } from 'rest_client/heimdallr/model/user';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MnemeBFState } from 'rest_client/mneme/model/mnemeBFState';
import { MnemeFileInfo } from 'rest_client/mneme/model/mnemeFileInfo';

enum UserType {
  SystemAdmin,
  SystemUser
}

const mockUsers: User[] = [
  {
    username: 'SystemAdmin',
    email: 'sysadmin@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin']
    }
  },
  {
    username: 'SystemUser',
    email: 'sysuser@nefeli.io',
    roles: {
      scope: 'system',
      id: '-3',
      roles: ['user']
    }
  }
];

describe('FileManagerComponent', () => {
  let component: FileManagerComponent;
  let fixture: ComponentFixture<FileManagerComponent>;
  let httpTestingController: HttpTestingController;
  let document: Document;
  let userService: UserService;
  let el: HTMLElement;

  const mockFiles = [
    {
      name: 'vsrx_img',
      desc: 'Juniper vSRX Image',
      bfi: {
        state: MnemeBFState.Ready
      }
    },
    {
      name: 'arista_img',
      desc: 'Arista Router Image',
      bfi: {
        state: MnemeBFState.Failed
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        MatInputModule,
        MatProgressSpinnerModule,
        MatCardModule,
        MatTableModule,
        MatCheckboxModule,
        RouterTestingModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        MatDialogModule,
        MatDividerModule,
        MatPaginatorModule,
        MatTooltipModule
      ],
      declarations: [
        FileManagerComponent,
        TableComponent,
        AvatarComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            close: jasmine.createSpy('close')
          }
        },
        FileService,
        AlertService,
        DecimalPipe,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        }
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(FileManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/files?status=true`);

    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    req.flush({
      files: cloneDeep(mockFiles)
    });

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should request for files on load and display', () => {
    expect(component.dataSource[0].name).toBe('vsrx_img');
    expect(component.dataSource[0].description).toBe('Juniper vSRX Image');
    expect(component.dataSource[1].name).toBe('arista_img');
    expect(component.dataSource[1].description).toBe('Arista Router Image');
  });

  it('should upload a new file', () => {
    const buttonEl: HTMLButtonElement = fixture.debugElement.query(
      By.css('.add-hide-button-wrapper button')
    ).nativeElement;

    // should have upload form hidden by default
    expect(buttonEl.textContent).toBe('Add New File');
    expect(el.querySelectorAll('mat-form-field').length).toBe(0);

    // display upload form
    buttonEl.click();
    fixture.detectChanges();
    expect(el.querySelectorAll('mat-form-field').length).toBe(2);
    expect(el.querySelector('.file-button').textContent.trim()).toBe('Select File');
    expect(el.querySelector('.button-description').textContent).toBe('Select file to upload');
    expect(el.querySelector('button[form="tenant-form"]').textContent).toBe('Upload');
    expect(buttonEl.textContent).toBe('Hide');

    // attempt to upload file
    component.setFileName({
      target: {
        files: [{
          name: 'test.img'
        }]
      }
    } as any);
    fixture.detectChanges();
    component.formGroup.patchValue({
      name: 'test-nf',
      description: 'Test NF Image',
      distributionDebian: true
    });
    component.submitForm();

    const uploadReq = httpTestingController.expectOne(`${environment.restServer}/v1/files/test-nf`);
    expect(uploadReq.request.method).toBe('PUT');
    expect(uploadReq.request.body.get('description')).toBe('Test NF Image');
    expect(uploadReq.request.body.get('file')).toBeTruthy();
    expect(uploadReq.request.body.get('isDistroDeb')).toBe('true');

    const mockFile: MnemeFileInfo = {
      name: 'vsrx_img',
      desc: 'Juniper vSRX Image',
      bfi: {
        state: MnemeBFState.Storing
      }
    };

    uploadReq.flush(mockFile);

    // new file is displayed on success
    expect(component.dataSource[2].name).toBe('test-nf');
    expect(component.dataSource[2].description).toBe('Test NF Image');
    expect(component.dataSource[2].status).toBe(mockFile.bfi.state, 'initial file status');

    const filesRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/files?status=true`
    });

    expect(component.uploadInProgress).toBe(false);

    filesRequest.flush({
      files: [
        ...mockFiles,
        {
          ...mockFile,
          bfi: {
            state: MnemeBFState.Ready
          }
        }
      ]
    });

    expect(component.dataSource[0].status).toBe(MnemeBFState.Ready, 'updated file status');
  });

  it('should remove a file', fakeAsync(() => {
    spyOn<any>(component, 'removeFile').and.callThrough();

    const fileRows: DebugElement[] = fixture.debugElement.queryAll(By.css('mat-row'));

    expect(fileRows.length).toBe(2, 'render 2 file rows before removal');

    expect(
      fileRows[0].query(By.css('mat-cell')).nativeElement.textContent.trim()
    ).toBe('vsrx_img');

    expect(
      fileRows[1].query(By.css('mat-cell')).nativeElement.textContent.trim()
    ).toBe('arista_img');

    const removeButtonEl: HTMLButtonElement = fileRows[0].query(
      By.css('button.remove')
    ).nativeElement;

    removeButtonEl.click();

    expect(component['removeFile']).toHaveBeenCalled();
    tick();

    const confirmationDialogEl: HTMLUnknownElement = document.querySelector(
      'nef-confirmation-dialog'
    );

    expect(confirmationDialogEl).not.toBeNull('render Confirmation dialog');

    /* TODO(andrew): text is not showed for paragraph and action button
       and dialog is not closed therefore could not test a request */
  }));

  it('should hide add button for system user', () => {
    userService.setUser(mockUsers[UserType.SystemUser]);

    fixture.detectChanges();

    const addFileButtonEl: DebugElement = fixture.debugElement.query(
      By.css('.add-hide-button-wrapper button')
    );

    expect(addFileButtonEl).toBeNull();
  });

  it('should display file table with defined columns', () => {
    const table = el.querySelector('mat-table');
    expect(table).not.toBeNull();
    const headers = table.querySelectorAll('mat-header-cell');
    expect(headers.length).toBe(4);
    expect(headers[0].textContent.trim()).toBe('name');
    expect(headers[1].textContent.trim()).toBe('description');
    expect(headers[2].textContent.trim()).toBe('status');
    expect(headers[3].textContent.trim()).toBe('actions');
  });

  it('should display status badge', () => {
    const rows = el.querySelectorAll('mat-table mat-row');
    expect(rows.length).toBe(2);
    const readyStatusBadge = rows[0].querySelector('.badge');
    const failedStatusBadge = rows[1].querySelector('.badge');
    expect(readyStatusBadge).not.toBeNull();
    expect(readyStatusBadge.textContent.trim()).toBe('Ready');
    expect(readyStatusBadge.classList.contains('green')).toBeTruthy();

    expect(failedStatusBadge).not.toBeNull();
    expect(failedStatusBadge.textContent.trim()).toBe('Failed');
    expect(failedStatusBadge.classList.contains('red')).toBeTruthy();
  });
});
