import { UserService } from 'src/app/users/user.service';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { delay, filter, retryWhen, startWith, switchMap, switchMapTo, tap } from 'rxjs/operators';
import {
  TableActions,
  TableActionButton,
  TableComponent,
  TableActionEvent,
  ColumnType,
  Column
} from 'src/app/shared/table/table.component';
import { FileService, MnemeFile, FileInfo } from './file.service';
import { AlertType, AlertService } from 'src/app/shared/alert.service';
import { ConfirmationDialogComponent, ConfirmationDialogData } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { interval, Observable, Subscription } from 'rxjs';
import { BadgeColor } from '../shared/table/table.component';
import { MnemeFileInfo } from 'rest_client/mneme/model/mnemeFileInfo';

interface FileRow {
  name: string;
  description: string;
  status: string;
  actions: Array<TableActionButton>;
}

const POLL_INTERVAL = 5000;

const ACTIONS = [{
  text: 'Remove',
  action: TableActions.Remove,
  icon: 'trash-blue'
}];

const DIALOG_CONFIG: MatDialogConfig<ConfirmationDialogData> = {
  width: '510px',
  restoreFocus: false
};

const COLUMNS: Column[] = [
  {
    name: 'name'
  },
  {
    name: 'description'
  },
  {
    name: 'status',
    type: ColumnType.BADGE,
    classes: [
      ['Ready', BadgeColor.green],
      ['Storing', BadgeColor.blue],
      ['Deleting', BadgeColor.yellow],
      ['Failed', BadgeColor.red]
    ]
  },
  {
    name: 'actions',
    type: ColumnType.ACTIONS
  }
];

@Component({
  selector: 'nef-file-manager',
  templateUrl: './file-manager.component.html',
  styleUrls: ['./file-manager.component.less']
})
export class FileManagerComponent implements OnInit, OnDestroy {
  private _formGroup: FormGroup;
  private _fileForUpload: File;
  private _files: MnemeFileInfo[] = [];
  private _dataSource = new Array<FileRow>();
  private _showNewFile = false;
  private _uploadInProgress = false;
  private _filesSubscription: Subscription;

  @ViewChild('fileTable', { static: true }) private _fileTable: TableComponent;

  constructor(
    private _dialog: MatDialog,
    private _fb: FormBuilder,
    private _fileService: FileService,
    private _alertService: AlertService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this.streamFiles();
    this.initForm();
    this.sortTable();
  }

  public ngOnDestroy() {
    this.stopFilesStream();
  }

  private initForm() {
    this._formGroup = this._fb.group({
      name: ['', Validators.required],
      description: [''],
      file: [],
      distributionDebian: ''
    });
  }

  private streamFiles() {
    this._filesSubscription = interval(POLL_INTERVAL)
      .pipe(
        startWith(undefined as string),
        switchMapTo(this._fileService.getFiles()),
        tap(files => {
          if (files) {
            this._files = files.files;

            this.setDataSource();
          }
        }),
        retryWhen(errors => errors.pipe(
          delay(POLL_INTERVAL)
        ))
      )
      .subscribe();
  }

  private stopFilesStream() {
    this._filesSubscription?.unsubscribe();
  }

  private sortTable() {
    this._fileTable.setSortDetails({
      id: 'name',
      start: 'asc',
      disableClear: false
    });
  }

  private setDataSource() {
    this._dataSource = [];

    this._files?.forEach((file: MnemeFileInfo) => {
      this._dataSource.push({
        name: file.name,
        description: file.desc,
        status: file.bfi.state,
        actions: ACTIONS
      } as FileRow);
    });
  }

  public setFileName(ev: Event) {
    if (!ev.target['files'][0]) {
      return;
    }

    this._fileForUpload = ev.target['files'][0];
  }

  public submitForm() {
    const { value, invalid } = this._formGroup;

    if (invalid) {
      return;
    }

    const { name, description, distributionDebian } = value;

    this._uploadInProgress = true;

    this.stopFilesStream();

    this._fileService.uploadFile(name, description, this._fileForUpload, distributionDebian).subscribe(
      (file: MnemeFileInfo) => {
        this._files?.push(file);

        this._dataSource.push({
          name,
          description,
          status: file.bfi.state,
          actions: ACTIONS
        } as FileRow);

        this.streamFiles();

        this._uploadInProgress = false;

        this._alertService.info(AlertType.INFO_FILE_UPLOADED);
      },
      () => {
        this._uploadInProgress = false;
        this._alertService.error(AlertType.ERROR_UPLOAD_FILE);
      }
    );
  }

  private removeFile({ name: label }: FileRow) {
    const data: ConfirmationDialogData = {
      description: `This will immediately remove the file <strong>${label}</strong>.<br>
      <br>Warning! Do not delete if an active Network Function is still referencing this file. Please proceed with caution.`,
      action: 'Remove File'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean),
        tap(this.stopFilesStream),
        switchMap(() => this._fileService.removeFile(label))
      )
      .subscribe(
        () => {
          const index: number = this._files?.findIndex(
            (file: MnemeFileInfo): boolean => file.name === label
          );

          this._files?.splice(index, 1);

          this.setDataSource();

          this._alertService.info(AlertType.INFO_FILE_REMOVED);
        },
        ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_REMOVE_FILE, message);
        },
        () => {
          this.streamFiles();
        }
      );
  }

  public onActionClick(event: TableActionEvent) {
    if (event.type === TableActions.Remove) {
      this.removeFile(event.data as FileRow);
    }
  }

  public get formGroup(): FormGroup {
    return this._formGroup;
  }

  public get fileName(): string {
    return this._fileForUpload ? this._fileForUpload.name : '';
  }

  public get columns(): any[] {
    return COLUMNS;
  }

  public get dataSource(): FileRow[] {
    return this._dataSource;
  }

  public get showNewFile(): boolean {
    return this._showNewFile;
  }

  public set showNewFile(val: boolean) {
    this._showNewFile = val;
  }

  public get uploadInProgress(): boolean {
    return this._uploadInProgress;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }
}
