import { TestBed } from '@angular/core/testing';

import { FileService, MnemeFile, FileInfo } from './file.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BASE_PATH } from 'rest_client/mneme/variables';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { MnemeFileInfos } from 'rest_client/mneme/model/mnemeFileInfos';
import { MnemeFileInfo } from 'rest_client/mneme/model/mnemeFileInfo';
import { MnemeBFState } from 'rest_client/mneme/model/mnemeBFState';

describe('FileService', () => {
  let service: FileService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        }
      ]
    });

    service = TestBed.inject(FileService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should upload file', () => {
    service.uploadFile('file-label', 'file-source', new Blob(), false).subscribe();
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/files/file-label`);
    expect(req.request.method).toBe('PUT');
    expect(req.request.body.get('description')).toBe('file-source');
    expect(req.request.body.get('file').name).toBe('blob');
  });

  it('should get files', () => {
    service.getFiles().subscribe((files: MnemeFileInfos) => {
      expect(files.files[0].name).toBe('vsrx_img');
      expect(files.files[0].desc).toBe('Juniper vSRX Image');
      expect(files.files[1].name).toBe('arista_img');
      expect(files.files[1].desc).toBe('Arista Router Image');
      expect(files.files.length).toBe(2);
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/files?status=true`);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');
    req.flush({
      files: [
        { name: 'vsrx_img', desc: 'Juniper vSRX Image', bfi: { state: MnemeBFState.Ready } },
        { name: 'arista_img', desc: 'Arista Router Image', bfi: { state: MnemeBFState.Ready } }
      ]
    });
  });

  it('should get file info', () => {
    service.getFileInfo('file-id').subscribe((info: MnemeFileInfo) => {
      expect(info.desc).toBe('file-id');
      expect(info.name).toBe('my test file');
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/files/file-id`);
    expect(req.request.method).toBe('GET');
    expect(req.request.responseType).toBe('json');

    req.flush(<MnemeFileInfo> {
      desc: 'file-id',
      name: 'my test file'
    });
  });
});
