import { Injectable } from '@angular/core';
import { FilesService as MnemeService } from 'rest_client/mneme/api/files.service';
import { Observable } from 'rxjs';
import { MnemeFileInfo } from 'rest_client/mneme/model/mnemeFileInfo';
import { MnemeFileInfos } from 'rest_client/mneme/model/mnemeFileInfos';

// tslint:disable-next-line: no-empty-interface
export interface FileInfo extends MnemeFileInfo { }

export interface MnemeFile {
  label: string;
  info: FileInfo;
}

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(
    private _restMnemeService: MnemeService
  ) { }

  public uploadFile(label: string, description: string, file: Blob, isDistroDeb: boolean): Observable<any> {
    return this._restMnemeService.uploadFile(label, description, file, isDistroDeb);
  }

  public getFiles(): Observable<MnemeFileInfos> {
    return this._restMnemeService.getFiles(true);
  }

  public getFileInfo(id: string): Observable<FileInfo> {
    return this._restMnemeService.getInfo(id);
  }

  public removeFile(id: string): Observable<any> {
    return this._restMnemeService.deleteFile(id);
  }

}
