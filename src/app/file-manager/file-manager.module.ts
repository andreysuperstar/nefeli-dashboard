import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { SharedModule } from '../shared/shared.module';

import { FileManagerRoutingModule } from './file-manager-routing.module';

import { FileManagerComponent } from './file-manager.component';
import { MatCheckboxModule } from '@angular/material/checkbox';

@NgModule({
  declarations: [FileManagerComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatProgressSpinnerModule,
    SharedModule,
    FileManagerRoutingModule,
    MatCheckboxModule
  ]
})
export class FileManagerModule { }
