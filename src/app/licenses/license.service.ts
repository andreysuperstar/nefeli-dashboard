import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RESTLicense } from 'rest_client/pangolin/model/rESTLicense';
import { License } from 'rest_client/pangolin/model/license';
import { LicenseConfig } from 'rest_client/pangolin/model/licenseConfig';
import { RESTLicenses } from 'rest_client/pangolin/model/rESTLicenses';
import { LicensePool } from 'rest_client/pangolin/model/licensePool';
import { RESTLicensePools } from 'rest_client/pangolin/model/rESTLicensePools';
import { LicensesService } from 'rest_client/pangolin/api/licenses.service';

export interface LicensePoolsResponse {
  pools: {
    [key: string]: LicensePool;
  };
}

export interface LicensesResponse {
  licenses: License[];
}

export interface UILicensePool extends LicensePool {
  disableSelect?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class LicenseService {

  constructor(
    private _restLicensesService: LicensesService
  ) { }

  public getLicensePools(): Observable<LicensePool[]> {
    return this._restLicensesService.getLicensePools().pipe(
      map((response: RESTLicensePools) => {
        return Object.values(response.licensePools);
      })
    );
  }

  public postLicensePool(name: string): Observable<LicensePool> {
    return this._restLicensesService.postLicensePool(name);
  }

  public getLicensePool(id: string): Observable<UILicensePool> {
    return this._restLicensesService.getLicensePool(id);
  }

  public deleteLicensePool(id: string): Observable<string> {
    return this._restLicensesService.deleteLicensePool(id);
  }

  public postLicense(poolId: string, licenseConfig: LicenseConfig): Observable<License> {
    return this._restLicensesService.postLicense(poolId, licenseConfig);
  }

  public getLicensesInPool(id: string): Observable<RESTLicense[]> {
    return this._restLicensesService.getLicensesFromPool(id).pipe(
      map((response: RESTLicenses) => response.licenses)
    );
  }

  public getLicense(id: string, poolId: string): Observable<RESTLicense> {
    return this._restLicensesService.getLicense(poolId, id);
  }

  public deleteLicense(id: string, poolId: string): Observable<any> {
    return this._restLicensesService.deleteLicense(poolId, id);
  }

  public grantLicense(licenseId: string, fromPoolId: string, toPoolId: string): Observable<string> {
    return this._restLicensesService.postLicenseOwner(fromPoolId, licenseId, toPoolId);
  }

  public revokeLicense(licenseId: string, poolId: string): Observable<string> {
    return this._restLicensesService.deleteLicenseOwner(poolId, licenseId);
  }
}
