import { Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AlertService, AlertType } from '../../shared/alert.service';
import { NetworkFunction, NFCatalog } from '../../pipeline/network-function.model';
import { NetworkFunctionService } from '../../pipeline/network-function.service';
import { LicenseService, UILicensePool } from '../license.service';
import { AddLicenseDialogData } from '../licenses.component';
import { LicenseConfig } from 'rest_client/pangolin/model/licenseConfig';
import { LoggerService } from 'src/app/shared/logger.service';

interface NF {
  id: string;
  name: string;
  vendor: string;
}

@Component({
  selector: 'nef-add-license-dialog',
  templateUrl: './add-license-dialog.component.html',
  styleUrls: ['./add-license-dialog.component.less']
})
export class AddLicenseDialogComponent implements OnInit, OnDestroy {

  private _licenseForm: FormGroup;
  private _licenseFiles: File[] = [];
  private _pools: UILicensePool[] = [];
  private _nfs: NF[] = [];
  private _licensesSubscription: Subscription;
  private _subscription = new Subscription();
  private _successfulLicensePosts = 0;

  constructor(
    private _fb: FormBuilder,
    private _dialogRef: MatDialogRef<AddLicenseDialogComponent>,
    private _alertService: AlertService,
    private _licensingService: LicenseService,
    private _nfsService: NetworkFunctionService,
    private _log: LoggerService,
    @Inject(MAT_DIALOG_DATA) private _data: AddLicenseDialogData,
  ) { }

  public ngOnInit() {
    this._pools = this._data.pools;
    const subscription = this._nfsService.getSystemNFs().subscribe(
      (nfs: NFCatalog) => {
        Object.keys(nfs).forEach((vendor: string) => {
          nfs[vendor].forEach((nf: NetworkFunction) => {
            this._nfs.push({
              id: nf.local.id,
              name: nf.name,
              vendor: vendor
            });
          });
        });
      }
    );

    this._subscription.add(subscription);
    this.initLicenseForm();
  }

  public ngOnDestroy() {
    this._licensesSubscription?.unsubscribe();
    this._subscription.unsubscribe();
  }

  private initLicenseForm() {
    this._licenseForm = this._fb.group(
      {
        name: ['', Validators.required],
        pool: ['', Validators.required],
        startDate: [''],
        endDate: [''],
        nf: ['', Validators.required],
        files: ['']
      }
    );
  }

  private postLicenseRequest(licenseData: string, fileName: string) {
    const { value: { name, startDate, endDate, nf, pool } }: {
      value: {
        name: string;
        startDate: Date;
        endDate: Date;
        nf: NF;
        pool: string;
      }
    } = this.licenseForm;

    let asciiLicenseData;
    try {
      asciiLicenseData = btoa(licenseData);
    } catch (e) {
      this._licenseForm.get('files').setErrors({
        invalid: true,
        fileName
      });
      this._log.error(e);
    }

    if (!asciiLicenseData) {
      return;
    }

    const licenseConfig: LicenseConfig = {
      nfId: nf.id,
      name: name,
      dateAdded: new Date().toISOString(),
      startDate: startDate ? startDate.toISOString() : '',
      endDate: endDate ? endDate.toISOString() : '',
      data: asciiLicenseData
    };

    const subscription = this._licensingService
      .postLicense(pool, licenseConfig)
      .subscribe(
        () => {
          // TODO(eric): handle case where some files succeed and some fail
          this._successfulLicensePosts++;

          if (this._successfulLicensePosts >= this._licenseFiles?.length) {
            // all licenses have been successfully uploaded
            this._dialogRef.close(true);
            this._alertService.info(AlertType.INFO_CREATE_LICENSE_SUCCESS);
          }
        },
        () => this._alertService.error(AlertType.ERROR_CREATE_LICENSE)
      );

    this._licensesSubscription.add(subscription);
  }

  public submitLicenseForm() {
    const { invalid, pristine } = this.licenseForm;

    this._licensesSubscription?.unsubscribe();

    if (!this._licenseFiles.length) {
      this.licenseForm.get('files').setErrors({
        required: true
      });
    } else {
      this.licenseForm.get('files').setErrors(undefined);
    }

    if (invalid || pristine) {
      return;
    }

    this._successfulLicensePosts = 0;

    this._licensesSubscription = new Subscription();

    this._licenseFiles.forEach(file => {
      const reader = new FileReader();

      reader.onload = (() => {
        return (ev: ProgressEvent) => {
          const licenseData = ev.target['result'];
          this.postLicenseRequest(licenseData, file.name);
        };
      })();

      reader.readAsText(file);
    });
  }

  public get licenseForm(): FormGroup {
    return this._licenseForm;
  }

  public get pools(): UILicensePool[] {
    return this._pools;
  }

  public get nfs(): NF[] {
    return this._nfs;
  }

  public get licenseFiles(): File[] {
    return this._licenseFiles;
  }

  public uploadLicenseFiles(event: any) {
    if (event.target && event.target.files) {
      this.licenseForm.get('files').setErrors(undefined);
      for (const file of event.target.files) {
        this._licenseFiles.push(file);
      }
    }
  }

  public removeLicenseFile(index: number) {
    this._licenseFiles.splice(index, 1);
  }
}
