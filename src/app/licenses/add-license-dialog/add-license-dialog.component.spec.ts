import { AddLicenseDialogData } from '../licenses.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RequestUrlInterceptor } from '../../http-interceptors/request-url.interceptor';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { AddLicenseDialogComponent } from './add-license-dialog.component';
import { environment } from '../../../environments/environment';
import { AlertService } from '../../shared/alert.service';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';
import { LicenseService } from '../license.service';

const dialogSpy = {
  close: jasmine.createSpy('close')
};

const mockMatDialogData: AddLicenseDialogData = {
  pools: [{
    identifier: 'pool-id1',
    name: 'pool name 1',
    licenses: {},
    licenseOwners: {}
  }]
};

const mockFileEvent = {
  target: {
    files: [{ name: 'file1' }]
  }
};

const nfsMockResponse: RESTNFCatalog = {
  descriptors: [
    {
      identifier: "pan-fw",
      "name": "Firewall"
    },
    {
      identifier: "arista-router",
      "name": "Router",
      components: {
        datapath: {
          vendor: 'arista'
        }
      }
    },
    {
      identifier: "nef-nat",
      "name": "NAT",
      components: {
        datapath: {
          vendor: 'arista'
        }
      }
    },
    {
      identifier: "nef-match",
      "name": "Exact Match",
      components: {
        datapath: {
          vendor: 'arista'
        }
      }
    }
  ]
};

describe('AddLicenseDialogComponent', () => {
  let component: AddLicenseDialogComponent;
  let fixture: ComponentFixture<AddLicenseDialogComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatDialogModule,
        MatExpansionModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatDatepickerModule,
        MatListModule,
        MatNativeDateModule
      ],
      declarations: [AddLicenseDialogComponent],
      providers: [
        AlertService,
        LicenseService,
        {
          provide: MatDialogRef,
          useValue: dialogSpy
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: MAT_DIALOG_DATA,
          useValue: mockMatDialogData
        },
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddLicenseDialogComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    dialogSpy.close.calls.reset();
    fixture.detectChanges();

    const reqNfs = httpTestingController.expectOne(`${environment.restServer}/v1/nfs`);
    expect(reqNfs.request.method).toBe('GET');
    reqNfs.flush(nfsMockResponse);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    expect(el.querySelector('[mat-dialog-title]').textContent.trim()).toBe('Add new license');
  });

  it('should display license form', () => {
    expect(el.querySelector('#license-form')).not.toBeNull();
  });

  it('should have form controls', () => {
    expect(el.querySelector('input[type=file]')).not.toBeNull();
    expect(el.querySelector('[formcontrolname=name]')).not.toBeNull();
    expect(el.querySelector('[formcontrolname=nf]')).not.toBeNull();
    expect(el.querySelector('[formcontrolname=pool]')).not.toBeNull();
    expect(el.querySelector('[formcontrolname=startDate]')).not.toBeNull();
    expect(el.querySelector('[formcontrolname=endDate]')).not.toBeNull();
  });

  it('should display list of files', () => {
    expect(el.querySelector('mat-list')).not.toBeNull();
  });

  it('should render system nfs', () => {
    expect(component.nfs.length).toBe(4);
    expect(component.nfs[0].id).toBe('pan-fw');
    expect(component.nfs[0].vendor).toBe('Network Function');
    expect(component.nfs[1].id).toBe('arista-router');
  });

  it('should render license pools', () => {
    expect(component.pools.length).toBe(1);
    expect(component.pools[0].name).toBe('pool name 1');
  });

  it('should display list of files', () => {
    component.uploadLicenseFiles(mockFileEvent);
    fixture.detectChanges();
    const items = el.querySelectorAll('mat-list mat-list-item');
    expect(items.length).toBe(1);
  });

  it('should delete files from the list', () => {
    component.uploadLicenseFiles(mockFileEvent);
    fixture.detectChanges();
    const button: HTMLButtonElement = el.querySelector('mat-list mat-list-item .mat-button');
    button.click();
    fixture.detectChanges();
    expect(el.querySelectorAll('mat-list mat-list-item').length).toBe(0);
  });

  it('should submit form', () => {
    spyOn(component, 'submitLicenseForm').and.callThrough();
    const submitButton: HTMLButtonElement = el.querySelector('button[type=submit]');
    component.licenseForm.markAsDirty();
    submitButton.click();
    expect(component['submitLicenseForm']).toHaveBeenCalled();
  });

  it('should create license on submit form', waitForAsync(() => {
    const nameField: AbstractControl = component.licenseForm.get('name');
    const nfField: AbstractControl = component.licenseForm.get('nf');
    const poolField: AbstractControl = component.licenseForm.get('pool');
    const startDate: AbstractControl = component.licenseForm.get('startDate');
    const endDate: AbstractControl = component.licenseForm.get('endDate');
    const submitButton: HTMLButtonElement = el.querySelector('button[type=submit]');
    const date = new Date('June 5, 2019 10:00:00');

    component['_licenseFiles'] = [new File(['hello world'], 'file')];

    spyOn<any>(component, 'postLicenseRequest').and.callFake((licenseData: string) => {
      const { value: { nf, pool } } = component.licenseForm;
      expect(pool).toBe('pool-id1');
      expect(licenseData).toBe('hello world');
      expect(nf.id).toBe('pan');
    });

    nameField.patchValue('License1');
    nfField.patchValue(component.nfs[0]);
    poolField.patchValue(component.pools[0]);
    startDate.patchValue(date);
    endDate.patchValue(date);
    component.licenseForm.markAsDirty();
    submitButton.click();
  }));
});
