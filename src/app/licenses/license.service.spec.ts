import { TestBed } from '@angular/core/testing';
import { LicenseService, UILicensePool } from './license.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { environment } from '../../environments/environment';
import { RESTLicense } from 'rest_client/pangolin/model/rESTLicense';
import { License } from 'rest_client/pangolin/model/license';
import { LicenseState } from 'rest_client/pangolin/model/licenseState';
import { RESTLicenses } from 'rest_client/pangolin/model/rESTLicenses';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { LicensePool } from 'rest_client/pangolin/model/licensePool';
import { RESTLicensePools } from 'rest_client/pangolin/model/rESTLicensePools';

describe('LicenseService', () => {
  let httpTestingController: HttpTestingController;
  let service: LicenseService;

  const mockLicensePool1: UILicensePool = {
    identifier: 'pool-id1',
    name: 'pool-name1',
    licenses: {},
    licenseOwners: {}
  };

  const mockLicensePool2: UILicensePool = {
    identifier: 'pool-id2',
    name: 'pool-name2',
    licenses: {},
    licenseOwners: {}
  };

  const mockLicensePools: RESTLicensePools = {
    licensePools: [
      mockLicensePool1,
      mockLicensePool2
    ]
  };

  const mockLicense1: License = {
    config: {
      identifier: 'license-id1',
      nfId: 'nf-catalog-id1',
      dateAdded: '1559266898',
      startDate: '1559263898',
      endDate: '1559269898',
      name: 'Arista Router License',
      description: 'description for this license',
      data: ''
    },
    status: {
      state: LicenseState.AVAILABLE,
      rootPool: 'Default Root Pool',
      parentPool: 'Tenant Pool'
    },
  };

  const mockRESTLicense1: RESTLicense = {
    license: mockLicense1,
    owner: 'owner'
  };

  const mockLicense2: License = {
    config: {
      identifier: 'license-id2',
      nfId: 'nf-catalog-id2',
      dateAdded: '1559266898',
      startDate: '1559263898',
      endDate: '1559269898',
      name: 'PAN License',
      description: 'description for this license',
      data: ""
    },
    status: {
      state: LicenseState.AVAILABLE,
      rootPool: 'Root Pool',
      parentPool: 'Root Pool'
    },
  };

  const mockRESTLicense2: RESTLicense = {
    license: mockLicense2,
    owner: 'owner'
  };

  const mockLicenseResponse: RESTLicenses = {
    licenses: [
      mockRESTLicense1,
      mockRESTLicense2
    ]
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        LicenseService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(LicenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get all license pools in system', () => {
    service.getLicensePools().subscribe((pools: LicensePool[]) => {
      expect(pools.length).toBe(2);
      expect(pools[0]).toBe(mockLicensePool1);
      expect(pools[1]).toBe(mockLicensePool2);
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`);
    req.flush(mockLicensePools);

    expect(req.request.method).toBe('GET');
  });

  it('should create a new license pool', () => {
    service.postLicensePool(mockLicensePool1.name).subscribe((response: UILicensePool) => {
      expect(response.name).toBe(mockLicensePool1.name);
    });
    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools?pool-name=${mockLicensePool1.name}`);
    const res: UILicensePool = {
      name: mockLicensePool1.name
    };
    req.flush(res);

    expect(req.request.method).toBe('POST');
  });

  it('should get a specific license pool', () => {
    service.getLicensePool('pool-id').subscribe((pool: LicensePool) => {
      expect(pool).toBe(mockLicensePool2);
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id`);
    req.flush(mockLicensePool2);

    expect(req.request.method).toBe('GET');
  });

  it('should delete an existing license pool', () => {
    service.deleteLicensePool('pool-id').subscribe((response: string) => {
      expect(response).toBe('success');
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id`);
    req.flush('success');

    expect(req.request.method).toBe('DELETE');
  });

  it('should create a new license', () => {
    service.postLicense('pool-id', mockLicense1.config).subscribe((response: License) => {
      expect(response).toEqual(mockRESTLicense1.license);
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses`);
    req.flush(mockRESTLicense1.license);

    expect(req.request.method).toBe('POST');
  });

  it('should get all licenses in pool', () => {
    service.getLicensesInPool('pool-id').subscribe((licenses: RESTLicense[]) => {
      expect(licenses.length).toBe(2);
      expect(licenses[0]).toBe(mockRESTLicense1);
      expect(licenses[1]).toBe(mockRESTLicense2);
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses`);
    req.flush(mockLicenseResponse);

    expect(req.request.method).toBe('GET');
  })

  it('should get a specific license', () => {
    service.getLicense('license-id', 'pool-id').subscribe((license: RESTLicense) => {
      expect(license.license).toBe(mockLicense1);
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses/license-id`);
    req.flush(mockRESTLicense1);

    expect(req.request.method).toBe('GET');
  });

  it('should delete a specific license', () => {
    service.deleteLicense('license-id', 'pool-id').subscribe((response: string) => {
      expect(response).toBe('success');
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses/license-id`);
    req.flush('success');

    expect(req.request.method).toBe('DELETE');
  });

  it('should grant license to another pool', () => {
    service.grantLicense('license-id', 'from-pool-id', 'to-pool-id').subscribe((response: string) => {
      expect(response).toBe('success');
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/from-pool-id/licenses/license-id/owner/to-pool-id`);
    req.flush('success');

    expect(req.request.method).toBe('POST');
  });

  it('should revoke license from current pool', () => {
    service.revokeLicense('license-id', 'pool-id').subscribe((response: string) => {
      expect(response).toBe('success');
    });

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses/license-id/owner`);
    req.flush('success');

    expect(req.request.method).toBe('DELETE');
  });
});
