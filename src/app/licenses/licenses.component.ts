import { KeyValue } from '@angular/common';
import { UserService } from 'src/app/users/user.service';
import { Subscription, forkJoin, Observable, of } from 'rxjs';
import { mergeMap, catchError, pluck, tap, map, mapTo, filter } from 'rxjs/operators';
import { SelectionModel } from '@angular/cdk/collections';
import { ConnectedPosition } from '@angular/cdk/overlay';
import { Component, OnInit, ViewChild, OnDestroy, QueryList, ViewChildren } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { cloneDeep } from 'lodash-es';

import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';
import { LicensePool } from 'rest_client/pangolin/model/licensePool';
import { Services } from 'rest_client/pangolin/model/services';
import { Service } from 'rest_client/pangolin/model/service';
import { ServicesService } from 'rest_client/pangolin/api/services.service';
import { LicenseService, UILicensePool } from './license.service';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { AddLicenseDialogComponent } from './add-license-dialog/add-license-dialog.component';
import { License } from 'rest_client/pangolin/model/license';
import { LicenseState } from 'rest_client/pangolin/model/licenseState';
import { Tenant, TenantService } from '../tenant/tenant.service';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { AlertService, AlertType } from '../shared/alert.service';
import { ConfirmationDialogComponent, ConfirmationDialogData } from '../shared/confirmation-dialog/confirmation-dialog.component';

import { PoolDialogComponent, POOL_DIALOG_CONFIG } from '../shared/pool-dialog/pool-dialog.component';
import { MatCheckbox } from '@angular/material/checkbox';

export interface AddLicenseDialogData {
  pools: UILicensePool[];
}

export interface RevokeLicensesDialogData {
  licenses: LicenseRow[];
}

interface UsedBy {
  tenant: string;
  service: string;
  logicalNF: string;
  nfInstanceID: string;
}

export interface LicenseRow {
  id: string;
  nf: string;
  name: string;
  poolId: string;
  grantingPool: string;
  rootPool: string;
  expiration: {
    date: Date,
    isExpired: boolean
  };
  status: string;
  statusDescription: string;
  usedBy: Partial<UsedBy>;
  enable: boolean;
  selected: boolean;
}

const initialSelection = [];
const allowMultiSelect = true;
const COLUMNS: string[] = [
  'select',
  'nf',
  'name',
  'grantingPool',
  'rootPool',
  'expiration',
  'status',
  'usedBy'
];

const DIALOG_CONFIG: MatDialogConfig<Object> = {
  width: '695px',
  restoreFocus: false
};

@Component({
  selector: 'nef-licenses',
  templateUrl: './licenses.component.html',
  styleUrls: ['./licenses.component.less']
})
export class LicensesComponent implements OnInit, OnDestroy {
  // TODO(eric): figure out why sorting isn't working?
  @ViewChild(MatSort) private _sort: MatSort;
  @ViewChildren('licenseCheckbox') private _licenseCheckboxes: QueryList<MatCheckbox>;

  private _pools = new Array<UILicensePool>();
  private _tenants: Tenant[];
  private _tenantServices = new Map<string, Service[]>();
  private _dataSourceMap = new Map<string, MatTableDataSource<any>>();
  private _subscripion: Subscription;
  private _dialogSubscription: Subscription;
  private _revokeLicenseDialogSubscription: Subscription;
  private _licensePoolSelection = new SelectionModel<UILicensePool>(allowMultiSelect, initialSelection);
  private _hasCheckedLicenses: boolean;
  private _isGrantLicensePoolDropdownOpen = false;
  private _selectedPoolId: string;
  private _licensePoolDropdownPositions: ConnectedPosition[] = [
    {
      originX: 'center',
      originY: 'top',
      overlayX: 'start',
      overlayY: 'top',
      offsetY: 14
    }
  ];

  constructor(
    private _licenseService: LicenseService,
    private _networkFunctionsService: NetworkFunctionsService,
    private _servicesService: ServicesService,
    private _dialog: MatDialog,
    private _tenantService: TenantService,
    private _alertService: AlertService,
    private _userService: UserService
  ) { }

  public ngOnInit() {
    this.getLicensePoolsAndTenants();
  }

  public ngOnDestroy() {
    this._subscripion?.unsubscribe();

    this._dialogSubscription?.unsubscribe();
  }

  /**
   * Get license pools with licenses service and NF names
   */
  private get licensePools$(): Observable<LicensePool[]> {
    return forkJoin([
      this._licenseService.getLicensePools(),
      this._networkFunctionsService.getNfs()
    ])
      .pipe(
        mergeMap(([pools, nfs]) => forkJoin(
          pools.map(pool => {
            pool = cloneDeep(pool);

            const poolLicenseEntries = Object.entries(pool.licenses);

            const poolLicenses = poolLicenseEntries.map(([key, license]) => {
              const nf = nfs.descriptors.find(({ identifier }) => identifier === license.config.nfId);

              license.config.nfId = nf?.name;

              let license$: Observable<[string, License]> = of([key, license]);

              if (license.status.state === LicenseState.INUSE) {
                const {
                  status: {
                    user: {
                      pipeline: serviceID,
                      tenantId: tenantID
                    }
                  }
                } = license;

                const tenantServices = this._tenantServices.get(tenantID);

                const tenantServices$ = tenantServices
                  ? of(tenantServices)
                  : this._servicesService
                    .getServices(tenantID)
                    .pipe(
                      catchError(() => of<Services>({
                        services: []
                      })),
                      pluck('services'),
                      tap(services => {
                        if (services.length) {
                          this._tenantServices.set(tenantID, services);
                        }
                      })
                    );

                const service$ = tenantServices$.pipe(
                  map(services => services.find(({
                    config: { identifier }
                  }) => serviceID === identifier))
                );

                license$ = service$.pipe(
                  tap(service => {
                    license.status.user.pipeline = service?.config?.name;
                  }),
                  mapTo([key, license])
                );
              }

              return license$;
            });

            const licenses$ = poolLicenses.length ? forkJoin(poolLicenses) : of<[string, License][]>([]);

            return licenses$.pipe(
              tap(licenses => {
                licenses.forEach(([key, license]) => pool.licenses[key] = license);
              }),
              mapTo(pool)
            );
          }))
        )
      );
  }

  private getLicensePoolsAndTenants() {
    this._licensePoolSelection.clear();

    this._subscripion?.unsubscribe();

    this._subscripion = forkJoin([
      this.licensePools$,
      this._tenantService.getTenants()
    ])
      .subscribe(([pools, { tenants }]: [UILicensePool[], Tenants]) => {
        this._pools = pools;
        this._tenants = tenants;

        this.createDataSource(pools);
      });
  }

  private createDataSource(pools: UILicensePool[]) {
    pools.forEach((pool: UILicensePool) => {
      const rows: LicenseRow[] = [];

      if (pool.licenses) {
        const licenses: License[] = Object.values(pool.licenses);

        licenses.forEach((license: License) => {
          const parentPool = license.status.parentPool;

          const grantingPool: string = parentPool !== pool.identifier ? parentPool : undefined;

          const enable = pool.licenseOwners[license.config.identifier] ? false : true;

          const endDate: Date = license.config.endDate
            ? new Date(license.config.endDate)
            : undefined;

          const isExpired: boolean = endDate
            ? endDate.getTime() < Date.now()
            : undefined;

          const status: LicenseState = license.status.state;

          let usedBy: Partial<UsedBy>;

          if (status === LicenseState.INUSE) {
            const {
              logicalNf: logicalNF,
              pipeline: service,
              tenantId: tenantID
            } = license?.status?.user ?? {};

            const tenant: Tenant = this._tenants.find(
              ({ identifier }: Tenant): boolean => identifier === tenantID
            );

            usedBy = { tenant: tenant?.name ?? '', service, logicalNF };
          }

          rows.push({
            id: license.config.identifier,
            nf: license.config.nfId,
            name: license.config.name || license.config.identifier,
            poolId: pool.identifier,
            grantingPool,
            rootPool: license.status.rootPool,
            expiration: {
              date: endDate,
              isExpired
            },
            status,
            statusDescription: this.getStatusDescription(license.status.state),
            usedBy,
            enable,
            selected: false
          });
        });
      }

      const dataSource = new MatTableDataSource<any>(rows);
      dataSource.sort = this._sort;
      this._dataSourceMap.set(pool.identifier, dataSource);
    });
  }

  private getStatusDescription(state: LicenseState): string {
    switch (state) {
      case LicenseState.AVAILABLE:
        return 'Available';
      case LicenseState.GRANTED:
        return 'Granted';
      case LicenseState.INUSE:
        return 'In Use';
      case LicenseState.RECLAIMING:
        return 'Reclaiming';
      case LicenseState.ALLOCATEDTOSITE:
        return 'Allocated to Site';
    }

    return state.toString();
  }

  public get dataSourceMap(): Map<string, MatTableDataSource<LicenseRow>> {
    return this._dataSourceMap;
  }

  public get columns(): string[] {
    return COLUMNS;
  }

  public get pools(): Array<UILicensePool> {
    return this._pools;
  }

  public onAddNewLicense(): void {
    const data: AddLicenseDialogData = {
      pools: this.pools
    };

    const dialogRef = this._dialog.open(AddLicenseDialogComponent, { ...DIALOG_CONFIG, data });

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe(() => {
        this.getLicensePoolsAndTenants();
      });
  }

  public onDeleteLicenses() {
    let description = '';
    let canDeleteLicenses = new Array<LicenseRow>();

    if (this.selectedLicenses.length) {
      canDeleteLicenses = this.selectedLicenses.filter((license: LicenseRow) => license.poolId === license.rootPool);
      const cannotDeleteLicenses = this.selectedLicenses.filter((license: LicenseRow) => license.poolId !== license.rootPool);

      if (canDeleteLicenses.length) {
        description += '<strong>Delete the following licenses:</strong>';

        canDeleteLicenses.forEach((license: LicenseRow) => {
          description += `<br><i>${license.name || license.id}</i>`;
        });
      }

      if (cannotDeleteLicenses.length) {
        // tslint:disable-next-line: max-line-length
        description += '<br><br><strong>The following licenses cannot be deleted because they have been granted from their root pool to another pool:</strong>';

        cannotDeleteLicenses.forEach((license: LicenseRow) => {
          description += `<br><i>${license.name || license.id}</i>`;
        });
      }
    }

    if (this.selectedLicenses.length && this.licensePoolSelection.selected.length) {
      description += '<br><br>';
    }

    if (this.licensePoolSelection.selected.length) {
      description += '<strong>Delete the following pools and licenses:</strong><br>';
      this.licensePoolSelection.selected.forEach((pool: UILicensePool) => {
        description += `<br><i>${pool.name}</i>`;

        Object.values(pool.licenses)?.forEach((license: License) => {
          description += `<div class="sublist">${license.config.name ?? license.config.identifier}</div>`;
        });
      });
    }

    const data: ConfirmationDialogData = {
      description,
      action: 'Delete'
    };

    const dialogRef = this._dialog.open(ConfirmationDialogComponent, { ...DIALOG_CONFIG, data });

    dialogRef
      .afterClosed().pipe(
        filter(Boolean)
      )
      .subscribe(() => {
        if (canDeleteLicenses.length && this.licensePoolSelection.selected.length) {
          forkJoin([this.deleteSelectedLicenses(canDeleteLicenses), this.deleteSelectedPools()]).subscribe(
            () => {
              this._alertService.info(AlertType.INFO_DELETE_LICENSES_AND_POOL_SUCCESS);

              this.getLicensePoolsAndTenants();
            },
            () => this._alertService.error(AlertType.ERROR_DELETE_LICENSES_AND_POOL)
          );
        } else if (canDeleteLicenses.length) {
          this.deleteSelectedLicenses(canDeleteLicenses).subscribe(
            () => {
              this._alertService.info(AlertType.INFO_DELETE_LICENSES_SUCCESS);

              this.getLicensePoolsAndTenants();
            },
            () => this._alertService.error(AlertType.ERROR_DELETE_LICENSES)
          );
        } else if (this.licensePoolSelection.selected) {
          this.deleteSelectedPools().subscribe(
            () => {
              this._alertService.info(AlertType.INFO_DELETE_LICENSE_POOLS_SUCCESS);

              this.getLicensePoolsAndTenants();
            },
            () => this._alertService.error(AlertType.ERROR_DELETE_LICENSE_POOLS)
          );
        }
      });
  }

  private deleteSelectedLicenses(licenses: LicenseRow[]): Observable<any> {
    return forkJoin(
      licenses.map(
        (license: LicenseRow) => this._licenseService.deleteLicense(license.id, license.poolId)
      )
    );
  }

  private deleteSelectedPools(): Observable<any> {
    return forkJoin(
      this.licensePoolSelection.selected.map(
        (pool: UILicensePool) => this._licenseService.deleteLicensePool(pool.identifier)
      )
    );
  }

  public onAddNewPool() {
    const dialogRef: MatDialogRef<PoolDialogComponent, string> = this._dialog.open(
      PoolDialogComponent,
      POOL_DIALOG_CONFIG
    );

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    this._dialogSubscription = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      )
      .subscribe((poolId: string) => {
        this.getLicensePoolsAndTenants();
      });
  }

  private setLicensePoolDropdownPools() {
    const selectedLicenseIds: string[] = this.selectedLicenses.map(
      (row: LicenseRow): string => row.id
    );

    this._pools.forEach((pool: UILicensePool) => {
      // pool licenses
      const poolLicenseIds: string[] = Object
        .values<License>(pool.licenses)
        .map((license: License): string => license.config.identifier);

      // check if selected licenses have been already granted to this pool
      const hasSelectedLicense: boolean = selectedLicenseIds.some(
        (id: string): boolean => poolLicenseIds.includes(id)
      );

      pool.disableSelect = hasSelectedLicense;
    });
  }

  public onOpenGrantLicensePoolDropdown() {
    this._isGrantLicensePoolDropdownOpen = true;
  }

  public get isGrantLicensePoolDropdownOpen(): boolean {
    return this._isGrantLicensePoolDropdownOpen;
  }

  public onGrantLicenses() {
    const licenses: LicenseRow[] = this.selectedLicenses;

    forkJoin(
      licenses.map(
        (license: LicenseRow) => this._licenseService.grantLicense(license.id, license.poolId, this.selectedPoolId)
      )
    )
      .subscribe(
        () => {
          this.getLicensePoolsAndTenants();

          this.closeGrantLicensePoolDropdown();
          this._alertService.info(AlertType.INFO_GRANT_LICENSES_SUCCESS);
        },
        () => this._alertService.error(AlertType.ERROR_GRANT_LICENSES)
      );
  }

  public get licensePoolDropdownPositions(): ConnectedPosition[] {
    return this._licensePoolDropdownPositions;
  }

  public closeGrantLicensePoolDropdown() {
    this._isGrantLicensePoolDropdownOpen = false;
    this._selectedPoolId = undefined;
  }

  public onSelectPool(poolId: string) {
    this._selectedPoolId = poolId;
  }

  public get selectedPoolId(): string {
    return this._selectedPoolId;
  }

  private revokeLicensesRequest(licenses: LicenseRow[]): Observable<string[]> {
    return forkJoin(
      licenses.map(
        (license: LicenseRow) => this._licenseService.revokeLicense(license.id, license.grantingPool)
      )
    );
  }

  public onRevokeLicenses() {
    let description = '<strong>Revoke the following licenses:</strong>';

    this.selectedLicenses.forEach((license: LicenseRow) => {
      description += `<br><i>${license.name || license.id}</i>`;
    });

    const data: ConfirmationDialogData = {
      description,
      action: 'Revoke'
    };

    const dialogRef = this._dialog.open(ConfirmationDialogComponent, { ...DIALOG_CONFIG, data });

    if (this._revokeLicenseDialogSubscription) {
      this._revokeLicenseDialogSubscription.unsubscribe();
    }

    this._revokeLicenseDialogSubscription = dialogRef
      .afterClosed().pipe(
        mergeMap((doRevoke: boolean): Observable<string[]> =>
          doRevoke ? this.revokeLicensesRequest(this.selectedLicenses) : of([])
        )
      )
      .subscribe((revokedLicenses: string[]) => {
        if (revokedLicenses.length) {
          this._alertService.info(AlertType.INFO_REVOKE_LICENSE_SUCCESS);

          this.getLicensePoolsAndTenants();
        }
      }, () => this._alertService.error(AlertType.ERROR_REVOKE_LICENSES));
  }

  public onLicensePoolChecked(pool: UILicensePool, checked: boolean) {
    if (checked) {
      this._licensePoolSelection.select(pool);
    } else {
      this._licensePoolSelection.deselect(pool);
    }
  }

  public onLicenseChecked() {
    this._hasCheckedLicenses = this._licenseCheckboxes ? this._licenseCheckboxes.some((item: MatCheckbox) => item.checked === true) : false;

    this.setLicensePoolDropdownPools();
  }

  /**
   * @description Compare function to preserve an order in `KeyValue` pipe.
   *
   * @param a Comparable object.
   * @param b Comparable object.
   */
  public originalOrder(a: KeyValue<string, string>, b: KeyValue<string, string>): number {
    return 0;
  }

  public findPool(id: string): UILicensePool {
    return this._pools?.find((pool: UILicensePool) => pool.identifier === id);
  }

  public get licensePoolSelection(): SelectionModel<UILicensePool> {
    return this._licensePoolSelection;
  }

  public get isDeleteDisabled(): boolean {
    const canDeleteLicenses = this.selectedLicenses.findIndex((license: LicenseRow) => license.poolId === license.rootPool) >= 0;
    return !(canDeleteLicenses || this.licensePoolSelection.selected.length);
  }

  public get hasCheckedLicenses(): boolean {
    return this._hasCheckedLicenses;
  }

  public get selectedLicenses(): LicenseRow[] {
    const licenses = new Array<LicenseRow>();

    if (this._licenseCheckboxes) {
      this._licenseCheckboxes.forEach((item: MatCheckbox) => {
        if (item.checked) {
          const value: unknown = item.value;

          licenses.push(value as LicenseRow);
        }
      });
    }

    return licenses;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get LicenseState(): typeof LicenseState {
    return LicenseState;
  }
}
