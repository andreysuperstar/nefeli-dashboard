import { NetworkFunctionsService } from 'rest_client/pangolin/api/networkFunctions.service';
import { UserService } from 'src/app/users/user.service';
import { LicenseService } from './license.service';
import { DOCUMENT } from '@angular/common';
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed, fakeAsync, tick, flush } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { LicensesComponent, LicenseRow } from './licenses.component';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';
import { OverlayModule } from '@angular/cdk/overlay';
import { HTTP_INTERCEPTORS, HttpRequest } from '@angular/common/http';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { TestRequest, HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { environment } from '../../environments/environment';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { By } from '@angular/platform-browser';
import { RESTNFCatalog } from 'rest_client/pangolin/model/rESTNFCatalog';
import { LicenseState } from 'rest_client/pangolin/model/licenseState';
import { RESTLicensePools } from 'rest_client/pangolin/model/rESTLicensePools';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { Tenants } from 'rest_client/heimdallr/model/tenants';
import { BASE_PATH as BASE_PATH_HEIMDALLR } from 'rest_client/heimdallr/variables';
import { Tenant, TenantService } from '../tenant/tenant.service';
import { PoolDialogComponent } from '../shared/pool-dialog/pool-dialog.component';
import { ConfirmationDialogComponent } from '../shared/confirmation-dialog/confirmation-dialog.component';
import { User } from 'rest_client/heimdallr/model/user';
import { cloneDeep } from 'lodash-es';
import { Services } from 'rest_client/pangolin/model/services';
import { ServicesService } from 'rest_client/pangolin/api/services.service';

enum UserType {
  SystemAdmin,
  TenantAdmin,
  SystemUser,
  TenantUser
}

const mockUsers: User[] = [
  {
    username: 'SystemAdmin',
    email: 'sysadmin@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin']
    }
  },
  {
    username: 'TenantAdmin',
    email: 'tenantadmin@nefeli.io',
    roles: {
      scope: 'tenant',
      id: '-2',
      roles: ['admin']
    }
  },
  {
    username: 'SystemUser',
    email: 'sysuser@nefeli.io',
    roles: {
      scope: 'system',
      id: '-3',
      roles: ['user']
    }
  }
];

describe('LicensesComponent', () => {
  let component: LicensesComponent;
  let fixture: ComponentFixture<LicensesComponent>;
  let el: HTMLElement;
  let httpTestingController: HttpTestingController;
  let document: Document;
  let userService: UserService;

  const mockNFCatalog: RESTNFCatalog = {
    descriptors: [
      {
        identifier: '21fb9258-b759-469d-bb9c-5db0f3668ec6',
        name: 'arista_router'
      },
      {
        identifier: '5fa5ff07-37d1-4582-a13e-3dc87c6f07f9',
        name: 'pan_firewall'
      },
      {
        identifier: '3dc87c6f07f9-37d1-4582-a13e-5fa5ff07',
        name: 'load_balancer_f5'
      }
    ]
  };

  const mockServices: Services = {
    services: [
      {
        config: {
          identifier: '48db070b-8ab2-4900-8628-2d110e5ac6a5',
          name: 'vsrx3'
        }
      }
    ]
  };

  const mockLicensePools: RESTLicensePools = {
    licensePools: [
      {
        "identifier": "pool-id",
        "name": "pool name 1",
        "licenses": {
          'license-id1': {
            "config": {
              "identifier": "license-id1",
              nfId: mockNFCatalog.descriptors[0].identifier,
              "dateAdded": "2019-06-06T22:48:19.549Z",
              "startDate": "2019-06-07T07:00:00.000Z",
              "endDate": "2019-06-10T07:00:00.000Z",
              "name": "Arista Router License",
              "description": "description for this license",
              "data": ""
            },
            status: {
              state: LicenseState.INUSE,
              rootPool: "pool-id2",
              parentPool: "pool-id2",
              user: {
                instanceId: '123e4567-e89b-12d3-a456-426614174000',
                logicalNf: 'vsrxA',
                nfClass: 'vsrx',
                pipeline: mockServices.services[0].config.identifier,
                tenantId: '39839134759531970412313163318949310715'
              }
            }
          },
          'license-id2': {
            "config": {
              "identifier": "license-id2",
              nfId: mockNFCatalog.descriptors[1].identifier,
              "dateAdded": "2019-06-06T22:48:19.549Z",
              "startDate": "2019-06-07T07:00:00.000Z",
              "endDate": "2025-06-10T07:00:00.000Z",
              "name": "PAN Firewall",
              "description": "description for this license",
              "data": ""
            },
            "status": {
              "rootPool": "pool-id",
              "parentPool": "",
              "state": LicenseState.RECLAIMING,
            },
          },
        },
        "licenseOwners": {}
      },
      {
        "identifier": "pool-id2",
        "name": "pool name 2",
        "licenses": {
          'license-id1': {
            "config": {
              "identifier": "license-id1",
              nfId: mockNFCatalog.descriptors[2].identifier,
              "dateAdded": "2019-06-06T22:48:19.549Z",
              "startDate": "2019-06-07T07:00:00.000Z",
              "endDate": "2025-06-10T07:00:00.000Z",
              "name": "F5 Load Balancer",
              "description": "description for this license",
              "data": ""
            },
            "status": {
              "rootPool": "pool-id",
              "parentPool": "pool-id",
              "state": LicenseState.AVAILABLE,
            }
          }
        },
        "licenseOwners": {}
      }
    ]
  };

  const mockTenants: Tenant[] = [
    {
      identifier: '39839134759531970412313163318949310714',
      name: 'Stumptown Coffee'
    },
    {
      identifier: '39839134759531970412313163318949310715',
      name: 'World Market'
    },
    {
      identifier: '39839134759531970412313163318949310716',
      name: 'Bed, Bath & Beyond'
    },
    {
      identifier: '39839134759531970412313163318949310717',
      name: 'Costco'
    }
  ];

  function fetchLicensePools() {
    const licensePoolsRequest: TestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/license_pools`
    });

    const nfCatalogRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/nfs`
    });

    const tenantsRequest: TestRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/auth/tenants`
    });

    licensePoolsRequest.flush(mockLicensePools);
    nfCatalogRequest.flush(mockNFCatalog);
    tenantsRequest.flush(mockTenantsResponse);

    const hasTenantServices = component['_tenantServices'].has(mockLicensePools.licensePools[0].licenses['license-id1'].status.user.tenantId);

    if (!hasTenantServices) {
      const servicesRequest = httpTestingController.expectOne({
        method: 'GET',
        url: `${environment.restServer}/v1/tenants/${mockLicensePools.licensePools[0].licenses['license-id1'].status.user.tenantId}/services`
      });

      servicesRequest.flush(mockServices);
    }
  }

  const mockTenantsResponse: Tenants = {
    tenants: mockTenants,
    metadata: {
      index: 0,
      total: mockTenants.length,
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatCheckboxModule,
        MatTableModule,
        MatSortModule,
        MatMenuModule,
        MatSnackBarModule,
        MatCardModule,
        MatSelectModule,
        OverlayModule,
        MatDialogModule,
        MatInputModule,
        MatTooltipModule
      ],
      declarations: [
        LicensesComponent,
        ConfirmationDialogComponent,
        PoolDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: BASE_PATH_HEIMDALLR,
          useValue: '/auth'
        },
        NetworkFunctionsService,
        ServicesService,
        LicenseService,
        UserService,
        TenantService
      ]
    });

    document = TestBed.inject(DOCUMENT);
    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    userService.setUser(mockUsers[UserType.SystemAdmin]);
    fixture = TestBed.createComponent(LicensesComponent);
    component = fixture.componentInstance;
    el = fixture.debugElement.nativeElement;
    fixture.detectChanges();

    fetchLicensePools();

    fixture.detectChanges();
  });

  const clickLicenseRow = (index: number = 0, selectedLicenses = 1): LicenseRow => {
    const pools = fixture.debugElement.queryAll(By.css('table'));
    let pool = pools[0].nativeElement;
    let poolRows = pool.querySelectorAll('tbody > tr');
    const checkbox: HTMLInputElement = poolRows[index].querySelector('.mat-column-select label');
    checkbox.click();
    expect(component.selectedLicenses.length).toBe(selectedLicenses);
    return component.selectedLicenses[0];
  };

  enum MENU_OPTIONS {
    GRANT,
    REVOKE,
    DELETE
  }

  const clickMenuOption = (option: MENU_OPTIONS) => {
    const menuButton: HTMLButtonElement = el.querySelector('header > h2 > button');
    menuButton.click();
    fixture.detectChanges();
    const menu = document.querySelector('.mat-menu-panel');
    const menuItems = menu.querySelectorAll('.mat-menu-item');
    const button: HTMLButtonElement = <HTMLButtonElement> menuItems[option];
    button.click();
    fixture.detectChanges();
  }

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render page heading', () => {
    expect(el.querySelector('header h2').textContent.trim()).toBe('Licenses');
  });

  it('should render action buttons', () => {
    const actionButtonDes: DebugElement[] = fixture.debugElement.queryAll(
      By.css('header .actions button')
    );

    expect(
      actionButtonDes[0].nativeElement.textContent
    ).toBe('Add New License', 'valid new license button text');
    expect(
      actionButtonDes[1].nativeElement.textContent
    ).toBe('Add New Pool', 'valid new pool button text');

    expect(
      actionButtonDes[0].nativeElement.disabled
    ).toBeFalsy('new license button is not disabled where there are pools');

    component['_pools'] = [];
    fixture.detectChanges();

    expect(
      actionButtonDes[0].nativeElement.disabled
    ).toBeTruthy('new license button is disabled where there are no pools');
  });

  it('should hide action buttons for system user', () => {
    userService.setUser(mockUsers[UserType.SystemUser]);
    fixture.detectChanges();
    const actionButtons: DebugElement[] = fixture.debugElement.queryAll(
      By.css('header .actions button')
    );
    const dotsButton: DebugElement = fixture.debugElement.query(
      By.css('.three-dots-button')
    );

    expect(actionButtons.length).toBe(0)
    expect(dotsButton).toBeNull();
  });

  it('should render all license pools', () => {
    const pools = fixture.debugElement.queryAll(By.css('table'));
    const poolTitles = fixture.debugElement.queryAll(By.css('.pool-title'));

    expect(pools.length).toBe(2);

    expect(poolTitles[0].query(By.css('span.h4-bold')).nativeElement.textContent).toBe(mockLicensePools.licensePools[0].name);
    expect(poolTitles[1].query(By.css('span.h4-bold')).nativeElement.textContent).toBe(mockLicensePools.licensePools[1].name);
  });

  it('should render all licenses', () => {
    const pools = fixture.debugElement.queryAll(By.css('table'));

    let pool = pools[0].nativeElement;
    let poolRows = pool.querySelectorAll('tbody > tr');
    expect(poolRows[0].querySelector('.mat-column-nf').textContent.trim()).toBe(mockNFCatalog.descriptors[0].name);
    expect(poolRows[0].querySelector('.mat-column-name').textContent.trim()).toBe('Arista Router License');
    expect(poolRows[0].querySelector('.mat-column-grantingPool').textContent).toBe('pool name 2');
    expect(poolRows[0].querySelector('.mat-column-rootPool').textContent).toBe('pool name 2');
    expect(poolRows[0].querySelector('.mat-column-expiration').textContent).toBe('Jun 10, 2019');
    expect(poolRows[0].querySelector('.mat-column-expiration img')).not.toBeNull('render expiry icon');
    expect(poolRows[0].querySelector('.mat-column-usedBy').textContent).toBe(
      `${mockTenants[1].name} ▸ ${mockServices.services[0].config.name} ▸ ${mockLicensePools.licensePools[0].licenses['license-id1'].status.user.logicalNf}`
    );

    expect(poolRows[1].querySelector('.mat-column-nf').textContent.trim()).toBe(mockNFCatalog.descriptors[1].name);
    expect(poolRows[1].querySelector('.mat-column-name').textContent.trim()).toBe('PAN Firewall');
    expect(poolRows[1].querySelector('.mat-column-grantingPool').textContent).toBe('');
    expect(poolRows[1].querySelector('.mat-column-rootPool').textContent).toBe('pool name 1');
    expect(poolRows[1].querySelector('.mat-column-expiration').textContent).toBe('Jun 10, 2025');
    expect(poolRows[1].querySelector('.mat-column-expiration img')).toBeNull('not render expiry icon');
    expect(poolRows[1].querySelector('.mat-column-usedBy').textContent).toBe('');

    pool = pools[1].nativeElement;
    poolRows = pool.querySelectorAll('tbody > tr');
    expect(poolRows[0].querySelector('.mat-column-nf').textContent.trim()).toBe(mockNFCatalog.descriptors[2].name);
    expect(poolRows[0].querySelector('.mat-column-name').textContent.trim()).toBe('F5 Load Balancer');
    expect(poolRows[0].querySelector('.mat-column-grantingPool').textContent).toBe('pool name 1');
    expect(poolRows[0].querySelector('.mat-column-rootPool').textContent).toBe('pool name 1');
    expect(poolRows[0].querySelector('.mat-column-expiration').textContent).toBe('Jun 10, 2025');
    expect(poolRows[0].querySelector('.mat-column-expiration img')).toBeNull('not render expiry icon');
    expect(poolRows[0].querySelector('.mat-column-usedBy').textContent).toBe('');
  });

  it('should display actions menu', () => {
    const menuButton: HTMLButtonElement = el.querySelector('header > h2 > button');
    expect(menuButton).toBeTruthy();
    menuButton.click();
    fixture.detectChanges();
    const menu = document.querySelector('.mat-menu-panel');
    expect(menu).toBeTruthy();
    const menuItems = menu.querySelectorAll('.mat-menu-item');
    expect(menuItems.length).toBe(3);
    expect(menuItems[0].textContent.trim()).toBe('Grant');
    expect(menuItems[1].textContent.trim()).toBe('Revoke');
    expect(menuItems[2].textContent.trim()).toBe('Delete');
  });

  it('should delete selected licenses', fakeAsync(() => {
    const licenseRow = clickLicenseRow(1);
    expect(licenseRow.id).toBe('license-id2');

    clickMenuOption(MENU_OPTIONS.DELETE);

    const dialog = document.querySelector('nef-confirmation-dialog');
    tick();
    const dialogText = dialog.querySelector('p');
    expect(dialogText.textContent).toBe('Delete the following licenses:PAN Firewall');
    const deleteButtonEl: HTMLButtonElement = dialog.querySelectorAll('button')[1];
    expect(deleteButtonEl.textContent).toBe('Delete');

    deleteButtonEl.click();
    tick();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/${licenseRow.poolId}/licenses/${licenseRow.id}`);
    expect(req.request.method).toBe('DELETE');
    req.flush({});

    const refreshLicensePools = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`);
    expect(refreshLicensePools.request.method).toBe('GET');

    flush();
  }));

  it('should remove selected license pools', fakeAsync(() => {
    const pools = fixture.debugElement.queryAll(By.css('.pool-title'));
    let pool = pools[0].nativeElement;
    const checkboxLabel: HTMLLabelElement = pool.querySelector('.mat-checkbox label');
    checkboxLabel.click();
    fixture.detectChanges();
    expect(component.licensePoolSelection.selected.length).toBe(1);

    clickMenuOption(MENU_OPTIONS.DELETE);

    const dialog = document.querySelector('nef-confirmation-dialog');
    tick();
    const dialogText = dialog.querySelector('p');
    expect(dialogText.textContent).toBe('Delete the following pools and licenses:pool name 1Arista Router LicensePAN Firewall');
    const deleteButtonEl: HTMLButtonElement = dialog.querySelectorAll('button')[1];
    expect(deleteButtonEl.textContent).toBe('Delete');

    deleteButtonEl.click();
    tick();

    const req = httpTestingController.expectOne((request: HttpRequest<any>) => {
      return request.url === `${environment.restServer}/v1/license_pools/pool-id`
        && request.method === 'DELETE'
    });
    req.flush({});

    const refreshLicensePools = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`);
    expect(refreshLicensePools.request.method).toBe('GET');

    flush();
  }));

  it('should open Pool dialog', () => {
    const addPoolButtonEl: HTMLButtonElement = fixture.debugElement.queryAll(
      By.css('header .actions button')
    )[1].nativeElement;

    spyOn(component, 'onAddNewPool').and.callThrough();

    addPoolButtonEl.click();

    expect(component.onAddNewPool).toHaveBeenCalled();

    const poolDialogEl: HTMLUnknownElement = document.querySelector(
      'nef-pool-dialog'
    );

    expect(poolDialogEl).not.toBeNull('render pool dialog');

    const cancelButton: HTMLButtonElement = <HTMLButtonElement> document.querySelectorAll('mat-dialog-actions button')[0];
    cancelButton.click();
  });

  it('should render disabled pools in License Pool dropdown', () => {
    spyOn<any>(component, 'setLicensePoolDropdownPools').and.callThrough();

    clickLicenseRow(1);

    expect(component['setLicensePoolDropdownPools']).toHaveBeenCalled();

    expect(component.pools[0].disableSelect).toBeTruthy(
      '\'disableSelect\' is truthy for pool with granted license'
    );
    expect(component.pools[1].disableSelect).toBeFalsy(
      '\'disableSelect\' is falsy for pool without granted license'
    );

    clickMenuOption(MENU_OPTIONS.GRANT);

    const poolSelectEl: HTMLUnknownElement = document
      .querySelector('mat-card.grant-licenses')
      .querySelector('mat-select');

    poolSelectEl.click();
    fixture.detectChanges();

    const optionEls: NodeListOf<HTMLUnknownElement> = document
      .querySelector('.mat-select-panel')
      .querySelectorAll('mat-option');

    expect(
      optionEls[0].getAttribute('aria-disabled')
    ).toBe('true', 'render disabled pool option');
    expect(
      optionEls[1].getAttribute('aria-disabled')
    ).toBe('false', 'render enabled pool option');
  });

  it('should enable grant button when licenses selected', () => {
    const menuButton: HTMLButtonElement = el.querySelector('header > h2 > button');
    menuButton.click();
    fixture.detectChanges();
    let menuItems = document.querySelectorAll('.mat-menu-panel .mat-menu-item');
    let grantButton: HTMLButtonElement = <HTMLButtonElement> menuItems[0]
    expect(grantButton.hasAttribute('disabled')).toBeTruthy();

    const pools = fixture.debugElement.queryAll(By.css('table'));
    let pool = pools[0].nativeElement;
    let poolRows = pool.querySelectorAll('tbody > tr');
    const checkbox: HTMLInputElement = poolRows[0].querySelector('.mat-column-select label');
    checkbox.click()
    menuButton.click();

    fixture.detectChanges();
    menuItems = document.querySelectorAll('.mat-menu-panel .mat-menu-item');
    grantButton = <HTMLButtonElement> menuItems[0]
    expect(grantButton.hasAttribute('disabled')).toBeFalsy();
  });

  it('should grant license to another pool', fakeAsync(() => {
    const licenseRow = clickLicenseRow();
    expect(licenseRow.id).toBe('license-id1');

    clickMenuOption(MENU_OPTIONS.GRANT);

    const grantCard = document.querySelector('.grant-licenses');
    expect(grantCard).not.toBeNull();
    expect(grantCard.querySelector('.mat-card-title').textContent).toBe('Grant license');
    const licenses = grantCard.querySelectorAll('.licenses');
    expect(licenses.length).toBe(1);
    expect(licenses[0].textContent.trim()).toBe('"Arista Router License"');
    const confirmButton: HTMLButtonElement = grantCard.querySelector('button');
    expect(confirmButton.hasAttribute('disabled')).toBeTruthy();

    component.onSelectPool('pool-id2');
    fixture.detectChanges();
    expect(confirmButton.hasAttribute('disabled')).toBeFalsy();
    confirmButton.click();

    const req = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses/license-id1/owner/pool-id2`);
    expect(req.request.method).toBe('POST');
    req.flush({});

    const refreshLicensePools = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools`);
    expect(refreshLicensePools.request.method).toBe('GET');

    flush();
  }));

  it('should revoke selected licenses', fakeAsync(() => {
    const licenseRow = clickLicenseRow();
    expect(licenseRow.id).toBe('license-id1');

    clickMenuOption(MENU_OPTIONS.REVOKE);
    tick();

    // confirmation dialog
    const dialogButtons: HTMLButtonElement[] = [].slice.call(document.querySelectorAll('mat-dialog-actions button'));
    const confirmButton = dialogButtons.find((elem: HTMLButtonElement): boolean => elem.textContent === 'Revoke');
    expect(confirmButton.textContent).toBe('Revoke');
    confirmButton.click();
    fixture.detectChanges();

    tick();

    // revoke selected license
    const revokeReq = httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id2/licenses/license-id1/owner`);
    revokeReq.flush('license');

    // refresh license pools
    fetchLicensePools();

    fixture.detectChanges();

    expect(component.selectedLicenses.length).toBe(0);

    tick(1000);
  }));

  it('should not delete when cancel confirmation dialog', fakeAsync(() => {
    const licenseRow = clickLicenseRow(1);

    clickMenuOption(MENU_OPTIONS.DELETE);

    const dialog = document.querySelector('nef-confirmation-dialog');
    tick();
    const dialogText = dialog.querySelector('p');
    expect(dialogText.textContent).toBe('Delete the following licenses:PAN Firewall');
    const cancelButtonEl: HTMLButtonElement = dialog.querySelectorAll('button')[0];
    expect(cancelButtonEl.textContent).toBe('Cancel');

    cancelButtonEl.click();
    tick();

    httpTestingController.expectNone(`${environment.restServer}/v1/license_pools/${licenseRow.poolId}/licenses/${licenseRow.id}`);
    flush();
  }));

  it('should disable delete if selected license is not in root pool', () => {
    clickLicenseRow();
    const menuButton: HTMLButtonElement = el.querySelector('header > h2 > button');
    menuButton.click();
    fixture.detectChanges();
    const menu = document.querySelector('.mat-menu-panel');
    const menuItems = menu.querySelectorAll('.mat-menu-item');
    const deleteMenuItem = <HTMLButtonElement> menuItems[2];
    expect(deleteMenuItem.disabled).toBe(true);
  });

  it('should not delete selected licenses that are not in root pool', fakeAsync(() => {
    // click on license not in root pool
    clickLicenseRow(0);

    // click on license in root pool
    clickLicenseRow(1, 2);

    clickMenuOption(MENU_OPTIONS.DELETE);
    tick();

    // confirmation dialog
    const dialog = document.querySelector('nef-confirmation-dialog');
    const dialogText = dialog.querySelector('p');
    expect(dialogText.textContent).toBe(
      "Delete the following licenses:PAN FirewallThe following licenses cannot be deleted because they have been granted from their root pool to another pool:Arista Router License");
    const dialogButtons: HTMLButtonElement[] = [].slice.call(document.querySelectorAll('mat-dialog-actions button'));
    const deleteButton = dialogButtons.find((elem: HTMLButtonElement): boolean => elem.textContent === 'Delete');
    deleteButton.click();
    fixture.detectChanges();

    tick();
    httpTestingController.expectOne(`${environment.restServer}/v1/license_pools/pool-id/licenses/license-id2`);
    httpTestingController.expectNone(`${environment.restServer}/v1/license_pools/pool-id/licenses/license-id1`);
    flush();
  }));

  it('should display a proper status badge', () => {
    let badges = fixture.debugElement.queryAll(By.css('.badge'));

    expect(badges[0].classes.blue).toBe(true);
    expect(badges[1].classes.yellow).toBe(true);
    expect(badges[2].classes.green).toBe(true);
    expect(badges[3]).toBeUndefined();

    const mockPools = cloneDeep(mockLicensePools.licensePools);

    mockPools[0].licenses['license-id2'].status = {
      state: LicenseState.GRANTED
    };

    mockPools[1].licenses['license-id1'].status = {
      state: LicenseState.INUSE,
      user: {
        instanceId: '123e4567-e89b-12d3-a456-426614174000',
        logicalNf: 'vsrxA',
        nfClass: 'vsrx',
        pipeline: mockServices.services[0].config.identifier,
        tenantId: '39839134759531970412313163318949310715'
      }
    };

    component['_pools'] = mockPools;
    component['createDataSource'](mockPools);

    fixture.detectChanges();

    badges = fixture.debugElement.queryAll(By.css('.badge'));

    expect(badges[0].classes.blue).toBe(true);
    expect(badges[1].classes.grey).toBe(true);
    expect(badges[2].classes.blue).toBe(true);
    expect(badges[3]).toBeUndefined();
  });
});
