import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';

import { SharedModule } from '../shared/shared.module';

import { SystemConfigurationRoutingModule } from './system-configuration-routing.module';

import { SystemConfigurationComponent } from './system-configuration.component';
import { WeaverSyncComponent } from './weaversync/weaversync.component';
import { SMTPConfigurationComponent } from './smtp-configuration/smtp-configuration.component';

@NgModule({
  declarations: [
    SystemConfigurationComponent,
    WeaverSyncComponent,
    SMTPConfigurationComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatCheckboxModule,
    MatInputModule,
    MatIconModule,
    MatTooltipModule,
    MatButtonModule,
    SharedModule,
    SystemConfigurationRoutingModule,
  ]
})
export class SystemConfigurationModule { }
