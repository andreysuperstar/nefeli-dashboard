import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

import { Subscription } from 'rxjs';
import { filter, mergeMapTo } from 'rxjs/operators';

import { SMTPConf } from 'rest_client/pangolin/model/sMTPConf';

import { SmtpService } from 'rest_client/pangolin/api/smtp.service';
import { AlertService, AlertType } from 'src/app/shared/alert.service';

import { ConfirmationDialogComponent, ConfirmationDialogData } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';

enum SMTPServerPort {
  Min = 1,
  Max = 65535
}

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  restoreFocus: false
};

@Component({
  selector: 'nef-smtp-configuration',
  templateUrl: './smtp-configuration.component.html',
  styleUrls: ['./smtp-configuration.component.less']
})
export class SMTPConfigurationComponent implements OnInit, OnDestroy {
  private _hasSMTPConfiguration: boolean;
  private _smtpForm: FormGroup;
  private _smtpSubscription: Subscription;

  constructor(
    private _fb: FormBuilder,
    private _dialog: MatDialog,
    private _smtpService: SmtpService,
    private _alertService: AlertService
  ) { }

  public ngOnInit() {
    this.initSMTPForm();

    this._smtpSubscription = this._smtpService
      .getSMTPConfig()
      .subscribe(({
        fromAddress: email,
        username,
        password,
        serverHostName: server,
        serverPort: port
      }) => {
        this._smtpForm.patchValue({ email, username, password, server, port });

        if (email) {
          this._hasSMTPConfiguration = true;
        }
      });
  }

  public ngOnDestroy() {
    this._smtpSubscription?.unsubscribe();
  }

  private initSMTPForm() {
    this._smtpForm = this._fb.group({
      email: [
        '',
        [
          Validators.required,
          Validators.email
        ]
      ],
      username: ['', Validators.required],
      password: ['', Validators.required],
      server: ['', Validators.required],
      port: [
        undefined,
        [
          Validators.required,
          Validators.min(SMTPServerPort.Min),
          Validators.max(SMTPServerPort.Max)
        ]
      ]
    });
  }

  public submitSMTPForm() {
    const { invalid, pristine } = this._smtpForm;

    this._smtpSubscription?.unsubscribe();

    if (invalid || pristine) {
      return;
    }

    const {
      email: fromAddress,
      username,
      password,
      server: serverHostName,
      port
    }: {
      email: string;
      username: string;
      password: string;
      server: string;
      port: number;
    } = this._smtpForm.value;

    const smtpConfiguration: SMTPConf = {
      fromAddress,
      username,
      password,
      serverHostName,
      serverPort: String(port)
    };

    this._smtpSubscription = this._smtpService
      .putSMTPConfig(smtpConfiguration)
      .subscribe({
        next: () => {
          this._hasSMTPConfiguration = true;

          this._alertService.info(AlertType.INFO_SUBMIT_SMTP_CONFIGURATION);
        },
        error: ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_SUBMIT_SMTP_CONFIGURATION, message);
        }
      });
  }

  public deleteSMTPConfiguration() {
    this._smtpSubscription?.unsubscribe();

    const data: ConfirmationDialogData = {
      description: 'This will delete the current <strong>SMTP configuration</strong>.',
      action: 'Delete Configuration'
    };

    const dialogRef = this._dialog.open<ConfirmationDialogComponent, ConfirmationDialogData, boolean>(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    const confirmation$ = dialogRef
      .afterClosed()
      .pipe(
        filter(Boolean)
      );

    this._smtpSubscription = confirmation$
      .pipe(
        mergeMapTo(this._smtpService.deleteSMTPConfig())
      )
      .subscribe({
        next: () => {
          this._hasSMTPConfiguration = false;

          this._alertService.info(AlertType.INFO_DELETE_SMTP_CONFIGURATION);
        },
        error: ({
          error: { message }
        }: HttpErrorResponse) => {
          this._alertService.error(AlertType.ERROR_DELETE_SMTP_CONFIGURATION, message);
        }
      });
  }

  public get smtpForm(): FormGroup {
    return this._smtpForm;
  }

  public get SMTPServerPort(): typeof SMTPServerPort {
    return SMTPServerPort;
  }

  public get hasSMTPConfiguration(): boolean {
    return this._hasSMTPConfiguration;
  }
}
