import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardHarness } from '@angular/material/card/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { OverlayContainer } from '@angular/cdk/overlay';

import { SMTPConf } from 'rest_client/pangolin/model/sMTPConf';

import { SMTPError } from 'rest_client/pangolin/model/sMTPError';

import { BASE_PATH } from 'rest_client/pangolin/variables';
import { environment } from 'src/environments/environment';
import { HttpError } from 'src/app/error-codes';

import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';

import { SmtpService } from 'rest_client/pangolin/api/smtp.service';
import { AlertService, AlertType } from 'src/app/shared/alert.service';

import { SMTPConfigurationComponent } from './smtp-configuration.component';

describe('SMTPConfigurationComponent', () => {
  let component: SMTPConfigurationComponent;
  let fixture: ComponentFixture<SMTPConfigurationComponent>;
  let httpTestingController: HttpTestingController;
  let overlayContainer: OverlayContainer;
  let documentRootLoader: HarnessLoader;
  let loader: HarnessLoader;
  let alertService: AlertService;

  const mockSMTPConfiguration: SMTPConf = {
    fromAddress: 'andrew@nefeli.io',
    username: 'andrew',
    password: 'noop',
    serverHostName: 'nefeli.io',
    serverPort: '10'
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        MatCardModule,
        MatInputModule,
        MatSnackBarModule,
        MatDialogModule
      ],
      declarations: [SMTPConfigurationComponent],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        SmtpService,
        AlertService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    overlayContainer = TestBed.inject(OverlayContainer);
    alertService = TestBed.inject(AlertService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SMTPConfigurationComponent);
    documentRootLoader = TestbedHarnessEnvironment.documentRootLoader(fixture);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(async () => {
    const dialogs = await documentRootLoader.getAllHarnesses(MatDialogHarness);

    await Promise.all(
      dialogs.map(async dialog => await dialog.close())
    );

    overlayContainer.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', async () => {
    const card = await loader.getHarness(MatCardHarness);

    expect(await card.getTitleText()).toBe('SMTP Configuration');
  });

  it('should render SMTP form', async () => {
    loader.getHarness(MatInputHarness.with({
      placeholder: 'Enter e-mail'
    }));

    loader.getHarness(MatInputHarness.with({
      placeholder: 'Enter username'
    }));

    loader.getHarness(MatInputHarness.with({
      placeholder: 'Enter password'
    }));

    loader.getHarness(MatInputHarness.with({
      placeholder: 'Enter server'
    }));

    loader.getHarness(MatInputHarness.with({
      placeholder: 'Enter port'
    }));

    loader.getHarness(MatButtonHarness.with({
      text: 'Save'
    }))
  });

  it('should fetch SMTP configuration', async () => {
    const smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/smtp/config`
    });

    smtpConfigurationRequest.flush(mockSMTPConfiguration);

    const inputs = await Promise.all([
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter e-mail'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter username'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter password'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter server'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter port'
      }))
    ]);

    const [
      emailInputValue,
      usernameInputValue,
      passwordInputValue,
      serverInputValue,
      portInputValue
    ] = await Promise.all(
      inputs.map(input => input.getValue())
    );

    expect(emailInputValue).toBe(mockSMTPConfiguration.fromAddress, 'valid e-mail input value');
    expect(usernameInputValue).toBe(mockSMTPConfiguration.username, 'valid username input value');
    expect(passwordInputValue).toBe(mockSMTPConfiguration.password, 'valid password input value');
    expect(serverInputValue).toBe(mockSMTPConfiguration.serverHostName, 'valid server input value');
    expect(portInputValue).toBe(mockSMTPConfiguration.serverPort, 'valid port input value');
  });

  it('should submit SMTP configuration', async () => {
    // Flush fetch SMTP configuration request
    let smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/smtp/config`
    });

    smtpConfigurationRequest.flush({});

    // Pristine SMTP configuration form
    const saveButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Save'
    }));

    await saveButton.click();

    httpTestingController.expectNone(`${environment.restServer}/v1/smtp/config`);

    // Valid SMTP configuration form
    const [
      emailInput,
      usernameInput,
      passwordInput,
      serverInput,
      portInput
    ] = await Promise.all([
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter e-mail'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter username'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter password'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter server'
      })),
      loader.getHarness(MatInputHarness.with({
        placeholder: 'Enter port'
      }))
    ]);

    await Promise.all([
      emailInput.setValue(mockSMTPConfiguration.fromAddress),
      usernameInput.setValue(mockSMTPConfiguration.username),
      passwordInput.setValue(mockSMTPConfiguration.password),
      serverInput.setValue(mockSMTPConfiguration.serverHostName),
      portInput.setValue(mockSMTPConfiguration.serverPort)
    ]);

    // SMTP configuration request success response
    spyOn(alertService, 'info');

    await saveButton.click();

    smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'PUT',
      url: `${environment.restServer}/v1/smtp/config`
    });

    expect(smtpConfigurationRequest.request.body).toEqual(mockSMTPConfiguration, 'valid SMTP configuration request parameters');

    smtpConfigurationRequest.flush({});

    expect(alertService.info).toHaveBeenCalledWith(AlertType.INFO_SUBMIT_SMTP_CONFIGURATION);

    // SMTP configuration request error response
    spyOn(alertService, 'error');

    await saveButton.click();

    smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'PUT',
      url: `${environment.restServer}/v1/smtp/config`
    });

    const mockSMTPError: SMTPError = {
      httpStatus: HttpError.BadRequest,
      message: `Invalid SMTP configuration: dial tcp: lookup ${mockSMTPConfiguration.serverHostName}: no such host`,
      errorCode: 2802
    };

    const responseOptions: {
      headers?: HttpHeaders | {
        [name: string]: string | string[];
      };
      status?: number;
      statusText?: string;
    } = {
      status: HttpError.BadRequest,
      statusText: 'Bad Request'
    };

    smtpConfigurationRequest.flush(mockSMTPError, responseOptions);

    expect(alertService.error).toHaveBeenCalledWith(AlertType.ERROR_SUBMIT_SMTP_CONFIGURATION, mockSMTPError.message);
  });

  it('should delete SMTP configuration', async () => {
    // Flush fetch SMTP configuration request
    let smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'GET',
      url: `${environment.restServer}/v1/smtp/config`
    });

    smtpConfigurationRequest.flush(mockSMTPConfiguration);

    let deleteButton = await loader.getHarness(MatButtonHarness.with({
      text: 'Delete'
    }));

    // Lack of SMTP configuration request
    await deleteButton.click();

    const confirmationDialog = await documentRootLoader.getHarness(MatDialogHarness);

    await confirmationDialog.close();

    httpTestingController.expectNone({
      method: 'PUT',
      url: `${environment.restServer}/v1/smtp/config`
    });

    // SMTP configuration request error response
    await deleteButton.click();
    await documentRootLoader.getHarness(MatDialogHarness);

    let deleteConfigurationButton = await documentRootLoader.getHarness(MatButtonHarness.with({
      text: 'Delete Configuration'
    }));

    spyOn(alertService, 'error');

    await deleteConfigurationButton.click();

    smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/smtp/config`
    });

    const mockErrorEvent = new ErrorEvent('Canceled');

    const responseOptions: {
      headers?: HttpHeaders | {
        [name: string]: string | string[];
      };
      status?: number;
      statusText?: string;
    } = {
      status: HttpError.Canceled,
      statusText: 'Canceled'
    };

    smtpConfigurationRequest.error(mockErrorEvent, responseOptions);

    expect(alertService.error).toHaveBeenCalledWith(AlertType.ERROR_DELETE_SMTP_CONFIGURATION, mockErrorEvent.message);

    // SMTP configuration request success response
    await deleteButton.click();
    await documentRootLoader.getHarness(MatDialogHarness);

    deleteConfigurationButton = await documentRootLoader.getHarness(MatButtonHarness.with({
      text: 'Delete Configuration'
    }));

    spyOn(alertService, 'info');

    await deleteConfigurationButton.click();

    smtpConfigurationRequest = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${environment.restServer}/v1/smtp/config`
    });

    smtpConfigurationRequest.flush({});

    [, deleteButton] = await loader.getAllHarnesses(MatButtonHarness);

    expect(deleteButton).toBeUndefined('hidden Delete button');

    expect(alertService.info).toHaveBeenCalledWith(AlertType.INFO_DELETE_SMTP_CONFIGURATION);
  });
});
