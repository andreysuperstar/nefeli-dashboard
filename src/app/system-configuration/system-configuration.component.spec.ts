import { AlertType } from 'src/app/shared/alert.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpTestingController, HttpClientTestingModule, TestRequest } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { SystemConfigurationComponent } from './system-configuration.component';

import { cloneDeep } from 'lodash-es';

import { environment } from '../../environments/environment';
import { RequestUrlInterceptor } from '../http-interceptors/request-url.interceptor';
import { AlertService } from '../shared/alert.service';

import { BASE_PATH as PANGOLIN_BASE_PATH } from 'rest_client/pangolin/variables';
import { BASE_PATH as ALARMS_BASE_PATH } from 'rest_client/pangolin/variables';
import { ClusterService } from '../home/cluster.service';
import { ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { UserService } from '../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { AAAService } from 'rest_client/pangolin/api/aAA.service';
import { TACPlusConfig } from 'rest_client/pangolin/model/tACPlusConfig';
import { TACPlusStatus } from 'rest_client/pangolin/model/tACPlusStatus';
import { AAAServers } from 'rest_client/pangolin/model/aAAServers';
import { WeaverSyncComponent } from './weaversync/weaversync.component';
import { SMTPConfigurationComponent } from './smtp-configuration/smtp-configuration.component';

describe('SystemConfigurationComponent', () => {
  let component: SystemConfigurationComponent;
  let fixture: ComponentFixture<SystemConfigurationComponent>;
  let el: HTMLElement;
  let httpMockClient: HttpTestingController;
  let userService: UserService;
  let loader: HarnessLoader;
  let aaaService: AAAService;

  const mockServersResponse: AAAServers = {
    aaaServers: [
      {
        identifier: '1',
        address: 'https://ztp.nefeli.io/'
      },
      {
        identifier: '2',
        address: 'https://provisioning.servers.com/'
      },
      {
        identifier: '3',
        address: 'https://192.168.5.100:2300/'
      }
    ]
  }

  const mockSystemAdmin: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['admin', 'user']
    }
  };

  const mockSystemUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: {
      scope: 'system',
      id: '-1',
      roles: ['user']
    }
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        ReactiveFormsModule,
        MatInputModule,
        MatCheckboxModule,
        MatSelectModule,
        MatSnackBarModule,
        MatCardModule,
        MatDialogModule
      ],
      declarations: [
        SystemConfigurationComponent,
        ConfirmationDialogComponent,
        WeaverSyncComponent,
        SMTPConfigurationComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        },
        {
          provide: PANGOLIN_BASE_PATH,
          useValue: '/v1'
        },
        {
          provide: ALARMS_BASE_PATH,
          useValue: '/v1'
        },
        AlertService,
        ClusterService,
        UserService,
        AAAService
      ]
    });
  });

  beforeEach(() => {
    userService = TestBed.inject(UserService);
    userService.setUser(mockSystemAdmin);
    fixture = TestBed.createComponent(SystemConfigurationComponent);
    component = fixture.componentInstance;
    loader = TestbedHarnessEnvironment.loader(fixture);
    el = fixture.debugElement.nativeElement;
    httpMockClient = TestBed.inject(HttpTestingController);
    aaaService = TestBed.inject(AAAService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a title', () => {
    const title = el.querySelector('header h2');
    expect(title.textContent.trim()).toBe('System Configuration');
  });

  it('should show TACACS settings form when checkbox enabled', async () => {
    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();
    let secretInput = el.querySelector('[formControlName=secret]');
    let confirmSecretInput = el.querySelector('[formControlName=confirmSecret]');
    let fallbackInput = el.querySelector('[formControlName=fallback]');
    let confirmFallbackInput = el.querySelector('[formControlName=confirmFallback]');

    expect(secretInput).toBeNull();
    expect(confirmSecretInput).toBeNull();
    expect(fallbackInput).toBeNull();
    expect(confirmFallbackInput).toBeNull();

    const enabledCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      selector: '[formControlName=enabled]'
    }));
    await enabledCheckbox.check();

    secretInput = el.querySelector('[formControlName=secret]');
    confirmSecretInput = el.querySelector('[formControlName=confirmSecret]');
    fallbackInput = el.querySelector('[formControlName=fallback]');
    confirmFallbackInput = el.querySelector('[formControlName=confirmFallback]');

    expect(secretInput).not.toBeNull();
    expect(confirmSecretInput).not.toBeNull();
    expect(fallbackInput).not.toBeNull();
    expect(confirmFallbackInput).not.toBeNull();
  });

  it('should fetch initial TACACS setting', async () => {
    const enabledCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      selector: '[formControlName=enabled]'
    }));
    expect(await enabledCheckbox.isChecked()).toBeFalsy();

    const req: TestRequest = httpMockClient.expectOne(`${environment.restServer}/v1/aaa/tacplus/config`);
    const config: TACPlusConfig = { enabled: TACPlusStatus.ENABLED }
    req.flush(config);
    fixture.detectChanges();

    expect(await enabledCheckbox.isChecked()).toBeTruthy();
  });

  it('should show display list of servers', () => {
    const tacsServersReq: TestRequest = httpMockClient.expectOne(`${environment.restServer}/v1/aaa/servers`);
    tacsServersReq.flush(cloneDeep(mockServersResponse));
    fixture.detectChanges();
    const servers = el.querySelectorAll('.tacacs-server');
    expect(servers.length).toBe(3);
    expect(servers[0].textContent.trim()).toBe('https://ztp.nefeli.io/');
    expect(servers[1].textContent.trim()).toBe('https://provisioning.servers.com/');
    expect(servers[2].textContent.trim()).toBe('https://192.168.5.100:2300/');
  });

  it('should display message when no TACACS+ servers', () => {
    const tacsServersReq: TestRequest = httpMockClient.expectOne(`${environment.restServer}/v1/aaa/servers`);
    const emptyResponse: AAAServers = {
      aaaServers: []
    }
    tacsServersReq.flush(emptyResponse);
    fixture.detectChanges();
    const servers = el.querySelectorAll('.tacacs-server');
    expect(servers.length).toBe(0);
    const serversSelector = '.content > mat-card:nth-child(1) > .mat-card-content > section:nth-child(2) > p';
    let message = el.querySelector(serversSelector);
    expect(message).toBeNull();
    userService.setUser(mockSystemUser);
    fixture.detectChanges();
    message = el.querySelector(serversSelector);
    expect(message.textContent).toBe('No servers configured');
  });

  it('should submit settings', async () => {
    spyOn(component['_alertService'], 'info');
    const enabledCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      selector: '[formControlName=enabled]'
    }));
    await enabledCheckbox.check();
    const password = '12345678';
    const fallbackPassword = 'fallback'
    const secretInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=secret]'
    }));
    await secretInput.setValue(password);
    const secretConfirmInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=confirmSecret]'
    }));
    await secretConfirmInput.setValue(password);
    const fallbackInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=fallback]'
    }));
    await fallbackInput.setValue(fallbackPassword);
    const confirmFallbackInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=confirmFallback]'
    }));
    await confirmFallbackInput.setValue(fallbackPassword);

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button[form=tacacs]'
    }));
    await submitButton.click();

    const patchRequest = httpMockClient.expectOne({
      url: `${environment.restServer}/v1/aaa/tacplus/config`,
      method: `PATCH`
    });

    expect(patchRequest.request.body.enabled).toBe('ENABLED');
    expect(patchRequest.request.body.secret).toBe(password);
    expect(patchRequest.request.body.localFallbackPassword).toBe(fallbackPassword);
    patchRequest.flush({});
    fixture.detectChanges();
    expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_EDIT_TACP_CONFIG_SUCCESS);
  });

  it('should add server', async () => {
    spyOn(component['_alertService'], 'info');
    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();
    const urlInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '#tacserver [formControlName=url]'
    }));
    await urlInput.setValue('192.168.0.1:9000');

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button[form=tacserver]'
    }));
    await submitButton.click();

    fixture.detectChanges();
    const postRequest = httpMockClient.expectOne({
      url: `${environment.restServer}/v1/aaa/servers`,
      method: `POST`
    });

    postRequest.flush({ id: '-1' });
    fixture.detectChanges();
    expect(component['_alertService'].info).toHaveBeenCalledWith(AlertType.INFO_CREATE_TACACS_SERVER);
  });

  it('should validate server', async () => {
    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();
    const testInvalidNames = [
      'localhost.',
      'invalid_hostname',
      'fluffy.-dog.org',
      'fluffy.lazy-.org',
      'fluffy..org',
      'fluffy.lazy_dog.org',
      'invalid@hostname.com',
      'http://hostname.com'
    ];

    const testValidNames = [
      '192.168.0.1',
      '192.168.0.1:4000',
      'fluffy',
      'fluffy-host',
      'fluffy:4000',
      'fluffy.nefeli.io',
      'fluffy.nefeli.io:4000'
    ];

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button[form=tacserver]'
    }));
    const urlInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '#tacserver [formControlName=url]'
    }));

    for (const invalidName of testInvalidNames) {
      await urlInput.setValue(invalidName);
      await submitButton.click();
      // verify error message
      const matError = el.querySelector('#tacserver mat-form-field mat-error');
      expect(matError).not.toBeNull();
      expect(matError.textContent.trim()).toBe('Not a valid address');
      httpMockClient.expectNone({
        url: `${environment.restServer}/v1/aaa/servers`,
        method: `POST`
      });
    }

    for (const validName of testValidNames) {
      await urlInput.setValue(validName);
      await submitButton.click();
      fixture.detectChanges();
      const matError = el.querySelector('#tacserver mat-form-field mat-error');
      expect(matError).toBeNull();
    }
  });

  it('should delete request on remove tacacs server', async () => {
    const tacsServersReq: TestRequest = httpMockClient.expectOne(`${environment.restServer}/v1/aaa/servers`);
    tacsServersReq.flush(cloneDeep(mockServersResponse));
    fixture.detectChanges();

    userService.setUser(mockSystemAdmin);
    fixture.detectChanges();

    const servers = el.querySelectorAll('.tacacs-server');
    expect(servers.length).toBe(3);

    const removeButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: '.tacacs-server button'
    }));

    await removeButton.click();
    const removeRequest = httpMockClient.expectOne({
      url: `${environment.restServer}/v1/aaa/servers/1`,
      method: `DELETE`
    });
    removeRequest.flush({});
    fixture.detectChanges();

    expect(el.querySelectorAll('.tacacs-server').length).toBe(2);
  });

  it('should validate password length', async () => {
    const enabledCheckbox = await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({
      selector: '[formControlName=enabled]'
    }));
    await enabledCheckbox.check();

    const shortPassword = '12345';
    const secretInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=secret]'
    }));
    await secretInput.setValue(shortPassword);
    const secretConfirmInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=confirmSecret]'
    }));
    await secretConfirmInput.setValue(shortPassword);
    const fallbackInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=fallback]'
    }));
    await fallbackInput.setValue(shortPassword);
    const confirmFallbackInput = await loader.getHarness<MatInputHarness>(MatInputHarness.with({
      selector: '[formControlName=confirmFallback]'
    }));
    await confirmFallbackInput.setValue(shortPassword);

    const submitButton = await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({
      selector: 'button[form=tacacs]'
    }));
    await submitButton.click();

    httpMockClient.expectNone({
      url: `${environment.restServer}/v1/aaa/tacplus/config`,
      method: `PATCH`
    });
  });
});
