import { UserService } from 'src/app/users/user.service';
import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl,
  ValidationErrors
} from '@angular/forms';
import { AAAServers } from 'rest_client/pangolin/model/aAAServers';
import { AlertService, AlertType } from '../shared/alert.service';
import { AAAService } from 'rest_client/pangolin/api/aAA.service';
import { AAAServer } from 'rest_client/pangolin/model/aAAServer';
import { TACPlusConfig } from 'rest_client/pangolin/model/tACPlusConfig';
import { TACPlusStatus } from 'rest_client/pangolin/model/tACPlusStatus';
import { take } from 'rxjs/operators';
import { HOST_PATTERN } from '../sites-overview/site.validation';

const Config = {
  minPasswordLength: 8
};

@Component({
  selector: 'nef-system-configuration',
  templateUrl: './system-configuration.component.html',
  styleUrls: ['./system-configuration.component.less']
})
export class SystemConfigurationComponent implements OnInit, AfterViewInit, OnDestroy {

  private _TACACSForm: FormGroup;
  private _TACACSServerForm: FormGroup;
  private _TACACSServers: Array<AAAServer> = [];
  private _lastSubmittedTACPlusStatus: TACPlusStatus;
  private _isSystemAdmin = false;
  private _fragment: string;
  private _subscription = new Subscription();

  @ViewChild('weaversync', {
    read: ElementRef
  }) private _weaversyncCardRef: ElementRef;

  constructor(
    private _route: ActivatedRoute,
    private _fb: FormBuilder,
    private _alertService: AlertService,
    private _userService: UserService,
    private _AAAService: AAAService
  ) { }

  public ngOnInit() {
    const fragmentSubscription = this._route.fragment.subscribe(fragment => {
      this._fragment = fragment;
    });

    this.isSystemAdmin$.pipe(
      take(1)
    ).subscribe((isSystemAdmin: boolean) => {
      this._isSystemAdmin = isSystemAdmin;
      this.initTACACSForm();
      this.initTACACSServerForm();

      const serverSubscription = this._AAAService.getAAAServers()
        .subscribe((serversResponse: AAAServers) => {
          this._TACACSServers = serversResponse.aaaServers ?? [];
        });

      const tacPlusSubscription = this._AAAService.getTACPlusConfiguration()
        .subscribe(({ enabled, secret, localFallbackPassword }: TACPlusConfig) => {
          this._lastSubmittedTACPlusStatus = enabled;
          const value: boolean = enabled === TACPlusStatus.ENABLED ? true : false;
          this._TACACSForm.patchValue({
            enabled: value,
            secret: secret,
            fallback: localFallbackPassword
          });
        });

      this._subscription.add(fragmentSubscription);
      this._subscription.add(serverSubscription);
      this._subscription.add(tacPlusSubscription);
    });
  }

  public ngAfterViewInit(): void {
    if (this._fragment === 'weaversync') {
      this._weaversyncCardRef.nativeElement.scrollIntoView();
    }
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private initTACACSForm() {
    this._TACACSForm = this._fb.group({
      enabled: false,
      secret: ['', this.requiredValidator()],
      confirmSecret: ['', this.matchValidator('secret')],
      fallback: ['', [
        Validators.minLength(Config.minPasswordLength),
        this.requiredValidator()
      ]],
      confirmFallback: ['', this.matchValidator('fallback')]
    });

    if (!this._isSystemAdmin) {
      this._TACACSForm.disable();
    }
  }

  private matchValidator(comparingField: string): ValidatorFn {
    let fieldControl: AbstractControl;
    let comparingControl: AbstractControl;

    return (control: AbstractControl): ValidationErrors => {
      if (!this._TACACSForm) {
        return;
      }

      if (!fieldControl) {
        fieldControl = control;
        comparingControl = this._TACACSForm.get(comparingField);

        const subscription = comparingControl.valueChanges.subscribe(() => {
          control.updateValueAndValidity();
        });

        this._subscription.add(subscription);
      }

      return control.value !== comparingControl.value
        ? {
          confirmSecret: {
            value: control.value
          }
        }
        : undefined;
    };
  }

  private requiredValidator(): ValidatorFn {
    let fieldControl: AbstractControl;
    let enabledControl: AbstractControl;

    return ((control: AbstractControl): ValidationErrors => {
      if (!this._TACACSForm) {
        return;
      }

      if (!fieldControl) {
        fieldControl = control;
        enabledControl = this._TACACSForm.get('enabled');

        const subscription = enabledControl.valueChanges
          .subscribe(() => {
            control.updateValueAndValidity();
          });

        this._subscription.add(subscription);
      }

      return enabledControl.value
        ? Validators.required(control)
        : undefined;
    });
  }

  private initTACACSServerForm() {
    this._TACACSServerForm = this._fb.group({
      url: ['', Validators.pattern(HOST_PATTERN)]
    });

    if (!this._isSystemAdmin) {
      this._TACACSServerForm.disable();
    }
  }

  public submitTACACSForm() {
    const { invalid, pristine, value } = this._TACACSForm;

    if (invalid || pristine) {
      return;
    }

    const config: TACPlusConfig = {
      enabled: value.enabled ? TACPlusStatus.ENABLED : TACPlusStatus.DISABLED
    };

    if (value.enabled) {
      config.secret = value.secret;
      config.localFallbackPassword = value.fallback;
    }

    if (this._lastSubmittedTACPlusStatus === TACPlusStatus.DISABLED
      && config.enabled === TACPlusStatus.DISABLED) {
      return;
    }

    this._AAAService
      .patchTACPlusConfiguration(config)
      .subscribe(
        (_: [any, any]) => {
          this._TACACSForm.markAsPristine();

          this._lastSubmittedTACPlusStatus = config.enabled;

          this._alertService.info(AlertType.INFO_EDIT_TACP_CONFIG_SUCCESS);
        },
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_EDIT_TACP_CONFIG)
      );
  }

  public submitTACACSServerForm() {
    const { invalid, pristine, value } = this._TACACSServerForm;

    if (invalid || pristine) {
      return;
    }

    const server: AAAServer = {
      address: value.url
    };

    this._AAAService.postAAAServer(server)
      .subscribe(
        (({ identifier }: AAAServer) => {
          this._TACACSServers.push({
            identifier,
            address: value.url
          });
          this._alertService.info(AlertType.INFO_CREATE_TACACS_SERVER);
          this._TACACSServerForm.reset();
        }),
        (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_CREATE_TACACS_SERVER)
      );
  }

  public deleteTACACSServer(index: number) {
    const id = this._TACACSServers[index].identifier;
    this._AAAService.deleteAAAServer(id).subscribe(
      (_: [any, any]) => {
        this._TACACSServers.splice(index, 1);
        this._alertService.info(AlertType.INFO_DELETE_TACACS_SERVER);
      },
      (_: HttpErrorResponse) => this._alertService.error(AlertType.ERROR_DELETE_TACACS_SERVER)
    );
  }

  public onTACACSEnabledChanged(checked: boolean) {
    if (!checked) {
      Object
        .entries(this._TACACSForm.controls)
        .filter(([key, _]) => key !== 'enabled')
        .forEach(([_, control]) => {
          control.reset();
        });
    }
  }

  public get TACACSForm(): FormGroup {
    return this._TACACSForm;
  }

  public get TACACSServers(): Array<AAAServer> {
    return this._TACACSServers;
  }

  public get TACACSServerForm(): FormGroup {
    return this._TACACSServerForm;
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get isTACACSEnabled(): boolean {
    const { value } = this._TACACSForm;
    return value.enabled;
  }
}
