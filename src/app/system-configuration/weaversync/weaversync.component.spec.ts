import { AlertType } from 'src/app/shared/alert.service';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatIconModule } from '@angular/material/icon';
import { WeaverSyncComponent } from './weaversync.component';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';
import { HighAvailabilityService } from 'rest_client/pangolin/api/highAvailability.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestUrlInterceptor } from 'src/app/http-interceptors/request-url.interceptor';
import { BASE_PATH } from 'rest_client/pangolin/variables';
import { WeaverSyncConfigMode } from 'rest_client/pangolin/model/weaverSyncConfigMode';
import { WeaverSyncConfigWeaver } from 'rest_client/pangolin/model/weaverSyncConfigWeaver';
import { WeaverSync } from 'rest_client/pangolin/model/weaverSync';
import { WeaverSyncStatusState } from 'rest_client/pangolin/model/weaverSyncStatusState';
import { environment } from '../../../environments/environment';
import { ClusterArchType } from 'rest_client/mneme/model/clusterArchType';
import { ClusterConf } from 'rest_client/pangolin/model/clusterConf';
import { TunnelType } from 'rest_client/pangolin/model/tunnelType';
import { Roles } from 'rest_client/heimdallr/model/roles';
import { UserService } from '../../users/user.service';
import { User } from 'rest_client/heimdallr/model/user';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCheckboxHarness } from '@angular/material/checkbox/testing';
import { cloneDeep } from 'lodash';

describe('WeaversyncComponent', () => {
  let component: WeaverSyncComponent;
  let fixture: ComponentFixture<WeaverSyncComponent>;
  let httpTestingController: HttpTestingController;
  let el: HTMLElement;
  let userService: UserService;
  let loader: HarnessLoader;

  const mockWeaverConfig: ClusterConf = {
    id: '-1',
    name: '642b47c1-1238-4c55-bf95-00fb207be49a',
    type: 'weaver',
    clusterNetwork: {
      arch: ClusterArchType.RACK,
      rack: {
        world: TunnelType.VLAN,
        machine: TunnelType.GREINUDP,
        cluster: TunnelType.GREINUDP
      },
      premise: {}
    },
    fqdn: '10.10.10.66',
    ip: '10.10.10.66',
    weaverFqdn: '10.10.10.66',
    weaverIp: '10.10.10.66',
    storageMode: 's3'
  };

  const mockWeavers: WeaverSyncConfigWeaver[] = [
    {
      id: '-1',
      endpoint: 'https://active.nefeli.io',
      mode: WeaverSyncConfigMode.ACTIVE
    },
    {
      id: '-2',
      endpoint: 'https://standby.nefeli.io',
      mode: WeaverSyncConfigMode.STANDBY
    }
  ];

  const mockWeaverSyncEnabled: WeaverSync = {
    config: {
      beganAt: new Date(),
      term: 0,
      weavers: mockWeavers
    },
    status: {
      state: WeaverSyncStatusState.ACTIVE,
      configSyncStatus: {
        '-1': {
          lastAttemptedAt: new Date('Jan 6, 2021, 12:00:00 PM'),
          lastSucceededAt: new Date('Jan 5, 2021, 10:00:00 PM'),
          error: 'Unexpected failure'
        }
      },
      etcdSyncStatus: {
        '-1': {
          lastAttemptedAt: new Date('Jan 6, 2021, 12:00:00 PM'),
          lastSucceededAt: undefined
        }
      },
      fileSyncStatus: {
        '-1': {
          lastAttemptedAt: new Date('Jan 6, 2021, 12:00:00 PM'),
          lastSucceededAt: new Date('Jan 6, 2021, 12:00:00 PM')
        }
      }
    }
  };

  const mockWeaverSyncDisabled: WeaverSync = {
    config: {
      beganAt: new Date(),
      term: 0,
      weavers: null
    },
    status: {
      state: WeaverSyncStatusState.DISABLED
    }
  };

  const mockUser: User = {
    username: 'ecarino',
    email: 'eric@nefeli.io',
    roles: null
  };

  const mockSiteUserRole: Roles = {
    scope: 'system',
    id: '-1',
    roles: ['user']
  };

  const mockSiteAdminRole: Roles = {
    scope: 'system',
    id: '-1',
    roles: ['admin', 'user']
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatSelectModule,
        MatInputModule,
        MatDialogModule,
        MatCheckboxModule,
        MatTooltipModule,
        MatIconModule
      ],
      declarations: [
        WeaverSyncComponent,
        ConfirmationDialogComponent
      ],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: RequestUrlInterceptor,
          multi: true
        }, {
          provide: BASE_PATH,
          useValue: '/v1'
        },
        HighAvailabilityService,
        UserService
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    userService = TestBed.inject(UserService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeaverSyncComponent);
    loader = TestbedHarnessEnvironment.loader(fixture);
    component = fixture.componentInstance;
    fixture.detectChanges();
    el = fixture.debugElement.nativeElement;

    httpTestingController.expectOne(`${environment.restServer}/v1/weaver/config`).flush(mockWeaverConfig);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should enable weaver sync', async () => {
    spyOn(component['_alertService'], 'error');
    const activeId = '-1';
    const activeUrl = 'https://active.weaver.com';
    const standbyId = '-2';
    const standbyUrl = 'https://standby.weaver.com';

    const user = cloneDeep(mockUser);
    user.roles = mockSiteAdminRole;
    userService.setUser(user);
    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncDisabled);
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;

    // verify form is displayed
    expect(el.querySelectorAll('form input').length).toBe(4);
    await (await loader.getHarness<MatInputHarness>(MatInputHarness.with({ selector: '[formControlName=activeWeaverID]' }))).setValue(activeId);
    await (await loader.getHarness<MatInputHarness>(MatInputHarness.with({ selector: '[formControlName=activeWeaver]' }))).setValue(activeUrl);
    await (await loader.getHarness<MatInputHarness>(MatInputHarness.with({ selector: '[formControlName=standbyWeaverID]' }))).setValue(standbyId);
    await (await loader.getHarness<MatInputHarness>(MatInputHarness.with({ selector: '[formControlName=standbyWeaver]' }))).setValue(standbyUrl);

    // verify status badge
    expect(el.querySelector('header > .badge.grey > div').textContent).toBe('status: disabled');

    // verify alert error
    await (await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({ text: 'Enable' }))).click();
    fixture.detectChanges();
    let req = httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync`);
    expect(req.request.method).toBe('POST');
    req.flush({}, { status: 400, statusText: 'Bad Request' });
    fixture.detectChanges();
    expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_CREATE_WEAVER_CONFIG);

    // verify enable issues correct request
    await (await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({ text: 'Enable' }))).click();
    fixture.detectChanges();

    req = httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync`);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.weavers[0].endpoint).toBe(activeUrl);
    expect(req.request.body.weavers[0].id).toBe(activeId);
    expect(req.request.body.weavers[0].mode).toBe(WeaverSyncConfigMode.ACTIVE);
    expect(req.request.body.weavers[1].endpoint).toBe(standbyUrl);
    expect(req.request.body.weavers[1].id).toBe(standbyId);
    expect(req.request.body.weavers[1].mode).toBe(WeaverSyncConfigMode.STANDBY);
    req.flush({});
    fixture.detectChanges();

    // verify re-retrieve weaversync config
    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`);
  });

  it('should disable weaver sync', waitForAsync(async () => {
    const user = cloneDeep(mockUser);
    user.roles = mockSiteAdminRole;
    userService.setUser(user);

    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncEnabled);
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;

    // verify weaver list
    expect(el.querySelectorAll('.weaver .badge')[0].textContent.trim()).toBe('active');
    expect(el.querySelectorAll('.weaver span')[0].textContent).toBe(mockWeaverSyncEnabled.config.weavers[0].endpoint);
    expect(el.querySelectorAll('.weaver .badge')[1].textContent.trim()).toBe('standby');
    expect(el.querySelectorAll('.weaver span')[1].textContent).toBe(mockWeaverSyncEnabled.config.weavers[1].endpoint);
    expect(el.querySelectorAll('.weaver span')[1].parentElement.querySelector('.promote').textContent.trim())
      .toBe('To promote to active, click here and login to the standby weaver.');
    expect(el.querySelectorAll('.weaver span')[1].parentElement.querySelector('.promote a').href)
      .toBe(`${mockWeaverSyncEnabled.config.weavers[1].endpoint}/`);

    // verify status badge
    expect(el.querySelectorAll('header > .badge.blue > div')[0].textContent).toBe('status: enabled');
    expect(el.querySelectorAll('header > .badge.blue > div')[1].textContent.trim()).toBe('mode: active');

    // verify 'force' disable issues correct request
    await (await loader.getHarness<MatCheckboxHarness>(MatCheckboxHarness.with({ selector: 'mat-checkbox' }))).check();
    await (await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({ text: 'Disable' }))).click();
    fixture.detectChanges();
    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    dialog.close(true);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync/disable?force=true`).flush({});

      // verify re-retrieve weaversync config
      httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`);
    });
  }));

  it('should promote to active', waitForAsync(async () => {
    const user = cloneDeep(mockUser);
    user.roles = mockSiteAdminRole;
    userService.setUser(user);

    // change this weaver to be the standby
    const weaverSync: WeaverSync = cloneDeep(mockWeaverSyncEnabled);
    weaverSync.config.weavers[0].id = '-2';
    weaverSync.config.weavers[1].id = '-1';
    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(weaverSync);
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;

    // verify promote button when on standby weaver
    expect(el.querySelector('.badge.grey').parentElement.querySelector('button').textContent).toBe('Promote to active');
    await (await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({ text: 'Promote to active' }))).click();

    // verify promote to active issues correct request
    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    dialog.close(true);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync/promote`).flush({});

      // verify re-retrieve weaversync config
      httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`);
    });
  }));

  it('should display error for promote to active bad request', waitForAsync(async () => {
    spyOn(component['_alertService'], 'error');
    const user = cloneDeep(mockUser);
    user.roles = mockSiteAdminRole;
    userService.setUser(user);

    // change this weaver to be the standby
    const weaverSync: WeaverSync = cloneDeep(mockWeaverSyncEnabled);
    weaverSync.config.weavers[0].id = '-2';
    weaverSync.config.weavers[1].id = '-1';
    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(weaverSync);
    fixture.detectChanges();
    const el = fixture.debugElement.nativeElement;

    // verify promote button when on standby weaver
    expect(el.querySelector('.badge.grey').parentElement.querySelector('button').textContent).toBe('Promote to active');
    await (await loader.getHarness<MatButtonHarness>(MatButtonHarness.with({ text: 'Promote to active' }))).click();

    // verify promote to active issues correct request
    const dialog: MatDialogRef<ConfirmationDialogComponent> = component['_dialog'].openDialogs[0];
    dialog.close(true);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync/promote`).flush({}, { status: 400, statusText: 'Bad Request' });
      fixture.detectChanges();
      expect(component['_alertService'].error).toHaveBeenCalledWith(AlertType.ERROR_PROMOTE_WEAVER);
    });
  }));

  it('should hide form and enable button when disabled and system-user', () => {
    const user = cloneDeep(mockUser);
    user.roles = mockSiteUserRole;
    userService.setUser(user);

    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncDisabled);
    fixture.detectChanges();

    // verify status badge
    expect(el.querySelector('header > .badge.grey > div').textContent).toBe('status: disabled');

    // verify form and button is missing
    expect(el.querySelector('form')).toBeFalsy();
  });

  it('should hide buttons when enabled and system-user', () => {
    const user = cloneDeep(mockUser);
    user.roles = mockSiteUserRole;
    userService.setUser(user);

    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncEnabled);
    fixture.detectChanges();

    // verify status badge
    expect(el.querySelectorAll('header > .badge.blue > div')[0].textContent).toBe('status: enabled');
    expect(el.querySelectorAll('header > .badge.blue > div')[1].textContent.trim()).toBe('mode: active');

    // verify weaver list
    expect(el.querySelectorAll('.weaver .badge')[0].textContent.trim()).toBe('active');
    expect(el.querySelectorAll('.weaver span')[0].textContent).toBe(mockWeaverSyncEnabled.config.weavers[0].endpoint);
    expect(el.querySelectorAll('.weaver .badge')[1].textContent.trim()).toBe('standby');
    expect(el.querySelectorAll('.weaver span')[1].textContent).toBe(mockWeaverSyncEnabled.config.weavers[1].endpoint);

    // verify no promote or disable buttons
    expect(el.querySelector('button')).toBeFalsy();
  });

  it('should display sync status', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncEnabled);
    fixture.detectChanges();

    const syncStatusEl = el.querySelector('.sync-status');
    expect(syncStatusEl).not.toBeNull();

    const rows = el.querySelectorAll('.sync-status > div');
    expect(rows.length).toBe(5);
    expect(rows[1].querySelector('h1').textContent).toBe('WeaverSync Config');
    expect(rows[1].querySelectorAll('span')[0].textContent).toBe('Jan 5, 2021, 10:00:00 PM');
    expect(rows[1].querySelectorAll('span')[1].textContent).toBe('Jan 6, 2021, 12:00:00 PM');

    expect(rows[1]
      .querySelectorAll('span')[2]
      .querySelector('em').textContent
    ).toBe('Unexpected failure');
  });

  it('should display sync status for system user', () => {
    const user = cloneDeep(mockUser);
    user.roles = mockSiteUserRole;
    userService.setUser(user);

    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncEnabled);
    fixture.detectChanges();
    expect(el.querySelector('.sync-status')).not.toBeNull();
  });

  it('should display not configured message for system user', () => {
    const user = cloneDeep(mockUser);
    user.roles = mockSiteUserRole;
    userService.setUser(user);

    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncDisabled);
    fixture.detectChanges();
    expect(el.querySelector('.sync-status')).toBeNull();

    const notConfiguredEl = el.querySelector('p');
    expect(notConfiguredEl).toBeDefined();
    expect(notConfiguredEl.textContent).toBe('Not configured')
  });

  it('should display dash symbol when there is no data', () => {
    httpTestingController.expectOne(`${environment.restServer}/v1/ha/weaversync?status=true`).flush(mockWeaverSyncEnabled);
    fixture.detectChanges();
    const rows = el.querySelectorAll('.sync-status > div');
    expect(rows[2].querySelector('h1').textContent).toBe('Services/NFs');
    expect(rows[2].querySelectorAll('span')[0].textContent).toBe('–');
  });
});
