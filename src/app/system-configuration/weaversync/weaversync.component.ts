import { filter, switchMap } from 'rxjs/operators';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Observable, Subscription } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog, MatDialogConfig, MatDialogRef } from '@angular/material/dialog';
import { AlertService, AlertType } from 'src/app/shared/alert.service';
import { UserService } from '../../users/user.service';
import { HighAvailabilityService } from 'rest_client/pangolin/api/highAvailability.service';
import { WeaverSyncConfig } from 'rest_client/pangolin/model/weaverSyncConfig';
import { ClusterConf } from 'rest_client/pangolin/model/clusterConf';
import { WeaverService } from 'rest_client/pangolin/api/weaver.service';
import { WeaverSyncConfigWeaver } from 'rest_client/pangolin/model/weaverSyncConfigWeaver';
import { WeaverSync } from 'rest_client/pangolin/model/weaverSync';
import { WeaverSyncConfigMode } from 'rest_client/pangolin/model/weaverSyncConfigMode';
import { WeaverSyncStatusState } from 'rest_client/pangolin/model/weaverSyncStatusState';
import { ConfirmationDialogData, ConfirmationDialogComponent } from 'src/app/shared/confirmation-dialog/confirmation-dialog.component';
import { URL_PATTERN } from '../../sites-overview/site.validation';
import { SyncStatus } from 'rest_client/pangolin/model/syncStatus';

const DIALOG_CONFIG: MatDialogConfig = {
  width: '510px',
  autoFocus: false,
  restoreFocus: false
};

@Component({
  selector: 'nef-weaversync',
  templateUrl: './weaversync.component.html',
  styleUrls: ['./weaversync.component.less']
})
export class WeaverSyncComponent implements OnInit, OnDestroy {
  private _form: FormGroup;
  private _subscription = new Subscription();
  private _weavers: WeaverSyncConfigWeaver[] = [];
  private _state: WeaverSyncStatusState;
  private _forceDisable: boolean;
  private _dialogSubscription: Subscription;
  private _weaverConfig: ClusterConf;
  private _weaverConfigSyncStatus: SyncStatus;
  private _servicesSyncStatus: SyncStatus;
  private _fileSyncStatus: SyncStatus;
  private _accountsSyncStatus: SyncStatus;

  constructor(
    private _userService: UserService,
    private _alertService: AlertService,
    private _highAvailabilityService: HighAvailabilityService,
    private _weaverService: WeaverService,
    private _fb: FormBuilder,
    private _dialog: MatDialog
  ) { }

  public ngOnInit() {
    this.initForm();
    this.getWeaverConfig();
    this.getWeaverSync();
  }

  public ngOnDestroy() {
    this._subscription.unsubscribe();
  }

  private getWeaverConfig() {
    this._subscription.add(
      this._weaverService.getWeaverConfig().subscribe((config: ClusterConf) => this._weaverConfig = config));
  }

  private getWeaverSync() {
    const subscription = this._highAvailabilityService.getWeaverSync(true).subscribe(
      ({ config, status }: WeaverSync) => {
        this._state = status?.state;
        this._weavers = config?.weavers ?? [];
        this._weaverConfigSyncStatus = status?.configSyncStatus ? Object.values(status?.configSyncStatus)[0] : undefined;
        this._servicesSyncStatus = status?.etcdSyncStatus ? Object.values(status?.etcdSyncStatus)[0] : undefined;
        this._fileSyncStatus = status?.etcdSyncStatus ? Object.values(status?.fileSyncStatus)[0] : undefined;
        this._accountsSyncStatus = status?.vaultSyncStatus ? Object.values(status?.vaultSyncStatus)[0] : undefined;
      }
    );

    this._subscription.add(subscription);
  }

  private initForm() {
    this._form = this._fb.group({
      activeWeaver: ['', [Validators.pattern(URL_PATTERN), Validators.required]],
      activeWeaverID: ['', Validators.required],
      standbyWeaver: ['', [Validators.pattern(URL_PATTERN), Validators.required]],
      standbyWeaverID: ['', Validators.required]
    });
  }

  private saveConfig(config: WeaverSyncConfig) {
    this._highAvailabilityService.postWeaverSync(config)
      .subscribe(
        () => {
          this.getWeaverSync();
          this._alertService.info(AlertType.INFO_CREATE_WEAVER_CONFIG);
        },
        () => this._alertService.error(AlertType.ERROR_CREATE_WEAVER_CONFIG)
      );
  }

  public onSubmit() {
    const { invalid, pristine } = this._form;

    if (invalid || pristine) {
      return;
    }

    const weavers: WeaverSyncConfigWeaver[] = [{
      endpoint: this._form.get('activeWeaver').value,
      id: this._form.get('activeWeaverID').value,
      mode: WeaverSyncConfigMode.ACTIVE
    }, {
      endpoint: this._form.get('standbyWeaver').value,
      id: this._form.get('standbyWeaverID').value,
      mode: WeaverSyncConfigMode.STANDBY
    }];

    this.saveConfig({ weavers });
  }

  public onForceDisableChange({ checked }: MatCheckboxChange) {
    this._forceDisable = checked;
  }

  public onPromoteWeaver(weaver: WeaverSyncConfigWeaver) {
    const { id, endpoint } = weaver;

    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: 'Promote this system to become the active Weaver.',
      action: 'Promote'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef.afterClosed().pipe(
      filter(Boolean),
      switchMap((_: boolean) => this._highAvailabilityService.postWeaverSyncPromotion())
    ).subscribe(
      () => {
        this.getWeaverSync();
        this._alertService.info(AlertType.INFO_PROMOTE_WEAVER);
      },
      () => this._alertService.error(AlertType.ERROR_PROMOTE_WEAVER)
    );
  }

  public onDisable() {
    if (this._dialogSubscription) {
      this._dialogSubscription.unsubscribe();
    }

    const data: ConfirmationDialogData = {
      description: `This will ${this._forceDisable ? 'force ' : ''}disable WeaverSync.`,
      action: 'Disable'
    };

    const dialogRef: MatDialogRef<ConfirmationDialogComponent, boolean> = this._dialog.open(
      ConfirmationDialogComponent,
      { ...DIALOG_CONFIG, data }
    );

    this._dialogSubscription = dialogRef.afterClosed().pipe(
      filter(Boolean),
      switchMap((_: boolean) => this._highAvailabilityService.postWeaverSyncDisable(this._forceDisable))
    ).subscribe(
      () => {
        this.getWeaverSync();
        this._alertService.info(AlertType.INFO_DISABLE_WEAVER_SYNC);
      },
      () => this._alertService.error(AlertType.ERROR_DISABLE_WEAVER_SYNC)
    );
  }

  public get isSystemAdmin$(): Observable<boolean> {
    return this._userService.isSystemAdmin$;
  }

  public get isSystemUser$(): Observable<boolean> {
    return this._userService.isSystemUser$;
  }

  public get form(): FormGroup {
    return this._form;
  }

  public get WeaverSyncConfigMode(): typeof WeaverSyncConfigMode {
    return WeaverSyncConfigMode;
  }

  public get statusState(): WeaverSyncStatusState {
    return this._state;
  }

  public get WeaverSyncStatusState(): typeof WeaverSyncStatusState {
    return WeaverSyncStatusState;
  }

  public get weavers(): WeaverSyncConfigWeaver[] {
    return this._weavers;
  }

  public get weaverMode(): WeaverSyncConfigMode {
    return this._weavers?.find((weaver: WeaverSyncConfigWeaver) => this._weaverConfig?.id === weaver.id)?.mode;
  }

  public get standbyWeaver(): WeaverSyncConfigWeaver {
    return this._weavers?.find((weaver: WeaverSyncConfigWeaver) => weaver.mode === WeaverSyncConfigMode.STANDBY);
  }

  public get weaverConfigSyncStatus(): SyncStatus {
    return this._weaverConfigSyncStatus;
  }

  public get servicesSyncStatus(): SyncStatus {
    return this._servicesSyncStatus;
  }

  public get accountsStatus(): SyncStatus {
    return this._accountsSyncStatus;
  }

  public get filesStatus(): SyncStatus {
    return this._fileSyncStatus;
  }
}
